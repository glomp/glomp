var templating_config = {
    section_delete_save: '',
    widget_delete_save: '',
    widget_load_options: ''
};

function load_components_binding() {
    load_sections_binds();
    templating_widget_binds();
}

function load_sections_binds() {
    /* Add widget qtip */
    var widget_btn = $('.templating_widget_add');
    $(widget_btn).qtip({
        content: 'Add Widget',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });

    /* Add widget click */
    $(widget_btn).click(function() {
        selected_section = $(this).closest('.section_holder').data('section_id');
        selected_widget_position = $(this).data('position');
        $('#templating_widget_dialog').dialog('open');
    });

    /* Add section close/move qtip */
    var section_del_btn = $('.templating_section_delete');
    var section_move_btn = $('.templating_section_move');

    $(section_del_btn).qtip({
        content: 'Remove Section',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });
    $(section_move_btn).qtip({
        content: 'Move Section',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });

    /*Bind tooltip events */
    $('.section_holder').bind("mouseenter mouseleave", function() {
        var section_tool_tip = $(this).children('.section_tool_tip');
        if ($(section_tool_tip).is(':hidden')) {
            return $(section_tool_tip).show();
        } else {
            return $(section_tool_tip).hide();
        }
    });

    $('.section_holder').children('.section_tool_tip').children('.templating_section_delete').click(function() {
        var section_id = $(this).closest('.section_holder').data('section_id');
        var section_del_btn = $(this);
        var section_move_btn = $(this).siblings('.templating_section_move');
        var widget_btn = $(this).closest('.section_holder').find('.templating_widget_add');

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: templating_config.section_delete_save,
            data: {
                section_id: section_id
            },
            async: false,
            success: function(response) {
                //remove to DOM
                var qtip_section_del_id = $(section_del_btn).qtip('api').get('id');
                var qtip_section_move_id = $(section_move_btn).qtip('api').get('id');
                $("div[data-section_id='" + section_id + "']").appendTo("#remove_sections");

                $('#qtip-' + qtip_section_del_id).remove();
                $('#qtip-' + qtip_section_move_id).remove();
                $.each($(widget_btn), function(key, value) {
                    var qtip_widget_add_id = $(value).qtip('api').get('id');
                    $('#qtip-' + qtip_widget_add_id).remove();
                });
                
                $('#option_holder').html('');
            }
        });
    });
}

function templating_widget_binds() {
    var widget_del_btn = $('.templating_widget_delete');
    var widget_edit_btn = $('.templating_widget_edit');
    /* Add widget del qtip */
    $(widget_del_btn).qtip({
        content: 'Delete Widget',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });
    /* Add widget edit qtip */
    $(widget_edit_btn).qtip({
        content: 'Edit Widget',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });

    /*Bind tooltip events */
    $('.widget_holder').bind("mouseenter mouseleave", function() {
        var widget_tool_tip = $(this).children('.widget_tool_tip');
        if ($(widget_tool_tip).is(':hidden')) {
            return $(widget_tool_tip).show();
        } else {
            return $(widget_tool_tip).hide();
        }
    });

    $(widget_del_btn).click(function() {
        var widget_id = $(this).closest('.widget_holder').data('widget_id');
        var widget_type = $(this).closest('.widget_holder').data('widget_type');
        
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {
                widget_id: widget_id,
                widget_type: widget_type
            },
            async: false,
            url: templating_config.widget_delete_save,
            success: function(response) {
                if (response.success) {
                    var qtip_widget_del_id = $(widget_del_btn).qtip('api').get('id');
                    var qtip_widget_edit_id = $(widget_del_btn).siblings('.templating_widget_edit').qtip('api').get('id');
                    
                    $("div[data-widget_id='" + widget_id + "']").remove();
                    $('#qtip-' + qtip_widget_del_id).remove();
                    $('#qtip-' + qtip_widget_edit_id).remove();

                    $(widget_del_btn).closest("div[data-widget_id='"+widget_id+"']").siblings('.templating_widget_add').show();

                    $('#option_holder').html('');
                }
            }
        });
    });


    $(widget_edit_btn).click(function() {
        var widget_id = $(this).closest('.widget_holder').data('widget_id');
        var widget_type = $(this).closest('.widget_holder').data('widget_type');
        
        $('#option_holder').load(templating_config.widget_load_options, {
            widget_id: widget_id,
            widget_type: widget_type
        });
    });
}