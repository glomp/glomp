jQuery(document).ready(function(){
    var addProductTpl = jQuery('.add-product').first().clone();
    /* remove input values from the template */
    jQuery(addProductTpl).find('select').val('');
    jQuery(addProductTpl).find('input').val('');
    jQuery(addProductTpl).find('.product_id').replaceWith('<input type="hidden" class="product_id" name="product_id[]" />');
    jQuery(addProductTpl).find('.voucher_qty').replaceWith('<input type="text" class="voucher_qty" name="voucher_qty[]" placeholder="Voucher Quantity" />');
    if( jQuery(addProductTpl).hasClass('edit-record') ) {
        jQuery(addProductTpl).find('.product_id').replaceWith('<input type="hidden" class="product_id" name="new_product_id[]" />');
        jQuery(addProductTpl).find('.voucher_qty').replaceWith('<input type="text" class="voucher_qty" name="new_voucher_qty[]" placeholder="Voucher Quantity" />');
    }
    jQuery(addProductTpl).find('.trigger_product_list').replaceWith('<input type="text" class="trigger_product_list" placeholder="Select a Product" />');
    jQuery(addProductTpl).find('.cam_details_id').remove();

    var prod_list = jQuery('#product-list').html();

    /* add product rows */
    jQuery(document).on('click','#add_products',function(e){
        e.preventDefault();
        jQuery('.add-product').last().after('<tr class="add-product">'+addProductTpl.html()+'</tr>');
    });

    /* trigger the product list drop down */
    jQuery(document).on('focus','.trigger_product_list',function(e){
        /* hide any active product list drop down */
        jQuery('td.add_products').find('.place-holder').html('').css('border','0').hide();
        /* show the product list */
        jQuery(e.target).next('.place-holder').html(prod_list).css('border','1px solid #ddd').show();
        /* filter product list by region */
        var parent = jQuery(e.target).parents('tr'),
            regionId = parent.find('.filter_by_region').val();
        if( regionId != '' ) {
            parent.find('.place-holder > li:not(.sponsor),.place-holder > li.sponsor li').not('[data-region_id='+regionId+']').hide();
        }
    });
    /* filter product list by keyword */
    jQuery(document).on('keyup','.trigger_product_list',function(e){
        delay(function(){
            var parent = jQuery(e.target).parents('tr'),
                list = parent.find('.place-holder');

            if(e.target.value != '') {
                jQuery(list).find("li.prod_info").slideUp();
                jQuery(list).find("li.prod_info[data-keywords*='"+e.target.value+"']").slideDown();
            } else {
                jQuery(list).find("li.prod_info").slideDown();
            }
        }, 500 );
    });
    /* remove product list drop down when body is clicked */
    jQuery(document).on('click', function(e){
        if(jQuery(e.target).attr('class')!='trigger_product_list') {
            jQuery('td.add_products').find('.place-holder').html('').css('border','0').hide();
        }
    });
    /* select a product */
    jQuery(document).on('click','.prod_info',function(e){
        var obj;
        if(e.target.tagName == 'LI') {
            obj = jQuery(e.target);
        } else {
            obj = jQuery(e.target).parent('li.prod_info');
        }
        console.log( jQuery(obj).parents('td.add_products').find('.product_id') );
        jQuery(obj).parents('td.add_products').find('.product_id').val( jQuery(obj).attr('data-prod_id') );
        jQuery(obj).parents('td.add_products').find('.trigger_product_list').val( jQuery(obj).attr('data-prod_name') );
        jQuery(obj).parents('td.add_products').find('.place-holder').html('').css('border','0').hide();
    });
    /* remove button - first button resets the row */
    jQuery(document).on('click','.remove_row',function(e){
        e.preventDefault();

        if( jQuery(addProductTpl).hasClass('edit-record') ) {
            if (!confirm('Are you sure you want to delete this record?')) {
                return false;
            }

            $.ajax({
                type: 'GET',
                url: jQuery(this).attr('href'),
                success: function(html) {
                    if(jQuery('.remove_row').length > 1) {
                        jQuery(e.target).parents('tr').remove();
                    } else {
                        jQuery(e.target).parents('tr').find('input').val('');
                        jQuery(e.target).parents('tr').find('select').val('');
                    }
                }
            });
        } else {
            if(jQuery('.remove_row').length > 1) {
                jQuery(e.target).parents('tr').remove();
            } else {
                jQuery(e.target).parents('tr').find('input').val('');
                jQuery(e.target).parents('tr').find('select').val('');
            }
        }
    });
});
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();
