var wt_opt = {};
var wt_current_image = 0;
var loadingMoreStories=false;
function walkThrough(prop) {
    $('#arrow_prev_wrapper, #arrow_next_wrapper, #arrow_close_wrapper').css('background-color', prop.bgcolor);
}

function walkThrough_1st() {
    $('html, body').animate({ scrollTop: 0 }, 0);
    $('#how_to_glomp_diag').dialog('close');
    $('#wt_overlay, #wt_frame_outer').removeClass('hide');
    
    $('#arrow_prev_wrapper').addClass('hide');
    $('#arrow_next_wrapper').addClass('hide');
    $('#arrow_close_wrapper').addClass('hide');
    
    $("#wt_frame_loader").fadeIn("fast");
        $("#wt_frame").hide();
        //$('#wt_frame').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame').html('<img id="wt_frame_img" src="'+wt_opt.images[wt_current_image]+'">');
        $('#wt_frame_img').waitForImages(function() {
            setTimeout(function()
            {
                $("#wt_frame_loader").fadeOut("fast",function(){                                
                    $("#wt_frame").show( "slide", { direction: "right" }, "normal", function(){
                        $('#arrow_next_wrapper').removeClass('hide');
                    });
                });
            }, 500);
        });
}
function walkThrough_1st_2() {
    $('html, body').animate({ scrollTop: 0 }, 0);    
    $('#wt_overlay, #wt_frame_outer_2').removeClass('hide');
    
    $('#arrow_prev_wrapper_2').addClass('hide');
    $('#arrow_next_wrapper_2').addClass('hide');
    $('#arrow_close_wrapper_2').addClass('hide');
    
    $("#wt_frame_loader_2").fadeIn("fast");
        $("#wt_frame_2").hide();
        //$('#wt_frame_2').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame_2').html('<img id="wt_frame_2_img" src="'+wt_opt.images[wt_current_image]+'" style="width:240px">');
        $('#wt_frame_2_img').waitForImages(function() {
            setTimeout(function()
            {
                $("#wt_frame_loader_2").fadeOut("fast",function(){                                
                    $("#wt_frame_2").show( "slide", { direction: "right" }, "normal", function(){
                        $('#arrow_next_wrapper_2').removeClass('hide');
                    });
                });
            }, 500);
        });
}

function walkThrough_Next() {
    wt_current_image = wt_current_image + 1;
    
    $('#arrow_prev_wrapper').addClass('hide');
    $('#arrow_next_wrapper').addClass('hide');
    $('#arrow_close_wrapper').addClass('hide');
    
    $("#wt_frame").hide( "slide", { direction: "left" }, "normal", function(){
        $("#wt_frame_loader").fadeIn("fast");
        //$('#wt_frame').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame').html('<img id="wt_frame_img" src="'+wt_opt.images[wt_current_image]+'">');		
        $('#wt_frame_img').waitForImages(function() {
            setTimeout(function()
            {                            
                $("#wt_frame_loader").fadeOut("fast",function(){                                
                    $("#wt_frame").show( "slide", { direction: "right" }, "normal", function(){
                        $('#arrow_prev_wrapper').removeClass('hide');
                        wt_current_image_last = wt_current_image + 1;
                        if (wt_opt.images[wt_current_image_last] != '') {
                            $('#arrow_next_wrapper').removeClass('hide');
                        } else {
                            $('#arrow_close_wrapper').removeClass('hide');
                        }
                    });
                });
            }, 500);
        });
    });
}
function walkThrough_Next_2() {
    wt_current_image = wt_current_image + 1;
    
    $('#arrow_prev_wrapper_2').addClass('hide');
    $('#arrow_next_wrapper_2').addClass('hide');
    $('#arrow_close_wrapper_2').addClass('hide');
    
    $("#wt_frame_2").hide( "slide", { direction: "left" }, "normal", function(){
        $("#wt_frame_loader_2").fadeIn("fast");
        //$('#wt_frame_2').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame_2').html('<img id="wt_frame_2_img" src="'+wt_opt.images[wt_current_image]+'" style="width:240px">');
        $('#wt_frame_2_img').waitForImages(function() {
            setTimeout(function()
            {                            
                $("#wt_frame_loader_2").fadeOut("fast",function(){                                
                    $("#wt_frame_2").show( "slide", { direction: "right" }, "normal", function(){
                        $('#arrow_prev_wrapper_2').removeClass('hide');
                        wt_current_image_last = wt_current_image + 1;
                        if (wt_opt.images[wt_current_image_last] != '') {
                            $('#arrow_next_wrapper_2').removeClass('hide');
                        } else {
                            $('#arrow_close_wrapper_2').removeClass('hide');
                        }
                    });
                });
            }, 500);
        });
    });
}

function walkThrough_Prev() {
    wt_current_image = wt_current_image - 1; 
    
   $('#arrow_prev_wrapper').addClass('hide');
   $('#arrow_next_wrapper').addClass('hide');
   $('#arrow_close_wrapper').addClass('hide');
   
    $("#wt_frame").hide( "slide", { direction: "right" }, "normal", function(){
        $("#wt_frame_loader").fadeIn("fast");     
        //$('#wt_frame').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame').html('<img id="wt_frame_img" src="'+wt_opt.images[wt_current_image]+'">');		
        $('#wt_frame_img').waitForImages(function() {
            setTimeout(function()
            {                            
                $("#wt_frame_loader").fadeOut("fast",function(){                                
                    $("#wt_frame").show( "slide", { direction: "left" }, "normal", function(){
                       if (wt_current_image != 0) {
                           $('#arrow_prev_wrapper').removeClass('hide');
                       }
                       $('#arrow_next_wrapper').removeClass('hide');
                    });
                });
            }, 500);
        });             
    });
}
function walkThrough_Prev_2() {
    wt_current_image = wt_current_image - 1; 
    
   $('#arrow_prev_wrapper_2').addClass('hide');
   $('#arrow_next_wrapper_2').addClass('hide');
   $('#arrow_close_wrapper_2').addClass('hide');
   
    $("#wt_frame_2").hide( "slide", { direction: "right" }, "normal", function(){
        $("#wt_frame_loader_2").fadeIn("fast");     
        //$('#wt_frame_2').prop("src",wt_opt.images[wt_current_image]);
		$('#wt_frame_2').html('<img id="wt_frame_2_img" src="'+wt_opt.images[wt_current_image]+'" style="width:240px">');
        $('#wt_frame_2_img').waitForImages(function() {
            setTimeout(function()
            {                            
                $("#wt_frame_loader_2").fadeOut("fast",function(){                                
                    $("#wt_frame_2").show( "slide", { direction: "left" }, "normal", function(){
                       if (wt_current_image != 0) {
                           $('#arrow_prev_wrapper_2').removeClass('hide');
                       }
                       $('#arrow_next_wrapper_2').removeClass('hide');
                    });
                });
            }, 500);
        });             
    });
}

$(document).ready(function(e) {    
    // Start Walkthrough 
    $('.glomp_dashboard_menu').click(function(){
        window.location =  GLOMP_SITE_URL + 'user/searchFriends/' ;
    });
        
    $('#how_to_glomp_diag').dialog({						
        autoOpen: false,
        closeOnEscape: false ,
        resizable: false,					
        dialogClass:'noTitleStuff overflowVisible',
        title:'Please wait...',
        modal: true,
        position: 'center',
        width:290
    });

    $('#how_to_glomp').click(function(){
        $('#how_to_glomp_diag').dialog('open');
    });

    $("#how_to_close").click(function() {
        $("#glomp_walkthrough_menu").hide( "blind", { direction: "up" }, "slow" );
        $('#glomp_buzz_content, #glomp_buzz_nano').height(480);
        
        $(".buzzScroll").nanoScroller();
    });
    
    $("#wt_search_friends").click(function(){
        wt_opt  = {
            bgcolor: 'rgba(203,18,139)',
            images: [
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_friend_F1.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_friend_F2.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_friend_F3.png',
                ''
            ]
        };
        
        walkThrough(wt_opt);
        walkThrough_1st();
    });
    $("#how_to_redeem").click(function(){
        var NewDialog = $('<div id="how_to_redeemDialog" class="tourDialog" align="center"> \
				<div id="" class="tourDialog_1_content" style="padding:15px;">\
				<div style="text-align:justify;font-weight:bold"> '+ $('#wt_desk_redeem_1').text() +'</div> \
				<div style="padding:20px 0px;" id="redeem_button_wrapper" > \
				</div> \
				</div> \
				</div>');
        //dialogClass:'dialog_style_tour',
        NewDialog.dialog({
            position: {
                my: 'center center',
                at: 'center center',
                of: $(document)
            },
            dialogClass: 'noButtonAndTitleStuff',
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            modal: true,
            width: 320,
            height:180,
            show: '',
            hide: '',
        });
        $elem = $('<button type="button" class="btn-custom-darkblue btn-block"  id="" >'+ $('#wt_desk_redeem_2').text() +'</button>');
        $elem.on('click', function(e) {
            $('#how_to_redeemDialog').dialog("close");            
            setTimeout(function() {
                $('#how_to_redeemDialog').dialog('destroy').remove();
            }, 500);
            
            wt_opt  = {
                bgcolor: 'rgba(242,121,37)',
                images: [
                    GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WT_redeem_F1.png',
                    GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WT_redeem_F2.png',
                    GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WT_redeem_F3.png',
                    GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WT_redeem_F4.png',
                    ''
                ]
            };            
            
            walkThrough_1st_2();

        });
        $("#redeem_button_wrapper").append($elem);
        NewDialog.dialog("open");
    });
    
    
    $("#wt_invite_friends").click(function(){
        wt_opt  = {
            bgcolor: 'rgba(242,121,37)',
            images: [
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_email_F1.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_email_F2.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_email_F3.png',
                ''
            ]
        };
        
        walkThrough(wt_opt);
        walkThrough_1st();
    });
    
    $("#wt_invite_fb").click(function(){
        wt_opt  = {
            bgcolor: 'rgba(4,140,209)',
            images: [
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_Fbook_F1.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_Fbook_F2.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_Fbook_F3.png',
                GLOMP_BASE_URL + 'assets/frontend/img/Glomp_WebWT_glomp_Fbook_F4.png',
                ''
            ]
        };
        
        walkThrough(wt_opt);
        walkThrough_1st();
    });
    
    $('#arrow_next_wrapper').click(function(){
        walkThrough_Next();
    });
    
    $('#arrow_prev_wrapper').click(function(){
        walkThrough_Prev();
    });
    
    $('#arrow_close_wrapper').click(function(){
        $('#wt_overlay, #wt_frame_outer').addClass('hide');
        //Reset
        wt_opt = {};
        wt_current_image = 0;
    });
    
    
    //2
    $('#arrow_next_wrapper_2').click(function(){
        walkThrough_Next_2();
    });
    
    $('#arrow_prev_wrapper_2').click(function(){
        walkThrough_Prev_2();
    });
    
    $('#arrow_close_wrapper_2').click(function(){
        $('#wt_overlay, #wt_frame_outer_2').addClass('hide');
        //Reset
        wt_opt = {};
        wt_current_image = 0;
    });
    // End
    check_if_reactivated(is_reactivated);
    $(".buzzScroll").nanoScroller();
    $(".buzzScroll").bind("scrollend", function(e){
        
        if(!loadingMoreStories)
        {
            loadingMoreStories=true;                        
            start_count=$('#glomp_buzz_content').children().last().data().id;
            if(start_count>0)
            {
                
                $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">'+ $('#Loading_more_stories').text() +'<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                $('#glomp_buzz_content').append($loader);
                data ='start_count='+start_count;
                setTimeout(function() {
                    $.ajax({
                        type: "POST",
                        dataType: 'html',
                        url: 'user/getMoreStories',
                        data: data,
                        success: function(response){ 
                            if(response!='' &&  response!='<!---->')
                            {
                                 $('#id_loader').remove();
                                $('#glomp_buzz_content').append(response);
                                $(".buzzScroll").nanoScroller();            
                                loadingMoreStories=false;
                                $('.glomp_fb_share').unbind('hover');
                                $('.glomp_fb_share').hover(        
                                    function(e){
                                        $this=$(this);
                                        $data=$this.data();                          
                                        
                                        if($data.voucher_id in share_wrapper)
                                        {
                                         
                                            clearTimeout( share_wrapper[$data.voucher_id] );                
                                            delete share_wrapper[$data.voucher_id];
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                        
                                        
                                        $('.glomp_fb_share').each(function(){
                                            $this_temp=$(this);
                                            $data_temp=$this_temp.data();                 
                                            if($data_temp.voucher_id!=$data.voucher_id){                    
                                                $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                                                clearTimeout( share_wrapper[$data.voucher_id] );                
                                                delete share_wrapper[$data.voucher_id];
                                            }
                                            else
                                            {
                                                //$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); 
                                            }
                                        });
                                        $('#share_wrapper_box_'+$data.voucher_id).fadeIn();
                                         e.stopPropagation();
                                    },
                                    function(){                
                                        $this=$(this);
                                        $data=$this.data();   
                                        if($data.voucher_id in share_wrapper)
                                        {            
                                            share_wrapper[$data.voucher_id]= setTimeout(function() {
                                                                console.log($data.voucher_id);
                                                                $('#share_wrapper_box_'+$data.voucher_id).fadeOut();
                                                                delete share_wrapper[$data.voucher_id];                                    
                                                            }, 500);								
                                        }            
                                    }
                                );
                            }
                            else
                            {                                
                                $('#id_loader').remove();
                                $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">No more stories to load.</div>');
                                $('#glomp_buzz_content').append($no_more);
                            }
                        }
                    });
                }, 1);
            
            }
            //console.log($('#glomp_buzz_content').children().last().data().id);
            //xx=1;
        }
           
    });        
    
    
    $(".glompedPanel").nanoScroller();
    $(".full_message").on('click', function(e) {
        $('.popUp').hide(100);
        var _ID = $(this).attr('id');
        $('#' + _ID + '_open').show(100, function() {
            $(".popUp").nanoScroller();
        });
        e.stopPropagation();
    });
    $('body').click(function() {
        $('.popUp').hide(100);
        $('.sutoSugSearch').hide();
    });

    var _y = ($(window).height() / 2) - (parseInt($(".glompItemPopUp").css("height")) / 2);
    var _x = ($(window).width() / 2) - (parseInt($(".glompItemPopUp").css("width")) / 2);
    $(".glompItemPopUp").css({"margin-top": _y, "margin-left": _x});


    //close glomp popup box
    $('._close_popup').click(function() {
        $('.glompItemPopUp').fadeOut();
    });
    /* end of glomp item popup */

    $('#Invite_Facebook').click(function() {
        window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getFb=true';
        //checkLoginStatus();
        //getPersonalMessage();		
        //getFbFriendsList();
        //fbSend();
        //glomp();
    });
    $('#Invite_LinkedIn').click(function() {
        window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getLinkedIn=true';
    });
    $('#invite_btn').click(function() {
        $('#inviteDialog').dialog('destroy').remove();
        var name = $('#i_name').val();
        var email = $('#i_email').val();
        var location = $('#location').val();
        var NewDialog = $('<div id="inviteDialog" align="center">\
						  <div class="" style="width:;background-color:;color:#59606B;font-size:12px !important;padding:0px 10px;" align="left">\
						  <h2 class="glompFont">Invite Friend</h2>\
						  	<form id="inviteFormDialog">\
						 	<table width="300" border="0" cellspacing="2" cellpadding="2" background="#fff">\
								<tr>\
									<td>Name</td>\
									<td><input type="text" name="name" id="name" value="' + name + '" style="width:250px" placeholder="Name" ></td>\
								</tr>\
								<tr>\
									<td>Email</td>\
									<td><input type="text" name="email" id="email" value="' + email + '" style="width:250px"  placeholder="Email"></td>\
								</tr>\
								<tr>\
									<td>Location</td>\
									<td><input type="text" name="location" id="invite_location" value="' + location + '" style="width:250px"  autocomplete="off" placeholder="Location">\
										<input type="hidden" name="location_id" id="invite_location_id" value="" autocomplete="off" class="">\
										<div class="sutoSugSearch" id="autoSugSearch" style="color:#59606B !important;">\
										</div>\
									</td>\
								</tr>\
								<tr>\
									<td colspan="2"><textarea name="message" id="message" style="width:305px"  class=""  placeholder="Add your message"></textarea></td>\
								</tr>\
							</table>\
							</form>\
						 </div>\
						 <div style="padding:20px 0px;" id="button_wrapper_invite" > \
						</div> \
						</div>');
        //
        NewDialog.dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'dialog_style_blue_grey noTitleStuff',
            title: '',
            modal: true,
            width: 380,
            height: 350,
            show: '',
            hide: ''
        });
        $elem = $('<button type="button" class="btn-custom-darkblue "  style="margin:0px 2px !important;width:100px" >Invite</button>');
        $elem.on('click', function(e) {
            var NewDialog = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">Please wait...<br><img style="width: 40px;" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
            NewDialog.dialog({
                autoOpen: false,
                closeOnEscape: false,
                resizable: false,
                dialogClass: 'dialog_style_blue_grey noTitleStuff',
                title: 'Please wait...',
                modal: true,
                position: 'center',
                width: 200,
                height: 120
            });
            NewDialog.dialog('open');
            data = $('#inviteFormDialog').serialize();
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: 'ajax_post/inviteThisFriend',
                data: data,
                success: function(response) {
                    if (response.status == 'Ok') {
                        $('#inviteDialog').dialog('destroy').remove();
                        $('#inviteDialog').dialog("close");
                        $('#waitPopup').dialog('destroy').remove();
                        var NewDialog = $('<div id="succesPopup" align="center">\
								<div align="center" style="margin-top:5px;font-size:12px;">' + response.message + '</div>\
								<div style="padding:20px 0px;" id="button_wrapper_alert" > \
								</div> \
						</div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'dialog_style_blue_grey noTitleStuff',
                            title: '',
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        $elem = $('<button type="button" class="btn-custom-white2"  style="margin:0px 2px !important;width:100px" >Ok</button>');
                        $elem.on('click', function(e) {
                            $('#waitPopup').dialog("close");
                            setTimeout(function() {
                                $('#succesPopup').dialog('destroy').remove();
                            }, 500);
                            $('#i_name').val("");
                            $('#i_email').val("");
                            $('#location').val("");

                        });
                        $("#button_wrapper_alert").append($elem);
                        NewDialog.dialog('open');

                    }
                    else {
                        $('#waitPopup').dialog("close");
                        $('#waitPopup').dialog('destroy').remove();
                        var NewDialog = $('<div id="succesPopup" align="center">\
								<div align="center" style="margin-top:5px;font-size:12px;">' + response.message + '</div>\
								<div style="padding:20px 0px;" id="button_wrapper_alert" > \
								</div> \
						</div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'dialog_style_blue_grey noTitleStuff',
                            title: 'Please wait...',
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        $elem = $('<button type="button" class="btn-custom-white2"  style="margin:0px 2px !important;width:100px" >Ok</button>');
                        $elem.on('click', function(e) {
                            $('#succesPopup').dialog("close");
                            setTimeout(function() {
                                $('#succesPopup').dialog('destroy').remove();
                            }, 500);

                        });
                        $("#button_wrapper_alert").append($elem);
                        NewDialog.dialog('open');

                    }
                }
            });
        });
        $("#button_wrapper_invite").append($elem);
        $elem = $('<button type="button" class="btn-custom-white2"  style="margin:0px 2px !important;width:100px" >Cancel</button>');
        $elem.on('click', function(e) {
            $('#inviteDialog').dialog("close");
            setTimeout(function() {
                $('#inviteDialog').dialog('destroy').remove();
            }, 500);

        });
        $("#button_wrapper_invite").append($elem);
        NewDialog.dialog('open');


        $('#invite_location').keyup(function() {
            $('#autoSugSearch').show();
            var _key = $(this).val();
            if (_key != '')
            {
                $.ajax({
                    type: "GET",
                    url: 'ajax/loadLocation/' + _key,
                    cache: false,
                    success: function(html) {
                        if (html == 0)
                        {
                            $('#autoSugSearch').html("No match found");
                            $('#invite_location').val('');
                        }
                        else
                        {
                            var obj = eval("(" + html + ")");
                            var _length = obj.searchResult.length;
                            var list = "<ul>";
                            var name_array = "";
                            for (var i = 0; i < _length; i++)
                            {
                                list += "<li><a href='javascript:void(0);' title='" + obj.searchResult[i].id + "'  style='color:#59606B !important;'>" + obj.searchResult[i].name + "</a></li>";
                            }
                            list += "</ul>";
                            $('#autoSugSearch').html(list);
                        }
                        $('#autoSugSearch ul li a').click(function() {
                            var id = $(this).attr('title');
                            $('#invite_location').val($(this).text());
                            $('#invite_location_id').val(id);
                        });

                        /*$(this).focusout(function(e) {
                         consloe.log('s');  
                         });*/

                    }
                });
            }
            else
            {
                $('.sutoSugSearch').hide();
            }
        });
    });

});

$(window).resize(function() {
    $("#tourDialog_1").dialog({
        position: {
            my: 'left-100 top+20',
            at: 'center bottom',
            of: $('#Invite_Facebook')
        }
    });
    $("#tourDialog_2").dialog({
        position: {
            my: 'right top',
            at: 'left-30 top+30',
            of: $('#tour_2_location')
        }
    });

    $("#tourDialog_3").dialog({
        position: {
            my: 'right top+3',
            at: 'left+60 bottom',
            of: $('#tour_3_location')
        }
    });
    $("#startTourDialog").dialog({
        position: {
            my: 'center center',
            at: 'center center',
            of: $(document)
        }
    });
});

function addToList(item, thisFriendStatus) {
    loc = "";
    item.loc = loc;
    if (hasOwnProperty(item, 'location')) {
        temp = item.location;
        loc = temp.name;
        item.loc = loc;
    }
    var icon = '';
    var href = '';
    if (thisFriendStatus == -1) {
        icon = '<image style="float:right;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/fb-icon.png" \>';
        href = '<a href="javascript:void(0);" >';

    }
    else if (thisFriendStatus == 0) {
        icon = '<image id="fb_plus_' + item.id + '" style="float:right;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
        href = '<a id="fb_link_' + item.id + '"  href="javascript:void(0);" >';
    }
    else {
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus + '" target="" >';
    }
    $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
										<div class=""> \
											' + href + ' \
												<div class="" > \
													<div style="width:70;height:70px; border:0px solid #333; overflow:hidden;" > \
														<image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="https://graph.facebook.com/' + item.id + '/picture?type=large&return_ssl_results=1" alt="' + item.name + '"  \> \
													</div> \
													' + icon + ' \
													<p><b id="search_name_' + item.id + '" >' + item.name + '</b><br /><span style="">' + loc + '</span></p> \
												</div> \
											</a> \
										</div> \
									</div>');
    $elem.val(item.id);
    $elem.on("click", function(ev) {
        var elemID = $elem.val();
        if ($(this).hasClass('friendThumb2')) {
            $(this).addClass('friendThumbSelected').removeClass('friendThumb2');
        }
        else {
            $(this).addClass('friendThumb2').removeClass('friendThumbSelected');
        }


    });

    /*
     if(thisFriendStatus<=0){
     $elem.on( "click", function(ev) {
     //ev.preventDefault ();
     //ev.stopPropagation();
     var data="fbID="+item.id;
     var buttonTitle="";							
     var name=($('#search_name_'+item.id).html());								
     var desc="";
     var title="";
     $.ajax({
     type: "POST",				
     dataType:'json',
     url: 'ajax_post/checkThisPersonStatus',
     data:data,
     success: function(response){
     if(response.status==-1){
     title=GLOMP_FB_POPUP_TITLE_INVITE;
     desc='Invite '+name+' to join glomp! network';
     var btns =[{text: 'Invite', 
     "class": 'btn-custom-darkblue',
     click: function() {
     $(this).dialog("close");
     setTimeout(function() {
     $('#FriendPopupDialog').dialog('destroy').remove();
     inviteThisFriend(item.id);
     }, 500 );														
     }},
     {text: "Glomp!", 
     "class": 'btn-custom-green2',
     click: function() {
     doGlomp(item.id);
     }},
     {text: "Cancel", 
     "class": 'btn-custom-white2',
     click: function() {
     $(this).dialog("close");
     setTimeout(function() {
     $('#FriendPopupDialog').dialog('destroy').remove();
     }, 500 );						
     }}
     ];
     }									
     else if(response.status==0){
     title=GLOMP_FB_POPUP_TITLE_ADD;
     desc='Add  '+name+' to your glomp! Network.';
     var btns =[{text: 'Add', 
     "class": 'btn-custom-darkblue',
     click: function() {														
     $(this).dialog("close");														
     setTimeout(function() {															
     $('#FriendPopupDialog').dialog('destroy').remove();
     addThisFriend(item.id);
     }, 500 );
     }},
     {text: "Glomp!", 
     "class": 'btn-custom-green2',
     click: function() {
     doGlomp(item.id);
     }},
     {text: "Cancel", 
     "class": 'btn-custom-white2',
     click: function() {
     $(this).dialog("close");
     setTimeout(function() {
     $('#FriendPopupDialog').dialog('destroy').remove();
     }, 500 );						
     }}
     ];
     }
     else if(response.status>0){
     desc='Glomp!  '+name+'.';
     title='';
     var btns =[
     {text: "Glomp!", 
     "class": 'btn-custom-green2',
     click: function() {
     doGlomp(item.id);
     }},
     {text: "Cancel", 
     "class": 'btn-custom-white2',
     click: function() {
     $(this).dialog("close");
     setTimeout(function() {
     $('#FriendPopupDialog').dialog('destroy').remove();
     }, 500 );						
     }}
     ];
     }
     //<image style="float:right;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="'+GLOMP_PUBLIC_IMAGE_LOC+'/icons/fb-icon.png" \> 
     var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
     <div class="friendThumb3" > \
     <div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
     <image id="fb_friend_popup_img" style="float:left;z-index:1;" src="https://graph.facebook.com/'+item.id+'/picture?type=large&return_ssl_results=1" alt="alt_pic"  \> \
     </div> \
     </div>\
     <div class="friendThumb3_name" > \
     <b>'+item.name+'</b><br /><span style="">'+item.loc+'</span>\
     </div>\
     <div class="friendThumb3_desc"  >'+desc+' \
     </div> \
     </div>');							
     NewDialog.dialog({
     dialogClass: 'noTitleStuff',
     dialogClass:'dialog_style_blue_grey',
     title:title,
     autoOpen: false,
     resizable: false,
     modal: true,
     width:300,
     height:250,				
     show: 'clip',
     hide: 'clip'										
     });
     
     
     NewDialog.dialog('option', 'buttons', btns);
     NewDialog.dialog('open');
     var img = document.getElementById('fb_friend_popup_img');
     //or however you get a handle to the IMG
     var width = parseInt(img.clientWidth);
     var height = parseInt(img.clientHeight);
     if(width>height){
     $('#fb_friend_popup_img').css({'height': 120}); 					
     }
     else{					
     $('#fb_friend_popup_img').css({'width': 120});
     }									
     }
     });
     });
     }	*/
    //$('#fb_frieds_row_'+row).append($elem);				
    $('#friendsList').append($elem);

    var img = document.getElementById('img_fb_' + item.id);
    //or however you get a handle to the IMG
    var width = parseInt(img.clientWidth);
    var height = parseInt(img.clientHeight);
    if (width > height) {
        $('#img_fb_' + item.id).css({'height': 100});
    }
    else {
        $('#img_fb_' + item.id).css({'width': 100});
    }

}
function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
            (!(prop in proto) || proto[prop] !== obj[prop]);
}

function afterCreateFbFriendsList() {
    $elem = $('<div id="search_not_found"  style="display:none; clear:both;" align="center"> \
			No results found... \
			</div>');
    $('#friendsList').append($elem);
    $(".searchUser").nanoScroller();
}
function sortByName(a, b) {
    var x = a.name.toLowerCase();
    var y = b.name.toLowerCase();
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}
var friendsList = [];
var _friend_data = null;
function createFbFriendsList() {
    $('#friendsList').html('<div align="center" style="margin-top:50px;">' + GLOMP_GETTING_FB_FRIENDS + '<span style="padding:9px 15px;background-image:url(\'assets/frontend/css/images/ajax-loader.gif\');repeat:no-reapeat;">&nbsp</span></div>');
    if (_friend_data == null) {
        FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
            _friend_data = response.data.sort(sortByName);
            //console.log(_friend_data);
            //FB.api('me/friends', function(response) {					
            var output = '';
            //res=response.data;			
            res = _friend_data;


            count = res.length;
            var ctr = 0;
            idList = new Array();
            $.each(res, function(i, item) {
                idList.push(item.id);
            });
            //console.log(idList);
            //do a query to cross check your friends list
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: 'ajax_post/checkThisFriendListStatus',
                data: 'idList=' + idList,
                success: function(response) {
                    friendsStatus = response;
                    $('#friendsList').html('');
                    $('#friendsList').addClass('content2');
                    //(response.fbid_24102928);
                    var limit = 0;
                    $.each(res, function(i, item) {
                        if (limit < 3000) {
                            limit++;
                            setTimeout(function() {
                                tempID = 'fbid_' + item.id;
                                friendStat = friendsStatus[tempID];
                                addToList(item, friendStat, function(status) {
                                    if (status == 'OK') {
                                        //do stuff...
                                    }
                                });
                                if (ctr == 100) {
                                    $(".searchUser").nanoScroller();
                                    ctr = 0;
                                }
                                else {
                                    ctr++;
                                }
                                if (!--count)
                                    afterCreateFbFriendsList();
                            }, i * 5);
                        }
                    });
                }
            });
            //do a query to cross check your friends list
            //console.log(res);			
        });
    }
    else {
        var res = _friend_data;

        count = res.length;
        var ctr = 0;
        idList = new Array();
        $.each(res, function(i, item) {
            idList.push(item.id);
        });
        //console.log(idList);
        //do a query to cross check your friends list
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: 'ajax_post/checkThisFriendListStatus',
            data: 'idList=' + idList,
            success: function(response) {
                $('#friendsList').html('');
                $('#friendsList').addClass('content2');
                friendsStatus = response;
                //(response.fbid_24102928);
                var limit = 0;
                $.each(res, function(i, item) {
                    if (limit < 3000) {
                        limit++;
                        setTimeout(function() {
                            tempID = 'fbid_' + item.id;
                            friendStat = friendsStatus[tempID];
                            addToList(item, friendStat, function(status) {
                                if (status == 'OK') {
                                    //do stuff...
                                }
                            });
                            if (ctr == 100) {
                                $(".searchUser").nanoScroller();
                                ctr = 0;
                            }
                            else {
                                ctr++;
                            }
                            if (!--count)
                                afterCreateFbFriendsList();
                        }, i * 5);
                    }
                });
            }
        });
    }

}
function closeDialog(dialog) {
    $(dialog).dialog("close");
    setTimeout(function() {
        $(dialog).dialog('destroy').remove();
    }, 500);
}
var promoCodePopupLoop = 0;
function promoCodePopup(data)
{
    var applyOkayButton = false;
    var NewDialog2 = null;
    var _H = 150;
    switch (data.status)
    {
        case 'Invalid':
            if (promoCodePopupLoop == 0)
            {
                var NewDialog = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + PROMO_CODE_DESC_INVALID + '</div> \
				<div style="text-align:center;"><input type="text" name="promo_code" id="promo_code" style="margin:15px 0px 0px 0px;text-align:center" placeholder="Promo Code" /></div> \
				<div style="padding:20px 0px;width:100%;" class="fl" id="tour_button_wrapper" ></div> \
				<div class="cl"></div> \
				</div> \
				</div>');
            }
            else
            {
                var NewDialog = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + PROMO_CODE_DESC_INVALID_2 + '</div> \
				<div style="text-align:center;"><input type="text" name="promo_code" id="promo_code" style="margin:15px 0px 0px 0px;text-align:center" placeholder="Promo Code" /></div> \
				<div style="padding:20px 0px;width:100%;" class="fl" id="tour_button_wrapper" ></div> \
				<div class="cl"></div> \
				</div> \
				</div>');
            }
            //dialogClass:'dialog_style_tour',
            NewDialog.dialog({
                position: {
                    my: 'center center',
                    at: 'center center',
                    of: $(document)
                },
                dialogClass: 'noButtonAndTitleStuff',
                autoOpen: false,
                closeOnEscape: false,
                resizable: false,
                modal: true,
                width: 400,
                height: 240,
                show: '',
                hide: ''
            });
            $elem = $('<button type="button" class="btn-custom-darkblue w150px fl"  id="" >SUBMIT</button>');
            $elem.on('click', function(e) {
                var NewDialog = $('<div id="waitPopup" align="center">\
										<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'noTitleStuff',
                    title: 'Please wait...',
                    modal: true,
                    position: 'center',
                    width: 200,
                    height: 120
                });
                NewDialog.dialog('open');
                data = '&promo_code=' + escape($('#promo_code').val());
                $('#promoCodeDialog').dialog("close");
                $('#promoCodeDialog').dialog('destroy').remove();
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: 'ajax_post/promo_code_save_to_user',
                    data: data,
                    success: function(response) {
                        $('#waitPopup').dialog("close");
                        $('#waitPopup').dialog('destroy').remove();
                        //if(response.status=='Error' || response.status=='Ok')
                        {
                            promoCodePopupLoop++;
                            GLOMP_PC_DATA = response.user_promo_code;
                            GLOMP_PC_DATA_PROD = response.res_info_prod;
                            GLOMP_PROMO_RULE_DESC = response.rule_desc;
                            promoCodePopup(GLOMP_PC_DATA);


                        }
                    }
                });



            });
            $("#tour_button_wrapper").append($elem);
            $skip = $('<button type="button" class="btn-custom-white w150px fr"  id="" >Cancel</button>');
            $skip.on('click', function(e) {
                $('#promoCodeDialog').dialog("close");
                $('#promoCodeDialog').dialog('destroy').remove();
                var NewDialog = $('<div id="waitPopup" align="center">\
									<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'noTitleStuff',
                    title: 'Please wait...',
                    modal: true,
                    position: 'center',
                    width: 200,
                    height: 120
                });
                NewDialog.dialog('open');
                data = '';
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: 'ajax_post/clear_promo_from_user',
                    data: data,
                    success: function(response) {
                        $('#waitPopup').dialog("close");
                        $('#waitPopup').dialog('destroy').remove();
                        welcompePopup(GLOMP_START_TOUR);
                    }
                });
            });
            $("#tour_button_wrapper").append($skip);
            NewDialog.dialog('open');


            break;
        case 'DoneAlready':
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + PROMO_CODE_DESC_ENDED_ALREADY + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            applyOkayButton = true;
            break;
        case 'ForMemberOnly':
            var temp = PROMO_CODE_DESC_FOR_MEMBER_ONLY.replace("[rule_criteria]", GLOMP_PROMO_RULE_DESC);
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + temp + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            applyOkayButton = true;
            break;
        case 'DidNoTPassedTheRule':
            var temp = PROMO_CODE_DESC_FOR_MEMBER_ONLY.replace("[rule_criteria]", GLOMP_PROMO_RULE_DESC);
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + temp + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            applyOkayButton = true;
            break;
        case 'NoVoucherLeft':
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + PROMO_CODE_DESC_NO_VOUCHER_LEFT + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            applyOkayButton = true;
            break;
        case 'SuccessPrimary':
            //console.log(GLOMP_PC_DATA_PROD['product']);
            var Success_msg = PROMO_CODE_DESC_SUCCESS_PRIMARY;
            if (GLOMP_PC_DATA_PROD['product'].voucher_type == 'Assignable') {
                Success_msg = PROMO_CODE_DESC_SUCCESS_ASSIGNABLE;
                _H = 190;
            }
            tempProd = GLOMP_PC_DATA_PROD['product'];
            temp = Success_msg.replace("[brand_name]", tempProd.merchant_name);
            temp = temp.replace("[product_name]", tempProd.prod_name);
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + temp + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            applyOkayButton = true;
            break;
        case 'SuccessFollowUp':
            tempProd = GLOMP_PC_DATA_PROD['product'];
            temp = PROMO_CODE_DESC_SUCCESS_FOLLOW_UP.replace("[brand_name]", tempProd.merchant_name);
            temp = temp.replace("[product_name]", tempProd.prod_name);
            NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">\
				<div style="text-align:left;">' + temp + '</div> \
				<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
				</div> \
				</div>');
            _H = 210;
            applyOkayButton = true;
            break;
    }
    if (applyOkayButton)
    {
        NewDialog2.dialog({
            position: {
                my: 'center center',
                at: 'center center',
                of: $(document)
            },
            dialogClass: 'noButtonAndTitleStuff',
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            modal: true,
            width: 400,
            height: _H,
            show: '',
            hide: ''
        });
        $okay = $('<button type="button" class="btn-custom-white w150px "  id="" >Ok</button>');
        $okay.on('click', function(e) {
            $('#promoCodeDialog').dialog("close");
            $('#promoCodeDialog').dialog('destroy').remove();
            var NewDialog = $('<div id="waitPopup" align="center">\
								<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
            NewDialog.dialog({
                autoOpen: false,
                closeOnEscape: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: 'Please wait...',
                modal: true,
                position: 'center',
                width: 200,
                height: 120
            });
            NewDialog.dialog('open');
            data = '';
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: 'ajax_post/clear_promo_from_user',
                data: data,
                success: function(response) {
                    $('#waitPopup').dialog("close");
                    $('#waitPopup').dialog('destroy').remove();
                    if (GLOMP_PC_DATA_PROD['product'].voucher_type == 'Assignable') {
                        return window.location = GLOMP_SITE_URL + 'glomp';
                    }
                    welcompePopup(GLOMP_START_TOUR);
                }
            });
        });
        $("#tour_button_wrapper").append($okay);
        NewDialog2.dialog('open');

    }

}

function welcompePopup(open) {
    if (open == 'Y' && (inTour == '1' || inTour == '2' || inTour == '3')) {
        startTour(inTour);
    }
    else if (open == 'Y') {
        var NewDialog = $('<div id="startTourDialog" class="tourDialog" align="center"> \
				<div id="tourDialog_1_content" class="tourDialog_1_content"><div align="center" style="font-size:18px;padding:10px 0px 20px 0px;">Welcome to your <b>glomp!</b> page</div> \
				<div style="text-align:justify;">' + GLOMP_TOUR_MAIN_DESC + '</div> \
				<div style="padding:20px 0px;" id="tour_button_wrapper" > \
				</div> \
				</div> \
				</div>');
        //dialogClass:'dialog_style_tour',
        NewDialog.dialog({
            position: {
                my: 'center center',
                at: 'center center',
                of: $(document)
            },
            dialogClass: 'noButtonAndTitleStuff',
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            modal: true,
            width: 360,
            height: 225,
            show: '',
            hide: '',
        });
        $elem = $('<button type="button" class="btn-custom-darkblue btn-block"  id="" >' + GLOMP_TOUR_START + '</button>');
        $elem.on('click', function(e) {
            $('#startTourDialog').dialog("close");
            startTour(1);
            setTimeout(function() {
                $('#startTourDialog').dialog('destroy').remove();
            }, 500);

        });
        $("#tour_button_wrapper").append($elem);
        $skip = $('<div class="tour_skip" align="center" >Skip</div> ');
        $skip.on('click', function(e) {
            data = '&tour_stat=N'
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: 'ajax_post/setTourStat',
                data: data,
                success: function(response) {
                    closeDialog('#startTourDialog');
                }
            });
        });
        $("#tour_button_wrapper").append($skip);
        NewDialog.dialog('open');
    }
}
function startTour(step) {
    if (step == 1) {
        var NewDialog = $('<div id="tourDialog_1" class="tourDialog" align="left"> \
				<div class="tour_arrow_up_outer" style=""><div class="tour_arrow_up_inner" style=""></div></div> \
					<div id="tourDialog_1_content" class="tourDialog_1_content">' + GLOMP_TOUR_DESC_1 + '\
					<div style="padding:20px 0px;" id="tour_button_wrapper_1" > \
					</div> \
				</div>');
        //dialogClass:'dialog_style_tour',
        NewDialog.dialog({
            position: {
                my: 'left-100 top+20',
                at: 'center bottom',
                of: $('#Invite_Facebook')
            },
            dialogClass: 'noButtonAndTitleStuff overflowVisible',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: false,
            width: 300,
            height: 175,
            show: '',
            hide: '',
        });
        $elem = $('<button type="button" class="btn-custom-blue btn-block"  id="" >' + GLOMP_INVITE_FB_FRIENDS + '</button>');
        $elem.on('click', function(e) {
            window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getFb=true&inTour=yes';

        });
        $("#tour_button_wrapper_1").append($elem);
        $skip = $('<div class="tour_skip" align="center" >Skip</div> ');
        $skip.on('click', function(e) {
            $('#tourDialog_1').dialog("close");
            startTour(2);
            setTimeout(function() {
                $('#tourDialog_1').dialog('destroy').remove();
            }, 500);
        });
        $("#tour_button_wrapper_1").append($skip);
        NewDialog.dialog('open');
    }
    else if (step == 2) {
        var NewDialog = $('<div id="tourDialog_2" class="tourDialog" align="left"> \
				<div class="tour_arrow_right_outer" style=""><div class="tour_arrow_right_inner" style=""></div></div> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">' + GLOMP_TOUR_DESC_2 + '\
				<div style="padding:20px 0px;" id="tour_button_wrapper_2" > \
				</div> \
				</div> \
				</div>');
        //dialogClass:'dialog_style_tour',
        NewDialog.dialog({
            position: {
                my: 'right top',
                at: 'left-30 top+30',
                of: $('#tour_2_location')
            },
            dialogClass: 'noButtonAndTitleStuff overflowVisible',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: false,
            width: 320,
            height: 200,
            show: '',
            hide: '',
        });
        $elem = $('<button type="button" class="btn-custom-darkblue btn-block"  id="" >' + GLOMP_TOUR_GET_STARTED + '</button>');
        $elem.on('click', function(e) {
            window.location.href = GLOMP_BASE_URL + 'profile/?tab=tbMerchant&inTour=yes';
        });
        $("#tour_button_wrapper_2").append($elem);
        $skip = $('<div class="tour_skip" align="center" >Skip</div> ');
        $skip.on('click', function(e) {
            $('#tourDialog_2').dialog("close");
            startTour(3);
            setTimeout(function() {
                $('#tourDialog_2').dialog('destroy').remove();
            }, 500);
        });
        $("#tour_button_wrapper_2").append($skip);
        NewDialog.dialog('open');
    }
    else if (step == 3) {
        var NewDialog = $('<div id="tourDialog_3" class="tourDialog" align="left"> \
				<div class="tour_arrow_topright_outer" style=""><div class="tour_arrow_topright_inner" style=""></div></div> \
				<div id="tourDialog_1_content" class="tourDialog_1_content">' + GLOMP_TOUR_DESC_3 + '\
				<div style="padding:20px 0px;" id="tour_button_wrapper_3" > \
				</div> \
				</div> \
				</div>');
        //dialogClass:'dialog_style_tour',
        NewDialog.dialog({
            position: {
                my: 'right top+3',
                at: 'left+60 bottom',
                of: $('#tour_3_location')
            },
            dialogClass: 'noButtonAndTitleStuff overflowVisible',
            autoOpen: false,
            resizable: false,
            modal: false,
            closeOnEscape: false,
            width: 280,
            height: 160,
            show: '',
            hide: '',
        });
        $elem = $('<button type="button" class="btn-custom-white2 btn-block"  id="" >Ok</button>');
        $elem.on('click', function(e) {
            $('#tourDialog_3').dialog("close");
            var data = '&tour_stat=N';
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: 'ajax_post/setTourStat',
                data: data,
                success: function(response) {
                    setTimeout(function() {
                        $('#tourDialog_3').dialog('destroy').remove();
                        if (is_connected_to_fb != "") {
                            window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getFb=true';
                        }
                    }, 500);
                }
            });

        });
        $("#tour_button_wrapper_3").append($elem);
        NewDialog.dialog('open');

    }
}
var hasHidden = false;
function searchFriends() {
    var searchThis = $.trim($("#searchTextField").val());
    if (searchThis != "") {
        var found = 0;
        $('.friendSearcheable').each(function(i, item) {
            var thisID = ($(this).val()).toLowerCase();
            var name = ($('#search_name_' + thisID).html()).toLowerCase();

            if (name.indexOf(searchThis) >= 0) {
                $('#search_wrapper_' + thisID).show();
                found++;
            }
            else {
                $('#search_wrapper_' + thisID).hide();
                hasHidden = true;
            }
        });
        if (found == 0) {
            $('#search_not_found').show();
        }
        else {
            $('#search_not_found').hide();
        }

        $(".searchUser").nanoScroller();
    }
    else if (hasHidden) {
        $('.friendSearcheable').show();
        $(".searchUser").nanoScroller();
    }
}
var cc = 0;
var t;
var timer_is_on = 0;

function timedCount(type)
{
    cc = cc + 1;
    if (cc == 2) {
        searchFriends();
    }
    else
        t = setTimeout("timedCount()", 500);
}
function doTimer() {
    cc = 0;
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}
function stopCount() {
    cc = 0;
    clearTimeout(t);
    timer_is_on = 0;
}
function getKey() {
    stopCount();
    doTimer();
}
function check_if_reactivated(is_reactivated)
{
    if(is_reactivated=='true')
    {
        var NewDialog =$('  <div id="reactivated_popup">\
                                <div align="left" style="line-height: 22px;">\
                                    Welcome back to glomp!. We\'re very glad that you have decided to reactivate your account. You\'re ready now to glomp! away!\
                                </div><br>\
                                <div align="center">\
                                    <button type="button" class="btn-custom-darkblue " id="reactivated_closed">Close</button>\
                                </div>\
                            </div>');
            NewDialog.dialog({						
                autoOpen: false,
                closeOnEscape: false ,
                resizable: false,					
                dialogClass:' noTitleStuff dialog_style_glomp_wait',
                modal: true,
                title:$('#Please_wait').text(),
                position: 'center',
                width:400,
                open: function() {
                    $('#reactivated_closed').click(function(){
                        NewDialog.dialog('close');
                        NotClaimedVoucher(NOT_CLAIM_MESSAGE);
                    });
                }
            });

            NewDialog.dialog('open');
            return;
        
    }
    NotClaimedVoucher(NOT_CLAIM_MESSAGE);
}
function NotClaimedVoucher(message) {
    var showTourOrPromo = function (message) {
        if (message == '') {
            if (GLOMP_PC_RESPONSE == 'Y')
            {
                promoCodePopup(GLOMP_PC_DATA);
            }
            else
            {
                welcompePopup(GLOMP_START_TOUR);
            }
            return;
        }

        var NewDialog = $('<div class="tourDialog" align="left" style="min-height: 10px !important;font-size:12px!important"> \
                    <div align="center">\
                        <div>' + message + '</div> \
                        <br /> \
                        <button id = "not_claimed_btn" class= "btn-custom-white2">Ok</button> \
                    </div>');

        NewDialog.dialog({
            dialogClass: 'noTitleStuff',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 320,
        });

        NewDialog.dialog('open');

        $('#not_claimed_btn').click(function() {
            NewDialog.dialog('close');
            if (GLOMP_PC_RESPONSE == 'Y')
            {
                promoCodePopup(GLOMP_PC_DATA);
            }
            else
            {
                welcompePopup(GLOMP_START_TOUR);
            }
        });
    };

    $.ajax({
        type: "POST",
        dataType: 'json',
        async: false,
        url: GLOMP_SITE_URL + 'user2/welcome_incomplete_profile',
        success: function(r) {            
            if(! r.bypass && r.incomplete) {
                
                var NewDialog = $('<div class="tourDialog" align="left" style="min-height: 10px !important;font-size:12px!important"> \
                <div align="justify">\
                    <div>' + r.msg.body + '</div> \
                    <br /> \
                </div> \
                <div align="left">\
                    <button id= "incomplete_btn_ok" class="btn-custom-white2">'+ r.msg.ok +'</button>\
                    <button id= "close_me" class="btn-custom-blue">'+ r.msg.later +'</button>\
                </div>\\');

                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 350,
                    open: function() {
                        $('#incomplete_btn_ok').click(function(){
                            return window.location = GLOMP_SITE_URL + 'user/update';
                        });
                        
                        $('#close_me').click(function(){
                            return NewDialog.dialog('close');
                        });
                    }
                });

                NewDialog.dialog('open');
                return;
            } else {
                return showTourOrPromo(message);
            }
        }
    });
}

function getFbFriendsList() {
    var NewDialog = $('<div id="listDialog" align="left" style="!important;font-size:12px!important"> \
		<div id="" style="font-weight:bold;padding:px;font-size:16px;" >Select friends to invite</div>\
			<div class="searchFriends" style="border:0px solid #fff">\
				<input type="text" placeholder="Search or Find Friend" name="keywords" value="" id="searchTextField">\
				<div class="searchUser nano searchForFriendPanel">\
					<div class="content" id="friendsList" style="color:#000">\
					</div>\
				</div>\
			</div>\
		</div>');
    NewDialog.dialog({
        dialogClass: 'dialog_style_glomped_alerts',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 650,
        height: 570,
        buttons: [
            {text: "Invite",
                "class": 'btn-custom-blue',
                click: function() {
                    // check if some friends are selected
                    var j = 0;
                    var selectedFriends = [];
                    $('.friendThumbSelected').each(function(i, item) {
                        var friendID = $(this).val();
                        selectedFriends.push(friendID);
                        j++;
                    });
                    if (j > 0) {
                        doInviteFbFriends(selectedFriends);
                        return false;
                    }

                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#listDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
    $(".searchUser").nanoScroller();
    $('#searchTextField').keyup(function() {
        getKey();
    });
    checkLoginStatus();

}
function doInviteFbFriends(selectedFriends) {
    console.log(selectedFriends);
    var names = "";
    var selectedCtr = 0;
    var firstID = "";
    var tags = "";
    $.each(selectedFriends, function(i, item) {
        selectedCtr++;

        tags += ' @[' + item + ']\n';
        var friendName = $("#search_name_" + item).html();
        if (firstID == "")
            firstID = item;
        if (selectedCtr <= 5) {
            if (names != "")
                names += ', ';
            names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
        }

    });
    if (selectedCtr > 5) {
        names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';
    }


    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
			<div id="personalMessageTips" style="padding:2px;font-size:11px;"></div> \
			<table border=0> \
				<tr> \
					<td><b>To</b>:' + names + '</td> \
				</tr> \
				<tr> \
					<td><hr size="1" style="padding:0px;margin:0px;">\</td> \
				</tr> \
				<tr> \
					<td ><textarea id="personalMessage"  style="font-size:12px;resize:none;width:360px;height:50px;" placeholder="Your personal Message"></textarea></td> \
				</tr> \
			</table> \
			<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 400,
        height: 210,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "Invite",
                "class": 'btn-custom-blue',
                click: function() {
                    var bValid = true;
                    $("#personalMessage").removeClass("ui-state-error");
                    bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 50, '#personalMessageTips');
                    if (bValid) {
                        var NewDialog = $('<div id="confirmPopup" align="center">\
												<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'noTitleStuff',
                            title: 'Please wait...',
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        NewDialog.dialog('open');

                        personalMessage = $("#personalMessage").val();
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                        }, 500);

                        var profile = "http://velocitydev.dyndns-server.com:8040/Glomp/v7-manila/welcome.html";
                        FB.api(
                                'me/glomp_app:invite',
                                'post',
                                {
                                    profile: profile,
                                    message: '' + tags + ' \r\n \n' + personalMessage
                                },
                        function(response) {
                            $('#confirmPopup').dialog('destroy').remove();
                            if (hasOwnProperty(response, 'id')) {
                                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
											<div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
											<p></div>');
                                NewDialog.dialog({
                                    dialogClass: 'noTitleStuff',
                                    autoOpen: false,
                                    resizable: false,
                                    modal: true,
                                    width: 220,
                                    height: 120,
                                    show: 'clip',
                                    hide: 'clip',
                                    buttons: [
                                        {text: "Ok",
                                            "class": 'btn-custom-white2',
                                            click: function() {
                                                $(this).dialog("close");
                                                setTimeout(function() {
                                                    $('#messageDialog').dialog('destroy').remove();
                                                }, 500);

                                                $('#listDialog').dialog("close");
                                                setTimeout(function() {
                                                    $('#listDialog').dialog('destroy').remove();
                                                }, 500);
                                            }}
                                    ]
                                });
                                NewDialog.dialog('open');
                            }
                            else {
                                $('#listDialog').dialog("close");
                                setTimeout(function() {
                                    $('#listDialog').dialog('destroy').remove();
                                }, 500);
                            }
                            // handle the response
                            console.log(response);
                            //var URL='http://facebook.com/'+response.id;
                            //window.open(URL);
                        }
                        );
                    }

                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');

}
var personalMessage = "";
function getPersonalMessage() {
    /**/
    var NewDialog = $('<div id="messageDialog" align="center"> \
		<div id="personalMessageTips" style="padding:4px;font-size:12px;"></div> \
		<table> \
			<tr> \
			<td><textarea id="personalMessage"  style="font-size:12px;resize:none;width:320px;height:80px;" placeholder="Your personal Message"></textarea></td> \
			</tr> \
		</table> \
		<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 400,
        height: 220,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "Select Facebook Friends",
                "class": 'btn-custom-blue',
                click: function() {
                    var bValid = true;
                    $("#personalMessage").removeClass("ui-state-error");
                    bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 50, '#personalMessageTips');
                    if (bValid) {
                        personalMessage = $("#personalMessage").val();
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                            checkLoginStatus();
                        }, 500);
                    }
                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
    /*
     FB.ui({
     method: 'send',
     message:friendsList,	
     to:1126112471,
     link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/',
     }, requestCallback);//
     
     //*
     //to:friendsList,
     //100006614254149*/


}
//description:  "ABCDEFGH10ABCDEFGH20ABCDEFGH30ABCDEFGH40ABCDEFGH50ABCDEFGH60ABCDEFGH70ABCDEFGH80ABCDEFGH90ABCDEFG100ABCDEFGH10ABCDEFGH20ABCDEFGH30ABCDEFGH40ABCDEFGH50ABCDEFGH60ABCDEFGH70ABCDEFGH80ABCDEFGH90ABCDEFG200ABCDEFGH10ABCDEFGH20ABCDEFGH30ABCDEFGH40ABCDEFGH50ABCDEFGH60ABCDEFGH70ABCDEFGH80ABCDEFGH90ABCDEFG300ABCDEFGH10ABCDEFGH20ABCDEFGH30ABCDEFGH40ABCDEFGH50ABCDEFGH60ABCDEFGH70ABCDEFGH80ABCDEFGH90ABCDEFG400ABCDEFGH10ABCDEFGH20ABCDEFGH30ABCDEFGH40ABCDEFGH50ABCDEFGH60ABCDEFGH70ABCDEFGH80ABCDEFGH90ABCDEFG500",
function glomp() {
    FB.api(
            'me/glomp_app:glomp',
            'post',
            {
                product: "http://samples.ogp.me/297573733621733"
            },
    function(response) {
        // handle the response
        console.log(response);
    }
    );
}
/*
 tags:'100006614254149',
 message:'You should really try this @[100006614254149] ',
 product: "http://samples.ogp.me/297573733621733"
 */
function fbSend() {
    /*	FB.ui({
     method: 'send',
     to:[690625912],
     message: 'message',         
     link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/'		 
     });
     */
    FB.ui({
        method: 'feed',
        body: 'Trying the Graph',
        tags: '100006614254149',
        place: '102152049827030',
        link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/',
        description: 'description',
        message: 'message',
    }, callback
            );
}
function callback() {

}
function checkLoginStatus_old() {
    FB.getLoginStatus(function(response) {
        if (response && response.status == 'connected') {
            createFbFriendsList();
            //sendRequestToRecipients();
        } else {
            FB.login(function(response) {
                if (response.status === 'connected') {
                    createFbFriendsList();
                    //sendRequestToRecipients();
                }
            }, {scope: 'publish_actions,email'});
            // Display the login button
        }
    }
    );

    // Login in the current user via Facebook and ask for email permission


    // Check the result of the user status and display login button if necessary


}

function sendRequestToRecipients() {
    //to: friendsList
    FB.ui({method: 'apprequests',
        message: GLOMP_MESSAGE_JOIN + personalMessage
    }, requestCallback);
}
function requestCallback(response) {
    console.log(response);
    if (response != null) {
        var invited_IDs = response.request;
        var data = response;
        data["personalMessage"] = escape(personalMessage);

        //now send to server
        $.ajax({
            type: "POST",
            url: 'ajax_post/saveInvitedFriendsFb',
            data: data,
            success: function(response) {
                console.log(response);
                var NewDialog = $('<div id="messageDialog" align="center"> \
									<p style="padding:15px 0px;">' + GLOMP_INVITE_REQUEST_SENT_BODY + '</p> \
								</div>');
                //
                NewDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    dialogClass: 'noTitleStuff',
                    title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                    modal: true,
                    width: 400,
                    height: 220,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: "Ok",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);
                            }}
                    ]
                });
                NewDialog.dialog('open');
            }
        });
        //now send to server		
    }
    //console.log(response);

    /*FB.ui({
     method: 'feed',
     to:"505463380",
     link: 'https://developers.facebook.com/docs/reference/dialogs/',
     picture: 'http://fbrell.com/f8.jpg',
     name: 'Facebook Dialogs',
     caption: 'Reference Documentation',
     description: 'Using Dialogs to interact with people.'
     },
     function(response){});*/


}
function checkLength(o, n, min, max, tipsID) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(tipsID, "Length of " + n + " must be between " + min + " and " + max + ".");
        return false;
    } else {
        return true;
    }
}
function checkRegexp(o, regexp, n, tipsID) {
    if (!(regexp.test(o.val()))) {
        o.addClass("ui-state-error");
        updateTips(tipsID, n);
        return false;
    } else {
        return true;
    }
}
function updateTips(tipsID, t) {
    $(tipsID).html(t);
    //.addClass( "ui-state-highlight" );
    setTimeout(function() {
        $(tipsID).removeClass("ui-state-highlight", 1500);
    }, 500);
}

/* User dashboard JS */

//Enable or disable invite button
// when required fields are set
function toogle_invite_button()
{
    $('#invite_friends_form').find('#name, #email').keyup(function() {
        var empty = false;

        $('#invite_friends_form').find('#name, #email').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#Invite').attr('disabled', 'disabled');
        } else {
            $('#Invite').removeAttr('disabled');
        }
    });

    $('#invite_friends_form').find('#location_id').change(function() {
        if ($(this).val == 0) {
            $('#Invite').attr('disabled', 'disabled');
        } else {
            $('#Invite').removeAttr('disabled');
        }
    });

}
$(window).resize(function() {
    $(".ui-dialog-content").dialog("option", "position", "center");
});