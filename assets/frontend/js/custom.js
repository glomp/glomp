$(document).ready(function(e) {

    $('#location').keyup(function() {
		stopCount_2();
		doTimer_2();		
    })

//		 $('body').click(function(){ $('.sutoSugSearch').hide();
//		 _key = $("#location").val();
//		 if(all_location_array.indexOf(_key.toLowerCase())==-1)
//						{
//						$("#location").val('');
//						$("#location_id").val('');
//						}
//		 });
    $('.info_tooltip').popover({html:true});
    $('.info_tooltip').mouseenter(function(){
        $(this).popover('show');
        $(this).next().addClass('popover-bg-gray');
        var arrow = $(this).next().children()[0];
        $(arrow).addClass('arrow-bg-gray');
    }).mouseleave(function(){
        $(this).popover('hide');
    });
    
    $('.info_tooltip_profile').popover({html:true});
    $('.info_tooltip_profile').mouseenter(function(){
        $(this).popover('show');
        $(this).next().addClass('popover-bg-gray2');
        var arrow = $(this).next().children()[0];
        $(arrow).addClass('arrow-bg-gray2');
    }).mouseleave(function(){
        $(this).popover('hide');
    });
    
    $('.info_tooltip_m').popover({html:true});
    var show = 0;
    $('.info_tooltip_m').click(function(e){
        e.stopPropagation();
        if (show == 0) {
            $(this).popover('show');
            $(this).next().addClass('popover-bg-gray');
            var arrow = $(this).next().children()[0];
            $(arrow).addClass('arrow-bg-gray');
            show = 1;
        } else {
            $(this).popover('hide');
            show = 0;
        }
    });
}); 

	var cc_2=0;
	var t_2;
	var timer_is_on_2=0;	
	function timedCount_2()
	{	
		cc_2=cc_2+1;
		if(cc_2==2){		
			searchLocation();
		}
		else
			t_2=setTimeout("timedCount_2()",500);
	}
	function doTimer_2(){
		cc_2=0;
		if (!timer_is_on_2){
			timer_is_on_2=1;
			timedCount_2();
		}
	}
	function stopCount_2(){
		cc_2=0;
		clearTimeout(t_2);
		timer_is_on_2=0;
	}		
	
	function 	searchLocation(){
		$('.sutoSugSearch').show();
		$('.sutoSugSearch').html("Please wait...");
        var _key = $('#location').val();		
		if(_key!=''){
			$.ajax({
				type: "GET",
				url: 'ajax/loadLocation/' + _key,
				cache: false,
				success: function(html) {
					if (html == 0)
					{
						$('.sutoSugSearch').html("No match found");
						$('#location').val('');
					}
					else
					{
						var obj = eval("(" + html + ")");
						var _length = obj.searchResult.length;
						var list = "<ul>";
						var name_array = "";
						for (var i = 0; i < _length; i++)
						{
							list += "<li><a href='javascript:void(0);' title='" + obj.searchResult[i].id + "'>" + obj.searchResult[i].name + "</a></li>";
						}
						list += "</ul>";
						$('.sutoSugSearch').html(list);
					}
					$('.sutoSugSearch ul li a').click(function() {
						var id = $(this).attr('title');
						$('#location').val($(this).text());
						$('#location_id').val(id);
						$('.sutoSugSearch').hide();
					});

					/*$(this).focusout(function(e) {
					 consloe.log('s');  
					 });*/

				}
			});
		}
		else
		{
			$('.sutoSugSearch').hide();
		}
	}