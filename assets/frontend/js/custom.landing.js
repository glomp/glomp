var firstRun = true;
var dateNow = new Date().getTime();
var fbApiInit = false;
$(document).ready(function() {
    // validate signup form on keyup and submit	
    $("#frmLogin").validate({
        rules: {
            user_name:
                    {
                        required: true,
                        email: true
                    },
            user_password: "required",
        }
        ,
        messages: {
            user_name: "",
            user_password: "",
        }
    });

});
var facebookDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
            Connecting to Facebook...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
        </div></div>');
$(function() {

    $("#video_play").click(function(event) {
        $('#video_wrapper').hide();
        //publish_actions,email
    });
    $("#Login_Facebook").click(function(event) {
        event.preventDefault();
        event.stopPropagation();                
        if(!login_click)        
        {
            login_click=true;
            facebookDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 300,
                height: 120,
                show: '',
                hide: ''
            });
            facebookDialog.dialog("open");
            if (fbApiInit)
            {
                
                FB.getLoginStatus(function(response) {
                    if (response.status == "connected") {
                        getUserDetailsAndDoPost();
                    }
                    else {
                        FB.login
                                (
                                        function(response)
                                        {
                                            if (response.status === 'connected') {
                                                getUserDetailsAndDoPost();
                                            }
                                            else
                                            {
                                                facebookDialog.dialog("close");
                                            }
                                        }, {scope: 'publish_actions,email'}
                                );
                    }
                });
            }
            else {
                //console.log("asdasdasd");
                FB.init({
                    appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
                    status: true, // check login status
                    cookie: true, // enable cookies to allow the server to access the session
                    xfbml: true  // parse XFBML	  		
                });
                fbApiInit = true;
                //console.log("fbApiInit");
                FB.Event.subscribe('auth.authResponseChange', function(response) {
                    if (response.status === 'connected') {
                        // The response object is returned with a status field that lets the app know the current
                        // login status of the person. In this case, we're handling the situation where they 
                        // have logged in to the app.			
                        console.log("connected");

                    } else if (response.status === 'not_authorized') {
                    console.log("not_authorized");
                        // In this case, the person is logged into Facebook, but not into the app, so we call
                        // FB.login() to prompt them to do so. 
                        // In real-life usage, you wouldn't want to immediately prompt someone to login 
                        // like this, for two reasons:
                        // (1) JavaScript created popup windows are blocked by most browsers unless they 
                        // result from direct interaction from people using the app (such as a mouse click)
                        // (2) it is a bad experience to be continually prompted to login upon page load.
                        FB.login(
                                function(response)
                                {
                                    if (response.status === 'connected') {
                                        getUserDetailsAndDoPost();
                                        

                                    }
                                    else
                                    {
                                        facebookDialog.dialog("close");
                                    }
                                }, {scope: 'publish_actions,email'}
                        );

                    } else {
                        console.log("else");
                        // In this case, the person is not logged into Facebook, so we call the login() 
                        // function to prompt them to do so. Note that at this stage there is no indication
                        // of whether they are logged into the app. If they aren't then they'll see the Login
                        // dialog right after they log in to Facebook. 
                        // The same caveats as above apply to the FB.login() call here.
                        //FB.login();
                        FB.login
                                (
                                        function(response)
                                        {
                                            if (response.status === 'connected') {
                                                getUserDetailsAndDoPost();
                                            }
                                            else
                                            {
                                                facebookDialog.dialog("close");
                                            }
                                        }, {scope: 'publish_actions,email'}
                                );
                    }
                    return;
                });
                
                FB.getLoginStatus(function(response) {
                    if (response.status == "connected") {
                        getUserDetailsAndDoPost();
                    }
                    else {
                        FB.login
                                (
                                        function(response)
                                        {
                                            if (response.status === 'connected') {
                                                getUserDetailsAndDoPost();
                                            }
                                            else
                                            {
                                                facebookDialog.dialog("close");
                                            }
                                        }, {scope: 'publish_actions,email'}
                                );
                    }
                });
            }
        }
        else
        {
            login_click=false; 
        }
    });
    // Link to open the dialog	
    $("#Link_Facebook").click(function(event) {
        facebookDialog.dialog({
            dialogClass: 'noTitleStuff',
            title: '',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 300,
            height: 110,
            show: '',
            hide: ''
        });
        facebookDialog.dialog("open");
        if (fbApiInit)
        {
        }
        else {
            FB.init({
                appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML	  		
            });
            fbApiInit = true;
        }
        FB.getLoginStatus(function(response) {

            if (response.status == "connected") {
                checkIfUserHasAnExistingAcct();
            }
            else {
                FB.login
                        (
                                function(response)
                                {
                                    console.log(response);
                                    if (response.status === 'connected') {
                                        checkIfUserHasAnExistingAcct();
                                    }
                                    else
                                    {
                                        facebookDialog.dialog("close");
                                    }
                                }, {scope: 'publish_actions,email'}
                        );
            }
        });

    });
    
    
    /*$("#what_is_glomp_button").click(function(event) {
        event.preventDefault();
        event.stopPropagation();        
        $('#what_is_glomp_video').show("slide", { direction: "up" }, 500, function(){
            //$('#login_wrapper').hide();
        });
    
    });
    $("#what_is_glomp_video").click(function(event) {
        event.preventDefault();
        event.stopPropagation();        
        $('#what_is_glomp_video').hide("slide", { direction: "up" }, 500, function(){
            //$('#login_wrapper').hide();
        });
    });*/
    $("#what_glomp_can_do").click(function(event) {                     
        event.preventDefault();
        event.stopPropagation();        
        $('#what_glomp_can_do_wrapper').show();         
        
        /*$('#what_glomp_can_do_wrapper').show("slide", { direction: "left" }, 500, function(){
           // $("#body_wrapper").hide();
        });*/
        
    });
    $("#what_glomp_can_do_close").click(function(event) {  
        event.preventDefault();
        event.stopPropagation();                         
        $("#what_glomp_can_do_wrapper").hide();
        /*$('#what_glomp_can_do_wrapper').hide("slide", { direction: "left" }, 500, function(){
            
        });*/
        
    });     
    $("#signup-link, .signup-link").click(function(event) {
        event.preventDefault();
        event.stopPropagation(); 
    
    
        
        $(".ui-dialog-titlebar-close").hide();
        
        $("#dialog").dialog({
            dialogClass: 'noTitleStuff',
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 550,
            height: 180,
            buttons: [
                {
                    text: "Login with Facebook",
                    "class": 'btn-custom-darkblue',
                    click: function() {                    
                        $(this).dialog("close");
                        facebookDialog.dialog({
                            dialogClass: 'noTitleStuff',
                            title: '',
                            autoOpen: false,
                            resizable: false,
                            closeOnEscape: true,
                            modal: true,
                            width: 300,
                            height: 110,
                            show: '',
                            hide: ''
                        });
                        facebookDialog.dialog("open");
                        
                        if (fbApiInit)
                        {
                        
                        }
                        else {
                            FB.init({
                                appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
                                status: true, // check login status
                                cookie: true, // enable cookies to allow the server to access the session
                                xfbml: true  // parse XFBML	  		
                            });
                            fbApiInit = true;
                        }
                        FB.getLoginStatus(function(response) {
                            if (response.status == "connected") {
                                checkIfUserHasAnExistingAcct();
                            }
                            else {
                                FB.login
                                        (
                                                function(response)
                                                {
                                                    if (response.status === 'connected') {
                                                        checkIfUserHasAnExistingAcct();
                                                    }
                                                    else
                                                    {
                                                        facebookDialog.dialog("close");
                                                    }
                                                }, {scope: 'publish_actions,email'}
                                        );
                            }
                        });
                    }
                },
                {
                    text: "Login with LinkedIn",
                    "class": 'btn-custom-darkblue',
                    click: function() {
                    try{
                        facebookDialog.dialog("destroy");            
                    }
                    catch(e)
                    {
                    
                    }
                        
                        IN.User.authorize(function() {
                            var params = {
                                type: 'linked',
                                type_string: 'LinkedIn',
                                func: function(obj) {
                                    return liCheckIfConnected(obj);
                                }
                            };
                            
                            processIntegratedApp(params);
                        });
                    }
                },
                {
                    text: "Manually register",
                    "class": 'btn-custom-white3',
                    click: function() {
                        window.location = LOCATION_REGISTER;
                    }
                },
            ]
        });
        $("#dialog-content").html("<div style='padding:15px 15px;'>" + SIGNUP_POPUP_TEXT + "</div>");
        $("#dialog").dialog("open");
    });

});

window.fbAsyncInit = function() {
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true  // parse XFBML	  		
    });
    setTimeout(function() {
        fbApiInit = true;
    }, 1000);


    FB.Event.subscribe('auth.authResponseChange', function(response) {


        if (response.status === 'connected') {
            // The response object is returned with a status field that lets the app know the current
            // login status of the person. In this case, we're handling the situation where they 
            // have logged in to the app.			
            if (!firstRun) {
                //getUserDetailsAndDoPost();			
            }
            else {
                firstRun = false;
            }
        } else if (response.status === 'not_authorized') {
            // In this case, the person is logged into Facebook, but not into the app, so we call
            // FB.login() to prompt them to do so. 
            // In real-life usage, you wouldn't want to immediately prompt someone to login 
            // like this, for two reasons:
            // (1) JavaScript created popup windows are blocked by most browsers unless they 
            // result from direct interaction from people using the app (such as a mouse click)
            // (2) it is a bad experience to be continually prompted to login upon page load.
            FB.login(function(response) {
                if (response.status === 'connected') {
                }
            }, {scope: 'publish_actions,email'});
            firstRun = false;
        } else {
            firstRun = false;
            // In this case, the person is not logged into Facebook, so we call the login() 
            // function to prompt them to do so. Note that at this stage there is no indication
            // of whether they are logged into the app. If they aren't then they'll see the Login
            // dialog right after they log in to Facebook. 
            // The same caveats as above apply to the FB.login() call here.
            //FB.login();
            FB.login(
                    function(response)
                    {
                        if (response.status === 'connected') {
                            getUserDetailsAndDoPost();

                        }
                    }, {scope: 'publish_actions,email'}
            );
        }
        //console.log(response+"H");
    });
    /*FB.getLoginStatus(function (response) {
     if(response.status=="unknown"){
     firstRun=false;
     }
     console.log(response);
     });*/

};
// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);

    //fbApiInit=true;								 
}(document));


var posted = false;
function getUserDetailsAndDoPost()
{
    doFacebookDelay('facebookLoadingDialog', 'friendsList_loading', 5000, 'popup', 'facebookConnectDialog');
    if (!posted)
    {
        //console.log(from);
        posted = true;
        FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
            var output = '';
            for (property in response) {
                output += property + ': ' + response[property] + '; ';
            }
            for (property in response.hometown) {
                output += property.hometown + ': ' + response.hometown[property] + '; ';
            }
            console.log(output);
            facebookDialog.dialog("close");
            clearFacebookDelay();

            var alias ="";
            if (typeof PUBLIC_ALIAS !== 'undefined') {
                alias="/"+PUBLIC_ALIAS;
                response.referrer = GLOMP_BASE_URL+ PUBLIC_ALIAS;
            }
            post_to_url((GLOMP_BASE_URL + 'landing'), response, 'post');
            //console.log('Good to see you, ' + response.id + '::'+response.email );
        });
    }
}
function checkIfUserHasAnExistingAcct() {
    facebookDialog.dialog("close");
    FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
        var fbID = response.id;
        $.ajax({
            type: "POST",
            url: 'ajax_post/checkIfUserHasAnExistingAcct/',
            data: 'fbID=' + fbID
        }).done(function(response) {
            facebookDialog.dialog("close");
            if (response != "") {                
                response = jQuery.parseJSON(response);
                if (response.data == '1' && response.data_status == "Active") {
                    $("#dialog").dialog({//dialogClass: 'noTitleStuff'
                        dialogClass: 'noTitleStuff',
                        resizable: false,
                        modal: true,
                        autoOpen: false,
                        width: 500,
                        height: 200,
                        buttons: [
                            {
                                text: "Login with facebook",
                                "class": 'btn-custom-darkblue',
                                click: function() {
                                    $(this).dialog("close");
                                    FB.getLoginStatus(function(response) {
                                        if (response.status == "connected") {
                                            getUserDetailsAndDoPost();
                                        }
                                        else {
                                            FB.login
                                                    (
                                                            function(response)
                                                            {
                                                                if (response.status === 'connected') {
                                                                    getUserDetailsAndDoPost();
                                                                }
                                                            }, {scope: 'publish_actions,email'}
                                                    );
                                        }
                                    });
                                }
                            },
                            {
                                text: "Cancel",
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $("#dialog").dialog("close");
                                }
                            }
                        ]
                    });
                    $("#dialog-content").html(SIGNUP_WITH_FB_BUT_REG);
                    $("#dialog").dialog("open");

                }
                else {
                    getUserDetailsAndDoPost();
                }
            }
        });
    });
}
function post_to_url(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);

        }
    }

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "fb_login");
    hiddenField.setAttribute("value", "fb_login");
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
}
function autoPlayVideo(vcode, width, height) {
    "use strict";
    $("#videoContainer").html('<iframe width="' + width + '" height="' + height + '" src="https://www.youtube.com/embed/' + vcode + '?autoplay=1&loop=1&rel=0&wmode=transparent" frameborder="0" allowfullscreen wmode="Opaque"></iframe>');
}
var GLOMP_POP_TEXT_FROM_FACEBOOK_LINK = "Welcome to <b>glomp!</b>. To accept your friend's invitation, please register with your Facebook account. If you already have a <b>glomp!</b> account, then please link your Facebook account to connect with your friend.";
function popUpFacebook() {
    var NewDialog = $('<div id="FacebookPopupDialog" align="center" style="text-align:justify;line-height:120%;padding:15px;">\
		' + GLOMP_POP_TEXT_FROM_FACEBOOK_LINK + '\
	\</div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 500,
        height: 220,
        show: '',
        hide: '',
        buttons: [
            {
                text: "Login with Facebook",
                "class": 'btn-custom-darkblue',
                click: function() {
                    $(this).dialog("close");
                    $("#Login_Facebook").click();
                }
            },
            {
                text: "Link Facebook account",
                "class": 'btn-custom-white3',
                click: function() {

                    $('#FacebookPopupDialog').dialog('destroy').remove();
                }
            }
        ]
    });
    NewDialog.dialog("open");
}
function voucherPopup(from) {
    SIGNUP_POPUP_TEXT_VOUCHER = SIGNUP_POPUP_TEXT_VOUCHER.replace("[friend_name]", "<b>" + from + "</b>");
    var NewDialog = $('<div id="voucherPopupDialog" align="center" style="text-align:left;line-height:120%;padding:15px;">\
		' + SIGNUP_POPUP_TEXT_VOUCHER + '<br><br><br><div align="right"><a href="javascript:void(0)" style="text-decoration:underline" id="what_is_glomp">What is glomp!?</a></div>\
	\</div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 450,
        closeOnEscape: false,
        height: 220,
        show: '',
        hide: '',
        buttons: [
            {
                text: "Register",
                "class": 'btn-custom-darkblue  w200px',
                click: function() {
                    $(this).dialog("close");
                    if (linkedIn) return $("#LinkedIN").click();
                    $("#Login_Facebook").click();
                }
            }
        ]
    });
    NewDialog.dialog("open");

    $('#what_is_glomp').click(function() {
        var loginBoxVideo_2 = $('#loginBoxVideo').html();
        var NewDialog = $('<div id="what_is_glompDialog" align="center" style="text-align:left;line-height:120%;padding:15px;">\
			<div class="fr" align="center" style="width:100%" >\
				<span style="margin-bottom:2px" class="what_is_glompDialog_close fr" id="what_is_glompDialog_close"></span>\
			</div>\
			<div class="loginBoxVideo" style="padding:0px 12px" id="loginBoxVideo_2">' + loginBoxVideo_2 + '</div>\
			<div id="video_wrapper_2" style="float:right;background-color:#59606B;padding:0px;height:326px;width:535px;position:absolute;margin:-330px 0px 0px 10px;border-radius:0px 5px 0px 5px">\
				<div style="float:left;border:0px solid #fff;color:#fff;padding:0px 25px;font-wieght:normal;line-height:130%;width:260px;font-size:16px" class="glompFont cl" align="left">\
					<h2 style="color:#ff0000;margin:0px;padding:0px" class="fl glompFont">' + WHAT_IS_GLOMP + '</h2>\
					<p style="text-align:justify;">\
						' + WHAT_IS_GLOMP_DETAILS + '\
					</p>\
				</div>\
				<div style="float:right;padding-right:50px;margin-top:90px;" align="center">\
					<img src="' + GLOMP_BASE_URL + 'assets/frontend/img/glomp_play.png" width="120" style="cursor:pointer" id="video_play_2" />\
					<div style="color:#B7D436;font-size:22px;font-weight:bold;margin:10px;">\
						' + WHAT_IS_GLOMP_LEARN_MORE + '\
					</div>\
				</div>\
			</div>\
		</div>');
        NewDialog.dialog({
            dialogClass: 'dialog_style_what_is_glomp noTitleStuff',
            title: 'What is glomp!?',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 565,
            closeOnEscape: false,
            height: 360,
            show: '',
            hide: ''
        });
        NewDialog.dialog("open");
        $('#what_is_glompDialog_close').click(function() {
            $('#what_is_glompDialog').dialog("close");
            $('#what_is_glompDialog').dialog('destroy').remove();
        });
        $("#video_play_2").click(function(event) {
            $('#video_wrapper_2').hide();
        });

    });
}


var timerFacebookDelay = null;
function doFacebookDelay(targetPopup, targetElement, timeout, outputType, closeThisFirst)
{
    console.log("asdasd");
    var message = 'We are experiencing a loading delay from Facebook.<br>Please wait a moment.';
    message = '<span style="font-size:14px">' + message + '</span>&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />';
    timerFacebookDelay = setTimeout(function()
    {
        if (closeThisFirst != '')
        {
            $("#" + closeThisFirst).dialog('close');
        }
        if (outputType == 'popup')
        {

            if ($('#' + targetPopup).doesExist())
            {//target popupExist
                $('#' + targetElement).html(message);
            }
            else
            {//create a new popup                    

                var NewDialog = $(' <div id="' + targetPopup + '" align="center">\
                                            <div align="center" style="margin-top:5px;" id="' + targetElement + '">\
                                            ' + message + '\
                                            </div>\
                                        </div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'noTitleStuff',
                    title: 'Please wait...',
                    modal: true,
                    position: 'center center',
                    width: 420,
                    height: 100
                });
                NewDialog.dialog('open');
            }
        }
        else
        {

            $('#' + targetElement).html(message);
        }


    }, timeout);
}

function clearFacebookDelay()
{
    clearTimeout(timerFacebookDelay);
    if ($('#facebookLoadingDialog').doesExist())
    {
        $("#facebookLoadingDialog").dialog('close');
        $("#facebookLoadingDialog").dialog('destroy').remove();
    }
}

$(window).resize(function() {
    $(".ui-dialog-content").dialog("option", "position", "center");
});


$.fn.doesExist = function() {
    return jQuery(this).length > 0;
};


/* Linked IN */
function liInitOnload() {
    if (IN.User.isAuthorized()) {
        //NO NEED TO DO ANYTHING        
    }
}

function liAuth() {
    IN.User.authorize(function() {
        //If authorized and logged in
        var params = {
            type: 'linked',
            type_string: 'LinkedIn',
            func: function(obj) {
                return login_linkedIN(obj);
            }
        };
        
        processIntegratedApp(params);
    });
}

function liCheckIfConnected(obj) {
        //If authorized and logged in
        IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {
            params = {
                id: data.values[0].id,
                email: data.values[0].emailAddress,
                field: 'user_linkedin_id'
            };
            
            var ret = checkIfIntegrated(params);
            
            obj.dialog('close');
            obj.dialog('destroy');
            if (ret) {
                /*TODO: MAKE THIS GENERIC FUNCTION */
                $("#dialog").dialog('destroy'); /*THIS IS NEEDED TO BE ABLE TO USE DIALOG AGAIN! */
                $("#dialog").dialog({
                    dialogClass: 'noTitleStuff',
                    resizable: false,
                    modal: true,
                    autoOpen: false,
                    width: 500,
                    height: 200,
                    buttons: [
                        {
                            text: "Login with linkedIn",
                            "class": 'btn-custom-darkblue',
                            click: function() {
                                $(this).dialog("close");
                                return liAuth();
                            }
                        },
                        {
                            text: "Cancel",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                $(this).dialog('destroy'); /*THIS IS NEEDED TO BE ABLE TO USE DIALOG AGAIN! */
                            }
                        }
                    ]
                });
                
                $("#dialog-content").html(SIGNUP_WITH_FB_BUT_REG);
                $("#dialog").dialog("open");
            }
            else {
                return liAuth();
            }
        });
}

function login_linkedIN(obj) {
    IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: 'ajax_post/processIntegratedApp',
            data: {
                id: data.values[0].id,
                email: data.values[0].emailAddress,
                field: 'user_linkedin_id',
                mode: 'linkedIN',
                device: 'desktop',
                data: data.values[0]
            },
            success: function(res) {
                if (res != false) {
                    if (res.post_to_url) {
                        if (typeof PUBLIC_ALIAS !== 'undefined') {
                            response.referrer = GLOMP_BASE_URL+ "/"+PUBLIC_ALIAS;;
                        }
                        // The rest of this code assumes you are not using a library.
                        // It can be made less wordy if you use one.
                        var form = document.createElement("form");
                        form.setAttribute("method", 'post');
                        form.setAttribute("action", res.redirect_url);

                        for (var key in res.data) {
                            if (res.data.hasOwnProperty(key)) {
                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", key);
                                hiddenField.setAttribute("value", res.data[key]);
                                form.appendChild(hiddenField);
                            }
                        }

                        document.body.appendChild(form);
                        obj.dialog('close');
                        return form.submit();
                    }
                    obj.dialog('close');
                    return window.location = res.redirect_url;
                }
            }
        });
    });
}
/* Usable functions */
function processIntegratedApp(params) {
    var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
            Connecting to ' + params.type_string + '...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
    </div></div>');


    connectDialog.dialog({
        dialogClass: 'noTitleStuff',
        title: '',
        autoOpen: false,
        resizable: false,
        closeOnEscape: false,
        modal: true,
        width: 300,
        height: 120,
        show: '',
        hide: ''
    });
    
    connectDialog.dialog("open");
    params.func(connectDialog);
}

function checkIfIntegrated(param) {
    var ret = true;
    $.ajax({
        type: "POST",
        dataType: 'json',
        async: false,
        url: 'ajax_post/checkIfIntegrated',
        data: {
            id: param.id,
            email: param.email,
            field: param.field,
        },
        success: function(res) {
            if (res) {
                ret = true;
            } else {
                ret = false;
            }
        }
    });
    
    return ret;
}