var FB_WHAT_TO_DO = "";
var max_limit = 3000;
var global_fbID = "";
var global_Country = "";
var global_first_name = "";
var global_last_name = "";
var arrSelected = "";
var selectedCtr = 0;

var _friend_data = null;
var _friend_data_global = null;
var global_page = 1;
var global_per_page = 32;
var _selectedFriends = [];
var scopes = 'publish_actions,email';


var NewDialog_Loader ="";
$(document).ready(function(e){

	NewDialog_Loader = $('<div id="confirmPopup" align="center">\
				<div align="center" style="margin-top:5px;">'+ $('#please_wait').text() +'<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
	NewDialog_Loader.dialog({
	autoOpen: false,
	closeOnEscape: false,
	resizable: false,
	dialogClass: 'noTitleStuff',
	title: $('#please_wait').text(),
	modal: true,
	position: 'center',
	width: 200,
	height: 120
	});
    //invitationSent();
    /*$(".friendThumbSelected").click(function(e) {			
     $(this).prop('checked',(!$(this).prop('checked')));
     e.stopPropagation();
     alert("asd");
     });*/

    $("#page_prev").click(function() {
        global_page--;
        loadPage();
    });
    $("#page_next").click(function() {
        global_page++;
        loadPage();
    });
    /* LinkedIn */
    $("#li_page_prev").click(function() {
        global_page--;
        liloadPage();
    });
    $("#li_page_next").click(function() {
        global_page++;
        liloadPage();
    });
    $('#Invite_Through_Fb').click(function() {
        window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getFb=true';
        //askFbOption();
        //getPersonalMessage();
        //sample();
    });
    $('#Invite_Through_LinkedIn').click(function() {
        window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getLinkedIn=true';
        //askLinkedInOption();
    });
    //searchFriends();
    //checkLoginStatus();

    $('#searchTextField').keyup(function(ev) {
        getKey();
    });
    $('#searchTextField').keydown(function(ev) {
        if (FB_WHAT_TO_DO != "" && ev.keyCode == 13) {
            return false;
        }
        else {
            return true;
        }
    });

    $("div").scroll(function() {
        var $this = $(this);
        var height = this.scrollHeight - $this.height(); // Get the height of the div
        var scroll = $this.scrollTop(); // Get the vertical scroll position

        var isScrolledToEnd = (scroll >= height);

        //   $(".scroll-pos").text(scroll);
        //    $(".scroll-height").text(height);

        if (isScrolledToEnd) {
            var additionalContent = loadMoreItems(); // Get the additional content

            $this.append(additionalContent); // Append the additional content

        }
    });
    $("#chk_select_all").click(function() {
        $(".friendThumbSelected").prop('checked', ($("#chk_select_all").prop('checked')));
        if ($("#chk_select_all").prop('checked')) {
            hasList = false;
            idList = new Array();
            $.each(_friend_data_global, function(i, item) {
                idList.push(item.id);
                hasList = true;
            });
            if (hasList)
            {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus',
                    data: 'idList=' + idList,
                    success: function(response) {
                        friendsStatus = response;
                        ctr = 0;
                        $.each(_friend_data_global, function(i, item) {
                            tempID = 'fbid_' + item.id;
                            friendStat = friendsStatus[tempID];
                            friendStat = friendStat.split(":");
                            
                            if (friendStat[0] == -1)
                            {
                                var friendID = item.id;
                                _selectedFriends.push(friendID);
                            }
                        });
                    }
                });
            }
        }
        else
        {
            _selectedFriends = [];
        }

        $('.friendThumbSelected').each(function(i, item) {
            if ($(this).prop('checked')) {
                var friendID = $(this).val();
                removeByValue(_selectedFriends, friendID);
                _selectedFriends.push(friendID);
            }
            else
            {
                var friendID = $(this).val();
                removeByValue(_selectedFriends, friendID);
            }
        });
    });
    $("#li_chk_select_all").click(function() {
        $(".friendThumbSelected").prop('checked', ($("#li_chk_select_all").prop('checked')));
        if ($("#li_chk_select_all").prop('checked')) {
            hasList = false;
            idList = new Array();
            $.each(_friend_data_global, function(i, item) {
                idList.push(item.id);
                hasList = true;
            });
            if (hasList)
            {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_2',
                    data: {
                        idList: idList,  
                        field: 'user_linkedin_id',  
                    },
                    success: function(response) {
                        friendsStatus = response;
                        ctr = 0;
                        $.each(_friend_data_global, function(i, item) {
                            tempID = 'linkedInId_' + item.id;
                            friendStat = friendsStatus[tempID];
                            friendStat = friendStat.split(":");
                            if (friendStat[0] == -1)
                            {
                                var friendID = item.id;
                                _selectedFriends.push(friendID);
                            }
                        });
                    }
                });
            }
        }
        else
        {
            _selectedFriends = [];
        }

        $('.friendThumbSelected').each(function(i, item) {
            if ($(this).prop('checked')) {
                var friendID = $(this).val();
                removeByValue(_selectedFriends, friendID);
                _selectedFriends.push(friendID);
            }
            else
            {
                var friendID = $(this).val();
                removeByValue(_selectedFriends, friendID);
            }
        });
    });

    $(".addThisFriendClass").click(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        //alert($(this).prop("id"));
        var id = $(this).prop("id");
        var name = $('#data_name_' + id).html();
        var loc = $('#data_loc_' + id).html();
        var desc = $('#add_to_glomp_network').text().replace('[username]', name);
        var img = $('#data_img_' + id).prop('src');
        var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
									<div class="friendThumb3" > \
										<div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
											<image id="fb_friend_popup_img" style="float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
										</div> \
									</div>\
									<div class="friendThumb3_name" > \
										<b>' + name + '</b><br /><span style="">' + loc + '</span>\
									</div>\
									<div class="friendThumb3_desc"  >' + desc + ' \
									</div> \
								</div>');
        NewDialog.dialog({
            dialogClass: 'noTitleStuff',
            dialogClass:'dialog_style_blue_grey',
                    title: "",
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 300,
            height: 250,
            show: 'clip',
            hide: 'clip',
            buttons: [{text: $('#Confirm').text(),
                    "class": 'btn-custom-darkblue',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#FriendPopupDialog').dialog('destroy').remove();
                            addThisFriend(id);
                        }, 500);
                    }},
                {text: $('#cancel_btn_search_friends').text(),
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#FriendPopupDialog').dialog('destroy').remove();
                        }, 500);
                    }}
            ]
        });
        NewDialog.dialog('open');
    });

    $('#invite_li_friends').click(function(){
       $("#invite_fb_friends").click(); 
    });
    
    $("#invite_fb_friends").click(function() {
        // check if some friends are selected
        var j = _selectedFriends.length;
        //var selectedFriends= [];
        /*$('.friendThumbSelected').each(function( i,  item) {	
         if($(this).prop('checked')){
         var friendID= $(this).val();
         selectedFriends.push(friendID);	
         j++;
         }
         })*/;
        if (j > 0) {
            if (linkedIn) return doInviteLiFriends(_selectedFriends);
            doInviteFbFriends(_selectedFriends);
            return false;
        }
        else {
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
					<div id="personalMessageTips" style="padding:2px;font-size:14px;">'+ $('#No_friends_selected').text() +'</div> \
					<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);

                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        // check if some friends are selected


        /*	$(this).submit(function() {
         return false;
         });
         return true;*/
    });

    window.fbAsyncInit = function() {
        FB.init({
            appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML	  });		
        });
    };

});
function setCheckSelecAllValue() {
    var all = 0
    var chk = 0;
    $('.hasCheckBox').each(function(i, item) {
        all++;
    });
    $('.friendThumbSelected').each(function(i, item) {
        if ($(this).prop('checked')) {
            chk++;
        }
    });
    if (all > 0 && all == chk) {
        $("#chk_select_all").prop('checked', true);
    }
    else
        $("#chk_select_all").prop('checked', false);
}
function setCheckSelecAllValueLi() {
    var all = 0
    var chk = 0;
    $('.hasCheckBox').each(function(i, item) {
        all++;
    });
    $('.friendThumbSelected').each(function(i, item) {
        if ($(this).prop('checked')) {
            chk++;
        }
    });
    if (all > 0 && all == chk) {
        $("#li_chk_select_all").prop('checked', true);
    }
    else {
        $("#li_chk_select_all").prop('checked', false);
    }
}
function sortByName(a, b) {
    var x = a.name.toLowerCase();
    var y = b.name.toLowerCase();
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

//var _friend_data = null
function lazy_load_friend_data(uid) {
    if (_friend_data == null) {
        FB.api('/me/friends', function(response) {
            _friend_data = response.data.sort(sortByName);
        }
        )
    }
}
function removeMessage() {
    $('#definedMessage').html('');
    $('#messageDialog').dialog({
        width: 460,
        height: 280,
    });
}
function doInviteFbFriends(selectedFriends) {
    console.log(selectedFriends);
    var names = "";
    selectedCtr = 0;
    var firstID = "";
    var tags = "";
    arrSelected = new Array();
    var notes = "";
    $.each(selectedFriends, function(i, item) {
        selectedCtr++;

        tags += ' @[' + item + ']\n';
        arrSelected.push(item);
        var friendName = "";//$("#search_name_"+item).html();			
        $.each(_friend_data_global, function(i2, item2) {
            if (item2.id == item) {
                friendName = item2.name;
                //break;
            }
        });


        if (firstID == "")
            firstID = item;
        if (selectedCtr <= 5) {
            if (names != "")
                names += ', ';
            names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
        }

    });
    console.log(arrSelected);
    if (selectedCtr > 5) {
        names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

    }
    if (selectedCtr > 10)
    {
        notes = '<tr> \
							<td ><hr size="1" style="padding:0px;margin:0px;"><div style="font-size:9px;width:400px;text-align:justify;padding:0px 0px 0px 3px" >' + GLOMP_FB_INVITE_REMINDER + '</div></td> \
						</tr>';
    }
    if ($('#messageDialog').doesExist() == true) {
        $('#messageDialog').dialog('destroy').remove();
    }

    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="10" height="10" title="Remove this message" style="float:right" onClick="removeMessage()" />'
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
			<table border=0 width="399"> \
				<tr> \
					<td><b>To</b>:' + names + '</td> \
				</tr> \
				<tr> \
					<td><hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:11px;" align="left"></div></td> \
				</tr> \
				<tr> \
					<td ><textarea id="personalMessage" onKeyUp="checkPersonalMsgLength()" style="font-size:12px;resize:none;width:400px;height:50px;" placeholder="'+ $('#your_personal_message_placeholder').text() +'"></textarea></td> \
				</tr> \
				<tr> \
					<td >' + close_button + '<div id="definedMessage"  style="font-size:12px;padding:3px;text-align:justify;width:370px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div></td> \
				</tr> \
				' + notes + '\
			</table> \
			<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 460,
        height: 380,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: $('#invite_btn_search_friends').text(),
                "class": 'btn-custom-blue',
                click: function() {
                    var bValid = true;
                    $("#personalMessage").removeClass("ui-state-error");
                    bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 256, '#personalMessageTips');
                    if (bValid) {
                        var NewDialog = $('<div id="confirmPopup" align="center">\
												<div align="center" style="margin-top:5px;">'+ $('#please_wait').text() +'<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'noTitleStuff',
                            title: $('#please_wait').text(),
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        NewDialog.dialog('open');

                        personalMessage = $("#personalMessage").val();
                        definedMessage = $("#definedMessage").html();
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                        }, 500);

                        //var profile=GLOMP_BASE_URL+"welcome.html?from="+glomp_user_id;
                        //var profile ='http://glomp.it/welcome.html?from='+glomp_user_id;
                        //var profile ='http://glomp.it/?from='+glomp_user_id;
                        var profile = 'http://glomp.it/?from=' + glomp_user_id;


                        doGlompPost(profile, personalMessage, definedMessage, 0);

                    }

                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function doInviteLiFriends(selectedFriends) {
    var names = "";
    selectedCtr = 0;
    var firstID = "";
    var tags = "";
    arrSelected = new Array();
    var notes = "";
    $.each(selectedFriends, function(i, item) {
        selectedCtr++;

        tags += ' @[' + item + ']\n';
        arrSelected.push(item);
        var friendName = "";//$("#search_name_"+item).html();			
        $.each(_friend_data_global, function(i2, item2) {
            if (item2.id == item) {
                friendName = item2.formattedName;
                //break;
            }
        });


        if (firstID == "")
            firstID = item;
        if (selectedCtr <= 5) {
            if (names != "")
                names += ', ';
            names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
        }

    });
    console.log(arrSelected);
    if (selectedCtr > 5) {
            names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> '+ $('#Others').text() +'</span>';

    }
    if (selectedCtr > 10)
    {
        notes = '';
    }
    if ($('#messageDialog').doesExist() == true) {
        $('#messageDialog').dialog('destroy').remove();
    }

    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("icon", "link");
    var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="10" height="10" title="Remove this message" style="float:right" onClick="removeMessage()" />'
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
			<table border=0 width="399"> \
				<tr> \
					<td><b>To</b>:' + names + '</td> \
				</tr> \
				<tr> \
					<td><hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:11px;" align="left"></div></td> \
				</tr> \
				<tr> \
					<td ><textarea id="personalMessage" onKeyUp="checkPersonalMsgLength()" style="font-size:12px;resize:none;width:400px;height:50px;" placeholder="'+ $('#your_personal_message_placeholder').text() +'"></textarea></td> \
				</tr> \
				<tr> \
					<td >' + close_button + '<div id="definedMessage"  style="font-size:12px;padding:3px;text-align:justify;width:370px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div></td> \
				</tr> \
				' + notes + '\
			</table> \
			<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 460,
        height: 380,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: $('#invite_btn_search_friends').text(),
                "class": 'btn-custom-blue',
                click: function() {
                    var bValid = true;
                    $("#personalMessage").removeClass("ui-state-error");
                    bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 256, '#personalMessageTips');
                    if (bValid) {
                        var NewDialog = $('<div id="confirmPopup" align="center">\
												<div align="center" style="margin-top:5px;">' + $('#please_wait').text() +'<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'noTitleStuff',
                            title: $('#please_wait').text(),
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        NewDialog.dialog('open');

                        personalMessage = $("#personalMessage").val();
                        definedMessage = $("#definedMessage").html();
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                        }, 500);

                        //var profile=GLOMP_BASE_URL+"welcome.html?from="+glomp_user_id;
                        //var profile ='http://glomp.it/welcome.html?from='+glomp_user_id;
                        //var profile ='http://glomp.it/?from='+glomp_user_id;
                        var profile = 'http://glomp.it/?from=' + glomp_user_id;

                        if (personalMessage == '') {
                            personalMessage = 'Its time for a treat!'
                        }
                        
                        doGlompPostLi(profile, personalMessage, definedMessage, 0);

                    }

                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function checkPersonalMsgLength() {
    $("#personalMessage").removeClass("ui-state-error");
    $('#personalMessageTips').html('');

    checkLength($("#personalMessage"), "Personal message", 1, 256, '#personalMessageTips');
}
function reloadPage()
{
    $('#messageDialog').dialog("close");
    setTimeout(function() {
        $('#messageDialog').dialog('destroy').remove();
    }, 500);
    location.reload();
}

function doGlompPostLi(profile, personalMessage, definedMessage, ctr) {
    var lim = 10;
    var tags = "";
    var invitedIDs=new Array();
    var invitedNames=new Array();
    var receipients =new Array();
    for (; ctr < selectedCtr; ) {
        tags += ' @[' + arrSelected[ctr] + ']\n';
        
        invitedIDs.push(arrSelected[ctr]);
        $.each(_friend_data_global, function(i2, item2) {
            if (item2.id == arrSelected[ctr]) {                
                friendName = item2.formattedName;
                receipient ={
                    "person": {
                         "_path": "/people/" + item2.id,
                    }
                };
            }
        });
        invitedNames.push(friendName);
        receipients.push(receipient);
        
        lim--;
        ctr++;
        if (lim == 0)
            break;
    }
    /*if(ctr<selectedCtr){
     doGlompPost(profile, personalMessage, definedMessage, ctr);
     }*/
    
    IN.API.Raw("people/~/mailbox")
    .method("POST")
    .body(JSON.stringify({
        "recipients": {
          "values": receipients
        },
        "subject": personalMessage,
        "body": '\n' + personalMessage + '\n\n' + definedMessage + '\n\n' + profile
      })
    ).result(function(result){
        $.ajax({
            type: "POST",
            url: 'ajax_post/li_log_invite',
            data: 'invitedIDs=' + invitedIDs+'&invitedNames='+invitedNames,
        });
        
        if (ctr < selectedCtr) {
            doGlompPostLi(profile, personalMessage, definedMessage, ctr);
        }
        else {
            arrSelected="";
            _selectedFriends="";
            $('#confirmPopup').dialog('destroy').remove();
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">' + $('#invite_sent').text() + '</div> \
            <p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            checkTour();
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);

                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }        
        
    });

}                                
                                
function doGlompPost(profile, personalMessage, definedMessage, ctr) {
    var lim = 10;
    var tags = "";
    var invitedIDs=new Array();
    var invitedNames=new Array();
    for (; ctr < selectedCtr; ) {
        tags += ' @[' + arrSelected[ctr] + ']\n';
        
        invitedIDs.push(arrSelected[ctr]);
        $.each(_friend_data_global, function(i2, item2) {
            if (item2.id == arrSelected[ctr]) {
                friendName = item2.name;                
            }
        });
        invitedNames.push(friendName);
        
        lim--;
        ctr++;
        if (lim == 0)
            break;
    }
    console.log(ctr + "=" + tags);
    /*if(ctr<selectedCtr){
     doGlompPost(profile, personalMessage, definedMessage, ctr);
     }*/
    
    
    
    
    FB.api(
            'me/glomp_app:invite',
            'post',
            {
                profile: profile,
                message: '\n' + tags + '\n' + personalMessage + '\n\n' + definedMessage
            },
    function(response) {
        //log_invite
        $.ajax({
            type: "POST",
            url: 'ajax_post/fb_log_invite',
            data: 'invitedIDs=' + invitedIDs+'&invitedNames='+invitedNames,
            success: function(res) {
                
            }
        });
        //log_invite
        if (hasOwnProperty(response, 'id')) {
            if (ctr < selectedCtr) {
                doGlompPost(profile, personalMessage, definedMessage, ctr);
            }
            else {
                arrSelected="";
                _selectedFriends="";
                $('#confirmPopup').dialog('destroy').remove();
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">'+ $('#invite_sent').text() +'</div> \
							<p></div>');
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 220,
                    height: 120,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: $('#Ok').text(),
                            "class": 'btn-custom-white2',
                            click: function() {
                                $('.friendThumbSelected').each(function(i, item) {
                                    if ($(this).prop('checked')) {
                                        $(this).prop('checked', false);
                                    }
                                });

                                $(this).dialog("close");
                                checkTour();
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);

                                $('#listDialog').dialog("close");
                                setTimeout(function() {
                                    $('#listDialog').dialog('destroy').remove();
                                }, 500);
                            }}
                    ]
                });
                NewDialog.dialog('open');
            }
        }
        else if (response.error) {
            arrSelected="";
            _selectedFriends="";
            $('#confirmPopup').dialog('destroy').remove();
            $('#listDialog').dialog("close");
            setTimeout(function() {
                $('#listDialog').dialog('destroy').remove();
            }, 500);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                                '+ $('#fb_error_msg').text().replace('[here]', '<a href="javascript:void(0);" onclick="reloadPage()" style="text-decoration:underline" >here</a>') +'\
                            </div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 320,
                height: 130,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#cancel_btn_search_friends').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        else {
            arrSelected="";
            _selectedFriends="";

            $('#confirmPopup').dialog('destroy').remove();
            $('#listDialog').dialog("close");
            setTimeout(function() {
                $('#listDialog').dialog('destroy').remove();
            }, 500);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">'+ $('#an_error_occured').text() +'</div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        // handle the response
        console.log(response);
        //var URL='http://facebook.com/'+response.id;
        //window.open(URL);
    }
    );

}
function checkLength(o, n, min, max, tipsID) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(tipsID, "Length of " + n + " must be between " + min + " and " + max + ".");
        return false;
    } else {
        return true;
    }
}
function updateTips(tipsID, t) {
    $(tipsID).html(t);
    //.addClass( "ui-state-highlight" );
    setTimeout(function() {
        $(tipsID).removeClass("ui-state-highlight", 1500);
    }, 500);
}



// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

function bindScroll() {
    $('#searchUserScroller').bind('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            //var new_div = '<div class="new_block"></div>';
            loadMoreItems();
        }
    });
}

function loadMoreItems() {
    alert("loadMoreItems");
}
/*search friends////*/
var hasHidden = false;
function searchFriends() {
    if (FB_WHAT_TO_DO != '')
    {
        var searchThis = ($.trim($("#searchTextField").val())).toLowerCase();
        if (searchThis != "") {
            var found = 0;
            _friend_data = new Array();
            $.each(_friend_data_global, function(i, item) {
                if (linkedIn) {
                    var name = (item.formattedName).toLowerCase();
                } else {
                    var name = (item.name).toLowerCase();
                }
                if (name.indexOf(searchThis) >= 0) {
                    _friend_data.push(item);
                    found++;
                }
            });
            global_page = 1;
            if (found == 0) {
                $('#friendsList').html('');
                afterCreateFbFriendsList();
            }
            else {
                var count = _friend_data.length;
                if (linkedIn) {
                    paginateLiFriends(global_page, global_per_page, count, _friend_data);
                } else {
                    paginateFbFriends(global_page, global_per_page, count, _friend_data);
                }
            }

        }
        else {
            _friend_data = _friend_data_global;
            global_page = 1;
            var count = _friend_data.length;
            if (linkedIn) {
                paginateLiFriends(global_page, global_per_page, count, _friend_data);
            } else {
                paginateFbFriends(global_page, global_per_page, count, _friend_data);
            }
        }
    }
    else
    {

    }
    /*var searchThis=	$.trim( $("#searchTextField").val());	
     if(searchThis!=""){
     var found=0;
     $('.friendSearcheable').each(function( i,  item) {	
     var thisID=($(this).val()).toLowerCase();
     var name=($('#search_name_'+thisID).html()).toLowerCase();
     
     if (name.indexOf(searchThis) >= 0) { 
     $('#search_wrapper_'+thisID).show();
     found++;
     }
     else{
     $('#search_wrapper_'+thisID).hide();
     hasHidden=true;
     }
     });
     if(found==0){
     $('#search_not_found').show();			
     }
     else{
     $('#search_not_found').hide();
     }
     
     $(".searchUser").nanoScroller();
     }
     else if(hasHidden){
     $('.friendSearcheable').show();
     $(".searchUser").nanoScroller();
     }*/
}
var cc = 0;
var t;
var timer_is_on = 0;

function timedCount(type)
{
    cc = cc + 1;
    if (cc == 2) {
        searchFriends();
    }
    else
        t = setTimeout("timedCount()", 500);
}
function doTimer() {
    cc = 0;
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}
function stopCount() {
    cc = 0;
    clearTimeout(t);
    timer_is_on = 0;
}
function getKey() {
    stopCount();
    doTimer();
}
function getCheck(_this) {
    //_this.checked=!(_this.checked);        
    var tempID = _this.value;
    if (_this.checked)
    {
        _selectedFriends.push(tempID);
    }
    else {
        removeByValue(_selectedFriends, tempID);
    }
}
function addToList(item, thisFriendStatus) {
    thisFriendStatus = thisFriendStatus.split(":");
    loc = "";
    item.loc = loc;
    if (hasOwnProperty(item, 'location')) {
        temp = item.location;
        loc = temp.name;
        item.loc = loc;
    }
    var icon = '';
    var plus = '';
    var href = '';
    var wrapper = 'wrapper_img_fb_';

    if (thisFriendStatus[0] == -1) {

        icon = '<image style="float:left;z-index: 2;margin-top:-28px;margin-right:3px; width: 25px" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/fb-icon.png" \>';
        href = '<a href="javascript:void(0);" id="href_wrapper_img_fb_' + item.id + '" >';
        if (FB_WHAT_TO_DO == 'invite') {
            var chkVal = checkIfThisHasBeenChecked(_selectedFriends, item.id);
            if (chkVal)
                chkVal = 'checked';
            else
                chkVal = '';
            plus = '<input onclick="getCheck(this)" ' + chkVal + ' type="checkbox" value="' + item.id + '" id="chk_' + item.id + '" class="hasCheckBox friendThumbSelected" style="float:right;z-index: 2;margin-top:-18px;margin-right:3px" />';
        }
        else {

        }
    }
    else if (thisFriendStatus[0] == 0) {
        icon = '<image class="addThisFriendClass" onClick="askAddThisFriend(' + item.id + ')" id="fb_plus_' + item.id + '" style="cursor:pointer;float:right;z-index: 10;margin-top:-28px;margin-right:3px; width: 25px;" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
        href = '<a id="fb_link_' + item.id + '"  href="javascript:void(0);" >';
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[1] + '" target="" >';
        wrapper = '';
    }
    else {
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[0] + '" target="" >';
        wrapper = '';
    }
    $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
										<div class=""> \
												<div class="" > \
													' + href + ' \
													<div id="' + wrapper + '' + item.id + '"  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
														<image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="https://graph.facebook.com/' + item.id + '/picture?type=large&return_ssl_results=1" alt="' + item.name + '"  \> \
													</div> \
													</a> \
													' + icon + '' + plus + ' \
													' + href + ' \
													<p><b id="search_name_' + item.id + '" >' + item.name + '</b><br /><span style="" id="search_loc_' + item.id + '" >' + loc + '</span></p> \
													</a> \
												</div> \
										</div> \
									</div>');
    $elem.val(item.id);





    $('#friendsList').append($elem);

    var img = document.getElementById('img_fb_' + item.id);
    //or however you get a handle to the IMG
    var width = parseInt(img.clientWidth);
    var height = parseInt(img.clientHeight);
    if (width > height) {
        $('#img_fb_' + item.id).css({'height': 100});
    }
    else {
        $('#img_fb_' + item.id).css({'width': 100});
    }





    $(('#href_wrapper_img_fb_' + item.id)).on("click", function(ev) {
        //$(('#wrapper_img_fb_'+item.id)).click();
    });
    $(('#wrapper_img_fb_' + item.id)).on("click", function(ev) {
        var elemID = item.id;//$(this).val();	
        var name = $('#search_name_' + elemID).html();
        if (FB_WHAT_TO_DO == 'invite') {
            var chk = !($('#chk_' + elemID).prop('checked'));
            $('#chk_' + elemID).prop('checked', chk);
            if ($('#chk_' + elemID).prop('checked'))
            {
                _selectedFriends.push(elemID);
            }
            else {
                removeByValue(_selectedFriends, elemID);
            }
        }
        else {
            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
													  <div align="center" style="margin-top:5px;">' + $('#getting_friends_location').text() + '<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
													  </div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 200,
                height: 120,
                show: '',
                hide: ''
            });
            NewDialog.dialog('open');
            FB.api(elemID+ '?fields=first_name, last_name, location',
            function(response) {
                console.log(response);
                var country = "";
                var first_name = response['first_name'];
                var last_name = response['last_name'];
                if (response['location'] != undefined)
                    country = response['location'];
                console.log(country);
                if (country == "") {
                    $.ajax({
                        type: "POST",
                        url: 'ajax_post/getCountryList',
                        success: function(res) {
                            console.log(res);
                            $("#waitDialog").dialog("close");
                            $("#waitDialog").dialog('destroy').remove();
                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
															  <div align="center" style="margin-top:5px;"><b>' + name + '</b> '+ $('#hidden_location_msg').text().replace(new RegExp('%newline%', 'g'), '<br>') +'\
															  <br><br><select id="selected_country" name="selected_country">' + res + '</select></div>\
															  </div>');
                            NewDialog.dialog({
                                dialogClass: 'noTitleStuff',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 350,
                                height: 200,
                                show: '',
                                hide: '',
                                buttons: [
                                    {text: $('#notice_facebook_btn_2').text(),
                                        "class": 'btn-custom-darkblue',
                                        click: function() {
                                            country = $('#selected_country').val();
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                            showGlompMenu(elemID, country, first_name, last_name);
                                        }},
                                    {text: $('#cancel_btn_search_friends').text(),
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    });
                }
                else {
                    $("#waitDialog").dialog("close");
                    $("#waitDialog").dialog('destroy').remove();
                    showGlompMenu(elemID, country, first_name, last_name);
                }


            });



        }


    });








    $(".searchUser").nanoScroller();



}


function addToListLi(item, thisFriendStatus) {
    var loc = countries[item.location.country.code.toUpperCase()].printable_name;
    var firstName = item.firstName;
    var lastName = item.lastName;
    
    if (loc === '') {
        loc = '';
    }
    
    try {
        thisFriendStatus = thisFriendStatus.split(":");
    }catch(e){return;}
    
    pic = GLOMP_BASE_URL + 'custom/uploads/users/thumb/Male.jpg';
    try {
        var pic = item.pictureUrls.values[0];
    }catch(e){}
    
    var icon = '';
    var plus = '';
    var href = '';
    var wrapper = 'wrapper_img_fb_';

    if (thisFriendStatus[0] == -1) {

        icon = '<image style="float:left;z-index: 2;margin-top:-28px;margin-right:3px;width: 25px" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/li-icon.jpg" \>';
        href = '<a href="javascript:void(0);" id="href_wrapper_img_fb_' + item.id + '" >';
        if (FB_WHAT_TO_DO == 'invite') {
            var chkVal = checkIfThisHasBeenChecked(_selectedFriends, item.id);
            if (chkVal)
                chkVal = 'checked';
            else
                chkVal = '';
            plus = '<input onclick="getCheck(this)" ' + chkVal + ' type="checkbox" value="' + item.id + '" id="chk_' + item.id + '" class="hasCheckBox friendThumbSelected" style="float:right;z-index: 2;margin-top:-18px;margin-right:3px" />';
        }
        else {

        }
    }
    else if (thisFriendStatus[0] == 0) {
        icon = '<image class="addThisFriendClass" onClick="askAddThisFriend(\'' + item.id + '\')" id="fb_plus_' + item.id + '" style="cursor:pointer;float:right;z-index: 10;margin-top:-28px;margin-right:3px; width: 25px" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
        href = '<a id="fb_link_' + item.id + '"  href="javascript:void(0);" >';
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[1] + '" target="" >';
        wrapper = '';
    }
    else {
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[0] + '" target="" >';
        wrapper = '';
    }
    $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
            <div class=""> \
                            <div class="" > \
                                    ' + href + ' \
                                    <div id="' + wrapper + '' + item.id + '"  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
                                            <image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="' + pic + '" alt="' + item.formattedName + '"  \> \
                                    </div> \
                                    </a> \
                                    ' + icon + '' + plus + ' \
                                    ' + href + ' \
                                    <p><b id="search_name_' + item.id + '" >' + item.formattedName + '</b><br /><span style="" id="search_loc_' + item.id + '" >' + loc + '</span></p> \
                                    </a> \
                            </div> \
            </div> \
        </div>');
    $elem.val(item.id);

    $('#friendsList').append($elem);

    var img = document.getElementById('img_fb_' + item.id);
    //or however you get a handle to the IMG
    var width = parseInt(img.clientWidth);
    var height = parseInt(img.clientHeight);
    if (width > height) {
        $('#img_fb_' + item.id).css({'height': 100});
    }
    else {
        $('#img_fb_' + item.id).css({'width': 100});
    }
    
    $(('#href_wrapper_img_fb_' + item.id)).on("click", function(ev) {
        //$(('#wrapper_img_fb_'+item.id)).click();
    });
    $(('#wrapper_img_fb_' + item.id)).on("click", function(ev) {
        var elemID = item.id;//$(this).val();	
        var name = $('#search_name_' + elemID).html();
        if (FB_WHAT_TO_DO == 'invite') {
            var chk = !($('#chk_' + elemID).prop('checked'));
            $('#chk_' + elemID).prop('checked', chk);
            if ($('#chk_' + elemID).prop('checked'))
            {
                _selectedFriends.push(elemID);
            }
            else {
                removeByValue(_selectedFriends, elemID);
            }
        }
        else {
            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                <div align="center" style="margin-top:5px;">'+ $('#getting_friends_location').text() + '<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
            </div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 200,
                height: 120,
                show: '',
                hide: ''
            });
            NewDialog.dialog('open');
            
            if (loc == "") {
                $.ajax({
                    type: "POST",
                        url: 'ajax_post/getCountryList',
                        success: function(res) {
                            $("#waitDialog").dialog("close");
                            $("#waitDialog").dialog('destroy').remove();
                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                    <div align="center" style="margin-top:5px;"><b>' + name + '</b> has hidden their location in LinkedIn.<br><br>Please enter their location here:\
                                    <br><br><select id="selected_country" name="selected_country">' + res + '</select></div>\
                            </div>');
                            
                            NewDialog.dialog({
                                dialogClass: 'noTitleStuff',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 350,
                                height: 200,
                                show: '',
                                hide: '',
                                buttons: [
                                    {text: $('#notice_facebook_btn_2').text(),
                                        "class": 'btn-custom-darkblue',
                                        click: function() {
                                            country = $('#selected_country').val();
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                            showGlompMenu(elemID, loc, firstName, lastName);
                                        }},
                                    {text: $('#cancel_btn_search_friends').text(),
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    });
                }
                else {
                    $("#waitDialog").dialog("close");
                    $("#waitDialog").dialog('destroy').remove();
                    showGlompMenu(elemID, loc, firstName, lastName);
                }
        }
    });
    $(".searchUser").nanoScroller();
}

function askAddThisFriend(id) {
    var name = $('#search_name_' + id).html();
    var loc = $('#search_loc_' + id).html();
    var desc = $('#add_to_glomp_network').text().replace("[username]", name);
    var img = $('#img_fb_' + id).prop('src');
    var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
									<div class="friendThumb3" > \
										<div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
											<image id="fb_friend_popup_img" style="float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
										</div> \
									</div>\
									<div class="friendThumb3_name" > \
										<b>' + name + '</b><br /><span style="">' + loc + '</span>\
									</div>\
									<div class="friendThumb3_desc"  >' + desc + ' \
									</div> \
								</div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        dialogClass:'dialog_style_blue_grey',
                title: "",
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 300,
        height: 250,
        show: 'clip',
        hide: 'clip',
        buttons: [{text: $('#Confirm').text(),
                "class": 'btn-custom-darkblue',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#FriendPopupDialog').dialog('destroy').remove();
                        if(linkedIn) return addThisFriendLi(id);
                        else {
                            addThisFriendFB(id);
                        }
                    }, 500);
                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#FriendPopupDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}

var timerFacebookDelay = null;
function doFacebookDelay(targetPopup, targetElement, timeout, outputType)
{
    var message = $('#we_are_experiencing_a_loading').text();
    message = '<span style="font-size:14px">' + message + '</span>&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />';

    timerFacebookDelay = setTimeout(function()
    {
        if (outputType == 'popup')
        {

            if ($('#' + targetPopup).doesExist())
            {//target popupExist
                $('#' + targetElement).html(message);
            }
            else
            {//create a new popup                    

                var NewDialog = $(' <div id="' + targetPopup + '" align="center">\
                                            <div align="center" style="margin-top:5px;" id="' + targetElement + '">\
                                            ' + message + '\
                                            </div>\
                                        </div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'noTitleStuff',
                    title: $('#please_wait').text(),
                    modal: true,
                    position: 'center center',
                    width: 420,
                    height: 100
                });
                NewDialog.dialog('open');
            }
        }
        else
        {

            $('#' + targetElement).html(message);
        }


    }, timeout);
}

function clearFacebookDelay()
{
    clearTimeout(timerFacebookDelay);
    if ($('#facebookLoadingDialog').doesExist())
    {
        $("#facebookLoadingDialog").dialog('close');
        $("#facebookLoadingDialog").dialog('destroy').remove();
    }
}

function createFbFriendsList()
{
    //$('#friendsList').html('<div align="center" style="margin-top:50px;" id="friendsList_loading">' + GLOMP_GETTING_FB_FRIENDS + '<span style="padding:9px 15px;background-image:url(\'assets/frontend/css/images/ajax-loader.gif\');repeat:no-reapeat;">&nbsp</span></div>');
    doFacebookDelay('facebookLoadingDialog', 'friendsList_loading', 1000, 'popup');
    //if (_friend_data == null)
	if (true)
    {
		clearFacebookDelay();//clear the timeout
		FB.ui({method: 'apprequests',
			message: 'glomp!',
		}, function(response)
		{
			if (typeof response === "undefined") 
				return;
			
			else if(FB_WHAT_TO_DO == 'glomp')
			{
				if (response.to.length == 1)
				{
					/* do your fb glomping here*/
					/* do your fb glomping here*/
					idList = response.to;
					var list='';
					var not_registered_ids = new Array();
                    
                    //Get real FB ID
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: GLOMP_BASE_URL + 'api1/service/fb_data?param='+ idList.join(',') +'&format=json',
                        success: function(response) {
                            NewDialog_Loader.dialog('close');
                            var fbdata = response.data;
                            var idList = [];

                            //loop ids
                            for (fid = 0; fid < fbdata.length; fid++) {
                                idList.push(fbdata[fid].id);
                            }

                            //start check friend list status
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_new',
                                data: 'idList=' + idList,
                                success: function(response) {
                                    console.log(response);
                                    friendsStatus = response;
                                    
                                    tempID = 'fbid_' + idList;
                                        friendStat = friendsStatus[tempID];
                                        tempID = 'fbid__name_' + idList;
                                        friend_Name =friendsStatus[tempID];
                                        
                                        tempID = 'fbid_user_id' + idList;
                                        friend_user_id =friendsStatus[tempID];
                                        
                                        if(friendStat == 'not_registered')
                                        {
                                            /*fb glomp to non member*/
                                            /*fb glomp to non member*/
                                            FB.api(idList+ '?fields=first_name, last_name, location',
                                            function(response) {
                                                console.log(response);
                                                var first_name = response['first_name'];
                                                var last_name = response['last_name'];
                                                var name =first_name + ' '+last_name;
                                                var elemID =idList;
                                                
                                                $.ajax({
                                                    type: "POST",
                                                    url: 'ajax_post/getCountryList',
                                                    success: function(res) {
                                                        console.log(res);
                                                        $("#waitDialog").dialog("close");
                                                        $("#waitDialog").dialog('destroy').remove();
                                                        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                                                          <div align="center" style="margin-top:5px;"><b>' + name + '</b> '+ $('#hidden_location_msg').text().replace(new RegExp('%newline%', 'g'), '<br>') +'\
                                                                                          <br><br><select id="selected_country" name="selected_country">' + res + '</select></div>\
                                                                                          </div>');
                                                        NewDialog.dialog({
                                                            dialogClass: 'noTitleStuff',
                                                            title: '',
                                                            autoOpen: false,
                                                            resizable: false,
                                                            modal: true,
                                                            width: 350,
                                                            height: 200,
                                                            show: '',
                                                            hide: '',
                                                            buttons: [
                                                                {text: $('#notice_facebook_btn_2').text(),
                                                                    "class": 'btn-custom-darkblue',
                                                                    click: function() {
                                                                        country = $('#selected_country').val();
                                                                        $('#waitDialog').dialog("close");
                                                                        $('#waitDialog').dialog('destroy').remove();
                                                                        showGlompMenu(elemID, country, first_name, last_name);
                                                                    }},
                                                                {text: $('#cancel_btn_search_friends').text(),
                                                                    "class": 'btn-custom-white2',
                                                                    click: function() {
                                                                        $('#waitDialog').dialog("close");
                                                                        $('#waitDialog').dialog('destroy').remove();
                                                                    }}
                                                            ]
                                                        });
                                                        NewDialog.dialog('open');
                                                    }
                                                });
                                            });
                                            
                                            /*fb glomp to non member*/
                                            /*fb glomp to non member*/
                                        }
                                        else
                                        {
                                             list +='<div style="width:100px;min-height:90px; margin: 3px 0px 3px 5px; background: #fff;padding: 3px;border: solid 0px #000000;color: #59606B;"> \
                                                    <div class=""> \
                                                            <div class="" > \
                                                                <a target="_blank" href="'+GLOMP_BASE_URL+'profile/view/'+friend_user_id+'"> \
                                                                <div  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
                                                                    <image style="float:left;z-index:1;" src="https://graph.facebook.com/' + idList+ '/picture?type=large&return_ssl_results=1"   \> \
                                                                </div> \
                                                                <p><b style="font-size:12px;color:#333;" >' + friend_Name + '</b></p> \
                                                                </a> \
                                                            </div> \
                                                    </div> \
                                                </div>';
                                            var text = friend_Name +' is already on glomp!.';
                                        
                                            var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                    <div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
                                                                    <div style="" align="center">\
                                                                    '+list+'\
                                                                    </div>\
                                                                    </div>');
                                                NewDialog.dialog({                      
                                                    autoOpen: false,
                                                    closeOnEscape: false ,
                                                    resizable: false,                   
                                                    dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                                    title:'',
                                                    modal: true,
                                                    width:250,
                                                    position: 'center',
                                                    buttons: [{text: 'Go to profile',
                                                            "class": 'btn-custom-darkblue',
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                window.location =GLOMP_BASE_URL+'profile/view/'+friend_user_id;
                                                            }}
                                                    ]
                                                        
                                                });
                                                NewDialog.dialog('open');   
                                            }   
                                    
                                }
                            });
                            //end ajax check friend list status

                        }
                    });
                    return;
					/* do your fb glomping here*/
					/* do your fb glomping here*/
				}
				else
				{
					if(response.to.length>1)
						var text = 'Please select 1 Facebook friend only to glomp!.';
					else
						var text = 'Please select 1 Facebook friend to glomp!.';
             
					var NewDialog = $('<div id="confirmPopup" align="center">\
										<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
										</div>');
						NewDialog.dialog({						
							autoOpen: false,
							closeOnEscape: false ,
							resizable: false,					
							dialogClass:'dialog_style_glomp_wait noTitleStuff',
							title:'',
							modal: true,
							width:360,
							height:130,
							position: 'center',
							buttons: [{text: 'OK',
									"class": 'btn-custom-darkblue',
									click: function() {
										$(this).dialog("close");
										createFbFriendsList();
									}},
									{text: 'Cancel',
									"class": 'btn-custom-white2 ',
									click: function() {
										$(this).dialog("close");
										
									}}
							]
								
						});
						NewDialog.dialog('open');
				}
			} 
			/*if(FB_WHAT_TO_DO == 'glomp')*/
			/*if(FB_WHAT_TO_DO == 'glomp')*/
			else if(FB_WHAT_TO_DO == 'invite')
			{
			
				if (response.length == 0)
				{
					var NewDialog = $('<div id="confirmPopup" align="center">\
											<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">Please select atleast 1 Facebook friend to invite.</span></div>\
											</div>');
						NewDialog.dialog({						
							autoOpen: false,
							closeOnEscape: false ,
							resizable: false,					
							dialogClass:'dialog_style_glomp_wait noTitleStuff',
							title:'',
							modal: true,
							width:360,
							height:130,
							position: 'center',
							buttons: [{text: 'OK',
									"class": 'btn-custom-darkblue',
									click: function() {
										$(this).dialog("close");
										createFbFriendsList();
									}},
									{text: 'Cancel',
									"class": 'btn-custom-white2 ',
									click: function() {
										$(this).dialog("close");
										
									}}
							]
								
						});
						NewDialog.dialog('open');
				}
				else
				{
					 
					console.log(response.to);
					idList = response.to;
					var list='';
					var not_registered_ids = new Array();
					$.ajax({
						type: "POST",
						dataType: 'json',
						url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_new',
						data: 'idList=' + idList,
						success: function(response) {
							console.log(response);
							friendsStatus = response;
							$.each(idList, function(i, item)
							{
								tempID = 'fbid_' + item;
								friendStat = friendsStatus[tempID];
								tempID = 'fbid__name_' + item;
								friend_Name =friendsStatus[tempID];
								
								tempID = 'fbid_user_id' + item;
								friend_user_id =friendsStatus[tempID];
								
								
								if(friendStat == 'not_registered')
								{
									not_registered_ids.push(item);
									
								}
								else
								{
									 list +='<div style="float:left; width:100px;min-height:90px; margin: 3px 0px 3px 5px; background: #fff;padding: 3px;border: solid 0px #000000;color: #59606B;"> \
											<div class=""> \
													<div class="" > \
														<a target="_blank" href="'+GLOMP_BASE_URL+'profile/view/'+friend_user_id+'"> \
														<div  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
															<image style="float:left;z-index:1;" src="https://graph.facebook.com/' + item+ '/picture?type=large&return_ssl_results=1"   \> \
														</div> \
														<p><b style="font-size:12px;color:#333;" >' + friend_Name + '</b></p> \
														</a> \
													</div> \
											</div> \
										</div>';
								}
							});
							console.log(not_registered_ids);
							
							
							
							/*not_registered_ids*/
							/*not_registered_ids*/
							if (not_registered_ids.length == 0)
							{
								if(idList.length==1)
									var text = 'Your selected friend is already in glomp!.';
								else
									var text = 'All of the selected FB friends are already in glomp!:';
								
								var NewDialog = $('<div id="confirmPopup" align="center">\
														<div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
														<div style="max-height:250px;overflow-y:auto;float:left; border-radius:4px; border: solid #fff 1px;width:461px;padding:4px 4px">\
														'+list+'\
														</div>\
														</div>');
									NewDialog.dialog({						
										autoOpen: false,
										closeOnEscape: false ,
										resizable: false,					
										dialogClass:'dialog_style_glomp_wait noTitleStuff',
										title:'',
										modal: true,
										width:490,
										position: 'center',
										buttons: [{text: 'OK',
												"class": 'btn-custom-darkblue',
												click: function() {
													$(this).dialog("close");
												}}
										]
											
									});
									NewDialog.dialog('open');
							}
							else
							{
                                selectedCtr=0;
                                var tags='';
                                var names ='';
                                var notes ='';
                                var firstID ='';
                                $.each(not_registered_ids, function(i, item)
                                {
                                    selectedCtr++;

                                    tags += ' @[' + item + ']\n';
                                    var friendName = "";
                                    
                                    FB.api( item+ '?fields=first_name, last_name', function(response) {
                                        console.log(response);
                                        /*$.ajax({
                                          url: GLOMP_BASE_URL + 'api1/service/fb_data?param=' + response.id + '&format=json',
                                          success: function(response) {
                                            item = response.data.fbid;
                                          },
                                          async: false,
                                          dataType: 'json'
                                        });
                                        console.log(item);
                                        return;*/
                                        
                                        var country = "";
                                        var first_name = response['first_name'];
                                        var last_name = response['last_name'];
                                        friendName =  first_name + ' ' + last_name;
                                        
                                        
                                        if (firstID == "")
                                            firstID = item;
                                        if (selectedCtr <= 5) {
                                            if (names != "")
                                                names += ', ';
                                            names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
                                        }
                                        if (selectedCtr > 5) {
                                            names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

                                        }
                                        if (selectedCtr > 10)
                                        {
                                            notes = '<tr> \
                                                                <td ><hr size="1" style="padding:0px;margin:0px;"><div style="font-size:9px;width:400px;text-align:justify;padding:0px 0px 0px 3px" >' + GLOMP_FB_INVITE_REMINDER + '</div></td> \
                                                            </tr>';
                                        }
                                    
                                    
                                        if(selectedCtr == not_registered_ids.length)
                                        {
                                            if ($('#messageDialog').doesExist() == true) {
                                                $('#messageDialog').dialog('destroy').remove();
                                            }
                                            GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                                            GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
                                            GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                                            var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="10" height="10" title="Remove this message" style="float:right" onClick="removeMessage()" />'
                                            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                    <table border=0 width="399"> \
                                                        <tr> \
                                                            <td><b>To</b>:' + names + '</td> \
                                                        </tr> \
                                                        <tr> \
                                                            <td><hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:11px;" align="left"></div></td> \
                                                        </tr> \
                                                        <tr> \
                                                            <td ><textarea id="personalMessage" onKeyUp="checkPersonalMsgLength()" style="font-size:12px;resize:none;width:400px;height:50px;" placeholder="'+ $('#your_personal_message_placeholder').text() +'"></textarea></td> \
                                                        </tr> \
                                                        <tr> \
                                                            <td >' + close_button + '<div id="definedMessage"  style="font-size:12px;padding:3px;text-align:justify;width:370px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div></td> \
                                                        </tr> \
                                                        ' + notes + '\
                                                    </table> \
                                                    <p></div>');
                                            NewDialog.dialog({
                                                dialogClass: 'noTitleStuff',
                                                autoOpen: false,
                                                resizable: true,
                                                modal: true,
                                                width: 460,
                                                show: 'clip',
                                                hide: 'clip',
                                                buttons: [
                                                    {text: $('#invite_btn_search_friends').text(),
                                                        "class": 'btn-custom-blue',
                                                        click: function() {
                                                            
                                                            ga_invite_by_facebook();
                                                            var bValid = true;
                                                            $("#personalMessage").removeClass("ui-state-error");
                                                            bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 256, '#personalMessageTips');
                                                            if (bValid) {
                                                                var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                        <div align="center" style="margin-top:5px;">'+ $('#please_wait').text() +'<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                                                NewDialog.dialog({
                                                                    autoOpen: false,
                                                                    closeOnEscape: false,
                                                                    resizable: false,
                                                                    dialogClass: 'noTitleStuff',
                                                                    title: $('#please_wait').text(),
                                                                    modal: true,
                                                                    position: 'center',
                                                                    width: 200,
                                                                    height: 120
                                                                });
                                                                NewDialog.dialog('open');

                                                                personalMessage = $("#personalMessage").val();
                                                                definedMessage = $("#definedMessage").html();
                                                                definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                                                definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                                                $(this).dialog("close");
                                                                setTimeout(function() {
                                                                    $('#messageDialog').dialog('destroy').remove();
                                                                }, 500);

                                                                //var profile=GLOMP_BASE_URL+"welcome.html?from="+glomp_user_id;
                                                                //var profile ='http://glomp.it/welcome.html?from='+glomp_user_id;
                                                                //var profile ='http://glomp.it/?from='+glomp_user_id;
                                                                var profile = 'http://glomp.it/?from=' + glomp_user_id;


                                                                doGlompPost_v2(profile, personalMessage, definedMessage, 0, not_registered_ids, idList, list);

                                                            }

                                                        }},
                                                    {text: $('#cancel_btn_search_friends').text(),
                                                        "class": 'btn-custom-white2',
                                                        click: function() {
                                                            $(this).dialog("close");
                                                            setTimeout(function() {
                                                                $('#messageDialog').dialog('destroy').remove();
                                                            }, 500);
                                                        }}
                                                ]
                                            });
                                            NewDialog.dialog('open');
                                        }/*if(selectedCtr == not_registered_ids.length)*/
                                        
                                    });
                                });/*$.each(not_registered_ids, function(i, item)*/
                                
                                
                                
                                
                                
                                
                                
                                    

                                
                               
                               
                               
                               
                               
                               
								//send fb message
                                /*
								FB.ui({
								  to: not_registered_ids,
								  method: 'send',
								  link: 'https://glomp.it?fb_action_ids='+(Math.floor((Math.random() * 1000000) + 1))+'_'+(Math.floor((Math.random() * 1000000) + 1)),
								  
								},
								  function(response)
								  {
									var msg ='Message successfully sent.';
									if (response && !response.error_code)
									{
									  
										
									} else {
									  msg = 'Message not sent.'
									}
									
									var NewDialog = $('<div id="" align="center" style="padding-top:20px !important; font-size:14px">\
										<p align="center">'+msg+'</p>\
														</div>');
										NewDialog.dialog({                    
											dialogClass:'noTitleStuff',
											title: "",
											autoOpen: false,
											resizable: false,
											modal: true,
											width: 320,
											height:120,
											show: '',
											hide: '',
											buttons: [
												{text: "Okay",
												"class": 'btn-custom-darkblue width_80_per',
												click: function() {
													$(this).dialog("close");
													
													if (not_registered_ids.length != idList.length)
													{
														var text = 'Some of your selected FB friends are already in glomp!: ';
														var NewDialog = $('<div id="confirmPopup" align="center">\
																				<div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
																				<div style="max-height:250px;overflow-y:auto;float:left; border-radius:4px; border: solid #fff 1px;width:461px;padding:4px 4px">\
																				'+list+'\
																				</div>\
																				</div>');
															NewDialog.dialog({						
																autoOpen: false,
																closeOnEscape: false ,
																resizable: false,					
																dialogClass:'dialog_style_glomp_wait noTitleStuff',
																title:'',
																modal: true,
																width:490,
																position: 'center',
																buttons: [{text: 'OK',
																		"class": 'btn-custom-darkblue',
																		click: function() {
																			$(this).dialog("close");
																		}}
																]
																	
															});
														NewDialog.dialog('open');
													}
													
												}}
											]
										});
										NewDialog.dialog('open');
								  }
								);
								//send fb message*/
								
							}
							
							/*not_registered_ids*/
							/*not_registered_ids*/
						}
					});	
				}
			}
			/*if (FB_WHAT_TO_DO == 'invite')*/
			/*if (FB_WHAT_TO_DO == 'invite')*/
			/*if (FB_WHAT_TO_DO == 'invite')*/
			
		});
		/*
        FB.api('/me/friends',
        function(response)
		{
            console.log(response);
            _friend_data = response.data.sort(sortByName);
            _friend_data_global = response.data.sort(sortByName);
            count = _friend_data.length;
            clearFacebookDelay();//clear the timeout
            paginateFbFriends(global_page, global_per_page, count, _friend_data);
        }
        );
		*/
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        clearFacebookDelay();//clear the timeout
        paginateFbFriends(global_page, global_per_page, count, _friend_data);
    }



}
function doGlompPost_v2(profile, personalMessage, definedMessage, ctr, not_registered_ids,idList, list )
{
    console.log(not_registered_ids);
    var lim = 10;
    var tags = "";
    for (; ctr < selectedCtr; ) {
        //tags += ' @[' + not_registered_ids[ctr] + ']\n';
        if(tags!='')
            tags+=',';
        tags += '' + not_registered_ids[ctr] + '';
        
               
        lim--;
        ctr++;
        if (lim == 0)
            break;
    }
    console.log(ctr + "=" + tags);
    /*
     FB.api(
            'me/glomp_app:invite',
            'post',
            {
                profile: profile,
                message: '\n' + tags + '\n' + personalMessage + '\n\n' + definedMessage
            },
       
    */
    FB.api(
            '/me/feed',
			 "POST",
            {
				"message": personalMessage + '\n\n' + definedMessage,
                "tags"  :tags,
                "link" : profile
            },
   
    function(response)
    {
        if (hasOwnProperty(response, 'id')) {
            $('#confirmPopup').dialog('destroy').remove();
            if (ctr < selectedCtr) {
                doGlompPost_v2(profile, personalMessage, definedMessage, ctr, not_registered_ids);
            }
            else {
                arrSelected="";
                _selectedFriends="";
                $('#confirmPopup').dialog('destroy').remove();
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">'+ $('#invite_sent').text() +'</div> \
							<p></div>');
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 220,
                    height: 120,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: $('#Ok').text(),
                            "class": 'btn-custom-white2',
                            click: function() {
                                $('.friendThumbSelected').each(function(i, item) {
                                    if ($(this).prop('checked')) {
                                        $(this).prop('checked', false);
                                    }
                                });

                                $(this).dialog("close");
                                checkTour();
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);

                                $('#listDialog').dialog("close");
                                setTimeout(function() {
                                    $('#listDialog').dialog('destroy').remove();
                                }, 500);
                                
                                
                                
                                if (not_registered_ids.length != idList.length)
                                {
                                    var text = 'Some of your selected FB friends are already in glomp!: ';
                                    var NewDialog = $('<div id="confirmPopup" align="center">\
                                                            <div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
                                                            <div style="max-height:250px;overflow-y:auto;float:left; border-radius:4px; border: solid #fff 1px;width:461px;padding:4px 4px">\
                                                            '+list+'\
                                                            </div>\
                                                            </div>');
                                        NewDialog.dialog({						
                                            autoOpen: false,
                                            closeOnEscape: false ,
                                            resizable: false,					
                                            dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                            title:'',
                                            modal: true,
                                            width:490,
                                            position: 'center',
                                            buttons: [{text: 'OK',
                                                    "class": 'btn-custom-darkblue',
                                                    click: function() {
                                                        $(this).dialog("close");
                                                    }}
                                            ]
                                                
                                        });
                                    NewDialog.dialog('open');
                                }/*if (not_registered_ids.length != idList.length)*/
                                
                            }}
                    ]
                });
                NewDialog.dialog('open');
            }
        }
        else if (response.error) {
            arrSelected="";
            _selectedFriends="";
            $('#confirmPopup').dialog('destroy').remove();
            $('#listDialog').dialog("close");
            setTimeout(function() {
                $('#listDialog').dialog('destroy').remove();
            }, 500);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                                '+ $('#fb_error_msg').text().replace('[here]', '<a href="javascript:void(0);" onclick="reloadPage()" style="text-decoration:underline" >here</a>') +'\
                            </div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 320,
                height: 130,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#cancel_btn_search_friends').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        else {
            arrSelected="";
            _selectedFriends="";

            $('#confirmPopup').dialog('destroy').remove();
            $('#listDialog').dialog("close");
            setTimeout(function() {
                $('#listDialog').dialog('destroy').remove();
            }, 500);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">'+ $('#an_error_occured').text() +'</div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        // handle the response
        console.log(response);
        //var URL='http://facebook.com/'+response.id;
        //window.open(URL);
    }
    );

}
/* temporary until LinkedIn okay */
function returnHome() {
    return window.location = GLOMP_BASE_URL + 'user';
}

function createLiFriendsList() {
    var NewDialog = $('<div id="messageDialog" align="left"> \
        <div style="width: auto; height: auto;margin-bottom: 12px;"><div style="float: right; cursor: pointer;" onclick="returnHome();">X</div></div> \
        <p style="padding:15px 0px;">' + GLOMP_GETTING_LI_FRIENDS + '</p> \
    </div>');
    NewDialog.dialog({
        autoOpen: true,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: '',
        modal: true,
        width: 350,
        show: 'clip',
        hide: 'clip',
    });
    return;
    
    $('#friendsList').html('<div align="center" style="margin-top:50px;" id="friendsList_loading">' + GLOMP_GETTING_LI_FRIENDS + '<span style="padding:9px 15px;background-image:url(\'assets/frontend/css/images/ajax-loader.gif\');background-repeat:no-repeat;">&nbsp</span></div>');
    if (_friend_data == null)
    {
        IN.API.Connections("me")
        .fields("id","formatted-name", "email-address", "first-name", "last-name", "location:(country:(code))", "picture-urls::(original)")
        .result(function(res) {
    
            /* Removing data with private value */
//            Array.prototype.removeByValue = function(val) {
//                for(var i=0; i < this.length; i++) {
//                    if(this[i].id == val) {
//                        this.splice(i, 1);
//                    }
//                }
//            };

            //var values = removeByValue_linkedIn(res.values, 'private');
            var values = [];
            for (var i = 0; i < res.values.length; i++) {
                if (res.values[i].firstName != 'private' && res.values[i].lastName != 'private') {
                    values.push(res.values[i]);
                }
            }
            
            /*TODO: create a generic sorting function */
           _friend_data = values.sort(function(a, b){
               console.log(a);
               a.formattedName = a.firstName +' '+a.lastName;
               b.formattedName = b.firstName +' '+b.lastName;
                var x = a.formattedName.toLowerCase();
                var y = b.formattedName.toLowerCase();
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
           });
           
            _friend_data_global = values.sort(function(a, b){
                a.formattedName = a.firstName +' '+a.lastName;
                b.formattedName = b.firstName +' '+b.lastName;
                var x = a.formattedName.toLowerCase();
                var y = b.formattedName.toLowerCase();
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
           });
           
           count = _friend_data.length;
           paginateLiFriends(global_page, global_per_page, count, _friend_data);
        });
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        paginateLiFriends(global_page, global_per_page, count, _friend_data);
    }
}

function loadPage() {
    if (_friend_data == null) {
        FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
            _friend_data = response.data.sort(sortByName);
            _friend_data_global = _friend_data;
            count = _friend_data.length;
            paginateFbFriends(global_page, global_per_page, count, _friend_data);
        });
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        paginateFbFriends(global_page, global_per_page, count, _friend_data);
    }
}
function liloadPage() {
    if (_friend_data == null) {
        IN.API.Connections("me")
        .fields("id","formatted-name", "email-address", "first-name", "last-name", "location:(country:(code))", "picture-urls::(original)")
        .result(function(res) {
            res.values.removeByValue('private');
            
            /*TODO: create a generic sorting function */
           _friend_data = res.values.sort(function(a, b){
               console.log('liLoadPage_friend_data');
               console.log(a);
                var x = a.formattedName.toLowerCase();
                var y = b.formattedName.toLowerCase();
                
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
           });
           
            _friend_data_global = _friend_data;
           count = _friend_data.length;
           paginateLiFriends(global_page, global_per_page, count, _friend_data);
        });
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        paginateLiFriends(global_page, global_per_page, count, _friend_data);
    }
}
function paginateFbFriends(page, limit, max, data) {
    max = Math.ceil(max / limit);
    if (global_page <= 1) {
        $("#page_prev").prop("disabled", true);
    }
    else {
        $("#page_prev").prop("disabled", false);
    }
    if (global_page >= max) {
        $("#page_next").prop("disabled", true);
    }
    else {
        $("#page_next").prop("disabled", false);
    }

    start_here = (page - 1) * limit;
    stop_here = start_here + limit;
    hasList = false;
    idList = new Array();
    ctr = 0;
    $.each(data, function(i, item) {
        if (ctr >= start_here && stop_here > ctr) {
            idList.push(item.id);
            hasList = true;
        }
        ctr++;
    });
    if (hasList)
    {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus',
            data: 'idList=' + idList,
            success: function(response) {
                friendsStatus = response;
                $('#friendsList').html('');
                $('#friendsList').addClass('content2');
                //(response.fbid_24102928);								
                ctr = 0;
                $.each(data, function(i, item) {

                    if (ctr >= start_here && stop_here > ctr) {
                        setTimeout(function() {
                            tempID = 'fbid_' + item.id;
                            friendStat = friendsStatus[tempID];
                            addToList(item, friendStat, function(status) {
                                if (status == 'OK') {
                                    //do stuff...
                                }
                            });
                        }, i * 0);
                    }
                    ctr++;
                });
                setTimeout(function() {
                    setCheckSelecAllValue();
                }, ctr * 1);
            }
        });
    }
    else {
        afterCreateFbFriendsList();

    }
}

function paginateLiFriends(page, limit, max, data) {
    max = Math.ceil(max / limit);
    if (global_page <= 1) {
        $("#li_page_prev").prop("disabled", true);
    }
    else {
        $("#li_page_prev").prop("disabled", false);
    }
    if (global_page >= max) {
        $("#li_page_next").prop("disabled", true);
    }
    else {
        $("#li_page_next").prop("disabled", false);
    }

    start_here = (page - 1) * limit;
    stop_here = start_here + limit;
    hasList = false;
    idList = new Array();
    ctr = 0;
    $.each(data, function(i, item) {
        if (ctr >= start_here && stop_here > ctr) {
            idList.push(item.id);
            hasList = true;
        }
        ctr++;
    });
    if (hasList)
    {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_2',
            data: {
              idList: idList,  
              field: 'user_linkedin_id',  
            },
            success: function(response) {
                friendsStatus = response;
                $('#friendsList').html('');
                $('#friendsList').addClass('content2');
                //(response.fbid_24102928);								
                ctr = 0;
                $.each(data, function(i, item) {
                    if (ctr >= start_here && stop_here > ctr) {
                        setTimeout(function() {
                            tempID = 'linkedInId_' + item.id;
                            friendStat = friendsStatus[tempID];
                            addToListLi(item, friendStat, function(status) {
                                if (status == 'OK') {
                                    //do stuff...
                                }
                            });
                        }, i * 0);
                    }
                    ctr++;
                });
                setTimeout(function() {
                    setCheckSelecAllValueLi();
                }, ctr * 0);
            }
        });
    }
    else {
        afterCreateFbFriendsList();
    }
}

function afterCreateFbFriendsList() {
    $elem = $('<div id="search_not_found"  style="display:; clear:both;" align="center"> \
        '+ $('#No_result_found').text() +'... \
    </div>');
    $('#friendsList').append($elem);
}
function doGlomp(_this) {

    $("#glomp_form").dialog('destroy').remove();
    if ($('#merchantDialog').doesExist() == true) {
        $("#glomp_form").dialog('destroy').remove();
    }
    $("#showError").hide();
    $("#showSuccess").hide();
    var img = _this.data('image');
    var point = _this.data('point');
    var item = _this.data('productname');
    var merchant = _this.data('merchantname');
    var id = _this.data('id');
    var NewDialog = $('<form name="glomp_form" id="glomp_form" method="post" style=""><div id="" align="left">\
										<input type="hidden" name="user_fname"  id="" value="' + global_first_name + '" />\
										<input type="hidden" name="user_lname"  id="" value="' + global_last_name + '" />\
										 <input type="hidden" name="user_fbID"  id="" value="' + global_fbID + '"/>\
										 <input type="hidden" name="location_id"  id="" value="' + global_Country + '"/>\
										 <input type="hidden" name="product_id"  id="product_id" value="' + id + '" />\
										 <div class="text-center"><img src="' + GLOMP_BASE_URL + 'assets/frontend/img/logo-small.png" alt="Glomp Logo" /></div>\
										<div class="row-fluid">\
											<div class="span12">\
												<div class="alert alert-success"  id="showSuccess" style="display:none;font-size:12px !important;">    \
													<span id="showSuccessMessage"></span>\
												</div>\
												<div class="alert alert-error"  id="showError" style="display:none;font-size:12px !important;">  \
													<span id="showErrorMessage"></span>\
												</div>\
											<div class="span3">\
												<span class="label label-important" id="popUpPoints">' + point + '</span>\
												<img id="pipup_image" src="' + img + '" style="border-radius:4px" >\
											</div>\
											<div class="span1">&nbsp;</div>\
											<div class="span8">\
												<div class="itemname">\
													<div id="popUpMerchant">' + merchant + '</div><br />\
													<div id="popUpItem">' + item + '</div> <br />\
												</div>\
											</div>\
											<div class="cl">\
                                                <div class="p2px_5px_0px_5px" align="left" style="padding:8px 0px 0px 0px">\
												<div class="">\
                                                        <div id="personalMessageTips" style="color:#F00;padding:2px;font-size:11px;" align="left"></div>\
														<textarea id="personalMessage" onKeyUp="checkPersonalMsgLength()" style="font-size:12px;resize:none;width:430px;height:50px;" placeholder="'+ $('#your_personal_message_placeholder').text() +'"></textarea>\
													</div>\
													<div class="p2px_5px_0px_5px" align="left" style="padding:8px 0px 0px 0px">\
														<input type="password" name="password"  id="glomp_password" style="text-align:center;font-size:12px !important;" class="span12" placeholder="'+ $('#Password').text() +'">\
													</div>\
													<div class="p8px_5px_0px_5px" align="center" style="padding:8px 0px 0px 0px">\
														<a href="' + GLOMP_BASE_URL + '/landing/forgotPassword/" class="forgot_pass white">'+ $('#Forgot_Password').text() +'?</a>\
													</div>\
												</div>\
											</div>\
										</div></form>');


    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: false,
        title: '',
        modal: true,
        width: 460,
        height: 420,
        position: 'center',
        buttons: [
            {text: $('#notice_facebook_btn_2').text(),
                "class": 'btn-custom-darkblue',
                click: function() {
                    var NewDialog = $('<div id="waitPopup" align="center">\
												<div align="center" style="margin-top:5px;">' + $('#please_wait').text() +'<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'noTitleStuff',
                        title: $('#please_wait').text(),
                        modal: true,
                        position: 'center',
                        width: 200,
                        height: 120
                    });
                    NewDialog.dialog('open');
                    var personalMessage =  $('#personalMessage').val();
                    $.ajax({
                        type: "POST",
                        url: GLOMP_BASE_URL + "profile/glompToUserFacebook/",
                        data: $('#glomp_form').serialize(),
                        success: function(data) {
                            var d = eval("(" + data + ")");
                            if (d.status == 'success')
                            {
                                ga_glomp_on_fb();
                                $("#glomp_form").dialog('destroy').remove();
                                $("#merchantDialog").dialog('destroy').remove();
                                
                                fbSendGlomp(d.voucher_id, personalMessage);

                            }
                            else if (d.status == 'error')
                            {
                                $('#glomp_password').val('');
                                $("#waitPopup").dialog('destroy').remove();
                                $("#showError").show();
                                $("#showErrorMessage").text(d.msg);
                            }
                        }
                    });
                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $("#glomp_form").dialog('destroy').remove();
                }}
        ]

    });
    NewDialog.dialog("open");

}
function doGlompLi(_this) {

    if ($('#merchantDialog').doesExist() == true) {
        $("#glomp_form").dialog('destroy').remove();
    }
    $("#showError").hide();
    $("#showSuccess").hide();
    var img = _this.data('image');
    var point = _this.data('point');
    var item = _this.data('productname');
    var merchant = _this.data('merchantname');
    var id = _this.data('id');
    var NewDialog = $('<form name="glomp_form" id="glomp_form" method="post" style=""><div id="" align="left">\
        <input type="hidden" name="user_fname"  id="" value="' + global_first_name + '" />\
        <input type="hidden" name="user_lname"  id="" value="' + global_last_name + '" />\
         <input type="hidden" name="user_linkedInID"  id="" value="' + global_fbID + '"/>\
         <input type="hidden" name="location_id"  id="" value="' + global_Country + '"/>\
         <input type="hidden" name="product_id"  id="product_id" value="' + id + '" />\
         <div class="text-center"><img src="' + GLOMP_BASE_URL + 'assets/frontend/img/logo-small.png" alt="Glomp Logo" /></div>\
        <div class="row-fluid">\
                <div class="span12">\
                        <div class="alert alert-success"  id="showSuccess" style="display:none;font-size:12px !important;">    \
                                <span id="showSuccessMessage"></span>\
                        </div>\
                        <div class="alert alert-error"  id="showError" style="display:none;font-size:12px !important;">  \
                                <span id="showErrorMessage"></span>\
                        </div>\
                <div class="span3">\
                        <span class="label label-important" id="popUpPoints">' + point + '</span>\
                        <img id="pipup_image" src="' + img + '" style="border-radius:4px" >\
                </div>\
                <div class="span1">&nbsp;</div>\
                <div class="span8">\
                        <div class="itemname">\
                                <div id="popUpMerchant">' + merchant + '</div><br />\
                                <div id="popUpItem">' + item + '</div> <br />\
                        </div>\
                </div>\
                <div class="cl">\
                        <div class="">\
                                <br /> \
                                <div class="p8px_5px_0px_5px" align="left">\
                                        <textarea name="message" id="glomp_message" style="text-align: center; font-size: 12px !important; margin: 0px; width: 426px; height: 40px;" id="glomp_message" class="glomp_input" placeholder="'+ $('#Message').text() +'"></textarea>\
                                </div>\
                                <div class="p2px_5px_0px_5px" align="left" style="padding:8px 0px 0px 0px">\
                                        <input type="password" name="password"  id="glomp_password" style="text-align:center;font-size:12px !important;" class="span12" placeholder="'+ $('#Password').text() +'">\
                                </div>\
                                <div class="p8px_5px_0px_5px" align="center" style="padding:8px 0px 0px 0px">\
                                        <a href="' + GLOMP_BASE_URL + '/landing/forgotPassword/" class="forgot_pass white">'+ $('#Forgot_Password').text() +'?</a>\
                                </div>\
                        </div>\
                </div>\
        </div></form>');


    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: false,
        title: '',
        modal: true,
        width: 460,
        position: 'center',
        buttons: [
            {text: $('#notice_facebook_btn_2').text(),
                "class": 'btn-custom-darkblue',
                click: function() {
                    var contents = {
                        merchant: merchant,
                        item: item,
                        img: img
                    };
                    _doSubmitLi(false, contents);
                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $("#glomp_form").dialog('destroy').remove();
                }}
        ]

    });
    NewDialog.dialog("open");

}
function _doSubmitLi(override, contents) {
    var override = override || false;
    var merchant = contents.merchant;
    var item = contents.item;
    var img = contents.img;
    
    if ($('#glomp_message').val() =='' && override == false) {
        var NewDialog = $('<div id="" align="center" style="padding-top:25px !important;">\
            '+ $('#glomp_confirmation_message').text() +'\
        </div>');
        
        NewDialog.dialog({                    
            dialogClass:'noTitleStuff dialog_style_blue_grey',
            title: "",
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 320,
            height: 140,
            show: '',
            hide: '',
            buttons: [{text: $('#Add_message').text(),
                    "class": 'btn-custom-darkblue',
                    click: function() {
                        $(this).dialog("close");
                        $('#glomp_message').focus();
                    }},
                {text: $('#No_thanks').text(),
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                        var contents = {
                            merchant: merchant,
                            item: item,
                            img: img
                        };
                        _doSubmitLi(true, contents);
                    }}
            ]
        });
        NewDialog.dialog('open');
        return;
    }
    
    var NewDialog = $('<div id="waitPopup" align="center">\
        <div align="center" style="margin-top:5px;">' + $('#please_wait').text() + '<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
    NewDialog.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: $('#please_wait').text(),
        modal: true,
        position: 'center',
        width: 200,
        height: 120
    });
    NewDialog.dialog('open');
    $.ajax({
        type: "POST",
        url: GLOMP_BASE_URL + "profile/glompToUserLinkedIn/",
        data: $('#glomp_form').serialize(),
        success: function(data) {
            var d = eval("(" + data + ")");
            if (d.status == 'success')
            {
                var message = $('#glomp_message').val();
                $("#waitPopup").dialog('destroy').remove();
                $("#glomp_form").dialog('destroy').remove();
                $("#merchantDialog").dialog('destroy').remove();

                var comment = "shared a glomp!ed story - I glomp!ed "+ global_first_name+" " +global_last_name+ " to a "+ merchant + " "+ item +" on glomp!.";
                var submitted_url_1 = GLOMP_BASE_URL + 'glomp_story/linkedIn?voucher=' + d.voucher_id;
                var submitted_url_2 = GLOMP_BASE_URL + 'glomp_story/linkedIn?voucher=' + d.voucher_id + '&linkedInID=' + global_fbID;

                IN.API.Raw("people/~/shares/")
                .method("POST")
                .body(JSON.stringify({
                        "comment": comment,
                        "content": {
                            "submitted-url": submitted_url_1,
                            "title": 'glomp!', 
                            "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
                            "submitted-image-url": img
                        },
                        "visibility": {"code": "anyone"}
                    })
                );

                IN.API.Raw("people/~/mailbox")
                .method("POST")
                .body(JSON.stringify({
                    "recipients": {
                      "values": [
                      {
                        "person": {
                          "_path": "/people/" + global_fbID,
                         }
                      }]
                    },
                    "subject": comment,
                    "body": message + '\n\n' +comment + '\n\n Click link to view: \n\n' + submitted_url_2
                  })
                ).result(function(result){
                        postFbMessageCallback()
                });

            }
            else if (d.status == 'error')
            {
                $('#glomp_password').val('');
                $("#waitPopup").dialog('destroy').remove();
                $("#showError").show();
                $("#showErrorMessage").text(d.msg);
            }
        }
    });
}
function fbSendGlomp(voucher_id, personalMessage)
   {
        var profile =GLOMP_BASE_URL + '/glomp_story/?voucher=' + voucher_id + '&fbID=' + global_fbID;
        FB.api(
            '/me/feed',
			 "POST",
            {
				"message": personalMessage,
                "tags"  :global_fbID,
                "link" : profile,
                "privacy" : {
                    'value' : 'SELF'
                }
            },postFbMessageCallback);
/*
    console.log('http://glomp.it/glomp_story/?voucher=' + voucher_id + '&fbID=' + global_fbID);
    FB.ui({
        method: 'send',
        to: global_fbID,
        message: 'message',
        link: GLOMP_BASE_URL + '/glomp_story/?voucher=' + voucher_id + '&fbID=' + global_fbID
    }, postFbMessageCallback);
    */
}
function  postFbMessageCallback(response) {
    $("#waitPopup").dialog('destroy').remove();
    var NewDialog = $('<div id="confirmPopup" align="center">\
									<div align="center" style="margin-top:-5px;font-size:12px !important;">'+ $('#Successfully_Glomped').text() +'</div></div>');
    NewDialog.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: '',
        modal: true,
        width: 200,
        position: 'center',
        height: 120,
        buttons: [
            {text: $('#Ok').text(),
                "class": 'btn-custom-green2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#confirmPopup').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
            (!(prop in proto) || proto[prop] !== obj[prop]);
}

function askFbOption() {
    var NewDialog = $('<div id="messageDialog" align="center"> \
										<p style="padding:15px 0px;">' + GLOMP_ASK_OPTION + '</p> \
									</div>');
    NewDialog.dialog({
        autoOpen: false,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: GLOMP_INVITE_REQUEST_SENT_TITLE,
        modal: true,
        width: 280,
        height: 160,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "glomp!",
                "class": 'btn-custom-green2-1 m_left8px',
                click: function() {
                    $('#fb_invite_button_wrapper').hide();
                    $('#fb_paginator_wrapper').show();
                    FB_WHAT_TO_DO = 'glomp';
                    checkLoginStatus();
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }},
            {text: $('#invite_btn_search_friends').text(),
                "class": 'btn-custom-darkblue',
                click: function() {
                    $('#fb_invite_button_wrapper').show();
                    $('#fb_paginator_wrapper').show();
                    FB_WHAT_TO_DO = 'invite';
                    checkLoginStatus();
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);

                }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-transparent-green',
                click: function() {
                    window.location = GLOMP_BASE_URL + 'user/searchFriends/';
                    $(this).dialog("close");
            }}
        ]
    });
    NewDialog.dialog('open');
}

function askLinkedInOption() {
    try
    {
      /* Do nothing */
      IN.User.isAuthorized();
    }
    catch(err)
    {
        /* Please add to language file */
          setTimeout(function() {
                askLinkedInOption();
           }, 5000);
          return;
    }
    
    var NewDialog = $('<div id="messageDialog" align="center"> \
                        <p style="padding:15px 0px;">' + GLOMP_ASK_OPTION + '</p> \
                    </div>');
    NewDialog.dialog({
        autoOpen: false,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: GLOMP_INVITE_REQUEST_SENT_TITLE,
        modal: true,
        width: 280,
        height: 160,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "glomp!",
                "class": 'btn-custom-green2-1 m_left8px',
                click: function() {
                    $('#linkedIn_invite_button_wrapper').hide();
                    $('#linkedIn_paginator_wrapper').show();
                    FB_WHAT_TO_DO = 'glomp';
                    IN.User.authorize(function(){
                        createLiFriendsList();
                    });
                    $(this).dialog("close");
                    setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                    }, 500);
            }},
            {text: $('#invite_btn_search_friends').text(),
                "class": 'btn-custom-darkblue',
                click: function() {
                    $('#li_invite_button_wrapper').show();
                    $('#linkedIn_paginator_wrapper').show();
                    FB_WHAT_TO_DO = 'invite';
                    IN.User.authorize(function(){
                        createLiFriendsList();
                    });
                    $(this).dialog("close");
                    setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                    }, 500);
            }},
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-transparent-green',
                click: function() {
                    window.location = GLOMP_BASE_URL + 'user/searchFriends/';
                    $(this).dialog("close");
            }}
        ]
    });
    NewDialog.dialog('open');
}
function checkLoginStatus()
{
	NewDialog_Loader.dialog('open');
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML	  });		
    });
    FB.getLoginStatus(function(response){
	console.log(response);
	
		/*
 FB.api(
            'me/glomp_app:share',
            'post',
            {
                story: "https://glomp.it/glomp_story/share/5db85ee9-350d-11e5-ba24-02c64cce4ed5",
                message: '@[1834278807] \n test glomp!ed'
                
            },function(response)
            {
                console.log(response);
            });
            
        /*  
             FB.api(
            'me/glomp_app:invite',
            'post',
            {
                profile: "https://glomp.it",
                message: '@[100008254384307] \n test'
            });
            
            FB.ui({
              method: 'send',
              to:'100008254384307',
              link: "https://glomp.it"
            });
            return;
            /**
            FB.api(
                    '/me/feed',
                     "POST",
                    {
                        "message": 'Test Auto Feed',
                        "tags"  :'100008254384307',
                        "privacy" :{
                                "value" : "SELF"
                            },
                        "link" : "https://glomp.it"
                    },
           
            function(response)
            {
                console.log(response);
            });
            /**/        
		
	
        if (response && response.status == 'connected') {
            checkPermissions();
            //createFbFriendsList();
            //sendRequestToRecipients();
        } else {
			NewDialog_Loader.dialog('close');
            FB.login(
                    function(response)
                    {
                        if (response.status === 'connected') {
                            //auto link here
                            autoLink();
                            //createFbFriendsList();
                            //sendRequestToRecipients();
                        }
                    }, {scope: scopes}
            );
            // Display the login button          
        }
    }
    );
}

var permsNeeded = ['publish_actions', 'email'];

function autoLink() {
    FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
        var fetch_fb_id = response.id;

        $.ajax({
            type: "POST",
            url: 'ajax_post/auto_link_fb',
            data: {
                fetch_fb_id: fetch_fb_id
            },
            async: false,
            dataType: 'json',
            success: function(response) {
                if (response.success == 'false') {
                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px !important;"> \
                    '+ $('#notice_facebook_2').text() +'</div>');
                    
                    NewDialog.dialog({
                        dialogClass: 'noTitleStuff',
                        autoOpen: false,
                        resizable: false,
                        closeOnEscape: false,
                        modal: true,
                        width: 450,
                        height: 140,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: $('#login_using_diff_fb_account_btn').text(),
                                "class": 'btn-custom-darkblue',
                                click: function() {
                                    FB.logout(function(response) {
                                       return checkLoginStatus();
                                    });
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                }
                            },
                            {text: $('#cancel_btn_search_friends').text(),
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $(this).dialog("close");
                                    window.location = GLOMP_BASE_URL + 'user/searchFriends/';
                                }
                            }
                        ]
                    });
                    
                    return NewDialog.dialog('open');
                }
                
                return checkPermissions();
            }
        });
    });
}
function checkFBMatch()
{
    FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
        var fetch_fb_id = response.id;
			console.log(response);
        $.ajax({
            type: "POST",
            url: 'ajax_post/check_fb_match',
            data: {
                fetch_fb_id: fetch_fb_id
            },
            async: false,
            dataType: 'json',
            success: function(response) {
			
                if (response.check_fb_id == 'prompt')
				{
					NewDialog_Loader.dialog('close');
                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px !important;">\
                       '+ $('#havent_link_fb_msg').text().replace(new RegExp('%newline%', 'g'), '<br>') +'</div>');

                    NewDialog.dialog({
                        dialogClass: 'noTitleStuff',
                        autoOpen: false,
                        resizable: false,
                        closeOnEscape: false,
                        modal: true,
                        height: 140,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: $('#Yes').text(),
                                "class": 'btn-custom-darkblue',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    autoLink();
                                }
                            },
                            {text: $('#No').text(),
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $(this).dialog("close");
                                    window.location = GLOMP_BASE_URL + 'user/searchFriends/';
                                }
                            }
                        ]
                    });
                    return NewDialog.dialog('open');
                    //dialog box here
                }

                if (response.check_fb_id == 'true') {
                    return createFbFriendsList();
                }

                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px !important;"> ' + $('#notice_facebook_1').text() + '</div>');
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 600,
                    height: 140,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: $('#notice_facebook_btn_1').text(),
                            "class": 'btn-custom-darkblue',
                            click: function() {
                                FB.logout(function(response) {
                                   return checkLoginStatus();
                                });
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);
                            }
                        },
                        {text: $('#notice_facebook_btn_2').text(),
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);
                                return createFbFriendsList();
                            }
                        }
                    ]
                });
                NewDialog.dialog('open');
                //dialog box here
            }
        });
    });
}

function checkPermissions()
{
	
    FB.api('/me/permissions', function(response)
	{
		
		var permsArray = response.data;
		var permsToPrompt = [];
		for (var i in permsNeeded) 
		{
			var found =false;
			for (index = 0; index < permsArray.length; index++) 
			{
					if( permsNeeded[i] == permsArray[index].permission  && permsArray[index].status =="granted"  )
					{
							found = true;
					}
			}
			
		  if (!found) {
			permsToPrompt.push(permsNeeded[i]);
		  }
		}
			
        
        if (permsToPrompt.length > 0)
		{
			NewDialog_Loader.dialog('close');
            //alert('Need to re-prompt user for permissions: ' + permsToPrompt.join(','));
            //promptForPerms(permsToPrompt);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                            In order to invite or <b>glomp!</b> your Facebook friends, please click \
                            <a href="javascript:void(0);" onclick="promptForPerms()" style="text-decoration:underline" >here</a> and in the Facebook pop-up, click \'Okay\' so that that we can post to your Facebook. Thank you!\
                            </div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 450,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#cancel_btn_search_friends').text(),
                        "class": 'btn-custom-white2',
                        click: function() {

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);

                        }}
                ]
            });
            NewDialog.dialog('open');

        } else {
            checkFBMatch();
            return;
        }
    });

}
function promptForPerms()
{
    $('#messageDialog').dialog("close");
    setTimeout(function() {
        $('#messageDialog').dialog('destroy').remove();
    }, 500);
    FB.login(function(response) {
        if (response.status === 'connected') {
            checkPermissions();
        }
    }, {scope: scopes});
}
function inviteThisFriend(id) {
    FB.ui({
        method: 'send',
        to: [id],
        message: 'message',
        link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/'
    });
}
function addThisFriendFB(fbID) {
    var data = '&fbID=' + fbID;
    $.ajax({
        type: "POST",
        url: 'ajax_post/addThisFriend',
        data: data,
        dataType: 'json',
        success: function(response) {
            $('#fb_plus_' + fbID).hide();
            //var url=GLOMP_BASE_URL+'profile/view/'+response.status;
            //$("#fb_link_"+id).attr("href", url);				

            //alert(response.status);
            var NewDialog = $('<div id="messageDialog" align="center"> \
												<p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '</p> \
											</div>');
            NewDialog.dialog({
                autoOpen: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                modal: true,
                width: 280,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    });
}
function addThisFriendLi(ID) {
    var data = '&linkedInId=' + ID;
    $.ajax({
        type: "POST",
        url: 'ajax_post/addThisFriend',
        data: data,
        dataType: 'json',
        success: function(response) {
            $('#fb_plus_' + ID).hide();
            //var url=GLOMP_BASE_URL+'profile/view/'+response.status;
            //$("#fb_link_"+id).attr("href", url);				

            //alert(response.status);
            var NewDialog = $('<div id="messageDialog" align="center"> \
                    <p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '</p> \
            </div>');
            NewDialog.dialog({
                autoOpen: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                modal: true,
                width: 280,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    });
}
function addThisFriend(id) {
    var data = '&id=' + id;
    $.ajax({
        type: "POST",
        url: 'ajax_post/addThisFriendNotFB',
        data: data,
        dataType: 'json',
        success: function(response) {
            $('#add_' + id).hide();
            //var url=GLOMP_BASE_URL+'profile/view/'+response.status;
            //$("#fb_link_"+id).attr("href", url);				

            //alert(response.status);
            var NewDialog = $('<div id="messageDialog" align="center"> \
												<p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '</p> \
											</div>');
            NewDialog.dialog({
                autoOpen: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                modal: true,
                width: 280,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: $('#Ok').text(),
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    });
}
function checkTour()
{
    if (inTour == 'yes') {
        var NewDialog = $('<div id="tourDialog" align="center"> \
										<p style="padding:15px 0px;font-weight:bold !important;">'+ $('#add_more_friends').text() +'</p> \
									</div>');
        NewDialog.dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'noTitleStuff',
            title: '',
            modal: true,
            width: 320,
            height: 140,
            show: 'clip',
            hide: 'clip',
            buttons: [
                {text: $('#add_more').text() + '!',
                    "class": 'btn-custom-darkblue ',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#tourDialog').dialog('destroy').remove();
                        }, 500);
                    }},
                {text: $('#Return_to_Tour').text(),
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#tourDialog').dialog('destroy').remove();
                            window.location.href = GLOMP_BASE_URL + 'user/dashboard/?inTour=2';
                        }, 500);

                    }}
            ]
        });
        NewDialog.dialog('open');

    }
}
function sample() {
    FB.api(
            {
                method: 'fql.query',
                query: 'SELECT uid, first_name, last_name,current_location  FROM user WHERE uid = 1123352461'
            },
    function(data) {
        console.log(data);
        //    do something with the response
    }
    );
}
function showGlompMenu(id, country, first_name, last_name) {
    global_fbID = id;
    global_Country = country;
    global_first_name = first_name;
    global_last_name = last_name;
    var name = first_name + ' '+ last_name; /*$('#search_name_' + id).html();*/
	
    $("#waitDialog").dialog("close");
    $("#waitDialog").dialog('destroy').remove();
    if ($('#merchantDialog').doesExist() == true) {
        $("#merchantDialog").dialog('destroy').remove();
    }

    var NewDialog = $('<div id="merchantDialog" align="left" style="font-size:12px">\
        <div align="center" style="margin-top:5px;">' + $('#please_wait').text() +'<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
    </div>');
    NewDialog.dialog({
        dialogClass: 'dialog_style_blue_dark',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 710,
        show: '',
        hide: '',
        buttons: [
            {text: $('#cancel_btn_search_friends').text(),
                "class": 'btn-custom-white2 fr',
                click: function() {
                    $('#merchantDialog').dialog("close");
                    $('#merchantDialog').dialog('destroy').remove();
                }}
        ]
    });
    NewDialog.dialog('open');

    //call the menu
    var data = "";
    data += "&fbID=" + id;
    data += "&country=" + country;
    data += "&name=" + name;

    $.ajax({
        type: "POST",
        url: 'ajax_post/getGlompFBFrame',
        data: data,
        success: function(response) {
            $("#merchantDialog").html(response);
            $(".tabs").hide();
            $("#glomped_inactive").hide();

            $(".tabs:first").show();
            $(".profileTab a").click(function(e) {
                var activeTab = $(this).attr('href');
                if (activeTab == "#tabBuz")
                {
                    $("#glomped_active").show();
                    $("#glomped_inactive").hide();
                }
                else
                {
                    $("#glomped_inactive").show();
                    $("#glomped_active").hide();
                }
                e.preventDefault();
                $(".profileTab a").removeClass("activeTab");
                $(this).addClass("activeTab");
                $(".tabs").hide();
                $(activeTab).fadeIn();
            });
            $('.nameOfMerchant').click(function(e) {
                e.preventDefault();
                var _anchor = $(this).attr('href');
                $('#merchantProductDetails').load(_anchor, function() {
                    $('#tabMerchant').hide();
                    $('#merchantProductDetails').fadeIn();
                });
            });

            $('.glompItem').click(function() {
                console.log('here');
                console.log(linkedIn);
                if (linkedIn === true) {
                  doGlompLi($(this));
                  return;
                } else {
                    doGlomp($(this));
                }
            });
        }
    });
    //call the menu
}
function invitationSent(msg) {
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
					<div id="personalMessageTips" style="padding:2px;font-size:14px;">' + msg + '</div> \
					<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_blue_grey',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 260,
        height: 140,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: $('#Ok').text(),
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);

                    $('#listDialog').dialog("close");
                    setTimeout(function() {
                        $('#listDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function removeByValue(arr, val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == val) {
            arr.splice(i, 1);
            break;
        }
    }
}
function removeByValue_linkedIn(arr, val) {
    for (var i = 0; i < arr.length; i++) {
        if (hasOwnProperty(arr[i], 'firstName')) {
            if (arr[i].firstName == val) {
                arr.splice(i, 1);
            }
        }
    }
    return arr;
}
function checkIfThisHasBeenChecked(arr, val) {
    var hasChecked = false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == val) {
            hasChecked = true;
            break;
        }
    }
    return hasChecked;
}
$.fn.doesExist = function() {
    return jQuery(this).length > 0;
};	