var FB_WHAT_TO_DO = "";
var max_limit = 3000;
var global_fbID = "";
var global_Country = "";
var global_first_name = "";
var global_last_name = "";
var arrSelected = "";
var selectedCtr = 0;

var _friend_data = null;
var _friend_data_global = null;
var global_page = 1;
var global_per_page = 32;
$(document).ready(function(e) {
    //invitationSent();
    $("#page_prev").click(function() {
        global_page--;
        loadPage();
    });
    $("#page_next").click(function() {
        global_page++;
        loadPage();
    });
    $('#Invite_Through_Fb').click(function() {
        askFbOption();
        //getPersonalMessage();
        //sample();
    });
    //searchFriends();
    //checkLoginStatus();

    $('#searchTextField').keyup(function(ev) {
        getKey();
    });
    $('#searchTextField').keydown(function(ev) {
        if (FB_WHAT_TO_DO != "" && ev.keyCode == 13) {
            return false;
        }
        else {
            return true;
        }
    })

    $("div").scroll(function() {
        var $this = $(this);
        var height = this.scrollHeight - $this.height(); // Get the height of the div
        var scroll = $this.scrollTop(); // Get the vertical scroll position

        var isScrolledToEnd = (scroll >= height);

        //   $(".scroll-pos").text(scroll);
        //    $(".scroll-height").text(height);

        if (isScrolledToEnd) {
            var additionalContent = loadMoreItems(); // Get the additional content

            $this.append(additionalContent); // Append the additional content

        }
    });

    $("#chk_select_all").click(function() {
        $(".friendThumbSelected").prop('checked', ($("#chk_select_all").prop('checked')));
    });

    $(".addThisFriendClass").click(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        //alert($(this).prop("id"));
        var id = $(this).prop("id");
        var name = $('#data_name_' + id).html();
        var loc = $('#data_loc_' + id).html();
        var desc = "Add " + name + " to your glomp! network?";
        var img = $('#data_img_' + id).prop('src');
        var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
									<div class="friendThumb3" > \
										<div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
											<image id="fb_friend_popup_img" style="float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
										</div> \
									</div>\
									<div class="friendThumb3_name" > \
										<b>' + name + '</b><br /><span style="">' + loc + '</span>\
									</div>\
									<div class="friendThumb3_desc"  >' + desc + ' \
									</div> \
								</div>');
        NewDialog.dialog({
            dialogClass: 'noTitleStuff',
            dialogClass:'dialog_style_blue_grey',
                    title: "",
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 300,
            height: 250,
            show: 'clip',
            hide: 'clip',
            buttons: [{text: 'Confirm',
                    "class": 'btn-custom-darkblue',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#FriendPopupDialog').dialog('destroy').remove();
                            addThisFriend(id);
                        }, 500);
                    }},
                {text: "Cancel",
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#FriendPopupDialog').dialog('destroy').remove();
                        }, 500);
                    }}
            ]
        });
        NewDialog.dialog('open');
    });


    $("#invite_fb_friends").click(function() {
        // check if some friends are selected
        var j = 0;
        var selectedFriends = [];
        $('.friendThumbSelected').each(function(i, item) {
            if ($(this).prop('checked')) {
                var friendID = $(this).val();
                selectedFriends.push(friendID);
                j++;
            }
        });
        if (j > 0) {
            doInviteFbFriends(selectedFriends);
            return false;
        }
        else {
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
					<div id="personalMessageTips" style="padding:2px;font-size:14px;">No friends selected.</div> \
					<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);

                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        // check if some friends are selected


        /*	$(this).submit(function() {
         return false;
         });
         return true;*/
    });

    window.fbAsyncInit = function() {
        FB.init({
            appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML	  });		
        });
    };

});
function sortByName(a, b) {
    var x = a.name.toLowerCase();
    var y = b.name.toLowerCase();
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

//var _friend_data = null
function lazy_load_friend_data(uid) {
    if (_friend_data == null) {
        FB.api('/me/friends', function(response) {
            _friend_data = response.data.sort(sortByName);
        }
        )
    }
}
function removeMessage() {
    $('#definedMessage').html('');
    $('#messageDialog').dialog({
        width: 460,
        height: 280,
    });
}
function doInviteFbFriends(selectedFriends) {
    console.log(selectedFriends);
    var names = "";
    selectedCtr = 0;
    var firstID = "";
    var tags = "";
    arrSelected = new Array();
    var notes = "";
    $.each(selectedFriends, function(i, item) {
        selectedCtr++;

        tags += ' @[' + item + ']\n';
        arrSelected.push(item);
        var friendName = $("#search_name_" + item).html();
        if (firstID == "")
            firstID = item;
        if (selectedCtr <= 5) {
            if (names != "")
                names += ', ';
            names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
        }

    });
    console.log(arrSelected);
    if (selectedCtr > 5) {
        names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

    }
    if (selectedCtr > 10)
    {
        notes = '<tr> \
							<td ><hr size="1" style="padding:0px;margin:0px;"><div style="font-size:9px;width:400px;text-align:justify;padding:0px 0px 0px 3px" >' + GLOMP_FB_INVITE_REMINDER + '</div></td> \
						</tr>';
    }
    if ($('#messageDialog').doesExist() == true) {
        $('#messageDialog').dialog('destroy').remove();
    }

    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
    GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
    var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="10" height="10" title="Remove this message" style="float:right" onClick="removeMessage()" />'
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
			<table border=0 width="399"> \
				<tr> \
					<td><b>To</b>:' + names + '</td> \
				</tr> \
				<tr> \
					<td><hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:11px;" align="left"></div></td> \
				</tr> \
				<tr> \
					<td ><textarea id="personalMessage" onKeyUp="checkPersonalMsgLength()" style="font-size:12px;resize:none;width:400px;height:50px;" placeholder="Your personal message"></textarea></td> \
				</tr> \
				<tr> \
					<td >' + close_button + '<div id="definedMessage"  style="font-size:12px;padding:3px;text-align:justify;width:370px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div></td> \
				</tr> \
				' + notes + '\
			</table> \
			<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 460,
        height: 380,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "Invite",
                "class": 'btn-custom-blue',
                click: function() {
                    var bValid = true;
                    $("#personalMessage").removeClass("ui-state-error");
                    bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 100, '#personalMessageTips');
                    if (bValid) {
                        var NewDialog = $('<div id="confirmPopup" align="center">\
												<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'noTitleStuff',
                            title: 'Please wait...',
                            modal: true,
                            position: 'center',
                            width: 200,
                            height: 120
                        });
                        NewDialog.dialog('open');

                        personalMessage = $("#personalMessage").val();
                        definedMessage = $("#definedMessage").html();
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        definedMessage = definedMessage.replace("<br><br>", "\n\n");
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#messageDialog').dialog('destroy').remove();
                        }, 500);

                        //var profile=GLOMP_BASE_URL+"welcome.html?from="+glomp_user_id;
                        //var profile ='http://glomp.it/welcome.html?from='+glomp_user_id;
                        //var profile ='http://glomp.it/?from='+glomp_user_id;
                        var profile = 'http://glomp.it/?from=' + glomp_user_id;


                        doGlompPost(profile, personalMessage, definedMessage, 0);

                    }

                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function checkPersonalMsgLength() {
    $("#personalMessage").removeClass("ui-state-error");
    $('#personalMessageTips').html('');

    checkLength($("#personalMessage"), "Personal message", 1, 100, '#personalMessageTips');
}
function doGlompPost(profile, personalMessage, definedMessage, ctr) {
    var lim = 10;
    var tags = "";
    for (; ctr < selectedCtr; ) {
        tags += ' @[' + arrSelected[ctr] + ']\n';
        lim--;
        ctr++;
        if (lim == 0)
            break;
    }
    console.log(ctr + "=" + tags);
    /*if(ctr<selectedCtr){
     doGlompPost(profile, personalMessage, definedMessage, ctr);
     }*/

    FB.api(
            'me/glomp_app:invite',
            'post',
            {
                profile: profile,
                message: '\n' + tags + '\n' + personalMessage + '\n\n' + definedMessage
            },
    function(response) {
        $('#confirmPopup').dialog('destroy').remove();
        if (hasOwnProperty(response, 'id')) {
            if (ctr < selectedCtr) {
                doGlompPost(profile, personalMessage, definedMessage, ctr);
            }
            else {
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
							<p></div>');
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 220,
                    height: 120,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: "Ok",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $('.friendThumbSelected').each(function(i, item) {
                                    if ($(this).prop('checked')) {
                                        $(this).prop('checked', false);
                                    }
                                });

                                $(this).dialog("close");
                                checkTour();
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);

                                $('#listDialog').dialog("close");
                                setTimeout(function() {
                                    $('#listDialog').dialog('destroy').remove();
                                }, 500);
                            }}
                    ]
                });
                NewDialog.dialog('open');
            }
        }
        else {
            $('#listDialog').dialog("close");
            setTimeout(function() {
                $('#listDialog').dialog('destroy').remove();
            }, 500);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">An unknown error occurred.</div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white2',
                        click: function() {
                            $('.friendThumbSelected').each(function(i, item) {
                                if ($(this).prop('checked')) {
                                    $(this).prop('checked', false);
                                }
                            });

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                            $('#listDialog').dialog("close");
                            setTimeout(function() {
                                $('#listDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        // handle the response
        console.log(response);
        //var URL='http://facebook.com/'+response.id;
        //window.open(URL);
    }
    );

}
function checkLength(o, n, min, max, tipsID) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(tipsID, "Length of " + n + " must be between " + min + " and " + max + ".");
        return false;
    } else {
        return true;
    }
}
function updateTips(tipsID, t) {
    $(tipsID).html(t);
    //.addClass( "ui-state-highlight" );
    setTimeout(function() {
        $(tipsID).removeClass("ui-state-highlight", 1500);
    }, 500);
}



// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

function bindScroll() {
    $('#searchUserScroller').bind('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            //var new_div = '<div class="new_block"></div>';
            loadMoreItems();
        }
    });
}

function loadMoreItems() {
    alert("Asd");
}
/*search friends////*/
var hasHidden = false;
function searchFriends() {
    var searchThis = ($.trim($("#searchForFriend").val())).toLowerCase();
    if (searchThis != "") {
        var found = 0;
        _friend_data = new Array();
        console.log(_friend_data_global);
        $.each(_friend_data_global, function(i, item) {
            var name = (item.name).toLowerCase();
            if (name.indexOf(searchThis) >= 0) {
                _friend_data.push(item);
                found++;
            }
        });
        global_page = 1;
        if (found == 0) {
            $('#friendsList').html('');
            afterCreateFbFriendsList();
        }
        else {
            var count = _friend_data.length;
            paginateFbFriends(global_page, global_per_page, count, _friend_data);
        }

    }
    else {
        _friend_data = _friend_data_global;
        global_page = 1;
        var count = _friend_data.length;
        paginateFbFriends(global_page, global_per_page, count, _friend_data);
    }
    /*var searchThis=	$.trim( $("#searchTextField").val());	
     if(searchThis!=""){
     var found=0;
     $('.friendSearcheable').each(function( i,  item) {	
     var thisID=($(this).val()).toLowerCase();
     var name=($('#search_name_'+thisID).html()).toLowerCase();
     
     if (name.indexOf(searchThis) >= 0) { 
     $('#search_wrapper_'+thisID).show();
     found++;
     }
     else{
     $('#search_wrapper_'+thisID).hide();
     hasHidden=true;
     }
     });
     if(found==0){
     $('#search_not_found').show();			
     }
     else{
     $('#search_not_found').hide();
     }
     
     $(".searchUser").nanoScroller();
     }
     else if(hasHidden){
     $('.friendSearcheable').show();
     $(".searchUser").nanoScroller();
     }*/
}
var cc = 0;
var t;
var timer_is_on = 0;

function timedCount(type)
{
    cc = cc + 1
    if (cc == 2) {
        searchFriends();
    }
    else
        t = setTimeout("timedCount()", 500);
}
function doTimer() {
    cc = 0;
    if (!timer_is_on) {
        timer_is_on = 1;
        timedCount();
    }
}
function stopCount() {
    cc = 0;
    clearTimeout(t);
    timer_is_on = 0;
}
function getKey() {
    stopCount();
    doTimer();
}
function getCheck(_this) {
    _this.checked = !(_this.checked);
}
function addToList(item, thisFriendStatus) {
    thisFriendStatus = thisFriendStatus.split(":");
    loc = "";
    item.loc = loc;
    if (hasOwnProperty(item, 'location')) {
        temp = item.location;
        loc = temp.name;
        item.loc = loc;
    }
    var icon = '';
    var plus = '';
    var href = '';
    var wrapper = 'wrapper_img_fb_';

    if (thisFriendStatus[0] == -1) {
        icon = '<image style="float:left;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/fb-icon.png" \>';
        href = '<a href="javascript:void(0);" id="href_wrapper_img_fb_' + item.id + '" >';
        if (FB_WHAT_TO_DO == 'invite') {
            plus = '<input onclick="getCheck(this)"  type="checkbox" value="' + item.id + '" id="chk_' + item.id + '" class="friendThumbSelected" style="float:right;z-index: 2;margin-top:-18px;margin-right:3px" />';
        }
        else {

        }
    }
    else if (thisFriendStatus[0] == 0) {
        icon = '<image class="addThisFriendClass" onClick="askAddThisFriend(' + item.id + ')" id="fb_plus_' + item.id + '" style="cursor:pointer;float:right;z-index: 10;margin-top:-28px;margin-right:3px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
        href = '<a id="fb_link_' + item.id + '"  href="javascript:void(0);" >';
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[1] + '" target="" >';
        wrapper = '';
    }
    else {
        href = '<a href="' + GLOMP_BASE_URL + 'profile/view/' + thisFriendStatus[0] + '" target="" >';
        wrapper = '';
    }
    $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
										<div class=""> \
												<div class="" > \
													' + href + ' \
													<div id="' + wrapper + '' + item.id + '"  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
														<image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="https://graph.facebook.com/' + item.id + '/picture?type=large&return_ssl_results=1" alt="' + item.name + '"  \> \
													</div> \
													</a> \
													' + icon + '' + plus + ' \
													' + href + ' \
													<p><b id="search_name_' + item.id + '" >' + item.name + '</b><br /><span style="" id="search_loc_' + item.id + '" >' + loc + '</span></p> \
													</a> \
												</div> \
										</div> \
									</div>');
    $elem.val(item.id);





    $('#friendsList').append($elem);

    var img = document.getElementById('img_fb_' + item.id);
    //or however you get a handle to the IMG
    var width = parseInt(img.clientWidth);
    var height = parseInt(img.clientHeight);
    if (width > height) {
        $('#img_fb_' + item.id).css({'height': 100});
    }
    else {
        $('#img_fb_' + item.id).css({'width': 100});
    }





    $(('#href_wrapper_img_fb_' + item.id)).on("click", function(ev) {
        //$(('#wrapper_img_fb_'+item.id)).click();
    });
    $(('#wrapper_img_fb_' + item.id)).on("click", function(ev) {
        var elemID = item.id;//$(this).val();	
        var name = $('#search_name_' + elemID).html();
        if (FB_WHAT_TO_DO == 'invite') {
            var chk = !($('#chk_' + elemID).prop('checked'));
            $('#chk_' + elemID).prop('checked', chk);
        }
        else {
            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                    <div align="center" style="margin-top:5px;">Getting your friend\'s location...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                            </div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 200,
                height: 120,
                show: '',
                hide: ''
            });
            NewDialog.dialog('open');
            FB.api({
                method: 'fql.query',
                query: 'SELECT first_name, last_name, current_location, birthday_date  FROM user WHERE uid = ' + elemID
            },
            function(response) {
                var country = "";
                var first_name = response[0]['first_name'];
                var last_name = response[0]['last_name'];
                var birthday_date = response[0]['birthday_date'];
                if (response[0]['current_location'] != null)
                    country = response[0]['current_location']['country'];
                
                if (country == "") {
                    $.ajax({
                        type: "POST",
                        url: 'ajax_post/getCountryList',
                        success: function(res) {
                            $("#waitDialog").dialog("close");
                            $("#waitDialog").dialog('destroy').remove();
                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                    <div align="center" style="margin-top:5px;"><b>' + name + '</b> has hidden their location in Facebook.<br><br>Please enter their location here:\
                                                    <br><br><select id="selected_country" name="selected_country">' + res + '</select></div>\
                                            </div>');
                            NewDialog.dialog({
                                dialogClass: 'noTitleStuff',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 350,
                                height: 200,
                                show: '',
                                hide: '',
                                buttons: [
                                    {text: "Continue",
                                        "class": 'btn-custom-darkblue',
                                        click: function() {
                                            country = $('#selected_country').find(":selected").text();
                                            country_id = getCountryID(country);
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                            
                                            $('#email').val(elemID +'@facebook.com');
                                            $('#name').val(first_name);
                                            $('#location').val(country);
                                            $('#location_id').val(country_id);
                                            $('#fb_id').val(elemID);
                                            $('#fb_email').val(elemID +'@facebook.com');
                                            $('#fb_fname').val(first_name);
                                            $('#fb_lname').val(last_name);
                                            $('#fb_dob').val(birthday_date);
                                            show_glomp();
                                        }},
                                    {text: "Cancel",
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $('#waitDialog').dialog("close");
                                            $('#waitDialog').dialog('destroy').remove();
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    });
                }
                else {
                    $("#waitDialog").dialog("close");
                    $("#waitDialog").dialog('destroy').remove();
                    $('#email').val(elemID +'@facebook.com');
                    $('#name').val(first_name);
                    $('#location').val(country);
                    country_id = getCountryID(country);
                    $('#location_id').val(country_id);
                    $('#fb_id').val(elemID);
                    $('#fb_email').val(elemID +'@facebook.com');
                    $('#fb_fname').val(first_name);
                    $('#fb_lname').val(last_name);
                    $('#fb_dob').val(birthday_date);
                    show_glomp();
                }
            });
        }
    });
    $(".searchUser").nanoScroller();

}

function getCountryID(country) {
    var country_id;
    
    $.ajax({
        type: "POST",
        url: 'ajax_post/getCountryID',
        data: {
          country: country  
        },
        async:false,
        dataType: 'json',
        success: function(res) {
            country_id = res;
            return true;
        }
    });
    
    return country_id;
}
function askAddThisFriend(id) {
    var name = $('#search_name_' + id).html();
    var loc = $('#search_loc_' + id).html();
    var desc = "Add " + name + " to your glomp! network?";
    var img = $('#img_fb_' + id).prop('src');
    var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
									<div class="friendThumb3" > \
										<div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
											<image id="fb_friend_popup_img" style="float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
										</div> \
									</div>\
									<div class="friendThumb3_name" > \
										<b>' + name + '</b><br /><span style="">' + loc + '</span>\
									</div>\
									<div class="friendThumb3_desc"  >' + desc + ' \
									</div> \
								</div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        dialogClass:'dialog_style_blue_grey',
                title: "",
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 300,
        height: 250,
        show: 'clip',
        hide: 'clip',
        buttons: [{text: 'Confirm',
                "class": 'btn-custom-darkblue',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#FriendPopupDialog').dialog('destroy').remove();
                        addThisFriendFB(id);
                    }, 500);
                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#FriendPopupDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
function createFbFriendsList() {
    $('#friendsList').html('<div align="center" style="margin-top:50px;">' + GLOMP_GETTING_FB_FRIENDS + '<span style="padding:9px 15px;background-image:url(\'assets/frontend/css/images/ajax-loader.gif\');repeat:no-reapeat;">&nbsp</span></div>');
    if (_friend_data == null) {
        FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
            _friend_data = response.data.sort(sortByName);
            _friend_data_global = response.data.sort(sortByName);
            count = _friend_data.length;
            paginateFbFriends(global_page, global_per_page, count, _friend_data);
        });
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        paginateFbFriends(global_page, global_per_page, count, _friend_data);
    }
}
function loadPage() {
    if (_friend_data == null) {
        FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
            _friend_data = response.data.sort(sortByName);
            _friend_data_global = _friend_data;
            count = _friend_data.length;
            paginateFbFriends(global_page, global_per_page, count, _friend_data);
        });
    }
    else {
        //_friend_data = response.data.sort(sortByName);						
        count = _friend_data.length;
        paginateFbFriends(global_page, global_per_page, count, _friend_data);
    }
}
function paginateFbFriends(page, limit, max, data) {
    max = Math.ceil(max / limit);
    if (global_page <= 1) {
        $("#page_prev").prop("disabled", true);
    }
    else {
        $("#page_prev").prop("disabled", false);
    }
    if (global_page >= max) {
        $("#page_next").prop("disabled", true);
    }
    else {
        $("#page_next").prop("disabled", false);
    }

    start_here = (page - 1) * limit;
    stop_here = start_here + limit;
    hasList = false;
    idList = new Array();
    ctr = 0;
    $.each(data, function(i, item) {
        if (ctr >= start_here && stop_here > ctr) {
            idList.push(item.id);
            hasList = true;
        }
        ctr++;
    });
    if (hasList)
    {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus',
            data: 'idList=' + idList,
            success: function(response) {
                friendsStatus = response;
                $('#friendsList').html('');
                $('#friendsList').addClass('content2');
                //(response.fbid_24102928);								
                ctr = 0;
                $.each(data, function(i, item) {

                    if (ctr >= start_here && stop_here > ctr) {
                        setTimeout(function() {
                            tempID = 'fbid_' + item.id;
                            friendStat = friendsStatus[tempID];
                            addToList(item, friendStat, function(status) {
                                if (status == 'OK') {
                                    //do stuff...
                                }
                            });
                        }, i * 1);
                    }
                    ctr++;
                });
            }
        });
    }
    else {
        afterCreateFbFriendsList();

    }
}
function afterCreateFbFriendsList() {
    $elem = $('<div id="search_not_found"  style="display:; clear:both;" align="center"> \
					No results found... \
					</div>');
    $('#friendsList').append($elem);

}
function doGlomp(_this) {

    if ($('#merchantDialog').doesExist() == true) {
        $("#glomp_form").dialog('destroy').remove();
    }
    $("#showError").hide();
    $("#showSuccess").hide();
    var img = _this.data('image');
    var point = _this.data('point');
    var item = _this.data('productname');
    var merchant = _this.data('merchantname');
    var id = _this.data('id');
    var NewDialog = $('<form name="glomp_form" id="glomp_form" method="post" style=""><div id="" align="left">\
										<input type="hidden" name="user_fname"  id="" value="' + global_first_name + '" />\
										<input type="hidden" name="user_lname"  id="" value="' + global_last_name + '" />\
										 <input type="hidden" name="user_fbID"  id="" value="' + global_fbID + '"/>\
										 <input type="hidden" name="location_id"  id="" value="' + global_Country + '"/>\
										 <input type="hidden" name="product_id"  id="product_id" value="' + id + '" />\
										 <div class="text-center"><img src="' + GLOMP_BASE_URL + 'assets/frontend/img/logo-small.png" alt="Glomp Logo" /></div>\
										<div class="row-fluid">\
											<div class="span12">\
												<div class="alert alert-success"  id="showSuccess" style="display:none;font-size:12px !important;">    \
													<span id="showSuccessMessage"></span>\
												</div>\
												<div class="alert alert-error"  id="showError" style="display:none;font-size:12px !important;">  \
													<span id="showErrorMessage"></span>\
												</div>\
											<div class="span3">\
												<span class="label label-important" id="popUpPoints">' + point + '</span>\
												<img id="pipup_image" src="' + img + '" style="border-radius:4px" >\
											</div>\
											<div class="span1">&nbsp;</div>\
											<div class="span8">\
												<div class="itemname">\
													<div id="popUpMerchant">' + merchant + '</div><br />\
													<div id="popUpItem">' + item + '</div> <br />\
												</div>\
											</div>\
											<div class="cl">\
												<div class="">\
													<div class="p2px_5px_0px_5px" align="left" style="padding:8px 0px 0px 0px">\
														<input type="password" name="password"  id="glomp_password" style="text-align:center;font-size:12px !important;" class="span12" placeholder="Password">\
													</div>\
													<div class="p8px_5px_0px_5px" align="center" style="padding:8px 0px 0px 0px">\
														<a href="' + GLOMP_BASE_URL + '/landing/forgotPassword/" class="forgot_pass white">Forgot Password?</a>\
													</div>\
												</div>\
											</div>\
										</div></form>');


    NewDialog.dialog({
        dialogClass: 'noTitleStuff',
        autoOpen: false,
        resizable: false,
        title: '',
        modal: true,
        width: 460,
        height: 400,
        position: 'center',
        buttons: [
            {text: "Continue",
                "class": 'btn-custom-darkblue',
                click: function() {
                    var NewDialog = $('<div id="waitPopup" align="center">\
												<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'noTitleStuff',
                        title: 'Please wait...',
                        modal: true,
                        position: 'center',
                        width: 200,
                        height: 120
                    });
                    NewDialog.dialog('open');
                    $.ajax({
                        type: "POST",
                        url: GLOMP_BASE_URL + "profile/glompToUserFacebook/",
                        data: $('#glomp_form').serialize(),
                        success: function(data) {
                            var d = eval("(" + data + ")");
                            if (d.status == 'success')
                            {
                                $("#waitPopup").dialog('destroy').remove();
                                $("#glomp_form").dialog('destroy').remove();
                                $("#merchantDialog").dialog('destroy').remove();
                                fbSendGlomp(d.voucher_id);

                            }
                            else if (d.status == 'error')
                            {
                                $("#waitPopup").dialog('destroy').remove();
                                $("#showError").show();
                                $("#showErrorMessage").text(d.msg);
                            }
                        }
                    });
                }},
            {text: "Cancel",
                "class": 'btn-custom-white2',
                click: function() {
                    $("#glomp_form").dialog('destroy').remove();
                }}
        ]

    });
    NewDialog.dialog("open");

}
function fbSendGlomp(voucher_id, fb_id) {
    console.log('http://glomp.it/glomp_story/?voucher=' + voucher_id + '&fbID=' + fb_id);
    FB.ui({
        method: 'send',
        to: fb_id,
        message: 'message',
        link: GLOMP_BASE_URL + '/glomp_story/?voucher=' + voucher_id + '&fbID=' + fb_id
    }, postFbMessageCallback);
}
function  postFbMessageCallback(response) {
    console.log(response);
    var NewDialog = $('<div id="confirmPopup" align="center">\
									<div align="center" style="margin-top:-5px;font-size:12px !important;">Successfully glomp!ed</div></div>');
    NewDialog.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'noTitleStuff',
        title: '',
        modal: true,
        width: 200,
        position: 'center',
        height: 120,
        buttons: [
            {text: "Ok",
                "class": 'btn-custom-green2',
                click: function() {
                    $(this).dialog("close");
                    location.href = GLOMP_BASE_URL;
                }}
        ]
    });
    NewDialog.dialog('open');
}
function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
            (!(prop in proto) || proto[prop] !== obj[prop]);
}

function askFbOption() {
    
    $('#fb_invite_button_wrapper').hide();
    $('#fb_paginator_wrapper').show();
    FB_WHAT_TO_DO = 'glomp';
    checkLoginStatus();
    $(this).dialog("close");
    setTimeout(function() {
        $('#messageDialog').dialog('destroy').remove();
    }, 500);
}
function checkLoginStatus() {
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML	  });		
    });
    FB.getLoginStatus(function(response) {
        if (response && response.status == 'connected') {
            createFbFriendsList();
            //sendRequestToRecipients();
        } else {
            FB.login(
                    function(response)
                    {
                        if (response.status === 'connected') {

                            createFbFriendsList();
                            //sendRequestToRecipients();
                        }
                    }, {scope: 'publish_actions,email'}
            );
            // Display the login button          
        }
    }
    );
}
function inviteThisFriend(id) {
    FB.ui({
        method: 'send',
        to: [id],
        message: 'message',
        link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/'
    });
}
function addThisFriendFB(fbID) {
    var data = '&fbID=' + fbID;
    $.ajax({
        type: "POST",
        url: 'ajax_post/addThisFriend',
        data: data,
        dataType: 'json',
        success: function(response) {
            $('#fb_plus_' + fbID).hide();
            //var url=GLOMP_BASE_URL+'profile/view/'+response.status;
            //$("#fb_link_"+id).attr("href", url);				

            //alert(response.status);
            var NewDialog = $('<div id="messageDialog" align="center"> \
												<p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '</p> \
											</div>');
            NewDialog.dialog({
                autoOpen: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                modal: true,
                width: 280,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    });
}
function addThisFriend(id) {
    var data = '&id=' + id;
    $.ajax({
        type: "POST",
        url: 'ajax_post/addThisFriendNotFB',
        data: data,
        dataType: 'json',
        success: function(response) {
            $('#add_' + id).hide();
            //var url=GLOMP_BASE_URL+'profile/view/'+response.status;
            //$("#fb_link_"+id).attr("href", url);				

            //alert(response.status);
            var NewDialog = $('<div id="messageDialog" align="center"> \
												<p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '</p> \
											</div>');
            NewDialog.dialog({
                autoOpen: false,
                resizable: false,
                dialogClass: 'noTitleStuff',
                title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                modal: true,
                width: 280,
                height: 140,
                show: 'clip',
                hide: 'clip',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white2',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    });
}
function checkTour()
{
    if (inTour == 'yes') {
        var NewDialog = $('<div id="tourDialog" align="center"> \
										<p style="padding:15px 0px;font-weight:bold !important;">Add more Friends?</p> \
									</div>');
        NewDialog.dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'noTitleStuff',
            title: '',
            modal: true,
            width: 320,
            height: 140,
            show: 'clip',
            hide: 'clip',
            buttons: [
                {text: "Add more!",
                    "class": 'btn-custom-darkblue ',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#tourDialog').dialog('destroy').remove();
                        }, 500);
                    }},
                {text: "Return to Tour",
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $('#tourDialog').dialog('destroy').remove();
                            window.location.href = GLOMP_BASE_URL + 'user/dashboard/?inTour=2';
                        }, 500);

                    }}
            ]
        });
        NewDialog.dialog('open');

    }
}
function sample() {
    FB.api(
            {
                method: 'fql.query',
                query: 'SELECT uid, first_name, last_name,current_location  FROM user WHERE uid = 1123352461'
            },
    function(data) {
        console.log(data);
        //    do something with the response
    }
    );
}
function showGlompMenu(id, country, first_name, last_name) {
    global_fbID = id;
    global_Country = country;
    global_first_name = first_name;
    global_last_name = last_name;
    var name = $('#search_name_' + id).html();
    $("#waitDialog").dialog("close");
    $("#waitDialog").dialog('destroy').remove();
    if ($('#merchantDialog').doesExist() == true) {
        $("#merchantDialog").dialog('destroy').remove();
    }

    var NewDialog = $('<div id="merchantDialog" align="left" style="font-size:12px">\
									<div align="center" style="margin-top:5px;">Please wait...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
							  </div>');
    NewDialog.dialog({
        dialogClass: 'dialog_style_blue_dark',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 710,
        height: 500,
        show: '',
        hide: '',
        buttons: [
            {text: "Cancel",
                "class": 'btn-custom-white2 fr',
                click: function() {
                    $('#merchantDialog').dialog("close");
                    $('#merchantDialog').dialog('destroy').remove();
                }}
        ]
    });
    NewDialog.dialog('open');

    //call the menu
    var data = "";
    data += "&fbID=" + id;
    data += "&country=" + country;
    data += "&name=" + name;

    $.ajax({
        type: "POST",
        url: 'ajax_post/getGlompFBFrame',
        data: data,
        success: function(response) {
            $("#merchantDialog").html(response);
            $(".tabs").hide();
            $("#glomped_inactive").hide();

            $(".tabs:first").show();
            $(".profileTab a").click(function(e) {
                var activeTab = $(this).attr('href');
                if (activeTab == "#tabBuz")
                {
                    $("#glomped_active").show();
                    $("#glomped_inactive").hide();
                }
                else
                {
                    $("#glomped_inactive").show();
                    $("#glomped_active").hide();
                }
                e.preventDefault();
                $(".profileTab a").removeClass("activeTab");
                $(this).addClass("activeTab");
                $(".tabs").hide();
                $(activeTab).fadeIn();
            });
            $('.nameOfMerchant').click(function(e) {
                e.preventDefault();
                var _anchor = $(this).attr('href');
                $('#merchantProductDetails').load(_anchor, function() {
                    $('#tabMerchant').hide();
                    $('#merchantProductDetails').fadeIn();
                });
            });

            $('.glompItem').click(function() {
                doGlomp($(this));
            });
        }
    });
    //call the menu
}
function invitationSent(msg) {
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
					<div id="personalMessageTips" style="padding:2px;font-size:14px;">' + msg + '</div> \
					<p></div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_blue_grey',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 260,
        height: 140,
        show: 'clip',
        hide: 'clip',
        buttons: [
            {text: "Ok",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                    setTimeout(function() {
                        $('#messageDialog').dialog('destroy').remove();
                    }, 500);

                    $('#listDialog').dialog("close");
                    setTimeout(function() {
                        $('#listDialog').dialog('destroy').remove();
                    }, 500);
                }}
        ]
    });
    NewDialog.dialog('open');
}
$.fn.doesExist = function() {
    return jQuery(this).length > 0;
};	