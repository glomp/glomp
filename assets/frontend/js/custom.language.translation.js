var lang_1,lang_2;
var maxItems=0;
$(document).ready(function ($) {		
	
	$("#translationTable").tablesorter({
		 headers: {	
			  1: { sorter: false },
            2: { sorter: false },
			3: { sorter: false },            
			5: { sorter: false }
		}
	});
	loadTranslationTable();	
	
	$('#lang_col_1').on("change", function(){
		populateTranslationTable(0,30);
	});
	$('#lang_col_2').on("change", function(){
		populateTranslationTable(0,30);
	});
	$('#generateFile').on("click", function(event){
		//confirm
		$.ajax({
			type: "POST",
			url: 'adminControl/post/translation/generateFiles',
			data:''
		}).done(function(response) {														
			if(response!=""){		
				response=jQuery.parseJSON(response);
				if(response.status=='Ok'){
					res=(response.data);
					if(res.status=='Ok'){							
							showSuccessDialog('Success','Files has been created.');
							populateTranslationTable(0,30);
					}
					else{/*status is error*/showErrorDialog(res.status,res.message);}											
				}
				else{/*status is error*/showErrorDialog(response.status,response.message);}
			}
			else{/*no response receive*/showErrorDialog('Error','No response received.');}
		});					
		/*var NewDialog = $('<div id="confirmDialog"> \
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Sure to generate files?</p> </div>');
			NewDialog.dialog({
				modal: true,
				width:400,
				height:200,
				title: 'Confirm',
				show: 'clip',
				hide: 'clip',
				buttons: [                
					{text: "Yes", click: function() {
						//do delete
						$.ajax({
							type: "POST",
							url: 'adminControl/post/translation/generateFiles',
							data:''
						}).done(function(response) {														
							if(response!=""){		
								response=jQuery.parseJSON(response);
								if(response.status=='Ok'){
									res=(response.data);
									if(res.status=='Ok'){
											$('#confirmDialog').dialog("close");
											$('#confirmDialog').dialog('destroy').remove();
											showSuccessDialog('Success','Fiels has been created.');
											populateTranslationTable(0,30);
									}
									else{/*status is error/showErrorDialog(res.status,res.message);}											
								}
								else{/*status is error/showErrorDialog(response.status,response.message);}
							}
							else{/*no response receive/showErrorDialog('Error','No response received.');}
						});					
					//do delete
					}},
					{text: "Cancel", click: function() {
						$(this).dialog("close");
						$(this).dialog('destroy').remove();
					}}
				]
			});
			NewDialog.dialog('open');
		//confirm		*/
	});
	$('#deleteAllSelected').on("click", function(event){		
		var j=0;	
		var hasChecked=false;		
		var noError=true;
		var lang_col_1=$('#lang_col_1').val();
	    var lang_col_2=$('#lang_col_2').val();
		var data='';		
		$('.trans_chk').each(function( index ) {
			var transID=$(this).val();			
			if($(this).prop('checked')){
				hasChecked=true;								
				data+="&transID_"+j+"="+transID;				
				j++;
			}			
			return noError;			
		});		
		data+="&numSelected="+j;
		data+="&lang_col_1="+lang_col_1;
		data+="&lang_col_2="+lang_col_2;			
		if(noError){
				if(hasChecked){
					//confirm
					var NewDialog = $('<div id="confirmDialog"> \
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Sure to delete selected items?</p> </div>');
						NewDialog.dialog({
							modal: true,
							width:400,
							height:200,
							title: 'Confirm',
							show: 'clip',
							hide: 'clip',
							buttons: [                
								{text: "Yes", click: function() {
									//do delete
									$.ajax({
										type: "POST",
										url: 'adminControl/post/translation/deleteTransDetails',
										data:data
									}).done(function(response) {														
										if(response!=""){		
											response=jQuery.parseJSON(response);
											if(response.status=='Ok'){
												res=(response.data);
												if(res.status=='Ok'){
														$('#confirmDialog').dialog("close");
														$('#confirmDialog').dialog('destroy').remove();
														showSuccessDialog('Success','Selected terms been deleted');
														populateTranslationTable(0,30);
												}
												else{/*status is error*/showErrorDialog(res.status,res.message);}											
											}
											else{/*status is error*/showErrorDialog(response.status,response.message);}
										}
										else{/*no response receive*/showErrorDialog('Error','No response received.');}
									});					
								//do delete
								}},
								{text: "Cancel", click: function() {
									$(this).dialog("close");
									$(this).dialog('destroy').remove();
								}}
							]
						});
						NewDialog.dialog('open');
					//confirm					
				}
				else{/*error*/showErrorDialog('Error','Nothing Selected.');}			
			}
	});	
	$('#updateAllSelected').on("click", function(event){		
		var j=0;	
		var hasChecked=false;		
		var noError=true;
		var lang_col_1=$('#lang_col_1').val();
	    var lang_col_2=$('#lang_col_2').val();
		var data='';		
		$('.trans_chk').each(function( index ) {
			var transID=$(this).val();			
			if($(this).prop('checked')){
				hasChecked=true;				
				
				trans_label=$("#trans_label_"+transID).val();
				trans_desc=$("#trans_desc_"+transID).val();
				trans_col_1=$("#trans_col_1_"+transID).val();
				trans_col_2=$("#trans_col_2_"+transID).val();			
								
				if(trans_label==""){/*error*/showErrorDialog('Error','Term cannot be empty.');noError=false;}
				else if(trans_desc==""){/*error*/showErrorDialog('Error','Description cannot be empty.');noError=false;}
				else if(trans_col_1==""){/*error*/showErrorDialog('Error','Column 1 cannot be empty.');noError=false;}
				else if(trans_col_2==""){/*error*/showErrorDialog('Error','Column 2 cannot be empty.');noError=false;}
				
				
				
				data+="&transID_"+j+"="+transID;
				data+="&trans_label_"+j+"="+trans_label;
				data+="&trans_desc_"+j+"="+trans_desc;
				data+="&trans_col_1_"+j+"="+trans_col_1;
				data+="&trans_col_2_"+j+"="+trans_col_2;
				j++;				
			}			
			return noError;			
		});		
		data+="&numSelected="+j;
		data+="&lang_col_1="+lang_col_1;
		data+="&lang_col_2="+lang_col_2;			
		
		j=0;									
		$('.trans_chk_add').each(function( index ) {
			var transID=$(this).val();			
			if($(this).prop('checked')){
				hasChecked=true;				
				
				trans_label=$("#trans_label_"+transID).val();
				trans_desc=$("#trans_desc_"+transID).val();
				trans_col_1=$("#trans_col_1_"+transID).val();
				trans_col_2=$("#trans_col_2_"+transID).val();			
								
				if(trans_label==""){/*error*/showErrorDialog('Error','Term cannot be empty.');noError=false;}
				else if(trans_desc==""){/*error*/showErrorDialog('Error','Description cannot be empty.');noError=false;}
				else if(trans_col_1==""){/*error*/showErrorDialog('Error','Column 1 cannot be empty.');noError=false;}
				else if(trans_col_2==""){/*error*/showErrorDialog('Error','Column 2 cannot be empty.');noError=false;}
				
				
				
				data+="&new_transID_"+j+"="+transID;
				data+="&new_trans_label_"+j+"="+trans_label;
				data+="&new_trans_desc_"+j+"="+trans_desc;
				data+="&new_trans_col_1_"+j+"="+trans_col_1;
				data+="&new_trans_col_2_"+j+"="+trans_col_2;
				j++;				
			}			
			return noError;			
		});		
		data+="&new_numSelected="+j;
		//data+="&new_lang_col_1="+lang_col_1;
		//data+="&new_lang_col_2="+lang_col_2;
		
		
		if(noError){
				if(hasChecked){
					//do update
					$.ajax({
						type: "POST",
						url: 'adminControl/post/translation/updateTransDetails',
						data:data
					}).done(function(response) {														
						if(response!=""){		
							response=jQuery.parseJSON(response);
							if(response.status=='Ok'){
								res=(response.data);
								if(res.status=='Ok'){
										$elem=$('<p class="success_msg">Successfully updated.</p>');
										$('#notif_wrapper').html($elem);
										$('#notif_wrapper').fadeIn('fast');
										setTimeout(function() {
											populateTranslationTable(0,30);
											/*setTimeout(function() {		
												$('#notif_wrapper').fadeOut('slow');
												$('#notif_wrapper').html("");
											}, 1000);*/
										}, 100 );				
								}
								else{/*status is error*/showErrorDialog(res.status,res.message);}											
							}
							else{/*status is error*/showErrorDialog(response.status,response.message);}
						}
						else{/*no response receive*/showErrorDialog('Error','No response received.');}
					});					
				//do update
				}
				else{/*error*/showErrorDialog('Error','Nothing Selected.');}			
			}
	});	
	
	$('#addNewTerm').on("click", function(event){
		maxItems++;
		var tempID='new_'+maxItems;
		//'+maxItems+'
		var elem=' \
		<tr class="success_msg" id="row_'+tempID+'"> \
			<td id="num_'+tempID+'"></td> \
			<td id="chk_wrapper_'+tempID+'"></td> \
			<td id="label_'+tempID+'"></td> \
			<td><textarea  class="txt_'+tempID+'" id="trans_desc_'+tempID+'"  style="width:200px;height:30px;">'+tempID+'</textarea></td> \
			<td><textarea  class="txt_'+tempID+'" id="trans_col_1_'+tempID+'"  style="width:200px;height:30px;">'+tempID+'</textarea></td> \
			<td><textarea  class="txt_'+tempID+'"  id="trans_col_2_'+tempID+'" style="width:200px;height:30px;">'+tempID+'</textarea></td> \
		</tr> \
		';
		$('#translationTable > tbody:last').append(elem);
		
		var $label=$('<input type="text"  class="txt_'+tempID+'" id="trans_label_'+tempID+'" style="width:200px;" value="" />');
		$label.on('blur', function (e) {
				val =$label.val();
		});
		$('#label_'+tempID).append($label);	
		
		var $chk=$('<input type="checkbox" checked value="'+tempID+'" id="chk_new_'+tempID+'" class="trans_chk_add" />');
		$chk.on('change', function (e) {
				$(".txt_"+tempID).prop("disabled", (!this.checked));
		});
		var $del=$('<i class="icon-trash" style="cursor:pointer;margin-left:8px;" title="Delete this row"></i>');		
		$del.val(maxItems);
		$del.on('click', function (e) {
			var num='new_'+parseInt($del.val());						
			$('#row_'+num).remove();			
		});		
		$('#chk_wrapper_'+tempID).append($chk);	
		$('#chk_wrapper_'+tempID).append($del);	
	});	
	//old style
	$('.addNewTerm').on("click", function(event){		
		var NewDialog = $('<div id="addNewTermDialog"> \
		<div id="addTermTips" style="padding:4px;"></div> \
		<table> \
			<tr> \
				<th align="left">Term Name:&nbsp;</th> \
				<td><input type="text" style="width:400px;" id="term_name" value="" required="required" maxlength="50" /></td> \
			</tr> \
			<tr> \
				<th align="left">Term Description:&nbsp;</th> \
				<td><input type="text" style="width:400px;" id="term_desc" value="" maxlength="50" /></td> \
			</tr> \
			</table> \
		</div>');
        NewDialog.dialog({
            modal: true,
			width:700,
			height:300,
            title: "Add new Term",
            show: 'clip',
            hide: 'clip',
			 buttons: [
                {text: "Add", click: function() {
						var term_name		= $('#term_name').val();
						var term_desc		= $('#term_desc').val();						
						
						//check if the translation name is clean.
						var bValid = true;				
						$("#term_name").removeClass( "ui-state-error" );
						$("#term_desc").removeClass( "ui-state-error" );						
						
						bValid = bValid && checkLength( $("#term_name"), "Term Name Name", 5, 45 ,'#addTermTips');
						bValid = bValid &&	checkRegexp( $("#term_name"), /^[a-z]([0-9a-z_])+$/i, "Term Name name may consist of a-z, 0-9, underscores, begin with a letter.", '#addTermTips' )
						
						bValid = bValid && checkLength( $("#term_desc"), "Term Description", 1, 256 ,'#addTermTips');						
						if (bValid){
							var data="";
								data+="&term_name="+term_name;
								data+="&term_desc="+term_desc;								
							$.ajax({
								type: "POST",
								url: 'adminControl/post/translation/checkTermIfAvailable',
								data:data
							}).done(function(response) {														
								if(response!=""){		
									response=jQuery.parseJSON(response);
									if(response.status=='Ok'){
										res=(response.data);
										if(res.status=='Ok'){
												if(res.data==0){//trans name is valid												
												$.ajax({
													type: "POST",
													url: 'adminControl/post/translation/addNewTerm',
													data:data
												}).done(function(response) {	
													if(response!=""){		
														response=jQuery.parseJSON(response);
														if(response.status=='Ok'){
															res=(response.data);
															if(res.status=='Ok'){	
																$('#addNewTermDialog').dialog("close");
																$('#addNewTermDialog').dialog('destroy').remove();
																showSuccessDialog('Success','New Term has been added');																
																populateTranslationTable(0,30);
															}															
															else{/*status is error*/showErrorDialog(res.status,res.message);}											
														}
														else{/*status is error*/showErrorDialog(response.status,response.message);}
													}
													else{/*no response receive*/showErrorDialog('Error','No response received.');}
												});
											}
											else{//trans name already exists.
												$("#trans_eng_name").addClass( "ui-state-error" );
												updateTips('#addTransTips','Translation English name already exist');
											}
										}
										else{/*status is error*/showErrorDialog(res.status,res.message);}											
									}
									else{/*status is error*/showErrorDialog(response.status,response.message);}
								}
								else{/*no response receive*/showErrorDialog('Error','No response received.');}
							});		
						}
						
				}},	
				{text: "Cancel", click: function() {
					$(this).dialog("close");
					$(this).dialog('destroy').remove();
				}}
            ]
        });
		NewDialog.dialog('open');
	});
	$('#addNewTrans').on("click", function(event){		
		var NewDialog = $('<div id="addNewTransDialog"> \
		<div id="addTransTips" style="padding:4px;"></div> \
		<table> \
			<tr> \
				<th>Translation English Name:&nbsp;</th> \
				<td><input type="text" style="width:300px;" id="trans_eng_name" value="" required="required" maxlength="50" /></td> \
			</tr> \
			<tr> \
				<th>Translation Native Name:&nbsp;</th> \
				<td><input type="text" style="width:300px;" id="trans_native_name" value="" maxlength="50" /></td> \
			</tr> \
		</table> \
		</div>');
        NewDialog.dialog({
            modal: true,
			width:700,
			height:300,
            title: "Add new Translation",
            show: 'clip',
            hide: 'clip',
            buttons: [
                {text: "Add", click: function() {
						var trans_eng_name= $('#trans_eng_name').val();
						var trans_native_name= $('#trans_native_name').val();
						
						//check if the translation name is clean.
						var bValid = true;				
						$("#trans_eng_name").removeClass( "ui-state-error" );				
						bValid = bValid && checkLength( $("#trans_eng_name"), "Translation English Name", 5, 45 ,'#addTransTips');
						bValid = bValid &&	checkRegexp( $("#trans_eng_name"), /^[a-z]([0-9a-z_])+$/i, "Translation English name may consist of a-z, 0-9, underscores, begin with a letter.", '#addTransTips' )
						if (bValid){
							var data='trans_eng_name='+trans_eng_name;
							$.ajax({
								type: "POST",
								url: 'adminControl/post/translation/checkIfTransNameAvailable',
								data:data
							}).done(function(response) {														
								if(response!=""){		
									response=jQuery.parseJSON(response);
									if(response.status=='Ok'){
										res=(response.data);
										if(res.status=='Ok'){
											if(res.data==0){//trans name is valid
												var data='trans_eng_name='+trans_eng_name;
												data+='&trans_native_name='+trans_native_name;												
												$.ajax({
													type: "POST",
													url: 'adminControl/post/translation/addNewTranslation',
													data:data
												}).done(function(response) {	
													if(response!=""){		
														response=jQuery.parseJSON(response);
														if(response.status=='Ok'){
															res=(response.data);
															if(res.status=='Ok'){	
																$('#addNewTransDialog').dialog("close");
																$('#addNewTransDialog').dialog('destroy').remove();
																showSuccessDialog('Success','New Translation has been added');																
																populateTranslationTable(0,30);
															}															
															else{/*status is error*/showErrorDialog(res.status,res.message);}											
														}
														else{/*status is error*/showErrorDialog(response.status,response.message);}
													}
													else{/*no response receive*/showErrorDialog('Error','No response received.');}
												});
											}
											else{//trans name already exists.
												$("#trans_eng_name").addClass( "ui-state-error" );
												updateTips('#addTransTips','Translation English name already exist');
											}
										}
										else{/*status is error*/showErrorDialog(res.status,res.message);}											
									}
									else{/*status is error*/showErrorDialog(response.status,response.message);}
								}
								else{/*no response receive*/showErrorDialog('Error','No response received.');}
							});
						}					 
				}},
                {text: "Cancel", click: function() {
					$(this).dialog("close");
					$(this).dialog('destroy').remove();
				}}
            ]
        });
		NewDialog.dialog('open');
	});	
});

function checkLength( o, n, min, max ,tipsID) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips(tipsID, "Length of " + n + " must be between " +min + " and " + max + "." );
		return false;
	} else {
		return true;
	}
}
function checkRegexp( o, regexp, n ,tipsID) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( tipsID,n );
		return false;
	} else {
		return true;
	}
}
function updateTips(tipsID,t){
	$(tipsID).html(t);
	//.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$(tipsID).removeClass( "ui-state-highlight", 1500 );
	}, 500 );	
}
function loadTranslationTable(){
	getLanguageList();
	//populateTranslationTable(0,30);
}
function getLanguageList(){
	var sel_1="";
	var selected_1="";
	var sel_2="";	
	var selected_2="";
	$.ajax({
		type: "POST",
		url: 'adminControl/post/translation/getLanguageList',
		data:''
	}).done(function(response) {														
		if(response!=""){		
			response=jQuery.parseJSON(response);
			if(response.status=='Ok'){
				res=(response.data);
				if(res.status=='Ok'){
					$.each(res.data, function(i, item){
						selected_1="";
						selected_2="";
						if(sel_1==""){
							selected_1="selected";
							sel_1=item.lang_id;
							 lang_1=item.lang_name+' ('+item.lang_key+')';
						}
						if(sel_2==""){
							selected_2="selected";
							sel_2=item.lang_id;
							lang_2=item.lang_name+' ('+item.lang_key+')';
						}
						else if(sel_2==sel_1){
							selected_2="selected";
							sel_2=item.lang_id;
							lang_2=item.lang_name+' ('+item.lang_key+')';
						}
						$elem=$('<option '+selected_1+' value="'+item.lang_id+'" id="lang_1_'+item.lang_id+'">'+item.lang_name+' ('+item.lang_key+')</option>');
						$elem.appendTo("#lang_col_1");
						$elem=$('<option '+selected_2+' value="'+item.lang_id+'" id="lang_2_'+item.lang_id+'">'+item.lang_name+' ('+item.lang_key+')</option>');
						$elem.appendTo("#lang_col_2");
					});
					populateTranslationTable(0,30)
				}
				else{/*status is error*/showErrorDialog(res.status,res.message);}											
			}
			else{/*status is error*/showErrorDialog(response.status,response.message);}
		}
		else{/*no response receive*/showErrorDialog('Error','No response received.');}
	});
}
function populateTranslationTable(startRow,numRow){
	var lang_col_1=$('#lang_col_1').val();
	var lang_col_2=$('#lang_col_2').val();
	var data='startRow='+startRow;
	data+='&numRow='+numRow;
	data+='&lang_col_1='+lang_col_1;
	data+='&lang_col_2='+lang_col_2;
	
	$('#lang_col_1_head').html( ($('#lang_1_'+lang_col_1).html()) );
	$('#lang_col_2_head').html( ($('#lang_2_'+lang_col_2).html()) );		
	$("#translationTable > tbody").html("");
	$.ajax({
		type: "POST",
		url: 'adminControl/post/translation/populateTranslationTable',
		data:data
	}).done(function(response) {														
		if(response!=""){		
			response=jQuery.parseJSON(response);
			if(response.status=='Ok'){
				res=(response.data);
				if(res.status=='Ok'){
					maxItems=0;
					$.each(res.data, function(i, item){
						maxItems++;
						var elem=' \
						<tr> \
							<td>'+(i+1)+'</td> \
							<td id="chk_wrapper_'+item.trans_id+'"></td> \
							<td><input type="text" disabled class="txt_'+item.trans_id+'" id="trans_label_'+item.trans_id+'" style="width:200px;" value="'+(item.trans_label_name)+'" /></td> \
							<td><textarea disabled class="txt_'+item.trans_id+'" id="trans_desc_'+item.trans_id+'"  style="width:200px;height:30px;">'+item.trans_label_desc+'</textarea></td> \
							<td><textarea disabled class="txt_'+item.trans_id+'" id="trans_col_1_'+item.trans_id+'"  style="width:200px;height:30px;">'+item.col_1+'</textarea></td> \
							<td><textarea disabled class="txt_'+item.trans_id+'"  id="trans_col_2_'+item.trans_id+'" style="width:200px;height:30px;">'+item.col_2+'</textarea></td> \
						</tr> \
						';
						$('#translationTable > tbody:last').append(elem);
						
						var $chk=$('<input type="checkbox" value="'+item.trans_id+'" id="chk_'+item.trans_id+'" class="trans_chk" />');
							$chk.on('change', function (e) {
								$(".txt_"+item.trans_id).prop("disabled", (!this.checked));
						});
						$('#chk_wrapper_'+item.trans_id).append($chk);						
					});								
					$('#translationTable').trigger('update');
						
				}
				else{/*status is error*/showErrorDialog(res.status,res.message);}											
			}
			else{/*status is error*/showErrorDialog(response.status,response.message);}
		}
		else{/*no response receive*/showErrorDialog('Error','No response received.');}
	});

}
function showErrorDialog(title,details){
	/*var NewDialog = $('<div id="errorDialog"> \
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>'+details+'</p> </div>');
        NewDialog.dialog({
            modal: true,
			width:500,
			height:250,
            title: title,
            show: 'clip',
            hide: 'clip',
            buttons: [                
                {text: "Ok", click: function() {
					$(this).dialog("close");
					$(this).dialog('destroy').remove();
				}}
            ]
        });
		NewDialog.dialog('open');
		*/
			$elem=$('<p class="custom-error">'+title+': '+details+'</p>');
			$('#notif_wrapper').html($elem);
			$('#notif_wrapper').fadeIn('fast');
			/*setTimeout(function() {		
				$('#notif_wrapper').fadeOut('slow');
				$('#notif_wrapper').html("");
			}, 1000);	*/
		
}
function showSuccessDialog(title,details){
	/*NewDialog = $('<div id="successDialog"> \
	<p>'+details+'</p> </div>');
        NewDialog.dialog({
            modal: true,
			width:500,
			height:250,
            title: title,
            show: 'clip',
            hide: 'clip',
            buttons: [                
                {text: "Ok", click: function() {
					$(this).dialog("close");
					$(this).dialog('destroy').remove();
				}}
            ]
        });
		NewDialog.dialog('open');*/
		
		$elem=$('<p class="success_msg">'+title+': '+details+'</p>');
		$('#notif_wrapper').html($elem);
		$('#notif_wrapper').fadeIn('fast');
		/*setTimeout(function() {		
			$('#notif_wrapper').fadeOut('slow');
			$('#notif_wrapper').html("");
		}, 1000);*/
}