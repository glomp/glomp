$(document).ready(function(e) {
		$('#Invite_Through_Fb').click(function(){ 			
				//checkLoginStatus();
				getPersonalMessage();
		});
	});
	window.fbAsyncInit = function() {
	  FB.init({
		appId   : FB_APP_ID,//    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true,  // parse XFBML	  });		
	});		
	};
	// Load the SDK asynchronously
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	function createFbFriendsList() {
		FB.api('me/friends', function(response) {			
			var output = '';
			res=response.data;
			var friendsID =[];
			var x=0;
			$.each( res, function( key, value ) {				
				x++;												
				if(x<=50)
				{
					friendsID.push(value.id);
				}
			});			
			sendRequestToRecipients(friendsID);
			console.log(res);					
			//console.log('Good to see you, ' + response.name + '::'+output );
		});
    }
	function checkLoginStatus(){
		FB.getLoginStatus(function(response){
			if(response && response.status == 'connected') {
				//createFbFriendsList();
				sendRequestToRecipients();
			} else {
			  FB.login(
					function( response )
					{
						if (response.status === 'connected') {																		
							//createFbFriendsList();
							sendRequestToRecipients();
						}
					}
				);
			  // Display the login button          
			} 
		}
	 );

      // Login in the current user via Facebook and ask for email permission
      

      // Check the result of the user status and display login button if necessary
      
		
	}
	function sendRequestToRecipients() {
	//to: friendsList
	  FB.ui({method: 'apprequests',
		message: 'My Great Request'		
	  }, requestCallback);
	}	
	function requestCallback(response) {
		if(response!=null){
			//alert(response);
		}
		console.log(response);
	}
	
	var personalMessage="";
	function getPersonalMessage(){
	/**/
		var NewDialog = $('<div id="messageDialog" align="center"> \
		<div id="personalMessageTips" style="padding:4px;font-size:12px;"></div> \
		<table> \
			<tr> \
			<td><textarea id="personalMessage"  style="font-size:12px;resize:none;width:320px;height:80px;" placeholder="Your personal Message"></textarea></td> \
			</tr> \
		</table> \
		<p></div>');
			NewDialog.dialog({
				dialogClass: 'noTitleStuff',
				autoOpen: false,
				resizable: false,
				modal: true,
				width:400,
				height:220,				
				show: 'clip',
				hide: 'clip',
				buttons: [                
					{text: "Select Facebook Friends", 
					"class": 'btn-custom-blue',
					click: function() {
						var bValid = true;				
						$("#personalMessage").removeClass( "ui-state-error" );						
						bValid = bValid && checkLength( $("#personalMessage"), "Personal message", 1, 50 ,'#personalMessageTips');
						if (bValid){
							personalMessage=$("#personalMessage").val();							
							$(this).dialog("close");
							setTimeout(function() {
								$('#messageDialog').dialog('destroy').remove();
								checkLoginStatus();
							}, 500 );						
						}
					}},
					{text: "Cancel", 
					"class": 'btn-custom-white2',
					click: function() {
						$(this).dialog("close");
						setTimeout(function() {
							$('#messageDialog').dialog('destroy').remove();
						}, 500 );						
					}}
				]
			});
			NewDialog.dialog('open');
	}
		function sendRequestToRecipients(friendsList) {
		//to: friendsList
		  FB.ui({method: 'apprequests',
			message:GLOMP_MESSAGE_JOIN+personalMessage
		  }, requestCallback);
		}		
		function requestCallback(response) {
		console.log(response);
			if(response!=null){			
				var invited_IDs=response.request;
				var data=response	;		
				data["personalMessage"]=escape(personalMessage);
				
				//now send to server
				$.ajax({
					type: "POST",				
					url: 'ajax_post/saveInvitedFriendsFb',
					data:data,
					success: function(response){
					console.log(response);
							var NewDialog = $('<div id="messageDialog" align="center"> \
										<p style="padding:15px 0px;">'+GLOMP_INVITE_REQUEST_SENT_BODY+'</p> \
									</div>');
									//dialogClass: 'noTitleStuff',
						NewDialog.dialog({						
							autoOpen: false,
							resizable: false,
							title:GLOMP_INVITE_REQUEST_SENT_TITLE,
							modal: true,
							width:400,
							height:220,				
							show: 'clip',
							hide: 'clip',
							buttons: [                							
								{text: "Ok", 
								"class": 'btn-custom-white2',
								click: function() {
									$(this).dialog("close");
									setTimeout(function() {
										$('#messageDialog').dialog('destroy').remove();
									}, 500 );						
								}}
							]
						});
						NewDialog.dialog('open');	
					}			
				});				
				//now send to server		
			}
			//console.log(response);
			
			/*FB.ui({
			  method: 'feed',
			  to:"505463380",
			  link: 'https://developers.facebook.com/docs/reference/dialogs/',
			  picture: 'http://fbrell.com/f8.jpg',
			  name: 'Facebook Dialogs',
			  caption: 'Reference Documentation',
			  description: 'Using Dialogs to interact with people.'
			},
			function(response){});*/
			
		
		}
	function checkLength( o, n, min, max ,tipsID) {
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			updateTips(tipsID, "Length of " + n + " must be between " +min + " and " + max + "." );
			return false;
		} else {
			return true;
		}
	}
	function checkRegexp( o, regexp, n ,tipsID) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( tipsID,n );
			return false;
		} else {
			return true;
		}
	}
	function updateTips(tipsID,t){
		$(tipsID).html(t);
		//.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$(tipsID).removeClass( "ui-state-highlight", 1500 );
		}, 500 );	
	}