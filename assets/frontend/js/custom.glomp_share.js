var _friend_data =null;
var _friend_data_global =null;
var _getting_friends = false;

window.fbAsyncInit = function(){
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        frictionlessRequests: true,
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML	  });
    });
};
// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

var share_wrapper={};
$(document).ready(function(e) {
    /*Using off to unbind duplicate event handlers and on to attach event on DOM in the future (like DOM coming from ajax) */
    $('body').off('click','.share_on_facebook').on('click','.share_on_facebook', function(){                
        $this=$(this);
        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Pleas wait...<br><br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');							
        NewDialog.dialog({			
            dialogClass:'noTitleStuff',
            title:'',
            autoOpen: false,
            resizable: false,
            modal: true,
            width:200,
            height:120,
            show: '',
            hide: ''
        });
        NewDialog.dialog('open');
        checkLoginStatus($this);
    });
    
    $('body').off('click','.share_on_linkedin').on('click','.share_on_linkedin', function(e){
        $this=$(this);
        $data=$(this).data();
        doLinkedInShare($data);
    });
    
    $('body').off('click','.share_on_twitter').on('click','.share_on_twitter', function(){                
        $data=$(this).data();
        doTwitterShare($data);
    });
    
    $('.glomp_fb_share').hover(        
        function(e){
            $this=$(this);
            $data=$this.data();                          
            
            if($data.voucher_id in share_wrapper)
            {
                clearTimeout( share_wrapper[$data.voucher_id] );                
                delete share_wrapper[$data.voucher_id];   
            }
            else
            {
                
            }
            
            
            $('.glomp_fb_share').each(function(){
                $this_temp=$(this);
                $data_temp=$this_temp.data();                 
                if($data_temp.voucher_id!=$data.voucher_id){                    
                    $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                    clearTimeout( share_wrapper[$data.voucher_id] );                
                    delete share_wrapper[$data.voucher_id];
                }
                else
                {
                    //$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); 
                }
            });
            $('#share_wrapper_box_'+$data.voucher_id).fadeIn();
             e.stopPropagation();
        },
        function(){                
            $this=$(this);
            $data=$this.data();   
            if($data.voucher_id in share_wrapper)
            {            
                share_wrapper[$data.voucher_id]= setTimeout(function() {
                                    console.log($data.voucher_id);
									$('#share_wrapper_box_'+$data.voucher_id).fadeOut();
                                    delete share_wrapper[$data.voucher_id];                                    
								}, 500);								
            }            
        }
    );
    $('body').click(function()
    {
        $('.glomp_fb_share').each(function(){
            $this=$(this);            
            $data=$this.data();                       
            $('#share_wrapper_box_'+$data.voucher_id).fadeOut(); 
        });
    
    });  
    $('.share_wrapper_box').hover(
        function(e){  
            $this=$(this);
            $data=$this.data();
            if($data.voucher_id in share_wrapper)
            {
             
                clearTimeout( share_wrapper[$data.voucher_id] );
                delete share_wrapper[$data.voucher_id];
                share_wrapper[$data.voucher_id]='';                 
            }
            $('#share_wrapper_box_'+$data.voucher_id).show();            
             e.stopPropagation();
        },
        function(){                
            $this=$(this);
            $data=$this.data(); 
            $('#share_wrapper_box_'+$data.voucher_id).fadeOut();
        
        }
    );
    
});
    function doTwitterShare($data)
    {
        var voucher_id=$data.voucher_id;
        var from=$data.from;        
        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');							
        NewDialog.dialog({			
            dialogClass:'noTitleStuff',
            title:'',
            autoOpen: false,
            resizable: false,
            modal: true,
            width:200,
            height:120,
            show: '',
            hide: ''
        });
        NewDialog.dialog('open');        
        $.ajax({
            type: "GET",
            dataType:'json',
            url: GLOMP_BASE_URL+'twitter_api/tweet/'+from+'/'+voucher_id+'',            
            success: function(response){
                console.log(response);
                if(response.status=='Ok')
                {
                    doTweetSucces(response.status,response.message);
                }   
                else if(response.status=='Connect')
                {
                    window.location.href=response.data;
                }
                else if(response.status=='Error')
                {
                    doTweetSucces(response.status,response.message);
                }   
                
            }
        });	
    }
    function doLinkedInShare($data)
    {
        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');							
        NewDialog.dialog({			
            dialogClass:'noTitleStuff',
            title:'',
            autoOpen: false,
            resizable: false,
            modal: true,
            width:200,
            height:120,
            show: '',
            hide: ''
        });
        NewDialog.dialog('open');
        
        var comment = "shared a glomp!ed story - "+ $data.purchaser+" glomp!ed "+$data.belongs+" to a "+ $data.merchant + " "+ $data.prod_name +" on glomp!.";                 
        var submitted_url = GLOMP_BASE_URL + 'glomp_story/linkedIn?voucher=' + $data.voucher_id;
        var img= GLOMP_BASE_URL+$data.prod_img;
        IN.User.authorize(function(){
            IN.API.Raw("people/~/shares/")
            .method("POST")
            .body(JSON.stringify({
                    "comment": comment,
                    "content": {
                        "submitted-url": submitted_url,
                        "title": 'glomp!', 
                        "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
                        "submitted-image-url": img
                    },
                    "visibility": {"code": "anyone"}
                })
            ).result(function(result){
                console.log(result);
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                        <div id="personalMessageTips" style="padding:2px;font-size:12px;">Successfully shared on LinkedIn.</div> \
                        <p></div>');
                $('#waitDialog').dialog('destroy').remove();
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false ,
                    modal: true,
                    width: 220,
                    height: 120,
                    show: '',
                    hide: '',
                    buttons: [
                        {text: "Ok",										
                        "class": 'btn-custom-white2',
                        click: function() {                                                                            
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();

                        }}
                    ]
                });
                NewDialog.dialog('open');
            });
        });
    
    }
    function tryAutoShare(details)
    {
        checkLoginStatusAuto(details);    
    }
    function doAutoShare(details)
    {
        try
        {
            var story       =GLOMP_BASE_URL+'glomp_story/share/'+details.voucher_id+'?st=2';
            var story_details= '';//$data.purchaser +' glomp!ed '+ $data.belongs+' to a '+$data.merchant+' '+$data.prod_name+'.';
            var to_fb_id    =details.to_fb_id;
            if(to_fb_id!="")
            {
                FB.api({
                    method: 'fql.multiquery',
                    queries: {
                        query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+to_fb_id             
                    }            
                },
                function(response){
                    var tag ='';
                    var tagTo='';
                    if(response.error_code)
                    {
                       
                        console.log(response.error_code);
                    }                    
                    else
                    {
                    
                        if( (response[0].fql_result_set.length) >0)
                        {
                            tag ='@['+to_fb_id+']';
                            tagTo = ' ('+'@['+to_fb_id+']'+')';
                        }                    
                    }
                    //doPostPerson(person,tag,story_details);
                    doPostStory(story,tag,story_details,'',tagTo,'','', true);
                });   
                
            }
            else
            {
                //doPostPerson(person,"",story_details);
                doPostStory(story,'',story_details,'','','','', true);
            }
        }
        catch(e)
        { console.log(e); }
    
    }
    function checkLoginStatusAuto(details)
    {
        try
        {
            FB.init({
                appId       : FB_APP_ID,//    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : true,  // parse XFBML	  });		
            });	
            FB.getLoginStatus(function(response){
                if(response && response.status == 'connected') {
                    checkPermissionsAuto(details);				
                } else {
                    console.log('not logged in.');
                } 
            });
        }
        catch(e)
        { console.log(e); }
    
    }
    function checkPermissionsAuto(details)
    {
        try
        {
            FB.api('/me/permissions', function(response) {
                var permsArray = response.data;
				var permsToPrompt = [];
				for (var i in permsNeeded) 
				{
					var found =false;
					for (index = 0; index < permsArray.length; index++) 
					{
							if( permsNeeded[i] == permsArray[index].permission  && permsArray[index].status =="granted"  )
							{
									found = true;
							}
					}
					
				  if (!found) {
					permsToPrompt.push(permsNeeded[i]);
				  }
				}        
                if (permsToPrompt.length > 0) {                
                    console.log('Permission not allowed.');
                } else {
                    doAutoShare(details);
                }
            });  
        }
        catch(e)
        { console.log(e); }
    }
    function checkLoginStatus($this){
		FB.getLoginStatus(function(response){
			if(response && response.status == 'connected') {
                checkPermissions($this);				
			} else {
			  FB.login(
					function( response )
					{
						if (response.status === 'connected'){
							checkPermissions($this);							
						}
					}, {scope: 'publish_actions,email'}
				);
			  // Display the login button          
			} 
		}
	 );
    }
    var permsNeeded = ['publish_actions', 'email'];
    function checkPermissions($this)
    {
		
        FB.api('/me/permissions', function(response) {
			var permsArray = response.data;
            var permsToPrompt = [];
            for (var i in permsNeeded) 
			{
				var found =false;
				for (index = 0; index < permsArray.length; index++) 
				{
						if( permsNeeded[i] == permsArray[index].permission  && permsArray[index].status =="granted"  )
						{
								found = true;
						}
				}
				
              if (!found) {
                permsToPrompt.push(permsNeeded[i]);
              }
            }
            if (permsToPrompt.length > 0) {
                
              //alert('Need to re-prompt user for permissions: ' + permsToPrompt.join(','));
              //promptForPerms(permsToPrompt);
              $('#waitDialog').dialog('close');
              $('#waitDialog').dialog('destroy').remove();
              var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                            In order to post <b>glomp!</b> story to your Facebook wall, please click \
                            <a href="javascript:void(0);" onclick="promptForPerms()" style="text-decoration:underline" >here</a> and in the Facebook pop-up, click \'Okay\' so that that we can post to your Facebook. Thank you!\
                            </div> \
							<p></div>');
					NewDialog.dialog({
						dialogClass: 'noTitleStuff',
						autoOpen: false,
						resizable: false,
						closeOnEscape: false ,
						modal: true,
						width: 450,
						height: 140,
						show: 'clip',
						hide: 'clip',
						buttons: [
							{text: "Cancel",										
							"class": 'btn-custom-white2',
							click: function() {								
								
								$(this).dialog("close");
								setTimeout(function() {
									$('#messageDialog').dialog('destroy').remove();
								}, 500);								
								
							}}
						]
					});
					NewDialog.dialog('open');
              
            } else {
                FB.api('/me/friends', {fields: 'name,id,location,birthday'}, 
                    function(response) {
						
						
                        _friend_data = response.data.sort(sortByName);						
                        _friend_data_global=response.data.sort(sortByName);
                        count = _friend_data.length;
                    }
                );  
              doGlompShare($this);
            }
        });    
    }
    function doGlompShare($this)
    {
        $data=$this.data();
        //alert($data.voucher_id);
        var voucher_id  =$data.voucher_id;
        var prod_name   =$data.prod_name;
        var story_type  =$data.story_type;
        var prod_img    =GLOMP_BASE_URL+$data.prod_img;           
        var person      =GLOMP_BASE_URL+'glomp_story/share/'+voucher_id+'?st=2';
        var story       =GLOMP_BASE_URL+'glomp_story/share/'+voucher_id+'?st=2';
		
		//var story  = 'https://glomp.it/glomp_story/share/9a92009b-00f8-11e4-8dd2-40400d0a8b5f?st=2';
     
        
        var story_details= $data.purchaser +' glomp!ed '+ $data.belongs+' to a '+$data.merchant+' '+$data.prod_name+'.';
        
        var from_fb_id  =$data.from_fb_id;
        var to_fb_id    =$data.to_fb_id;
      
        
        
        
        
        
        
        if(story_type==1)
        {
            if(to_fb_id!="")
            {
                FB.api({
                    method: 'fql.multiquery',
                    queries: {
                        query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+to_fb_id             
                    }            
                },
                function(response){
                    var tag ='';
                    var tagTo='';
                    if(response.error_code)
                    {
                        console.log(response.error_code);
                    }                    
                    else
                    {
                    
                        if( (response[0].fql_result_set.length) >0)
                        {
                            tag ='@['+to_fb_id+']';
                            tagTo = ' ('+'@['+to_fb_id+']'+')';
                        }                    
                    }
                    //doPostPerson(person,tag,story_details);
                    doPostStory(story,tag,story_details,'',tagTo,($data.purchaser),($data.belongs), false);
                });   
                
            }
            else
            {
                //doPostPerson(person,"",story_details);
                doPostStory(story,'',story_details,'','',($data.purchaser),($data.belongs), false);
            }
            //check if this is your fb friend, if yes tag it.
            
            
        }
        else
        {
           if(to_fb_id!="" || from_fb_id!="")
            {
                if(to_fb_id=="")    to_fb_id=0;
                if(from_fb_id=="")  from_fb_id=0;
                FB.api({
                    method: 'fql.multiquery',
                    queries: {
                        query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+from_fb_id,
                        query2: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+to_fb_id
                    }            
                },
                function(response){
                    var tag ='';
                    var tagFrom='';
                    var tagTo='';
                    if(response.error_code)
                    {
                       
                        console.log(response.error_code);
                    }
                    else
                    {
                        if( (response[0].fql_result_set.length) >0)
                        {
                            tag ='@['+from_fb_id+']';
                            tagFrom = ' ('+'@['+from_fb_id+']'+')';
                        }
                        if( (response[1].fql_result_set.length) >0)
                        {
                            tag +='\n@['+to_fb_id+']';
                            tagTo = ' ('+'@['+to_fb_id+']'+')';
                        }
                    }
                    doPostStory(story,tag,story_details,tagFrom,tagTo,($data.purchaser),($data.belongs), false);
                });   
                
            }
            else
            {
                doPostStory (story,"",story_details,'','',($data.purchaser),($data.belongs), false);
            }
        }
    }
    function doPostStory(story,tag,story_details,tagFrom,tagTo,purchaser,belongs, noPopup)
    {
        //$('#shareDialog').dialog('close');
        //$('#shareDialog').dialog('destroy').remove();
        var message=tag;
        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Sharing your story.<br><br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');							
        NewDialog.dialog({			
            dialogClass:'noTitleStuff',
            title:'',
            autoOpen: false,
            resizable: false,
            modal: true,
            width:200,
            height:120,
            show: '',
            hide: ''
        });
        if(!noPopup)
            NewDialog.dialog('open');
        FB.api(
          'me/glomp_app:share',
          'post',
          {
            story: story,            
            message: message
          },
            function(response) {
                console.log(response);
                $('#waitDialog').dialog('close');
                $('#waitDialog').dialog('destroy').remove();
                if (response && response.id) {
                    //alert('Post was published.');
                       
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                            <p></div>');                             
                    
                    
                } else {
                    //alert('Post was not published.');
                     var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                    <p></div>');
                }
                if(!noPopup)
                {
                    $('#waitDialog').dialog('destroy').remove();
                    NewDialog.dialog({
                        dialogClass: 'noTitleStuff',
                        autoOpen: false,
                        resizable: false,
                        closeOnEscape: false ,
                        modal: true,
                        width: 220,
                        height: 120,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: "Ok",										
                            "class": 'btn-custom-white2',
                            click: function() {                                                                            
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove();
                                                                
                            }}
                        ]
                    });
                    NewDialog.dialog('open');
                }
            }
        );
    
    
    
    
    
    
    
        /*
    
    
        var space='';
        var space_2='';
        if(tagFrom!='')
            purchaser='<span contenteditable="false" style="background-color:#DFDFDF;margin:2px 1px;padding:2px;color:#000;border-radius:3px;">'+purchaser+'</span>';    
        else
            purchaser='';
        if(tagTo!='')
            belongs='<span contenteditable="false" style="background-color:#DFDFDF;margin:2px 1px;padding:2px;color:#000;border-radius:3px;">'+belongs+'</span>';    
        else
            belongs='';
           
        if(tagFrom!='' && tagTo!='')
            space='<span contenteditable="true">&nbsp;,&nbsp;</span>';
        if(tagFrom!='' || tagTo!='')
            space_2='<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>';
           
        $('#waitDialog').dialog('close');
        $('#waitDialog').dialog('destroy').remove();
        var NewDialog = $('<div id="shareDialog" align="left" style="font-size:11px;line-height:20px;">\
                          <div align="left" style="margin-top:5px;">\
                            <h4 style=";padding:0px">Share on Facebook</h4>\
                            <textarea style="height:100px;" class="mention" placeholder="Include your message and/or tag your friends here..."></textarea>\
                          </div>\
                          </div>');							
        NewDialog.dialog({			
            dialogClass:'noTitleStuff',
            title:'',
            autoOpen: false,
            resizable: false,
            modal: true,            
            width:350,
            height:450,
            show: '',
            hide: '',
            buttons: [
                        {text: "Share",
                            "class": 'btn-custom-blue',
                            click: function() {                                
                                $('textarea.mention').mentionsInput('val', function(text) {
                                    var res = text;
                                    //alert(text);
                                    $('textarea.mention').mentionsInput('getMentions', function(data) {
                                        console.log((data));                                        
                                        $.each( data, function( i,  item){
                                            var temp = '@['+item.name+'](contact:'+item.id+')';
                                            res = res.replace(temp ,'@['+item.id+']'); 
                                        });                                        
                                        console.log(res);
                                        
                                        
                                        ////////
                                        var story_details=res;//$('#story_details_wrapper').html();
                                        $('#shareDialog').dialog('close');
                                        $('#shareDialog').dialog('destroy').remove();
                                        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                          <div align="center" style="margin-top:5px;">Sharing your story.<br><br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div>\
                                                          </div>');							
                                        NewDialog.dialog({			
                                            dialogClass:'noTitleStuff',
                                            title:'',
                                            autoOpen: false,
                                            resizable: false,
                                            modal: true,
                                            width:200,
                                            height:120,
                                            show: '',
                                            hide: ''
                                        });
                                        NewDialog.dialog('open');
                                        FB.api(
                                          'me/glomp_app:share',
                                          'post',
                                          {
                                            story: story,            
                                            message: story_details
                                          },
                                            function(response) {
                                                console.log(response);
                                                if (response && response.id) {
                                                    //alert('Post was published.');
                                                       
                                                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                                                            <p></div>');                             
                                                    
                                                    
                                                } else {
                                                    //alert('Post was not published.');
                                                     var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                                                    <p></div>');
                                                }
                                                $('#waitDialog').dialog('destroy').remove();
                                                NewDialog.dialog({
                                                    dialogClass: 'noTitleStuff',
                                                    autoOpen: false,
                                                    resizable: false,
                                                    closeOnEscape: false ,
                                                    modal: true,
                                                    width: 220,
                                                    height: 120,
                                                    show: 'clip',
                                                    hide: 'clip',
                                                    buttons: [
                                                        {text: "Ok",										
                                                        "class": 'btn-custom-white2',
                                                        click: function() {                                                                            
                                                            $(this).dialog("close");
                                                            $(this).dialog('destroy').remove();
                                                                                            
                                                        }}
                                                    ]
                                                });
                                                NewDialog.dialog('open');
                                            }
                                        );
                                        ////
                                        
                                        
                                        
                                        
                                    });
                                });
                                
                            
                                
                            }
                        },
                        {text: "Cancel",
                            "class": 'btn-custom-white2',
                            click: function() {                                                                            
                                $(this).dialog("close");
                                $(this).dialog('destroy').remove();
                                                                
                            }
                        }
                    ]
        });
        NewDialog.dialog('open');        
                
        $('textarea.mention').mentionsInput({
            onDataRequest:function (mode, query, callback)
            {                                
                _getting_friends=true;
                responseData=getFriends(query);                
                var interval=setInterval(function() {                    
                    if(_getting_friends==false)
                    {
                        console.log(responseData);
                        responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                        callback.call(this, responseData);
                        clearTimeout(interval);
                    }                                        
                }, 10);
            }
        });      
        
        /*
        
        */
    }    
    var getFriends = function (searchThis)
    {
        
          // create a deferred object
        var r = $.Deferred();
        
        if (_friend_data == null)
        {
            console.log("query:"+searchThis);        
            FB.api('/me/friends', {fields: 'name,id,location,birthday'}, 
                function(response) {
                    _friend_data = response.data.sort(sortByName);						
                    _friend_data_global=response.data.sort(sortByName);
                    count = _friend_data.length;
                    _getting_friends=false;
                    
                    
                    
                    
                var found=0;				
                _friend_data=new Array();
                console.log(_friend_data_global);
                $.each( _friend_data_global, function( i,  item){
                    var name=(item.name).toLowerCase();
                    if (name.indexOf(searchThis) >= 0) {
                        var url = "http://graph.facebook.com/"+item.id+"/picture?width=140&height=140&redirect=false";
                        var temp={ id:item.id, name:item.name, 'avatar':url, 'type':'contact' };
                        _friend_data.push(temp);
                        found++;
                    }											
                });					        
                if(found==0){
                    console.log(_friend_data);            
                }
                else{																		
                    var count = _friend_data.length;
                    console.log(_friend_data);
                }
                return _friend_data;
                
                
                
                }
            );
        }
        else{
            //_friend_data = response.data.sort(sortByName);						
            count = _friend_data.length;                
            _getting_friends=false;
            
            var found=0;				
            _friend_data=new Array();
            console.log(_friend_data_global);
            $.each( _friend_data_global, function( i,  item){
                var name=(item.name).toLowerCase();
                if (name.indexOf(searchThis) >= 0) {
                    var url = "https://graph.facebook.com/"+item.id+"/picture?type=large&return_ssl_results=1";                                
                    var temp={ id:item.id, name:item.name, 'avatar':url, 'type':'contact' };
                    _friend_data.push(temp);
                    found++;
                }											
            });					        
            if(found==0){
                console.log(_friend_data);            
            }
            else{																		
                var count = _friend_data.length;
                console.log(_friend_data);
            }
            return _friend_data;
        }
        
        

          
    };

       
   
    function doPostPerson(person,tag,story_details)
    {
        FB.api(
            'me/glomp_app:glomp',
            'post',
            {
                person: person,                
                message: '"'+story_details+'"'+"\n"+ tag
                
            },
            function(response) {
                console.log(response);
                if (response && response.id) {
                    //alert('Post was published.');
                       
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                            <p></div>');                             
                    
                    
                } else {
                    //alert('Post was not published.');
                     var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                    <p></div>');
                }
                $('#waitDialog').dialog('destroy').remove();
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false ,
                    modal: true,
                    width: 220,
                    height: 120,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: "Ok",										
                        "class": 'btn-custom-white2',
                        click: function() {                                                                            
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();
                                                            
                        }}
                    ]
                });
                NewDialog.dialog('open');
            }
        );
    }
    function promptForPerms($this)
    {
        $('#messageDialog').dialog("close");
        setTimeout(function() {
            $('#messageDialog').dialog('destroy').remove();
        }, 500);								
         FB.login(function(response) {
			 
			 console.log(response);
			
              if (response.status === 'connected')
			  {
					
                    checkPermissions($this);                    
                }
            }, {scope: 'publish_actions,email' });
    }
    function doTweetSucces(status,message)
    {
         var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:12px;">'+message+'</div> \
                    <p></div>');
        $('#waitDialog').dialog('destroy').remove();
        NewDialog.dialog({
            dialogClass: 'noTitleStuff',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false ,
            modal: true,
            width: 220,
            height: 120,
            show: 'clip',
            hide: 'clip',
            buttons: [
                {text: "Ok",										
                "class": 'btn-custom-white2',
                click: function() {                                                                            
                    $(this).dialog("close");
                    $(this).dialog('destroy').remove();
                                                    
                }}
            ]
        });
        NewDialog.dialog('open');
    }
    function sortByName(a, b) {
		var x = a.name.toLowerCase();
		var y = b.name.toLowerCase();
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	}
    
    
    
    /* Linked IN */
              function liInitOnload() {
                  if (IN.User.isAuthorized()) {
                      /*NO NEED TO DO ANYTHING*/
                  }
              }

              function liAuth() {
                  IN.User.authorize(function(){
                      /*If authorized and logged in*/
                      var params = {
                          type: 'linked',
                          type_string: 'LinkedIN',
                          func: function(obj) {
                              return login_linkedIN(obj);
                          }
                      };

                     checkIntegratedAppConnected(params);
                 });
              }
            function login_linkedIN(obj)
            {
                  IN.API.Profile("me").fields("id","email-address", "first-name", "last-name", "date-of-birth").result(function(data){
                      $.ajax({
                          type: "POST",
                          dataType: 'json',
                          url: GLOMP_DESK_SITE_URL + 'ajax_post/checkIfIntegrated',
                          data: {
                              id: data.values[0].id,
                              email: data.values[0].emailAddress,
                              field: 'user_linkedin_id',
                              mode: 'linkedIN',
                              device: 'mobile',
                              data: data.values[0]
                          },
                          success: function(res) {
                              if (res != false) {
                                  if (res.post_to_url) {
                                      /* The rest of this code assumes you are not using a library.*/
                                      /* It can be made less wordy if you use one.*/
                                      var form = document.createElement("form");
                                      form.setAttribute("method", 'post');
                                      form.setAttribute("action", res.redirect_url);		

                                      for(var key in res.data) {
                                          if(res.data.hasOwnProperty(key)) {
                                              var hiddenField = document.createElement("input");
                                              hiddenField.setAttribute("type", "hidden");
                                              hiddenField.setAttribute("name", key);
                                              hiddenField.setAttribute("value", res.data[key]);
                                              form.appendChild(hiddenField);
                                          }
                                      }
                                      document.body.appendChild(form);
                                      obj.dialog('close');
                                      return (form.submit());
                                  }
                                  obj.dialog('close');
                                  window.location = res.redirect_url;
                                  return (window.location);
                              }
                          }
                      });
                  });
            }
         /* Linked IN */