$(document).ready(function(e) {
		$('#Invite_Through_Fb').click(function(){ 			
				checkLoginStatus();
				//getPersonalMessage();				
				//sample();
		});
		searchFriends();
		//checkLoginStatus();
		
		$('#searchTextField').keyup(function(){								
				getKey();
		});

			$("div").scroll(function () {
            var $this = $(this);
            var height = this.scrollHeight - $this.height(); // Get the height of the div
            var scroll = $this.scrollTop(); // Get the vertical scroll position

            var isScrolledToEnd = (scroll >= height);

         //   $(".scroll-pos").text(scroll);
        //    $(".scroll-height").text(height);

            if (isScrolledToEnd) {
                var additionalContent = loadMoreItems(); // Get the additional content

                $this.append(additionalContent); // Append the additional content

            }
        });
			
		$("#chk_select_all").click(function() {		
			$(".friendThumbSelected").prop('checked',($("#chk_select_all").prop('checked')));
		});
		
		$("#invite_fb_friends").click(function() {
			// check if some friends are selected
			var j=0;
			var selectedFriends= [];
			$('.friendThumbSelected').each(function( i,  item) {	
				if($(this).prop('checked')){
					var friendID= $(this).val();
					selectedFriends.push(friendID);	
					j++;
				}
			});
			if(j>0){				
				doInviteFbFriends(selectedFriends);			
				return false;
			}
			else{
				var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
					<div id="personalMessageTips" style="padding:2px;font-size:14px;">No friends selected.</div> \
					<p></div>');
				NewDialog.dialog({
					dialogClass: 'noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 220,
					height: 120,
					show: 'clip',
					hide: 'clip',
					buttons: [
						{text: "Ok",										
						"class": 'btn btn-custom-white2',
						click: function() {
							$(this).dialog("close");
							setTimeout(function() {
								$('#messageDialog').dialog('destroy').remove();
							}, 500);
							
							$('#listDialog').dialog("close");
							setTimeout(function() {
								$('#listDialog').dialog('destroy').remove();
							}, 500);
						}}
					]
					});
					NewDialog.dialog('open');	
			}
			// check if some friends are selected
			
			
		/*	$(this).submit(function() {
				return false;
			});
			return true;*/
		});	
		
		window.fbAsyncInit = function() {
		  FB.init({
			appId   : FB_APP_ID,//    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true,  // parse XFBML	  });		
		});		
		};
	
});
	function sortByName(a, b) {
		var x = a.name.toLowerCase();
		var y = b.name.toLowerCase();
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	}

	//var _friend_data = null
	function lazy_load_friend_data(uid) {
	  if (_friend_data == null) {    
		FB.api('/me/friends', function(response) {
			_friend_data = response.data.sort(sortByName);
		  }
		)
	  }
	}
	function removeMessage(){
		$('#definedMessage').html('');
		$('#messageDialog').dialog({
			width: 390,
			height: 200,
		});
	}
	function doInviteFbFriends(selectedFriends){
		console.log(selectedFriends);
		var names="";
		var selectedCtr=0;
		var firstID="";
		var tags="";
		$.each( selectedFriends, function( i,  item) {
			selectedCtr++;
			
			tags+= ' @['+item+']\n';
			var friendName = $("#search_name_"+item).html();			
			if(firstID=="")
				firstID=item;
			if(selectedCtr<=5){
				if(names!="")
					names+=', ';
				names+='<span style="padding:2px;font-size:12px;">'+friendName+'</span>';
			}
			
		});
		if(selectedCtr>5){
			names+='<span style="padding:2px;font-size:12px;"> and <b>'+(selectedCtr-5)+'</b> other(s)</span>';
		}
		
		GLOMP_FB_INVITE_PRE_MESSAGE=GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>"); 
		GLOMP_FB_INVITE_PRE_MESSAGE=GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname); 
		GLOMP_FB_INVITE_PRE_MESSAGE=GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>"); 
		var close_button='<img src="'+GLOMP_BASE_URL+'assets/images/icons/inactive.png" width="10" height="10" title="Remove this message" style="float:right" onClick="removeMessage()" />'
		var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
			<div id="personalMessageTips" style="padding:2px;font-size:11px;"></div> \
			<table border=0> \
				<tr> \
					<td><b>To</b>:'+names+'</td> \
				</tr> \
				<tr> \
					<td><hr size="1" style="padding:0px;margin:0px;">\</td> \
				</tr> \
				<tr> \
					<td ><textarea id="personalMessage"  style="font-size:12px;resize:none;width:360px;height:50px;" placeholder="Your personal message"></textarea></td> \
				</tr> \
				<tr> \
					<td >'+close_button+'<div id="definedMessage"  style="font-size:12px;padding:3px;text-align:justify;width:370px;" >'+GLOMP_FB_INVITE_PRE_MESSAGE+'</div></td> \
				</tr> \
			</table> \
			<p></div>');
		NewDialog.dialog({
			dialogClass: 'noTitleStuff',
			autoOpen: false,
			resizable: false,
			modal: true,
			width: 390,
			height: 340,
			show: 'clip',
			hide: 'clip',
			buttons: [
				{text: "Invite",
					"class": 'btn btn-custom-blue',
					click: function() {
						var bValid = true;
						$("#personalMessage").removeClass("ui-state-error");
						bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 50, '#personalMessageTips');
						if (bValid) {
							var NewDialog = $('<div id="confirmPopup" align="center">\
												<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');																
							NewDialog.dialog({						
								autoOpen: false,
								closeOnEscape: false ,
								resizable: false,					
								dialogClass:'noTitleStuff',
								title:'Please wait...',
								modal: true,
								position: 'center',
								width:200,
								height:120
							});
							NewDialog.dialog('open');
							
							personalMessage = $("#personalMessage").val();
							definedMessage	= $("#definedMessage").html();
							definedMessage=definedMessage.replace("<br><br>", "\n\n"); 
							definedMessage=definedMessage.replace("<br><br>", "\n\n"); 
							$(this).dialog("close");
							setTimeout(function() {
								$('#messageDialog').dialog('destroy').remove();
							}, 500);
						
							var profile="http://velocitydev.dyndns-server.com:8040/Glomp/v7-manila/welcome.html";
							FB.api(
							  'me/glomp_app:invite',
							  'post',
							  {
								profile: profile ,							
								message	: ''+tags+'\n'+personalMessage+'\n\n'+definedMessage
							  },
							  function(response) {
									$('#confirmPopup').dialog('destroy').remove();
								if(hasOwnProperty(response, 'id')){
										var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
											<div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
											<p></div>');
										NewDialog.dialog({
											dialogClass: 'noTitleStuff',
											autoOpen: false,
											resizable: false,
											modal: true,
											width: 220,
											height: 120,
											show: 'clip',
											hide: 'clip',
											buttons: [
												{text: "Ok",										
												"class": 'btn btn-custom-white2',
												click: function() {
													$(this).dialog("close");
													setTimeout(function() {
														$('#messageDialog').dialog('destroy').remove();
													}, 500);
													
													$('#listDialog').dialog("close");
													setTimeout(function() {
														$('#listDialog').dialog('destroy').remove();
													}, 500);
												}}
											]
											});
											NewDialog.dialog('open');
								}
								else{
									$('#listDialog').dialog("close");
									setTimeout(function() {
										$('#listDialog').dialog('destroy').remove();
									}, 500);
								}
								// handle the response
								console.log(response);
								//var URL='http://facebook.com/'+response.id;
								//window.open(URL);
							  }
							);
						}
												
					}},
				{text: "Cancel",
					"class": 'btn btn-custom-white2',
					click: function() {
						$(this).dialog("close");
						setTimeout(function() {
							$('#messageDialog').dialog('destroy').remove();
						}, 500);
					}}
			]
		});
		NewDialog.dialog('open');
	
	}
function checkLength(o, n, min, max, tipsID) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(tipsID, "Length of " + n + " must be between " + min + " and " + max + ".");
        return false;
    } else {
        return true;
    }
}



		// Load the SDK asynchronously
		(function(d){
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			ref.parentNode.insertBefore(js, ref);
		}(document));

	function bindScroll(){
		$('#searchUserScroller').bind('scroll', function(){
		if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
			//var new_div = '<div class="new_block"></div>';
			loadMoreItems();
		}
	});
	}	
	
	function loadMoreItems(){
		alert("Asd");
	}
	/*search friends////*/
	var hasHidden=false;
	function searchFriends(){
		var searchThis=	$.trim( $("#searchTextField").val());	
		if(searchThis!=""){
			var found=0;
			$('.friendSearcheable').each(function( i,  item) {	
				var thisID=($(this).val()).toLowerCase();
				var name=($('#search_name_'+thisID).html()).toLowerCase();
				
				if (name.indexOf(searchThis) >= 0) { 
					$('#search_wrapper_'+thisID).show();
					found++;
				}
				else{
					$('#search_wrapper_'+thisID).hide();
					hasHidden=true;
				}
			});
			if(found==0){
				$('#search_not_found').show();			
			}
			else{
				$('#search_not_found').hide();
			}
			
			$(".searchUser").nanoScroller();
		}
		else if(hasHidden){
			$('.friendSearcheable').show();
			$(".searchUser").nanoScroller();
		}
	}
	var cc=0;
	var t;
	var timer_is_on=0;
	
	function timedCount(type)
	{	
		cc=cc+1;
		if(cc==2){		
			searchFriends();
		}
		else
			t=setTimeout("timedCount()",500);
	}
	function doTimer(){
		cc=0;
		if (!timer_is_on){
			timer_is_on=1;
			timedCount();
		}
	}
	function stopCount(){
		cc=0;
		clearTimeout(t);
		timer_is_on=0;
	}
	function getKey(){
		stopCount();
		doTimer();
	}	
	function getCheck(_this){
		_this.checked=!(_this.checked);
	}
	function addToList(item,thisFriendStatus){
		loc="";
					item.loc=loc;
					if(hasOwnProperty(item, 'location')){
						temp=item.location;
						loc=temp.name;
						item.loc=loc;
					}
						var icon='';
						var plus='';
						var href='';
						if(thisFriendStatus==-1){
							icon='<image style="float:left;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="'+GLOMP_PUBLIC_IMAGE_LOC+'/icons/fb-icon.png" \>';
							href='<a href="javascript:void(0);" >';
							plus='<input onclick="getCheck(this)"  type="checkbox" value="'+item.id+'" id="chk_'+item.id+'" class="friendThumbSelected" style="float:right;z-index: 2;margin-top:-18px;margin-right:3px" />';
						}
						else if(thisFriendStatus==0){
							icon='<image id="fb_plus_'+item.id+'" style="float:right;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="'+GLOMP_PUBLIC_IMAGE_LOC+'/icons/plus.png" \>';	
							href='<a id="fb_link_'+item.id+'"  href="javascript:void(0);" >';
						}
						else{
							href='<a href="'+GLOMP_BASE_URL+'profile/view/'+thisFriendStatus+'" target="" >';
						}						
						$elem=$('<div class="friendThumb2 friendSearcheable " id="search_wrapper_'+item.id+'"  style="float:left; width:100px;"> \
										<div class=""> \
											'+href+' \
												<div class="" > \
													<div style="width:70;height:100px; border:0px solid #333; overflow:hidden;" > \
														<image id="img_fb_'+item.id+'" style="float:left;z-index:1;" src="https://graph.facebook.com/'+item.id+'/picture?type=large&return_ssl_results=1" alt="'+item.name+'"  \> \
													</div> \
													'+icon+''+plus+' \
													<p><b id="search_name_'+item.id+'" >'+item.name+'</b><br /><span style="">'+loc+'</span></p> \
												</div> \
											</a> \
										</div> \
									</div>');	
							$elem.val(item.id);							
							$elem.on( "click", function(ev) {
								var elemID=$(this).val();
									var chk = !($('#chk_'+elemID).prop('checked'));								
									$('#chk_'+elemID).prop('checked',chk);
								/*	var elemID=$elem.val();
									if ($(this).hasClass('friendThumb2')){										
										$(this).addClass('friendThumbSelected').removeClass('friendThumb2');
									}
									else{
										$(this).addClass('friendThumb2').removeClass('friendThumbSelected');
									}*/
								
							
							});
							
							/*
							if(thisFriendStatus<=0){
								$elem.on( "click", function(ev) {
									//ev.preventDefault ();
									//ev.stopPropagation();
									var data="fbID="+item.id;
									var buttonTitle="";							
									var name=($('#search_name_'+item.id).html());								
									var desc="";
									var title="";
									$.ajax({
										type: "POST",				
										dataType:'json',
										url: 'ajax_post/checkThisPersonStatus',
										data:data,
										success: function(response){
											if(response.status==-1){
												title=GLOMP_FB_POPUP_TITLE_INVITE;
												desc='Invite '+name+' to join glomp! network';
												var btns =[{text: 'Invite', 
													"class": 'btn btn-custom-darkblue',
													click: function() {
														$(this).dialog("close");
														setTimeout(function() {
															$('#FriendPopupDialog').dialog('destroy').remove();
															inviteThisFriend(item.id);
														}, 500 );														
													}},
													{text: "Glomp!", 
													"class": 'btn btn-custom-green2',
													click: function() {
														doGlomp(item.id);
													}},
													{text: "Cancel", 
													"class": 'btn btn-custom-white2',
													click: function() {
														$(this).dialog("close");
														setTimeout(function() {
															$('#FriendPopupDialog').dialog('destroy').remove();
														}, 500 );						
													}}
													];
											}									
											else if(response.status==0){
												title=GLOMP_FB_POPUP_TITLE_ADD;
												desc='Add  '+name+' to your glomp! Network.';
												var btns =[{text: 'Add', 
													"class": 'btn btn-custom-darkblue',
													click: function() {														
														$(this).dialog("close");														
														setTimeout(function() {															
															$('#FriendPopupDialog').dialog('destroy').remove();
															addThisFriend(item.id);
														}, 500 );
													}},
													{text: "Glomp!", 
													"class": 'btn btn-custom-green2',
													click: function() {
														doGlomp(item.id);
													}},
													{text: "Cancel", 
													"class": 'btn btn-custom-white2',
													click: function() {
														$(this).dialog("close");
														setTimeout(function() {
															$('#FriendPopupDialog').dialog('destroy').remove();
														}, 500 );						
													}}
													];
											}
											else if(response.status>0){
												desc='Glomp!  '+name+'.';
												title='';
												var btns =[
													{text: "Glomp!", 
													"class": 'btn btn-custom-green2',
													click: function() {
														doGlomp(item.id);
													}},
													{text: "Cancel", 
													"class": 'btn btn-custom-white2',
													click: function() {
														$(this).dialog("close");
														setTimeout(function() {
															$('#FriendPopupDialog').dialog('destroy').remove();
														}, 500 );						
													}}
													];
											}
											//<image style="float:right;z-index: 2;margin-top:-28px;margin-right:3px" width="25" src="'+GLOMP_PUBLIC_IMAGE_LOC+'/icons/fb-icon.png" \> 
											var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
												<div class="friendThumb3" > \
													<div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
														<image id="fb_friend_popup_img" style="float:left;z-index:1;" src="https://graph.facebook.com/'+item.id+'/picture?type=large&return_ssl_results=1" alt="alt_pic"  \> \
													</div> \
												</div>\
												<div class="friendThumb3_name" > \
													<b>'+item.name+'</b><br /><span style="">'+item.loc+'</span>\
												</div>\
												<div class="friendThumb3_desc"  >'+desc+' \
												</div> \
											</div>');							
											NewDialog.dialog({
												dialogClass: 'noTitleStuff',
												dialogClass:'dialog_style_blue_grey',
												title:title,
												autoOpen: false,
												resizable: false,
												modal: true,
												width:300,
												height:250,				
												show: 'clip',
												hide: 'clip'										
											});
											
											
											NewDialog.dialog('option', 'buttons', btns);
											NewDialog.dialog('open');
											var img = document.getElementById('fb_friend_popup_img');
											//or however you get a handle to the IMG
											var width = parseInt(img.clientWidth);
											var height = parseInt(img.clientHeight);
											if(width>height){
												$('#fb_friend_popup_img').css({'height': 120}); 					
											}
											else{					
												$('#fb_friend_popup_img').css({'width': 120});
											}									
										}
									});
								});
							}	*/								
					//$('#fb_frieds_row_'+row).append($elem);				
					$('#friendsList').append($elem);					
					
					var img = document.getElementById('img_fb_'+item.id);
					//or however you get a handle to the IMG
					var width = parseInt(img.clientWidth);
					var height = parseInt(img.clientHeight);
					if(width>height){
						$('#img_fb_'+item.id).css({'height': 100}); 					
					}
					else{					
						$('#img_fb_'+item.id).css({'width': 100});
					}
				
	}	
	
	var _friend_data =null;
	function createFbFriendsList() {
		$('#friendsList').html('<div align="center" style="margin-top:50px;">'+GLOMP_GETTING_FB_FRIENDS+'<span style="padding:9px 15px;background-image:url(\'assets/frontend/css/images/ajax-loader.gif\');repeat:no-reapeat;">&nbsp</span></div>');
		if (_friend_data == null) {    
			FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
				_friend_data = response.data.sort(sortByName);
				//console.log(_friend_data);
			//FB.api('me/friends', function(response) {					
				var output = '';
				//res=response.data;			
				res=_friend_data;
				
				
				count = res.length;
				var ctr=0;			
				idList = new Array();
				$.each( res, function( i,  item) {				
					idList.push(item.id);
				});
				//console.log(idList);
				//do a query to cross check your friends list
				$.ajax({
					type: "POST",				
					dataType:'json',
					url: 'ajax_post/checkThisFriendListStatus',
					data:'idList='+idList,
					success: function(response){
						friendsStatus=response;
						$('#friendsList').html('');
						$('#friendsList').addClass('content2');
						//(response.fbid_24102928);
						var limit=0;
						$.each( res, function( i,  item) {				
							if(limit<3000){
								limit++;					
								setTimeout( function () {
									tempID='fbid_'+item.id;								
									friendStat=friendsStatus[tempID];								
								   addToList(item,friendStat, function(status){
										if ( status == 'OK' ){
											//do stuff...
										}
								   });
								   if(ctr==100){
										$(".searchUser").nanoScroller();
										ctr=0;
									}
									else{
										ctr++;
									}
								   if (!--count) afterCreateFbFriendsList();
							   }, i * 5);
							}
						});
						$('#fb_invite_button_wrapper').show();
					}
				});			
				//do a query to cross check your friends list
				//console.log(res);			
			});
		}
		else{
			var res=_friend_data;
			
			count = res.length;
			var ctr=0;			
			idList = new Array();
			$.each( res, function( i,  item) {				
				idList.push(item.id);
			});
			//console.log(idList);
			//do a query to cross check your friends list
			$.ajax({
				type: "POST",				
				dataType:'json',
				url: 'ajax_post/checkThisFriendListStatus',
				data:'idList='+idList,
				success: function(response){
					$('#friendsList').html('');
					$('#friendsList').addClass('content2');
					friendsStatus=response;
					//(response.fbid_24102928);
					var limit=0;
					$.each( res, function( i,  item) {				
						if(limit<3000){
							limit++;					
							setTimeout( function () {
								tempID='fbid_'+item.id;								
								friendStat=friendsStatus[tempID];								
							   addToList(item,friendStat, function(status){
									if ( status == 'OK' ){
										//do stuff...
									}
							   });
							   if(ctr==100){
									$(".searchUser").nanoScroller();
									ctr=0;
								}
								else{
									ctr++;
								}
							   if (!--count) afterCreateFbFriendsList();
						   }, i * 5);
						}
					});
				}
			});
		}		
    }
	function afterCreateFbFriendsList(){
		$elem=$('<div id="search_not_found"  style="display:none; clear:both;" align="center"> \
			No results found... \
			</div>');	
		$('#friendsList').append($elem);	
		$(".searchUser").nanoScroller();		
	}
	function doGlomp(id){		
		$('#messageDialog').dialog("close").dialog('destroy').remove();
		var NewDialog = $('<div id="glompDialog" align="center"> \		</div>');							
		NewDialog.dialog({			
			dialogClass:'dialog_style_blue_grey',
			title:'Merchants',
			autoOpen: false,
			resizable: false,
			modal: true,
			width:600,
			height:400,
			show: 'clip',
			hide: 'clip'
		});
		NewDialog.dialog('open');
	
	}
	function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
        (!(prop in proto) || proto[prop] !== obj[prop]);
}

	function checkLoginStatus(){
		 FB.init({
			appId   : FB_APP_ID,//    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true,  // parse XFBML	  });		
		});	
		FB.getLoginStatus(function(response){
			if(response && response.status == 'connected') {
				createFbFriendsList();
				//sendRequestToRecipients();
			} else {
			  FB.login(
					function( response )
					{
						if (response.status === 'connected') {																		
							createFbFriendsList();
							//sendRequestToRecipients();
						}
					}, {scope: 'publish_actions,email'}
				);
			  // Display the login button          
			} 
		}
	 );
	}
	function inviteThisFriend(id){
		FB.ui({
		  method: 'send',
		  to:[id],
		 message: 'message',         
		 link: 'http://velocitydev.dyndns-server.com:8040/Glomp/v6/'		 
		});
	}
	function addThisFriend(id){
		var data='&fbID='+id;
		$.ajax({
			type: "POST",				
			url: 'ajax_post/addThisFriend',
			data:data,
			dataType:'json',
			success: function(response){
				$('#fb_plus_'+id).hide();
				var url=GLOMP_BASE_URL+'profile/view/'+response.status;
				$("#fb_link_"+id).attr("href", url);				
				
				//alert(response.status);
				var NewDialog = $('<div id="messageDialog" align="center"> \
												<p style="padding:15px 0px;">'+GLOMP_ADD_FRIEND_SUCCESS+'</p> \
											</div>');
					NewDialog.dialog({						
					autoOpen: false,
					resizable: false,
					dialogClass: 'noTitleStuff',
					title:GLOMP_INVITE_REQUEST_SENT_TITLE,
					modal: true,
					width:280,
					height:140,				
					show: 'clip',
					hide: 'clip',
					buttons: [                							
						{text: "Ok", 
						"class": 'btn btn-custom-white2',
						click: function() {
							$(this).dialog("close");
							setTimeout(function() {
								$('#messageDialog').dialog('destroy').remove();
							}, 500 );						
						}}
					]
				});
				NewDialog.dialog('open');
			}
		});	
	}
	
	function sample(){
		FB.api(
        {
            method: 'fql.query',
            query: 'SELECT uid, first_name, last_name,current_location  FROM user WHERE uid = 1123352461'
        },
        function(data) {
			console.log(data);
            //    do something with the response
        }
);
	}