window.fbAsyncInit = function() {
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        frictionlessRequests: true,
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML	  });		
    });
};
// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

var share_wrapper = {};
$(document).ready(function(e) {

    $('.share_on_facebook_redeem').click(function() {
        $this = $(this);
        checkLoginStatus($this, 'redeem');
        //share_redeem
    });
    $('.share_on_linkedin_redeem').click(function() {
        $data = $(this).data();
        var NewDialog1 = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
        NewDialog1.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            title: '',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 200,
            height: 120,
            show: '',
            hide: ''
        });
        NewDialog1.dialog('open');
        var comment = "redeemed a " + $data.merchant + " " + $data.prod_name + " on glomp!.";
        var submitted_url = GLOMP_BASE_URL + 'glomp_story/linkedIn?voucher=' + $data.voucher_id + '&linkedInID=' + $data.from_li_id;
        var img = GLOMP_BASE_URL + $data.prod_img;
        /*IN.API.Raw("people/~/shares/")
         .method("POST")
         .body(JSON.stringify({
         "comment": comment,
         "content": {
         "submitted-url": submitted_url,
         "title": 'glomp!', 
         "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
         "submitted-image-url": img
         },
         "visibility": {"code": "anyone"}
         })
         ).result(function(result){*/
        var params = {
            js_func_li: '',
            api: '/v1/people/~/shares',
            mail: 'true',
            body: JSON.stringify({
                "comment": comment,
                "content": {
                    "submitted-url": submitted_url,
                    "title": 'glomp!',
                    "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
                    "submitted-image-url": img
                },
                "visibility": {"code": "anyone"}
            }),
        };
        
        //When on validation url
        if (/redeem\/validated$/.test('redeem/validated')) {
            params.js_func_li = 'share_linkedIn_redeem()'; 
        }
        
        var ret = linkedin_api(params);
        
        if (! ret) {
            return;
        }
        
        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:12px;">Successfully shared on LinkedIn.</div> \
                    <p></div>');
        NewDialog1.dialog('destroy').remove();
        
        NewDialog.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 220,
            height: 120,
            show: '',
            hide: '',
            buttons: [
                {text: "Ok",
                    "class": 'btn-custom-white_xs w80px',
                    click: function() {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove();

                    }}
            ]
        });
        NewDialog.dialog('open');
        /*});*/
    });
    $('.share_on_twitter_redeem').click(function() {
        $data = $(this).data();
        var voucher_id = $data.voucher_id;
        var from = $data.from;
        var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
        NewDialog.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            title: '',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 200,
            height: 120,
            show: '',
            hide: ''
        });
        NewDialog.dialog('open');
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: GLOMP_BASE_URL + 'twitter_api/tweet/' + from + '/' + voucher_id + '/false/redeem',
            success: function(response) {
                console.log(response);
                $('#waitDialog').dialog('destroy').remove();
                if (response.status == 'Ok')
                {
                    doTweetSucces(response.status, response.message);
                }
                else if (response.status == 'Connect')
                {
                    window.location.href = response.data;
                }
                else if (response.status == 'Error')
                {
                    doTweetSucces(response.status, response.message);
                }

            }
        });
    });
    $('.share_on_facebook').click(function() {
        $this = $(this);
        checkLoginStatus($this, 'share');
    });
    $('.share_on_linkedin').click(function() {
        $this = $(this);
        $data = $(this).data();
        doLinkedInShare($data);
    });
    $('.share_on_twitter').click(function() {
        $data = $(this).data();
        doTwitterShare($data)

    });


    $('.glomp_fb_share').click(
            function(e) {
                $this = $(this);
                $data = $this.data();

                if ($data.voucher_id in share_wrapper)
                {

                    clearTimeout(share_wrapper[$data.voucher_id]);
                    delete share_wrapper[$data.voucher_id];
                }
                else
                {

                }

                $('.glomp_fb_share').each(function() {
                    $this_temp = $(this);
                    $data_temp = $this_temp.data();
                    if ($data_temp.voucher_id != $data.voucher_id) {
                        $('#share_wrapper_box_' + $data_temp.voucher_id).hide();
                        clearTimeout(share_wrapper[$data.voucher_id]);
                        delete share_wrapper[$data.voucher_id];
                    }
                    else
                    {
                        //$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); 
                    }
                });
                $('#share_wrapper_box_' + $data.voucher_id).toggle();
                e.stopPropagation();
            }

    );
    $('body').click(function()
    {
        $('.glomp_fb_share').each(function() {
            $this = $(this);
            $data = $this.data();
            $('#share_wrapper_box_' + $data.voucher_id).fadeOut();
        });

    });

});
function doLinkedInShare($data)
{
    var NewDialog1 = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
    NewDialog1.dialog({
        dialogClass: 'noTitleStuff dialog_style_glomp_wait',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 200,
        height: 120,
        show: '',
        hide: ''
    });
    NewDialog1.dialog('open');

    var comment = "shared a glomp!ed story - " + $data.purchaser + " glomp!ed " + $data.belongs + " to a " + $data.merchant + " " + $data.prod_name + " on glomp!.";
    var submitted_url = GLOMP_BASE_URL + 'glomp_story/linkedIn?voucher=' + $data.voucher_id + '&linkedInID=' + $data.from_li_id;
    var img = GLOMP_BASE_URL + $data.prod_img;
    /*IN.API.Raw("people/~/shares/")
            .method("POST")
            .body(JSON.stringify({
        "comment": comment,
        "content": {
            "submitted-url": submitted_url,
            "title": 'glomp!',
            "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
            "submitted-image-url": img
        },
        "visibility": {"code": "anyone"}
    })
            ).result(function(result) { */
        var params = {
            js_func_li: '',
            api: '/v1/people/~/shares',
            mail: 'true',
            body: JSON.stringify({
                "comment": comment,
                "content": {
                    "submitted-url": submitted_url,
                    "title": 'glomp!',
                    "description": 'Up your \'Like\'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!',
                    "submitted-image-url": img
                },
                "visibility": {"code": "anyone"}
            }),
        };

        var ret = linkedin_api(params);
        
        if (! ret) {
            return;
        }
        
        
        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:12px;">Successfully shared on LinkedIn.</div> \
                    <p></div>');
        NewDialog1.dialog('destroy').remove();
        NewDialog.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 220,
            height: 120,
            show: '',
            hide: '',
            buttons: [
                {text: "Ok",
                    "class": 'btn-custom-white_xs w80px',
                    click: function() {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove();

                    }}
            ]
        });
        NewDialog.dialog('open');
    /*});*/

}
function doTwitterShare($data)
{
    var voucher_id = $data.voucher_id;
    var from = $data.from;
    var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_glomp_wait',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 200,
        height: 120,
        show: '',
        hide: ''
    });
    NewDialog.dialog('open');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: GLOMP_BASE_URL + 'twitter_api/tweet/' + from + '/' + voucher_id + '',
        success: function(response) {
            console.log(response);
            if (response.status == 'Ok')
            {
                doTweetSucces(response.status, response.message);
            }
            else if (response.status == 'Connect')
            {
                window.location.href = response.data;
            }
            else if (response.status == 'Error')
            {
                doTweetSucces(response.status, response.message);
            }

        }
    });
}
function tryAutoShare(details)
{
    checkLoginStatusAuto(details);
}
function tryAutoShare_redeem(details)
{
    checkLoginStatusAuto_redeem(details);
}
function doAutoShare_redeem(details)
{
    doGlompShareRedeem(details, false)
}
function doAutoShare(details)
{
    try
    {
        var story = GLOMP_BASE_URL + 'glomp_story/share/' + details.voucher_id + '?st=2';
        var story_details = '';//$data.purchaser +' glomp!ed '+ $data.belongs+' to a '+$data.merchant+' '+$data.prod_name+'.';
        var to_fb_id = details.to_fb_id;
        if (to_fb_id != "")
        {
            FB.api({
                method: 'fql.multiquery',
                queries: {
                    query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = ' + to_fb_id
                }
            },
            function(response) {
                var tag = '';
                var tagTo = '';                
                var tagFrom = '';                
                if (response.error_code)
                {

                    console.log(response.error_code);
                }
                else
                {

                    if ((response[0].fql_result_set.length) > 0)
                    {
                        tag = '@[' + to_fb_id + ']';
                        tagTo = ' (' + '@[' + to_fb_id + ']' + ')';
                    }
                }
                //doPostPerson(person,tag,story_details);
                doPostStory(story, tag, story_details, '', tagTo, true);
            });

        }
        else
        {
            //doPostPerson(person,"",story_details);
            doPostStory(story, '', story_details, '', '', true);
        }
    }
    catch (e)
    {
        console.log(e);
    }

}
function checkLoginStatusAuto(details)
{
    try
    {
        FB.init({
            appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML	  });		
        });
        FB.getLoginStatus(function(response) {
            if (response && response.status == 'connected') {
                checkPermissionsAuto(details);
            } else {
                console.log('not logged in.');
            }
        });
    }
    catch (e)
    {
        console.log(e);
    }

}
function checkPermissionsAuto(details)
{
    try
    {
        FB.api('/me/permissions', function(response) {
            var permsArray = response.data;
            var permsToPrompt = [];
            for (var i in permsNeeded) 
            {
                var found =false;
                for (index = 0; index < permsArray.length; index++) 
                {
                        if( permsNeeded[i] == permsArray[index].permission  && permsArray[index].status =="granted"  )
                        {
                                found = true;
                        }
                }
                
              if (!found) {
                permsToPrompt.push(permsNeeded[i]);
              }
            }
            
            
            if (permsToPrompt.length > 0) {
                console.log('Permission not allowed.');
            } else {
                doAutoShare(details);
            }
        });
    }
    catch (e)
    {
        console.log(e);
    }
}
function checkLoginStatusAuto_redeem(details)
{
    try
    {
        FB.init({
            appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML	  });		
        });
        FB.getLoginStatus(function(response) {
            if (response && response.status == 'connected') {
                checkPermissionsAuto_redeem(details);
            } else {
                console.log('not logged in.');
            }
        });
    }
    catch (e)
    {
        console.log(e);
    }

}
function checkPermissionsAuto_redeem(details)
{
    try
    {
        FB.api('/me/permissions', function(response) {
            var permsArray = response.data[0];
            var permsToPrompt = [];
            for (var i in permsNeeded) {
                if (permsArray[permsNeeded[i]] == null) {
                    permsToPrompt.push(permsNeeded[i]);
                }
            }
            if (permsToPrompt.length > 0) {
                console.log('Permission not allowed.');
            } else {
                doAutoShare_redeem(details);
            }
        });
    }
    catch (e)
    {
        console.log(e);
    }
}
function checkLoginStatus($this, type) {
    try
    {
        FB.init({
            appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML	  });		
        });
        FB.getLoginStatus(function(response) {
            if (response && response.status == 'connected') {
                checkPermissions($this, type);
            } else {
                var return_url = window.location.href.split('?')[0];
                return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=publish_actions,email");      
            }
        });
    }
    catch (e)
    {
        console.log(e);
    }
}
var permsNeeded = ['publish_actions', 'email'];
function checkPermissions($this, type)
{
    FB.api('/me/permissions', function(response) {
        
       
        var permsArray = response.data;
		var permsToPrompt = [];
		for (var i in permsNeeded) 
		{
			var found =false;
			for (index = 0; index < permsArray.length; index++) 
			{
					if( permsNeeded[i] == permsArray[index].permission  && permsArray[index].status =="granted"  )
					{
							found = true;
					}
			}
			
		  if (!found) {
			permsToPrompt.push(permsNeeded[i]);
		  }
		}
       
        if (permsToPrompt.length > 0) {

            //alert('Need to re-prompt user for permissions: ' + permsToPrompt.join(','));
            //promptForPerms(permsToPrompt);
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
							<div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                            In order to invite or <b>glomp!</b> your Facebook friends, please click \
                            <a href="javascript:void(0);" onclick="promptForPerms()" style="text-decoration:underline" >here</a> and in the Facebook pop-up, click \'Okay\' so that that we can post to your Facebook. Thank you!\
                            </div> \
							<p></div>');
            NewDialog.dialog({
                dialogClass: 'noTitleStuff dialog_style_glomp_wait',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 280,
                height: 150,
                show: '',
                hide: '',
                buttons: [
                    {text: "Cancel",
                        "class": 'btn-custom-white_xs w80px',
                        click: function() {

                            $(this).dialog("close");
                            setTimeout(function() {
                                $('#messageDialog').dialog('destroy').remove();
                            }, 500);
                        }}
                ]
            });
            NewDialog.dialog('open');

        } else {
            if (type == 'redeem')
            {
                doGlompShareRedeem($this, true);
            }
            else
            {
                doGlompShare($this);
            }
        }
    });
}
function doGlompShareRedeem($this, showPopUp)
{
    var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Posting story to your wall...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_glomp_wait',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 200,
        height: 120,
        show: '',
        hide: ''
    });
    if (showPopUp)
    {
        NewDialog.dialog('open');
        $data = $this.data();
    }
    else
    {
        $data = $this;
    }
    var voucher_id = $data.voucher_id;
    var link = GLOMP_BASE_URL + 'glomp_story/share_redeem/' + voucher_id;
    console.log("here:" + link);
    FB.api(
            'me/glomp_app:redeem',
            'post',
            {
                product: link
            },
    function(response) {
        console.log(response);
		$('#waitDialog').dialog('close');
        $('#waitDialog').dialog('destroy').remove();
        if (response && response.id) {
            //alert('Post was published.');

            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                            <p></div>');


        } else {
            //alert('Post was not published.');
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                    <p></div>');
        }

        if (showPopUp)
        {
            $('#waitDialog').dialog('destroy').remove();
            NewDialog.dialog({
                dialogClass: 'noTitleStuff dialog_style_glomp_wait',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: '',
                hide: '',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white_xs w80px',
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();

                        }}
                ]
            });
            NewDialog.dialog('open');
        }

    }
    );

}
function doGlompShare($this)
{
    $data = $this.data();
    //alert($data.voucher_id);
    var voucher_id = $data.voucher_id;
    var prod_name = $data.prod_name;
    var story_type = $data.story_type;
    var prod_img = GLOMP_BASE_URL + $data.prod_img;
    var person = GLOMP_BASE_URL + 'glomp_story/share/' + voucher_id + '?st=2';
    var story = GLOMP_BASE_URL + 'glomp_story/share/' + voucher_id + '?st=2';


    var story_details = $data.purchaser + ' glomp!ed ' + $data.belongs + ' to a ' + $data.merchant + ' ' + $data.prod_name;

    var from_fb_id = $data.from_fb_id;
    var to_fb_id = $data.to_fb_id;


    var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Posting story to your wall...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_glomp_wait',
        title: '',
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 200,
        height: 120,
        show: '',
        hide: ''
    });
    NewDialog.dialog('open');

    if (story_type == 1)
    {
        if (to_fb_id != "")
        {
            FB.api({
                method: 'fql.multiquery',
                queries: {
                    query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = ' + to_fb_id
                }
            },
            function(response) {
                var tagFrom = '';
                if (response.error_code)
                {
                    tagFrom = '';
                    tag = '';
                    console.log(response.error_code);
                }
                else
                {
                    console.log(response);
                    var tag = '';
                    if ((response[0].fql_result_set.length) > 0)
                    {
                        tag = '@[' + to_fb_id + ']';
                        tagFrom = ' (' + '@[' + from_fb_id + ']' + ')';
                    }
                }
                //doPostPerson(person,tag,story_details);
                doPostStory(story, tag, story_details, tagFrom, '', false);
            });

        }
        else
        {
            //doPostPerson(person,"",story_details);
            doPostStory(story, '', story_details, '', '', false);
        }
        //check if this is your fb friend, if yes tag it.


    }
    else
    {
        if (to_fb_id != "" || from_fb_id != "")
        {
            if (to_fb_id == "")
                to_fb_id = 0;
            if (from_fb_id == "")
                from_fb_id = 0;
            FB.api({
                method: 'fql.multiquery',
                queries: {
                    query1: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = ' + from_fb_id,
                    query2: ' SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = ' + to_fb_id
                }
            },
            function(response) {
                var tag = '';
                var tagFrom = '';
                var tagTo = '';
                if (response.error_code)
                {

                    console.log(response.error_code);
                }
                else
                {
                    if ((response[0].fql_result_set.length) > 0)
                    {
                        tag = '@[' + from_fb_id + ']';
                        tagFrom = ' (' + '@[' + from_fb_id + ']' + ')';
                    }
                    if ((response[1].fql_result_set.length) > 0)
                    {
                        tag += '\n@[' + to_fb_id + ']';
                        tagTo = ' (' + '@[' + to_fb_id + ']' + ')';
                    }
                }
                doPostStory(story, tag, story_details, tagFrom, tagTo, false);
            });

        }
        else
        {
            doPostStory(story, "", story_details, '', '', false);
        }
    }
}
function doPostStory(story, tag, story_details, tagFrom, tagTo, noPopup)
{
    FB.api(
            'me/glomp_app:share',
            'post',
            {
                story: story,
                message: tag
            },
    function(response) {
        console.log(response);
        if (response && response.id) {
            //alert('Post was published.');

            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                            <p></div>');


        } else {
            //alert('Post was not published.');
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                    <p></div>');
        }
        if (!noPopup)
        {

            $('#waitDialog').dialog('destroy').remove();
            NewDialog.dialog({
                dialogClass: 'noTitleStuff dialog_style_glomp_wait',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 220,
                height: 120,
                show: '',
                hide: '',
                buttons: [
                    {text: "Ok",
                        "class": 'btn-custom-white_xs w80px',
                        click: function() {
                            $(this).dialog("close");
                            $(this).dialog('destroy').remove();

                        }}
                ]
            });
            NewDialog.dialog('open');
        }
    }
    );
}
function doPostPerson(person, tag, story_details)
{
    FB.api(
            'me/glomp_app:glomp',
            'post',
            {
                person: person,
                message: '"' + story_details + '"' + "\n" + tag

            },
    function(response) {
        console.log(response);
        if (response && response.id) {
            //alert('Post was published.');

            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was published.</div> \
                            <p></div>');


        } else {
            //alert('Post was not published.');
            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">Your story was not published.</div> \
                    <p></div>');
        }
        $('#waitDialog').dialog('destroy').remove();
        NewDialog.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 220,
            height: 120,
            show: '',
            hide: '',
            buttons: [
                {text: "Ok",
                    "class": 'btn-custom-white_xs w80px',
                    click: function() {
                        $(this).dialog("close");
                        $(this).dialog('destroy').remove();

                    }}
            ]
        });
        NewDialog.dialog('open');
    }
    );
}
function promptForPerms($this)
{
    $('#messageDialog').dialog("close");
    setTimeout(function() {
        $('#messageDialog').dialog('destroy').remove();
    }, 500);
    var return_url = window.location.href.split('?')[0];
    return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=publish_actions,email");
}
function doTweetSucces(status, message)
{
    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                    <div id="personalMessageTips" style="padding:2px;font-size:12px;">' + message + '</div> \
                    <p></div>');
    $('#waitDialog').dialog('destroy').remove();
    NewDialog.dialog({
        dialogClass: 'noTitleStuff dialog_style_glomp_wait',
        autoOpen: false,
        resizable: false,
        closeOnEscape: false,
        modal: true,
        width: 220,
        height: 120,
        show: '',
        hide: '',
        buttons: [
            {text: "Ok",
                "class": 'btn-custom-white_xs w80px',
                click: function() {
                    $(this).dialog("close");
                    $(this).dialog('destroy').remove();

                }}
        ]
    });
    NewDialog.dialog('open');
}

/* Linked IN */
function liInitOnload() {
    if (IN.User.isAuthorized()) {
        /*NO NEED TO DO ANYTHING*/
    }
}

function liAuth() {
    IN.User.authorize(function() {
        /*If authorized and logged in*/
        var params = {
            type: 'linked',
            type_string: 'LinkedIN',
            func: function(obj) {
                return login_linkedIN(obj);
            }
        };

        checkIntegratedAppConnected(params);
    });
}
function login_linkedIN(obj)
{
    IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: GLOMP_DESK_SITE_URL + 'ajax_post/checkIfIntegrated',
            data: {
                id: data.values[0].id,
                email: data.values[0].emailAddress,
                field: 'user_linkedin_id',
                mode: 'linkedIN',
                device: 'mobile',
                data: data.values[0]
            },
            success: function(res) {
                if (res != false) {
                    if (res.post_to_url) {
                        /* The rest of this code assumes you are not using a library.*/
                        /* It can be made less wordy if you use one.*/
                        var form = document.createElement("form");
                        form.setAttribute("method", 'post');
                        form.setAttribute("action", res.redirect_url);

                        for (var key in res.data) {
                            if (res.data.hasOwnProperty(key)) {
                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", key);
                                hiddenField.setAttribute("value", res.data[key]);
                                form.appendChild(hiddenField);
                            }
                        }
                        document.body.appendChild(form);
                        obj.dialog('close');
                        return (form.submit());
                    }
                    obj.dialog('close');
                    window.location = res.redirect_url;
                    return (window.location);
                }
            }
        });
    });
}
function linkedin_api(obj) {
    var data;
    obj.mail = obj.mail || '';
    obj.body = obj.body || '';

    $.ajax({
        type: "POST",
        dataType: 'json',
        async: false,
        url: GLOMP_BASE_URL + 'ajax_post/linkedIn_callback',
        data: {
            redirect_li: window.location.href,
            js_func_li: obj.js_func_li,
            api: obj.api,
            mail: obj.mail,
            body: obj.body,
        },
        success: function(_data) {
            if (_data.redirect_window != '') {
                window.location = _data.redirect_window;
                data = false;
                return;
            }
            data = _data;
        }
    });
    return data;
}
/* Linked IN */