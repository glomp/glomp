window.fbAsyncInit = function() {
    FB.init({
        appId: FB_APP_ID, //    channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        frictionlessRequests: true,
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML	  });		
    });
};
// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

var glomped=false;
var prod_data="";
$(document).ready(function(e)
{
	$('body').on('blur','#card_num_1',function(){
		var card_num_1 = $(this).val();
		check_bin(card_num_1,AMEX_CARD_BIN);
		
	});
	$('body').on('blur','#card_num_hsbc',function(){
		var card_num_hsbc = $(this).val();
		if(card_num_hsbc!='')
		{
			if(! check_bin_range(card_num_hsbc))
			{
				$("#showErrorMessage_brand").html('The card numbers you have entered is not a '+public_alias.toUpperCase()+' Singapore credit card, please re-enter the first 6 numbers of your ' +public_alias.toUpperCase()+ ' Singapore credit card.');
				$("#showError_brand").show();
			}
			else
			{
				$("#showErrorMessage_brand").html('');
				$("#showError_brand").hide();
			}
		}
		

	});		

	
	
	
	$('.glompItemMerchant').on('click',function(e)
	{
	
		prod_data = $(this).data();

		has_option = $(this).data('has_option');
		if(has_option=='yes')
		{
			add_to_cart_has_option(prod_data);
		}
		else
		{
			add_to_cart();
		}
		
		e.stopPropagation();
	});
	$('.cart_qty').on('change',function(){
		prod_data = $(this).data();
		update_cart();
		
	});
	$('.cart_qty_2').on('click',function(e){
		e.stopPropagation();
		
	});
    $('#chk_confirm').on('change',function()
	{
        var status =$(this).prop('checked');
        if(status)
        {
            $('#checkout').removeClass('btn-custom-white_grey_normal').addClass('btn-custom-green_normal');
            
        }
        else
        {
            $('#checkout').removeClass('btn-custom-green_normal').addClass('btn-custom-white_grey_normal');
            
        }			
    });
    $('.delivery_type').on('click',function()
	{
		
		var delivery_type = $(this).val();
		var brand_id  = $(this).data('id');
		var delivery_charge  = $(this).data('delivery_charge');
		

		$('#delivery_charge_'+brand_id).data('delivery_type',delivery_type);

		if(delivery_type=="delivery")
		{
			if($('#delivery_charge_value_'+brand_id).html()!='0.00')
			{
				$('#delivery_charge_'+brand_id).show();	
			}
			if((accumulated_brand_list.indexOf(brand_id) > -1 ) && $('#error_'+brand_id).html()!='' )
			{
				$('#error_'+brand_id).show();	
			}
		}
		else
		{
			if(accumulated_brand_list.indexOf(brand_id) > -1)
			{
				$('#error_'+brand_id).hide();	
			}
			else
			{
				$('#delivery_charge_'+brand_id).hide();	
			}
		}
		compute_grand_total();
	});
	$('#checkout').on('click',function()
	{
        var status =$('#chk_confirm').prop('checked');
        if(status)
        {
            if(winery_count==1){
                alert_winery();
            }
            else{
                var grand_total = $('#grand_total').html();
                
                if(grand_total=='0.00')
                {
                    alert_no_cart_items();
                }
                else
                {
                	if(public_alias=='hsbc' || public_alias=='uob' || public_alias=='dbs' || public_alias=='amex')
                	{
                		/*
                		check if there is an error                		
                		*/
                		var error='';
                		$('.brand_error').each(function(i, item)
						{
							if($(this).html() !=''  && $(this).is(":visible") )
							{
								error = $(this).html();
							}
						});
						if(error !='')
						{
							error = error.replace("<br>"," ");
							var NewDialog = $('<div id="" align="center" style="padding-top:15px !important; font-size:12px">\
								<p align="left" class="alert alert-error">'+error+'</p>\
												</div>');
								NewDialog.dialog({                    
									dialogClass:'noTitleStuff dialog_style_glomp_wait',
									title: "",
									autoOpen: false,
									resizable: false,
									modal: true,
									width: 300,
									show: '',
									hide: '',
									buttons: [
											{text: "OK",
											"class": 'btn-custom-blue_xs w80px',
											click: function() {
												$(this).dialog("close");
												
												
											}}
									]
								});
								NewDialog.dialog('open');
						}
						else
						{
							choose_buy_or_glomp();	
						}
                	}
                	else	
                    	choose_buy_or_glomp();
                }
            }
        }
        else
        {
            var NewDialog = $('<div id="" align="center" style="padding-top:15px !important; font-size:12px">\
			<p align="left" class="alert alert-error">Please check the box at the bottom left in order to proceed to Checkout.</p>\
							</div>');
			NewDialog.dialog({                    
				dialogClass:'noTitleStuff dialog_style_glomp_wait',
				title: "",
				autoOpen: false,
				resizable: false,
				modal: true,
				width: 300,
				height:160,
				show: '',
				hide: '',
				buttons: [
						{text: "Okay",
                        "class": 'btn-custom-blue_xs w80px',
                        click: function() {
                            $(this).dialog("close");
                        }}
				]
			});
			NewDialog.dialog('open');
        }
	});


});
function check_bin(bin,AMEX_BIN)
{
	if(bin!=AMEX_BIN)
	{
		$("#showErrorMessage_brand").html('Sorry, this card is not applicable to this offer. Please pay with your American Express Singapore card.');
		$("#showError_brand").show();
	}
	else
	{
		$("#showError_brand").hide();
	}
}
function alert_no_cart_items()
{
	var NewDialog = $('<div id="" align="center" style="padding-top:5px !important; font-size:14px">\
					<p align="justify"class="alert alert-error"><b>Error:</b> There are no items on your cart yet.</p>\
					</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		height:150,
		show: '',
		hide: '',
		buttons: [
				{text: "Okay",
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					$(this).dialog("close");
				}}
		]
	});
	NewDialog.dialog('open');
}
function update_cart()
{	
	var NewDialog = $('<div id="loading_dialog" align="center" style="">\
		<div align="center" style="margin-top:5px;">Please wait...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
		</div>');
	NewDialog.dialog({                    
		closeOnEscape: false,
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 200,
        height: 120,
		show: '',
		hide: ''
	});
	NewDialog.dialog('open');
	
	var prod_qty = $('#quantity_'+prod_data.id+'_'+prod_data.prod_group).val();

	var brands=[];
	$(('.delivery_charge')).each(function(i, item)
	{
		var delivery_charge = $(this).data('delivery_charge');
		var delivery_type = $(this).data('delivery_type');
		var brand_id = $(this).data('id');
		var temp={};

		temp.delivery_type=delivery_type;
		temp.brand_id = brand_id;
		brands.push(temp);    
	});
	
	var prod_qty = $('#quantity_'+prod_data.id+'_'+prod_data.prod_group).val();
	var data = {brands:JSON.stringify(brands),brand_id:prod_data.brand_id,prod_group:prod_data.prod_group,public_alias:prod_data.public_alias,prod_id:prod_data.id,qty:prod_qty, prod_merchant_cost:prod_data.prod_merchant_cost};
	//var data = {prod_id:prod_data.id,qty:prod_qty, prod_merchant_cost:prod_data.prod_merchant_cost};
	$.ajax({
		type: "POST",
		dataType:'json',
		url:GLOMP_BASE_URL+"brands/update_cart/",
		data: data,
		success: function(data){
			if(data.status=='success')
			{
				if (data.singbev.brand_id == prod_data.brand_id) {
					if(data.singbev.del_charge !='0' && data.singbev.del_charge !='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.singbev.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.singbev.del_charge=='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}
				

				if (data.valrhona.brand_id == prod_data.brand_id) {
					if(data.valrhona.del_charge !='0' && data.valrhona.del_charge !='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.valrhona.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.valrhona.del_charge=='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}

				if (data.laurent_perrier.brand_id == prod_data.brand_id) {
					if(data.laurent_perrier.del_charge!='0' && data.laurent_perrier.del_charge!='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.laurent_perrier.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.laurent_perrier.del_charge =='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}
				
				if (data.providore.brand_id == prod_data.brand_id) {
					if(data.providore.del_charge !='0' && data.providore.del_charge !='-1')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.providore.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.providore.del_charge=='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}

				if (data.glenfiddich.brand_id == prod_data.brand_id) {

					if(data.glenfiddich.del_charge!='0' && data.glenfiddich.del_charge!='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.glenfiddich.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.glenfiddich.del_charge=='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}

				if (data.moet_chandon.brand_id == prod_data.brand_id) {

					if(data.moet_chandon.del_charge !='0' && data.moet_chandon.del_charge!='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.moet_chandon.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.moet_chandon.del_charge =='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}

				if (data.kusmi_tea.brand_id == prod_data.brand_id) {
					if(data.kusmi_tea.del_charge !='0' && data.kusmi_tea.del_charge!='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.kusmi_tea.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.kusmi_tea.del_charge =='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}

				if (data.euraco.brand_id == prod_data.brand_id) {
					if(data.euraco.del_charge !='0' && data.euraco.del_charge!='-1' )
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html(data.euraco.del_charge);
						$('#delivery_charge_'+prod_data.brand_id).show();
					}
					else if(data.euraco.del_charge =='0')
					{
						$('#delivery_charge_value_'+prod_data.brand_id).html('');
						$('#delivery_charge_'+prod_data.brand_id).hide();
					}
				}
					
				if(prod_qty==0)
				{
					$('#item_wrapper_'+prod_data.id+'_'+prod_data.prod_group).remove();
					var count = $('#brand_wrapper_'+prod_data.brand_id).children().length;
					if(count<=2)
					{
						$('#brand_wrapper_'+prod_data.brand_id).remove();
					}
				}
				$('#session_cart_qty').html(data.cart_qty);
				
				if(data.cart_qty==0)
				{
					$('#session_cart_qty').removeClass('red').addClass('gray');
				}
				else
				{
					$('#session_cart_qty').removeClass('gray').addClass('red');
				}
					
				$('#total_'+prod_data.id+'_'+prod_data.prod_group).html('S$ '+data.total);	
				$('#prod_cost_'+prod_data.id+'_'+prod_data.prod_group).html(data.prod_cost);	

				
				$('#sub_total').html(data.sub_total);
				$('#gst').html(data.gst);
				$('#grand_total').html(data.grand_total);
				$('#grand_total_usd').html(data.grand_total_usd);

				if(data.brand_eror=="" && $('#error_'+(prod_data.brand_id)).is(":visible")  )
				{
					$('#error_'+prod_data.brand_id).html('').hide();
					if(accumulated_brand_list.indexOf(prod_data.brand_id) > -1)
					{
						$.each(accumulated_brand_list, function (i, item) {
							$('#error_'+item).html('').hide();
						});
					}


				}
				if(data.brand_eror!="" && !($('#error_'+(prod_data.brand_id)).is(":visible"))  )
				{
					$('#error_'+prod_data.brand_id).html(data.brand_eror).show();
					if(accumulated_brand_list.indexOf(prod_data.brand_id) > -1)
					{
						$.each(accumulated_brand_list, function (i, item) {
							$('#error_'+item).html(data.brand_eror).show();
						});
					}
				}

				
				$('#loading_dialog').dialog("close").dialog('destroy').remove();
				
				winery_count = data.winery_count;
				if(data.winery_count==1)
				{
					alert_winery();
					$('#checkout').removeClass('btn-custom-green_normal').addClass('btn-custom-white_grey_normal');
				}
				else
				{
					$('#checkout').removeClass('btn-custom-white_grey_normal').addClass('btn-custom-green_normal');
				}
				
				
				$.each(data.winery_data, function(i, item)
				{
					$('#total_'+item.prod_id).html('S$ '+item.prod_total);	
					$('#prod_cost_'+item.prod_id).html(item.prod_cost);	
				});
			}
			else if(data.status=='error')
			{
				
			}
		}
	
	});
}

function add_to_cart_has_option(prod_data)
	{
		var elem="";
		var c=0;
		$.each(items_options[prod_data.id].options.option_list, function(i, option) {
			c++;
			if(c==1)
			{
				elem +='<tr>';
			}
				elem +='<td>\
							<select class="flavors_'+prod_data.id+'" data-flavor="'+option.name+'" style="width:38px;">\
								<option value="0" selected="selected">0</option>\
								<option value="1">1</option>\
								<option value="2">2</option>\
								<option value="3">3</option>\
								<option value="4">4</option>\
								<option value="5">5</option>\
								<option value="6">6</option>\
								<option value="7">7</option>\
								<option value="8">8</option>\
								<option value="9">9</option>\
								<option value="10">10</option>\
								<option value="11">11</option>\
								<option value="12">12</option>\
							</select>\
						</td>\
						<td>&nbsp;'+option.name+'</td>';	
			if(c==1)
			{
				elem +='<td>&nbsp;&nbsp;</td>';
			}
			else
			{
				c=0;
				elem +='</tr>';

			}
		});

		var num = $('#quantity_'+prod_data.id).val();
		var option_count = items_options[prod_data.id].options.option_count;

		var NewDialog = $('	<div id="has_option_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important;text-align:left !important; " align="left">\
			<div><b style="font-size:16px;">'+num+'</b> Regular Cupcakes (Box of '+option_count+')</div>\
			<br><div>Choose your '+option_count+' flavors:</div>\
			<div id="has_option_dialog_error" class="alert alert-error" style="display:none;font-size:12px;" >Chosen flavors must be total of '+option_count+'</div>\
			<table align="center">'+elem+'</table>\
		</div>');
		NewDialog.dialog({                    
			dialogClass:'noTitleStuff dialog_style_glomp_wait',
			title: "",
			autoOpen: false,
			resizable: false,
			modal: true,
			width: 300,
			show: '',
			hide: '',
			buttons: [{text: 'Add to Cart',
					"class": 'btn-custom-blue_xs  ',
					click: function() {

						var total=0;
						var flavors_data=[];
						$(('.flavors_'+prod_data.id)).each(function(i, item)
						{
							var val =parseInt($(this).val());
							if(val>0)
							{
								var data =	{name:$(this).data('flavor'), qty: val };
								flavors_data.push(data);    
							}
							total += val;
						});
						
						if(total!=option_count)
							$("#has_option_dialog_error").show();
						else
						{
							$("#has_option_dialog_error").hide();

							$('#has_option_dialog').dialog("close").dialog('destroy').remove();

							var NewDialog = $('<div id="loading_dialog" align="center" style="">Adding to cart...</div>');
								NewDialog.dialog({
						            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
						            title: '',
						            autoOpen: false,
						            resizable: false,
						            modal: true,
						            width: 200,
						            height: 120,
						            show: '',
						            hide: ''
						        });
								NewDialog.dialog('open');

								
							var prod_qty =num;
							items[prod_data.id].merchant_about="";
							var data = {brandproduct_id:items[prod_data.id].brandproduct_id,public_alias:prod_data.public_alias,prod_id:prod_data.id,qty:prod_qty,prod_merchant_cost:prod_data.prod_merchant_cost,flavors_data:JSON.stringify(flavors_data),prod_data:JSON.stringify(items[prod_data.id])};
							/****/
							/****/
							$.ajax({
								type: "POST",
								dataType:'json',
								url:GLOMP_BASE_URL+"brands/add_to_cart/",
								data: data,
								success: function(data){
									
									if(data.status=='success')
									{

										$('#loading_dialog').html("<div style='height:11px;'  align='center'>Added to cart!</div>");
										setTimeout(function() {
											$('#loading_dialog').dialog("close").dialog('destroy').remove();
										},500);
										/*
										
										*/
										$('#session_cart_qty').html(data.cart_qty);
										if(data.cart_qty==0)
										{
											$('#session_cart_qty').removeClass('red').addClass('gray');
										}
										else
										{
											$('#session_cart_qty').removeClass('gray').addClass('red');
										}
										
									}
									else if(data.status=='error')
									{
										
									}
								}
							
							});





						}
					}},
					{text: "Cancel",
					"class": 'btn-custom-ash_xs w80px',
					click: function() {
						$('#has_option_dialog').dialog("close").dialog('destroy').remove();
					}}
			]
		});
		NewDialog.dialog('open');
	}
function add_to_cart()
{
	$('#cart_'+prod_data.id).html('Adding to cart...').addClass('red');
	$('.glompItemMerchant').addClass('add_to_cart_disable').attr('disabled','disabled');
	
	var prod_qty = $('#quantity_'+prod_data.id).val();	

	items[prod_data.id].merchant_about="";
	items[prod_data.id].prod_details="";

	var data = {brandproduct_id:items[prod_data.id].brandproduct_id,public_alias:prod_data.public_alias,prod_id:prod_data.id,qty:prod_qty,prod_merchant_cost:prod_data.prod_merchant_cost,prod_data:JSON.stringify(items[prod_data.id])};
	/****/
	/****/
	$.ajax({
		type: "POST",
		dataType:'json',
		url:GLOMP_BASE_URL+"brands/add_to_cart/",
		data: data,
		success: function(data){
			
			if(data.status=='success')
			{
				$('#session_cart_qty').html(data.cart_qty);
				if(data.cart_qty==0)
				{
					$('#session_cart_qty').removeClass('red').addClass('gray');
				}
				else
				{
					$('#session_cart_qty').removeClass('gray').addClass('red');
				}
				
				$('.glompItemMerchant').removeAttr('disabled').removeClass('add_to_cart_disable');
				$('#cart_'+prod_data.id).html('Added to cart!').removeClass('red').addClass('blue');
				setTimeout(function() {
					$('#cart_'+prod_data.id).html('Add to Cart').removeClass('blue');
				},250);
			}
			else if(data.status=='error')
			{
				
			}
		}
	
	});
	/****/
	/****/
	
}
function choose_buy_or_glomp()
{
	var NewDialog = $('<div id="" align="center" style="padding-top:5px !important;font-size:14px; height: 15px !important;">\
						Do you want to?\
					</div>');
	NewDialog.dialog({
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		height: 140,
		show: '',
		hide: '',
		buttons: [{text: 'Buy',
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					$(this).dialog("close");
					if(public_alias=='hsbc' || public_alias=='uob' || public_alias=='dbs'){
						buy_this_hsbc();
					}
					else{
						buy_this();
					}
				}},
				{text: "Give",
				"class": 'btn-custom-green_2 w80px',
				click: function() {
					$(this).dialog("close");
					/**glomp function here**/	
					/**glomp function here**/
					glomp_this();
					/**glomp function here**/
					/**glomp function here**/
				}},
				{text: "Cancel",
				"class": 'btn-custom-ash_xs w80px',
				click: function() {
					$(this).dialog("close");
					
					
					
				}}
		]
	});
	NewDialog.dialog('open');
}
function buy_this_hsbc()
{
	var grand_total = $('#grand_total').html();
		var grand_total_usd = $('#grand_total_usd').html();

		/*show_delivery*/
		/*show_delivery*/
		var has_delivery = false;
		var delivery_type='pickup';
		$(('.delivery_charge')).each(function(i, item)
		{
			if($(this).data('delivery_type') == 'delivery')
			{
				has_delivery=true;
				delivery_type='Delivery';
			}
		});
		/*show_delivery*/
		/*show_delivery*/

		var show_delivery = (has_delivery)? '': 'none';
		var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
		<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
			<span id="showErrorMessage_brand"></span>\
		</div>\
		<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
			<input type="hidden" value="buy" name="transaction_type" />\
			<input type="hidden" value="'+delivery_type+'" name="delivery_type" />\
			<input type="hidden" value="'+grand_total+'" name="grand_total" />\
			<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
				<table width="" border="0" padding="0" cellspacing="0">\
					<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
						<td width="" align="left"><input type="text" class="helvetica_font form-control" name="email" value="'+public_email+'"id="email" placeholder="Email" style="height:30px;width:213px;text-align:;left" /></td>\
					</tr>\
					<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
						<td width="" align="left"><input type="text" class="helvetica_font form-control" name="fname" value="'+public_fname+'"id="fname" placeholder="First Name" style="height:30px;width:213px;text-align:;left" /></td>\
					</tr>\
					<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
						<td width="" align="left"><input type="text" class="helvetica_font form-control" name="lname" value="'+public_lname+'" id="lname" placeholder="Last Name" style="height:30px;width:213px;text-align:left" /></td>\
					</tr>\
					<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Contact Number</th>\
						<td width="" align="left"><input type="text" class="helvetica_font form-control" name="contact_num" value="" id="contact_num" placeholder="Contact Number" style="height:30px;width:213px;text-align:left" /></td>\
					</tr>\
					<tr style="display:'+show_delivery+'">\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Delivery Address</th>\
						<td width="" align="left"><textarea name="street_address" id="street_address"  style="display:;width:213px;text-align:left;resize:none"  class="helvetica_font form-control" placeholder="Delivery Address"></textarea></td>\
					</tr>\
                    <tr style="display:'+show_delivery+'">\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Delivery Postcode</th>\
						<td width="" align="left"><input type="text" class="helvetica_font form-control" name="post_code" value="" id="post_code" placeholder="Delivery Postcode" style="margin-top:8px;height:30px;width:213px;text-align:left" /></td>\
					</tr>\
					<tr>\
						<td colspan="2" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
					</tr>\
					<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number<br><small>First 6 digits</small></th>\
						<td width="" align="left">\
							<table>\
								<tr>\
									<td width="1">\
										<input type="text"  class="helvetica_font form-control" name="card_num" maxlength="6" value="" id="card_num_hsbc" placeholder="" style="height:30px;width:70px;text-align:left" />\
									</td>\
								</tr>\
							</table>\
						</td>\
					</tr>\
					<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
					<td width="" align="left"><b>S$ '+grand_total+'</b></td>\
					</tr>\
					<tr>\
						<td colspan="2" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
					</tr>\
					<tr>\
						<td colspan="2" width="" align="center" style="">\
						<img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg" style="width:80%" alt="" />\
						</td>\
					</tr>\
				</table>\
		</form>\
		</div>');
		NewDialog.dialog({                    
			dialogClass:'noTitleStuff dialog_style_glomp_wait',
			title: "",
			autoOpen: false,
			resizable: false,
			modal: true,
			width: 300,
			show: '',
			hide: '',
			buttons: [{text: 'Pay',
					"class": 'btn-custom-blue_xs w80px',
					click: function() {
						if(!glomped)
						{
							glomped=true;
							doPay_hsbc();
						}
						
					}},
					{text: "Cancel",
					"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
					click: function() {
						$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
						
					}}
			]
		});
		NewDialog.dialog('open');
		
		$('#contact_num').mask('00000000');
		$('#post_code').mask('000000');
}
function buy_this()
{
	var grand_total = $('#grand_total').html();
	var grand_total_usd = $('#grand_total_usd').html();

	/*show_delivery*/
	/*show_delivery*/
	var has_delivery = false;
	var delivery_type='pickup';
	$(('.delivery_charge')).each(function(i, item)
	{
		if($(this).data('delivery_type') == 'delivery')
		{
			has_delivery=true;
			delivery_type='Delivery';
		}
	});
	/*show_delivery*/
	/*show_delivery*/

	var show_delivery = (has_delivery)? '': 'none';

	var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
	<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
		<span id="showErrorMessage_brand"></span>\
	</div>\
	<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
		<input type="hidden" value="buy" name="transaction_type" />\
		<input type="hidden" value="'+delivery_type+'" name="delivery_type" />\
		<input type="hidden" value="'+grand_total+'" name="grand_total" />\
		<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
			<table width="" border="0" padding="0" cellspacing="0">\
				<tr>\
						<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
						<td colspan="4"  width="" align="left"><input type="text" class="helvetica_font form-control" name="email" value="'+public_email+'"id="email" placeholder="Email" style="height:30px;width:213px;text-align:;left" /></td>\
					</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
					<td colspan="4" width="" align="left"><input type="text" class="helvetica_font form-control" name="fname" value="'+public_fname+'"id="fname" placeholder="First Name" style="height:30px;width:213px;text-align:;left" /></td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
					<td colspan="4" width="" align="left"><input type="text" class="helvetica_font form-control" name="lname" value="'+public_lname+'" id="lname" placeholder="Last Name" style="height:30px;width:213px;text-align:left" /></td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Contact Number</th>\
					<td colspan="4" width="" align="left"><input type="text" class="helvetica_font form-control" name="contact_num" value="" id="contact_num" placeholder="Contact Number" style="height:30px;width:213px;text-align:left" maxlength="8" /></td>\
				</tr>\
				<tr style="display:'+show_delivery+'">\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Delivery Address</th>\
					<td colspan="4" width="" align="left"><textarea name="street_address" class="form-control" id="street_address"  style=";width:213px;text-align:left;resize:none"  class="helvetica_font" placeholder="Delivery Address"></textarea></td>\
				</tr>\
                <tr style="display:'+show_delivery+'">\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Delivery Postcode</th>\
					<td colspan="4" width="" align="left"><input type="text" class="helvetica_font form-control" name="post_code" value="" id="post_code" placeholder="Delivery Postcode" style="height:30px;width:213px;text-align:left" /></td>\
				</tr>\
				<tr>\
					<td colspan="6" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number</th>\
					<td colspan="4" width="" align="left">\
						<table>\
							<tr>\
								<td width="1">\
									<input type="text"  class="helvetica_font form-control" name="card_num_1" maxlength="4" value="" id="card_num_1" placeholder="" style="height:30px;width:58px;text-align:left" />\
								</td>\
								<td width="1">\
									<div style="color:#fff;margin-top:-8px">-</div>\
								</td>\
								<td width="1">\
									<input type="text"  class="helvetica_font form-control" name="card_num_2" maxlength="6" value="" id="card_num_2" placeholder="" style="height:30px;width:72px;text-align:left" />\
								</td>\
								<td width="1">\
									<div style="color:#fff;margin-top:-8px">-</div>\
								</td>\
								<td width="1">\
									<input type="text"  class="helvetica_font form-control" name="card_num_3" maxlength="5" value="" id="card_num_3" placeholder="" style="height:30px;width:64px;text-align:left" />\
								</td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Expiry Date</th>\
					<td width="1" align="left">\
						<input type="text"  class="helvetica_font form-control" name="mm" value="" id="mm" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
					</td>\
					<td width="1" align="center">\
						&nbsp;<span style="color:#fff">/</span>&nbsp;\
					</td>\
					<td width="1" align="left">\
						<input type="text"  class="helvetica_font form-control" name="yy" value="" id="yy" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
					</td>\
					<td width="" align="left">\
						<span style="color:#fff;font-size:12px"> MM&nbsp;/&nbsp;YY</span>\
					</td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Security Code</th>\
					<td colspan="4" width="" align="left"><input type="text"  class="helvetica_font form-control" name="sec_code" value="" maxlength="4" id="sec_code" placeholder="" style="height:30px;width:80px;text-align:left" /></td>\
				</tr>\
				<tr>\
					<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
					<td colspan="4" width="" align="left" style="color:#495368"><b>S$ '+grand_total+'</b></td>\
				</tr>\
                                <tr class="hide" >\
                                    <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                    <td colspan="4" width="" align="left" style="font-size:10px;color: rgb(108, 108, 108);" class=""><b>USD $ '+ grand_total_usd +'</b></td>\
                                </tr>\
                                <tr class="hide" >\
                                        <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                        <td colspan="4" width="" align="left" style="color: rgb(108, 108, 108);font-size:10px;" class="">Please note: Your card will be billed in USD.</td>\
                                </tr>\
				<tr>\
					<td colspan="6" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
				</tr>\
				<tr>\
					<td colspan="6" width="" align="right" style="">\
					<img src="'+GLOMP_BASE_URL+'assets/frontend/img/dialect.jpg" style="height:30px" alt="" />\
					<img src="'+GLOMP_BASE_URL+'assets/frontend/img/digicert.png" style="height:30px" alt="" />\
					</td>\
				</tr>\
			</table>\
		</div>\
	</form>\
	</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		show: '',
		hide: '',
		buttons: [{text: 'Pay',
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					if(!glomped)
					{
						glomped=true;
						doPay();
					}
					
				}},
				{text: "Cancel",
				"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
				click: function() {
					$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
					
				}}
		]
	});
	NewDialog.dialog('open');
	$('#contact_num').mask('00000000');
	$('#post_code').mask('000000');
}
function doPay_hsbc()
{
	var card_num_hsbc = $("#card_num_hsbc").val();
	if(check_bin_range(card_num_hsbc) || card_num_hsbc=='')
	{
		var NewDialog = $('<div id="loading_dialog" align="center" style="">Please wait...\
			<i class="icon-spinner icon-spin icon-large"></i>\
			</div>');
		NewDialog.dialog({                    
			dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            title: '',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 200,
            height: 120,
            show: '',
            hide: ''
		});
		NewDialog.dialog('open');
		
		$('.cancel_glomp_option').attr('disabled','disabled');
			
        $("#showError_brand").hide();
        $("#showSuccess").hide();
		$(".Loader").show();
		 $.ajax({
            type: "POST",
			dataType:'json',
            url:GLOMP_BASE_URL+"m/brands/glomp_hsbcsg",
            data: $('#frmGlomp_brand').serialize()+'&public_alias='+public_alias,
            success: function(data) {
            	
            	if(data.status=='redirect')
            	{
            		window.location = data.location;
            	}
                else if(data.status=='success')
                {
					
                }
                else if(data.status=='error')
                {
					$('#loading_dialog').dialog("close").dialog('destroy').remove();
                    glomped=false;
                    $('.password_this').val('');                    
                    $(".Loader").hide();
                    $("#showErrorMessage_brand").html(data.data);
					$("#showError_brand").show();
                }
                $('.cancel_glomp_option').removeAttr('disabled');
            }
        
        });
	 }
	 else
	{
		glomped=false;
	}
}
function doPay()
{
	
	var NewDialog = $('<div id="loading_dialog" align="center" style="font-size:12px">\
                          <div align="center" style="margin-top:5px;">Please wait...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                          </div>');
        NewDialog.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            title: '',
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 200,
            height: 120,
            show: '',
            hide: ''
        });
        
	NewDialog.dialog('open');
	
	$('.cancel_glomp_option').attr('disabled','disabled');
	$("#showError_brand").hide();
	$("#showSuccess").hide();
	$(".Loader").show();
	$.ajax({
		type: "POST",
		dataType:'json',
		url:GLOMP_BASE_URL+"brands/glompToMe",
		data: $('#frmGlomp_brand').serialize()+'&public_alias='+public_alias,
		success: function(data){
			if(data.status=='success')
			{
				$('#loading_dialog').dialog("close").dialog('destroy').remove();
				
				$('.cancel_glomp_option').removeAttr('disabled');
				
				window.location = GLOMP_BASE_URL+"m/brands/success_checkout/"+data.data;
				
			}
			else if(data.status=='error')
			{
				$('#loading_dialog').dialog("close").dialog('destroy').remove();
				
				$('.cancel_glomp_option').removeAttr('disabled');
				glomped=false;
				$('.password_this').val('');                    
				$(".Loader").hide();
				$("#showErrorMessage_brand").html(data.data);
				$("#showError_brand").show();
			}
		}
	
	});
}
function glomp_this()
{	
	var NewDialog = $('<div id="" align="center" style="font-size:14px;padding-top:5px !important; height: 15px !important;">\
			Choose options:\
					</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		height:140,
		show: '',
		hide: '',
		buttons: [
				{text: "Email",
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					$(this).dialog("close");
					if(public_alias=='hsbc' || public_alias=='uob' || public_alias=='dbs'){
						glomp_this_to_email_hsbc();
					}
					else{
						glomp_this_to_email();
					}
					
				}},	
				{text: "Cancel",
				"class": 'btn-custom-ash_xs w80px',
				click: function() {
					$(this).dialog("close");
					
				}}
		]
	});
	NewDialog.dialog('open');
}
function glomp_this_to_email_hsbc()
{
	var grand_total = $('#grand_total').html();
	var grand_total_usd = $('#grand_total_usd').html();
	var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
	<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
		<span id="showErrorMessage_brand"></span>\
	</div>\
	<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
		<input type="hidden" value="glomp_to_friend" name="transaction_type" />\
		<input type="hidden" value="Delivery" name="delivery_type" />\
		<input type="hidden" value="'+grand_total+'" name="grand_total" />\
		<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
			<table width="" border="0" padding="0" cellspacing="0">\
				<tr>\
					<td valign="top">\
						<table width="" border="0" padding="0" cellspacing="0">\
							<tr>\
								<th colspan="2" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Pucrchaser Info:</th>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="email" value=""id="email" placeholder="Email" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="fname" value=""id="fname" placeholder="First Name" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="lname" value="" id="lname" placeholder="Last Name" style="height:30px;width:213px;text-align:left" /></td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<hr size="1" style="margin:1px" />\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<table width="" border="0" padding="0" cellspacing="0">\
							<tr>\
								<th colspan="2" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Recipient Info:</th>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="glompee_email" value=""id="email" placeholder="Recipient Email" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="glompee_fname" value=""id="fname" placeholder="Recipient First Name" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="glompee_lname" value="" id="lname" placeholder="Recipient Last Name" style="height:30px;width:213px;text-align:left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Personal Message</th>\
								<td width="" align="left"><textarea class="form-control" name="user_message" id="user_message"  style=";width:213px;text-align:left;resize:none"  class="helvetica_font" placeholder="Personal Message"></textarea></td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<hr size="1" style="margin:1px" />\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<table width="" border="0" padding="0" cellspacing="0">\
							<tr>\
								<th colspan="6" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Card Details:</th>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number<br><small>First 6 digits</small></th>\
								<td colspan="4" width="" align="left">\
									<table>\
										<tr>\
											<td width="1">\
												<input type="text"  class="helvetica_font form-control" name="card_num" maxlength="6" value="" id="card_num_hsbc" placeholder="" style="height:30px;width:58px;text-align:left" />\
											</td>\
										</tr>\
									</table>\
								</td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
								<td colspan="4" width="" align="left"><b>S$ '+grand_total+'</b></td>\
							</tr>\
                                                        <tr class="hide" >\
                                                            <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                            <td colspan="4" width="" align="left" style="font-size:10px;color: rgb(108, 108, 108);" class=""><b>USD $ '+ grand_total_usd +'</b></td>\
                                                        </tr>\
                                                        <tr class="hide" >\
                                                                <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                                <td colspan="4" width="" align="left" style="color: rgb(108, 108, 108);font-size:10px;" class="">Please note: Your card will be billed in USD.</td>\
                                                        </tr>\
							<tr>\
								<td colspan="6" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
							</tr>\
							<tr>\
								<td colspan="6" width="" align="center" style="">\
								<img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg" style="width:80%" alt="" />\
								</td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
			</table>\
		</div>\
	</form>\
	</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		show: '',
		hide: '',
		buttons: [{text: 'Pay',
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					if(!glomped)
					{
						glomped=true;
						doPay_hsbc();
					}
				}},
				{text: "Cancel",
				"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
				click: function() {
					$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
				}}
		]
	});
	NewDialog.dialog('open');
}
function glomp_this_to_email()
{
	var grand_total = $('#grand_total').html();
	var grand_total_usd = $('#grand_total_usd').html();
	var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
	<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
		<span id="showErrorMessage_brand"></span>\
	</div>\
	<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
		<input type="hidden" value="glomp_to_friend" name="transaction_type" />\
		<input type="hidden" value="Delivery" name="delivery_type" />\
		<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
			<table width="" border="0" padding="0" cellspacing="0">\
				<tr>\
					<td valign="top">\
						<table width="" border="0" padding="0" cellspacing="0">\
							<tr>\
								<th colspan="2" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Recipient Info:</th>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="email" value=""id="email" placeholder="Email" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="fname" value=""id="fname" placeholder="First Name" style="height:30px;width:213px;text-align:;left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
								<td width="" align="left"><input type="text" class="helvetica_font form-control" name="lname" value="" id="lname" placeholder="Last Name" style="height:30px;width:213px;text-align:left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Personal Message</th>\
								<td width="" align="left"><textarea class="form-control" name="user_message" id="user_message"  style=";width:213px;text-align:left;resize:none"  class="helvetica_font" placeholder="Personal Message"></textarea></td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<hr size="1" style="margin:1px" />\
					</td>\
				</tr>\
				<tr>\
					<td valign="top">\
						<table width="" border="0" padding="0" cellspacing="0">\
							<tr>\
								<th colspan="6" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Card Details:</th>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number</th>\
								<td colspan="4" width="" align="left">\
									<table>\
										<tr>\
											<td width="1">\
												<input type="text"  class="helvetica_font form-control" name="card_num_1" maxlength="4" value="" id="card_num_1" placeholder="" style="height:30px;width:58px;text-align:left" />\
											</td>\
											<td width="1">\
												<div style="color:#fff;margin-top:-8px">-</div>\
											</td>\
											<td width="1">\
												<input type="text"  class="helvetica_font form-control" name="card_num_2" maxlength="6" value="" id="card_num_2" placeholder="" style="height:30px;width:72px;text-align:left" />\
											</td>\
											<td width="1">\
												<div style="color:#fff;margin-top:-8px">-</div>\
											</td>\
											<td width="1">\
												<input type="text"  class="helvetica_font form-control" name="card_num_3" maxlength="5" value="" id="card_num_3" placeholder="" style="height:30px;width:64px;text-align:left" />\
											</td>\
										</tr>\
									</table>\
								</td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Expiry Date</th>\
								<td width="1" align="left">\
									<input type="text"  class="helvetica_font form-control" name="mm" value="" id="mm" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
								</td>\
								<td width="1" align="center">\
									&nbsp;<span style="color:#fff">/</span>&nbsp;\
								</td>\
								<td width="1" align="left">\
									<input type="text"  class="helvetica_font form-control" name="yy" value="" id="yy" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
								</td>\
								<td width="" align="left">\
									<span style="color:#fff;font-size:12px"> MM&nbsp;/&nbsp;YY</span>\
								</td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Security Code</th>\
								<td colspan="4" width="" align="left"><input type="text"  class="helvetica_font form-control" name="sec_code" value="" maxlength="4" id="sec_code" placeholder="" style="height:30px;width:80px;text-align:left" /></td>\
							</tr>\
							<tr>\
								<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
								<td colspan="4" width="" align="left"><b>S$ '+grand_total+'</b></td>\
							</tr>\
                                                        <tr class="hide" >\
                                                            <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                            <td colspan="4" width="" align="left" style="font-size:10px;color: rgb(108, 108, 108);" class=""><b>USD $ '+ grand_total_usd +'</b></td>\
                                                        </tr>\
                                                        <tr class="hide" >\
                                                                <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                                <td colspan="4" width="" align="left" style="color: rgb(108, 108, 108);font-size:10px;" class="">Please note: Your card will be billed in USD.</td>\
                                                        </tr>\
							<tr>\
								<td colspan="6" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
							</tr>\
							<tr>\
								<td colspan="6" width="" align="right" style="">\
								<img src="'+GLOMP_BASE_URL+'assets/frontend/img/dialect.jpg" style="height:30px" alt="" />\
								<img src="'+GLOMP_BASE_URL+'assets/frontend/img/digicert.png" style="height:30px" alt="" />\
								</td>\
							</tr>\
						</table>\
					</td>\
				</tr>\
			</table>\
		</div>\
	</form>\
	</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		show: '',
		hide: '',
		buttons: [{text: 'Pay',
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					if(!glomped)
					{
						glomped=true;
						doPay();
					}
				}},
				{text: "Cancel",
				"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
				click: function() {
					$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
				}}
		]
	});
	NewDialog.dialog('open');
	
	
}	
function glomp_this_to_fb_hsbc()
{
	FB.ui({method: 'apprequests',
		message: '-'
	}, function(response){
		
		if (typeof response === "undefined") 
			return;
		if (response.length == 0){
			return;
		}
		if(response.to.length> 1)
		{
			 var NewDialog = $('<div id="confirmPopup" align="center">\
									<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">Please select 1 Facebook friend only to glomp!.</span></div>\
									</div>');
				NewDialog.dialog({						
					autoOpen: false,
					closeOnEscape: false ,
					resizable: false,					
					dialogClass:'dialog_style_glomp_wait noTitleStuff',
					title:'',
					modal: true,
					width:360,
					position: 'center',
					buttons: [{text: 'OK',
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
							}}
					]
						
				});
				NewDialog.dialog('open');
		
		}
		else
		{
			var selected_fb_id = response.to[0];
			
			/*create the fb checkout form here*/
			var grand_total = $('#grand_total').html();
			var grand_total_usd = $('#grand_total_usd').html();
			var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
			<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
				<span id="showErrorMessage_brand"></span>\
			</div>\
			<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
				<input type="hidden" value="buy" name="transaction_type" />\
				<input type="hidden" value="glomp_to_fb" name="transaction_type" />\
				<input type="hidden" value="Delivery" name="delivery_type" />\
				<input type="hidden" value="" name="street_address" />\
				<input type="hidden" value="" name="contact_num" />\
				<input type="hidden" value="'+selected_fb_id+'@facebook.com" name="email" />\
				<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
					<table width="" border="0" padding="0" cellspacing="0">\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">FB Friend</th>\
							<td colspan="4" width="" align="left" style="color:#fff;font-size:12px;">Click <a  style="color:#E72525" href="https://facebook.com/'+selected_fb_id+'" target="_blank">here</a> to view your FB friend\'s profile.</td>\
						</tr>\
						<tr>\
							<td colspan="5" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
						</tr>\
						<tr>\
							<td valign="top" colspan="2">\
								<table width="" border="0" padding="0" cellspacing="0">\
									<tr>\
										<th colspan="2" align="left" valign="top" style="color:#fff;font-size:16px; padding-bottom: 5px;">Pucrchaser Info:</th>\
									</tr>\
									<tr>\
										<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Email</th>\
										<td width="" align="left"><input type="text" class="helvetica_font form-control" name="email" value=""id="email" placeholder="Email" style="height:30px;width:213px;text-align:;left" /></td>\
									</tr>\
									<tr>\
										<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">First Name</th>\
										<td width="" align="left"><input type="text" class="helvetica_font form-control" name="fname" value=""id="fname" placeholder="First Name" style="height:30px;width:213px;text-align:;left" /></td>\
									</tr>\
									<tr>\
										<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Last Name</th>\
										<td width="" align="left"><input type="text" class="helvetica_font form-control" name="lname" value="" id="lname" placeholder="Last Name" style="height:30px;width:213px;text-align:left" /></td>\
									</tr>\
									<tr>\
										<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Personal Message</th>\
										<td width="" align="left"><textarea class="form-control" name="user_message" id="user_message"  style=";width:213px;text-align:left;resize:none"  class="helvetica_font" placeholder="Personal Message"></textarea></td>\
									</tr>\
								</table>\
							</td>\
						</tr>\
						<tr>\
							<td colspan="5" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number<br><small>First 6 digits</small></th>\
							<td colspan="4"  width="" align="left">\
								<table>\
									<tr>\
										<td width="1">\
											<input type="text"  class="helvetica_font form-control" name="card_num" maxlength="6" value="" id="card_num_hsbc" placeholder="" style="height:30px;width:58px;text-align:left" />\
										</td>\
									</tr>\
								</table>\
							</td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
							<td colspan="4" width="" align="left"><b>S$ '+grand_total+'</b></td>\
						</tr>\
                                                <tr class="hide" >\
                                                    <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                    <td colspan="4" width="" align="left" style="font-size:10px;color: rgb(108, 108, 108);;" class=""><b>USD $ '+ grand_total_usd +'</b></td>\
                                                </tr>\
                                                <tr class="hide" >\
                                                        <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                        <td colspan="4" width="" align="left" style="color: rgb(108, 108, 108);font-size:10px;" class="">Please note: Your card will be billed in USD.</td>\
                                                </tr>\
						<tr>\
							<td colspan="5" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
						</tr>\
						<tr>\
							<td colspan="5" width="" align="center" style="">\
								<img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg" style="width:80%" alt="" />\
								</td>\
						</tr>\
					</table>\
				</div>\
			</form>\
			</div>');
			NewDialog.dialog({                    
				dialogClass:'noTitleStuff dialog_style_glomp_wait',
				title: "",
				autoOpen: false,
				resizable: false,
				modal: true,
				width: 300,
				show: '',
				hide: '',
				buttons: [{text: 'Pay',
						"class": 'btn-custom-blue_xs w80px',
						click: function() {
							if(!glomped)
							{
								glomped=true;
								doPay_hsbc();
							}
						}},
						{text: "Cancel",
						"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
						click: function() {
							$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
						}}
				]
			});
			NewDialog.dialog('open');
			/*create the fb checkout form here*/
			
			
		}	
		
	});
}
function glomp_this_to_fb()
{
	FB.ui({method: 'apprequests',
		message: '-'
	}, function(response){
		
		if (typeof response === "undefined") 
			return;
		if (response.length == 0){
			return;
		}
		if(response.to.length> 1)
		{
			 var NewDialog = $('<div id="confirmPopup" align="center">\
									<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">Please select 1 Facebook friend only to glomp!.</span></div>\
									</div>');
				NewDialog.dialog({						
					autoOpen: false,
					closeOnEscape: false ,
					resizable: false,					
					dialogClass:'dialog_style_glomp_wait noTitleStuff',
					title:'',
					modal: true,
					width:360,
					position: 'center',
					buttons: [{text: 'OK',
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
							}}
					]
						
				});
				NewDialog.dialog('open');
		
		}
		else
		{
			var selected_fb_id = response.to[0];
			
			/*create the fb checkout form here*/
			var grand_total = $('#grand_total').html();
			var grand_total_usd = $('#grand_total_usd').html();
			var NewDialog = $('<div id="buy_this_dialog" align="center" style="padding-top:15px !important; padding-bottom:0px !important; ">\
			<div class="alert alert-error"  id="showError_brand" style="display:none;font-size:12px" align="left">\
				<span id="showErrorMessage_brand"></span>\
			</div>\
			<form name="frmGlomp_brand" id="frmGlomp_brand" method="post" action="" style="padding:0px;margin:0px">\
				<input type="hidden" value="buy" name="transaction_type" />\
				<input type="hidden" value="glomp_to_fb" name="transaction_type" />\
				<input type="hidden" value="Delivery" name="delivery_type" />\
				<input type="hidden" value="N/A" name="fname" />\
				<input type="hidden" value="N/A" name="lname" />\
				<input type="hidden" value="N/A" name="lname" />\
				<input type="hidden" value="" name="street_address" />\
				<input type="hidden" value="" name="contact_num" />\
				<input type="hidden" value="'+selected_fb_id+'@facebook.com" name="email" />\
				<div class="" style="font-weight:normal;font-size:12px;font-family:Helvetica,Arial,sans-serif;" align="center">\
					<table width="" border="0" padding="0" cellspacing="0">\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">FB Friend</th>\
							<td colspan="4" width="" align="left" style="color:#fff;font-size:12px;">Click <a  style="color:#E72525" href="https://facebook.com/'+selected_fb_id+'" target="_blank">here</a> to view your FB friend\'s profile.</td>\
						</tr>\
						<tr>\
							<td colspan="5" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Card Number</th>\
							<td colspan="4"  width="" align="left">\
								<table>\
									<tr>\
										<td width="1">\
											<input type="text"  class="helvetica_font form-control" name="card_num_1" maxlength="4" value="" id="card_num_1" placeholder="" style="height:30px;width:58px;text-align:left" />\
										</td>\
										<td width="1">\
											<div style="color:#fff;margin-top:-8px">-</div>\
										</td>\
										<td width="1">\
											<input type="text"  class="helvetica_font form-control" name="card_num_2" maxlength="6" value="" id="card_num_2" placeholder="" style="height:30px;width:72px;text-align:left" />\
										</td>\
										<td width="1">\
											<div style="color:#fff;margin-top:-8px">-</div>\
										</td>\
										<td width="1">\
											<input type="text"  class="helvetica_font form-control" name="card_num_3" maxlength="5" value="" id="card_num_3" placeholder="" style="height:30px;width:64px;text-align:left" />\
										</td>\
									</tr>\
								</table>\
							</td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Expiry Date</th>\
							<td width="1" align="left">\
								<input type="text"  class="helvetica_font form-control" name="mm" value="" id="mm" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
							</td>\
							<td width="1" align="center">\
								&nbsp;<span style="color:#fff">/</span>&nbsp;\
							</td>\
							<td width="1" align="left">\
								<input type="text"  class="helvetica_font form-control" name="yy" value="" id="yy" placeholder="" maxlength="2" style="height:30px;width:50px;text-align:left" />\
							</td>\
							<td width="" align="left">\
								<span style="color:#fff;font-size:12px"> MM&nbsp;/&nbsp;YY</span>\
							</td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Security Code</th>\
							<td colspan="4" width="" align="left"><input type="text"  class="helvetica_font form-control" name="sec_code" value="" maxlength="4" id="sec_code" placeholder="" style="height:30px;width:80px;text-align:left" /></td>\
						</tr>\
						<tr>\
							<th width="80"align="left" valign="top" style="color:#fff;font-size:12px;">Purchase Amount</th>\
							<td colspan="4" width="" align="left"><b>S$ '+grand_total+'</b></td>\
						</tr>\
                                                <tr class="hide" >\
                                                    <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                    <td colspan="4" width="" align="left" style="font-size:10px;color: rgb(108, 108, 108);;" class=""><b>USD $ '+ grand_total_usd +'</b></td>\
                                                </tr>\
                                                <tr class="hide" >\
                                                        <th width="80" align="left" valign="top" style="color:#fff;font-size:10px;"></th>\
                                                        <td colspan="4" width="" align="left" style="color: rgb(108, 108, 108);font-size:10px;" class="">Please note: Your card will be billed in USD.</td>\
                                                </tr>\
						<tr>\
							<td colspan="5" width="" align="left" style="height:10px;"><div style="border-bottom:solid 1px #fff"></div></td>\
						</tr>\
						<tr>\
							<td colspan="5" width="" align="right" style="">\
							<img src="'+GLOMP_BASE_URL+'assets/frontend/img/dialect.jpg" style="height:30px" alt="" />\
							<img src="'+GLOMP_BASE_URL+'assets/frontend/img/digicert.png" style="height:30px" alt="" />\
							</td>\
						</tr>\
					</table>\
				</div>\
			</form>\
			</div>');
			NewDialog.dialog({                    
				dialogClass:'noTitleStuff dialog_style_glomp_wait',
				title: "",
				autoOpen: false,
				resizable: false,
				modal: true,
				width: 300,
				show: '',
				hide: '',
				buttons: [{text: 'Pay',
						"class": 'btn-custom-blue_xs w80px',
						click: function() {
							if(!glomped)
							{
								glomped=true;
								doPay();
							}
						}},
						{text: "Cancel",
						"class": 'btn-custom-ash_xs w80px cancel_glomp_option',
						click: function() {
							$('#buy_this_dialog').dialog("close").dialog('destroy').remove();
						}}
				]
			});
			NewDialog.dialog('open');
			/*create the fb checkout form here*/
			
			
		}	
		
	});
}
function alert_winery()
{
	var NewDialog = $('<div id="" align="center" style="padding-top:5px !important; font-size:14px">\
					<p align="justify">Note: You need atleast two(2) bottles of winery products before proceeding to checkout.</p>\
					</div>');
	NewDialog.dialog({                    
		dialogClass:'noTitleStuff dialog_style_glomp_wait',
		title: "",
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 300,
		height:120,
		show: '',
		hide: '',
		buttons: [
				{text: "Okay",
				"class": 'btn-custom-blue_xs w80px',
				click: function() {
					$(this).dialog("close");
				}}
		]
	});
	NewDialog.dialog('open');
}

function alert_winer_welcome_pop(text)
{
	if(text!='')
	{
		var NewDialog = $('<div id="" align="center" style="padding-top:5px !important; font-size:14px">\
						<p align="justify">'+text+'</p>\
						</div>');
		NewDialog.dialog({                    
			dialogClass:'noTitleStuff dialog_style_glomp_wait',
			title: "",
			autoOpen: false,
			resizable: false,
			modal: true,
			width: 300,
			show: '',
			hide: '',
			buttons: [
					{text: "OK",
					"class": 'btn-custom-blue_xs w80px',
					click: function() {
						$(this).dialog("close");
					}}
			]
		});
		NewDialog.dialog('open');
	}
}
function compute_grand_total()
    {
    	var brands=[];
    	$(('.delivery_charge')).each(function(i, item)
		{
			var delivery_charge = $(this).data('delivery_charge');
			var delivery_type = $(this).data('delivery_type');
			var brand_id = $(this).data('id');
			var temp={};

			temp.delivery_type=delivery_type;
			temp.brand_id = brand_id;
			brands.push(temp);    
		});
    	var data={brands:JSON.stringify(brands), public_alias:public_alias};
    	$.ajax({
			type: "POST",
			dataType:'json',
			url:GLOMP_BASE_URL+"brands/compute_grand_total/",
			data: data,
			success: function (data) {				
				if(data.status=='success')
				{
					$('#sub_total').html(data.sub_total);
					$('#gst').html(data.gst);
					$('#grand_total').html(data.grand_total);
					$('#grand_total_usd').html(data.grand_total_usd);
				}
			}
		});
    }


    function check_bin_range(card_num)
	{
		if(public_alias=='hsbc')
		{
			var range = [
		 		'436324',
				'451297',
				'455227',
				'455228',
				'483585',
				'489085',
				'492160',
				'496645',
				'512043',
				'539700',
				'541287',
				'544234',
				'458555',
				'458556',
				'458557',
				'458558',
				'436326',
				'436327'
			];
		}
		else if(public_alias=='uob')
		{
			var range = [
				'376316',
				'376317',
				'377149',
				'622433',
				'356240',
				'356241',
				'356243',
				'426588',
				'400682',
				'407514',
				'418238',
				'433241',
				'438174',
				'438202',
				'438213',
				'454182',
				'454183',
				'454718',
				'456598',
				'471542',
				'486374',
				'493725',
				'494084',
				'515705',
				'531218',
				'531219',
				'540191',
				'540543',
				'541126',
				'542125',
				'552163',
				'552253',
				'552592',
				'558957',
				'559221',
				'493725',
			];
		} else {
			var range = [
				'411911',
				'425895',
				'455621',
				'455622',
				'479134',
				'448533',
				'419031',
				'558702',
				'558804',
				'540571',
				'552038',
				'540042',
				'542089',
				'377910',
				'377911',
				'554827',
				'526471',
				'554797',
				'428425',
				'409636',
				'462845',
				'476073',
				'453950',
				'540804',
				'492103',
				'541819',
				'465884',
				'465883',
				'451835',
				'451834',
				'463237',
			];
		}
		
		if( range.indexOf(card_num) == -1)
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}