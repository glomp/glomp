/*
 * Start code
 * 
 * 
 */
function facebook_alert_error()
{
    return true;
/*
    try
    {    
        facebookDialog.dialog("close");
    }
    catch(e){
    }
    var NewDialog = $(' <div  align="center"><div align="center" style="margin-top:5px;" ><p style="text-align:justify">Due to new changes in Facebook policy, we are not able to connect to your Facebook account here at present.</p><p style="text-align:justify">We will have this reinstated very shortly so please try again soon. Sorry for the inconvenience caused.</p></div></div>');
    NewDialog.dialog({						
        autoOpen: false,
        closeOnEscape: false ,
        resizable: false,					
        dialogClass:'noTitleStuff dialog_style_glomp_wait',
        title:'',
        modal: true,
        position: 'center center',
        width:280,
        height:185,
        buttons: [	
            {
                text: "Ok",
                "class": 'btn-custom-ash_xs w100px',
                click: function() {
                    $( this ).dialog( "close" );
                }
            }			
        ]
    });
    NewDialog.dialog('open');                        
    return false;
    */
}
function save_system_log(logs) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        async: false,
        url: GLOMP_DESK_SITE_URL + 'ajax_post/save_system_log',
        data: {
            logs: logs
        }
    });
}
$.fn.loadLoader = function() {
        
    var popup_append='<div class="loader_background"></div>';
	$("body").append(popup_append);
	
	var _height_of_container =  $(".redeem").height();
	$(".loader_background").css({"height":$(document).height()})
	var popup_append_inner='<div class="loader_background_loading_images">&nbsp;</div>';
	$(".loader_background").append(popup_append_inner);
	var _y = ($(window).height()/2)-(parseInt($(".loader_background_loading_images").css("height"))/2);
	var _x = ($(window).width()/2)-(parseInt($(".loader_background_loading_images").css("width"))/2);	
	$(".loader_background_loading_images").css({"margin-top":_height_of_container-_y,"margin-left":_x}); 
    }

$.fn.removeLoadLoader = function() {
	$(".loader_background_loading_images").remove();
	$(".loader_background").remove();
}

/* Generic functions */
