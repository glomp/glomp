<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-26-2013
*/
require_once('super_model.php');	
class Merchant_m extends Super_model
{
	protected $INSTACNE_NAME="gl_merchant";
	protected $INSTACNE_MERCHANT_USERS="gl_merchant_users";
	protected $INSTACNE_OUTLET="gl_outlet";
	protected $INSTACNE_ADDRESS="gl_address";
protected $INSTANCE_REGION="gl_region";
  
    function __construct() {
       parent::__construct('gl_merchant merchant');
   }
   
   function delete_prefix($prefix_id)
    {
        $sql="DELETE FROM gl_region_oulet_codes WHERE id='".$prefix_id."'";
		$result=$this->db->query($sql);        
    }
    function generate_outlet_code($region_id=0)
    {
        $code="";
        if($region_id>0)
        {
        $sql="select * from  gl_region_oulet_codes WHERE region_id='".$region_id."'";
            $result=$this->db->query($sql);                    
            if($result->num_rows()>0)
            {
                foreach($result->result() as $prefix)
                {
                    $code="";
                    $sql="select * from gl_outlet WHERE outlet_code LIKE '".$prefix->prefix_outlet_code."%' ORDER BY outlet_code DESC LIMIT 1";
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0)
                    {
                       $res=$result->row();
                        $code_suffix= substr($res->outlet_code,3)+1;
                        if($code_suffix>999)
                        {
                            $code="prefix_reached_max";
                        }
                        else
                        {
                            $zeroes="00000";
                            $code_suffix=substr($zeroes,0,(3-strlen($code_suffix))).$code_suffix;
                            $code=$prefix->prefix_outlet_code.$code_suffix;
                        }
                    }
                    else
                    {
                        $code=$prefix->prefix_outlet_code."000";
                    }
                    if($code!='prefix_reached_max')
                    {
                       break;
                    }
                }                        
            }
            else
            {
                $code="no_prefix";
            } 
        }
        return $code;        
    }
    function check_if_prefix_in_use($prefix)
    {
        $sql="select * from gl_outlet WHERE outlet_code LIKE '".$prefix."%'";
		$result=$this->db->query($sql);
        return $result->num_rows();
    }
    function get_prefix_outlet_codes($region_id=0)
    {
        if($region_id>0)
        {
            $sql="select * from gl_region_oulet_codes WHERE region_id='".$region_id."'";
            return $this->db->query($sql);
        }
        else
            return null;
    }
    function get_this_prefix_code($prefix, $region_id=0)
    {
        if($region_id==0)
            $sql="select * from gl_region_oulet_codes WHERE prefix_outlet_code='".$prefix."'";
        else
            $sql="select * from gl_region_oulet_codes WHERE prefix_outlet_code='".$prefix."' AND region_id <>'".$region_id."'";
		$result=$this->db->query($sql);
        return $result->num_rows();
    }
	
	function selectOutletsAddress($merchant_id)
	{
		
		$sql="SELECT  ".$this->INSTACNE_ADDRESS." .*,".$this->INSTACNE_OUTLET." .*, ".$this->INSTANCE_REGION.".region_name  from ".$this->INSTACNE_OUTLET." 
		inner join  ".$this->INSTACNE_ADDRESS." ON ".$this->INSTACNE_OUTLET.".outlet_address_id=".$this->INSTACNE_ADDRESS.".address_id
		inner join  ".$this->INSTANCE_REGION." ON ".$this->INSTACNE_OUTLET.".outlet_country_region_id=".$this->INSTANCE_REGION.".region_id
		WHERE ".$this->INSTACNE_OUTLET.".outlet_merchant_id= '".$merchant_id."' ";
//                echo $sql;
//                exit;
        $params = array(
            'cache_name' => 'selectOutletsAddress_'.$merchant_id, #unique name
            'cache_table_name' => array('gl_outlet', 'gl_address'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$result=$this->db->query($sql);
        $result = get_create_cache($params);   
		
		return $result;
	}
	function selectOutletsAddress_2($merchant_id)
	{
		$sql="SELECT  ".$this->INSTACNE_ADDRESS." .*, gr1.region_name, gr2.region_name outlet_region, gl_outlet.outlet_name
                    FROM ".$this->INSTACNE_OUTLET." 
		inner join  ".$this->INSTACNE_ADDRESS." ON ".$this->INSTACNE_OUTLET.".outlet_address_id=".$this->INSTACNE_ADDRESS.".address_id
		inner join  ".$this->INSTANCE_REGION." gr1 ON ".$this->INSTACNE_OUTLET.".outlet_country_region_id=gr1.region_id
		inner join  ".$this->INSTANCE_REGION." gr2 ON ".$this->INSTACNE_OUTLET.".outlet_region_id=gr2.region_id
		WHERE ".$this->INSTACNE_OUTLET.".outlet_merchant_id= '".$merchant_id."' ";
//                echo $sql;
//                exit;
		$result=$this->db->query($sql);
		return $result;
	}
	function selectMerchants($start=0,$per_page=0, $subQuery="",$search_q="")
	{
		$limit_query="";		
		if($per_page>0)
		$limit_query=" limit $start, $per_page";
        
        $sql_add="";
        $sql_search_q="";
        if($search_q!="")
        {
            $sql_search_q="(merchant_name LIKE '%".$search_q."%') AND ";
        }
		
		//$sql="select * from $this->INSTACNE_NAME  WHERE ".$subQuery."  merchant_status != 'Deleted'  order by merchant_rank asc $limit_query";
        $sql="select * from $this->INSTACNE_NAME 
            JOIN $this->INSTANCE_REGION 
            ON $this->INSTACNE_NAME.merchant_region_id =$this->INSTANCE_REGION.region_id
            WHERE ".$subQuery." ".$sql_search_q."   merchant_status != 'Deleted'  order by merchant_rank asc $limit_query";
		$q=$this->db->query($sql);
		return $q;
	}
    function getLocationIDOfThisMerchant($merchant_id)
	{
        $sql="select merchant_region_id from $this->INSTACNE_NAME  WHERE  merchant_id = '".$merchant_id."'";    
        $result=$this->db->query($sql);                
        return $result->row()->merchant_region_id;
    }
	function selectMerchantID($merchant_id)
	{
		$merchant_id = (int) $merchant_id;
        $sql="select * from $this->INSTACNE_NAME  WHERE  merchant_id = '".$merchant_id."'";
		$params = array(
            'cache_name' => 'selectMerchantID_'.$merchant_id, #unique name
            'cache_table_name' => array('gl_merchant'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$prod_result = $this->db->query($sql);
        return get_create_cache($params);
	}
	function listMerchantOutlets($merchant_id){
		$merchant_id = (int) $merchant_id;
		$where = array('outlet_merchant_id'=>$merchant_id);				
		return $this->db->get_where($this->INSTACNE_OUTLET,$where);
	}
	function getAddress($address_id)
	{
		$address_id = (int) $address_id;
		$where = array('address_id'=>$address_id);
		return $this->db->get_where($this->INSTACNE_ADDRESS,$where);
	}
	
	function delete($contentID)
	{
		
		$data = array('merchant_status'=>'Deleted');
		$where=array(
		'merchant_id'=>$contentID
		);
		$this->db->update($this->INSTACNE_NAME,$data,$where);
		
		
	}
	 function generate_hash($length = 8) {
        $chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789$%^!@&*';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }
	function hash_key_value($salt, $password) {
        $hash_password = hash('SHA256', $salt . $password);
        return $hash_password;
    }
	function addRecord()
	{	
		//FIRST  INSERT THE ADDRESS 
		$contentTemp=array(
			'address_1'=>$this->input->post('merchant_address_1'),
			'address_2'=>$this->input->post('merchant_address_2'),
			'address_street'=>$this->input->post('merchant_street'),
			'address_locality_name'=>$this->input->post('merchant_locality_name'),
			'address_locality'=>$this->input->post('merchant_locality'),
			'address_city_town'=>$this->input->post('merchant_city'),
			'address_region_name'=>$this->input->post('merchant_region_name'),
			'address_region'=>$this->input->post('region_id'),
			'address_zip_code'=>$this->input->post('merchant_zip'),
			'address_country_id'=>$this->input->post('merchant_country')
			);
		$this->db->insert($this->INSTACNE_ADDRESS,$contentTemp);
		$merchant_address_id=$this->db->insert_id();				
		
		$merchant_salt = $this->generate_hash();
        $merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('pword'));
		
			$content=array(
			'merchant_password'=>$merchant_password,
			'merchant_salt'=>$merchant_salt,
            'merchant_username'=>$this->input->post('merchant_username'),
			'merchant_contact_name'=>$this->input->post('merchant_contact_name'),
			'accounts_contact_name'=>$this->input->post('accounts_contact_name'),
			'accounts_contact_email'=>$this->input->post('accounts_contact_email'),
			'notify_excempt_amount'=>$this->input->post('notify_excempt_amount'),
			'notify_top_limit'=>$this->input->post('notify_top_limit'),
			'merchant_name'=>$this->input->post('merchant_name'),
			'merchant_email'=>$this->input->post('merchant_email'),
			'merchant_region_id'=>$this->input->post('region_id'),
			'merchant_contact'=>$this->input->post('merchant_contact'),
			'merchant_country_region_id'=>$this->input->post('merchant_country'),
			'merchant_address_id'=>$merchant_address_id,
			'merchant_fax'=>$this->input->post('merchant_fax'),
			'merchant_details'=>$this->input->post('merchant_details'),
			'merchant_about'=>$this->input->post('merchant_about'),
			'merchant_terms'=>$this->input->post('merchant_terms'),
			'merchant_created_date'=>date('Y-m-d H:i:s'),
			'merchant_status'=>'Active',
			'updated_by_id' => $this->session->userdata('admin_id')
			);
			$this->db->insert($this->INSTACNE_NAME,$content);
			
			$inserted_id=$this->db->insert_id();
            
            $rank= $this->input->post('merchant_rank');
            if($rank<$inserted_id)
               $rank=$inserted_id;
            
            $this->updateMerchantRanking($inserted_id, $rank);
            
            
            //memcached clear
            
            $params = array(
                'affected_tables' 
                    => array(
                        'gl_region',
                        'gl_merchant'
                    ) #cache name                
            );
            delete_cache($params);                
            //memcached clear
            
            return $inserted_id;
		
	}
	function updateLogo($logo_name,$merchant_id,$old_image='')
	{
		$data = array('merchant_logo'=>$logo_name);
		$where = array('merchant_id'=>$merchant_id);
		$this->db->update($this->INSTACNE_NAME,$data,$where);
		if($old_image!="")
			@unlink('custom/uploads/merchant/'.$old_image);
				if($old_image!="")
			@unlink('custom/uploads/merchant/thumb/'.$old_image);

		
	}
	
	function editRecord($contentID=0)
	{
		$merchant_status = !isset($_POST['merchant_status'])?'InActive':$_POST['merchant_status'];
		$merchant_address_id=$this->input->post('merchant_address_id');
		
		//FIRST  update THE ADDRESS 
		$contentTemp=array(
			'address_1'=>$this->input->post('merchant_address_1'),
			'address_2'=>$this->input->post('merchant_address_2'),
			'address_street'=>$this->input->post('merchant_street'),
			'address_locality_name'=>$this->input->post('merchant_locality_name'),
			'address_locality'=>$this->input->post('merchant_locality'),
			'address_city_town'=>$this->input->post('merchant_city'),
			'address_region_name'=>$this->input->post('merchant_region_name'),
			'address_region'=>$this->input->post('region_id'),
			'address_zip_code'=>$this->input->post('merchant_zip'),
			'address_country_id'=>$this->input->post('merchant_country')
			);		
		
		$where=array(
		'address_id'=>$merchant_address_id
		);
		$this->db->update($this->INSTACNE_ADDRESS,$contentTemp,$where);		
		
		
		
		
		
		$content=array(
			'merchant_name'=>$this->input->post('merchant_name'),
                        'merchant_contact_name'=>$this->input->post('merchant_contact_name'),
			'accounts_contact_name'=>$this->input->post('accounts_contact_name'),
			'accounts_contact_email'=>$this->input->post('accounts_contact_email'),
			'notify_excempt_amount'=>$this->input->post('notify_excempt_amount'),
			'notify_top_limit'=>$this->input->post('notify_top_limit'),
			'merchant_email'=>$this->input->post('merchant_email'),
			'merchant_region_id'=>$this->input->post('region_id'),
			'merchant_contact'=>$this->input->post('merchant_contact'),			
			'merchant_fax'=>$this->input->post('merchant_fax'),
			'merchant_details'=>$this->input->post('merchant_details'),
			'merchant_about'=>$this->input->post_vars['merchant_about'],
			'merchant_terms'=>$this->input->post('merchant_terms'),
            'merchant_username'=>$this->input->post('merchant_username'),
			'merchant_status'=>$merchant_status,
			'updated_by_id' => $this->session->userdata('admin_id')
			
			);
		$where=array(
		'merchant_id'=>$contentID
		);
		$this->db->update($this->INSTACNE_NAME,$content,$where);
		
		if( $_POST['npword']!="" ){

			$merchant_salt = $this->generate_hash();
			$merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('npword'));
					
			$content=array(
						'merchant_password'=>$merchant_password,
						'merchant_salt'=>$merchant_salt
			);
			
			$where=array(
			'merchant_id'=>$contentID
			);
			$this->db->update($this->INSTACNE_NAME,$content,$where);
		}
        $this->updateMerchantRanking($contentID, $this->input->post('merchant_rank'));
        
        
        //memcached clear            
        $params = array(
            'affected_tables' 
                => array(
                    'gl_region',
                    'gl_merchant',
                    'gl_favourite',
                    'gl_outlet',                    
                ), #cache name   
                'specific_names'
                => array(
                    'selectMerchantID_'.$contentID,
                    'product_by_merchant_id_'.$contentID,
                )                
        );
        delete_cache($params);                
        //memcached clear
		
	}
    function updateRanking($sortedIDs,$location_id)
    {
    
        //first get all the old ranking list                    
        $sql="select merchant_id, merchant_rank from $this->INSTACNE_NAME             
        WHERE merchant_id IN (".$sortedIDs.") ORDER BY merchant_rank asc";            
        
		$result=$this->db->query($sql);        
        $ctr=0;
        echo $sortedIDs;
        $sortedIDs= explode(",",$sortedIDs);                        
        foreach($result->result() as $row)
        {
        
            if($row->merchant_id != $sortedIDs[$ctr])
            {
                $content=array(
                    'merchant_rank'=>$row->merchant_rank
                );			
                $where=array(
                    'merchant_id'=>$sortedIDs[$ctr]
                );
                $this->db->update($this->INSTACNE_NAME,$content,$where);
                echo "asd";             
            }
            $ctr++;           
        }
        
        //memcached clear     
        if($location_id>0)
        {            
            $params = array(                
                'specific_names'
                => array(
                    'regionwise_merchant_'.$location_id                    
                )                
            );                
        }
        else
        {
            $params = array(
                'affected_tables' 
                    => array(                
                        'gl_merchant'
                    ) #cache name                   
            );
        }
        delete_cache($params);                
        //memcached clear
        
    }
    function updateMerchantRanking($merchant_id, $rank)
    {
        $rank=(int) $rank;
        //get old rank        
        $sql="SELECT merchant_rank from $this->INSTACNE_NAME WHERE merchant_id=".$merchant_id." ";            
        $result = $this->db->query($sql);
        $result = $result->row();        
        if($rank >= $result->merchant_rank)
        {
            //rank up
            
            $this->doRecursiveMerchantRankUpdate($merchant_id, $rank, 1);
            
        }
        else if($rank < $result->merchant_rank)
        {
            //rank down
            $this->doRecursiveMerchantRankUpdate($merchant_id, $rank, 1);
        }
        
    }
    function doRecursiveMerchantRankUpdate($merchant_id=0, $rank, $way)
    {
        if($merchant_id>0)
        {
            $content=array(
                'merchant_rank'=>$rank
			);			
			$where=array(
                'merchant_id'=>$merchant_id
			);
			$this->db->update($this->INSTACNE_NAME,$content,$where);
            
            //new
            //get region id of this merchant
            $sql="SELECT merchant_region_id from gl_merchant WHERE merchant_id='".$merchant_id."'";            
            $result = $this->db->query($sql);
            $region_id = $result->row()->merchant_region_id;
            
            
            $sql="SELECT merchant_id from gl_merchant WHERE merchant_region_id='".$region_id."' ORDER BY merchant_rank ASC, merchant_last_updated DESC";            
            $result = $this->db->query($sql);
            $ctr=0;
            foreach($result->result() as $row)
            {
                $ctr++;
                $content=array(
                    'merchant_rank'=>($ctr)
                );
                $where=array(
                    'merchant_id'=>$row->merchant_id
                );
                $this->db->update($this->INSTACNE_NAME,$content,$where);                
            }
            //new

            /*/old
            //check if there is an existings merhcant with that rank
            $sql="SELECT merchant_id from gl_merchant WHERE merchant_id<>'".$merchant_id."' AND merchant_rank='".$rank."'";            
            $result = $this->db->query($sql);            
            $first="";
            foreach($result->result() as $row)
            {
                if($first=="")
                    $first=$row;
                $content=array(
                    'merchant_rank'=>($rank+$way)
                );
                $where=array(
                    'merchant_id'=>$row->merchant_id
                );
                $this->db->update($this->INSTACNE_NAME,$content,$where);                
            }            
            if($first!="")
            {
                 $this->doRecursiveMerchantRankUpdate($first->merchant_id, ($rank+$way), $way);            
            }
            //old*/
            
        }
        
        
    }
		function merchantDropDown($merchant_id)
		{
			$sql = "SELECT * FROM ".$this->INSTACNE_NAME." ORDER BY merchant_name";
			$result = $this->db->query($sql);
			$list = "";
			foreach($result->result() as $row){
                if( $row->merchant_status == 'Active' ) {
                    $active[$row->merchant_id] = $row->merchant_name;
                } else {
                    $inactive[$row->merchant_id] = $row->merchant_name;
                }
			}
            foreach( $active as $id => $name ) {
                $selected = ($merchant_id==$id)?'selected="selected"':"";
                $list .= "<option value='".$id."' $selected>".$name."</option>";
            }
            if( count($inactive) ) {
                $list .= '<optgroup label="- Inactive Merchants -">';
                foreach( $inactive as $id => $name ) {
                    $selected = ($merchant_id==$id)?'selected="selected"':"";
                    $list .= "<option value='".$id."' $selected>".$name."</option>";
                }
                $list .= '</optgroup>';
            }

			return $list;
			}
			
		
		function outletDropDown($merchant_id,$outletID)
		{
			$sql = "SELECT * FROM gl_outlet  WHERE outlet_merchant_id = '$merchant_id' ORDER BY outlet_name ASC";
			$result = $this->db->query($sql);
			$list = "";
			foreach($result->result() as $row){
				$selected = ($outletID==$row->outlet_id)?'selected="selected"':"";
			$list .= "<option value='".$row->outlet_id."' $selected>".$row->outlet_name."</option>";
			}
			return $list;
			}
	function selectVoucher($subquery)
		{
		$sql= "SELECT * FROM gl_voucher WHERE 1=1 $subquery";
		return $result = $this->db->query($sql);
		}
			
/**********************************outlet of merchant******************************/
	function addOutlet($merchant_id)
	{
		//FIRST  INSERT THE ADDRESS 
		$contentTemp=array(
			'address_1'=>$this->input->post('outlet_address_1'),
			'address_2'=>$this->input->post('outlet_address_2'),
			'address_street'=>$this->input->post('outlet_street'),
			'address_locality_name'=>$this->input->post('outlet_locality_name'),
			'address_locality'=>$this->input->post('outlet_locality'),
			'address_city_town'=>$this->input->post('outlet_city'),
			'address_region_name'=>$this->input->post('outlet_region_name'),
			'address_region'=>$this->input->post('outlet_region_id'),
			'address_zip_code'=>$this->input->post('outlet_zip'),
			'address_country_id'=>$this->input->post('outlet_country')
			);
		$this->db->insert($this->INSTACNE_ADDRESS,$contentTemp);		
		$outlet_address_id=$this->db->insert_id();
		
		$data = array('outlet_merchant_id'=>$merchant_id,
			'outlet_name'=>$this->input->post('outlet_name'),
			'outlet_code'=>$this->input->post('outlet_code'),
            'outlet_phone_number'=>$this->input->post('outlet_phone_number'),
			'outlet_added'=>date('Y-m-d H:i:s'),
			'outlet_status'=>'Active',
			'updated_by_id' => $this->session->userdata('admin_id'),
			'outlet_country_region_id'=>$this->input->post('outlet_country'),
			'outlet_region_id'=>$this->input->post('outlet_region_id'),
			'outlet_address_id'=>$outlet_address_id
			);			
		$this->db->insert($this->INSTACNE_OUTLET,$data);
		
		}
	function selectOutlet($merchant_id)
	{
		$where = array(
            'outlet_merchant_id'=>$merchant_id,
            'outlet_status'=>'Active'
        );
		$result = $this->db->get_where($this->INSTACNE_OUTLET,$where);
		return $result;
	}
	
	function selectOutletByOutletID($outLetID)
	{
		$where = array(
            'outlet_id'=>$outLetID,
            'outlet_status'=>'Active'
        );
		$result = $this->db->get_where($this->INSTACNE_OUTLET,$where);
		return $result;
	}
	
	function deleletOutlet($outlet_id)
	{
        /*$sql = "DELETE FROM ".$this->INSTACNE_OUTLET." WHERE outlet_id = '$outlet_id'";
		$result = $this->db->query_delete($sql);
		return $result['status'];
        */
        $data = array('outlet_status' => 'Deleted');
        $where = array(
            'outlet_id' => $outlet_id
        );
        $this->db->update($this->INSTACNE_OUTLET, $data, $where);
	}
	function outletById($outlet_id)
	{
        $data = array('outlet_id'=>$outlet_id);
		$result = $this->db->get_where($this->INSTACNE_OUTLET,$data);
		return $result;
	}
	function updateOutlet($outlet_id)
	{			
		//FIRST  update THE ADDRESS 
		$outlet_address_id=$this->input->post('outlet_address_id');		
		$contentTemp=array(
			'address_1'=>$this->input->post('outlet_address_1'),
			'address_2'=>$this->input->post('outlet_address_2'),
			'address_street'=>$this->input->post('outlet_street'),
			'address_locality_name'=>$this->input->post('outlet_locality_name'),
			'address_locality'=>$this->input->post('outlet_locality'),
			'address_city_town'=>$this->input->post('outlet_city'),
			'address_region_name'=>$this->input->post('outlet_region_name'),
			'address_region'=>$this->input->post('outlet_region_id'),
			'address_zip_code'=>$this->input->post('outlet_zip'),
			'address_country_id'=>$this->input->post('outlet_country')
			);
			
		$where=array(
		'address_id'=>$outlet_address_id
		);
		$this->db->update($this->INSTACNE_ADDRESS,$contentTemp,$where);
		
		$data = array('outlet_name'=>$this->input->post('outlet_name'),
                    'outlet_code'=>$this->input->post('outlet_code'),
                    'outlet_phone_number'=>$this->input->post('outlet_phone_number'),
                    'updated_by_id' => $this->session->userdata('admin_id'),
                    'outlet_country_region_id'=>$this->input->post('outlet_country'),
                    'outlet_region_id'=>$this->input->post('outlet_region_id')
                );
//                echo $outlet_id;
//                echo '<pre>';
//                print_r($data);
//                exit;
		$where = array('outlet_id'=>$outlet_id);
		
		$this->db->update($this->INSTACNE_OUTLET,$data,$where);
		
	}
	function is_mercahnt($merchant_id)
	{
			$sql = "SELECT merchant_id FROM gl_merchant WHERE merchant_id = '$merchant_id'";	
	$res = $this->db->query($sql);
	if($res->num_rows($res)>0)
		return true;
	else
		return false;
	}
/**********************implemented in fornt-end******************/
function get_region_path($region_id)
{
	$where = array('region_id'=>$region_id)	;
	$res = $this->db->get_where('gl_region',$where);
	if($res->num_rows()>0)
	{
		$rec = $res->row();
		return $rec->region_parent_path_id;	
	}
	else
		return 0;
}
#return result
function regionwise_merchant_deep($region_id)
{
	$region_path = $this->get_region_path($region_id);
	$sql = "SELECT region_id,merchant_id,merchant_name,merchant_logo FROM gl_merchant, gl_region
			WHERE 
            region_parent_path_id LIKE'%/" . $region_id . "/%'
            AND merchant_region_id =region_id
			AND merchant_status = 'Active' 
            GROUP by merchant_id ORDER BY merchant_rank, merchant_name ASC
			";
	$result = $this->db->query($sql);
	return $result;
}
function regionwise_merchant($region_id)
{
	$region_path = $this->get_region_path($region_id);
	$sql = "SELECT merchant_id,merchant_name,merchant_logo FROM gl_merchant, gl_region
			WHERE 
			region_id = merchant_region_id
			AND merchant_region_id ='$region_id'
			AND merchant_status = 'Active' ORDER BY merchant_rank, merchant_name ASC";

    $params = array(
        'cache_name' => 'regionwise_merchant_'.$region_id, #unique name
        'cache_table_name' => array('gl_merchant', 'gl_region'), #database table
        'result' => $sql, #the result
        'result_type' => 'result', #if from DB result object
    );
    
    $result= get_create_cache($params);
    
	//$result = $this->db->query($sql);
    
	return $result;
}
function regionwise_merchant_list($region_id)
{
	$merchant_id = "";
	 $sql = "SELECT merchant_id from gl_merchant,gl_region
			WHERE 
			region_id = merchant_region_id
			AND merchant_region_id ='$region_id'
			AND merchant_status = 'Active'
			";
	
    $params = array(
        'cache_name' => 'regionwise_merchant_list_'.$region_id, #unique name
        'cache_table_name' => array('gl_merchant', 'gl_region'), #database table
        'result' => $sql, #the result
        'result_type' => 'result', #if from DB result object
    );
    //$result = $this->db->query($sql);
    $result= get_create_cache($params);
	
	if($result->num_rows()>0)
	{
		foreach($result->result() as $row)
		{
			$merchant_id .= $row->merchant_id.",";
		
		}//foreach
	}//
	$list = rtrim($merchant_id,',');
	if($list =="") return 0 ;else return $list;
}//function end

function merchantName($merchant_id)
{
	$merchant_id = (int) $merchant_id;
    
	/*$where = array('merchant_id'=>$merchant_id);
	$res = $this->db->get_where($this->INSTACNE_NAME,$where);
    */
    $sql = "SELECT merchant_name FROM gl_merchant  WHERE merchant_id= '$merchant_id'";
    $params = array(
        'cache_name' => 'merchantName_'.$merchant_id, #unique name
        'cache_table_name' => array('gl_merchant'), #database table
        'result' => $sql, #the result
        'result_type' => 'result', #if from DB result object
    );
    //$res = $this->db->query($sql);
    $res= get_create_cache($params);
    
    
	if($res->num_rows()>0)
		{
			$rec = $res->row();
			return $rec->merchant_name;
		}
	else 
		{
			return "";
		}
	}
	function password_verify_update($password, $merchant_id,$account_id) {
		$sql = "SELECT merchant_user_salt,merchant_user_password FROM gl_merchant_users WHERE merchant_user_id= '$account_id'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            $row = $res->row();
            $db_pass = $row->merchant_user_password;
            $db_salt = $row->merchant_user_salt;

            $cur_pass = $this->hash_key_value($db_salt, $password);
            if ($cur_pass == $db_pass)
                return 'ok';
        }
        return 'bad';	
	}
	function password_verify($password, $merchant_id) {
		$sql = "SELECT merchant_salt,merchant_password FROM gl_merchant WHERE merchant_id= '$merchant_id'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            $row = $res->row();
            $db_pass = $row->merchant_password;
            $db_salt = $row->merchant_salt;

            $cur_pass = $this->hash_key_value($db_salt, $password);
            if ($cur_pass == $db_pass)
                return 'ok';
        }
        return 'bad';	
	}
	function check_username_if_exists($username,$merchant_id){
		$sql = "SELECT * FROM gl_merchant_users WHERE merchant_username='".$username."' LIMIT 1";
        $res = $this->db->query($sql);	
		if ($res->num_rows() > 0) {
        	return 'bad';
        }
        return 'ok';	
	
	}
	function check_username_if_exists_update($username,$merchant_id,$account_id){
		$sql = "SELECT * FROM gl_merchant_users WHERE merchant_id= '$merchant_id' AND merchant_username='".$username."' AND merchant_user_id<>'$account_id' LIMIT 1";
        $res = $this->db->query($sql);	
		if ($res->num_rows() > 0) {
        	return 'bad';
        }
        return 'ok';		
	}
	
	function merchantPortalUpdatePassword($merchantID){
		$merchant_salt = $this->generate_hash();
		$merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('npword'));
		$content=array(
			'merchant_password'=>$merchant_password,
			'merchant_salt'=>$merchant_salt
		);
		$where=array(
			'merchant_id'=>$merchantID
		);
		$this->db->update($this->INSTACNE_NAME,$content,$where);
	}
	function merchantPortalUpdatePasswordUSERS($accountID){
		$merchant_salt = $this->generate_hash();
		$merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('npword'));
		$content=array(
			'merchant_user_password'=>$merchant_password,
			'merchant_user_salt'=>$merchant_salt
		);
		$where=array(
			'merchant_user_id'=>$accountID
		);
		$this->db->update($this->INSTACNE_MERCHANT_USERS,$content,$where);
	}
	function merchantPortalAddNewUser($merchantID)
	{
		$merchant_salt = $this->generate_hash();
		$merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('pword'));
		$contentTemp=array(
			'merchant_id'=>$merchantID,
			'merchant_username'=>$this->input->post('username'),
			'merchant_user_fname'=>$this->input->post('fname'),
			'merchant_user_lname'=>$this->input->post('lname'),
			'merchant_user_password'=>$merchant_password,
			'merchant_user_salt'=>$merchant_salt
			);
		$this->db->insert($this->INSTACNE_MERCHANT_USERS,$contentTemp);
		$merchant_new_user_id=$this->db->insert_id();
		return $merchant_new_user_id;
	}
	function merchantPortalUpdateUser($accountID)
	{
		$data = array(
			'merchant_user_fname'=>$this->input->post('fname'),
			'merchant_user_lname'=>$this->input->post('lname')
		);
		$where=array(
		'merchant_user_id'=>$accountID
		);
		$this->db->update($this->INSTACNE_MERCHANT_USERS,$data,$where);	
	}
	function merchantPortalUpdateUserPassword($accountID)
	{
		$merchant_salt = $this->generate_hash();
		$merchant_password = $this->hash_key_value($merchant_salt, $this->input->post('npword'));
		$data = array(
			'merchant_user_password'=>$merchant_password,
			'merchant_user_salt'=>$merchant_salt
		);
		$where=array(
		'merchant_user_id'=>$accountID
		);
		$this->db->update($this->INSTACNE_MERCHANT_USERS,$data,$where);	
	}
	function merchantPortalGetUserList($merchantID)
	{
		$sql="select * from $this->INSTACNE_MERCHANT_USERS  WHERE merchant_id= '$merchantID'";
		$q=$this->db->query($sql);
		return $q;
	}
	function merchantPortalCheckThisUserForThisMerchant($userID, $merchantID){
		$sql="select * from $this->INSTACNE_MERCHANT_USERS  WHERE merchant_user_id='$userID' AND merchant_id= '$merchantID'";
		$q=$this->db->query($sql);
		return $q;
	}
        
        
        function get_weekly_report_list($from_date, $to_date, $args, $paginate = FALSE) {
            if (count($args) > 0) {
                list($sort, $order, $rows, $page) = $args;
            }
            
            $join_voucher = array(
                'table' => 'gl_voucher voucher',
                'condition' => "merchant.merchant_id = voucher.voucher_merchant_id AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_redemption_date BETWEEN  '$from_date' AND '$to_date'",
                'join' => 'LEFT',
            );
            
            $join_region = array(
                'table' => 'gl_region gr',
                'condition' => 'merchant.merchant_region_id = gr.region_id',
            );

           
            $params = array(
               'select' => '
                   voucher.voucher_status,
                   merchant.merchant_id, 
                   merchant.merchant_name, 
                   merchant.merchant_contact_name, 
                   merchant.accounts_contact_name,  
                   SUM(voucher.voucher_prod_cost) AS amount,
                   gr.region_name AS location,
                   merchant.notify_top_limit',
                'join' => array(
                    $join_voucher,
                    $join_region
                ),
                #'where' => $where,
                'group_by' => 'merchant.merchant_id, merchant.merchant_region_id',
                
                'resource_id' => FALSE
            );
            
            if ($paginate) {
                $params['order_by'] = $sort.' '.$order;
                $params['limit'] = $rows;
                $params['offset'] = $page - 1;
            }
            
            $rec = $this->get_list($params);
            
            
//            echo $this->db->last_query();
//            exit;
            $rec = $this->db->query('
                SELECT * FROM ('. $this->db->last_query() .') summary 
                WHERE amount >= notify_top_limit AND notify_top_limit IS NOT NULL AND notify_top_limit <> 0
            ');
            
//            echo $this->db->last_query();
//            exit;
            
            return $rec;
        }
        
        function get_monthly_report_list($from_date, $to_date, $args, $paginate = FALSE) {
            if (count($args) > 0) {
                list($sort, $order, $rows, $page) = $args;
            }
            
            $join_voucher = array(
                'table' => 'gl_voucher voucher',
                'condition' => "merchant.merchant_id = voucher.voucher_merchant_id AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_redemption_date BETWEEN  '$from_date' AND '$to_date'",
                'join' => 'LEFT',
            );
            
            $join_region = array(
                'table' => 'gl_region gr',
                'condition' => 'merchant.merchant_region_id = gr.region_id',
            );
            
            $where = "voucher.voucher_orginated_by = 0 AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_purchased_date BETWEEN  '$from_date' AND '$to_date'";
            
            $params = array(
               'select' => '
                   voucher.voucher_status,
                   merchant.merchant_id, 
                   merchant.merchant_name, 
                   merchant.merchant_contact_name, 
                   merchant.accounts_contact_name,  
                   IFNULL(SUM(voucher.voucher_prod_cost), 0.00) AS amount,
                   gr.region_name AS location,
                   merchant.notify_top_limit,
                   merchant.notify_excempt_amount',
                'join' => array(
                    $join_voucher,
                    $join_region
                ),
                #'where' => $where,
                'order_by' => 'amount DESC',
                'group_by' => 'merchant.merchant_id, merchant.merchant_region_id',
                'resource_id' => TRUE
            );
            
            if ($paginate) {
                $params['order_by'] = $sort.' '.$order;
                $params['limit'] = $rows;
                $params['offset'] = $page - 1;
            }
            
            $rec = $this->get_list($params);
            
            return $rec;
            
            //echo $this->db->last_query();
            //exit;
//            $rec = $this->db->query('
//                SELECT * FROM ('. $this->db->last_query() .') summary 
//                WHERE amount >= notify_top_limit AND notify_top_limit IS NOT NULL
//            ');
        }
    /**
     * Returns a template
     * creates if founf nothing
     *
     * @param $id Int Brand ID
     */
    public function get_or_create_template( $merchant_id ) {

        $template = $this->get_template( $merchant_id );

        if( !$template ) {
            $template_id = $this->create_template( $merchant_id );
        }

        return $this->get_template( $merchant_id );
    }
    public function get_template( $merchant_id ) {

        $q = $this->db->get_where( 'gl_template', array('type'=>'merchant','referer_id'=>(int) $merchant_id) );

        if( $q->num_rows() ) {
            return $q->row();
        }

        return FALSE;
    }
    public function create_template( $merchant_id ) {
        $now = gmdate('Y-m-d H:i:s');

        $this->db->insert( 'gl_template', array(
            'created' => $now,
            'modified' => $now,
            'created_by' => $this->session->userdata('user_id'),
            'modified_by' => $this->session->userdata('user_id'),
            'status' => 'Active',
            'referer_id' => $merchant_id,
            'sort_order' => 0,
            'type' => 'merchant'
        ) );

        return $this->db->insert_id();
    }

}//eoc
?>
