<?php
class Underage_m extends CI_Model {
    
    public static $default = 18;

    public $fields = array(
        'age_limit' => array('Int','int(2)')
    );
    
    public $has_one = array(
        'region_id' => array('Int','int(11)')
    );
    
    function __construct() {
        parent::__construct();
    }
    
    public function get_limit_by_region_id( $region_id ) {
    
        $q = $this->db->get_where('gl_underage','region_id = ' . $region_id);
        if( $q->num_rows() > 0 ) {
            return (int) $q->row()->age_limit;
        }
        
        return self::$default;
    }
    
    public function is_underage( $dob, $region_id ) {
        
        $user_age = date_diff(date_create( $dob ), date_create('now'))->y;
        
        if( $user_age < $this->get_limit_by_region_id( $region_id ) ) {
            return true;
        }

        return false;
    }
    
    public function save( $data, $id = 0 ) {
        $now = gmdate('Y-m-d H:i:s');

        if( $id ) {
            // update

            if( $this->db->update( 'gl_underage', $data, 'id = ' . $id ) ) {

                return TRUE;
            } else {

                log_message( 'error', $now . ' An error occured updating gl_underage('.$id.').' );
                exit($now . ' An error occured updating gl_underage('.$id.').');
            }
        } else {
            // insert
            $data['created'] = $now;
            if( $this->db->insert( 'gl_underage', $data ) ) {

                return $this->db->insert_id();
            } else {

                log_message( 'error', $now . ' An error occured saving the new gl_underage.' );
                exit($now . ' An error occured saving the new gl_underage.');
            }
        }

        return FALSE;
    }

    public function delete( $id ) {

        return $this->db->delete('gl_underage', array('id'=>$id));
    }
}