<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-28-2013
*/
require_once('super_model.php');
class Product_m extends Super_model
{
	protected $INSTACNE_NAME="gl_product";

    function __construct() {
        parent::__construct('gl_product product');
    }
	
	function selectProducts($start=0,$per_page=0)
	{
		$limit_query="";
		
	
		if($per_page>0)
		$limit_query=" limit $start, $per_page";
		
		$sql="select gl_product.*,cat_id,cat_name,merchant_id,merchant_name from $this->INSTACNE_NAME,gl_categories ,gl_merchant 
				WHERE 
				prod_cat_id = cat_id
				AND prod_merchant_id = merchant_id
				AND prod_status != 'Deleted'  
				order by prod_id desc $limit_query";
		$q=$this->db->query($sql);
		return $q;
	}
	
function selectProductsForAdmin($subQuery="")
		{
			/// Author : Prakash on 8/6/2013  for admin purpose
			$sql = "select gl_product.*,cat_id,cat_name,merchant_id,merchant_name from $this->INSTACNE_NAME,gl_categories ,gl_merchant JOIN gl_region ON gl_merchant.merchant_region_id=gl_region.region_id
				WHERE                 
				prod_cat_id = cat_id
				AND prod_merchant_id = merchant_id
				
				 $subQuery";
			$result = $this->db->query($sql);
			return $result;
		}
        
        function checkIfThereIsExistingVoucherForThisProduct($prod_id) {
        $prod_id = (int) $prod_id;
        $where = array();
        $sql = "SELECT * FROM gl_voucher WHERE 
    voucher_product_id=$prod_id AND (voucher_status='Consumable' OR voucher_status='Consumable')
	";
        $result = $this->db->query($sql);
        return $result;
    }
    function checkIfThereIsExistingCampaignByRuleForThisProduct($prod_id) {
        $prod_id = (int) $prod_id;
        $sql = "SELECT * FROM gl_campaign_voucher WHERE 
    camp_prod_id=$prod_id AND (camp_voucher_assign_status='UnAssigned') AND camp_voucher_by_type='by_rule'
	";
        $result = $this->db->query($sql);
        return $result;
    }

    function checkIfThereIsExistingCampaignByPromoCodeForThisProduct($prod_id) {
        $prod_id = (int) $prod_id;
        $where = array();
        $sql = "SELECT * FROM gl_campaign_details JOIN gl_campaign_details_by_promo ON gl_campaign_details.camp_campaign_id=gl_campaign_details_by_promo.campaign_id
    WHERE 
    campaign_by_promo_status<>'Done' AND camp_prod_id=$prod_id AND camp_status='Active' AND  (campaign_by_type='by_promo_primary' OR campaign_by_type='by_promo_follow_up')
    
	";
        $result = $this->db->query($sql);
        return $result;
    }

    function setThisProductToPendingForDeletion($contentID, $delete_type, $num_days) {
        $num_days = (int) $num_days;

        $date_to_be_deleted = date('Y-m-d', (strtotime((date('Y-m-d') . ' 00:00:00')) + ($num_days * 86400)));
        $data = array('prod_status' => 'Pending for Deletion', 'prod_delete_type' => $delete_type, 'prod_delete_num_days' => $date_to_be_deleted);
        $where = array(
            'prod_id' => $contentID
        );
        $this->db->update($this->INSTACNE_NAME, $data, $where);
    }
function selectProductByID($prod_id)
{
	$prod_id = (int) $prod_id;
	$where = array('prod_id'=>$prod_id);
	return $this->db->get_where($this->INSTACNE_NAME,$where);
}
function product_desc($prod_id)
{
	$prod_id = (int) $prod_id;
	$where = array('prod_id'=>$prod_id);	
	$result =$this->db->get_where($this->INSTACNE_NAME,$where);
	$rec 	=$result->row();
	return $rec->prod_details ;
}
function delete($contentID)
{
		
		$data = array('prod_status'=>'Deleted');
		$where=array(
		'prod_id'=>$contentID
		);
		$this->db->update($this->INSTACNE_NAME,$data,$where);
}
	
	function addRecord()
	{
			
			$content=array(
			'prod_name'=>$this->input->post('prod_name'),
			'prod_point'=>$this->input->post('prod_point'),
			'prod_reverse_point'=>$this->input->post('prod_reverse_point'),
			'prod_merchant_cost'=>$this->input->post('prod_merchant_cost'),
			'prod_cat_id'=>$this->input->post('prod_cat_id'),
			'prod_merchant_id'=>$this->input->post('prod_merchant_id'),
			'prod_voucher_expiry_day'=>$this->input->post('prod_voucher_expiry_day'),
			'prod_terms_condition'=>$this->input->post('prod_terms_condition'),
			'prod_details'=>$_POST['prod_details'],
			'prod_warning'=>$this->input->post('prod_warning'),
			'prod_added'=>date('Y-m-d'),
			'prod_status'=>$this->input->post('prod_status'),
			'updated_by_id' => $this->session->userdata('admin_id')
			);
			
			$this->db->insert($this->INSTACNE_NAME,$content);
            $inserted_id = $this->db->insert_id();
        $rank= $this->input->post('prod_rank');
            if($rank<$inserted_id)
               $rank=$inserted_id;
       $rank_cat= $this->input->post('prod_rank_cat');
            if($rank_cat<$inserted_id)
               $rank_cat=$inserted_id;
       
            $this->updateProductRanking($inserted_id, $rank);
            $this->updateProductRanking($inserted_id, $rank_cat,'prod_rank_cat');
			
        //memcached clear
        $params = array(
            'affected_tables' 
                => array(                    
                    'gl_product'
                ) #cache name                
        );
        delete_cache($params);                
        //memcached clear    
            
        
			return $inserted_id;
		
	}
	function updateLogo($prod_image,$prod_id,$old_image='')
	{
		$data = array('prod_image'=>$prod_image);
		$where = array('prod_id'=>$prod_id);
		$this->db->update($this->INSTACNE_NAME,$data,$where);
		if($old_image!="")
			@unlink('custom/uploads/products/'.$old_image);
				if($old_image!="")
			@unlink('custom/uploads/products/thumb/'.$old_image);

		
	}
	
	function editRecord($contentID=0)
	{
	//	print_r($_POST);
		$prod_status = !isset($_POST['prod_status'])?'InActive':$_POST['prod_status'];
		  
		  $content=array(
			'prod_name'=>$this->input->post('prod_name'),
			'prod_point'=>$this->input->post('prod_point'),
			'prod_reverse_point'=>$this->input->post('prod_reverse_point'),
			'prod_merchant_cost'=>$this->input->post('prod_merchant_cost'),
			'prod_cat_id'=>$this->input->post('prod_cat_id'),
			'prod_merchant_id'=>$this->input->post('prod_merchant_id'),
			'prod_voucher_expiry_day'=>$this->input->post('prod_voucher_expiry_day'),
			'prod_terms_condition'=>$this->input->post('prod_terms_condition'),
			'prod_details'=>$_POST['prod_details'],
			'prod_warning'=>$this->input->post('prod_warning'),
			'prod_status'=>$prod_status,
			'updated_by_id' => $this->session->userdata('admin_id')
			);
	//print_r($content);	
		$where=array(
			'prod_id'=>$contentID
		);
			$this->db->update($this->INSTACNE_NAME,$content,$where);
            
        //memcached clear
        $params = array(
            'affected_tables' 
                => array(
                    'gl_favourite',
                    'gl_product'
                ), #cache name  
            'specific_names' 
                => array(
                    'productInfo_'.$contentID,
                    'merchantName_'.$contentID
                )    
        );
        delete_cache($params);                
        //memcached clear
            
            
            $this->updateProductRanking($contentID, $this->input->post('prod_rank'));
        $this->updateProductRanking($contentID, $this->input->post('prod_rank_cat'),'prod_rank_cat');

    }
    function updateRanking($sortedIDs,$location_id,$merchant_id,$category_id)
    {
        //first get all the old ranking list                    
        if($category_id>0)
        {
            $sql="select prod_id, prod_rank_cat from $this->INSTACNE_NAME             
            WHERE prod_id IN (".$sortedIDs.") ORDER BY prod_rank_cat ASC";   
            
            
        }
        else
        {
            $sql="select prod_id, prod_rank from $this->INSTACNE_NAME             
            WHERE prod_id IN (".$sortedIDs.") ORDER BY prod_rank ASC";            
        }
        
		$result=$this->db->query($sql);        
        $ctr=0;
        $sortedIDs= explode(",",$sortedIDs);                        
        foreach($result->result() as $row)
        {
            
            if($row->prod_id != $sortedIDs[$ctr])
            {
                
                if($category_id>0)
                {
                    $content=array(
                        'prod_rank_cat'=>$row->prod_rank_cat
                    );	
               
                }
                else
                {
                    $content=array(
                        'prod_rank'=>$row->prod_rank
                    );
                }
                $where=array(
                    'prod_id'=>$sortedIDs[$ctr]
                );
                $this->db->update($this->INSTACNE_NAME,$content,$where);
            }
            $ctr++;        
        }        
        //memcached clear 
        $params = array(
            'affected_tables' 
                => array(                
                    'gl_product'
                ) #cache name                   
        );       
        delete_cache($params);                        
        //memcached clear
    }
    function updateProductRanking($prod_id, $rank, $update="prod_rank")
    {
        $rank=(int) $rank;        
        $this->doRecursiveProcutRankUpdate($prod_id, $rank, 1,$update);        
        
    }
    function doRecursiveProcutRankUpdate($prod_id=0, $rank, $way,$update="prod_rank")
    {
        if($prod_id>0)
        {
            if($update=="prod_rank")
            {
                $content=array(
                    'prod_rank'=>$rank
                );			
            }
            else
            {
                $content=array(
                    'prod_rank_cat'=>$rank
                );
            }
			$where=array(
                'prod_id'=>$prod_id
			);
			$this->db->update($this->INSTACNE_NAME,$content,$where);
                        
            //check if there is an existings merhcant with that rank
            if($update=="prod_rank")
            {
                $sql="SELECT prod_id from gl_product WHERE prod_id<>'".$prod_id."' AND prod_rank='".$rank."'";                        
            }
            else
            {
                $sql="SELECT prod_id from gl_product WHERE prod_id<>'".$prod_id."' AND prod_rank_cat='".$rank."'";                        
            }
            
            $result = $this->db->query($sql);
            //echo $result->num_rows()."asdasd<br>";
            $first="";
            foreach($result->result() as $row){
                if($first=="")
                {
                    $first=$row;
                }
                if($update=="prod_rank")
                {
                    $content=array(
                        'prod_rank'=>$rank
                    );			
                }
                else
                {
                    $content=array(
                        'prod_rank_cat'=>$rank
                    );
                }
                $where=array(
                    'prod_id'=>$row->prod_id
                );
                $this->db->update($this->INSTACNE_NAME,$content,$where);                
            }            
            if($first!="")
            {
                 $this->doRecursiveProcutRankUpdate($first->prod_id, ($rank+$way), $way, $update);            
            }
            
        }
        
        
    }
#input product id (it can be list)
#return product_id,product_name,merchant_name,merchat_id,product_pic,merchant_pic
function productInfo($product_list_id)
{
	if ($product_list_id == "")
        $product_list_id = 0;
    $sql = "SELECT prod_id,prod_name,prod_image,merchant_id,merchant_name,merchant_logo, prod_cat_id
        FROM gl_product,gl_merchant
        WHERE prod_merchant_id = merchant_id
        AND prod_id IN($product_list_id)
        ";
    $product_list = array();
    
    //$result = $this->db->query($sql);        
    $params = array(
                'cache_name' => 'productInfo_'.$product_list_id, #unique name
                'cache_table_name' => array('gl_product','gl_merchant'), #database table
                'result' => $sql, #the result 
                'result_type' => 'result', #if from DB result object
            );
            
    $result= get_create_cache($params);
    
    
    
    
    if ($result->num_rows() > 0) {
        foreach ($result->result() as $row) {
            $product_list['product'][$row->prod_id] = array('prod_name' => $row->prod_name,
                'prod_image' => $row->prod_image,
                'prod_cat_id' => $row->prod_cat_id,
                'merchant_name' => $row->merchant_name,
                'merchant_id' => $row->merchant_id,
                'merchant_logo' => $row->merchant_logo
            );
        }
    }
    return json_encode($product_list);
	
}
function productInfo_2($product_list_id)
{
	if($product_list_id=="")
		$product_list_id = 0;
	$sql = "SELECT prod_id,prod_name,prod_image,merchant_id,merchant_name,merchant_logo
			FROM gl_product,gl_merchant
			WHERE prod_merchant_id = merchant_id
			AND prod_id IN($product_list_id)
			";
		$product_list = array();
		$result = $this->db->query($sql);
		if($result->num_rows()>0)
		{
			foreach($result->result() as $row){
				$product_list['product']= array('prod_name'=>$row->prod_name,
																'prod_image'=>$row->prod_image,
																'merchant_name'=>$row->merchant_name,
																'merchant_id'=>$row->merchant_id,
																'merchant_logo'=>$row->merchant_logo
																);
				
				}
			
		}
		return json_encode($product_list);
	
}
function product_by_merchant_id($merchant_id,$status='Active')
{
    if ($status == '') {
            $where = 'prod_merchant_id ="'. $merchant_id .'" AND prod_cat_id != 9';
        } else {
            $where = 'prod_merchant_id ="'. $merchant_id .'" AND prod_cat_id != 9 AND prod_status ="'.$status.'"';
        }
        $sql = "SELECT * FROM gl_product  WHERE ".$where." ORDER BY prod_rank ASC";
        $params = array(
            'cache_name' => 'product_by_merchant_id_'.$merchant_id, #unique name
            'cache_table_name' => array('gl_product'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$res = $this->db->get_where($this->INSTACNE_NAME, $where);
        $res = get_create_cache($params);        
        return $res;
}

function product_by_merchant_id_campaign($merchant_id,$status='Active')
{
    if ($status == '') {
            $where = 'prod_merchant_id ="'. $merchant_id .'"';
        } else {
            $where = 'prod_merchant_id ="'. $merchant_id .'"AND prod_status ="'.$status.'"';
        }
        $sql = "SELECT * FROM gl_product  WHERE ".$where." ORDER BY prod_rank ASC";
        $params = array(
            'cache_name' => 'product_by_merchant_id_'.$merchant_id, #unique name
            'cache_table_name' => array('gl_product'), #database table
            'result' => $sql, #the result
            'result_type' => 'result', #if from DB result object
        );
        //$res = $this->db->get_where($this->INSTACNE_NAME, $where);
        $res = get_create_cache($params);
        return $res;
}
function merchant_id_by_product($prod_id)
{
	$where = array('prod_id'=>$prod_id);
	$res = $this->db->get_where($this->INSTACNE_NAME,$where);	
	$rec = $res->row();
	return isset($rec->prod_merchant_id)?$rec->prod_merchant_id:0;

}
#this function take care for user specific project with merchant and region and if it is available
function users_cat_product($mechant_id,$cat_id)
{
	 if ($cat_id == "other")
            $cat_query = "  prod_cat_id > '4'";
        else
            $cat_query = "  prod_cat_id = '$cat_id'";

        $sql = "SELECT prod_id,prod_name,prod_image,prod_point,prod_details,prod_merchant_id FROM
			gl_product
			WHERE prod_merchant_id IN($mechant_id)
			AND prod_cat_id = '$cat_id'
			AND prod_status = 'Active'
            ORDER BY prod_rank_cat ASC
			";
            
        $params = array(
            'cache_name' => 'users_cat_product_'.$mechant_id.'_'.$cat_id, #unique name
            'cache_table_name' => array('gl_product'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$prod_result = $this->db->query($sql);
        $prod_result = get_create_cache($params);		
        return $prod_result;
	
}
function is_user_valid_prodcut($user_id,$prod_id)
{
	#not neccessary for now to verify prodcut by merchant which avaliable  or not into user's region.
	#this function just verify product is valid or not
	
	$sql = "SELECT prod_id,prod_name,prod_merchant_id,prod_image,prod_point,prod_merchant_cost,prod_voucher_expiry_day,
				prod_reverse_point,
				merchant_name
				FROM ".$this->INSTACNE_NAME .",gl_merchant
			WHERE merchant_id = prod_merchant_id
			AND prod_id = '$prod_id'			
	";
	$result = $this->db->query($sql);
	return $result;
}

function userFevProduct($userID)
		{
			$sql = "SELECT prod_id,prod_name,prod_image,prod_point,prod_details,merchant_id,prod_merchant_id,prod_cat_id FROM
			gl_favourite,gl_product,gl_merchant
			 WHERE favourite_product_id = prod_id 
			 AND merchant_id = prod_merchant_id 
			 AND favourite_user_id='$userID' 
			 AND prod_status = 'Active'
			 AND favourite_status = 'Active' 
			 AND merchant_status = 'Active'
			 ORDER BY  favourite_id DESC";
        $params = array(
            'cache_name' => 'userFevProduct_'.$userID, #unique name
            'cache_table_name' => array('gl_favourite','gl_product','gl_merchant'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        );
        //$prod_result = $this->db->query($sql);
        $prod_result = get_create_cache($params);
        
        return $prod_result;
		}
		
function removeuserFevItem($itemID)
	{
		$sql="UPDATE gl_favourite SET
			favourite_status = 'Removed'
		 WHERE favourite_product_id='$itemID'
		 AND favourite_user_id='".$this->session->userdata('user_id')."'";
         
     $params = array(                                        
            'specific_names' 
                => array(
                    'userFevProduct_'.$this->session->userdata('user_id') 
                )
        );
        delete_cache($params);
		return  $this->db->query($sql);
	}
function removeuserFevItem_api($itemID, $user_id)
	{
		$sql="UPDATE gl_favourite SET
			favourite_status = 'Removed'
		 WHERE favourite_product_id='$itemID'
		 AND favourite_user_id='".$user_id."'";
         
     $params = array(                                        
            'specific_names' 
                => array(
                    'userFevProduct_'.$user_id
                )
        );
        delete_cache($params);
		return  $this->db->query($sql);
	}
	
function addFevitemToFevList($itemID)
	{
		$sql="SELECT 1 FROM gl_favourite WHERE  favourite_product_id='$itemID' 
		AND favourite_status = 'Active'
		AND favourite_user_id='".$this->session->userdata('user_id')."'";
		$res =  $this->db->query($sql);
		if($res->num_rows()==0)
		{
			$sqlInsert = "INSERT INTO gl_favourite SET  favourite_product_id='$itemID', favourite_user_id='".$this->session->userdata('user_id')."',
			favourite_added_date='".$this->custom_func->datetime()."'
			";
			$this->db->query($sqlInsert);
            
            
            $params = array(                                        
                'specific_names' 
                    => array(                    
                        'userFevProduct_'.$this->session->userdata('user_id') 
                    )
            );
            delete_cache($params);
		}
	}
function addFevitemToFevList_api($itemID,$user_id)
	{
		$sql="SELECT 1 FROM gl_favourite WHERE  favourite_product_id='$itemID' 
		AND favourite_status = 'Active'
		AND favourite_user_id='".$user_id."'";
		$res =  $this->db->query($sql);
		if($res->num_rows()==0)
		{
			$sqlInsert = "INSERT INTO gl_favourite SET  favourite_product_id='$itemID', favourite_user_id='".$user_id."',
			favourite_added_date='".$this->custom_func->datetime()."'
			";
			$this->db->query($sqlInsert);
            
            
            $params = array(                                        
                'specific_names' 
                    => array(                    
                        'userFevProduct_'.$user_id
                    )
            );
            delete_cache($params);
		}
	}
	
function checkAddFevitemToFevList($itemID, $user_id = NULL)
	{
        //TODO: Remove this quick fix for API
        $session_id = $this->session->userdata('user_id');
        $user_id = (! is_null($user_id)) ? $user_id : $session_id; 

		$sql="SELECT 1 FROM gl_favourite WHERE  favourite_product_id='$itemID' 
				AND favourite_status = 'Active'
				AND favourite_user_id='".$user_id."'";
		$res =  $this->db->query($sql);
		return $res->num_rows();
		
	}
function productAndMerchant($product_id)
{
	$sql = "SELECT * FROM gl_product,gl_merchant WHERE
			merchant_id = prod_merchant_id
			AND prod_id = '$product_id'
			";
	$res =  $this->db->query($sql);
	return $res;

}
function verify_outlet_code($merchat_id,$outlet_code)
{
	$whr = array('outlet_merchant_id'=>$merchat_id,
				 'outlet_code'=>$outlet_code,
                 'outlet_status'=>'Active'
				 );
	$res = $this->db->get_where('gl_outlet',$whr);
	return $res;
}
function product_by_merchant_dropdown($selected_id)
{
	$sql = "SELECT merchant_id,merchant_name FROM gl_merchant WHERE merchant_status = 'Active'";
	$res = $this->db->query($sql);
	$list = "";
	foreach($res->result() as $rec){
	
		$list .= '<optgroup label="'.$rec->merchant_name.'">';
		$res_product = $this->product_by_merchant_id($rec->merchant_id,'');
        //$status='Active'
		foreach($res_product->result() as $row_prod){
			$selected = ($selected_id == $row_prod->prod_id)?'selected="selected"':'';
			$list .= '<option value="'.$row_prod->prod_id.'" '.$selected.'>'.$row_prod->prod_name.'  [Point:'.$row_prod->prod_point.']</option>';	
		}
		$list .= '</optgroup>';
	}
	return $list;
}
function product_by_merchant_dropdown_campaign($selected_id)
{
	$sql = "SELECT merchant_id,merchant_name FROM gl_merchant WHERE merchant_status = 'Active'";
	$res = $this->db->query($sql);
	$list = "";
	foreach($res->result() as $rec){

		$list .= '<optgroup label="'.$rec->merchant_name.'">';
		$res_product = $this->product_by_merchant_id_campaign($rec->merchant_id,'');
        //$status='Active'
		foreach($res_product->result() as $row_prod){
			$selected = ($selected_id == $row_prod->prod_id)?'selected="selected"':'';
			$list .= '<option value="'.$row_prod->prod_id.'" '.$selected.'>'.$row_prod->prod_name.'  [Point:'.$row_prod->prod_point.']</option>';
		}
		$list .= '</optgroup>';
	}
	return $list;
}
}//eoc
?>