<?php

/*
 * Country Model Class
 * Look up table for country code
 * 
 * @author Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Country_m extends Super_model {

    function __construct() {
        parent::__construct('gl_country country');
    }
    
    function get_json_list() {
        $data = array();
        $rec = $this->get_list();
        
        foreach ($rec as $r) {
            $data[$r['iso']] = array('printable_name' => $r['printable_name']);
        }
        
        return json_encode($data);
    }
}