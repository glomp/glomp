<?php

require_once('super_model.php');
class Admin_m extends Super_model {

    private $INSTANCE = "gl_admin";

    function __construct() {
        parent::__construct('gl_admin admin');
    }

    function generate_hash($length = 8) {
        $chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789$%^!@&*';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    function loginValidation() {
        $username = $this->input->post('username');
        $plain_password = $this->input->post('password');
        $ip = $_SERVER['REMOTE_ADDR'];
        $cur_date = date('Y-m-d');
        $currentTime = time();
        $date_time = date('Y-m-d H:i:s');
        $login_success = false;

        $result = $this->db->get_where($this->INSTANCE, array('admin_email' => $username, 'admin_status' => 'Active'));



        if ($result->num_rows() == 1) {

            $admin_rec = $result->row();
            if (strtolower($admin_rec->admin_email) == strtolower($username)) {

                $admin_salt = $admin_rec->admin_salt;
                $admin_id = $admin_rec->admin_id;

                $hash_password = $this->hash_key_value($admin_salt, $plain_password);

                //now verify password
                $result_rec = $this->db->get_where($this->INSTANCE, array('admin_id' => $admin_id, 'admin_password' => $hash_password));

                if ($result_rec->num_rows() == 1) {

                    $data = array('admin_id' => $admin_rec->admin_id,
                        'admin_name' => $admin_rec->admin_name,
                        'admin_email' => $admin_rec->admin_email,
                        'admin_last_login' => $admin_rec->admin_last_login,
                        'admin_last_ip' => $admin_rec->admin_last_ip,
                        'is_logged_in' => true
                    );
                    $this->session->set_userdata($data);

                    $admin_id = $this->session->userdata('admin_id');

                    $update_data = array(
                        'admin_last_login' => $date_time,
                        'admin_last_ip' => $ip
                    );

                    $this->db->where('admin_id', $admin_id);
                    $this->db->update($this->INSTANCE, $update_data);
                    //die("password matched");
                    return true;
                }//end of password match verification
                else {
                    return false;
                }
            }//email address string verificaiton
        }//number of rows 
        //it always return false	
        return false;
    }

//function end

    function resetPassword($loginAs) {
        $hash_key = md5(time());
    }

    function hash_key_value($salt, $password) {
        $hash_password = hash('SHA256', $salt . $password);
        return $hash_password;
    }

    function verifyResetPassword($key, $userType) {
        /*
          if($userType==='customer')
          {
          $sql = "SELECT * FROM tbl_customer WHERE customer_pword_reset_hash_key = ? AND customer_email_verify = ? AND customer_active = ?";
          $q = $this->db->query($sql, array($key, 'Y', 'Y'));
          if($q->num_rows()>0)
          {
          ///  message
          $rec = $q->row();
          $name = $rec->customer_fname." ".$rec->customer_lname;
          $email =  $rec->customer_email;
          $password=$this->generatePassword($length = 8);
          $message="<p> Hi <strong>".$name."</strong></p>
          Here is your login details:<br/>
          E-mail address: ".$email."<br/>
          Password:".$password."<br/><br/>";
          $this->custom_func->sendEmail($to_name=$name,$to_email=$email ,$from_name=_SITE_NAME,$from_email=_DEFAULT_FROM_EMAIL,$subject="Password[Reset]",$message=$message);
          $where=array(
          'customer_pword_reset_hash_key'=>$key
          );
          $data=array(
          'customer_pword'=>md5($password)
          );
          $this->db->update("tbl_customer",$data,$where);
          return true;
          }
         */
    }

    function selectAdmin($admin_id = 0) {

        if (is_array($admin_id))
            $result = $this->db->get_where($this->INSTANCE, $admin_id);
        else
            $result = $this->db->get($this->INSTANCE);

        return $result;
    }

#add new admin

    function addNewAdmin() {
        $admin_salt = $this->generate_hash();
        $admin_password = $this->hash_key_value($admin_salt, $this->input->post('password'));
        $content = array(
            'admin_name' => $this->input->post('admin_name'),
            'admin_email' => $this->input->post('email_address'),
            'admin_added' => date('Y-m-d H:i:s'),
            'admin_status' => 'Active',
            'admin_salt' => $admin_salt,
            'admin_password' => $admin_password
        );
        $this->db->insert($this->INSTANCE, $content);
        return $inserted_id = $this->db->insert_id();
    }

#update admin

    function editAdmin($admin_id) {
        $where = array(
            'admin_id' => $admin_id
        );

        if (isset($_POST['reset_password'])) {
            $admin_salt = $this->generate_hash();
            $admin_password = $this->hash_key_value($admin_salt, $this->input->post('password'));
            $data_update = array('admin_password' => $admin_password);
            $this->db->update($this->INSTANCE, $data_update, $where);
        }
        $content = array(
            'admin_name' => $this->input->post('admin_name'),
            'admin_email' => $this->input->post('email_address'),
            'admin_status' => $this->input->post('admin_status'),
        );

        $this->db->update($this->INSTANCE, $content, $where);
    }

    function adminEmailCheck($email, $admin_id) {

        $result = "SELECT * from gl_admin where admin_email = '$email' and  admin_id != '$admin_id'";
        $result = $this->db->query($result);
        return $result;
    }

    function deleteAdmin($admin_id) {
        $where = array(
            'admin_id' => $admin_id
        );
        $content = array(
            'admin_status' => 'Deleted',
        );

        $this->db->update($this->INSTANCE, $content, $where);
        return true;
    }

    function checkLogin() {

        $response = (object) array('status' => 'Error', 'message' => '');
        if ($this->session->userdata('admin_id') && $this->session->userdata('is_logged_in') == true) {
            $data = array(
                'admin_id' => $this->session->userdata('admin_id'),
                'admin_status' => 'Active'
            );

            $result = $this->db->get_where('gl_admin', $data);
            if ($result->num_rows() == 1) {


                $inactive = 4800; //1200
                $session_life = time() - $this->session->userdata('since_logged_in');
                if ($session_life > $inactive) {
                    $response->status = 'Error';
                    $response->message = 'Session has been expired';
                    //direct(base_url(ADMIN_FOLDER.'/login/logout/'.$msg));
                    //exit();
                } else {
                    $this->session->set_userdata('since_logged_in', time());
                    $response->status = 'Ok';
                    $response->message = 'Ok';
                }
            } else {
                $this->session->set_flashdata("err", "Session has been expired.");
                $response->status = 'Error';
                $response->message = 'Session has been expired';
                //redirect(base_url(ADMIN_FOLDER.'/login/logout/'.$msg));
                //exit();
            }
        }//if close of checking session set
        else {
            $response->status = 'Error';
            $response->message = 'You are not logged in.';
        }
        return $response;
    }

//function close
}

//eoc
?>