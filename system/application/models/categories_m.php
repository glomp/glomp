<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-25-2013
*/

class Categories_m extends CI_Model
{
	protected $CATEGORY_INSTANCE="gl_categories";
	
	function selectCategories($start=0,$per_page=0)
	{
		$limit_query="";
		if($per_page>0)
		$limit_query=" limit $start, $per_page";
		
		$sql="SELECT * from ".$this->CATEGORY_INSTANCE." ORDER BY cat_display_order asc, cat_name $limit_query";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function UpdateDisplayOrder($order,$id)
	{
		$data=array(
			'cat_display_order'=>$order
		);
		
		$where=array(
			'cat_id'=>$id
		);
		
		$this->db->update($this->CATEGORY_INSTANCE,$data,$where);
	}
	
	function selectCategoriesByID($contentID=0)
	{
		$sql="select * from $this->CATEGORY_INSTANCE WHERE  cat_id='$contentID'";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function publish($catID,$catStatus)
	{
		$data=array(
			'cat_status'=>$catStatus
		);
		
		$where=array(
			'cat_id'=>$catID
		);
		
		$this->db->update($this->CATEGORY_INSTANCE,$data,$where);
	}
	
	function deleteCategories($catID=0)
	{
		#delete category if its not permanent category
		# delete if it is not used anywehre like into product table
		$sql = "DELETE FROM ".$this->CATEGORY_INSTANCE. " WHERE cat_id = '$catID' AND cat_permanent = 'N'";
		$result = $this->db->query_delete($sql);
		return $result['status'];	
	}
	
	function addCategories()
	{
		//echo "<pre>"; print_r($_POST['cat_name'][1]); die;
		$data=array('cat_name' => $this->input->post('cat_name'),
			'cat_display_order' => $this->input->post('display_order'),
			'cat_added' => date("Y-m-d")
		);
		$this->db->insert($this->CATEGORY_INSTANCE,$data);
		$inserted_id=$this->db->insert_id();
		
	}
	
	function editCategories($catID='')
	{
		$where = array(
			'cat_id' => $catID
		);
		$main_contain=array(
			'cat_display_order' => $this->input->post('display_order'),
			'cat_name' => $this->input->post('cat_name')
		);
		$this->db->update($this->CATEGORY_INSTANCE,$main_contain,$where);
        
        //memcached clear
        $params = array(                                                       
           'affected_tables' 
            => array(
                'gl_product'
            ) #cache name  
        );
        delete_cache($params);                
        //memcached clear
		
	}
	
	
	function catOption($selected_id=0)
	{
		$list ="";
		$sql="select * from $this->CATEGORY_INSTANCE order by cat_display_order, cat_name";
		$q=$this->db->query($sql);
		
		
		if($q->num_rows > 0)
		{
			foreach($q->result() as $r)
			{
				$selected = ($r->cat_id == $selected_id)?"selected='selected'":"";	
				$list .="<option value='".$r->cat_id."' $selected>".$r->cat_name."</option>";
			}
		}
		return $list;
	}
}
?>