<?php

/*
 * Campaign Newsletter Model Class
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Campaign_newsletter_m extends Super_model {

    function __construct() {
        parent::__construct('gl_campaign_newsletter cn');
    }
    function get_ischecked($name = '', $camp_newsletter_id) {
        
        $rec = $this->get_record(array(
            'select' => $name,
            'where' => array(
                $name => 'yes',
                'camp_newsletter_id' => $camp_newsletter_id
            )
        ));
        
        if ($rec == FALSE) {
            return FALSE;
        }
        
        return TRUE;
    }
}