<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: ryan
Date: September-03-2013
*/
class Merchant_portal_m extends CI_Model{
	private $INSTANCE="gl_merchant";
	private $INSTANCE_USERS="gl_merchant_users";
	
	function loginValidation_USERS(){
		$username = $this->input->post('username');
		$plain_password = $this->input->post('password');
		$ip = $_SERVER['REMOTE_ADDR'];
		$cur_date=date('Y-m-d');
		$currentTime = time();
		$date_time = date('Y-m-d H:i:s');
		$login_success = false;
		
		
		
		$result=$this->db->get_where($this->INSTANCE_USERS,array('merchant_username'=>$username));
		if($result->num_rows()==1)
		{		
								
			$merchant_rec = $result->row();
			if(strtolower($merchant_rec->merchant_username) == strtolower($username))
			{
				
				$merchant_salt = $merchant_rec->merchant_user_salt;
				$merchant_id = $merchant_rec->merchant_id;
				$merchant_user_id = $merchant_rec->merchant_user_id;
				
				$hash_password = $this->hash_key_value($merchant_salt,$plain_password);
				$merchant_name = $this->getMerchantName($merchant_id);
				//now verify password
				$result_rec = $this->db->get_where($this->INSTANCE_USERS,array('merchant_user_id'=>$merchant_user_id,'merchant_user_password'=>$hash_password));				
				if($result_rec->num_rows() == 1)
				{				
					$data = array('merchant_id'=>$merchant_rec->merchant_id,
								'merchant_name'=>$merchant_name,
								'merchant_user_id'=>$merchant_rec->merchant_user_id,
								'merchant_username'=>$merchant_rec->merchant_username,
								'merchant_last_login'=>$merchant_rec->merchant_user_login_date,
								'merchant_last_ip'=>$merchant_rec->merchant_user_last_login_ip,
								'is_merchant_users'=>true,
								'is_logged_in'=>true								
								);
								
					$this->session->set_userdata($data);					
					$merchant_id=$this->session->userdata('merchant_id');
					
					$update_data=array(
					'merchant_user_login_date'=>$date_time,
					'merchant_user_last_login_ip'=>$ip
					);
					
					$this->db->where('merchant_user_id',$merchant_user_id);
					$this->db->update($this->INSTANCE_USERS, $update_data); 
					//die("password matched");
					return true;
				}//end of password match verification
				else
				{
					return false;
				}
			
			}//email address string verificaiton
		}//number of rows 
		//it always return false	
		return false;
	}
	function loginValidation(){
		$username = $this->input->post('username');
		$plain_password = $this->input->post('password');
		$ip = $_SERVER['REMOTE_ADDR'];
		$cur_date=date('Y-m-d');
		$currentTime = time();
		$date_time = date('Y-m-d H:i:s');
		$login_success = false;
		
        
        $result=$this->db->get_where($this->INSTANCE,array('merchant_username'=>$username));
        if($result->num_rows()==0)
            $result=$this->db->get_where($this->INSTANCE,array('merchant_email'=>$username));
		
		
		if($result->num_rows()==1)
		{		
			$merchant_rec = $result->row();
			if( strtolower($merchant_rec->merchant_username) == strtolower($username)
                    ||
                strtolower($merchant_rec->merchant_email) == strtolower($username)
            )
			{
				
				$merchant_salt = $merchant_rec->merchant_salt;
				$merchant_id = $merchant_rec->merchant_id;
				
				$hash_password = $this->hash_key_value($merchant_salt,$plain_password);
				
				//now verify password
				$result_rec = $this->db->get_where($this->INSTANCE,array('merchant_id'=>$merchant_id,'merchant_password'=>$hash_password));
				
				if($result_rec->num_rows() == 1)
				{
				
					$data = array('merchant_id'=>$merchant_rec->merchant_id,
								'merchant_name'=>$merchant_rec->merchant_name,
								'merchant_username'=>$merchant_rec->merchant_email,
								'merchant_email'=>$merchant_rec->merchant_email,
								'merchant_last_login'=>$merchant_rec->merchant_last_login,
								'merchant_last_ip'=>$merchant_rec->merchant_last_ip,
								'is_merchant_users'=>false,
								'is_logged_in'=>true
								);
								
					$this->session->set_userdata($data);					
					$merchant_id=$this->session->userdata('merchant_id');
					
					$update_data=array(
					'merchant_last_login'=>$date_time,
					'merchant_last_ip'=>$ip
					);
					
					$this->db->where('merchant_id',$merchant_id);
					$this->db->update($this->INSTANCE, $update_data); 
					//die("password matched");
					return true;
				}//end of password match verification
				else
				{
					return false;
				}
			
			}//email address string verificaiton
		}//number of rows 
		//it always return false	
		return false;
	}//function end
	function hash_key_value($salt,$password)
	{
		$hash_password = hash('SHA256',$salt.$password);		
		return $hash_password;
	}
	function getMerchantName($merchantID){
		$result=$this->db->get_where($this->INSTANCE,array('merchant_id'=>$merchantID));
		$result=$result->row();
		return $result->merchant_name;
	}
}
?>