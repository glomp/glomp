<?php

/*
 * Widget Model Class
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Widget_m extends Super_model {

    function __construct() {
        parent::__construct('gl_widget widget');
    }
}