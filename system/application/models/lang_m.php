<?php
require_once('super_model.php');
class Lang_m extends Super_model
{
	function __Construct()
	{
		parent::__construct('gl_language lang');
		if(!$this->session->userdata('lang_id'))
		{
			$this->selectLang();
			$this->lang->load('EN_files', '');			
		}
		else{
			$lang_filename 	=$this->loadThisLang('lang_key',$this->session->userdata('lang_id'));
 			$lang_name		='';//$this->loadThisLang('lang_name',$this->session->userdata('lang_id'));
			$this->lang->load($lang_filename.'_files', $lang_name);
		}
		/*if($this->session->userdata('lang_id')=='1')
		{
			$this->lang->load('EN_files', '');
			//$this->lang->load('en_label', 'english');
		}
		else if($this->session->userdata('lang_id')=='2')
		{
			//$this->lang->load('cn_label', 'chinese');
		}*/
		
	}
	function loadThisLang($name,$id){
		$sql="SELECT ".$name." FROM gl_language
		WHERE lang_id=".$id;
		$q= $this->db->query($sql);
		$r=$q->row();
		return $r->$name;
	}
	function selectLang()
	{
		$sql="SELECT * FROM gl_language
		WHERE lang_default='Y' AND lang_active='Y' AND lang_deleted='N'";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            $r = $q->row();
            $data = array(
                'lang_id' => $r->lang_id
            );
            $def_lang = $r->lang_id;
            $this->session->set_userdata($data);
        } else {
            $sql = "select * from gl_language
					where lang_active='Y' and lang_deleted='N' order by lang_name asc limit 1";
            $q = $this->db->query($sql);
            if ($q->num_rows() > 0) {
                $r = $q->row();
                $data = array(
                    'lang_id' => $r->lang_id
                );
                $this->session->set_userdata($data);
            }
        }
    }

    function selectAllLang() {
        $sql = "select * from gl_language
        where lang_active='Y' and lang_deleted='N' order by lang_id ASC";
        
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        else
            return false;
    }

    function listAllLang() {
        $sql = "select * from gl_language
					where lang_active='Y' and lang_deleted='N' order by lang_id ASC";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $rows[] = (object) array(
                            'lang_id' => $row->lang_id,
                            'lang_name' => $row->lang_name,
                            'lang_key' => $row->lang_key
                );
            }
            return $rows;
        }
        else
            return false;
    }

    function selectLangID($id = 0) {
        $sql = "select * from gl_language
        where lang_id='" . $id . "' AND lang_active='Y' and lang_deleted='N' limit 1";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            $r = $q->row();
            $data = array(
                'lang_id' => $r->lang_id
            );
            $this->session->set_userdata($data);
        }
    }

}

?>