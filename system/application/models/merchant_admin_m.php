<?php 
class Merchant_admin_m extends CI_Model {

	private $INSTANCE="gl_merchant";
	
	function generate_hash($length = 8) {
    $chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789$%^!@&*';
    $count = mb_strlen($chars);

	for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}
		return $result;
	}

	function loginValidation()
	{
		$username = $this->input->post('username');
		$plain_password = $this->input->post('password');
		$ip = $_SERVER['REMOTE_ADDR'];
		$cur_date=date('Y-m-d');
		$currentTime = time();
		$login_success = false;
		
		$result=$this->db->get_where($this->INSTANCE,array('merchant_email'=>$username,'merchant_status'=>'Active'));

		
		
		if($result->num_rows()==1)
		{		
			
			$merchant_rec = $result->row();
			if(strtolower($merchant_rec->merchant_email) == strtolower($username))
			{
				
				$merchant_salt = $merchant_rec->merchant_salt;
				$merchant_id = $merchant_rec->merchant_id;
				
				$hash_password = $this->hash_key_value($merchant_salt,$plain_password);
				
				//now verify password
				$result_rec = $this->db->get_where($this->INSTANCE,array('merchant_id'=>$merchant_id,'merchant_password'=>$hash_password));
				
				if($result_rec->num_rows() == 1)
				{
				
					$data = array('merchant_id'=>$merchant_rec->merchant_id,
								'merchant_name'=>$merchant_rec->merchant_name,
								'merchant_email'=>$merchant_rec->merchant_email,
								'merchant_last_login'=>$merchant_rec->merchant_last_login,
								'merchant_last_ip'=>$merchant_rec->merchant_last_ip,
								'is_logged_in'=>true
								);
					$this->session->set_userdata($data);
				
					$merchant_id=$this->session->userdata('merchant_id');
					
					$update_data=array(
					'merchant_last_login'=>$cur_date,
					'merchant_last_ip'=>$ip,
					);
					
					$this->db->where('merchant_id',$merchant_id);
					$this->db->update($this->INSTANCE, $update_data); 
					//die("password matched");
					return true;
				}//end of password match verification
				else
				{
					return false;
				}
			
			}//email address string verificaiton
		}//number of rows 
		//it always return false	
		return false;
	}//function end
	
	
	function resetPassword($loginAs)
	{
		$hash_key = md5(time());
		
	}
	function hash_key_value($salt,$password)
	{
		$hash_password = hash('SHA256',$salt.$password);		
		return $hash_password;
	}

	function selectAdmin($merchant_id=0)
	{
		
		if(is_array($merchant_id))
			$result = $this->db->get_where($this->INSTANCE,$merchant_id);
		else 
			$result = $this->db->get($this->INSTANCE);

		return $result;
		
		}


#update admin
function editAdmin($merchant_id)
{
	$where=array(
		'merchant_id'=>$merchant_id
		);
	
	if(isset($_POST['reset_password']))
	{
		$merchant_salt = $this->generate_hash();
		$merchant_password = $this->hash_key_value($merchant_salt,$this->input->post('password'));
		$data_update = array('merchant_password'=>$merchant_password);
		$this->db->update($this->INSTANCE,$data_update,$where);
	}
	$content=array(
			'merchant_name'=>$this->input->post('merchant_name'),
			'merchant_email'=>$this->input->post('email_address'),
			'admin_status'=>$this->input->post('admin_status'),
			);
		
		$this->db->update($this->INSTANCE,$content,$where);
}
function adminEmailCheck($email,$merchant_id)
{	

	$result = "SELECT * from $this->INSTANCE where merchant_email = '$email' and  merchant_id != '$merchant_id'";
	$result = $this->db->query($result);
	return $result;
}
}//eoc
?>