<?php 
/*
 * This model class is for managing user notification settings
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @parent class super_model.php (Extends CI_Model)
 * @depenencies none             
 * @version     1.0
 */
require_once('super_model.php');
class User_notification_settings_m extends Super_model {
    
    function __construct() {
        parent::__construct('gl_user_notification_settings uns');
        
        $CI =& get_instance();
        $CI->load->model('notifications_m');
    }
    
    function is_checked($notification_id = 0, $user_id = 0) {
        
        $args = array(
            'select' => 'setting_user_id, checked',
            'where' => 'setting_user_id = "'.$user_id.'" AND notification_id = "'.$notification_id.'"',
        );
        
        $rec = $this->get_record($args);
        
        //var_dump($rec2);
        
        //If no record at all default is to check the checkbox
        if (is_bool($rec) && $rec == FALSE) {
            return TRUE;
        }
        
        if ($rec['checked'] == 'yes') return TRUE;
        
        return FALSE;
        
        //Normal rule
        if ($rec === FALSE) {
            return FALSE;
        }
        
        if (count($rec) > 0)  {
            return TRUE;
        }
        
        return FALSE;
    }
    
    function set_notifcations($data, $user_id) {
        if (empty($data)) {
            $data = array();
        }
        
        $notifications = $this->notifications_m->get_list();

        foreach($notifications as $not) {
            $args = array(
                'select' => 'setting_user_id',
                'where' => 'notification_id = "'.$not['notification_id'].'" 
                    AND setting_user_id = "'.$user_id.'"'
            );
            
            $rec = $this->get_record($args);
            
            $set_data = array(
                'setting_user_id' => $user_id,
                'notification_id' => $not['notification_id'],
                'checked' => (in_array($not['notification_id'], $data)) ? 'yes' : 'no'
            );
            
            if (is_bool($rec) && $rec == FALSE) {    
                $this->_insert($set_data);
                continue;
            }
            
            $where = array(
                'setting_user_id' => $user_id,
                'notification_id' => $not['notification_id'],
            );

            $this->_update($where, $set_data);
        }
        
        $set_data = array(
            'checked' => 'no'
        );
        
        if (count($data) > 0) {
            $notifications_ids = implode(",", $data);
            $where = 'notification_id NOT IN ('.$notifications_ids.') AND setting_user_id ='.$user_id;
            //All unchecked items must be set to checked = no
        } else {
            $where = 'setting_user_id ='.$user_id;
        }
        
        return $this->_update($where, $set_data);
    }
}