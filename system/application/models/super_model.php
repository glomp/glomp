<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super_model extends CI_Model {
	
	private $table;
	private $id;
	
	function __construct($table = NULL, $id = NULL)
	{
		parent::__construct();
		
		if ( ! empty($table)) $this->set_table($table);
		if ( ! empty($id)) $this->set_id($id);
	}
	
	/**
	 * set_table
	 * 
	 * Set current table being used
	 * 
	 * @access public
	 * @param string
	 * @return void
	 */
	function set_table($table)
	{
		$this->table = $table;
	}
	
	/**
	 * set_id
	 * 
	 * Set primary id of table
	 * 
	 * @access public
	 * @param string
	 * @return void
	 */
	function set_id($id)
	{
		$this->id = $id;
	}
	
	/**
	 * get_id
	 * 
	 * Get primary id of table
	 * 
	 * @access public
	 * @param void
	 * @return string
	 */
	function get_id()
	{
		return $this->id;
	}
	
	/**
	 * set_params
	 * 
	 * Set parameters values
	 * 
	 * @access public
	 * @param array
	 * @param array
	 * @return array
	 */
	function set_params($params, $args)
	{
		foreach ($args as $i => $value)
		{
			if (isset($args[$i]))
			{
				$params[$i] = $value;
			}
		}
		
		return $params;
	}
	
	/**
	 * get_total_dependents
	 * 
	 * Get total rows with data dependent on value specified
	 * 
	 * @access public
	 * @param array
	 * @param array
	 * @param boolean
	 * @return integer
	 */
	function get_total_dependents($where, $table = NULL, $and = FALSE)
	{
		$this->db->select('COUNT(1) total');		
		
		if ($and)
		{
			$this->db->where($where);
		}
		else
		{
			$this->db->or_where($where);
		}
		
		if ( ! empty($table))
		{
			$rec = $this->db->get($table);
		}
		else
		{
			$rec = $this->db->get($this->table);
		}
				
		$record = $rec->row();
		$rec->free_result();
		
		return $record->total;
	}
	
	/**
	 * _insert
	 * 
	 * Inserts new record
	 * 
	 * @access private
	 * @param array
	 * @return integer
	 */
	function _insert($data)
	{
		$table = (strpos($this->table, ' ') !== FALSE) ? substr($this->table, 0, strpos($this->table, ' ')) : $this->table;
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * _update
	 * 
	 * Updates record
	 * 
	 * @access private
	 * @param integer/array
	 * @param array
	 * @return boolean
	 */
	function _update($where, $data)
	{
		$table = (strpos($this->table, ' ') !== FALSE) ? substr($this->table, 0, strpos($this->table, ' ')) : $this->table;
		
		if (is_numeric($where)) $this->db->where($this->id, $where);
		else $this->db->where($where);
		
		return $this->db->update($table, $data);
	}
	
	/**
	 * _delete
	 * 
	 * Deletes user record
	 * 
	 * @access private
	 * @param integer/array
	 * @return boolean
	 */
	function _delete($where)
	{
		$table = (strpos($this->table, ' ') !== FALSE) ? substr($this->table, 0, strpos($this->table, ' ')) : $this->table;
		
		if (is_numeric($where)) $this->db->where($this->id, $where);
		else $this->db->where($where);
		
		return $this->db->delete($table);
	}
	
	/**
	 * get_value
	 * 
	 * Gets value of specified field
	 * 
	 * @access public
	 * @param string
	 * @param array
	 * @param string
	 * @return string
	 */
	function get_value($field, $where) 
	{
		$this->db->select($field, FALSE);
		
		if (is_numeric($where)) $this->db->where($this->id, $where);
		else $this->db->where($where);
		
		$rec = $this->db->get($this->table);
		
		if ($rec->num_rows() >= 1)
		{
			$record = $rec->row();
			$rec->free_result();
		
			return $record->$field;
		}
		
		return FALSE;
	}
	
	/**
	 * exists
	 * 
	 * Checks if a record already exists
	 * 
	 * @access public
	 * @param array
	 * @return boolean
	 */
	function exists($where = array())
	{
		$this->db->select('1', FALSE);
		
		if (is_numeric($where)) $this->db->where($this->id, $where);
		else $this->db->where($where);
		
		$rs = $this->db->get($this->table);
		
		if ($rs->num_rows() > 0)
		{
			return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * get_total
	 * 
	 * Gets total records based on qualifier
	 * 
	 * @access public
	 * @param array
	 * @return integer
	 */
	function get_total($where = NULL)
	{
		$this->db->select('COUNT(1) total', FALSE);
		
		$this->db->from($this->table);
		
		if (! empty($where)) 
		{
			$this->db->where($where, NULL, FALSE);
		}
		
		$rec = $this->db->get();
		$record = $rec->row();
		$rec->free_result();
		
		return $record->total;
	}
	
	/**
	 * get_record
	 * 
	 * Get table record
	 * 
	 * @access public
	 * @param array
	 * @return array
	 */
	function get_record($args = array())
	{
		$params = array(
			'select' => NULL,
			'where' => NULL,
			'join' => array(),
			'order_by' => NULL,
                        'resource_id' => FALSE,
		);
		
		$params = $this->set_params($params, $args);
		
		if ( ! empty($params['select']))
		{
			$this->db->select($params['select'], FALSE);
		}
		
		$this->db->from($this->table);
		
		if ( ! empty($params['where']))
		{
			if (is_numeric($params['where'])) $this->db->where($this->id, $params['where']);
			else $this->db->where($params['where']);
		}
		
		if (count($params['join']) > 0)
		{
			foreach ($params['join'] as $join)
			{
				if (empty($join['join'])) $join['join'] = 'RIGHT';
				
				$this->db->join($join['table'], $join['condition'], $join['join']);
			}
		}
                
                if ( ! empty($params['order_by']))
		{
			$this->db->order_by($params['order_by']);
		}
		
		$this->db->limit(1);
		
		$rec = $this->db->get();
                
                //echo $this->db->last_query();
		
		if ($rec->num_rows() < 1)
		{
			return FALSE;
		}
                
                if ( ! $params['resource_id'])
		{
                    $record = $rec->row_array();
                    $rec->free_result();
                    return $record;
		}
		
		return $rec;
	}
	
	/**
	 * get_list
	 * 
	 * Get table list
	 * 
	 * @access public
	 * @param array
	 * @return array/object
	 */
	function get_list($args = array())
	{
		$params = array(
			'select' => NULL,
			'where' => NULL,
			'join' => array(),
			'like'=>NULL,
			'order_by' => NULL,
			'group_by' => NULL,
			'offset' => NULL,
			'limit' => NULL,
			'resource_id' => FALSE,

		);
		
		$params = $this->set_params($params, $args);
		
		if ( ! empty($params['select']))
		{
			$this->db->select($params['select'], FALSE);
		}
		
		$this->db->from($this->table);
		
		if ( ! empty($params['where']))
		{
			$this->db->where($params['where']);
		}
		
		if (count($params['join']) > 0)
		{
			foreach ($params['join'] as $join)
			{
				if (empty($join['join'])) $join['join'] = NULL;
				
				$this->db->join($join['table'], $join['condition'], $join['join']);
			}
		}

                if ( ! empty($params['like']))
		{
			$this->db->like($params['like'], FALSE);
		}


		if ( ! empty($params['order_by']))
		{
			$this->db->order_by($params['order_by']);
		}
                
		if ( ! empty($params['group_by']))
		{
			$this->db->group_by($params['group_by']);
		}
		
		if ( ! empty($params['limit']))
		{
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		$rs = $this->db->get();
                
                //echo $this->db->last_query();
		
		if ( ! $params['resource_id']) 
		{
			$result = $rs->result_array();
			$rs->free_result();
			return $result;
		}
		
		return $rs;
	}
	
	/**
	 * query
	 * 
	 * Run customized query
	 * 
	 * @access public
	 * @param string
	 * @return object
	 */
	function query($sql)
	{
		$rs = $this->db->query($sql);
		
		$result = $rs->result_array();
		$rs->free_result();
		return $result;
	}
}

/* End of file super_model.php */
/* Location: ./application/models/super_model.php */
