<?php
require_once('super_model.php');
class Activity_m extends Super_model {

    private $INSTANCE = "gl_activity_log";

    function __construct() {
        parent::__construct('gl_activity_log activity');
    }    
    function get_activity($user_id,$start=0,$limit=0, $log_type="")    
	{        
		
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";            
        $type_array="'friend_added_by_friend', 'invited_a_friend', 'unfriend_a_friend', 'friend_added_you', 'added_a_friend', 'friend_redeemed_a_voucher', 'redeemed_a_voucher', 'received_a_reward', 'received_a_glomp', 'glomp_someone'";
        if($log_type!="" && $log_type!="log")
        {
            $type_array="'points_purchased', 'points_spent', 'points_reversed', 'points_rewarded'";
        }                        
           
		$sql = " SELECT *
				FROM gl_activity_log
				WHERE user_id='".$user_id."' AND log_type IN(".$type_array.")
				ORDER BY log_timestamp DESC
				$limit_query
				";
        $rec = $this->db->query($sql);
        /*
        $rec = $this->get_list(array(            
            'where' => array(                
                'user_id' => $user_id
            ),
            'order_by' => 'log_timestamp DESC',            
            'resource_id' => true
        ));                
        //'limit' => '1000',
        */
        return $rec;
    }
}
?>