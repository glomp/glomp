<?php
/*
 * This model class is for Campagin rule
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @parent class super_model.php (Extends CI_Model)
 * @depenencies none             
 * @version     1.0
 */
require_once('super_model.php');
class Campaign_rule_m extends Super_model {
    
    function __construct() {
        parent::__construct('gl_campaign_rule cr');
    }
    
    function get_ischecked($name = '', $camp_id) {
        
        $rec = $this->get_record(array(
            'select' => $name,
            'where' => array(
                $name => 'yes',
                'camp_rule_campaign_id' => $camp_id
            )
        ));
        
        if ($rec == FALSE) {
            return FALSE;
        }
        
        return TRUE;
    }
}