<?php

#written by shree
#Date july 29, 2013
#this class will have only users login function, user login, forgot password for both mobile and web user

class Login_m extends CI_Model {

    private $INSTANCE = "gl_user";
    private $LOGIN_USERNAME = 'user_email'; //for username just change to username
    
    public function __construct()
    {
          $CI =& get_instance();
          $CI->load->model('invites_m');
          $CI->load->model('voucher_m');
          $CI->load->model('translation_m');
          $CI->load->model('user_account_m');
          $CI->load->model('users_m');
    }

    function generate_hash($length = 8) {
        $chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    function hash_key_value($salt, $password) {
        $hash_password = hash('SHA256', $salt . $password);
        return $hash_password;
    }
    /**
    * login_using_social_app
    * 
    * login or register using social app;
    * It can be switch to different social app site
    *  
    * 
    * @access public
    * @param string
    * @return bool
    */
    function login_using_social_app($mode = 'fb') {
        if ($mode == 'linkedIN') {
            return $this->_process_linked_in();
        }
    }
    /**
    * _process_linked_in
    * 
    * process social app when using linkedIN
    * LIMIT: AJAX Request
    *  #TODO: MAKE MORE DESCRIPTION
    *
    * 
    * @access private
    * @return array
    */
    function _process_linked_in($format = '') {
        if (isset($_POST['data'])) {
            //See: controllers/api1/; function: _login_li. format = json is used is only for API.
            $data = ($format == 'json') ? json_decode($_POST['data'], TRUE) : $_POST['data'];
            
            $user_linkedin_id = '';
            $user_email = '';
            $dob = '';
            //If mobile or desktop
            $device = $_POST['device'];
            
            if (isset($data['id']) && isset($data['emailAddress'])) {
                $user_linkedin_id = $data['id'];
                $user_email = $data['emailAddress'];
            }
            
            if (isset($data['dateOfBirth'])) {
                $dob = $data['dateOfBirth']['month'].'/'.$data['dateOfBirth']['day'].'/'.$data['dateOfBirth']['year'];
                $dob = strtotime($dob);
            }
            $where = array('user_linkedin_id' => $user_linkedin_id, 'user_status' => 'Active');
            $user = $this->users_m->get_record(array('where' => $where));
            
            if ($user == FALSE) {
                $where = array('user_email' => $user_email, 'user_status' => 'Active');
                $user = $this->users_m->get_record(array('where' => $where));
                
                if ($user == FALSE) {
                    
                    
                    /*check if deactivated*/
                    $q = $this->db->get_where( 'gl_user', "user_linkedin_id = '".$user_linkedin_id."' AND deactivated_status=1");
                    if( $q->num_rows()  > 0)
                    {
                        //return $q->row();
                        $update_data = array(
                            'user_status'               => 'Active',
                            'deactivated_status'        => 0
                        );
                        $where = array(
                                'user_id' 		=>$q->row()->user_id
                                );
                        $this->db->update('gl_user', $update_data, $where);
                        
                        $this->session->set_userdata('reactivated', 'true');
                        return $this->_process_linked_in($format);
                    }
                    //deactivated_status
                    /*check if deactivated*/
                    
                    
                    
                    $this->session->set_userdata('temp_user_linkedin_id', $user_linkedin_id);
                    $this->session->set_userdata('temp_fname', $data['firstName']);
                    $this->session->set_userdata('temp_mname', '');
                    $this->session->set_userdata('temp_lname', $data['lastName']);
                    $this->session->set_userdata('temp_gender', '');
                    $this->session->set_userdata('temp_email', $user_email);
                    
                    if ( ! empty($dob) ) {
                        $this->session->set_userdata('temp_day', date('d', $dob));
                        $this->session->set_userdata('temp_months', date('m', $dob));
                        $this->session->set_userdata('temp_year', date('Y', $dob));
                    }
                    
                    $r_data = array(
                      'redirect_url' => site_url('user/register?linkedin=1'),  
                      'post_to_url'=> FALSE,
                      'status'=> 'register',
                    );
                    
                    if ($device == 'mobile') {
                        $r_data = array(
                            'redirect_url' => site_url(MOBILE_M.'/user/register/linkedin?linkedin'),  
                            'post_to_url'=> FALSE,
                            'status'=> 'register',
                        );
                    }
                    
                    return $r_data;
                    
                } else {
                    if ($user_linkedin_id == $user['user_linkedin_id']) {
                        $login_result = $this->_login_using_socialapp($user_linkedin_id, 'user_linkedin_id');
                        $login = json_decode($login_result);
                        if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                            
                            $r_data = array(
                                'redirect_url' => site_url('user/dashboard'),  
                                'post_to_url'=> FALSE,
                                'status'=> 'login',
                            );
                            
                            if ($device == 'mobile') {
                                $r_data = array(
                                    'redirect_url' => site_url(MOBILE_M.'/user/dashboard'),  
                                    'post_to_url'=> FALSE,
                                    'status'=> 'login',
                                );
                            }
                    
                            return $r_data;
                        } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                            $link = base_url('landing/resendVerification/?email=' . $user_email);
                            $click_here = "<a href='$link'>Click here</a>";
                            $v_data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                        } else {
                            $v_data['error_msg'] = "Invalid E-mail or password";
                        }
                    } else {
                        //linkedIN email has a glomp account but not connected with linkedIN
                        //update the user account and connect them with linkedIN
                        $up_data = array(
                            'user_linkedin_id' => $user_linkedin_id
                        );

                        $where = array(
                            'user_id' => $user['user_id']
                        );
                        
                        $this->users_m->_update($where,$up_data);

                        $login_result = $this->_login_using_socialapp($user_linkedin_id, 'user_linkedin_id');
                        $login = json_decode($login_result);
                        if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                            $r_data = array(
                                'redirect_url' => site_url('user/dashboard'),  
                                'post_to_url'=> FALSE,
                                'status'=> 'login',
                            );
                            
                            if ($device == 'mobile') {
                                $r_data = array(
                                    'redirect_url' => site_url(MOBILE_M.'/user/dashboard'),  
                                    'post_to_url'=> FALSE,
                                    'status'=> 'login',
                                );
                            }
                    
                            return $r_data;
                        } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                            $link = base_url('landing/resendVerification/?email=' . $user_email);
                            $click_here = "<a href='$link'>Click here</a>";
                            $v_data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                        } else {
                            $v_data['error_msg'] = "Invalid E-mail or password";
                        }
                    }
                }
            } else {
                //get username then login
                $login_result = $this->_login_using_socialapp($user_linkedin_id, 'user_linkedin_id');
                $login = json_decode($login_result);
                if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                    $r_data = array(
                        'redirect_url' => site_url('user/dashboard'),  
                        'post_to_url'=> FALSE,
                        'status'=> 'login',
                    );
                    
                    if ($device == 'mobile') {
                        $r_data = array(
                            'redirect_url' => site_url(MOBILE_M.'/user/dashboard'),  
                            'post_to_url'=> FALSE,
                            'status'=> 'login',
                        );
                    }
                    
                    return $r_data;
                } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                    $link = base_url('landing/resendVerification/?email=' . $user_email);
                    $click_here = "<a href='$link'>Click here</a>";
                    $v_data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                } else {
                    $v_data['error_msg'] = "Invalid E-mail or password";
                }
            }
            $v_data['linkedin_login'] = 'linkedin_login';
            $r_data = array(
                'redirect_url' => site_url('landing/index'),  
                'post_to_url'=> TRUE,
                'status'=> 'verify email',
                'data'=> $v_data,
            );
            
            if ($device == 'mobile') {
                $r_data = array(
                    'redirect_url' => site_url(MOBILE_M.'/landing/index'),  
                    'post_to_url'=> TRUE,
                    'status'=> 'verify email',
                    'data'=> $v_data,
                );
            }
            
            return $r_data;
        }
            
    }
    
    function check_if_fb_user_is_registered($user_fb_id) {
        $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $user_fb_id));
        return $result->num_rows();
    }
	 function check_if_fb_user_is_registered_fb($user_fb_id) {
        $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $user_fb_id, 'user_status' => 'Active'));
        return $result;
    }
	function check_if_fb_user_is_registered_email($user_email) {
        $result = $this->db->get_where($this->INSTANCE, array('user_email' => $user_email, 'user_status' => 'Active'));
        return $result;
    }
    function check_if_registered_email($user_email) {
        $result = $this->db->get_where($this->INSTANCE, array('user_email' => $user_email));
        return $result;
    }
	function user_fb_login_update_account($userID) {

        $fb_id			=$_POST['id'];
		$fb_first_name='';
		$fb_middle_name='';
		$fb_last_name='';
		
		if(isset($_POST['first_name']))
			$fb_first_name	=$_POST['first_name'];
		
		if(isset($_POST['middle_name']))
			$fb_middle_name	=$_POST['middle_name'];
				
		if(isset($_POST['last_name']))
			$fb_last_name	=$_POST['last_name'];
			
		
		$fb_gender	=ucfirst($_POST['gender']);
		$fb_email		=$_POST['email'];							
		$dob				= (isset($_POST['birthday'])) ? strtotime() : NULL;
		$fb_dob 		= (is_null($dob)) ? NULL : date('Y',$dob) . '-' . date('m',$dob) . '-' . date('d',$dob);


        $where = array(
            'user_id' => $userID
        );

        $main_contain = array(
            'user_fb_id' => $fb_id,
            'user_fb_fname' => $fb_first_name,
			'user_fb_mname' => $fb_middle_name,
            'user_fb_lname' => $fb_last_name,
			'user_fb_email' => $fb_email,
            'user_fb_dob' => $fb_dob,
			'user_fb_gender' => $fb_gender
        );
        //print_r($signup_data);
        $this->db->update($this->INSTANCE, $main_contain, $where);
        
        
        //memcached clear
        $params = array(                                                       
            'specific_names' 
                => array(
                    'user_info_by_id_'.$userID,
                    'getUserFname_'.$userID,
                    'user_short_info_'.$userID,
                    
                )    
        );
        
        delete_cache($params);
        
    }
    function login_using_fb_id($user_fb_id) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $cur_date = $this->custom_func->dateOnly();
        $currentTime = $this->custom_func->datetime();
        $login_success = false;

        $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $user_fb_id));
        if ($result->num_rows() != 0) {
            $login_rec = $result->row();
			if($login_rec->user_email_verified=='Y')
			{
				$login_session_data = array('user_id' => $login_rec->user_id,
					'username' => ($login_rec->username == "") ? $login_rec->user_email : $login_rec->username,
					'user_name' => $login_rec->user_fname . ' ' . $login_rec->user_lname,
                    'user_fname' => $login_rec->user_fname,
                    'user_lname' => $login_rec->user_lname,
					'user_email' => $login_rec->user_email,
					'user_last_login_date' => $login_rec->user_last_login_date,
					'user_last_login_ip' => $login_rec->user_last_login_ip,
					'lang_id' => $login_rec->user_lang_id,
                    'twitter_oauth_token' => $login_rec->user_twitter_token,
                    'twitter_oauth_token_secret' => $login_rec->user_twitter_secret,
					'is_user_logged_in' => true,
                    'user_status'=>$login_rec->user_status
				);
				$this->session->set_userdata($login_session_data);
				$this->session->set_userdata('since_user_logged_in', time());
				$user_id = $this->session->userdata('user_id');
	
				$update_data = array(
					'user_last_login_date' => $cur_date,
					'user_last_login_ip' => $ip,
				);
	
				$this->db->where('user_id', $user_id);
				$this->db->update($this->INSTANCE, $update_data);
				return json_encode($login_session_data);
			}
			else
			{
				$login_session_data = array('user_id' => 0, 'is_user_logged_in' => false , 'user_email_verified' => 'N', 'user_status'=>$login_rec->user_status);
				return json_encode($login_session_data);
			}
        }//number of rows
        //it always return false
        return false;
    }
    function _login_using_socialapp($id, $field) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $cur_date = $this->custom_func->dateOnly();
        $currentTime = $this->custom_func->datetime();
        $login_success = false;
        
        $where = array($field => $id, 'user_status' => 'Active');
        $result = $this->users_m->get_record(array('where' => $where));
        
        if (count($result) > 0) {
            $login_rec = $result;
            if($login_rec['user_email_verified'] == 'Y')
            {
                $login_session_data = array('user_id' => $login_rec['user_id'],
                    'username' => ($login_rec['username'] == "") ? $login_rec['user_email'] : $login_rec['username'],
                    'user_name' => $login_rec['user_fname'] . ' ' . $login_rec['user_lname'],
                    'user_fname' => $login_rec['user_fname'],
                    'user_lname' => $login_rec['user_lname'],
                    'user_email' => $login_rec['user_email'],
                    'user_last_login_date' => $login_rec['user_last_login_date'],
                    'user_last_login_ip' => $login_rec['user_last_login_ip'],
                    'lang_id' => $login_rec['user_lang_id'],
                    'twitter_oauth_token' => $login_rec['user_twitter_token'],
                    'twitter_oauth_token_secret' => $login_rec['user_twitter_secret'],
                    'is_user_logged_in' => true
                );

                $this->session->set_userdata($login_session_data);
                $this->session->set_userdata('since_user_logged_in', time());
                $user_id = $this->session->userdata('user_id');

                $update_data = array(
                    'user_last_login_date' => $cur_date,
                    'user_last_login_ip' => $ip,
                );
                $where = array('user_id' => $user_id);
                $this->users_m->_update($where, $update_data);
                return json_encode($login_session_data);
            }
            else
            {
                    $login_session_data = array('user_id' => 0, 'is_user_logged_in' => false , 'user_email_verified' => 'N');
                    return json_encode($login_session_data);
            }
        }//number of rows
        //it always return false
        return false;        
    }

//function end

    function loginValidation($username='', $plain_password='') {
        $ip = $_SERVER['REMOTE_ADDR'];
        $cur_date = $this->custom_func->dateOnly();
        $currentTime = $this->custom_func->datetime();
        $login_success = false;

        //$result = $this->db->get_where($this->INSTANCE, array($this->LOGIN_USERNAME => $username, 'user_status' => 'Active'));
		$result = $this->db->get_where($this->INSTANCE, array($this->LOGIN_USERNAME => $username ));



        if ($result->num_rows() == 1) {

            $login_rec = $result->row();
            $db_field_name = $this->LOGIN_USERNAME;
            if (strtolower($login_rec->$db_field_name) == strtolower($username)) {

                $user_salt = $login_rec->user_salt;
                $user_id = $login_rec->user_id;

                $hash_password = $this->hash_key_value($user_salt, $plain_password);

                //now verify password
                $result_rec = $this->db->get_where($this->INSTANCE, array('user_id' => $user_id, 'user_password' => $hash_password));

                if ($result_rec->num_rows() == 1) {
				
					if($login_rec->user_status=='Active')
					{
                        if($login_rec->user_email_verified=='Y')
                        {
                            $login_session_data = array('user_id' => $login_rec->user_id,
                                'username' => ($login_rec->username == "") ? $login_rec->user_email : $login_rec->username,
                                'user_name' => $login_rec->user_fname . ' ' . $login_rec->user_lname,
                                'user_email' => $login_rec->user_email,
                                'user_fname' => $login_rec->user_fname,
                                'user_lname' => $login_rec->user_lname,
                                'user_email_verified' => $login_rec->user_email_verified,
                                'user_last_login_date' => $login_rec->user_last_login_date,
                                'user_last_login_ip' => $login_rec->user_last_login_ip,
                                'lang_id' => $login_rec->user_lang_id,
                                'twitter_oauth_token' => $login_rec->user_twitter_token,
                                'twitter_oauth_token_secret' => $login_rec->user_twitter_secret,
                                'is_user_logged_in' => true
                            );
                            $this->session->set_userdata($login_session_data);
                            $this->session->set_userdata('since_user_logged_in', time());
                            $user_id = $this->session->userdata('user_id');
        
                            $update_data = array(
                                'user_last_login_date' => $cur_date,
                                'user_last_login_ip' => $ip,
                            );
        
                            $this->db->where('user_id', $user_id);
                            $this->db->update($this->INSTANCE, $update_data);
        
                            return json_encode($login_session_data);
                        }//end of email verification
                        else
                        {
                            $login_session_data = array('user_id' => $login_rec->user_id, 'deactivated_status'=>$login_rec->deactivated_status ,'user_status'=>$login_rec->user_status ,'is_user_logged_in' => false , 'user_email_verified' => $login_rec->user_email_verified);
                            return json_encode($login_session_data);
                        }
                    }//end of status verification
                    else
                    {
                        $login_session_data = array('user_id' => $login_rec->user_id, 'deactivated_status'=>$login_rec->deactivated_status ,'user_status'=>$login_rec->user_status ,'is_user_logged_in' => false , 'user_email_verified' => $login_rec->user_email_verified);
                        return json_encode($login_session_data);
                    }
                }//end of password match verification
                else {
                    $login_session_data = array('user_id' => $login_rec->user_id, 'deactivated_status'=>$login_rec->deactivated_status ,'user_status'=>$login_rec->user_status ,'is_user_logged_in' => false);
                    return json_encode($login_session_data);
                }
            }//user_statusstring verificaiton
        }//number of rows
        //it always return false
        return false;
    }

//function end
    #only validate email address will be passed to this function
    #if success it will return generated hash along with reset_success true otherwise false

    function reset_password_info($user_email) {
//        $user_array = array('user_id' => $user_id, 'user_status' => 'Active');
			//$user_array = array('user_id' => $user_id );
			$user_array = array('user_email' => $user_email );
        $res = $this->db->get_where($this->INSTANCE, $user_array);

        if ($res->num_rows() == 1) {
            $row = $res->row();
            $random_password = $this->generate_hash(8);

            $result_array = array('plain_password' => $random_password,
                'reset_success' => 'true',
                'user_name' => $row->user_fname . ' ' . $row->user_lname,
				'user_fname' => $row->user_fname,
                'user_email' => $row->user_email,
                'valid_email' => 'true',
                'user_id' => $row->user_id
            );

            return json_encode($result_array);
        } else {
            $result_array = array('valid_email' => 'false');
            return json_encode($result_array);
        }
    }
	  function reset_password_info_id($user_id) {
//        $user_array = array('user_id' => $user_id, 'user_status' => 'Active');
			//$user_array = array('user_id' => $user_id );
			$user_array = array('user_id' => $user_id );
        $res = $this->db->get_where($this->INSTANCE, $user_array);

        if ($res->num_rows() == 1) {
            $row = $res->row();
            $random_password = $this->generate_hash(8);

            $result_array = array('plain_password' => $random_password,
                'reset_success' => 'true',
                'user_name' => $row->user_fname . ' ' . $row->user_lname,
				'user_fname' => $row->user_fname,
                'user_email' => $row->user_email,
                'valid_email' => 'true',
                'user_id' => $row->user_id
            );

            return json_encode($result_array);
        } else {
            $result_array = array('valid_email' => 'false');
            return json_encode($result_array);
        }
    }

//user signup function
    function user_signup() {
        $user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer');

        $dob = $this->input->post('year') . '-' . $this->input->post('months') . '-' . $this->input->post('day');
        $dob_display = $this->input->post('dob_display');
        
        $user_salt = $this->generate_hash();
        $entered_password = $this->input->post('pword');
        $hash_password = $this->hash_key_value($user_salt, $entered_password);
        
        
        $location_id=$this->input->post('location');
        $location_text=$this->input->post('location_text');
        if(!is_numeric($location_id))
        {
        
            $location_id= $this->regions_m->getCountryID($location_text);            
        }

        $signup_data = array('username' => $this->input->post('email'),
            'user_fname' => ucfirst($this->input->post('fname')),
            'user_lname' => ucfirst($this->input->post('lname')),
            'user_email' => $this->input->post('email'),
            'user_password' => $hash_password,
            'user_salt' => $user_salt,
            'user_gender' => $this->input->post('gender'),
            'user_dob' => $dob,
            'user_dob_display' => $dob_display,
            'user_city_id' => $location_id,
            'user_join_date' => date('Y-m-d H:i:s'),
            'user_account_created_ip' => $this->custom_func->getUserIP(),
            'user_hash_key' => $this->generate_hash(25),
            'user_signup_device' => $user_signup_device,
            'user_status' => 'Active',
            'user_email_verified' => 'N',
            'user_last_updated_date' => $this->custom_func->datetime()
        );
        if ($this->input->post('fb_user_id') != "") {
            $signup_data["user_fb_id"] = $this->input->post('fb_user_id');
            $signup_data["user_fb_fname"] = $this->input->post('fb_fname');
            $signup_data["user_fb_mname"] = $this->input->post('fb_mname');
            $signup_data["user_fb_lname"] = $this->input->post('fb_lname');
            $signup_data["user_fb_email"] = $this->input->post('fb_email');
            $signup_data["user_fb_gender"] = $this->input->post('fb_gender');

            $signup_data["user_fb_dob"] = $this->input->post('fb_year') . '-' . $this->input->post('fb_months') . '-' . $this->input->post('fb_day');

            if ($this->input->post('chk_notif_via_fb') == 'Y')
                $signup_data["user_fb_nofify_via_fb"] = $this->input->post('chk_notif_via_fb');
			else
                $signup_data["user_fb_nofify_via_fb"] = 'N';
        }
        if ($this->input->post('user_linkedin_id') != "") {
            $signup_data["user_linkedin_id"] = $this->input->post('user_linkedin_id');
        }
        //print_r($signup_data);
        //$insert_id=0;
        $this->db->insert($this->INSTANCE, $signup_data);

        $insert_id = $this->db->insert_id();

        if ($insert_id > 0) {
            $result_array = array('insert_id' => $insert_id,
                'user_name' => $this->input->post('fname') . ' ' . $this->input->post('lname'),
                'user_email' => $this->input->post('email'),
                'user_password' => $hash_password,
                'user_hash_key' => $signup_data['user_hash_key'],
                'signup' => 'success');
        }
        else
            $result_array = array('signup' => 'failed');
        return json_encode($result_array);
    }
    
    function user_signup_pending($data) {

        $signup_data = array(
            'username'=> $data['email'],
            'user_fname' => ucfirst($data['fname']),
            'user_lname' => ucfirst($data['lname']),
            'user_email' => $data['email'],
            'user_join_date'=>date('Y-m-d H:i:s'),
            'user_account_created_ip'=>$this->custom_func->getUserIP(),
            'user_hash_key' => $this->generate_hash(25),
            'user_status'=>'Pending',
            'user_last_updated_date'=>$this->custom_func->datetime()
        );
        
        $this->db->insert($this->INSTANCE, $signup_data);
        return $this->db->insert_id();
    }

//user signup function
    function user_signup_update($userID)
    {
        $user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer');

        $dob = $this->input->post('year') . '-' . $this->input->post('months') . '-' . $this->input->post('day');
        $dob_display = $this->input->post('dob_display');
        $user_salt = $this->generate_hash();
        $entered_password = $this->input->post('pword');
        $hash_password = $this->hash_key_value($user_salt, $entered_password);

        $location_id=$this->input->post('location');
        $location_text=$this->input->post('location_text');
        if(!is_numeric($location_id))
        {
        
            $location_id= $this->regions_m->getCountryID($location_text);            
        }
        
        $where = array(
            'user_id' => $userID
        );

        $main_contain = array('username' => $this->input->post('email'),
            'user_fname' => ucfirst($this->input->post('fname')),
            'user_lname' => ucfirst($this->input->post('lname')),
            'user_email' => $this->input->post('email'),
            'user_password' => $hash_password,
            'user_salt' => $user_salt,
            'user_gender' => $this->input->post('gender'),
            'user_dob' => $dob,
            'user_dob_display' => $dob_display,
            'user_city_id' => $location_id,
            'user_join_date' => date('Y-m-d H:i:s'),
            'user_account_created_ip' => $this->custom_func->getUserIP(),
            'user_signup_device' => $user_signup_device,
            'user_status' => 'Active',
            'user_last_updated_date' => $this->custom_func->datetime()
        );
        
        
        
        //print_r($signup_data);
        $this->db->update($this->INSTANCE, $main_contain, $where);
    }
    
    function _verify($code,$user_id) {
        $sql = "SELECT 1 FROM gl_user WHERE user_hash_key = '$code' AND user_id='$user_id' AND user_email_verified='N' ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $where = array(
                'user_hash_key' => $code,
                'user_id' => $user_id
            );

            $main_contain = array(
                'user_email_verified' => 'Y',
                'user_status' => 'Active',
                'user_last_updated_date' => $this->custom_func->datetime()
            );

            $this->db->update($this->INSTANCE, $main_contain, $where);
            
            $rec = $this->invites_m->get_record(array(
                'where' => 'invite_invited_id = "'.$user_id.'" AND invite_voucher_id IS NOT NULL ',
                'order_by' => 'invite_id DESC',
            ));
            
            $invite_where = array('invite_invited_id' => $user_id);
            $invite_rec = $this->invites_m->exists($invite_where);
            //For vouchers invite
            if ($invite_rec) {
                $this->invites_m->_update($invite_where, array(
                    'invite_responded' => 'Y',
                    'invite_responded_date' => $this->custom_func->datetime(),
                ));
            }
            $user_rec = $this->user_account_m->user_short_info1($user_id);
            $user_rec = json_decode($user_rec);
            
            $invite_where = array('invite_invited_email' => $user_rec->user_email);
            $invite_rec = $this->invites_m->exists($invite_where);
            //For invites without treat(glomp)
            if ($invite_rec) {
                $this->invites_m->_update($invite_where, array(
                    'invite_responded' => 'Y',
                    'invite_responded_date' => $this->custom_func->datetime(),
                ));
            }
            
            //To get last invite on this email with voucher
            if ($rec !== FALSE) {    
                $voucher = $this->voucher_m->get_record(array(
                    'where' => array('voucher_id' => $rec['invite_voucher_id'])
                ));
                
                
                if ($voucher !== FALSE) {
                    if($voucher['voucher_status'] == 'Expired') {
                        
                        $not_claim_message = '';
                        $translation = $this->translation_m->get_record(array(
                            'select' => 'trans_id',
                            'where' => array('trans_label_name' => $rec['invite_notclaim_voucher_message'])
                        ));
                        
                        if($translation !== FALSE) {
                            $user = $this->users_m->user_info_by_id($user_id);
                            
                            $trans_string = $this->translation_m->get_translation_string($translation['trans_id'], $user->row()->user_lang_id);
                            $not_claim_message = $trans_string->row()->trans_detail_translation_string;
                        }
                        
                        $this->session->set_userdata('not_claim_voucher_notification', $not_claim_message);
                    }
                }
            }
            
            return TRUE;
        }
        
        return FALSE;
    }

    function emailExist($email) {
        $result = $this->db->get_where($this->INSTANCE, array('user_email' => $email, 'user_status' => 'Active'));
        return $result->num_rows();
    }

    function email_validateion($email) {
        $sql = "SELECT user_email FROM gl_user WHERE user_email = '$email' AND user_status='Active'";
        $result = $this->db->query($sql);
        return $result->num_rows();
    }

    function verify_user($email) {
        //$sql = "SELECT user_id,user_email FROM gl_user WHERE user_email = '$email' AND user_status='Active'";
		$sql = "SELECT user_fname, user_lname, user_id,user_email,user_status FROM gl_user WHERE user_email = '$email' ";
        $result = $this->db->query($sql);
        return $result;
    }
    /*
     * user_friend_and_exsists
     * Check whether the member is exists and already your friend
     * 
     */
    function user_friend_and_exists($email) {
        $user_id = $user_id = $this->session->userdata('user_id');
        
        $sql = 'SELECT u.user_id, u.user_fname, u.user_lname, u.user_email, f.friend_user_friend_id, f.friend_user_id
        FROM gl_user u
        left join gl_friends f ON  f.friend_user_id = "'.$user_id.'" and f.friend_user_friend_id = u.user_id
        WHERE u.user_email = "'.$email.'"';
        
        $result = $this->db->query($sql);
        return $result;
    }
    

    function update_email_exist_check($email, $user_id) {
        $sql = "SELECT 1 FROM gl_user WHERE user_email = '$email' AND user_id !='$user_id'";
        $result = $this->db->query($sql);
        return $result->num_rows();
    }

    function updatePhoto($user_photo, $user_id) {
        $data = array('user_profile_pic' => $user_photo);
        $where = array('user_id' => $user_id);
        $this->db->update($this->INSTANCE, $data, $where);
        
        //memcached clear
        $params = array(                                        
            'specific_names' 
                => array(
                    'user_info_by_id_'.$user_id,
                    'user_short_info_'.$user_id,
                )
        );
        
        delete_cache($params);
    }
	function updatePhotoOrig($user_photo, $user_id) {
        $data = array('user_profile_pic_orig' => $user_photo);
        $where = array('user_id' => $user_id);
        $this->db->update($this->INSTANCE, $data, $where);
        
        
        $params = array(                                        
            'specific_names' 
                => array(
                    'user_info_by_id_'.$user_id,
                    'user_short_info_'.$user_id,
                )    
        );
        
        delete_cache($params);
    }
	function getPhotoOrig($user_id) {
		$sql = "SELECT user_profile_pic_orig  FROM gl_user WHERE user_id = '$user_id'";
        $result = $this->db->query($sql);
		return $result;
	}

    function password_verify($password, $user_id) {

        $sql = "SELECT user_salt,user_password FROM gl_user WHERE user_id= '$user_id'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            $row = $res->row();
            $db_pass = $row->user_password;
            $db_salt = $row->user_salt;

            $cur_pass = $this->hash_key_value($db_salt, $password);
            if ($cur_pass == $db_pass)
                return 'ok';
        }
        return 'bad';
    }

    function update_profile($user_id) {
	
	$gender = $this->input->post('gender');
        $dob = $this->input->post('year') . '-' . $this->input->post('months') . '-' . $this->input->post('day');
        $dob_display = $this->input->post('dob_display', TRUE);
        if($dob_display=="")
            $dob_display='dob_display_wo_year';
        
        $location_id=$this->input->post('location');
        $location_text=$this->input->post('location_text');
        if(!is_numeric($location_id))
        {
        
            $location_id= $this->regions_m->getCountryID($location_text);            
        }
        
        $lang_id = 1;
        if( $this->input->post('user_lang_id') ) {
            $lang_id = $this->input->post('user_lang_id');
            $lang_filename 	= $this->db->get_where('gl_language', array('lang_id'=>$lang_id))->row()->lang_key;
            $this->lang->load($lang_filename.'_files', '');
            $this->session->set_userdata('lang_id', $lang_id);
        }
        
        $update_data = array('user_fname' => $this->input->post('fname'),
            'user_lname' => $this->input->post('lname'),
            'user_email' => $this->input->post('email'),
            'user_gender' => (empty($gender)) ? NULL : $gender,
            'user_dob' => $dob,
            'user_dob_display' => $dob_display,            
			'user_city_id' => $location_id,
            'user_lang_id' => $lang_id
        );
        $where = array('user_id' => $user_id);
        $this->db->update('gl_user', $update_data, $where);
		if ( isset($_POST['chk_notif_via_fb']))
		{
			$fb_notif=$this->input->post('chk_notif_via_fb');
		}
		else{
			$fb_notif = 'N';
		}
		$update_data = array('user_fb_nofify_via_fb' =>$fb_notif);		
			  $where = array('user_id' => $user_id);
	        $this->db->update('gl_user', $update_data, $where);

		//memcached clear
        $params = array(
            'affected_tables' 
                    => array(
                        'gl_user'
                    ), #cache name
            'specific_names' 
                => array(
                    'user_info_by_id_'.$user_id,
                    'getUserFname_'.$user_id,
                    'user_short_info_'.$user_id,
                    
                )    
        );
        delete_cache($params);                
        //memcached clear
    }

    function change_password($new_password, $user_id) {
        $res_salt = $this->db->get_where($this->INSTANCE, array('user_id' => $user_id));
        $rec_salt = $res_salt->row();
        $user_salt = $rec_salt->user_salt;
        $new_pass = $this->hash_key_value($user_salt, $new_password);
        $new_hash = $this->generate_hash(50);
        $res_update = $this->db->update($this->INSTANCE, array('user_password' => $new_pass, 'user_hash_key' => $new_hash), array('user_id' => $user_id));
    }

    function preFillSelect($hashKey) {
        $sql = "SELECT user_id,user_fname,user_lname,user_email,username,user_city_id FROM gl_user WHERE user_hash_key = '$hashKey' AND user_status ='Pending'";
        $result = $this->db->query($sql);
        return $result;
    }

    function ckeckPrefillEmail($user_id) {
        $sql = "SELECT 1 FROM gl_user WHERE user_id = '$user_id' AND (user_status ='Pending' OR user_status ='Registered')";
        $result = $this->db->query($sql);
        return $result->num_rows();
    }
	function checkThisEmailIfExistsAndPending($email){
		$sql = "SELECT user_id,user_hash_key FROM gl_user WHERE user_email= '$email' AND (user_status ='Pending' OR user_status ='Registered')";
        $result = $this->db->query($sql);
		return $result;
	}
    function getUserHashKey($user_id){
		$sql = "SELECT user_hash_key FROM gl_user WHERE user_id= '$user_id'";
        $result = $this->db->query($sql);
		return $result;
	}
	function checkIfThisVoucherExist($voucher){
		$sql = "SELECT voucher_purchaser_user_id FROM gl_voucher WHERE voucher_id= '$voucher' AND (voucher_status ='Consumable' OR voucher_status ='Assigned')";
        $result = $this->db->query($sql);
		return $result;
	}

}
