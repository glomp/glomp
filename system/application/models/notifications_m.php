<?php 
/*
 * This model class is for managing notifications
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @parent class super_model.php (Extends CI_Model)
 * @depenencies none             
 * @version     1.0
 */
require_once('super_model.php');
class Notifications_m extends Super_model {
    
    function __construct() {
        parent::__construct('gl_notifications notification');
    }
}