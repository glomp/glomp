<?php
/*
 * This model class is for managing products stocks
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @parent class super_model.php (Extends CI_Model)
 * @depenencies none             
 * @version     1.0
 */
require_once('super_model.php');
class Product_inventory_m extends Super_model {
    function __construct() {
        parent::__construct('gl_product_inventory pi');
    }
    
    function current_stock($prod_id) {
        
        $join_product = array('table' => 'gl_product p'
            , 'condition' => 'p.prod_id = pi.prod_id');
        
        //CHECK FIRST IF CATEGORY OF A PRODUCT IS 9 (GIVEAWAY)
        $rec = $this->get_record(array(
            'where' => array('p.prod_id' => $prod_id, 'p.prod_cat_id' => 9),
            'join' => array($join_product),
            'order_by' => 'prod_inv_id DESC'
        ));
        
        return $rec;
    }
}