<?php

#written by shree
#Date Aug 18, 2013
#This model will have all about campaign
/* Below functions are depended to other model

  user_short_info	=>user_account
  productInfo =>product
 */

class Campaign_m extends CI_Model {

    protected $INSTANCE = "gl_campaign";
    protected $INSTANCE_DETAILS = "gl_campaign_details";
    protected $INSTANCE_DETAILS_PROMO = "gl_campaign_details_by_promo";	
    protected $INSTANCE_CAMP_RULE = 'gl_campaign_rule';
    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;
    
    
    public $white_label_list = Array(  'amex-sg'=> Array( 'name' => 'Amex SG', 'alias' => 'amex-sg')
    );
    
    public $white_label_actions = Array('viewed_landing_page','viewed_merchant_page','added_to_cart','viewed_cart_page','checkout','success_transaction');
    

    function selectCampaign($by='by_rule', $start = 0, $per_page = 0) {
        $limit_query = "";
        if ($per_page > 0)
            $limit_query = " limit $start, $per_page";
		if($by=='by_rule')
			$sql = "SELECT * FROM " . $this->INSTANCE . " WHERE campaign_by_type='$by' ORDER BY campaign_id DESC  $limit_query";
		else
			$sql = "SELECT * FROM " . $this->INSTANCE . " JOIN " . $this->INSTANCE_DETAILS_PROMO ." ON " . $this->INSTANCE . ".campaign_id=" . $this->INSTANCE_DETAILS_PROMO . ".campaign_id
			WHERE campaign_by_type='$by' ORDER BY " . $this->INSTANCE . ".campaign_id DESC  $limit_query";
        $res = $this->db->query($sql);
        return $res;
    }
	
	function get_total_campaign_voucher_by_promo($campaign_id,$by_type="")
	{
		if($by_type=='')
			$sql = "SELECT SUM(camp_voucher_qty) AS total FROM " . $this->INSTANCE_DETAILS . " WHERE camp_status='Active' AND camp_campaign_id='$campaign_id'";
		else
			$sql = "SELECT SUM(camp_voucher_qty) AS total FROM " . $this->INSTANCE_DETAILS . " WHERE camp_status='Active' AND camp_campaign_id='$campaign_id' AND campaign_by_type='$by_type'";
		$res = $this->db->query($sql);		
			$row = $res->row();
			$total=$row->total;
		
		if(!is_numeric($total))
			$total=0;
		return $total;
	}

    function cmapaign_by_id($camp_id) {
        $arr = array('campaign_id' => $camp_id);
        $res = $this->db->get_where($this->INSTANCE, $arr);
        return $res;
    }
	
    function campaign_by_id_by_promo($camp_id) {
        $sql = "SELECT * FROM " . $this->INSTANCE . " JOIN " . $this->INSTANCE_DETAILS_PROMO ." ON " . $this->INSTANCE . ".campaign_id=" . $this->INSTANCE_DETAILS_PROMO . ".campaign_id
			WHERE " . $this->INSTANCE . ".campaign_id='$camp_id'";
        $res = $this->db->query($sql);
        return $res;
    }

    function campaign_details_by_id($campaign_id) {
        $arr = array('camp_campaign_id' => $campaign_id, 'camp_status' => 'Active');
        $res = $this->db->get_where($this->INSTANCE_DETAILS, $arr);
        return $res;
    }
	function campaign_details_by_id_by_type($campaign_id,$type='by_rule') {
        $arr = array('camp_campaign_id' => $campaign_id, 'camp_status' => 'Active', 'campaign_by_type' => $type);
        $res = $this->db->get_where($this->INSTANCE_DETAILS, $arr);
        return $res;
    }
    
    function create_invite_details($campaign_id, $counter) {
        $this->load->model('login_m');
        $data['sender_email'] = $_POST['invite_sender_email'][$counter];
        $data['sender_name'] = $_POST['invite_sender_name'][$counter];
        $data['email'] = $_POST['invite_email'][$counter];
        $data['fname'] = $_POST['invite_fname'][$counter];
        $data['lname'] = $_POST['invite_lname'][$counter];
        
        //If email address is not exist create a new user with a pending status
        if (! $this->login_m->emailExist($data['email'])) {
            $u_id =  $this->login_m->user_signup_pending($data);
            #TODO: Send an email with prefill link
            if (! $u_id) {
                return FALSE;
            }
        } else {
            $rec = $this->user_account_m->user_short_info_by_email($data['email']);
            $rec = json_decode($rec);
            
            $u_id = $rec->user_id;
        }
        
        //Add to gl_campaign_invite_users
        $campaign_invite_d['camp_campaign_id'] = $campaign_id;
        $campaign_invite_d['user_id'] = $u_id;
        $campaign_invite_d['sender_email'] = $data['sender_email'];
        $campaign_invite_d['sender_name'] = $data['sender_name'];
                
        $ciu_id = $this->set_campaign_invite_users($campaign_invite_d);
        
        return $ciu_id;
    }
    
    function set_campaign_invite_users($data) {
        $data = array(
            'camp_campaign_id'=> $data['camp_campaign_id'],
            'user_id' => $data['user_id'],
            'sender_email' => $data['sender_email'],
            'sender_name' => $data['sender_name'],
            'date_created'=>$this->custom_func->datetime()
        );
        
        #TODO: PLEASE USE GLOBAL VARIABLE TO INITIALIZE TABLE
        $this->db->insert('gl_campaign_invite_users', $data);
        return $this->db->insert_id();
    }
    
    function remove_campaign_details($details_id) {
        $data = array('camp_status' => 'Deleted');
        $whr = array('camp_details_id' => $details_id);
        $this->db->update($this->INSTANCE_DETAILS, $data, $whr);
    }
	
	function create_campaign_by_promo()
    {
        $arr = array('campaign_name' => $this->input->post('campaign_name'),
            'campaign_type' => $this->input->post('campaign_type'),
            'campaign_voucher_expiry_day' => $this->input->post('expiry_day'),
            'campaign_created_by_id' => $this->session->userdata('admin_id'),
            'campaign_created_date' => $this->custom_func->datetime(),
			'campaign_by_type' =>'by_promo'
        );
        $this->db->insert($this->INSTANCE, $arr);
		$campaign_id=$this->db->insert_id();
		
		$start_date='0000-00-00 00:00:00';
		$expiry_date='0000-00-00 00:00:00';
		$enable_time_limit='N';
		if(isset($_POST['enable_time_limit']) && $_POST['enable_time_limit']=='Y')
		{
			$enable_time_limit='Y';
			$start_date=$this->input->post('start_date');
			$start_date=date('Y-m-d',strtotime($start_date)).' 00:00:00';
			
			$expiry_date=$this->input->post('expiry_date');
			$expiry_date=date('Y-m-d',strtotime($expiry_date)).' 00:00:00';
		}
		
		$follow_up_campaign='N';
		if(isset($_POST['follow_up_campaign']) && $_POST['follow_up_campaign']=='enable'){
			$follow_up_campaign='Y';
		}
		$follow_up_campagin_type=$this->input->post('follow_up_campagin_type');
		$arr = array('campaign_id' => $campaign_id,
			'campaign_by_promo_code' => $this->input->post('promo_code'),
            'campaign_by_promo_start_date' => $start_date,
            'campaign_by_promo_end_date' => $expiry_date,
			'campaign_by_promo_for' => $this->input->post('campaign_for'),
			'campaign_by_promo_follow_up' => $follow_up_campaign,
			'campaign_by_promo_follow_up_campagin_type' => $follow_up_campagin_type,
			'campaign_by_promo_enable_time_limit' => $enable_time_limit,
			'campaign_by_promo_rule_desc' => $this->input->post('rule_desc')
			
        );
        $this->db->insert($this->INSTANCE_DETAILS_PROMO, $arr);
        
        return $campaign_id;
    }

    function create_campaign() {
        $arr = array('campaign_name' => $this->input->post('campaign_name'),
            'campaign_type' => $this->input->post('campaign_type'),
            'campaign_voucher_expiry_day' => $this->input->post('expiry_day'),
            'campaign_created_by_id' => $this->session->userdata('admin_id'),
            'campaign_created_date' => $this->custom_func->datetime()
        );
        $this->db->insert($this->INSTANCE, $arr);
        return $this->db->insert_id();
    }

    function update_campaign($camp_id) {
        $arr = array('campaign_name' => $this->input->post('campaign_name'),
            'campaign_type' => $this->input->post('campaign_type'),
            'campaign_voucher_expiry_day' => $this->input->post('expiry_day'),
            'campaign_updated_by_id' => $this->session->userdata('admin_id')
        );
        $whr = array('campaign_id' => $camp_id);
        $this->db->update($this->INSTANCE, $arr, $whr);
    }
	function update_campaign_by_promo($camp_id) {
        $arr = array('campaign_name' => $this->input->post('campaign_name'),
            'campaign_type' => $this->input->post('campaign_type'),
            'campaign_voucher_expiry_day' => $this->input->post('expiry_day'),
            'campaign_updated_by_id' => $this->session->userdata('admin_id')
        );
        $whr = array('campaign_id' => $camp_id);
        $this->db->update($this->INSTANCE, $arr, $whr);
		
		
		$start_date='0000-00-00 00:00:00';
		$expiry_date='0000-00-00 00:00:00';
		$enable_time_limit='N';
		if(isset($_POST['enable_time_limit']) && $_POST['enable_time_limit']=='Y')
		{
			$enable_time_limit='Y';
			$start_date=$this->input->post('start_date');
			$start_date=date('Y-m-d',strtotime($start_date)).' 00:00:00';
			
			$expiry_date=$this->input->post('expiry_date');
			$expiry_date=date('Y-m-d',strtotime($expiry_date)).' 00:00:00';
		}		
		
		$follow_up_campaign='N';
		if(isset($_POST['follow_up_campaign']) && $_POST['follow_up_campaign']=='enable')
			$follow_up_campaign='Y';
		$follow_up_campagin_type=$this->input->post('follow_up_campagin_type');
		$arr = array(
			'campaign_by_promo_code' => $this->input->post('promo_code'),
            'campaign_by_promo_start_date' => $start_date,
            'campaign_by_promo_end_date' => $expiry_date,
			'campaign_by_promo_for' => $this->input->post('campaign_for'),
			'campaign_by_promo_follow_up' =>$follow_up_campaign,
			'campaign_by_promo_follow_up_campagin_type' => $follow_up_campagin_type,
			'campaign_by_promo_enable_time_limit' => $enable_time_limit,
			'campaign_by_promo_rule_desc' => $this->input->post('rule_desc')
        );
		$whr = array('campaign_id' => $camp_id);
        $this->db->update($this->INSTANCE_DETAILS_PROMO, $arr, $whr);
		
    }

    function create_campaign_details($campaign_id, $prod_id, $qty,$type='by_rule') {
        $prod_details = $this->prod_info($prod_id);
        $arr = array('camp_campaign_id' => $campaign_id,
            'camp_prod_id' => $prod_id,
            'camp_prod_point' => $prod_details->prod_point,
            'camp_voucher_qty' => $qty,
			'campaign_by_type' => $type
        );
        $this->db->insert($this->INSTANCE_DETAILS, $arr);
    }

    function update_campaign_details($details_id, $prod_id, $qty,$type='by_rule') {
        $prod_details = $this->prod_info($prod_id);
        $arr = array('camp_prod_id' => $prod_id,
            'camp_prod_point' => $prod_details->prod_point,
            'camp_voucher_qty' => $qty,
			'campaign_by_type' => $type
        );
        $whr = array('camp_details_id' => $details_id);
        $this->db->update($this->INSTANCE_DETAILS, $arr, $whr);
    }

    function create_campaign_rule($campaign_id, $min_age, $max_age, $gender, $region_id) {
        $arr = array('camp_rule_campaign_id' => $campaign_id,
            'min_age' => $min_age,
            'max_age' => $max_age,
            'gender' => $gender,
            'region_id' => $region_id
        );
        $this->db->insert($this->INSTANCE_CAMP_RULE, $arr);
    }

    function prod_info($prod_id) {
        $whr = array('prod_id' => $prod_id);
        $res = $this->db->get_where('gl_product', $whr);
        $rec = $res->row();
        return $rec;
    }

    function select_voucher_product($camp_id, $type='by_rule') {
        $sql = "SELECT m.merchant_name,p.prod_name,p.prod_image,d.camp_voucher_qty,d.camp_prod_point,camp_campaign_id,camp_prod_id
			FROM gl_product p,gl_merchant m,gl_campaign_details d
			WHERE p.prod_merchant_id=m.merchant_id
			AND p.prod_id = d.camp_prod_id
			AND d.camp_campaign_id='$camp_id'
			AND d.camp_status = 'Active'
			AND d.campaign_by_type = '$type'
		";
        $res = $this->db->query($sql);
        return $res;
    }

    function update_campaign_rule($campaign_id) {
        $campaign_id = (int) $campaign_id;

        $list = '';
        $rule_query = '';
        if (isset($_POST['regions_id_selected'])) {
            $cnt = count($_POST['regions_id_selected']);

            for ($i = 0; $i < $cnt; $i++) {
                $list .= $_POST['regions_id_selected'][$i] . ',';
            }
            $list = trim($list, ',');
        }
        
        $min_age_checked = isset($_POST['min_age_checked']) ? 'yes' : 'no';
        $max_age_checked = isset($_POST['max_age_checked']) ? 'yes' : 'no';
        $regions_checked = isset($_POST['regions_checked']) ? 'yes' : 'no';
        $loggedover_checked = isset($_POST['loggedover_checked']) ? 'yes' : 'no';
        $noactivity_checked = isset($_POST['noactivity_checked']) ? 'yes' : 'no';
        $previously_rewarded_checked = isset($_POST['previously_rewarded_checked']) ? 'yes' : 'no';
        $notglomp_checked = isset($_POST['notglomp_checked']) ? 'yes' : 'no';
        $glomped_checked = isset($_POST['glomped_checked']) ? 'yes' : 'no';
        $lessfriends_checked = isset($_POST['lessfriends_checked']) ? 'yes' : 'no';
        $member_fb_notconnected = isset($_POST['member_fb_notconnected']) ? 'yes' : 'no';
        
                
        $map_rules = array(
            'selected_list' =>  ( isset($_POST['whitelabel_id_checked']) && isset($_POST['whitelabel_id_selected']) ) ? $_POST['whitelabel_id_selected']:'',
            'selected_actions' => ( isset($_POST['whitelabel_actions_checked']) && isset($_POST['whitelabel_actions_selected'])) ? $_POST['whitelabel_actions_selected']:'',
            'selected_merchants' => ( isset($_POST['whitelabel_merchants_checked']) && isset($_POST['whitelabel_merchants_selected'])) ? $_POST['whitelabel_merchants_selected']:'',
            'selected_products'=> ( isset($_POST['whitelabel_products_checked']) && isset($_POST['whitelabel_products_selected'])) ? $_POST['whitelabel_products_selected']:'',
            'date_from' => ( isset($_POST['date_from_checked']) && isset($_POST['whitelabel_date_from'])) ? $_POST['whitelabel_date_from']:'',
            'date_to' =>( isset($_POST['date_from_checked']) && isset($_POST['whitelabel_date_to'])) ? $_POST['whitelabel_date_to']:'',
        );
        
        
        $data = array('min_age' => $this->input->post('min_age'),
            'max_age' => $this->input->post('max_age'),
            'gender' => $this->input->post('gender'),
            'region_id' => $list,
            'cam_rule_query' => $rule_query,
            'min_age_checked' => $min_age_checked,
            'max_age_checked' => $max_age_checked,
            'regions_checked' => $regions_checked,
            'loggedover_checked' => $loggedover_checked,
            'noactivity_checked' => $noactivity_checked,
            'previously_rewarded_checked' => $previously_rewarded_checked,
            'notglomp_checked' => $notglomp_checked,
            'glomped_checked' => $glomped_checked,
            'lessfriends_checked' => $lessfriends_checked,
            'member_fb_notconnected' => $member_fb_notconnected,
            'member_less_nofriends' => $this->input->post('member_less_nofriends'),
            'member_less_glomped' => $this->input->post('member_less_glomped'),
            'member_glomped_days' => $this->input->post('member_glomped_days'),
            'member_noglomp_days' => $this->input->post('member_noglomp_days'),
            'member_prev_rewarded_days' => $this->input->post('member_prev_rewarded_days'),
            'member_noactivity_days' => $this->input->post('member_noactivity_days'),
            'member_logout_days' => $this->input->post('member_logout_days'),
            'rule_details' => json_encode(($map_rules))
        );

        $this->db->update($this->INSTANCE_CAMP_RULE, $data, array('camp_rule_campaign_id' => $campaign_id));
        $this->update_associated_regions($campaign_id);
        
    }

    function select_rule_region($regions_id_list) {
        if ($regions_id_list == "")
            $regions_id_list = -1; //nothing is selected

        $sql = "SELECT region_id,region_name FROM gl_region WHERE region_id IN($regions_id_list)";
        $res = $this->db->query($sql);
        return $res;
    }

    function select_campaign_rule($camp_id) {
        $sql = "SELECT * FROM $this->INSTANCE_CAMP_RULE WHERE camp_rule_campaign_id = '$camp_id'";
        $res = $this->db->query($sql);
        return $res;
    }

    function select_people_by_rule($camp_id, $sub_query_where = '') {
    
        //memcached clear
        $params = array(                            
            'affected_tables' 
                => array(
                    'gl_voucher'
                ) #cache name            
        );
        delete_cache($params);       
  

        
        //memcached clear
    
        $res_rule = $this->select_campaign_rule($camp_id);
        $rec_rule = $res_rule->row();
        $min_age = $rec_rule->min_age;
        $max_age = $rec_rule->max_age;
        $gender = $rec_rule->gender;
        $region_list = $rec_rule->associated_region_id;
        $logout_days = $rec_rule->member_logout_days;
        $member_fb_notconnected = $rec_rule->member_fb_notconnected;
        $member_noactivity_days = $rec_rule->member_noactivity_days;
        $member_prev_rewarded_days = $rec_rule->member_prev_rewarded_days;
        $member_noglomp_days = $rec_rule->member_noglomp_days;
        $member_less_glomped = $rec_rule->member_less_glomped;
        $member_glomped_days = $rec_rule->member_glomped_days;
        $member_less_nofriends = $rec_rule->member_less_nofriends;
        
        
        
        $rule_details = json_decode($rec_rule->rule_details);
        
        if(isset($rule_details->selected_list) && $rule_details->selected_list!='')
        {
            $temp = '';
            foreach ($rule_details->selected_list as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= " w.campaign_label='".$this->white_label_list[$row]['alias']."'";
            }
            $query_selected_list=  "( ".$temp." )";
        }
            
        else
            $query_selected_list = '';
        
        
        if(isset($rule_details->selected_actions) && $rule_details->selected_actions!='')
        {
            $temp = '';
            foreach ($rule_details->selected_actions as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= " w.action='".$row."'";
            }
            $query_selected_actions=  "( ".$temp." )";
        }
            
        else
            $query_selected_actions = '';
        
        
        if(isset($rule_details->selected_merchants) && $rule_details->selected_merchants!='')
        {
            $temp = '';
            foreach ($rule_details->selected_merchants as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= "  ( wd.name='merchant_id' AND wd.value='".$row."') ";
            }
            $query_selected_merchants=  "( ".$temp." )";
        }
            
        else
            $query_selected_merchants = '';
        
        if(isset($rule_details->selected_products) && $rule_details->selected_products!='')
        {
            $temp = '';
            foreach ($rule_details->selected_products as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= "  ( wd.name='prod_id' AND wd.value='".$row."') ";
            }
            $query_selected_products=  "( ".$temp." )";
        }
            
        else
            $query_selected_products = '';
        
        if( isset($rule_details->date_from) && $rule_details->date_from!='' && $rule_details->date_from!='0'
        && isset($rule_details->date_to) && $rule_details->date_to!='' && $rule_details->date_to!='0')
        {
            $query_date=  "( w.date_created BETWEEN '".$rule_details->date_from." 00:00:00'  AND '".$rule_details->date_to." 23:59:59')";
        }
            
        else
            $query_date = '';
        
        
        if  ( 
                $query_selected_list!='' OR
                $query_selected_actions!='' OR
                $query_selected_merchants!='' OR
                $query_selected_products!='' OR
                $query_date!='' 
            )
        {
            $query ="AND ";
            if($query_selected_list!='') 
                $query .=$query_selected_list;
            if($query_selected_actions!='') 
            {
                if($query!='')    
                    $query .=" AND ";    
                $query .=$query_selected_actions;
            }
            if($query_date!='') 
            {
                if($query!='')    
                    $query .=" AND ";    
                $query .=$query_date;
            }
            $sub_query ="";
            if($query_selected_merchants!='' OR $query_selected_products!='') 
            {
                $sub_query="LEFT JOIN gl_campaign_whitelabel_details 
                            AS wd
                            ON w.id = wd.campaign_whitelabel_id
                            ";
                if($query_selected_merchants!='') 
                {
                    if($query!='')    
                        $query .=" AND ";    
                    $query .=$query_selected_merchants;
                }
                if($query_selected_products!='') 
                {
                    if($query!='')    
                        $query .=" AND ";    
                    $query .=$query_selected_products;
                }
            }
            
            $join_rule_details = "
                LEFT JOIN gl_campaign_whitelabel_data 
                AS w 
                ON w.user_id = gl_user.user_id
                ".$sub_query;
            $where_rule_details= $query;
        }   
        else
        {
             $join_rule_details ='';   
             $where_rule_details ='';
        }
        
        
        
        //echo '<pre>';
        //print_r($join_rule_details);
        //exit;
        
        
        
        
        
        $today = date('Y-m-d');
        
        if ($max_age == 0)
            $max_age = 100;
        $age_query = ' AND TIMESTAMPDIFF(YEAR,user_dob,CURDATE()) BETWEEN ' . $min_age . ' AND ' . $max_age;


        if ($gender != "Both")
            $gender_query = " AND user_gender = '$gender'";
        else
            $gender_query = "";
        //region

        if ($region_list != "")
            $region_query = " AND user_city_id IN($region_list)";
        else
            $region_query = "";
        
        if ($logout_days > 0) {
            $logout_days = " AND DATEDIFF('".$today."', user_last_login_date) >='".$logout_days."'";
        }
        else {
            $logout_days ="";
        }
        
        if ($member_fb_notconnected == 'yes') {
            $member_fb_notconnected = " AND (ISNULL(user_fb_id) OR user_fb_id = '')";
        }
        else {
            $member_fb_notconnected ="";
        }
        
        if ($member_noactivity_days > 0) {
            $member_noactivity_days = " AND DATEDIFF('".$today."', ga.log_timestamp) >='".$member_noactivity_days."'";
            $join_member_noactivity_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id,  ga.log_id
                FROM (SELECT log_timestamp, log_user_id, log_id 
                    FROM gl_activity WHERE log_title not in('Voucher reversed', 'Voucher expired') ORDER BY log_id DESC) AS ga
                    GROUP BY ga.log_user_id
                ) as ga ON ga.log_user_id = gl_user.user_id";
        }
        else {
            $member_noactivity_days ="";
            $join_member_noactivity_days = "";
        }
        
        if ($member_prev_rewarded_days > 0) {
            $member_prev_rewarded_days = " AND DATEDIFF('".$today."', ga2.log_timestamp) BETWEEN 0 AND ".$member_prev_rewarded_days;
            $join_member_prev_rewarded_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id,  ga.log_id
                FROM (SELECT log_timestamp, log_user_id, log_id 
                    FROM gl_activity WHERE log_title = 'You\'ve been rewarded!' ORDER BY log_id DESC) AS ga
                    GROUP BY ga.log_user_id
                ) as ga2 ON ga2.log_user_id = gl_user.user_id";
        }
        else {
            $member_prev_rewarded_days ="";
            $join_member_prev_rewarded_days = "";
        }
        
        if ($member_noglomp_days  > 0) {
            $member_noglomp_days  = " AND DATEDIFF('".$today."', ga3.log_timestamp) >= '".$member_noglomp_days ."'";
            $join_member_noglomp_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id, ga.log_id 
                FROM (SELECT log_timestamp, log_user_id, log_id
                    FROM gl_activity 
                    WHERE log_title LIKE '%glomp!ed <a href%' ORDER BY log_id DESC) AS ga 
                    GROUP BY ga.log_user_id 
                ) AS ga3 ON ga3.log_user_id = gl_user.user_id";
        }
        else{
            $member_noglomp_days ="";
            $join_member_noglomp_days = "";
        }
        
        
        if ($member_less_glomped  > 0) {
            $member_less_glomped  = " AND (
                ga4.number_of_glomps <= ".$member_less_glomped."
                AND DATEDIFF('".$today."', ga4.log_timestamp) ='".$member_glomped_days ."')";
            $join_member_less_glomped = "
                LEFT JOIN (SELECT COUNT(ga.log_timestamp) AS number_of_glomps, ga.log_timestamp, ga.log_user_id, ga.log_id 
                FROM (SELECT log_timestamp, log_user_id, log_id
                    FROM gl_activity WHERE log_title LIKE '%glomp!ed <a href%' ORDER BY log_id DESC) AS ga GROUP BY ga.log_user_id ) AS ga4 ON ga4.log_user_id = gl_user.user_id";
        }
        else{
            $member_less_glomped ="";
            $join_member_less_glomped = "";
        }
        
        if ($member_less_nofriends  > 0) {
            $member_less_nofriends  = " AND gf.no_of_friends <= ".$member_less_nofriends;
            $join_member_less_nofriends = "
               LEFT JOIN (SELECT COUNT(gl_friends.friend_id) no_of_friends, gl_friends.friend_user_id
                FROM gl_friends group by gl_friends.friend_user_id) as gf ON gf.friend_user_id = gl_user.user_id";
        }
        else{
            $member_less_nofriends ="";
            $join_member_less_nofriends = "";
        }
        

        $sql = "SELECT gl_user.user_id,concat(user_fname,' ',user_lname)as user_name,user_profile_pic
            FROM gl_user
            $join_member_noactivity_days
            $join_member_prev_rewarded_days
            $join_member_noglomp_days
            $join_member_less_glomped
            $join_member_less_nofriends
            $join_rule_details
            WHERE gl_user.user_id!=1 AND user_status!='Deleted' 
                $age_query 
                $gender_query 
                $region_query 
                $logout_days 
                $member_fb_notconnected 
                $member_noactivity_days
                $member_prev_rewarded_days
                $member_noglomp_days
                $member_less_glomped
                $member_less_nofriends  
                $where_rule_details
            GROUP BY gl_user.user_id
            $sub_query_where
        ";
        
//        echo $sql;
//        exit;
        //
        $reuslt = $this->db->query($sql);
        //echo '<pre>';
        //print_r($this->db->last_query());
        //print_r($reuslt);
//        exit;
        
//        foreach( $reuslt->result() as $row){print_r($row);}exit;
         
        return $reuslt;
    }
	function generate_single_voucher_by_promo($user_id, $camp_id,$by_type)
	{
		$time = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		if($by_type=='by_promo_follow_up')
		{
			$sql = "SELECT camp_details_id, camp_campaign_id,campaign_type,campaign_by_promo_follow_up_campagin_type,camp_prod_id,camp_voucher_qty FROM gl_campaign,gl_campaign_details,gl_campaign_details_by_promo
				WHERE 
				gl_campaign.campaign_id = camp_campaign_id
				AND camp_campaign_id = '$camp_id' 
				AND gl_campaign_details.campaign_by_type= '$by_type'
				AND gl_campaign_details_by_promo.campaign_id= '$camp_id'
				AND camp_status = 'Active'";
		}
		else{
			$sql = "SELECT camp_details_id, camp_campaign_id,campaign_type,camp_prod_id,camp_voucher_qty FROM gl_campaign,gl_campaign_details WHERE 
				campaign_id = camp_campaign_id
				AND camp_campaign_id = '$camp_id' 
				AND gl_campaign_details.campaign_by_type= '$by_type'			
				AND camp_status = 'Active'";
		}
		$result = $this->db->query($sql);
        $num_rows = $result->num_rows();
		$voucher_id="";
		$uuid="";
        if ($num_rows > 0)
		{
			$this->db->query('BEGIN');
            $time = date('Y-m-d H:i:s');
			foreach ($result->result() as $row) {
				//check if it can still provide a voucher
				$res_remaining = $this->voucherByCampAndProduct($camp_id, $row->camp_prod_id,'',$by_type);
				$remaining_num = $res_remaining->num_rows();				
				$prod_voucher = $row->camp_voucher_qty;
				$rem = ($prod_voucher - $remaining_num);
				
					
				if($rem>0){
					$camp_id = $row->camp_campaign_id;
					if($by_type=='by_promo_follow_up')
					{
						$type = $row->campaign_by_promo_follow_up_campagin_type;
					}
					else
					{
						$type = $row->campaign_type;
					}
						
					$qty =$row->camp_voucher_qty;
					$prod_id = $row->camp_prod_id;
					$voucher_id=$this->mysql_uuid();
					
					$sql = "INSERT INTO gl_campaign_voucher (camp_voucher_id,camp_campaign_id,camp_prod_id,
						camp_voucher_type,generated_date,camp_winner_user_id,camp_voucher_assign_status,camp_voucher_assigned_date,camp_voucher_by_type)
						VALUES('$voucher_id','$camp_id','$prod_id','$type','$time','$user_id','Assigned','$time','$by_type')";
					$this->db->query($sql);
					
					
					$sql = "SELECT ADDDATE('$date',INTERVAL campaign_voucher_expiry_day DAY)AS expiry_date
						FROM gl_campaign 
						WHERE campaign_id = '$camp_id'";

					$res = $this->db->query($sql);
					$rec = $res->row();
					$expiry_date = $rec->expiry_date;
					///insert
					//insert					                     
                    $voucher_type = $type;

                    //prodcut info
                    $res_info_prod = $this->product_m->productInfo($prod_id);
                    $rec_info_prod = json_decode($res_info_prod);



                    $merchant_id = $rec_info_prod->product->$prod_id->merchant_id;
                    $prod_name = $rec_info_prod->product->$prod_id->prod_name;
                    $merchant_name = $rec_info_prod->product->$prod_id->merchant_name;
					
					
					$uuid = $this->mysql_uuid();
					$voucher_status = ($voucher_type == 'Consumable') ? 'Consumable' : 'Assigned';
					$insert_array = array('voucher_id' => $uuid,
						'voucher_purchaser_user_id' => 1,
						'voucher_belongs_usser_id' => $user_id,
						'voucher_merchant_id' => $merchant_id,
						'voucher_product_id' => $prod_id,
						'voucher_purchased_date' => $time,
						'voucher_expiry_date' => $expiry_date,
						'voucher_status' => $voucher_status,
						'voucher_type' => $voucher_type,
						'voucher_orginated_by' => $camp_id,
						'voucher_sender_glomp_message' => 'Congrats! You won this voucher'
					);
					$this->db->insert('gl_voucher', $insert_array);

					//log activity [received_a_reward]                    
                        $details=array(
                                'voucher_id'=>$uuid,
                                'reward_type'=>'promo_code',                                
                                'product_id'=>$prod_id,
                                'product_name'=>$prod_name,
                                'merchant_id'=>$merchant_id,
                                'merchant_name'=>$merchant_name                        
                                );
                        $params = array(
                                'user_id'=>$user_id,
                                'type' =>'received_a_reward',
                                'details' =>json_encode($details)
                                );
                        add_activity_log($params);
                        //log activity [received_a_reward]
                        
                        
					//update number of recevied glomp and last glomp date

					$sql = "UPDATE gl_activity_summary SET
			summary_last_glomp_date = '$time',
			summary_total_received_glomp = (summary_total_received_glomp+1)
			WHERE summary_user_id = '$user_id'
			";
					$this->db->query($sql);


					$res_winner = $this->user_account_m->user_short_info($user_id);
					$rec_winner = json_decode($res_winner);
					$user_url = anchor('profile/view/' . $rec_winner->user_id, $rec_winner->user_name, 'target="_blank"');
					$log_title = "You've been rewarded!";
					$log_details = "You've been glomp!ed a $merchant_name $prod_name by glomp! ";

					$arr = array('log_user_id' => $user_id,
						'log_title' => $log_title,
						'log_details' => $log_details,
						'log_timestamp' => date('Y-m-d H:i:s'),
						'log_ip' => $_SERVER['REMOTE_ADDR']
					);
					$this->db->insert('gl_activity', $arr);					
					//$this->sent_reward_email($rec_winner, $prod_name, $merchant_name);	
					//insert					
					break;
				}       
            }
            $this->db->query('COMMIT');			
		
		}
		return $uuid;
	}
    function generate_voucher_by_camp_id($camp_id) {
        $sql = "SELECT camp_details_id, camp_campaign_id,campaign_type,camp_prod_id,camp_voucher_qty FROM gl_campaign,gl_campaign_details WHERE 
			campaign_id = camp_campaign_id
			AND camp_campaign_id = '$camp_id' 
			AND camp_status = 'Active'";
        $result = $this->db->query($sql);
        $num_rows = $result->num_rows();
        if ($num_rows > 0) {
            $this->db->query('BEGIN');
            $time = date('Y-m-d H:i:s');
            //$invited_users = $this->db->query('SELECT user_id FROM gl_campaign_invite_users WHERE camp_campaign_id ='.$camp_id);
            
            foreach ($result->result() as $row) {
                $camp_id = $row->camp_campaign_id;
                $type = $row->campaign_type;
                $qty = $row->camp_voucher_qty;
                $prod_id = $row->camp_prod_id;
                for ($i = 0; $i < $qty; $i++) {
                    if ($type == 'Both') {
                        $sql1 = "INSERT INTO gl_campaign_voucher (camp_voucher_id,camp_campaign_id,camp_prod_id,
								camp_voucher_type,generated_date)
								VALUES(uuid(),'$camp_id','$prod_id','Assignable','$time')";
                        $this->db->query($sql1);
                        $sql2 = "INSERT INTO gl_campaign_voucher (camp_voucher_id,camp_campaign_id,camp_prod_id,
								camp_voucher_type,generated_date)
							VALUES(uuid(),'$camp_id','$prod_id','Consumable','$time')";
                        $this->db->query($sql2);
                    } else {
                        $sql = "INSERT INTO gl_campaign_voucher (camp_voucher_id,camp_campaign_id,camp_prod_id,
							camp_voucher_type,generated_date)
							VALUES(uuid(),'$camp_id','$prod_id','$type','$time')";
                        $this->db->query($sql);
                    }
                }
            }
            $this->db->query('COMMIT');
            //$this->db->query('ROLLBACK');
            
             //memcached clear
            $params = array(                            
                'affected_tables' 
                    => array(
                        'gl_voucher'
                    ) #cache name            
            );
            delete_cache($params); 
        }
    }

    function campaign_voucher($camp_id, $sub_query = '') {
        $sql = "SELECT * FROM gl_campaign_voucher 
			WHERE camp_campaign_id = '$camp_id' 
                        AND camp_voucher_assign_status = 'Assigned'
			$sub_query
			";
        $result = $this->db->query($sql);
        return $result;
    }
    /*TODO: CLEAN THIS ONE quick fix
     * 
     */
    function campaign_voucher_by_camp_id($camp_id, $sub_query= '') {
        $sql = "SELECT * FROM gl_campaign_voucher 
			WHERE camp_campaign_id = '$camp_id'
			$sub_query
			";
        $result = $this->db->query($sql);
        return $result;
    }

    function run_campaign($camp_id, $rule_disabled = FALSE) {
    
        //memcached clear
        $params = array(                            
            'affected_tables' 
                => array(
                    'gl_voucher',
                    'gl_user'
                ) #cache name        
        );
        delete_cache($params);                
        //memcached clear
    
        $time = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        $w = 0;
        #Step: reterive cmapagin information
        #step: update campaign table with last exection date
        #step: select the number of vouchers available for the selected campaign.
        #step: select random people applying the campagin rule. Max number of people will be selected less or equal to available voucher
        #Assign the inidividual voucher to selected people and update into gl_campaign_voucher table  user id as voucher winner_id and insert into the gl_voucher table
        # Insert into activity log table
        # update activity summary table
        #####################################################################################
        #step:
        $sql = "SELECT ADDDATE('$date',INTERVAL 7 DAY)AS expiry_date,
            campaign_type, campaign_voucher_expiry_day
			FROM gl_campaign 
			WHERE campaign_id = '$camp_id'";

        $res = $this->db->query($sql);
        $rec = $res->row();
        $expiry_date = $rec->expiry_date;
        $expiry_day = $rec->campaign_voucher_expiry_day;
        $campaign_type = $rec->campaign_type;

        #step:
        $sql_update = "UPDATE gl_campaign SET campaign_last_executed_date = '$time' WHERE campaign_id = '$camp_id'";
        $this->db->query($sql_update);
        $res_available_voucher = $this->campaign_voucher_by_camp_id($camp_id, " AND camp_voucher_assign_status = 'UnAssigned'");
        $available_voucher = $res_available_voucher->num_rows();
        #step:
        if ($campaign_type == 'Both') {
            $available_voucher = ceil($available_voucher / 2);
            $voucher_type_array = array(0 => 'Consumable', 1 => 'Assignable');
        } else {
            $voucher_type_array = array(0 => $campaign_type);
        }
        #Note: voucher type array will denote how many time loop should be iterate. ie. if both type, 2 types of voucher should be distributed to the winner and it available voucher will denone how many people select randomly form the database
        #step: random user id is now stored in array
        $res_people = $this->select_people_by_rule($camp_id, " ORDER BY rand() LIMIT $available_voucher");
        $num_rows = $res_people->num_rows();
        $random_people = array();
        
        $invited_users = $this->db->query('SELECT user_id, sender_name, sender_email FROM gl_campaign_invite_users WHERE camp_campaign_id ='.$camp_id);
        
        $x = 0;
        //Priority to add into random user
        if ($invited_users->num_rows() > 0) {
            foreach($invited_users->result() as $row) {
                $random_people[$x]['user_id'] = $row->user_id;
                $random_people[$x]['sender_name'] = (empty($row->sender_name)) ? SITE_DEFAULT_SENDER_NAME : $row->sender_name;
                $random_people[$x]['sender_email'] = (empty($row->sender_email)) ? SITE_DEFAULT_SENDER_EMAIL : $row->sender_email;
                $random_people[$x]['invited'] = TRUE;
                $x++;
            }
        }
        
        
        if ($num_rows > 0 && $rule_disabled == FALSE) {
            foreach ($res_people->result() as $rec_people) {
                $invited_user = $this->db->query('SELECT user_id FROM gl_campaign_invite_users 
                    WHERE camp_campaign_id = "'.$camp_id.'" AND user_id ='.$rec_people->user_id);
                if ($invited_user->num_rows() > 0) {
                    continue;
                }
                $random_people[$x]['user_id'] = $rec_people->user_id;
                $random_people[$x]['sender_name'] = SITE_DEFAULT_SENDER_NAME;
                $random_people[$x]['sender_email'] = SITE_DEFAULT_SENDER_EMAIL;
                $random_people[$x]['invited'] = FALSE;
                $x++;
            }
        }
        
        
        #step:
        foreach ($voucher_type_array as $value_type) {

            $res_available_voucher = $this->campaign_voucher_by_camp_id($camp_id, " AND camp_voucher_assign_status = 'UnAssigned' 
			AND camp_voucher_type = '$value_type'");

            $remaining_voucher = $res_available_voucher->num_rows();


            if ($remaining_voucher > 0) {
                $p = 0;
                foreach ($res_available_voucher->result() as $row_voucher) {

                    $voucher_id = $row_voucher->camp_voucher_id;
                    $winning_user_id = isset($random_people[$p]['user_id']) ? $random_people[$p]['user_id'] : 0;
                    $prod_id = $row_voucher->camp_prod_id;
                    $voucher_type = $row_voucher->camp_voucher_type;

                    //prodcut info
                    $res_info_prod = $this->product_m->productInfo($prod_id);
                    $rec_info_prod = json_decode($res_info_prod);



                    $merchant_id = $rec_info_prod->product->$prod_id->merchant_id;
                    $prod_name = $rec_info_prod->product->$prod_id->prod_name;
                    $merchant_name = $rec_info_prod->product->$prod_id->merchant_name;
                    if ($winning_user_id == 0) {
                        break;
                    }
                    //now we have voucher id and winner id, update gl_campaign_voucher and insert into gl_voucher_table
                    if ($winning_user_id > 0) {
                        $this->db->query('BEGIN');

                        $sql = "UPDATE gl_campaign_voucher SET 
				camp_winner_user_id = '$winning_user_id',
				camp_voucher_assign_status = 'Assigned',
				camp_voucher_assigned_date = '$time'
				WHERE camp_voucher_id = '$voucher_id'
				";
                        $this->db->query($sql);
                        $affected_rows = $this->db->affected_rows();
                        
                        if (! $random_people[$p]['invited']) {
                            $purch_id = 1; //reward by "glomp"
                        } else {
                            //Purchaser ID must be exists on the database
                            $purch_id = $this->_set_voucher_purchaser($random_people[$p]['sender_email']);
                        }
                        
//                        echo '<pre>';
//                        print_r($random_people);
//                        exit;
                        
                        //insert
                        $uuid = $this->mysql_uuid();
                        $voucher_status = ($voucher_type == 'Consumable') ? 'Consumable' : 'Assigned';
                        $insert_array = array('voucher_id' => $uuid,
                            'voucher_purchaser_user_id' => $purch_id,
                            'voucher_belongs_usser_id' => $winning_user_id,
                            'voucher_merchant_id' => $merchant_id,
                            'voucher_product_id' => $prod_id,
                            'voucher_purchased_date' => $time,
                            //'voucher_expiry_date' => $expiry_date,
                            'voucher_status' => $voucher_status,
                            //'voucher_invitation_status' => 'pending',
                            //'voucher_expiry_day' => $expiry_day,
                            'voucher_expiry_day' => $expiry_day,
                            'voucher_type' => 'Pending',
                            'voucher_expiry_date' => $expiry_date,
                            'voucher_orginated_by' => $camp_id,
                            'voucher_sender_glomp_message' => 'Congrats! You won this voucher'
                        );
                        $this->db->insert('gl_voucher', $insert_array);

                        //
                        
                         //log activity [received_a_reward]                    
                        $details=array(
                                'voucher_id'=>$uuid,
                                'reward_type'=>'campaign',                                
                                'product_id'=>$prod_id,
                                'product_name'=>$prod_name,
                                'merchant_id'=>$merchant_id,
                                'merchant_name'=>$merchant_name                        
                                );
                        $params = array(
                                'user_id'=>$winning_user_id,
                                'type' =>'received_a_reward',
                                'details' =>json_encode($details)
                                );
                        add_activity_log($params);
                        //log activity [received_a_reward]
                        //update number of recevied glomp and last glomp date

                        $sql = "UPDATE gl_activity_summary SET
				summary_last_glomp_date = '$time',
				summary_total_received_glomp = (summary_total_received_glomp+1)
				WHERE summary_user_id = '$winning_user_id'
				";
                        $this->db->query($sql);


                        $res_winner = $this->user_account_m->user_short_info2($winning_user_id);
                        $rec_winner = json_decode($res_winner);
                        $user_url = anchor('profile/view/' . $rec_winner->user_id, $rec_winner->user_name, 'target="_blank"');
                        $log_title = "You've been rewarded!";
                        $log_details = "You've been glomp!ed a $merchant_name $prod_name by glomp! ";

                        $arr = array('log_user_id' => $winning_user_id,
                            'log_title' => $log_title,
                            'log_details' => $log_details,
                            'log_timestamp' => date('Y-m-d H:i:s'),
                            'log_ip' => $_SERVER['REMOTE_ADDR']
                        );
                        $this->db->insert('gl_activity', $arr);
                        
                        
                        $this->db->query('COMMIT');
                        $this->sent_reward_email($rec_winner, $prod_name, $merchant_name, $random_people[$p]['sender_name'], $random_people[$p]['sender_email'], $random_people[$p]['invited'], $voucher_status, $campaign_type, $uuid);
                        
                         //memcached clear
                        $params = array(                            
                            'affected_tables' 
                                => array(
                                    'gl_voucher'
                                ), #cache name
                            'specific_names' 
                                => array(
                                    'friends_info_buzz_'.$winning_user_id,
                                    'my_glomp_details_'.$winning_user_id
                                )    
                        );
                        delete_cache($params);                
                        //memcached clear
                        
                        
                        $p++;
                        $w++;
                    }//if winning user id
                }//for each close of voucher
            }//if remaining voucher close
        }//voucher type array close
        return $w;
    }

//end of function
    function _set_voucher_purchaser($sender_email) {
        $purchaser = $this->user_account_m->user_shortinfo_by_email($sender_email);
        $purchaser = json_decode($purchaser);
        
        $purchaser_id = (isset($purchaser->user_id)) ? $purchaser->user_id : 1;
        return $purchaser_id;
    }
	function get_this_promo_code($code){
		$sql = "SELECT * FROM " . $this->INSTANCE_DETAILS_PROMO." WHERE campaign_by_promo_code='$code'";
        $res = $this->db->query($sql);
        return $res;
	}
	function get_this_promo_code_update($code,$camp_id){
		$sql = "SELECT * FROM " . $this->INSTANCE_DETAILS_PROMO." WHERE campaign_by_promo_code='$code' AND campaign_id<>'$camp_id'";
        $res = $this->db->query($sql);
        return $res;
	}
    function voucherByCampAndProduct($camp_id, $product_id, $voucher_type = '',$by_type='by_rule') {
        $v_type = '';
        if ($voucher_type == "")
            $v_type = "camp_voucher_type = '$voucher_type'";
        $sql = "SELECT * FROM gl_campaign_voucher
			WHERE camp_campaign_id = '$camp_id'
			AND camp_voucher_assign_status = 'Assigned'
			AND camp_prod_id = '$product_id'
			AND camp_voucher_by_type = '$by_type'			
			";
        $res = $this->db->query($sql);
        return $res;
    }

    function mysql_uuid() {
        $sql = "select UUID() as uuid";
        $res = $this->db->query($sql);
        return $res->row()->uuid;
    }

    function voucher_details($camp_id, $limit = '') {
        $sql = "SELECT p.prod_id, p.prod_name,p.prod_image,cv.camp_voucher_assign_status,cv.camp_voucher_type
			FROM gl_product p, gl_campaign_voucher cv
			WHERE
			p.prod_id = cv.camp_prod_id
			AND cv.camp_campaign_id = '$camp_id' ORDER BY p.prod_id $limit ";
        $res = $this->db->query($sql);
        return $res;
    }

    function voucherDetails($capID) {
        $sql = "SELECT * FROM gl_campaign_voucher,gl_product WHERE prod_id = camp_prod_id AND camp_campaign_id='$capID' 
				ORDER BY camp_voucher_assigned_date DESC";
        $res = $this->db->query($sql);
        return $res;
    }

    function sent_reward_email($rec_friend, 
            $prod_name, 
            $merchant_name, 
            $sender_name = SITE_DEFAULT_SENDER_NAME, 
            $sender_email = SITE_DEFAULT_SENDER_EMAIL, 
            $invited = FALSE, 
            $voucher_status = 'consumable',
            $campaign_type,
            $voucher_id) {
        
        $receiver_name = $rec_friend->user_fname;
        $receiver_email = $rec_friend->user_email;
        $email_subject = "You’ve been rewarded!";
        $check_it_out = anchor('profile/view/' . $rec_friend->user_id, 'Check it out');
        $link = anchor(site_url(), '<u>sign up</u>');
        
        $this->load->library('email_templating');
        $this->load->model('invites_m');
        
        $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher_id))->row();
        $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
        $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();
        
        if ($invited) {
            $link = anchor('landing/preFill/' . $rec_friend->user_hash_key, '<u>sign up</u>');
            
            if ($campaign_type == 'Both') {    
                $vars = array(
                    'template_name' => 'invitation_to_glomp_campaign_by_email',
                    'from_name' => $sender_name,
                    'from_email' => $sender_email,
                    'lang_id' => 1,
                    'to' => $receiver_email,
                    'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                    'params' => array(
                        '[insert recipients first name]' => $receiver_name,
                        '[insert merchant brand and product]' => $merchant_name.' '.$prod_name,
                        '[link]' => $link,
                    ),
                    'others' => array(
                        'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                        'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                    )
                );
                $this->email_templating->config($vars);
                
                //Save to gl_invite table
                $invite_data = array(
                  'invite_by_user_id' => 1, //CHANGE THIS 1 IS FOR ADMIN
                  'invite_through' => 'email',  
                  'invite_voucher_id' => $voucher_id,
                  'invite_invited_id' =>  $rec_friend->user_id,
                  'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                );
                
            } else {
                $vars = array(
                    'template_name' => 'invitation_to_glomp_campaign_by_email_consumable',
                    'from_name' => $sender_name,
                    'from_email' => $sender_email,
                    'lang_id' => 1,
                    'to' => $receiver_email,
                    'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                    'params' => array(
                        '[insert treaters personal message]' => '',
                        '[insert treaters name]' => $sender_name,
                        '[insert merchant brand and product]' => $merchant_name.' '.$prod_name,
                        '[link]' => $link,
                    ),
                    'others' => array(
                        'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                        'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                    )
                );
                $this->email_templating->config($vars);
                $this->email_templating->set_subject($sender_name .' has given you a free treat!');
                
                //Save to gl_invite table
                $invite_data = array(
                  'invite_by_user_id' => 1, //CHANGE THIS; 1 IS FOR ADMIN
                  'invite_through' => 'email',  
                  'invite_voucher_id' => $voucher_id,
                  'invite_invited_id' =>  $rec_friend->user_id,
                  'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_2', //sets voucher message when not claimed
                );
                
            }
        }
        else {
            //Assignable voucher given by glomp to a member
            if ($voucher_status == 'Assigned') {
                
                $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher_id))->row();
                $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
                $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();
                
                $accept_this = anchor(site_url(), 'accept this product');
                $vars = array(
                    'template_name' => 'campaign_member_assignable_reward',
                    'from_name' => $sender_name,
                    'from_email' => $sender_email,
                    'lang_id' => 1,
                    'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                    'to' => $receiver_email,
                    'params' => array(
                        '[insert merchant brand and product]' => $merchant_name.' '.$prod_name,
                        '[link]' => $accept_this
                    ),
                    'others' => array(
                        'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                        'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                    )
                );
                
                $e_template = $this->db->get_where('gl_email_templates', array('template_name' => $vars['template_name']));
                $args['user_email'] = $receiver_email;
                $args['type'] = 'reward';
                $args['params'] = array(
                    'type' => '1',
                    'id' => 0
                );
                $args['message'] = $e_template->row()->subject;

                push_notification($args);
            }
            //Consumable voucher given by glompt to a member
            else {
                $check_it_out = anchor(site_url(), 'Check it out');
                $vars = array(
                    'template_name' => 'campaign_member_consumable_reward',
                    'from_name' => $sender_name,
                    'from_email' => $sender_email,
                    'lang_id' => 1,
                    'to' => $receiver_email,
                    'params' => array(
                        '[link]' => $check_it_out
                    )
                );
                
                
                $e_template = $this->db->get_where('gl_email_templates', array('template_name' => $vars['template_name']));
                $args['user_email'] = $receiver_email;
                $args['type'] = 'reward';
                $args['params'] = array(
                    'type' => '1',
                    'id' => 0
                );
                $args['message'] = $e_template->row()->subject;

                push_notification($args);
            }
            
            $this->email_templating->config($vars);
        }

        $email_log_id = $this->email_templating->send();
        
        if ($invited) {
            $invite_data['invite_email_log_id'] = $email_log_id;
            $this->invites_m->_insert($invite_data);
        }
        
        //$this->send_email_m->sendEmail($this->site_sender_email, $this->site_sender_name, $receiver_email, $receiver_name, $email_subject, $email_message);
    }

    function update_associated_regions($camp_id) {
        $res_rule = $this->select_campaign_rule($camp_id);
        $rec_rule = $res_rule->row();
        $region_list = $rec_rule->region_id;
        if ($region_list == '')
            $region_list = 0;

        $sql = "SELECT  region_id, region_parent_path_id FROM gl_region 
			WHERE region_id IN ($region_list) 
			AND region_status = 'Active'";

        $res = $this->db->query($sql);
        $num = $res->num_rows($res);

        $region_array = array();
        if ($num > 0) {
            foreach ($res->result() as $row) {

                $region_parent_path = $row->region_parent_path_id;
                //
                $sql = "SELECT  region_id FROM gl_region 
			WHERE region_parent_path_id like '$region_parent_path%'
			AND region_status = 'Active'";
                $res_path = $this->db->query($sql);
                $num_rows = $res_path->num_rows();
                if ($num_rows > 0) {
                    foreach ($res_path->result() as $row_path) {
                        $region_array[] = $row_path->region_id;
                    }
                }
            }
        }
        $list_of_region_id = "";
        $region_array = array_unique($region_array);
        if (count($region_array) > 0) {

            foreach ($region_array as $value) {
                $list_of_region_id .= $value . ',';
            }
        }
        $list_of_region_id = rtrim($list_of_region_id, ',');
        $sql = "UPDATE gl_campaign_rule SET 
			associated_region_id = '$list_of_region_id'
			WHERE camp_rule_campaign_id = '$camp_id'
			";
        $this->db->query($sql);
    }

}
//eoc
?>