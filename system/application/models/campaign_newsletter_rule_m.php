<?php

/*
 * Campaign Newsletter Model Class
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Campaign_newsletter_rule_m extends Super_model {
    
    function __construct() {
        parent::__construct('gl_campaign_newsletter_rule cnr');
    }
    function get_ischecked($name = '', $camp_newsletter_id) {
        $rec = $this->get_record(array(
            'select' => $name,
            'where' => array(
                $name => 'yes',
                'campaign_newsletter_id' => $camp_newsletter_id
            )
        ));
        
        if ($rec == FALSE) {
            return FALSE;
        }
       
        return TRUE;
    }
    
    
    function get_isset($name = '', $camp_newsletter_id, $value = 'yes') {
        $rec = $this->get_record(array(
            'select' => $name,
            'where' => array(
                $name => $value,
                'campaign_newsletter_id' => $camp_newsletter_id
            )
        ));
        
        if ($rec == FALSE) {
            return FALSE;
        }
       
        return TRUE;
    }
    
    function update_campaign_rule($campaign_id) {
        $campaign_id = (int) $campaign_id;

        $list = '';
        $rule_query = '';
        if (isset($_POST['regions_id_selected'])) {
            $cnt = count($_POST['regions_id_selected']);

            for ($i = 0; $i < $cnt; $i++) {
                $list .= $_POST['regions_id_selected'][$i] . ',';
            }
            $list = trim($list, ',');
        }
        
        $min_age_checked = isset($_POST['min_age_checked']) ? 'yes' : 'no';
        $max_age_checked = isset($_POST['max_age_checked']) ? 'yes' : 'no';
        $regions_checked = isset($_POST['regions_checked']) ? 'yes' : 'no';
        $loggedover_checked = isset($_POST['loggedover_checked']) ? 'yes' : 'no';
        $noactivity_checked = isset($_POST['noactivity_checked']) ? 'yes' : 'no';
        $previously_rewarded_checked = isset($_POST['previously_rewarded_checked']) ? 'yes' : 'no';
        $notglomp_checked = isset($_POST['notglomp_checked']) ? 'yes' : 'no';
        $glomped_checked = isset($_POST['glomped_checked']) ? 'yes' : 'no';
        $lessfriends_checked = isset($_POST['lessfriends_checked']) ? 'yes' : 'no';
        $member_fb_notconnected = isset($_POST['member_fb_notconnected']) ? 'yes' : 'no';
        
        
        $loggedover_set = ($_POST['loggedover_set'] == 'yes') ? 'yes' : 'no';
        $noactivity_set = ($_POST['noactivity_set'] == 'yes') ? 'yes' : 'no';
        $previously_rewarded_set = ($_POST['previously_rewarded_set'] == 'yes') ? 'yes' : 'no';
        $notglomp_set = ($_POST['notglomp_set'] == 'yes') ? 'yes' : 'no';
        $glomped_set = ($_POST['glomped_set'] == 'yes') ? 'yes' : 'no';
        $lessfriends_set= ($_POST['lessfriends_set']  == 'yes') ? 'yes' : 'no';
        $member_fb_notconnected_set = ($_POST['member_fb_notconnected_set'] == 'yes') ? 'yes' : 'no';
        
        $map_rules = array(
            'selected_list' =>  ( isset($_POST['whitelabel_id_checked']) && isset($_POST['whitelabel_id_selected']) ) ? $_POST['whitelabel_id_selected']:'',
            'selected_actions' => ( isset($_POST['whitelabel_actions_checked']) && isset($_POST['whitelabel_actions_selected'])) ? $_POST['whitelabel_actions_selected']:'',
            'selected_merchants' => ( isset($_POST['whitelabel_merchants_checked']) && isset($_POST['whitelabel_merchants_selected'])) ? $_POST['whitelabel_merchants_selected']:'',
            'selected_products'=> ( isset($_POST['whitelabel_products_checked']) && isset($_POST['whitelabel_products_selected'])) ? $_POST['whitelabel_products_selected']:'',
            'date_from' => ( isset($_POST['date_from_checked']) && isset($_POST['whitelabel_date_from'])) ? $_POST['whitelabel_date_from']:'',
            'date_to' =>( isset($_POST['date_from_checked']) && isset($_POST['whitelabel_date_to'])) ? $_POST['whitelabel_date_to']:'',
        );
        
        
        $data = array('min_age' => $this->input->post('min_age'),
            'max_age' => $this->input->post('max_age'),
            'gender' => $this->input->post('gender'),
            'region_id' => $list,
            'cam_rule_query' => $rule_query,
            'min_age_checked' => $min_age_checked,
            'max_age_checked' => $max_age_checked,
            'regions_checked' => $regions_checked,
            'loggedover_checked' => $loggedover_checked,
            'noactivity_checked' => $noactivity_checked,
            'previously_rewarded_checked' => $previously_rewarded_checked,
            'notglomp_checked' => $notglomp_checked,
            'glomped_checked' => $glomped_checked,
            'lessfriends_checked' => $lessfriends_checked,
            'member_fb_notconnected' => $member_fb_notconnected,
            //Mandatory or not
            'loggedover_set' => $loggedover_set,
            'noactivity_set' => $noactivity_set,
            'previously_rewarded_set' => $previously_rewarded_set,
            'notglomp_set' => $notglomp_set,
            'glomped_set' => $glomped_set,
            'lessfriends_set' => $lessfriends_set,
            'member_fb_notconnected_set' => $member_fb_notconnected_set,
            //end
            'member_less_nofriends' => $this->input->post('member_less_nofriends'),
            'member_less_glomped' => $this->input->post('member_less_glomped'),
            'member_glomped_days' => $this->input->post('member_glomped_days'),
            'member_noglomp_days' => $this->input->post('member_noglomp_days'),
            'member_prev_rewarded_days' => $this->input->post('member_prev_rewarded_days'),
            'member_noactivity_days' => $this->input->post('member_noactivity_days'),
            'member_logout_days' => $this->input->post('member_logout_days'),
            'rule_details' => json_encode(($map_rules))
        );
        $rec = $this->exists(array('campaign_newsletter_id' => $campaign_id));
        
        if ($rec) {
            $this->_update(array('campaign_newsletter_id' => $campaign_id), $data);
        }
        else {
            $data['campaign_newsletter_id'] = $campaign_id;
            $this->_insert($data);
        }
        
        $this->update_associated_regions($campaign_id);
    }
    
    function update_associated_regions($camp_id) {
        $res_rule = $this->select_campaign_rule($camp_id);
        $rec_rule = $res_rule->row();
        $region_list = $rec_rule->region_id;
        if ($region_list == '')
            $region_list = 0;

        $sql = "SELECT  region_id, region_parent_path_id FROM gl_region 
			WHERE region_id IN ($region_list) 
			AND region_status = 'Active'";

        $res = $this->db->query($sql);
        $num = $res->num_rows($res);

        $region_array = array();
        if ($num > 0) {
            foreach ($res->result() as $row) {

                $region_parent_path = $row->region_parent_path_id;
                //
                $sql = "SELECT  region_id FROM gl_region 
			WHERE region_parent_path_id like '$region_parent_path%'
			AND region_status = 'Active'";
                $res_path = $this->db->query($sql);
                $num_rows = $res_path->num_rows();
                if ($num_rows > 0) {
                    foreach ($res_path->result() as $row_path) {
                        $region_array[] = $row_path->region_id;
                    }
                }
            }
        }
        $list_of_region_id = "";
        $region_array = array_unique($region_array);
        if (count($region_array) > 0) {

            foreach ($region_array as $value) {
                $list_of_region_id .= $value . ',';
            }
        }
        
        $list_of_region_id = rtrim($list_of_region_id, ',');
        
        $this->_update(array(
            'campaign_newsletter_id' => $camp_id
        ), array(
            'associated_region_id' => $list_of_region_id
        ));
        
        $res = $this->get_record(array(
            'where' => array('campaign_newsletter_id' => $camp_id)
        ));
        
        //print_r($res);
        //print_r($_POST);
        //exit;
    }
    
    function select_campaign_rule($camp_id) {
        $sql = "SELECT * FROM gl_campaign_newsletter_rule WHERE campaign_newsletter_id = '$camp_id'";
        $res = $this->db->query($sql);
        return $res;
    }
    
    function select_people_by_rule($camp_id, $sub_query_where = '') {
        $res_rule = $this->select_campaign_rule($camp_id);
        $rec_rule = $res_rule->row();
        $min_age = $rec_rule->min_age;
        $max_age = $rec_rule->max_age;
        $gender = $rec_rule->gender;
        $region_list = $rec_rule->associated_region_id;
        $logout_days = $rec_rule->member_logout_days;
        $member_fb_notconnected = $rec_rule->member_fb_notconnected;
        $member_noactivity_days = $rec_rule->member_noactivity_days;
        $member_prev_rewarded_days = $rec_rule->member_prev_rewarded_days;
        $member_noglomp_days = $rec_rule->member_noglomp_days;
        $member_less_glomped = $rec_rule->member_less_glomped;
        $member_glomped_days = $rec_rule->member_glomped_days;
        $member_less_nofriends = $rec_rule->member_less_nofriends;
        //
        $loggedover_set = $rec_rule->loggedover_set;
        $noactivity_set = $rec_rule->noactivity_set;
        $previously_rewarded_set = $rec_rule->previously_rewarded_set;
        $notglomp_set = $rec_rule->notglomp_set;
        $glomped_set = $rec_rule->glomped_set;
        $lessfriends_set = $rec_rule->lessfriends_set;
        $member_fb_notconnected_set = $rec_rule->member_fb_notconnected_set;
        
        $rule_details = json_decode($rec_rule->rule_details);
        
        if(isset($rule_details->selected_list) && $rule_details->selected_list!='')
        {
            $temp = '';
            foreach ($rule_details->selected_list as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= " w.campaign_label='".$this->campaign_m->white_label_list[$row]['alias']."'";
            }
            $query_selected_list=  "( ".$temp." )";
        }
            
        else
            $query_selected_list = '';
        
        
        if(isset($rule_details->selected_actions) && $rule_details->selected_actions!='')
        {
            $temp = '';
            foreach ($rule_details->selected_actions as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= " w.action='".$row."'";
            }
            $query_selected_actions=  "( ".$temp." )";
        }
            
        else
            $query_selected_actions = '';
        
        
        if(isset($rule_details->selected_merchants) && $rule_details->selected_merchants!='')
        {
            $temp = '';
            foreach ($rule_details->selected_merchants as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= "  ( wd.name='merchant_id' AND wd.value='".$row."') ";
            }
            $query_selected_merchants=  "( ".$temp." )";
        }
            
        else
            $query_selected_merchants = '';
        
        if(isset($rule_details->selected_products) && $rule_details->selected_products!='')
        {
            $temp = '';
            foreach ($rule_details->selected_products as $row){
                if($temp!='')    
                    $temp .=" OR ";
                $temp .= "  ( wd.name='prod_id' AND wd.value='".$row."') ";
            }
            $query_selected_products=  "( ".$temp." )";
        }
            
        else
            $query_selected_products = '';
        
        if( isset($rule_details->date_from) && $rule_details->date_from!='' && $rule_details->date_from!='0'
        && isset($rule_details->date_to) && $rule_details->date_to!='' && $rule_details->date_to!='0')
        {
            $query_date=  "( w.date_created BETWEEN '".$rule_details->date_from." 00:00:00'  AND '".$rule_details->date_to." 23:59:59')";
        }
            
        else
            $query_date = '';
        
        
        if  ( 
                $query_selected_list!='' OR
                $query_selected_actions!='' OR
                $query_selected_merchants!='' OR
                $query_selected_products!='' OR
                $query_date!='' 
            )
        {
            $query ="AND ";
            if($query_selected_list!='') 
                $query .=$query_selected_list;
            if($query_selected_actions!='') 
            {
                if($query!='')    
                    $query .=" AND ";    
                $query .=$query_selected_actions;
            }
            if($query_date!='') 
            {
                if($query!='')    
                    $query .=" AND ";    
                $query .=$query_date;
            }
            $sub_query ="";
            if($query_selected_merchants!='' OR $query_selected_products!='') 
            {
                $sub_query="LEFT JOIN gl_campaign_whitelabel_details 
                            AS wd
                            ON w.id = wd.campaign_whitelabel_id
                            ";
                if($query_selected_merchants!='') 
                {
                    if($query!='')    
                        $query .=" AND ";    
                    $query .=$query_selected_merchants;
                }
                if($query_selected_products!='') 
                {
                    if($query!='')    
                        $query .=" AND ";    
                    $query .=$query_selected_products;
                }
            }
            
            $join_rule_details = "
                LEFT JOIN gl_campaign_whitelabel_data 
                AS w 
                ON w.user_id = gl_user.user_id
                ".$sub_query;
            $where_rule_details= $query;
        }   
        else
        {
             $join_rule_details ='';   
             $where_rule_details ='';
        }
        
        
        $today = date('Y-m-d');
        
        if ($max_age == 0)
            $max_age = 100;
        $age_query = ' AND YEAR(CURDATE())-YEAR(user_dob) BETWEEN ' . $min_age . ' AND ' . $max_age;


        if ($gender != "Both")
            $gender_query = " AND user_gender = '$gender'";
        else
            $gender_query = "";
        //region

        if ($region_list != "")
            $region_query = " AND user_city_id IN($region_list)";
        else
            $region_query = "";
        
        if ($logout_days > 0) {
            $logout_days = "DATEDIFF('".$today."', user_last_login_date) >='".$logout_days."'";
        }
        else {
            $logout_days ="";
        }
        
        if ($member_fb_notconnected == 'yes') {
            $member_fb_notconnected = "user_fb_id = ''";
        }
        else {
            $member_fb_notconnected ="";
        }
        
        if ($member_noactivity_days > 0) {
            $member_noactivity_days = "DATEDIFF('".$today."', ga.log_timestamp) >='".$member_noactivity_days."'";
            $join_member_noactivity_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id,  ga.log_id
                FROM (SELECT log_timestamp, log_user_id, log_id 
                    FROM gl_activity WHERE log_title not in('Voucher reversed', 'Voucher expired') ORDER BY log_id DESC) AS ga
                    GROUP BY ga.log_user_id
                ) as ga ON ga.log_user_id = gl_user.user_id";
        }
        else {
            $member_noactivity_days ="";
            $join_member_noactivity_days = "";
        }
        
        if ($member_prev_rewarded_days > 0) {
            $member_prev_rewarded_days = "DATEDIFF('".$today."', ga2.log_timestamp) BETWEEN 0 AND ".$member_prev_rewarded_days;
            $join_member_prev_rewarded_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id,  ga.log_id
                FROM (SELECT log_timestamp, log_user_id, log_id 
                    FROM gl_activity WHERE log_title = 'You\'ve been rewarded!' ORDER BY log_id DESC) AS ga
                    GROUP BY ga.log_user_id
                ) as ga2 ON ga2.log_user_id = gl_user.user_id";
        }
        else {
            $member_prev_rewarded_days ="";
            $join_member_prev_rewarded_days = "";
        }
        
        if ($member_noglomp_days  > 0) {
            $member_noglomp_days  = "DATEDIFF('".$today."', ga3.log_timestamp) >= '".$member_noglomp_days ."'";
            $join_member_noglomp_days = "
                LEFT JOIN (SELECT ga.log_timestamp, ga.log_user_id, ga.log_id 
                FROM (SELECT log_timestamp, log_user_id, log_id
                    FROM gl_activity 
                    WHERE log_title LIKE '%glomp!ed <a href%' ORDER BY log_id DESC) AS ga 
                    GROUP BY ga.log_user_id 
                ) AS ga3 ON ga3.log_user_id = gl_user.user_id";
        }
        else{
            $member_noglomp_days ="";
            $join_member_noglomp_days = "";
        }
        
        
        if ($member_less_glomped  > 0) {
            $member_less_glomped  = "(
                ga4.number_of_glomps <= ".$member_less_glomped."
                AND DATEDIFF('".$today."', ga4.log_timestamp) ='".$member_glomped_days ."')";
            $join_member_less_glomped = "
                LEFT JOIN (SELECT COUNT(ga.log_timestamp) AS number_of_glomps, ga.log_timestamp, ga.log_user_id, ga.log_id 
                FROM (SELECT log_timestamp, log_user_id, log_id
                    FROM gl_activity WHERE log_title LIKE '%glomp!ed <a href%' ORDER BY log_id DESC) AS ga GROUP BY ga.log_user_id ) AS ga4 ON ga4.log_user_id = gl_user.user_id";
        }
        else{
            $member_less_glomped ="";
            $join_member_less_glomped = "";
        }
        
        if ($member_less_nofriends  > 0) {
            $member_less_nofriends  = "gf.no_of_friends <= ".$member_less_nofriends;
            $join_member_less_nofriends = "
               LEFT JOIN (SELECT COUNT(gl_friends.friend_id) no_of_friends, gl_friends.friend_user_id
                FROM gl_friends group by gl_friends.friend_user_id) as gf ON gf.friend_user_id = gl_user.user_id";
        }
        else{
            $member_less_nofriends ="";
            $join_member_less_nofriends = "";
        }
        
        //Rules holder
        $rule_sets = array(
            'logout_days' => array($loggedover_set, $logout_days),
            'member_fb_notconnected' => array($member_fb_notconnected_set,$member_fb_notconnected),
            'member_noactivity_days' => array($noactivity_set,$member_noactivity_days),
            'member_prev_rewarded_days' => array($previously_rewarded_set,$member_prev_rewarded_days),
            'member_noglomp_days' => array($notglomp_set,$member_noglomp_days),
            'member_less_glomped' => array($glomped_set,$member_less_glomped),
            'member_less_nofriends' => array($lessfriends_set,$member_less_nofriends),
        );
        
        //Query holder
        $mandatory_groups_queries = '';
        $optional_groups_queries = '';
        
        //Array that holds queries that are not empty
        $optional_rules = array();
        $mandatory_rules = array();
        
        $i = 1;
        //Loop through rules
        foreach ($rule_sets as $mr) {
            if ($mr[0] == 'yes') {
                if ($i == 1) {
                    $mandatory_groups_queries .= $mr[1];
                } else {
                    $mandatory_groups_queries .= (! empty($mr[1])) ? ' AND '.$mr[1] : '';
                }
                //Add rules to array that are not empty
                if ($mr[1] != '') $mandatory_rules[] = $mr[1];
            } else {
                if ($i == 1) {
                    $optional_groups_queries .= $mr[1];
                } else {
                    $optional_groups_queries .= (! empty($mr[1])) ? ' OR '.$mr[1] : '';
                }
                //Add rules to array that are not empty
                if ($mr[1] != '') $optional_rules[] = $mr[1];
            }
            $i++;
        }
        //Group themeselves by optionals and mandatories: Default
        $final_groups_query = ' AND ('.$mandatory_groups_queries.') AND ('.$optional_groups_queries.')';
        
        //if all optionals
        if (count($mandatory_rules) == 0 && count($optional_rules) > 0) {
            $final_groups_query = ' AND ('.$optional_groups_queries.')';
            if (count($optional_rules) == 1) {
                $final_groups_query = ' AND ('.$optional_rules[0].')';
            }
        }
        //If all mandatories
        if (count($mandatory_rules) > 0 && count($optional_rules) == 0) {
            $final_groups_query = ' AND ('.$mandatory_groups_queries.')';
            if (count($mandatory_rules) == 1) {
                $final_groups_query = ' AND ('.$mandatory_rules[0].')';
            }
        }
        
        //If mandatory and optional rules are only one
        if (count($mandatory_rules) == 1 && count($optional_rules) == 1) {
            $final_groups_query= ' AND ('.$mandatory_rules[0].') AND ('.$optional_rules[0].')';
        }
        //If mandatory record has only one and optional greater that one record
        if (count($mandatory_rules) == 1 && count($optional_rules) > 1) {
            $final_groups_query= ' AND ('.$mandatory_rules[0].') AND ('.$optional_groups_queries.')';
        }
        //If mandatory record has only greater than one and optional has one
        if (count($mandatory_rules) > 1 && count($optional_rules) == 1) {
            $final_groups_query= ' AND ('.$mandatory_groups_queries.') AND ('.$optional_rules[0].')';
        }
        
        if (count($mandatory_rules) == 0 && count($optional_rules) == 0)
        {
            $final_groups_query= '';
        }
        //else default;
        
//        echo '<pre>';
//        echo print_r($mandatory_rules). ' - ';
//        echo print_r($optional_rules);
        

        $sql = "SELECT gl_user.user_id,concat(user_fname,' ',user_lname)as user_name,user_profile_pic, user_email
            FROM gl_user
            $join_member_noactivity_days
            $join_member_prev_rewarded_days
            $join_member_noglomp_days
            $join_member_less_glomped
            $join_member_less_nofriends
            $join_rule_details
            WHERE gl_user.user_id!=1 AND user_status!='Deleted' 
                $age_query 
                $gender_query 
                $region_query 
                $final_groups_query
                $where_rule_details
            GROUP BY gl_user.user_id
            $sub_query_where
        ";
        
//        echo $sql;
//        exit;
        //
        $reuslt = $this->db->query($sql);
//        echo '<pre>';
//        print_r($this->db->last_query());
//        exit;
        //echo $reuslt->num_rows();
        
        return $reuslt;
    }
    
}