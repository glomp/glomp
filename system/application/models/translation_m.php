<?php 
require_once('super_model.php');
class Translation_m extends Super_model {

	private $INSTANCE="gl_trans";
	private $INSTANCE_Details="gl_trans_detail";
	private $INSTANCE_Language="gl_language";
	
        function __construct() {
            parent::__construct('gl_trans trans');
        }
        
	function checkIfTermNameAvailable($name) {    
		$sql="SELECT * from ".$this->INSTANCE." WHERE trans_label_name='".$name."' LIMIT 1";
		$q=$this->db->query($sql);
		return $q->num_rows;
	}
	function checkIfTransNameAvailable($name) {    
		$sql="SELECT * from ".$this->INSTANCE." WHERE trans_eng_name='".$name."' LIMIT 1";
		$q=$this->db->query($sql);
		return $q->num_rows;
	}
	function addNewTerm(){
		$data=array(
			'trans_label_name' 		=> $this->input->post('term_name'),
			'trans_label_desc' 	=> $this->input->post('term_desc'),
			'trans_added'=>date('Y-m-d H:i:s'),
			'trans_added_by' => $this->session->userdata('admin_id')
		);
		$this->db->insert($this->INSTANCE,$data);
	}
	function addNewTranslation(){	
		$data=array(
			'trans_eng_name' 		=> $this->input->post('trans_eng_name'),
			'trans_native_name' 	=> $this->input->post('trans_native_name'),
			'trans_added'=>date('Y-m-d H:i:s'),
			'updated_by_user_id' => $this->session->userdata('admin_id')
		);
		$this->db->insert($this->INSTANCE,$data);
		$inserted_id=$this->db->insert_id();
		$colname="lang_col_".$inserted_id;
		$sql="ALTER table ".$this->INSTANCE_Details." ADD COLUMN ".$colname." VARCHAR(1000) DEFAULT '' ";
		$q=$this->db->query($sql);		
		
		return $inserted_id;
	}
	function populateTranslationTable(){
		$startRow	=$this->input->post('numRow');
		$numRow	=$this->input->post('numRow');
		
		$lang_col_1	=$this->input->post('lang_col_1');
		$lang_col_2	=$this->input->post('lang_col_2');
			
		//get trans list
		$sql="SELECT trans_id,trans_label_name,trans_label_desc FROM  ".$this->INSTANCE." ORDER BY trans_id ASC";
		$q=$this->db->query($sql);		
		if($q->num_rows()>0){
			foreach($q->result() as $row){
				//get trans_details for column 1
				$sql1="SELECT trans_detail_translation_string FROM  ".$this->INSTANCE_Details."  
				WHERE trans_detail_trans_id=".$row->trans_id."  AND trans_detail_language_id=".$lang_col_1." LIMIT 1";
				$q1=$this->db->query($sql1);
				if($q1->num_rows()>0){
					$r1=$q1->row();
					$trans_col_1=$r1->trans_detail_translation_string;
				}
				else{
					$trans_col_1="";
				}
				
				//get trans_details for column 2
				$sql2="SELECT trans_detail_translation_string FROM  ".$this->INSTANCE_Details."  
				WHERE trans_detail_trans_id=".$row->trans_id."  AND trans_detail_language_id=".$lang_col_2." LIMIT 1";
				$q2=$this->db->query($sql2);
				if($q2->num_rows()>0){
					$r2=$q2->row();
					$trans_col_2=$r2->trans_detail_translation_string;
				}
				else{
					$trans_col_2="";
				}
				
				$rows[] = (object) array(				
				'trans_id' 					=>  $row->trans_id,
				'trans_label_name' 	=>  $row->trans_label_name,
				'trans_label_desc' 	=>  $row->trans_label_desc,
				'col_1' 	=>  $trans_col_1,
				'col_2' 	=>  $trans_col_2
				);				
			}
			return $rows;
		}
		else{
			return false;
		}
	}
	function updateTransDetails(){
		$numSelected	=$this->input->post('numSelected');
		
		$lang_col_1	=$this->input->post('lang_col_1');
		
		$lang_col_2	=$this->input->post('lang_col_2');		
		
		$new_numSelected	=$this->input->post('new_numSelected');		
		
		//insert
		for($j=0;$j<$new_numSelected;$j++){			
			$trans_label_name	=$this->input->post('new_trans_label_'.$j);
			$trans_label_desc	=$this->input->post('new_trans_desc_'.$j);
			$trans_col_1			=$this->input->post('new_trans_col_1_'.$j);
			$trans_col_2			=$this->input->post('new_trans_col_2_'.$j);
			
			
			$trans_col_1=str_replace('"','&quot;',$trans_col_1);
			$trans_col_2=str_replace('"','&quot;',$trans_col_2);
			
			
			
			//add the trans table			
			$content=array(
				'trans_label_name'	=>$this->input->post('new_trans_label_'.$j),
				'trans_label_desc'	=>$this->input->post('new_trans_desc_'.$j),				
				'trans_added'=>date('Y-m-d H:i:s'),
				'trans_added_by' => $this->session->userdata('admin_id')
				);					
			$this->db->insert($this->INSTANCE,$content);			
			$inserted_id=$this->db->insert_id();
			//
			
			//add the trans details table											
			$content1=array(
				'trans_detail_trans_id'=>$inserted_id,
				'trans_detail_language_id'=>$lang_col_1,
				'trans_detail_translation_string'=>$trans_col_1						
			);
			$this->db->insert($this->INSTANCE_Details,$content1);					
			
			
			$content2=array(
				'trans_detail_trans_id'=>$inserted_id,
				'trans_detail_language_id'=>$lang_col_2,
				'trans_detail_translation_string'=>$trans_col_2
			);
			$this->db->insert($this->INSTANCE_Details,$content2);					
				
		}
		
		//update
		for($j=0;$j<$numSelected;$j++){
		
				$trans_id				=$this->input->post('transID_'.$j);
				$trans_label_name=$this->input->post('trans_label_'.$j);
				$trans_label_desc	=$this->input->post('trans_desc_'.$j);
				$trans_col_1			=$this->input->post('trans_col_1_'.$j);
				$trans_col_2			=$this->input->post('trans_col_2_'.$j);
				
				$trans_col_1=str_replace('"','&quot;',$trans_col_1);
				$trans_col_2=str_replace('"','&quot;',$trans_col_2);
								
								
				
				//update the trans table
				$where=array('trans_id'=>$trans_id);								
				$content=array(
						'trans_label_name'	=>$this->input->post('trans_label_'.$j),
						'trans_label_desc'	=>$this->input->post('trans_desc_'.$j),				
						'trans_last_updated'=>date('Y-m-d H:i:s'),
						'trans_last_updated_by' => $this->session->userdata('admin_id')
						);		
				$this->db->update($this->INSTANCE,$content,$where);		
				//update the trans table
				
				//update the trans details table
				//check first if exist
				$sql1="SELECT trans_detail_translation_string FROM  ".$this->INSTANCE_Details."  
				WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_1." LIMIT 1";
				$q1=$this->db->query($sql1);
				if($q1->num_rows()>0){				
					$sql1="UPDATE ".$this->INSTANCE_Details."  SET trans_detail_translation_string='".$trans_col_1."'
						WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_1." LIMIT 1";
					$q1=$this->db->query($sql1);
				}
				else{
					//no data saved so insert						
					$content1=array(
						'trans_detail_trans_id'=>$trans_id,
						'trans_detail_language_id'=>$lang_col_1,
						'trans_detail_translation_string'=>$trans_col_1						
					);
					$this->db->insert($this->INSTANCE_Details,$content1);					
				}
					
				
				//update the trans details table
				//check first if exist
				$sql2="SELECT trans_detail_translation_string FROM  ".$this->INSTANCE_Details."  
				WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_2." LIMIT 1";
				$q2=$this->db->query($sql2);
				if($q2->num_rows()>0){
				
					$sql2="UPDATE ".$this->INSTANCE_Details."  SET trans_detail_translation_string='".$trans_col_2."'
						WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_2." LIMIT 1";
					$q2=$this->db->query($sql2);
				}
				else{
					//no data saved so insert
					$content=array(
						'trans_detail_trans_id'=>$trans_id,
						'trans_detail_language_id'=>$lang_col_2,
						'trans_detail_translation_string'=>$trans_col_2						
					);
					$this->db->insert($this->INSTANCE_Details,$content);					
				}
		}
	}
	function generateFile(){
		/*/$my_file = __DIR__.base_url();		
			//$my_file = base_url().'assets/files.php';		
		$my_file = APPPATH.'language/english/files.php';
		echo $my_file;
		//$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //implicitly creates file				
		$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //open file for writing ('w','r','a')...		
		
		$data ="\n".'$lang["landing_page_title"]="Welcome | glomp!"; ';
		fwrite($handle, $data);
		$new_data = "\n".'New data line 2';
		fwrite($handle, $new_data);
		fclose($handle);
		*/
		//generate all languages
		$sql="SELECT lang_id,lang_name,lang_key FROM  ".$this->INSTANCE_Language." ORDER BY lang_id ASC";
		$q=$this->db->query($sql);		
		if($q->num_rows()>0){
			foreach($q->result() as $row){
				// in each language create a file
				$lang_key=$row->lang_key;
				$my_file = APPPATH.'language/all_languages/'.$lang_key.'_files_lang.php';
				$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //open file for writing ('w','r','a')...				
				//$data = '<?php\n';
				$data = '<?php';
				fwrite($handle, $data);
				$data ="\n".'/*Author: Ryan Magbanua';
				fwrite($handle, $data);
				$data ="\n".'Velocity Solutions';
				fwrite($handle, $data);
				$data ="\n".'Date:'.date('Y-m-d H:i:s').'';
				fwrite($handle, $data);
				$data ="\n".'Desc: Auto generated language label';
				fwrite($handle, $data);
				$data ="\n".'Language: '.$row->lang_name.'*/';
				fwrite($handle, $data);
				$data ="\n".'// now create data';
				fwrite($handle, $data);
				
					$sql1="SELECT  ".$this->INSTANCE.".trans_id,  ".$this->INSTANCE.".trans_label_name, ".$this->INSTANCE_Details.".trans_detail_translation_string FROM  ".$this->INSTANCE."
					JOIN ".$this->INSTANCE_Details."  ON " . $this->INSTANCE_Details.".trans_detail_trans_id = ". $this->INSTANCE.".trans_id
					WHERE ".$this->INSTANCE_Details.".trans_detail_language_id=".$row->lang_id;
					$q1=$this->db->query($sql1);
					foreach($q1->result() as $row1){
						$data ="\n".'$lang["'.$row1->trans_label_name.'"]="'. $row1->trans_detail_translation_string.'"; ';
						fwrite($handle, $data);	
					
					}
				fclose($handle);				
			}			
			//chown($my_file,"ftp_user");
			//chmod($my_file,0777);
			//chgrp($my_file,"ftp_user");
			
			//return $rows;
                        
                        $data = array(
                            'config_value' => $this->custom_func->config_value('TRANSLATION_VERSION') + 1,
                        );
                            
                        $this->db->where('config_key', 'TRANSLATION_VERSION');
                        $this->db->update('gl_config', $data); 

		}
		else{
			//return false;
		}
		
	}
        function generateTable(){
		//generate all languages
		$sql="SELECT lang_id,lang_name,lang_key FROM  ".$this->INSTANCE_Language." ORDER BY lang_id ASC";
		$q=$this->db->query($sql);
                $data = '';
		if($q->num_rows()>0){
			foreach($q->result() as $row){
                            $data .= '<h1> Language:'. $row->lang_name.'</h1>';
				// in each language create a file

					$sql1="SELECT  ".$this->INSTANCE.".trans_id,  ".$this->INSTANCE.".trans_label_name, ".$this->INSTANCE_Details.".trans_detail_translation_string FROM  ".$this->INSTANCE."
					JOIN ".$this->INSTANCE_Details."  ON ".$this->INSTANCE.".trans_id = ".$this->INSTANCE_Details.".trans_detail_trans_id
					WHERE ".$this->INSTANCE_Details.".trans_detail_language_id=".$row->lang_id;
					$q1=$this->db->query($sql1);
                                        $data .= '<table>';
					foreach($q1->result() as $row1){
						$data .="<tr><td>".$row1->trans_label_name.'</td>';
						$data .="<td>".$row1->trans_detail_translation_string.'</td></tr>';
					}
                                        $data .= '</table><br />';
			}
		}
		else{ }
                echo $data;
	}        
        
	function deleteTransDetails(){
		$numSelected	=$this->input->post('numSelected');
		
		$lang_col_1	=$this->input->post('lang_col_1');
		$lang_col_2	=$this->input->post('lang_col_2');		
		for($j=0;$j<$numSelected;$j++){
			$trans_id	=$this->input->post('transID_'.$j);
			//delete the trans table
			$sql1="DELETE FROM ".$this->INSTANCE."  WHERE trans_id=".$trans_id."  LIMIT 1";
			$q1=$this->db->query($sql1);				
			//delete the trans table
			
			$sql1="DELETE FROM ".$this->INSTANCE_Details."  
			WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_1." LIMIT 1";
			$q1=$this->db->query($sql1);							
			
			$sql2="DELETE FROM ".$this->INSTANCE_Details."  
			WHERE trans_detail_trans_id=".$trans_id."  AND trans_detail_language_id=".$lang_col_2." LIMIT 1";
			$q2=$this->db->query($sql2);
			
		}
	}
	
	function generateCountry(){ 
		$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        
        $countries= array(
            'Afghanistan' => 'Asia/Kabul',
            'Albania' => 'Europe/Zagreb',
            'Algeria' => 'Europe/Zagreb',
            'American Samoa' => 'Pacific/Samoa',
            'Andorra' => 'Europe/Zagreb',
            'Angola' => 'Europe/Zagreb',
            'Anguilla' => 'America/Santiago',
            'Antarctica' => '',
            'Antigua and Barbuda' => 'America/Santiago',
            'Argentina' => 'America/Godthab',
            'Armenia' => 'Asia/Muscat',
            'Aruba' => '',
            'Australia' => 'Asia/Karachi',
            'Austria' => 'Europe/Zagreb',
            'Azerbaijan' => 'Asia/Muscat',
            'Bahamas' => 'America/Bogota',
            'Bahrain' => 'Europe/Volgograd',
            'Bangladesh' => 'Asia/Yekaterinburg',
            'Barbados' => 'America/Santiago',
            'Belarus' => 'Europe/Volgograd',
            'Belgium' => 'Europe/Zagreb',
            'Belize' => 'Canada/Saskatchewan',
            'Benin' => 'Europe/Zagreb',
            'Bermuda' => 'America/Santiago',
            'Bhutan' => 'Asia/Yekaterinburg',
            'Bolivia' => 'America/Santiago',
            'Bosnia and Herzegowina' => 'Europe/Zagreb',
            'Botswana' => 'Europe/Vilnius',
            'Bouvet Island' => '',
            'Brazil' => 'America/Santiago',
            'British Indian Ocean Territory' => 'Asia/Yekaterinburg',
            'Brunei Darussalam' => 'Asia/Singapore',
            'Bulgaria' => 'Europe/Vilnius',
            'Burkina Faso' => 'UTC',
            'Burundi' => 'Europe/Vilnius',
            'Cambodia' => 'Asia/Novosibirsk',
            'Cameroon' => 'Europe/Zagreb',
            'Canada' => 'America/Tijuana',
            'Cape Verde' => 'Atlantic/Cape_Verde',
            'Cayman Islands' => 'America/Bogota',
            'Central African Republic' => 'Europe/Zagreb',
            'Chad' => 'Europe/Zagreb',
            'Chile' => 'America/Santiago',
            'China' => 'Asia/Singapore',
            'Christmas Island' => 'America/Bogota',
            'Cocos (Keeling) Islands' => 'Asia/Rangoon',
            'Colombia' => 'America/Bogota',
            'Comoros' => 'Europe/Volgograd',
            'Congo' => 'Europe/Vilnius',
            'Congo, the Democratic Republic of the' => 'Europe/Zagreb',
            'Cook Islands' => 'Pacific/Honolulu',
            'Costa Rica' => 'Canada/Saskatchewan',
            'Cote d Ivoire' => '',
            'Croatia (Hrvatska)' => 'Europe/Zagreb',
            'Cuba' => 'America/Bogota',
            'Cyprus' => 'Europe/Vilnius',
            'Czech Republic' => 'Europe/Zagreb',
            'Denmark' => 'Atlantic/Cape_Verde',
            'Djibouti' => 'Europe/Volgograd',
            'Dominica' => 'America/Santiago',
            'Dominican Republic' => 'America/Santiago',
            'East Timor' => 'Asia/Irkutsk',
            'Ecuador' => 'Canada/Saskatchewan',
            'Egypt' => 'Europe/Vilnius',
            'El Salvador' => 'Canada/Saskatchewan',
            'Equatorial Guinea' => 'Europe/Zagreb',
            'Eritrea' => 'Europe/Volgograd',
            'Estonia' => 'Europe/Vilnius',
            'Ethiopia' => 'Europe/Volgograd',
            'Falkland Islands (Malvinas)' => 'America/Godthab',
            'Faroe Islands' => 'UTC',
            'Fiji' => 'Pacific/Auckland',
            'Finland' => 'Europe/Vilnius',
            'France' => 'Pacific/Honolulu',
            'France Metropolitan' => 'Europe/Zagreb',
            'French Guiana' => 'America/Godthab',
            'French Polynesia' => 'Pacific/Honolulu',
            'French Southern Territories' => 'Pacific/Honolulu',
            'Gabon' => 'Europe/Zagreb',
            'Gambia' => 'UTC',
            'Georgia' => 'Asia/Muscat',
            'Germany' => 'Europe/Zagreb',
            'Ghana' => 'UTC',
            'Gibraltar' => 'Europe/Zagreb',
            'Greece' => 'Europe/Vilnius',
            'Greenland' => 'America/Santiago',
            'Grenada' => 'America/Santiago',
            'Guadeloupe' => 'America/Santiago',
            'Guam' => 'Asia/Yakutsk',
            'Guatemala' => 'Canada/Saskatchewan',
            'Guinea' => 'UTC',
            'Guinea-Bissau' => 'UTC',
            'Guyana' => 'America/Santiago',
            'Haiti' => 'America/Bogota',
            'Heard and Mc Donald Islands' => 'Asia/Karachi',
            'Holy See (Vatican City State)' => 'Europe/Zagreb',
            'Honduras' => 'Canada/Saskatchewan',
            'Hong Kong' => 'Asia/Singapore',
            'Hungary' => 'Europe/Zagreb',
            'Iceland' => 'UTC',
            'India' => 'Asia/Calcutta',
            'Indonesia' => 'Asia/Novosibirsk',
            'Iran (Islamic Republic of)' => 'Asia/Tehran',
            'Iraq' => 'Europe/Volgograd',
            'Ireland' => 'UTC',
            'Israel' => 'Europe/Vilnius',
            'Italy' => 'Europe/Zagreb',
            'Jamaica' => 'America/Bogota',
            'Japan' => 'Asia/Irkutsk',
            'Jordan' => 'Europe/Volgograd',
            'Kazakhstan' => 'Asia/Karachi',
            'Kenya' => 'Europe/Volgograd',
            'Kiribati' => 'Pacific/Auckland',
            'Korea, Democratic Peoples Republic of' => 'Asia/Irkutsk',
            'Korea, Republic of' => 'Asia/Irkutsk',
            'Kuwait' => 'Europe/Volgograd',
            'Kyrgyzstan' => 'Asia/Yekaterinburg',
            'Lao, Peoples Democratic Republic' => 'Asia/Novosibirsk',
            'Latvia' => 'Europe/Vilnius',
            'Lebanon' => 'Europe/Vilnius',
            'Lesotho' => 'Europe/Vilnius',
            'Liberia' => 'UTC',
            'Libyan Arab Jamahiriya' => 'Europe/Zagreb',
            'Liechtenstein' => 'Europe/Zagreb',
            'Lithuania' => 'Europe/Vilnius',
            'Luxembourg' => 'Europe/Zagreb',
            'Macau' => 'Asia/Singapore',
            'Macedonia, The Former Yugoslav Republic of' => 'Europe/Zagreb',
            'Madagascar' => 'Europe/Volgograd',
            'Malawi' => 'Europe/Vilnius',
            'Malaysia' => 'Asia/Singapore',
            'Maldives' => 'Asia/Karachi',
            'Mali' => 'UTC',
            'Malta' => 'Europe/Zagreb',
            'Marshall Islands' => 'Pacific/Auckland',
            'Martinique' => 'America/Santiago',
            'Mauritania' => 'UTC',
            'Mauritius' => 'Asia/Muscat',
            'Mayotte' => 'Europe/Volgograd',
            'Mexico' => 'Canada/Saskatchewan',
            'Micronesia, Federated States of' => 'Asia/Vladivostok',
            'Moldova, Republic of' => 'Europe/Vilnius',
            'Monaco' => 'Europe/Zagreb',
            'Mongolia' => 'Asia/Singapore',
            'Montserrat' => 'America/Santiago',
            'Morocco' => 'UTC',
            'Mozambique' => 'Europe/Vilnius',
            'Myanmar' => 'Asia/Rangoon',
            'Namibia' => 'Europe/Zagreb',
            'Nauru' => 'Pacific/Auckland',
            'Nepal' => 'Asia/Katmandu',
            'Netherlands' => 'Europe/Zagreb',
            'Netherlands Antilles' => 'Europe/Zagreb',
            'New Caledonia' => 'Asia/Vladivostok',
            'New Zealand' => 'Pacific/Auckland',
            'Nicaragua' => 'Canada/Saskatchewan',
            'Niger' => 'Europe/Zagreb',
            'Nigeria' => 'Europe/Zagreb',
            'Niue' => 'Pacific/Samoa',
            'Norfolk Island' => 'Asia/Vladivostok',
            'Northern Mariana Islands' => 'Asia/Yakutsk',
            'Norway' => 'Europe/Zagreb',
            'Oman' => 'Asia/Muscat',
            'Pakistan' => 'Asia/Karachi',
            'Palau' => 'Asia/Irkutsk',
            'Panama' => 'America/Bogota',
            'Papua New Guinea' => 'Asia/Yakutsk',
            'Paraguay' => 'America/Santiago',
            'Peru' => 'America/Bogota',
            'Philippines' => 'Asia/Singapore',
            'Pitcairn' => 'America/Tijuana',
            'Poland' => 'Europe/Zagreb',
            'Portugal' => 'UTC',
            'Puerto Rico' => 'America/Santiago',
            'Qatar' => 'Europe/Volgograd',
            'Reunion' => '',
            'Romania' => 'Europe/Vilnius',
            'Russian Federation' => 'Asia/Muscat',
            'Rwanda' => 'Europe/Vilnius',
            'Saint Kitts and Nevis' => 'America/Santiago',
            'Saint Lucia' => 'America/Santiago',
            'Saint Vincent and the Grenadines' => 'America/Santiago',
            'Samoa' => 'Pacific/Tongatapu',
            'San Marino' => 'Europe/Zagreb',
            'Sao Tome and Principe' => '',
            'Saudi Arabia' => 'Europe/Volgograd',
            'Senegal' => 'UTC',
            'Seychelles' => 'Asia/Muscat',
            'Sierra Leone' => 'UTC',
            'Singapore' => 'Asia/Singapore',
            'Slovakia (Slovak Republic)' => 'Europe/Zagreb',
            'Slovenia' => 'Europe/Zagreb',
            'Solomon Islands' => 'Asia/Vladivostok',
            'Somalia' => 'Europe/Volgograd',
            'South Africa' => 'Europe/Vilnius',
            'South Georgia and the South Sandwich Islands' => '',
            'Spain' => 'Europe/Zagreb',
            'Sri Lanka' => 'Asia/Calcutta',
            'St. Helena' => 'UTC',
            'St. Pierre and Miquelon' => 'America/Godthab',
            'Sudan' => 'Europe/Volgograd',
            'Suriname' => 'America/Godthab',
            'Svalbard and Jan Mayen Islands' => '',
            'Swaziland' => 'Europe/Vilnius',
            'Sweden' => 'Europe/Zagreb',
            'Switzerland' => 'Europe/Zagreb',
            'Syrian Arab Republic' => 'Europe/Vilnius',
            'Taiwan, Province of China' => 'Asia/Singapore',
            'Tajikistan' => 'Asia/Karachi',
            'Tanzania, United Republic of' => 'Europe/Volgograd',
            'Thailand' => 'Asia/Novosibirsk',
            'Togo' => 'UTC',
            'Tokelau' => 'Pacific/Tongatapu',
            'Tonga' => 'Pacific/Tongatapu',
            'Trinidad and Tobago' => 'America/Santiago',
            'Tunisia' => 'Europe/Zagreb',
            'Turkey' => 'Europe/Vilnius',
            'Turkmenistan' => 'Asia/Karachi',
            'Turks and Caicos Islands' => 'America/Bogota',
            'Tuvalu' => 'Pacific/Auckland',
            'Uganda' => 'Europe/Volgograd',
            'Ukraine' => 'Europe/Vilnius',
            'United Arab Emirates' => 'Asia/Muscat',
            'United Kingdom' => 'America/Tijuana',
            'United States' => 'Pacific/Samoa',
            'United States Minor Outlying Islands' => 'Pacific/Samoa',
            'Uruguay' => 'America/Godthab',
            'Uzbekistan' => 'Asia/Karachi',
            'Vanuatu' => 'Asia/Vladivostok',
            'Venezuela' => 'America/Caracas',
            'Vietnam' => 'Asia/Novosibirsk',
            'Virgin Islands (British)' => 'America/Santiago',
            'Virgin Islands (U.S.)' => 'America/Santiago',
            'Wallis and Futuna Islands' => 'Pacific/Auckland',
            'Western Sahara' => '',
            'Yemen' => 'Europe/Volgograd',
            'Yugoslavia' => '',
            'Zambia' => 'Europe/Vilnius',
            'Zimbabwe' => 'Europe/Vilnius');
        
		//foreach ($countries as $value) {
        foreach($countries as $key => $value)
        {
            if($value=='')
                $value='Asia/Singapore';
                
            $sql="SELECT * from gl_region WHERE region_name='".$key."' LIMIT 1";
            $q=$this->db->query($sql);
            if($q->num_rows()>0)
            {//update
                $data=$q->row();
                
                $data_update=array(
                    'region_timezone'=>$value,
                );
                $this->db->where('region_id', $data->region_id);
                $this->db->update('gl_region', $data_update); 
                
                echo $key." has beed updated.<br>";
            }
            else
            {
                $content=array(
                'region_parent_id'=>'0',
                'region_name'=>$key,			
                'region_status'=>'Active',
                'region_selectable'=>'Y',			
                'region_added'=>date('Y-m-d H:i:s'),
                'region_timezone'=>$value
                );
                
                $this->db->insert('gl_region',$content);
                $inserted_id=$this->db->insert_id();
                
                $data_update=array(
                'region_parent_path_id'=>'/'.$inserted_id.'/',
                );
                $this->db->where('region_id', $inserted_id);
                $this->db->update('gl_region', $data_update); 
                
                echo $key." has beed inserted.<br>";
            }
                
			
		}
		
	}
        
        function get_translation_string($trans_detail_trans_id, $trans_detail_language_id) {
            $sql = 'SELECT trans_detail_translation_string 
                FROM '.$this->INSTANCE_Details.'
                WHERE trans_detail_trans_id = "'.$trans_detail_trans_id.'" AND trans_detail_language_id = "'.$trans_detail_language_id.'"';
            
            return $this->db->query($sql);
        }
	
	function generateLanguage(){
		$lang["landing_page_title"]="Welcome | glomp!"; 
		$lang["signup_email_subject"]="Welcome to glomp!"; 
		$lang["reset_password"]="Reset Password"; 
		$lang["reset_password_email_doesnot_exist"]="This email does not exist in our system"; 
		$lang["reset_password_email_subject"]="glomp! reset password"; 
		$lang["reset_password_successful_msg"]="Your new password has been sent into your supplied email address"; 
		$lang["Unexpected_Error"]="Unexpected error occured. Please try again."; 
		$lang["dob_can_not_future_date"]="Date of birth can not be future date"; 
		$lang["plz_provide_valid_dob"]="Please enter valid date of birth"; 
		$lang["email_already_exist_try_new_one"]="This email address is already exist. Plese try new one."; 
		$lang["page_404_error"]="Error 404 ! page not found"; 
		$lang["support_msg_sent_successfully"]="Your message has been sent successfully."; 
		$lang["first_name_blank"]="Please enter first name."; 
		$lang["last_name_blank"]="Please enter last name."; 
		$lang["invalid_email_address"]="Invalid email address. Please enter another email address."; 
		$lang["Invalid_Friend"]="Invalid Friend"; 
		$lang["Successfully_Glomped"]="Successfully Glomped"; 
		$lang["Insufficent_Balance"]="Sorry ! you do not have sufficient balance."; 
		$lang["Invalid_Product"]="Invalid product."; 
		$lang["Invalid_Password"]="Invalid Password."; 
		$lang["already_in_friend_list"]="This user is already in your friend list."; 
		$lang["invitation_email_msg_sent"]="Your invitataion email message has been sent."; 
		$lang["your_profile_udpated_successfully"]="Your profile upated successfully."; 
		$lang["GLOMP_INVITE_REQUEST_SENT_BODY"]=""; 
		$lang["GLOMP_INVITE_REQUEST_SENT_TITLE"]=""; 
		$lang["GLOMP_MESSAGE_JOIN"]=""; 
		$lang["GLOMP_FB_FRIENDS_LIST_TITLE"]=""; 
		$lang["GLOMP_GETTING_FB_FRIENDS"]=""; 
		$lang["Friends"]="Friends"; 
		$lang["No_result_found"]="No result found"; 
		$lang["Potential_Matches"]="Potential Matches"; 
		$lang["are_you_looking_for_these_persons"]="Are you looking these persons ?"; 
		$lang["add_friend"]="Add Friend"; 
		$lang["View_Profile"]="View Profile"; 
		$lang["Yes"]="Yes"; 
		$lang["Close"]="Close"; 
		$lang["Confirm"]="Confirm"; 
		$lang["Cancel"]="Cancel"; 
		$lang["DOB"]="DOB"; 
		$lang["add_as_friend"]="Add as Friend"; 
		$lang["friend"]="Friend"; 
		$lang["unfriend"]="Unfriend"; 
		$lang["remove_friend"]="Remove Friend"; 
		$lang["remove_friend_confrim_msg"]="Are you sure you want to remove this friend from your list?"; 
		$lang["favourite"]="favourite"; 
		$lang["has_not_added_favourite"]="has not added favourite items."; 
		$lang["merchants"]="merchants"; 
		$lang["drinks"]="drinks"; 
		$lang["snacks"]="snacks"; 
		$lang["cocktails"]="cocktails"; 
		$lang["sweets"]="sweets"; 
		$lang["others"]="others"; 
		$lang["to_a"]="to a"; 
		$lang["Fev_item_del_conformation"]="Are you sure you want to delete this favourite Item?"; 
		$lang["Removing"]="Removing..."; 
		$lang["Adding"]="Adding..."; 
		$lang["Update_Details"]="Update Details"; 
		$lang["My_Favourite"]="My Favourite"; 
		$lang["merchant_list"]="Merchant List"; 
		$lang["create_more_buzz_msg"]="Create more buzz by adding more friends now !"; 
		$lang["Add_Friends"]="Add Friends"; 
		$lang["ago"]="ago"; 
		$lang["second"]="Second"; 
		$lang["minute"]="Minute"; 
		$lang["hour"]="Hour"; 
		$lang["day"]="Day"; 
		$lang["week"]="Week"; 
		$lang["month"]="Month"; 
		$lang["year"]="Year"; 
		$lang["decade"]="Decade"; 
		$lang["glomp_confirmation_message"]="Are you sure you dont want to include a message"; 
		$lang["Reassign_Voucher"]="Re-assign Voucher"; 
		$lang["Message"]="Message"; 
		$lang["Your_Password"]="Your Password"; 
		$lang["Friend_First_Name"]=" Friend First Name"; 
		$lang["Friend_Last_Name"]="Friend Last Name"; 
		$lang["Friend_Email"]="Friend Email"; 
		$lang["Forgot_Password"]="Forgot Password"; 
		$lang["search_my_friends"]="Search My Friends"; 
		$lang["Facebook"]="Facebook"; 
		$lang["invite_friend"]="Invite Friend"; 
		$lang["Name"]="Name"; 
		$lang["Email"]="Email"; 
		$lang["Location"]="Location"; 
		$lang["Full_Message"]="Full Message"; 
		$lang["reward_voucher_gifted"]="Reward voucher has been gifted."; 
		$lang["reward_voucher_expired"]="Reward voucher has been expired."; 
		$lang["dashboard_no_glomped_msg"]="Want to be glomp!ed (treated)? of course  you do ! make it easier for your friends  to glomp! you by creating your favorite  list that a list of your favorite products from favorite  place."; 
		$lang["Add_Favorites_Now"]="Add Favorites Now"; 
		$lang["dashboard_no_glomped_msg_2"]="And of course, and the more friends you have, the more change you will be glomp!ed! so add more friends now."; 
		$lang["setting"]="Setting"; 
		$lang["Personal_Details"]="Personal Details"; 
		$lang["fname"]="First Name"; 
		$lang["lname"]="Last Name"; 
		$lang["Date_of_Birth"]="Date of Birth"; 
		$lang["Gender"]="Gender"; 
		$lang["Male"]="Male"; 
		$lang["Female"]="Female"; 
		$lang["Select_Location"]="Select Location"; 
		$lang["upload_profile_photo"]="Upload Profile Photo"; 
		$lang["Note"]="Note"; 
		$lang["profile_image_upload_note"]="Max. 2MB of jpg,gif and png images are supported."; 
		$lang["E-mail"]="E-mail"; 
		$lang["Current_Password"]="Current Password"; 
		$lang["New_password"]="New password"; 
		$lang["Confirm_Password"]="Confirm Password"; 
		$lang["Save"]="Save"; 
		$lang["update_password"]="Update Password"; 
		$lang["Support"]="Support"; 
		$lang["contact_support"]="Contact Support"; 
		$lang["Subject"]="Subject"; 
		$lang["Description"]="Description"; 
		$lang["required_field"]="Required field"; 
		$lang["Submit"]="Submit"; 
		$lang["reset_password_confirm_msg"]="Are you sure you want to reset your password?"; 
		$lang["Register"]="Register"; 
		$lang["Fname"]="First Name"; 
		$lang["Lname"]="Last Name"; 
		$lang["Notif_via_fb"]="Notify via Facebook message"; 
		$lang["email"]="Email"; 
		$lang["emailTips"]="If you were invited via email treat attached, please enter the same email adddress here inorder to redeem."; 
		$lang["Create_Password"]="Create Password"; 
		$lang["passwors_tips"]="You will need to enter this Password when sending and redeeming product so please remember it."; 
		$lang["I_Agree_to_the"]="I agree to the"; 
		$lang["termNconditi0n"]="Term & Conditions"; 
		$lang["Reset_All_Fields"]="Reset All Fields"; 
		$lang["termNcondition"]="Terms & Condtions"; 
		$lang["sorry_no_product_available"]="Sorry! No products are currently available in this category."; 
		$lang["sorry_invalid_voucher"]="Sorry, Invalid voucher"; 
		$lang["back"]="Back"; 
		$lang["redeem_your_voucher_now"]="Redeem your voucher now"; 
		$lang["voucher_alrady_reddemed_msg"]="This voucher is already redeemed."; 
		$lang["voucher_reversed_msg"]="This voucher has been reversed and no longer available"; 
		$lang["outlet_code"]="Outlet Code"; 
		$lang["incorrect_password"]="Incorrect Password"; 
		$lang["incorrect_outlet_code"]="Incorrect outlet code"; 
		$lang["unexpected_error_occurred_try_again"]="Unexpected error occured please try again."; 
		$lang["validate"]="Validate"; 
		$lang["signup_popup_text"]="If you were Invited via Facebook with a treat attached, please login with Facebook in order to redeem."; 
		$lang["signup_with_fb_but_already_registered"]="Ooop! looks like you have already an account registered here."; 
		$lang["Glomp"]="Glomp"; 
		$lang["Glompv"]="[Glomp] v."; 
		$lang["Glomp_welcome_message"]="To treat friends a nice little something through the \'social\' world, 	for them to enjoy in the real one"; 
		$lang["Login"]="Login"; 
		$lang["Sign_Up"]="Sign Up!"; 
		$lang["Password"]="Password"; 
		$lang["Login_Facebook"]="Login"; 
		$lang["close"]="Close"; 
		$lang["Buy_points"]="Buy Points"; 
		$lang["Payment"]="Payment"; 
		$lang["Remaning"]="Remaning"; 
		$lang["paupal_redirect_msg"]="Please be patient, you will be redirected to "; 
		$lang["PayPal"]="  PayPal"; 
		$lang["Purchase_Detals"]="Purchase Details"; 
		$lang["You_Selected"]="You Selected"; 
		$lang["please_pay"]="Please Pay"; 
		$lang["checkout"]="Checkout"; 
		$lang["name"]="Name"; 
		$lang["location"]="Location"; 
		$lang["add_your_message"]="Add your message"; 
		$lang["invite"]="Invite"; 
		$lang["faq"]="FAQ"; 
		$lang["Home"]="Home"; 
		$lang["profile"]="Profile"; 
		$lang["buy_points"]="Buy Points"; 
		$lang["menu"]="Menu"; 
		$lang["favourites"]="Favourites"; 
		$lang["log_out"]="Log Out"; 
		$lang["voucher_already_expired_msg"]="Sorry, This voucher is already expired"; 
		$lang["invalid_voucher"]="Sorry... We\\\'re afraid this voucher is not valid."; 
		$lang["fame"]=""; 
		$lang["Profile_Picture"]="Profile Picture"; 
		$lang["Read"]="Read"; 
		$lang["your_voucher_has_been_validated"]="Your voucher has been validated"; 
		$lang["enjoy"]="Enjoy"; 
		$lang["verification_code"]="verification code"; 
		$lang["reedmed_success_message_note"]="Please do not leave this page until the merchant has view and recored the verification code."; 
		$lang["Sign_in"]="Sign in"; 
		$lang["register"]="Register"; 
		$lang["full_destop_website"]="Full Destop Website"; 
		$lang["Redeem"]="Redeem"; 
		$lang["try_again"]="Try Again"; 
		$lang["glomp_password"]="glomp! password"; 
		$lang["Log_Out"]="Log Out"; 
		$lang["lang"]="English"; 
		$lang["GLOMP_INVITE_REQUEST_SENT_TITLE"]="Success!"; 
		$lang["GLOMP_INVITE_REQUEST_SENT_BODY"]="Your glomp invite request has been sent."; 
		$lang["GLOMP_MESSAGE_JOIN"]="Join Glomp Now! "; 
		$lang["GLOMP_FB_FRIENDS_LIST_TITLE"]="Facebook Friends List"; 
		$lang["GLOMP_GETTING_FB_FRIENDS"]="Getting you Facebook friends..."; 
		$lang["GLOMP_INVITE_FB_FRIENDS"]="Invite from Facebook"; 
		$lang["GLOMP_FB_POPUP_TITLE_ADD"]="Add Friend"; 
		$lang["GLOMP_FB_POPUP_TITLE_INVITE"]="Invite Friend"; 
		$lang["GLOMP_TOUR_GET_STARTED"]="Get started now"; 
		$lang["GLOMP_TOUR_START"]="Let us help you get started"; 
		$lang["GLOMP_ADD_FRIEND_SUCCESS"]="Friend added";

		foreach ($lang as $key => $value) {
			echo "$key => $value<br>";			
			$content=array(
			'trans_label_name'=>$key,
			'trans_label_desc'=>$value,
			'trans_added'=>date('Y-m-d H:i:s'),
			'trans_added_by'=>'6'
			);
			$this->db->insert('gl_trans',$content);
			$inserted_id=$this->db->insert_id();
			
					
			$content=array(
			'trans_detail_trans_id'=>$inserted_id,
			'trans_detail_language_id'=>'1',
			'trans_detail_translation_string'=>$value
			);
			$this->db->insert('gl_trans_detail',$content);			
			
			$content=array(
			'trans_detail_trans_id'=>$inserted_id,
			'trans_detail_language_id'=>'2',
			'trans_detail_translation_string'=>$value
			);
			$this->db->insert('gl_trans_detail',$content);			
		}
	}
	
}//eoc
?>
