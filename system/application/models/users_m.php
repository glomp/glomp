<?php 
#written by shree
#Date july 30, 2013
#This model will have all about users activity
require_once('super_model.php');	
class Users_m extends Super_model {
    
    function __construct() {
        $CI =& get_instance();
        parent::__construct('gl_user user');
        $CI->load->model('campaign_m');
        $CI->load->model('product_m');
    }
    
    function update_twitter_token($user_id, $user_twitter_token, $user_twitter_secret)
    {
        $data = array(
            'user_twitter_token'=>$user_twitter_token,
            'user_twitter_secret'=>$user_twitter_secret
        );
		$where=array(
		'user_id'=>$user_id
		);
		$this->db->update('gl_user',$data,$where);	
    
    }
    function get_image($profil_id)
    {
        $sql ="SELECT u.user_profile_pic,u.user_gender  FROM gl_user u                         
                WHERE user_id='".$profil_id."'";
        $result = $this->db->query($sql);        
        return $result;
    }

    function get_promo_rule_desc($camp_id)
    {
        $sql ="SELECT campaign_by_promo_rule_desc FROM gl_campaign_details_by_promo WHERE campaign_id = '$camp_id'";
        $result = $this->db->query($sql);
        $desc='';
		if($result->num_rows()>0)
		{
            $data=$result->row();
            $desc=$data->campaign_by_promo_rule_desc;
        }
        return $desc;
    }
    
	function promo_code_save_to_user($promo_code,$user_id)
    {
		//check if promo code Exists
		$error=false;
		$sql ="SELECT * FROM gl_campaign_details_by_promo JOIN gl_campaign_rule ON gl_campaign_details_by_promo.campaign_id=gl_campaign_rule.camp_rule_campaign_id
		WHERE campaign_by_promo_code = '".db_input($promo_code)."'";
        
		$result = $this->db->query($sql);
		if($result->num_rows()>0)
		{//promo code exist			
			//check if promo code is still running
			$promo_data=$result->row();
			if($promo_data->campaign_by_promo_status=='Running' || $promo_data->campaign_by_promo_status=='Active')
			{//promo code running
                //check if there is still available primary vouchers
                $camp_id=$promo_data->campaign_id;
                $res_voucher = $this->campaign_m->campaign_voucher($camp_id);
                $num_rows = $res_voucher->num_rows();						
                $used_voucher = $num_rows;                
                $total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($camp_id);
                $available_voucher = $total_voucher-$used_voucher;						
                if($available_voucher>0)
                {//has available total voucher
                    //check if for non-member or both
                    if($promo_data->campaign_by_promo_for!='Member')
                    {//for non meber or both					
                        $min_age = $promo_data->min_age;
                        $max_age = $promo_data->max_age;
                        $gender = $promo_data->gender;
                        $region_list = $promo_data->associated_region_id;
                        $region_array = explode(',',$region_list);
                        if ($max_age == 0)
                            $max_age = 100;
                            
                        //query user	
                         $sql = "SELECT YEAR(CURDATE())-YEAR(user_dob)  as age , user_gender,user_city_id, user_dob  FROM gl_user WHERE user_id='$user_id'";			
                         $resUser = $this->db->query($sql);
                         $user_data=$resUser->row();
                         $user_data_age = $user_data->age;
                         
                         if ($user_data->user_dob == '0000-00-00' || empty($user_data->user_dob) ) {
                            $user_data_age = 0;
                         }					 
                         
                         
                        $rule_details = json_decode($promo_data->rule_details);
                        $whit_label_rule_okay = TRUE;
                        if(isset($rule_details->selected_list) && $rule_details->selected_list!='')
                        { 
                            $whit_label_rule_okay = FALSE;
                            foreach($this->campaign_m->select_people_by_rule($camp_id, "")->result() as $row)
                            {
                                if($row->user_id == $user_id)
                                    $whit_label_rule_okay = TRUE;    
                            }
                        }
                         
                        if( (($user_data_age) >= $min_age && ($user_data_age) <= $max_age)
                        && ($gender == "Both" || $gender == $user_data->user_gender)
                        && ( in_array($user_data->user_city_id,$region_array) ||  $region_list=="")
                        && $whit_label_rule_okay
                        )
                        {		
	
                            $camp_id=$promo_data->campaign_id;
                            //check if there is still available primary vouchers
                            $res_voucher = $this->campaign_m->campaign_voucher($camp_id,"AND camp_voucher_by_type='by_promo_primary' ");
                            $num_rows = $res_voucher->num_rows();						
                            $used_voucher = $num_rows;
                            $total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($camp_id,'by_promo_primary');
                            $available_voucher = $total_voucher-$used_voucher;						
                            if($available_voucher>0)
                            {//has available primary voucher
                                //now, generate a primary voucher to campaign 
                                $voucher_id=$this->campaign_m->generate_single_voucher_by_promo($user_id, $camp_id,'by_promo_primary');
                                if($voucher_id!="")
                                {//voucher has been generated
                                    $update_data= (object) array('status' => 'SuccessPrimary','campaign_id' => $promo_data->campaign_id,'voucher_id' => $voucher_id);							
                                    $sql = "UPDATE gl_campaign_details_by_promo SET campaign_by_promo_status = 'Running' WHERE campaign_id = '$camp_id'";
                                    $this->db->query($sql);
                                }
                                else
                                {//Unknown Error
                                    $update_data= (object) array('status' => 'UnknownError','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');							
                                }
                            
                            }
                            else
                            {//available primary voucher is zero
                                //now check if it has a follow up voucher
                                if($promo_data->campaign_by_promo_follow_up=='Y')
                                {//campaign has a follow up promo
                                    //check if there is still available follow up vouchers
                                    $res_voucher = $this->campaign_m->campaign_voucher($camp_id,"AND camp_voucher_by_type='by_promo_follow_up' ");
                                    $num_rows = $res_voucher->num_rows();						
                                    $used_voucher = $num_rows;
                                    $total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($camp_id,'by_promo_follow_up');
                                    $available_voucher = $total_voucher-$used_voucher;	
                                    if($available_voucher>0)
                                    {//has available follow up voucher
                                        //now, generate a follow up voucher and assign it to user
                                        $voucher_id=$this->campaign_m->generate_single_voucher_by_promo($user_id, $camp_id,'by_promo_follow_up');
                                        if($voucher_id!="")
                                        {//voucher has been generated
                                            $update_data= (object) array('status' => 'SuccessFollowUp','campaign_id' => $promo_data->campaign_id,'voucher_id' => $voucher_id);																	
                                            $sql = "UPDATE gl_campaign_details_by_promo SET campaign_by_promo_status = 'Running' WHERE campaign_id = '$camp_id'";
                                            $this->db->query($sql);
                                        }
                                        else
                                        {//Unknown Error
                                            $update_data= (object) array('status' => 'NoVoucherLeft','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');							
                                        }
                                    
                                    
                                    }
                                    else
                                    {//no follow up voucher left
                                        $update_data= (object) array('status' => 'NoVoucherLeft','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');													
                                    }
                                
                                }
                                else
                                {
                                    $update_data= (object) array('status' => 'NoVoucherLeft','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');							
                                }
                            
                            }
                        }
                        else
                        {//promo code is DidNoTPassedTheRule
                            $update_data= (object) array('status' => 'DidNoTPassedTheRule','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');					
                        }
                        
                    }
                    else
                    {//promo code is ForMemberOnly
                        $update_data= (object) array('status' => 'ForMemberOnly','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');					
                    }
                }
                else
                {// no vouchers left
                    $update_data= (object) array('status' => 'NoVoucherLeft','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');							
                }
			
			}
			else
			{//promo code is already done
				$update_data= (object) array('status' => 'DoneAlready','campaign_id' => $promo_data->campaign_id,'voucher_id' => '');				
			}
		}
		else
		{//invalid promo code
			$update_data= (object) array('status' => 'Invalid','campaign_id' => '','voucher_id' => '');			
		}
		
		$data = array('user_sign_up_promo_code'=>json_encode($update_data));
		$where=array(
		'user_id'=>$user_id
		);
		$this->db->update('gl_user',$data,$where);		
		
	}

	function is_connected_to_fb($user_id){
		$sql ="SELECT user_fb_id FROM gl_user WHERE user_id = '$user_id'";
		$result = $this->db->query($sql);
		$res	=$result->row();
		return $res->user_fb_id;		
	}
	function get_tour_status($user_id){
		$sql ="SELECT user_start_tour FROM gl_user WHERE user_id = '$user_id'";
		$result = $this->db->query($sql);
		$res	=$result->row();
		return $res->user_start_tour;		
	}
	function get_promo_code_status($user_id){
		$sql ="SELECT user_sign_up_promo_code FROM gl_user WHERE user_id = '$user_id'";
		$result = $this->db->query($sql);
		$res	=$result->row();
		return $res->user_sign_up_promo_code;		
	}
	
	function selectUser($subQuery="")
		{
			/// Author : Prakash on 8/6/2013  for admin purpose
			$sql ="SELECT *,CONCAT(user_fname,' ',user_lname)as user_name FROM gl_user WHERE 1=1 $subQuery";
			$result = $this->db->query($sql);
			return $result;
		}
	function userName($id)
		{
			/// Author : Prakash on 8/6/2013  for admin purpose
			$sql ="SELECT *,CONCAT(user_fname,' ',user_lname)as user_name FROM gl_user WHERE user_id='$id'";
			$result = $this->db->query($sql);
			if($result->num_rows())
			{
			$rec=$result->row();
			return $rec->user_name;
			}
			else
			return "";
		}
		
		
    function delete($userID)
    {
        /*gl_account        
        gl_activity
        gl_activity_log
        gl_activity_summary        
        gl_voucher
        gl_user_notification_settings
        gl_user        
        */
        
        $sql ="SELECT user_email FROM gl_user WHERE user_id = '$userID'";
        $result = $this->db->query($sql);                   
    if($result->num_rows()>0)
    {
        $recUser=$result->row();
        if( $recUser->user_email=='testone.glomp@gmail.com' ||
            $recUser->user_email=='testtwo.glomp@gmail.com' ||
            $recUser->user_email=='testthree.glomp@gmail.com' ||
            $recUser->user_email=='testfour.glomp@gmail.com' ||
            $recUser->user_email=='testfive.glomp@gmail.com' ||
            $recUser->user_email=='testsix.glomp@gmail.com')
        {
            //messages  
            // NOTE: not yet included on live
            /*$sql ="DELETE FROM gl_messages_rooms WHERE users_id = '$userID'";
            $this->db->query($sql);
            
            $sql ="DELETE FROM gl_messages WHERE user_from = '$userID' OR user_to = '$userID'";
            $this->db->query($sql);*/
            //messages
            
            //gl_invite
            $sql ="DELETE FROM gl_invite WHERE invite_by_user_id = '$userID'";
            $this->db->query($sql);
            //gl_invite
        
            $sql ="DELETE FROM gl_activity WHERE log_user_id = '$userID'";
            $this->db->query($sql);
            
            $sql ="DELETE FROM gl_account WHERE account_user_id = '$userID'";
            $this->db->query($sql);
            
            $sql ='DELETE FROM gl_activity_log WHERE user_id = "'.$userID.'"
                OR details LIKE \'%"recipient_id":"'.$userID.'"%\'
                OR details LIKE \'%"glomper_id":"'.$userID.'"%\'
                OR details LIKE \'%"friend_id":"'.$userID.'"%\'
                OR details LIKE \'%"added_friend_id":"'.$userID.'"%\'
                OR details LIKE \'%"recipient_id":"'.$userID.'"%\'
            ';             
            $this->db->query($sql);
            
            $sql ="DELETE FROM gl_activity_summary WHERE summary_user_id = '$userID'";
            $this->db->query($sql);
            
            $sql ="DELETE FROM gl_voucher WHERE voucher_purchaser_user_id = '$userID' OR voucher_belongs_usser_id = '$userID'";
            $this->db->query($sql);
            
            $sql ="DELETE FROM gl_user_notification_settings WHERE setting_user_id = '$userID'";
            $this->db->query($sql);
                    
            $sql ="DELETE FROM gl_user WHERE user_id = '$userID'";
            $this->db->query($sql);
        
            $this->session->set_flashdata('msg',"Record deleted successfully.$userID");
            
            //memcached clear
            $params = array(
                'affected_tables' 
                        => array(
                            'gl_user',
                            'gl_voucher'
                        ), #cache name
                'specific_names' 
                    => array(
                        'user_info_by_id_'.$userID,
                        'getUserFname_'.$userID,
                        'user_short_info_'.$userID,
                        
                    )    
            );
            delete_cache($params);                
            //memcached clear
            echo "deleted";
        }
        
        else
        {
            $this->session->set_flashdata('msg',"Cannot delete this record.");
        }
    }
        /* Old 
        /// Author : Prakash on 8/6/2013  for admin purpose
        $data = array('user_status'=>'Deleted');
        $where=array(
        'user_id'=>$userID
        );
        $this->db->update('gl_user',$data,$where);		        
        */
    }

		function updateStatus($userID,$status)
		{
		/// Author : Prakash on 8/6/2013  for admin purpose
	
		$data = array('user_status'=>$status);
		$where=array(
		'user_id'=>$userID
		);
		$this->db->update('gl_user',$data,$where);		
		}
		
		function selectActivityLogByID($userID,$limit)
		{
		/// Author : Prakash on 8/6/2013  for admin purpose	
		$sql ="SELECT * FROM gl_activity WHERE log_user_id = '$userID' ORDER  BY log_timestamp DESC $limit";
		$result = $this->db->query($sql);
		return $result;
		}
	
	function user_info_by_id($user_id)
	{
		$sql = "SELECT u.user_id,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_fname, 1)), LOWER(SUBSTRING(user_fname, 2)))
                    ELSE user_fname
                    END user_fname,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_lname, 1)), LOWER(SUBSTRING(user_lname, 2)))
                    ELSE user_lname
                    END user_lname, u.user_fb_id, u.user_linkedin_id, u.user_fb_nofify_via_fb,u.user_status,
                    concat(u.user_fname,' ',u.user_lname)as user_name,
                    u.user_dob,u.user_email,u.user_profile_pic,u.user_gender,u.user_city_id, u.user_start_tour,
                    user_email_verified,
                    u.user_lang_id, u.user_dob_display, u.user_hash_key, u.user_fname as ufname, u.user_lname as ulname
                    FROM gl_user u
                    WHERE
                    u.user_id = '$user_id'					
                    ";
				//AND u.user_status = 'Active'
                
                 $params = array(
                    'cache_name' => 'user_info_by_id_'.$user_id, #unique name
                    'cache_table_name' => array('gl_user'), #database table
                    'result' => $sql, #the result 
                    'result_type' => 'result', #if from DB result object
                );
                $result = get_create_cache($params);
                //$result = $this->db->query($sql);
		return $result;
	}
	function random_friends($user_id,$limit=10)
	{
		$sql = "SELECT u.user_id,
                    CASE WHEN u.user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(u.user_fname, 1)), LOWER(SUBSTRING(u.user_fname, 2)))
                    ELSE user_fname
                    END user_fname,
                    CASE WHEN u.user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(u.user_lname, 1)), LOWER(SUBSTRING(u.user_lname, 2)))
                    ELSE user_lname
                    END user_lname,
                    concat(u.user_fname,' ',u.user_lname)as user_name,
                    u.user_email,u.user_dob,u.user_profile_pic,u.user_gender 
                    FROM gl_user u,gl_friends f
                    WHERE u.user_id = f.friend_user_friend_id
                    AND f.friend_user_id = '$user_id'
                    AND f.freind_relation_status>0
                    AND u.user_status = 'Active'
                    ORDER BY rand()
                    LIMIT $limit
				";
		$result = $this->db->query($sql);
		return $result;
	}
	#return  separated by , friends_id
	function friends_id($user_id,$start=0,$limit=0)
	{
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";
			
		$sql = "SELECT u.user_id 
				FROM gl_user u,gl_friends f
				WHERE u.user_id = f.friend_user_friend_id
				AND f.friend_user_id = '$user_id'
				AND f.freind_relation_status>0
				AND u.user_status = 'Active'
				$limit_query
				";
		$result = $this->db->query($sql);	
		$friends_id = "";
		foreach($result->result() as $row){
			$friends_id .= $row->user_id.",";
		}
		if($friends_id=="")
			$friends_id = 0;
		return rtrim($friends_id,',');
	}
    function get_specific_voucher_details($voucher_id)
	{
		$sql = " SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_purchased_date
				FROM gl_voucher	
				WHERE  voucher_id='".$voucher_id."';
				";		
		return $this->db->query($sql);
	}
	function glomp_buzz($user_id,$start=0,$limit=0)
	{
        
		$friend_list_id = $this->friends_id($user_id);
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";
                
                
		$sql = " SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_purchased_date
				FROM gl_voucher	
				WHERE (voucher_purchaser_user_id IN($friend_list_id)
				OR voucher_belongs_usser_id IN($friend_list_id)
				OR voucher_purchaser_user_id IN($user_id)
				OR voucher_belongs_usser_id IN($user_id) ) AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				ORDER BY voucher_purchased_date DESC
				$limit_query
				";
                
                $params = array(
                    'cache_name' => 'glomp_buzz_'.$user_id.'_'.$start.'_'.$limit, #unique name
                    'cache_table_name' => array('gl_voucher'), #database table
                    'result' => $sql, #the result 
                    'result_type' => 'result', #if from DB result object
                );
                
		return get_create_cache($params);
	}
	function glomp_buzz_specific_user($user_id,$start=0,$limit=0)
	{
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";
		$sql = "SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_purchased_date
				FROM gl_voucher
				WHERE (voucher_purchaser_user_id IN($user_id)
				OR voucher_belongs_usser_id IN($user_id)) AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				ORDER BY voucher_purchased_date DESC
				$limit_query
				";
        $params = array(
            'cache_name' => 'glomp_buzz_specific_user_'.$user_id.'_'.$start.'_'.$limit, #unique name
            'cache_table_name' => array('gl_voucher'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        );
		
		//return $this->db->query($sql);
        return get_create_cache($params);
	}
        function checkEmailExists($email){
		$sql = "SELECT user_id,user_hash_key FROM gl_user WHERE user_email= '$email'";
                $result = $this->db->query($sql);
		return $result;
	}
	function friends_info_buzz($friends_list_id)        
	{        
        
        
        //$friends_list_id=rtrim($friends_list_id, ",");
        
        $user_id=$this->session->userdata('user_id');
		if($friends_list_id=="")
			$friends_list_id = 0;
        
		
		$sql = "SELECT user_id,user_fb_id,user_linkedin_id,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_fname, 1)), LOWER(SUBSTRING(user_fname, 2)))
                    ELSE user_fname
                    END user_fname,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_lname, 1)), LOWER(SUBSTRING(user_lname, 2)))
                    ELSE user_lname
                    END user_lname,
                    user_gender,user_dob,user_status,user_profile_pic
				FROM gl_user
				WHERE 
				 user_id IN($friends_list_id)
				";
		//$result = $this->db->query($sql);
        
        
         $params = array(
                    'cache_name' => 'friends_info_buzz_'.$friends_list_id, #unique name
                    'cache_table_name' => array('gl_user'), #database table
                    'result' => $sql, #the result 
                    'result_type' => 'result', #if from DB result object
                );
                
		$result= get_create_cache($params);
        
		$friends_array = array();
		if($result->num_rows()>0)
		{
			foreach($result->result() as $row)
			{
				$friends_array['friends'][$row->user_id] = array('user_name'=>$row->user_fname.' '.$row->user_lname,
                                                                 'user_fb_id'=>$row->user_fb_id,
                                                                 'user_linkedin_id'=>$row->user_linkedin_id,                                                                 
																 'user_fname'=>$row->user_fname,
																 'user_lname'=>$row->user_lname,
																 'user_gender'=>$row->user_gender,
																 'user_dob'=>$row->user_dob,
																 'user_prifile_pic'=>$row->user_profile_pic,
																 'user_status'=>$row->user_status,
																);
				
			}
			
			return json_encode($friends_array);
			
		}	
	}
	#friends list separated by coma
	function friends_info($friends_list_id)
	{
		if($friends_list_id=="")
			$friends_list_id = 0;
		
		$sql = "SELECT user_id,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_fname, 1)), LOWER(SUBSTRING(user_fname, 2)))
                    ELSE user_fname
                    END user_fname,
                    CASE WHEN user_fname <> 'glomp!' 
                    THEN
                            CONCAT(UPPER(LEFT(user_lname, 1)), LOWER(SUBSTRING(user_lname, 2)))
                    ELSE user_lname
                    END user_lname,
                    user_gender,user_dob,user_status,user_profile_pic,region_id,region_name 
				FROM gl_user,gl_region
				WHERE 
				user_city_id = region_id
				AND user_id IN($friends_list_id)
				";
		$result = $this->db->query($sql);
		$friends_array = array();
		if($result->num_rows()>0)
		{
			foreach($result->result() as $row)
			{
				$friends_array['friends'][$row->user_id] = array('user_name'=>$row->user_fname.' '.$row->user_lname,
																 'user_fname'=>$row->user_fname,
																 'user_lname'=>$row->user_lname,
																 'user_gender'=>$row->user_gender,
																 'user_dob'=>$row->user_dob,
																 'user_prifile_pic'=>$row->user_profile_pic,
																 'user_status'=>$row->user_status,
																 'region_id'=>$row->region_id,
																 'region_name'=>$row->region_name
																);
				
			}
			
			return json_encode($friends_array);
			
		}
		
	}
	function my_glomp_details($user_id=0,$start=0,$limit=0)
	{
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";

		$sql = " SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_status,										
					voucher_type,
					voucher_orginated_by,
					voucher_purchased_date,
					voucher_sender_glomp_message,
					voucher_expiry_date,
					voucher_assigned_date,
					concat(user_fname,' ',user_lname)as user_name,
					user_profile_pic,
					user_gender
				FROM gl_voucher,gl_user	
				WHERE 
				(user_id = voucher_purchaser_user_id
				AND voucher_belongs_usser_id = '$user_id') AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				ORDER BY FIELD(voucher_status, 'Consumable', 'Assigned', 'Redeemed', 'Expired'),  voucher_purchased_date DESC
				$limit_query
				";		
				//ENUM('Consumable','Assigned','Redeemed','Expired')
        $params = array(
            'cache_name' => 'my_glomp_details_'.$user_id.'_'.$start.'_'.$limit, #unique name
            'cache_table_name' => array('gl_voucher','gl_user'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        );                        
		//return $this->db->query($sql);		
        return get_create_cache($params);	
		
	}
	
	
	function glomped_me($user_id=0,$start=0,$limit=0)
	{
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";

		$sql = " SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_status,
					voucher_type,
					voucher_orginated_by,
					voucher_purchased_date,
					voucher_sender_glomp_message,
					concat(user_fname,' ',user_lname)as user_name,
					user_profile_pic,
					user_gender
				FROM gl_voucher,gl_user	
				WHERE 
				(user_id = voucher_purchaser_user_id
				AND voucher_belongs_usser_id = '$user_id') AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				ORDER BY voucher_purchased_date DESC
				$limit_query
				";

                $params = array(
                    'cache_name' => 'glomped_me_'.$user_id, #unique name
                    'cache_table_name' => array('gl_voucher','gl_user'), #database table
                    'result' => $sql, #the result 
                    'result_type' => 'result', #if from DB result object
                );
                
                
		return get_create_cache($params);		
		
	}
function mobile_glomped_me($user_id=0,$start=0,$limit=0)
	{
		if($limit>0)
			$limit_query = "LIMIT $start,$limit";
		else
			$limit_query = "";

		$sql = " SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_merchant_id,
					voucher_product_id,
					voucher_status,
					voucher_code,
					voucher_type,
					voucher_expiry_date,
					voucher_orginated_by,
					voucher_purchased_date,
					voucher_redemption_date,
					voucher_sender_glomp_message,
					concat(user_fname,' ',user_lname)as user_name,
					user_profile_pic,
					user_gender
				FROM gl_voucher,gl_user	
				WHERE 
				(user_id = voucher_purchaser_user_id
				AND voucher_belongs_usser_id = '$user_id'
				AND voucher_status !='Assigned') AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				ORDER BY FIELD(voucher_status, 'Consumable', 'Assigned', 'Redeemed', 'Expired'),  voucher_purchased_date DESC
				$limit_query
				";
		
		return $this->db->query($sql);		
		
	}
function remove_friend($user_id,$friend_id)
{
		
	$sql = "DELETE FROM gl_friends WHERE
			friend_user_id = '$user_id'
			AND friend_user_friend_id = '$friend_id'
			LIMIT 1
			";
		$res = $this->db->query_delete($sql);
		//remove both way
		$sql = "DELETE FROM gl_friends WHERE
			friend_user_id = '$friend_id'
			AND friend_user_friend_id = '$user_id'
			LIMIT 1
			";
		$res = $this->db->query_delete($sql);
        
        
        //log activity [unfriend_a_friend]                                
        $details=array(                    
                'friend_id'=>$friend_id,
                'friend_name'=>$this->userName($friend_id)
                );
        $params = array(
                'user_id'=>$user_id,
                'type' =>'unfriend_a_friend',
                'details' =>json_encode($details)
                );
        add_activity_log($params);
        //log activity [unfriend_a_friend]
        
        $params = array(    
                'affected_tables' 
                    => array(
                        'gl_voucher',
                        'gl_user'
                    ), #cache name
                'specific_names' 
                    => array(
                        'is_friend_'.$user_id.'_'.$friend_id                                                                    
                    )    
                    
            );
        delete_cache($params);
}
function add_friend($user_id,$friend_id)
{
	if(!$this->is_friend($user_id,$friend_id))
	{
	 $sql = "INSERT INTO gl_friends SET
			friend_user_id = '$user_id',
			friend_user_friend_id = '$friend_id',
			friend_request_date = '".date('Y-m-d H:i:s')."',
			freind_relation_status = '2'
			";
            $res = $this->db->query_delete($sql);
            
            
            //log activity [added_a_friend]                                
            $details=array(                    
                    'friend_id'=>$friend_id,
                    'friend_name'=>$this->userName($friend_id)
                    );
            $params = array(
                    'user_id'=>$user_id,
                    'type' =>'added_a_friend',
                    'details' =>json_encode($details)
                    );
            add_activity_log($params);
            //log activity [added_a_friend]
            //log activity [friend_added_you]                                
            $details=array(                    
                    'friend_id'=>$user_id,
                    'friend_name'=>$this->session->userdata('user_name')
                    );
            $params = array(
                    'user_id'=>$friend_id,
                    'type' =>'friend_added_you',
                    'details' =>json_encode($details)
                    );
            add_activity_log($params);
            //log activity [friend_added_you]
            //log activity [friend_added_by_friend] 
            $sql = "Select table1.friend_user_friend_id from 
                        (SELECT friend_user_friend_id FROM gl_friends  WHERE friend_user_id=".$user_id.") as table1
                    join 
                        (SELECT friend_user_friend_id FROM gl_friends  WHERE friend_user_id=".$friend_id.") as table2 
                    ON table1.friend_user_friend_id= table2.friend_user_friend_id;

			";
            $result = $this->db->query($sql);
            foreach($result->result() as $row){                
                $details=array(                    
                        'friend_id'=>$user_id,
                        'friend_name'=>$this->session->userdata('user_name'),
                        'added_friend_id'=>$friend_id,
                        'added_friend_name'=>$this->userName($friend_id)
                        );
                $params = array(
                        'user_id'=>$row->friend_user_friend_id,
                        'type' =>'friend_added_by_friend',
                        'details' =>json_encode($details)
                        );
                add_activity_log($params);
            }
            
            //log activity [friend_added_by_friend]
                
            $this->load->library('email_templating');
            
            $rec_user = $this->user_info_by_id($user_id);
            $rec_touser = $this->user_info_by_id($friend_id);
            
            $args['user_email'] = $rec_touser->row()->user_email;
            $args['type'] = 'add_friend';
            $args['params'] = array(
                'type' => '5',
                'id' => $user_id
            );
            $args['message'] = $rec_user->row()->user_fname.' '.$rec_user->row()->user_lname.' added you as a friend';;
                    
            push_notification($args);
            
            $this->load->model('user_notification_settings_m', 'user_not_sett');
            //TODO: FIND A WAY NOT TO HARDCODE
            // PLEASE SEE NOTICATION ID = 2 ON gl_notifications
            if( ! $this->user_not_sett->is_checked(2, $rec_touser->row()->user_id)) {
                return;
            }
            
             $params = array(    
                'affected_tables' 
                    => array(
                        'gl_voucher',
                        'gl_user'
                    ), #cache name
                'specific_names' 
                    => array(
                        'is_friend_'.$user_id.'_'.$friend_id                                                                    
                    )    
            );
            
            delete_cache($params);
            
            $link = site_url('landing/profileRedirect/'.$user_id);
            $vars = array(
                    'template_name' => 'friend_added_notification',
                    'lang_id' => $rec_user->row()->user_lang_id,
                    'from_name' => $rec_user->row()->user_fname.' '.$rec_user->row()->user_lname,
                    'from_email' => SITE_DEFAULT_SENDER_EMAIL,
                    'reply_to' => $rec_user->row()->user_email,
                    'to' => $rec_touser->row()->user_email,
                    'params' => array(
                        '[member_name]' => $rec_touser->row()->user_fname.' '.$rec_touser->row()->user_lname,
                        '[profile_link]' => "<a href='" . $link . "'>profile</a>",
                        '[friend_name]' => $rec_user->row()->user_fname.' '.$rec_user->row()->user_lname
                    )
            );

            $this->email_templating->config($vars);
            $this->email_templating->set_subject($rec_user->row()->user_fname.' '.$rec_user->row()->user_lname.' has added you as a friend!');

            $this->email_templating->send();
	/*	
	//make accepted by default
	 $sql = "INSERT INTO gl_friends SET
			friend_user_id = '$friend_id',
			friend_user_friend_id = '$user_id',
			friend_request_date = '".date('Y-m-d H:i:s')."',
			freind_relation_status = '2'
			";
		$res = $this->db->query_delete($sql);
		#NOTE: query_delete is a equivalent of query function but it handle the sql constraint error so it has been used for insert data as well so don't be confused
		*/
	}
		
	
}

function is_friend($user_id,$friend_id)
{
	$sql = "SELECT friend_user_friend_id FROM gl_friends 
				WHERE friend_user_id = '".$user_id."'
				AND friend_user_friend_id = '".$friend_id."'
				AND freind_relation_status >= 0
			";
    $params = array(
        'cache_name' => 'is_friend_'.$user_id.'_'.$friend_id, #unique name
        'cache_table_name' => array('gl_friends'), #database table
        'result' => $sql, #the result 
        'result_type' => 'result', #if from DB result object
    );
    //$res = $this->db->query($sql);            
    $res = get_create_cache($params);	
	return $res->num_rows();
}
function is_user($user_id)
{
	$sql = "SELECT user_id FROM gl_user WHERE user_id = '$user_id'";	
	$res = $this->db->query($sql);
	if($res->num_rows($res)>0)
		return true;
	else
		return false;
}
function user_id_by_email($email_adddress)
{
	$sql = "SELECT user_id FROM gl_user WHERE user_email = '$email_adddress'";	
	$res = $this->db->query($sql);
	if($res->num_rows($res)>0)
	{
			$res_user  = $res->row();
			return $res_user->user_id;
	}
	else
		return 0;	
}

///////////////////////////////////////////////////////by PB
/// for seacrhing friends from friend list, it  recieves query string parameters
	function searchOwnFriends($keywords)//searchOwnFriends($subQuery)
	{
		/*$sql = "SELECT u.user_id, u.user_fname,u.user_lname,
				concat(u.user_fname,' ',u.user_lname)as user_name,
				 u.user_city_id,
				 user_profile_pic,user_gender
				FROM gl_user u,gl_friends f
				WHERE u.user_id = f.friend_user_friend_id
				AND f.freind_relation_status>0
				AND u.user_status = 'Active'
				AND f.friend_user_id = '".$this->session->userdata('user_id')."'
				$subQuery
				";*/
	/*select all users with given keywords*/ 
	if($keywords!=""){
		$keywords=trim($keywords);
		$nameArr=explode(" ",$keywords);
		$hasTwoWords=false;
		if(isset($nameArr[1]) && ($nameArr[1]!="")){
			$hasTwoWords=true;
		}
		$sqlDefault="SELECT u.user_id, u.user_fname,u.user_lname,
				concat(u.user_fname,' ',u.user_lname)as user_name,
				 u.user_city_id,
				 user_profile_pic,user_gender
				FROM gl_user u
				WHERE  (u.user_status = 'Active' OR u.user_status = 'Pending' )
				AND u.user_id <> '".$this->session->userdata('user_id')."'";
		if($hasTwoWords)
		{
			$sql="";
			if($sql!="")
					$sql .= " UNION ";
					
			$sql .= $sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '$keywords%'  )
			";//ORDER BY user_fname ASC";
			
			$sql .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname) LIKE '$keywords%' )
			";//ORDER BY user_fname ASC";
			$ctr=0;
			while(array_key_exists($ctr, $nameArr))
			{
				if($sql!="")
					$sql .= " UNION ";				
				$sql .= " ".$sqlDefault.
				" AND ( (user_fname) LIKE '".$nameArr[$ctr]."%' )  
				";//ORDER BY user_lname ASC";
				
				$sql .= " UNION ".$sqlDefault.
				" AND ( (user_lname) LIKE '".$nameArr[$ctr]."%' )  
				";//ORDER BY user_lname ASC";
				
				$sql .= " UNION ".$sqlDefault.
				" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."' )  
				";//ORDER BY user_lname ASC";
				
				$sql .= " UNION ".$sqlDefault.
				" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."' )  
				";//ORDER BY user_lname ASC";
				
				$sql .= " UNION ".$sqlDefault.
				" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."%' )  
				";//ORDER BY user_lname ASC";
				
				$sql .= " UNION ".$sqlDefault.
				" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."%' )  
				";//ORDER BY user_lname ASC";
				
				$ctr++;
			
			}
			$sql .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '$keywords%' )  
			";//ORDER BY user_lname ASC";
			
			$sql .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%'  )
			";//ORDER BY user_fname ASC";
			
			$sql .= " UNION ".$sqlDefault.
			" AND ( user_fname LIKE '%$keywords%' )
			";//ORDER BY user_fname ASC";
			
			$sql .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '%$keywords%' )  
			";//ORDER BY user_lname ASC";
		
		}
		else{
			$sql = "SELECT u.user_id, u.user_fname,u.user_lname,
				concat(u.user_fname,' ',u.user_lname)as user_name,
				 u.user_city_id,
				 user_profile_pic,user_gender
				FROM gl_user u
				WHERE  (u.user_status = 'Active' OR u.user_status = 'Pending' )
				AND u.user_id <> '".$this->session->userdata('user_id')."'
				AND (user_fname LIKE '%$keywords%' OR user_Lname LIKE '%$keywords%' OR CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%')
				";
		
		}	
		

	}
	else{
		$sql = "SELECT u.user_id, u.user_fname,u.user_lname,
				concat(u.user_fname,' ',u.user_lname)as user_name,
				 u.user_city_id,
				 user_profile_pic,user_gender
				FROM gl_user u,gl_friends f
				WHERE u.user_id = f.friend_user_friend_id
				AND f.freind_relation_status>0
				AND (u.user_status = 'Active' OR u.user_status = 'Pending'  )
				AND f.friend_user_id = '".$this->session->userdata('user_id')."'";
	}				
	/*get all mutual*/ 
		$rows="";
		$result = $this->db->query($sql);	
		foreach($result->result() as $row){
				$q="SELECT count(*) As mutual FROM gl_friends AS f
					INNER JOIN gl_friends AS mf ON f.friend_user_friend_id = mf.friend_user_friend_id
					WHERE f.friend_user_id = '".$this->session->userdata('user_id')."'
					   AND f.freind_relation_status = 2
					   AND mf.friend_user_id = '".$row->user_id."'	
				";
				$d= $this->db->query($q);
				$res = $d->row();
				
				
				
			$rows[]= (object) array(
			'friend_data' => $row,
			'mutual' => $res->mutual,
			'is_friend'=>($this->custom_func->checkIfFriend($this->session->userdata('user_id'),$row->user_id))
			);
		}
		//return $result = $this->db->query($sql);	
		return $rows;
		
	}
        /**
         * searchOwnFriends_api
         * 
         * same searchOwnFriends function, just an API version
         * 
         * @access public
         * @param string
         * @param string
         * @return object array
         */
	function searchOwnFriends_api($keywords, $user_id, $limit = 10, $offset = 0)
	{
            /*select all users with given keywords*/ 
            if($keywords!=""){
                    $keywords=trim($keywords);
                    $nameArr=explode(" ",$keywords);
                    $hasTwoWords=false;
                    if(isset($nameArr[1]) && ($nameArr[1]!="")){
                            $hasTwoWords=true;
                    }
                    $sqlDefault="SELECT u.user_id, u.user_fname,u.user_lname,
                                    concat(u.user_fname,' ',u.user_lname)as user_name,
                                     u.user_city_id,
                                     u.user_dob,
                                     u.user_dob_display,
                                     user_profile_pic,user_gender
                                    FROM gl_user u
                                    WHERE  (u.user_status = 'Active' OR u.user_status = 'Pending' )
                                    AND u.user_id <> '".$user_id."'";
                    if($hasTwoWords)
                    {
                            $sql="";
                            if($sql!="")
                                            $sql .= " UNION ";

                            $sql .= $sqlDefault.
                            " AND ( CONCAT(user_fname,' ',user_lname) LIKE '$keywords%'  )
                            ";//ORDER BY user_fname ASC";

                            $sql .= " UNION ".$sqlDefault.
                            " AND ( CONCAT(user_fname) LIKE '$keywords%' )
                            ";//ORDER BY user_fname ASC";
                            $ctr=0;
                            while(array_key_exists($ctr, $nameArr))
                            {
                                    if($sql!="")
                                            $sql .= " UNION ";				
                                    $sql .= " ".$sqlDefault.
                                    " AND ( (user_fname) LIKE '".$nameArr[$ctr]."%' )  
                                    ";//ORDER BY user_lname ASC";

                                    $sql .= " UNION ".$sqlDefault.
                                    " AND ( (user_lname) LIKE '".$nameArr[$ctr]."%' )  
                                    ";//ORDER BY user_lname ASC";

                                    $sql .= " UNION ".$sqlDefault.
                                    " AND ( (user_fname) LIKE '%".$nameArr[$ctr]."' )  
                                    ";//ORDER BY user_lname ASC";

                                    $sql .= " UNION ".$sqlDefault.
                                    " AND ( (user_lname) LIKE '%".$nameArr[$ctr]."' )  
                                    ";//ORDER BY user_lname ASC";

                                    $sql .= " UNION ".$sqlDefault.
                                    " AND ( (user_fname) LIKE '%".$nameArr[$ctr]."%' )  
                                    ";//ORDER BY user_lname ASC";

                                    $sql .= " UNION ".$sqlDefault.
                                    " AND ( (user_lname) LIKE '%".$nameArr[$ctr]."%' )  
                                    ";//ORDER BY user_lname ASC";

                                    $ctr++;

                            }
                            $sql .= " UNION ".$sqlDefault.
                            " AND ( user_lname LIKE '$keywords%' )  
                            ";//ORDER BY user_lname ASC";

                            $sql .= " UNION ".$sqlDefault.
                            " AND ( CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%'  )
                            ";//ORDER BY user_fname ASC";

                            $sql .= " UNION ".$sqlDefault.
                            " AND ( user_fname LIKE '%$keywords%' )
                            ";//ORDER BY user_fname ASC";

                            $sql .= " UNION ".$sqlDefault.
                            " AND ( user_lname LIKE '%$keywords%' )  
                            ";//ORDER BY user_lname ASC";

                    }
                    else{
                            $sql = "SELECT u.user_id, u.user_fname,u.user_lname,
                                    concat(u.user_fname,' ',u.user_lname)as user_name,
                                     u.user_city_id,
                                     u.user_dob,
                                     u.user_dob_display,
                                     user_profile_pic,user_gender
                                    FROM gl_user u
                                    WHERE  (u.user_status = 'Active' OR u.user_status = 'Pending' )
                                    AND u.user_id <> '".$user_id."'
                                    AND (user_fname LIKE '%$keywords%' OR user_Lname LIKE '%$keywords%' OR CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%')
                                    ";

                    }	


            }
            else{
                    $sql = "SELECT u.user_id, u.user_fname,u.user_lname,
                                    concat(u.user_fname,' ',u.user_lname)as user_name,
                                     u.user_city_id,
                                     u.user_dob,
                                     u.user_dob_display,
                                     user_profile_pic,user_gender
                                    FROM gl_user u,gl_friends f
                                    WHERE u.user_id = f.friend_user_friend_id
                                    AND f.freind_relation_status>0
                                    AND (u.user_status = 'Active' OR u.user_status = 'Pending'  )
                                    AND f.friend_user_id = '".$user_id."'";
            }				
            /*get all mutual*/ 
            $sql .= " LIMIT $offset, $limit";
            $rows="";
            $result = $this->db->query($sql);	
            foreach($result->result() as $row){
                            $q="SELECT count(*) As mutual FROM gl_friends AS f
                                    INNER JOIN gl_friends AS mf ON f.friend_user_friend_id = mf.friend_user_friend_id
                                    WHERE f.friend_user_id = '".$this->session->userdata('user_id')."'
                                       AND f.freind_relation_status = 2
                                       AND mf.friend_user_id = '".$row->user_id."'	
                            ";
                            $d= $this->db->query($q);
                            $res = $d->row();



                    $rows[]= (object) array(
                    'friend_data' => $row,
                    'mutual' => $res->mutual,
                    'is_friend'=>($this->custom_func->checkIfFriend($user_id, $row->user_id))
                    );
            }
            return $rows;
	}
	
/// for seacrhing friends from friend list, it  recieves query string parameters
	function searchForFriends($name="",$email="",$location_id="0")
	{
		$sqlMain="";
		$sqlDefault="SELECT user_id, user_fname, user_lname, user_city_id, user_profile_pic, user_gender , region_parent_path_id FROM gl_user, gl_region
				WHERE 
				( user_status = 'Active' OR user_status = 'Pending')				
				AND user_id!='".$this->session->userdata('user_id')."'				
				AND region_id = user_city_id ";
		
		//get path if location is entered
		if ($location_id > 0) {
			$sql = "SELECT region_parent_path_id FROM gl_region WHERE region_id='" . $location_id . "'";
            $result = $this->db->query($sql);
            $recPath = $result->row();
            $path = $recPath->region_parent_path_id;            
		
		}
		$name=trim($name);
		$nameArr=explode(" ",$name);
		$hasTwoWords=false;
		if(isset($nameArr[1]) && ($nameArr[1]!="")){
			$hasTwoWords=true;
		}
		
		//1st query query with namem email and location
		if ($name != "" && $email != "" && $location_id > 0)
		{
			
			
			
			$sqlMain .= $sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname) LIKE '$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_fname ASC";
			
			if($hasTwoWords)
			{
				$ctr=0;
				while(array_key_exists($ctr, $nameArr))
				{
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '".$nameArr[$ctr]."%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '".$nameArr[$ctr]."%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";					
					
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";					
					
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
					";//ORDER BY user_fname ASC";					
					$ctr++;
				}			
			}
			
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_lname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '%$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_fname LIKE '%$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '%$name%' AND user_email = '$email' AND region_parent_path_id LIKE '$path%' )  
			";//ORDER BY user_lname ASC";
		}		
		
		//2th query by name
		if ($name != "")
		{
			if($sqlMain!="")
					$sqlMain .= " UNION ";
					
			$sqlMain .= $sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '$name%'  )
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname) LIKE '$name%' )
			";//ORDER BY user_fname ASC";
			
			
			if($hasTwoWords)
			{
				$ctr=0;
				while(array_key_exists($ctr, $nameArr))
				{
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '".$nameArr[$ctr]."%' )  
					";//ORDER BY user_lname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '".$nameArr[$ctr]."%' )  
					";//ORDER BY user_lname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."' )  
					";//ORDER BY user_lname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."' )  
					";//ORDER BY user_lname ASC";
					
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_fname) LIKE '%".$nameArr[$ctr]."%' )  
					";//ORDER BY user_lname ASC";
					
					$sqlMain .= " UNION ".$sqlDefault.
					" AND ( (user_lname) LIKE '%".$nameArr[$ctr]."%' )  
					";//ORDER BY user_lname ASC";
					
					$ctr++;
				
				}
			
			}
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '$name%' )  
			";//ORDER BY user_lname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( CONCAT(user_fname,' ',user_lname) LIKE '%$name%'  )
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_fname LIKE '%$name%' )
			";//ORDER BY user_fname ASC";
			
			$sqlMain .= " UNION ".$sqlDefault.
			" AND ( user_lname LIKE '%$name%' )  
			";//ORDER BY user_lname ASC";
		}		
		if($sqlMain!="")
		{
			$sqlMain.=' LIMIT 50';
			return $result = $this->db->query($sqlMain);	
		}
		else
			return "";
		
	}
	function email_exist($email)
	{
		$whr = array('user_email'=>$email);
		$res = $this->db->get_where('gl_user',$whr);
		return $res;
	}
	
	
	function email_exist2($email)
	{
		$sql = "SELECT *
		FROM gl_user
		WHERE 
		user_status = 'Active'
		AND user_id NOT IN (SELECT friend_user_friend_id FROM gl_friends WHERE friend_user_id= '".$this->session->userdata('user_id')."') 
		AND user_id!='".$this->session->userdata('user_id')."'
		AND user_email='$email'
		
		";
		return $result = $this->db->query($sql);	
		
	}
    function tabZero($user_id){
        $sql = "UPDATE gl_user SET user_tab_open='0' WHERE  user_id = '$user_id'";
        $result = $this->db->query($sql);	
    }
    function tabOpen($user_id,$plus)
    {
        $sql = "SELECT user_tab_open FROM gl_user
		WHERE 
		user_id = '$user_id'";
        $result = $this->db->query($sql);	
        $data= $result->row();
        $tab=0;
        if ($result->num_rows() > 0) {
            $tab= (int )$data->user_tab_open;    
        }
        
        
        if($plus=='open')
            $tab++;
        else
            $tab--;
            
        $sql = "UPDATE gl_user SET user_tab_open='".$tab."' WHERE 
		user_id = '$user_id'";
        $result = $this->db->query($sql);	
    }
    function count_unread_messages($user_id)
    {
        $sql = "SELECT message_id num_unread FROM  gl_messages 
				WHERE
				user_from <> '$user_id'
                AND user_to = '$user_id'
				AND status ='Active'
                AND date_viewed ='0000-00-00 00:00:00'                
                GROUP BY room_number
				";
		$res = $this->db->query($sql);
		$num_unread = $res->num_rows();
		
		return $num_unread;
    }
	
	function count_active_voucher($user_id)
	{
		$sql = "SELECT COUNT(*)AS num_active, voucher_id FROM  gl_voucher
				WHERE
				(voucher_belongs_usser_id = '$user_id'
				AND voucher_status IN('Consumable','Assigned')) AND voucher_id NOT IN (SELECT
                                    voucher_id
                                FROM gl_voucher_brand_group_list)
				";
		$res = $this->db->query($sql);
		$rec = $res->row();
		
		return $rec->num_active;	
	}
	
	function customFormat($date)
	{
		$sql = "SELECT DATE_FORMAT('$date','%H:%i %m/%d/%Y') AS new_date";
		$res = mysql_query($sql);
		$res_date = mysql_fetch_array($res);
		return $res_date['new_date'];
	}

    function check_if_18($user_id)
    {

        $user_dob = $this->db->get_where('gl_user', array(
            'user_id' => $user_id
        ));
        $cur_date = date('Y-m-d');
        $entered_date = $user_dob->row()->user_dob;
        $sql = "SELECT DATEDIFF('$cur_date','$entered_date') AS future_date";
        $res = $this->db->query($sql);
        $rec = $res->row();
        $future = $rec->future_date;

        if($future < (365*18))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
        
    }

}//eoc
?>