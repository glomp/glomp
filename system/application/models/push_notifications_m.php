<?php 
/*
 * This model class is for managing push notifications
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @parent class super_model.php (Extends CI_Model)
 * @depenencies none             
 * @version     1.0
 */
require_once('super_model.php');
class Push_notifications_m extends Super_model {
    
    function __construct() {
        parent::__construct('gl_push_notification push_notification');
    }
}