<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand_m extends CI_Model {

    public static $db = array(
        'int' => "int(11) NOT NULL AUTO_INCREMENT",
        'status' => "enum('Active','Inactive','Deleted') DEFAULT 'Active'",
        'created' => "datetime DEFAULT NULL",
        'modified' => "datetime DEFAULT NULL",
        'created_by' => "int(11) DEFAULT NULL",
        'modified_by' => "int(11) DEFAULT NULL",
        'name' => "varchar(255) DEFAULT NULL",
        'description' => "text",
        'logo' => "varchar(255) DEFAULT NULL",
        'thumbnail' => "varchar(255) DEFAULT NULL",
        'banner' => "varchar(255) DEFAULT NULL",
    );

    public static $many_many = array(
        'Products' => 'Products'
    );


    public $insert_id; // last insert id

    public function __construct() {
        parent::__construct();
    }

    public function save( $data, $id = 0 ) {
        $now = gmdate('Y-m-d H:i:s');
        $data['modified'] = $now;


        if( $id ) {
            // update

            if( $this->db->update( 'gl_brand', $data, 'id = ' . $id ) ) {

                return TRUE;
            } else {

                log_message( 'error', $now . ' An error occured updating a Brand('.$id.').' );
                exit($now . ' An error occured updating a Brand('.$id.').');
            }
        } else {
            // insert

            $data['created'] = $now;
            if( $this->db->insert( 'gl_brand', $data ) ) {

                return $this->db->insert_id();
            } else {

                log_message( 'error', $now . ' An error occured saving the new Brand.' );
                exit($now . ' An error occured saving the new Brand.');
            }
        }

        return FALSE;
    }

    public function delete( $id ) {

        return $this->save( array(
            'status' => 'Deleted'
        ), $id );
    }

    public function get_all() {

        $this->db->limit(10);
        $this->db->order_by( 'id', 'desc' );
        $q = $this->db->get( 'gl_brand' );
        if( $q ) {

        }
        return $q->result();
    }

    public function get_all_active() {
        $this->db->order_by( 'name', 'asc' );
        $this->db->where(array('status'=>'Active'));
        $q = $this->db->get( 'gl_brand' );
        if( $q->num_rows() > 0 ) {
            return $q->result();
        }
        return false;
    }
	
    public function get_by_public_alias( $public_alias ) {

        $q = $this->db->get_where( 'gl_brand', "public_alias = '".$public_alias."' AND type='public' " );
        if( $q->num_rows() ) {
            return $q->row();
        }
        return FALSE;
    }
	
	public function get_by_id( $id ) {

        $q = $this->db->get_where( 'gl_brand', 'id = '.$id );
        if( $q->num_rows() ) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_template( $brand_id ) {

        $q = $this->db->get_where( 'gl_template', array('type'=>'brand','referer_id'=>(int) $brand_id) );

        if( $q->num_rows() ) {
            return $q->row();
        }

        return FALSE;
    }

    public function create_template( $brand_id ) {
        $now = gmdate('Y-m-d H:i:s');

        $this->db->insert( 'gl_template', array(
            'created' => $now,
            'modified' => $now,
            'created_by' => $this->session->userdata('user_id'),
            'modified_by' => $this->session->userdata('user_id'),
            'status' => 'Active',
            'referer_id' => $brand_id,
            'sort_order' => 0,
            'type' => 'brand'
        ) );

        return $this->db->insert_id();
    }

    /**
     * Returns a template
     * creates if founf nothing
     *
     * @param $id Int Brand ID
     */
    public function get_or_create_template( $brand_id ) {

        $template = $this->get_template( $brand_id );

        if( !$template ) {
            $template_id = $this->create_template( $brand_id );
        }

        return $this->get_template( $brand_id );
    }

    public function get_by_merchant( $merchant_id ) {

    }

    public function get_merchants( $brand_id ) {

    }

    public function get_brandproducts( $brand_id, $limit = 0 , $is_random=true) {
        
        if( $limit )
            $this->db->limit( $limit );
        if($is_random)
            $this->db->order_by('id','random');
        else
            $this->db->order_by('id','asc');

        $q = $this->db->get_where( 'gl_brandproduct', array ('brand_id' => $brand_id ) );
        
        if( $q->num_rows() > 0 ) {
            return $q->result();
        }

        return false;
    }

    public function get_brandproduct( $id ) {
        
        $q = $this->db->get_where( 'gl_brandproduct', array ('id' => $id ) );
        
        if( $q->num_rows() > 0 ) {
            return $q->row();
        }

        return false;
    }
    public function get_brandproduct_products( $id ) {
        
        $q = $this->db->get_where( 'gl_brands_products', array ('id' => $id ) );
        
        if( $q->num_rows() > 0 ) {
            return $q->row();
        }

        return false;
    }

    public function add_product( $brand_id, $product_id ) {
        $this->db->insert('gl_brands_products',array('brand_id'=>$brand_id,'product_id',$product_id));
    }

    public function delete_product( $brand_id, $product_id ) {
        $this->db->where(array('brand_id'=>$brand_id,'product_id',$product_id));
        $this->db->delete('gl_brands_products');
    }
    
    public function save_brandproduct( $data ) {
        
        if( isset( $data['id'] ) ) {
            
            $id = $data['id'];
            unset($data['id']);
            
            $this->db->update('gl_brandproduct', $data, "id = $id" );
        } else {

            $this->db->insert('gl_brandproduct', $data);
            $id = $this->db->insert_id();
        }
        
        return $id;
    }
}
