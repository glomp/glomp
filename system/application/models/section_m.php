<?php

/*
 * Sections Model Class
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Section_m extends Super_model {

    function __construct() {
        parent::__construct('gl_section section');
    }

}