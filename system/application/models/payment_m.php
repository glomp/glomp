<?php 
#written by shree
#Date Aug 4, 2013
	
class Payment_m extends CI_Model {
function check_payment_if_received($user_id,$session_id,$token,$amount)
{
    $sql = "SELECT session_status FROM gl_payment_session WHERE
                sesion_payment_id = '$session_id' AND
                session_uer_id = '$user_id' AND
                session_status ='Completed'";
    return $res = $this->db->query($sql);
}
function update_session_token($user_id,$session_id,$token){    
    $sql = "UPDATE gl_payment_session SET
				session_token_id = '$token'
               WHERE session_uer_id='$user_id' AND sesion_payment_id='$session_id' LIMIT 1";    
    $this->db->query($sql);
}
function get_session_id($user_id,$token){
    $sql = "SELECT sesion_payment_id,sesion_buying_point,session_amount FROM gl_payment_session WHERE session_uer_id='$user_id' AND session_token_id='$token' LIMIT 1";    
    return $res = $this->db->query($sql);
}
function checkout_payment_session($user_id,$point,$amount,$session_payment_visisted_id,$payment_method_id=1)
{
    $res = $this->db->get_where('gl_package_point', array('package_point' => $point));
    
    //Verify point and amount
    if ($res->num_rows() > 0) {
        $point = $res->row()->package_point;
        $amount = $res->row()->package_price;
    } else {
        return FALSE;
    }
    
    $sql = "UPDATE gl_payment_session SET
            sesion_buying_point = '$point',
            session_amount = '$amount',
            session_status = 'Checkout'
            WHERE sesion_payment_id = '$session_payment_visisted_id'
    ";
    $this->db->query($sql);
    return array('session_payment_id'=>$session_payment_visisted_id,'session_amount'=>$amount);
		
	
}//
function create_payment_session_visited($user_id,$payment_method_id=1)
{
	$date = date('Y-m-d H:i:s');
	$uuid = $this->mysql_uuid();
	$sql = "INSERT INTO gl_payment_session SET
				sesion_payment_id = '$uuid',
				session_uer_id = '$user_id',
				session_date = '$date',
				session_payment_method_id = '$payment_method_id',
				session_status = 'Visited'
			";
		$this->db->query($sql);
		return $uuid;
		
	
}//
function mysql_uuid()
{
	$sql = "select UUID() as uuid";	
	$res = $this->db->query($sql);
	return $res->row()->uuid;
}

    function selectPoints()
    {
        $sql = "SELECT * FROM gl_package_point WHERE package_status='Active' ORDER BY package_point ASC ";
        return $res = $this->db->query($sql);
    }
    function getPointDetails($package_id)
    {
        $sql = "SELECT * FROM gl_package_point WHERE package_status='Active' AND package_id='".$package_id."' ORDER BY package_point ASC ";
        return $res = $this->db->query($sql);
    }	
}//eoc
?>