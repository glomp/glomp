<?php
class Template_m extends CI_Model {

    public function __construct() {
    
        parent::__construct();
    }
    
    /**
     * Returns Sections of a template
     *
     *
     * @param $template_id Integer TemplateID
     */
    public function _get_sections( $template_id ) {
        $this->db->order_by('sort_order','asc');
        $sections = $this->db->get_where( 'gl_section', array('template_id'=>$template_id) );

        if( $sections->num_rows() > 0 ) {
            return $sections->result();
        }

        return FALSE;
    }
    
    /**
     * Returns Sections of a template
     *
     * @param $sections Array
     */
    public function _get_widgets( $sections ) {
        $widgets = FALSE;
        if( is_array($sections) > 0 ) {
            foreach( $sections as $section ) {
                $section_id = $section->id;
                $this->db->order_by('position');
                $query = $this->db->get_where('gl_widget',array('section_id'=>$section_id));
                if( $query->num_rows() > 0 ) {
                    foreach( $query->result() as $widget ) {
                        $widget->data = $this->_get_widget_data( $widget->id, $widget->widget_type );
                        $widgets[$section->id][] = $widget;
                    }
                }
            }
        }
        return $widgets;
    }

    /**
     * Returns Widget data based on widget type
     *
     * @param $widget_id Integer
     */
    private function _get_widget_data( $widget_id, $type ) {

        $q = $this->db->get_where( 'gl_widget_'.$type, array('widget_id'=>$widget_id) );
        if( $q->num_rows() > 0 ) {
            return $q->row();
        }
        return FALSE;
    }
}