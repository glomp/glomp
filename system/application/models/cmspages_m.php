<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Lomas
Date: May-23-2013
*/
class Cmspages_m extends CI_Model
{
	protected $CMS_INSTANCE="gl_cms";
	protected $CONTENT_INSTANCE="gl_cms_content";
	protected $MENU_INSTANCE="gl_cms_menu";
	protected $LANG_INSTANCE="gl_language";
	
	function selectCmspages($start=0,$per_page=0,$lang_id=0)
	{
		$lang_query="";
		$limit_query="";
		
		if($lang_id>0)
		$lang_query=" and lang_id='$lang_id'";
		
		if($per_page>0)
		$limit_query=" limit $start, $per_page";
		
		$sql="select * from $this->CMS_INSTANCE,$this->CONTENT_INSTANCE where content_cms_id=cms_id and cms_deleted='N' $lang_query order by cms_menu_id,cms_display_order $limit_query";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function UpdateDisplayOrder($order,$id)
	{
		$data=array(
		'cms_display_order'=>$order
		);
		
		$where=array(
		'cms_id'=>$id
		);
		
		$this->db->update($this->CMS_INSTANCE,$data,$where);
	}
	
	function selectCmspageByID($contentID=0,$lang_id=0)
	{
		$lang_query="";
		if($lang_id>0)
		$lang_query=" and lang_id='$lang_id'";
		
		$sql="select * from $this->CMS_INSTANCE,$this->CONTENT_INSTANCE where content_cms_id=cms_id and cms_deleted='N' and cms_id='$contentID' $lang_query";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function cmsMenu($selected_id=0)
	{
		$q = $this->db->get_where($this->MENU_INSTANCE,array('menu_active'=>'Y'));
		
		$list ="";
		if($q->num_rows > 0)
		{
			foreach($q->result() as $r)
			{
				$selected = ($r->menu_id == $selected_id)?"selected='selected'":"";	
				$list .="<option value='".$r->menu_id."' $selected>".$r->menu_name."</option>";
			}
		}
		return $list;
	}
	
	function cmsMenuNameByID($contentID=0)
	{
		$menu=$this->db->get_where($this->MENU_INSTANCE,array('menu_id'=>$contentID))->row();
		if($menu)
		return $menu->menu_name;
		else
		return "";
	}
	
	function cmsLanguage($selected_id=0)
	{
		$q = $this->db->get_where($this->LANG_INSTANCE,array('lang_active'=>'Y'));
		
		$list ="";
		if($q->num_rows > 0)
		{
			foreach($q->result() as $r)
			{
				$selected = ($r->lang_id == $selected_id)?"selected='selected'":"";	
				$list .="<option value='".$r->lang_id."' $selected>".$r->lang_name."</option>";
			}
		}
		return $list;
	}
	
	function publish($contentID,$contentStatus)
	{
		$data=array(
		'cms_active'=>$contentStatus
		);
		
		$where=array(
		'cms_id'=>$contentID
		);
		
		$this->db->update($this->CMS_INSTANCE,$data,$where);
	}
	
	function delete($contentID)
	{
		$data=array(
		'cms_deleted'=>'Y'
		);
		
		$where=array(
		'cms_id'=>$contentID
		);
		
		$page=$this->cmspages_m->selectCmspageByID($contentID,DEFAULT_LANG_ID)->row();
		if($page->cms_delete_allow=="Y")
		{
			$this->db->update($this->CMS_INSTANCE,$data,$where);
			return 'TRUE';
		}
		else
		{
			return 'FALSE';
		}
	}
	
	function addPage()
	{
		$data=array(
		'cms_parent_id'=>'0',
		'cms_display_order'=>'0',
		'cms_active'=>'Y',
		'cms_added'=>date('Y-m-d'),
		'cms_deleted'=>'N',
		'cms_subpage_allow'=>'N',
		'cms_delete_allow'=>'Y',
		'cms_menu_id'=>$this->input->post('menu_id')
		);
		$this->db->insert($this->CMS_INSTANCE,$data);
		$inserted_id=$this->db->insert_id();
		
		$lang=$this->db->get($this->LANG_INSTANCE)->result();
		foreach($lang as $language)
		{
			$content=array(
			'content_cms_id'=>$inserted_id,
			'content_title'=>$this->input->post('page_title'),
			'content_keywords'=>$this->input->post('keyword'),
			'content_desc'=>$this->input->post('description'),
			'content_name'=>$this->input->post('page_name'),
			'content_content'=>$this->input->post('contents'),
			'content_date'=>date('Y-m-d'),
			'lang_id'=>$language->lang_id,
			);
			$this->db->insert($this->CONTENT_INSTANCE,$content);
		}
	}
	
	function editPage($contentID=0)
	{
		$data=array(
		'cms_menu_id'=>$this->input->post('menu_id')
		);
		
		$where=array(
		'cms_id'=>$contentID
		);
		
		$this->db->update($this->CMS_INSTANCE,$data,$where);
		
		$content=array(
		'content_cms_id'=>$contentID,
		'content_title'=>$this->input->post('page_title'),
		'content_keywords'=>$this->input->post('keyword'),
		'content_desc'=>$this->input->post('description'),
		'content_name'=>$this->input->post('page_name'),
		'content_content'=>$this->input->post('contents'),
		'content_date'=>date('Y-m-d'),
		'lang_id'=>$this->input->post('lang_id'),
		);
		
		$content_where=array(
		'content_cms_id'=>$contentID,
		'lang_id'=>$this->input->post('lang_id')
		);		
		$this->db->update($this->CONTENT_INSTANCE,$content,$content_where);
	}
function selectStaticPage($cms_id,$lang_id)
{
	return $this->selectCmspageByID($cms_id,$lang_id);
}

function selectCmsPageNotByID($contentID=0,$lang_id=0)
	{
		$lang_query="";
		if($lang_id>0)
		$lang_query=" and lang_id='$lang_id'";
		
		$sql="select * from $this->CMS_INSTANCE,$this->CONTENT_INSTANCE where content_cms_id=cms_id and cms_deleted='N' and cms_id !='$contentID' AND cms_menu_id='1' $lang_query";
		$q=$this->db->query($sql);
		return $q;
	}

}
?>