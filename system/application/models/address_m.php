<?php 
class Address_m extends CI_Model {

	private $INSTANCE="";	
	function address_format($address1='', $address2='', $street='', $locality='', $city_town='', $region_city='', $postal_code='', $country=''){			
		$country=strtoupper($country);
		$res= (object) array(
			'order' => '1,2,3',
			'address_data' => '1,2,3',
			'address_formatted' => 'sample<br>sample');
			
		$address_data=(object) array(
			'address1' 		=> $address1,
			'address2' 		=> $address2,
			'street' 		=> $street,
			'locality' 		=> $locality,
			'city_town' 	=> $city_town,
			'region_city' 	=> $region_city,
			'postal_code' 	=> $postal_code,
			'country' 		=> $country
		);
		$formatted="";				
		switch($country){
			case 'SINGAPORE':
				$address_data->city_town=strtoupper($address_data->city_town);								
				$formatted=$address_data->address1.'<br>';
				if($address_data->street!='')
					$formatted.=$address_data->street.'<br>';
				if($address_data->city_town!='')
					$formatted.=$address_data->city_town.'<br>';
				if($address_data->country!='')
					$formatted.=$address_data->country;			
				if($address_data->postal_code!='')
					$formatted.=' '.$address_data->postal_code;
			break;
			case 'HONG KONG':			
			default:
				$address_data->city_town=strtoupper($address_data->city_town);								
				$formatted=$address_data->address1.'<br>';
				if($address_data->street!='')
					$formatted.=$address_data->street.'<br>';
				if($address_data->city_town!='')
					$formatted.=$address_data->city_town.'<br>';
				if($address_data->country!='')
					$formatted.=$address_data->country;
				if($address_data->postal_code!='')
					$formatted.=' '.$address_data->postal_code;				
			break;		
		}
		$res->address_formatted=$formatted;
		return ($res);    
	}	
}//eoc
?>