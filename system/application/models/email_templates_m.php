<?php

/*
 * Email Templates Model Class
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 *              
 */
require_once('super_model.php');

class Email_templates_m extends Super_model {

    function __construct() {
        parent::__construct('gl_email_templates et');
    }

}