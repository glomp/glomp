<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-26-2013
*/
class Regions_m extends CI_Model
{
	protected $INSTACNE_NAME="gl_region";
    protected $INSTACNE_CODES="gl_region_oulet_codes";
	
	function selectRegions($parent_zone=0,$start=0,$per_page=0,$lang_id=0,$search_q="")
	{
		$limit_query="";
        $sql_search_q="";
        if($search_q!="")
        {
         $sql_search_q="(region_name LIKE '%".$search_q."%') AND ";
        }
		
		if($parent_zone>0)
		$parent_query=" and region_parent_id='$parent_zone'";
		else
		$parent_query=" and (region_parent_id=0 || region_parent_id=NULL)";
		
		if($per_page>0)
		$limit_query=" limit $start, $per_page";		
		
        $sql="select * from $this->INSTACNE_NAME WHERE $sql_search_q 1=1 $parent_query  order by region_name $limit_query";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function TotalSubRegion($ContentID=0)
	{
		$where=array(
		'region_parent_id'=>$ContentID
		);
		$q=$this->db->get_where($this->INSTACNE_NAME,$where);
		return $q->num_rows();
	}
	
	function selectRegionByID($contentID=0)
	{
		
		$sql="select * from $this->INSTACNE_NAME where region_id='$contentID'";
		$q=$this->db->query($sql);
		return $q;
	}
	
	
	function selectRegionKeywords($keyWords)
	{
		$keyWords = urldecode($keyWords);
		$sql="select * from $this->INSTACNE_NAME WHERE region_name LIKE '%$keyWords%' 
				AND  region_status = 'Active'
				AND region_selectable = 'Y'
				ORDER BY region_name ASC LIMIT 10";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function selectRegionKeywords1($keyWords)
	{
		
		$sql="select * from $this->INSTACNE_NAME WHERE region_name = '$keyWords' 
				AND  region_status = 'Active'
				AND region_selectable = 'Y'
				ORDER BY region_name ASC LIMIT 10";
		$q=$this->db->query($sql);
		return $q;
	}
	
	function selectRegionNameByID($contentID=0)
	{
		
		$sql="select * from $this->INSTACNE_NAME WHERE  region_id='$contentID'";
        $params = array(
            'cache_name' => 'selectRegionNameByID_'.$contentID, #unique name
            'cache_table_name' => array('gl_region'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        );        
        //$q=$this->db->query($sql);
        $q = get_create_cache($params);		
		if($q->num_rows()>0)
		{
			$r = $q->row();
			return $r->region_name;
		}
		else
		return $this->lang->line('Unavilable');
	}
	
	function parentRegion($selected_id=0)
	{
		if($lang_id>0)
		$lang_query=" and lang_id=$lang_id";
		
		$sql="select * from $this->INSTACNE_NAME where region_parent_id=0  ORDER BY region_name";
		$q = $this->db->query($sql);
		
		$list ="";
		if($q->num_rows > 0)
		{
			foreach($q->result() as $r)
			{
				$selected = ($r->region_id == $selected_id)?"selected='selected'":"";	
				$list .="<option value='".$r->region_id."' $selected>".stripslashes($r->region_name)."</option>";
			}
		}
		return $list;
	}
	function activeRegionByParent($parent_id)
	{
		//$parent_data = array('region_parent_id'=> $parent_id,
		//				'region_status'=>'Active'
		//				);
		$sql="select * from $this->INSTACNE_NAME WHERE region_parent_id='".$parent_id."' AND region_status='Active' order by region_name ASC";
		$result=$this->db->query($sql);								
		//$result = $this->db->get_where($this->INSTACNE_NAME,$parent_data);	
		return $result;
		
	}	
	function getCountryName($id){
		$sql="select * from $this->INSTACNE_NAME WHERE region_id ='".$id."'";
        $params = array(
            'cache_name' => 'getCountryName_'.$id, #unique name
            'cache_table_name' => array('gl_region'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$result=$this->db->query($sql);
        $result = get_create_cache($params); 
		$res=$result->row();
		return $res->region_name;
	}
	function getCountryID($country){
		$sql="select * from $this->INSTACNE_NAME WHERE region_name ='".db_input($country)."'";
		$result=$this->db->query($sql);								
		$res=$result->row();
		return $res->region_id;
	}
    function getCountryID_Check($country){
		$sql="select * from $this->INSTACNE_NAME WHERE region_name ='".db_input($country)."'";
		$result=$this->db->query($sql);										
		return $result;
	}
	function getAllCountryDropdown($selected_id)
	{
		$result = $this->activeRegionByParent($parent_id=0);
		$option = "";
		foreach($result->result() as $rec)
		{
			//$option .= " <optgroup>";
			$selected = ($selected_id==$rec->region_id)?' selected="selected"':'';
			$option.='<option value="'.$rec->region_id.'"'.$selected.'>'.$rec->region_name.'</option>';
			//$sub_result = $this->activeRegionByParent($rec->region_id);			
			//$option.= "</optgroup>";
		}
		return $option;
	}
	function getLocalityDropdown($selected_id){		
		$list= array(
			array('name' => 'Locality'),
			array('name' => 'District')			
			);			
		$option = "";
		foreach($list as $item)		{			
			$selected = ($selected_id==$item['name'])?' selected="selected"':'';			
			$option.='<option value="'.$item['name'].'"'.$selected.'>'.$item['name'].'</option>';			
		}
		return $option;
	}
	function getRegionListDropdown($selected_id){		
		$list= array(
			array('name' => 'State'),
			array('name' => 'Province'),
			array('name' => 'Region')
			);			
		$option = "";
		foreach($list as $item)		{			
			$selected = ($selected_id==$item['name'])?' selected="selected"':'';			
			$option.='<option value="'.$item['name'].'"'.$selected.'>'.$item['name'].'</option>';			
		}
		return $option;
	}
	function getAllRegionDropdown($selected_id)
	{
		$result = $this->activeRegionByParent($parent_id=0);
		$option = "";
		foreach($result->result() as $rec)
		{
			$option .= " <optgroup>";
			$selected = ($selected_id==$rec->region_id)?' selected="selected"':'';
			$option.='<option value="'.$rec->region_id.'"'.$selected.'>'.stripslashes($rec->region_name).'</option>';
			$sub_result = $this->activeRegionByParent($rec->region_id);
			foreach($sub_result->result() as $sub_rec)
			{
				$selected = ($selected_id==$sub_rec->region_id)?' selected="selected"':'';
				$option.='<option value="'.$sub_rec->region_id.'"'.$selected.'>'.stripslashes($sub_rec->region_name).'</option>';
			}
			$option.= "</optgroup>";
		}
		return $option;
	}
	
	function publish($contentID,$contentStatus)
	{
		$data=array(
		'region_status'=>$contentStatus
		);
		
		$where=array(
		'region_id'=>$contentID
		);
		
		$this->db->update($this->INSTACNE_NAME,$data,$where);
	}
	
	function delete($contentID)
	{
		
		$where=array(
		'region_parent_id'=>$contentID
		);
		$q=$this->db->get_where($this->INSTACNE_NAME,$where);
		if($q->num_rows()>0)
			return 'FALSE';
		else
		{		
			$sql = "DELETE FROM ".$this->INSTACNE_NAME." WHERE region_id = '$contentID'";
			$result = $this->db->query_delete($sql);
			return $result['status'];
		}
	}
	
	function addPage()
	{
			$yn = isset($_POST['region_selectable'])?'Y':'N';
			$content=array(
			'region_parent_id'=>$this->input->post('parent_id'),
			'region_status'=>'Active',
			'region_selectable'=>$yn,
			'region_name'=>$this->input->post('region_name'),
			'region_added'=>date('Y-m-d H:i:s')
			);
			$this->db->insert($this->INSTACNE_NAME,$content);
			$inserted_id=$this->db->insert_id();
            
            
            //*insert the outlet code prefix
            foreach( $this->input->post('region_outlet_prefix') as $p)
            {
                $content=array(

                    'region_id'=>$inserted_id,
                    'prefix_outlet_code'=>$p
                );
                $this->db->insert($this->INSTACNE_CODES,$content);
            }
            //*insert the outlet code prefix
			
			$sql="SELECT region_parent_id, region_parent_path_id FROM $this->INSTACNE_NAME WHERE region_id='".$this->input->post('parent_id')."'";
		$result = $this->db->query($sql);
		$rec_path = $result->row();
		if($this->input->post('parent_id')=="0")
		{
			$path="/$inserted_id/";
		}
		else
		{
		$path=$rec_path->region_parent_path_id."$inserted_id/";
		}
		
		$data_update=array(
		'region_parent_path_id'=>$path,
		);
		$this->db->where('region_id', $inserted_id);
		$this->db->update($this->INSTACNE_NAME, $data_update); 
		return $insert_id;
		
	}
	
	function editPage($contentID=0)
	{
		$yn = isset($_POST['region_selectable'])?'Y':'N';
		$data=array(
		'region_parent_id'=>$this->input->post('parent_id'),
		'region_name'=>addslashes($this->input->post('region_name')),
		'region_selectable'=>$yn
		);
		$where=array(
		'region_id'=>$contentID
		);
		$this->db->update($this->INSTACNE_NAME,$data,$where);
        
        //*insert the new outlet code prefix
            foreach( $this->input->post('new_region_outlet_prefix') as $p)
            {
                $content=array(
                    'region_id'=>$contentID,
                    'prefix_outlet_code'=>$p
                );
                $this->db->insert($this->INSTACNE_CODES,$content);
            }
            //*insert the outlet code prefix
        
		
		$sql="SELECT region_parent_id, region_parent_path_id FROM $this->INSTACNE_NAME WHERE region_id='".$this->input->post('parent_id')."'";
		$result = $this->db->query($sql);
		$rec_path = $result->row();
		
		if($this->input->post('parent_id')=="0")
		{
			$path="/$contentID/";
		}
		else
		{
			$path=$rec_path->region_parent_path_id."$contentID/";
		}
		$data_update=array(
		'region_parent_path_id'=>$path,
		);
		$this->db->where('region_id', $contentID);
		$this->db->update($this->INSTACNE_NAME, $data_update); 
        
        //clear cache
        $params = array(
            'affected_tables' 
                => array(
                    'gl_outlet'                    
                ), #cache name   
            'specific_names'
                => array(
                    'regionwise_merchant_'.$contentID,
                    'regionwise_merchant_list_'.$contentID,                    
                    'region_name_'.$contentID,
                    'getCountryName_'.$contentID,                    
                    'region_name_only_',
                    'selectRegionNameByID_'.$contentID                    
                ) #cache name
        );
        delete_cache($params);
        //clear cache
        
	}
	
	
function location_dropdown($parent_id=0,$selected_id = 0)
{
	$res_parent = 	$this->activeRegionByParent($parent_id);
	
	if($res_parent->num_rows()>0){
			foreach($res_parent->result() as $rec)
			{
				$selected = ($selected_id==$rec->region_id)?' selected="selected"':'';
				$disabled = ($rec->region_selectable!='Y')?'  disabled="disabled"  class="disabled_option"':'';
				if($rec->region_selectable=='Y'){
					echo '<option value="'.$rec->region_id.'"'.$selected.'>'.stripslashes($rec->region_name).'</option>';
				}
				$this->recursive($rec->region_id,$selected_id,'&nbsp;');
			}
	}
}
function recursive($parent_id=0,$selected_id,$space)
{
		$res_parent = 	$this->activeRegionByParent($parent_id);
	
	if($res_parent->num_rows()>0)
	{$space.='&nbsp;&nbsp;';
			foreach($res_parent->result() as $rec)
			{
				$selected = ($selected_id==$rec->region_id)?' selected="selected"':'';
				$disabled = ($rec->region_selectable!='Y')?'  disabled="disabled"  class="disabled_option"':'';
				if($rec->region_selectable=='Y'){
				echo '<option value="'.$rec->region_id.'"'.$selected. $disabled.'>'.$space.stripslashes($rec->region_name).'</option>';}
				
				$this->recursive($rec->region_id,$selected_id,$space);
				
			}
			
	}else
	 $space ='&nbsp;&nbsp;&nbsp;';
}
function region_name($region_id)
{
	$region_name= "";
     $sql = "SELECT region_parent_path_id,region_name FROM gl_region 
            WHERE region_id = '$region_id'
            AND region_status = 'Active'
            ";
    $params = array(
        'cache_name' => 'region_name_'.$region_id, #unique name
        'cache_table_name' => array('gl_region'), #database table
        'result' => $sql, #the result 
        'result_type' => 'result', #if from DB result object
    );
    //$res = $this->db->query($sql);    
    $res = get_create_cache($params);
    
    if($res->num_rows()>0)
    {
        $rec = $res->row();
        $region_path = 	explode('/',$rec->region_parent_path_id);
        $region_path = array_reverse($region_path);
        $region_name = "";
        $cnt = count($region_path);
        if($cnt>0)
        {
            foreach($region_path as $key)
            {				
                if($key>0)
                    $region_name .= $this->selectRegionNameByID($key).",";
            }
        }
	}
    
	return trim($region_name,',');
	
}//function close


function region_name_only()
{
		$region_name= "";
		$sql = "SELECT region_name FROM gl_region WHERE region_status = 'Active'";
		$params = array(
            'cache_name' => 'region_name_only_', #unique name
            'cache_table_name' => array('gl_region'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$res = $this->db->query($sql);
        $res = get_create_cache($params); 
        
        
		return $res;
	
}//function close
}//eoc
?>