<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: shree
Date: July-23-2013
*/
class Myaccount_m extends CI_Model
{
	private $INSTANCE="gl_admin";
	
	function passwordVerify($user_id=0,$cur_pword="")
	{
	
	
		$data=array(
		'user_id'=>$user_id,
		'user_pword'=>md5($cur_pword)
		);
		
		$q=$this->db->get_where($this->INSTANCE,$data);
		
		if($q->num_rows()==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function passwordChange($admin_id=0,$new_pword="")
	{
	
	
		$data=array(
		'admin_password'=>($new_pword)
		);
		
		$where=array(
		'admin_id'=>$admin_id
		);
		
		$this->db->update($this->INSTANCE,$data,$where);
	}
	function hash_key_value($salt,$password)
	{
		$hash_password = hash('SHA256',$salt.$password);		
		return $hash_password;
	}
}
?>