<?php

#written by shree
#Date july 30, 2013
#

class User_account_m extends CI_Model {

    private $INSTANCE_USER = "gl_user";
    private $INSTANCE_ACCTUNT = "gl_account";
    private $INSTANCE_VOUCHER = "gl_voucher";
    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;

    public function __construct()
    {
          $CI =& get_instance();
          $CI->load->model('invites_m');
    }
    
    
    function getUserFname($user_id) {
        $sql = "SELECT user_id, CASE WHEN user_fname <> 'glomp!' 
                    THEN
                        CONCAT(UPPER(LEFT(user_fname, 1)), LOWER(SUBSTRING(user_fname, 2)))
                    ELSE user_fname
                    END user_fname,
                    user_fb_id FROM gl_user u WHERE user_id = '$user_id' ";
         $params = array(
            'cache_name' => 'getUserFname_'.$user_id, #unique name
            'cache_table_name' => array('gl_user'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$result = $this->db->query($sql);
        $result = get_create_cache($params);
        
        $rec = $result->row();
        return $rec;
    }

    function user_short_info_by_email($email) {
        $sql = "SELECT u.user_id,
                CASE WHEN u.user_fname <> 'glomp!' 
                    THEN
                        CONCAT(UPPER(LEFT(u.user_fname, 1)), LOWER(SUBSTRING(u.user_fname, 2)))
                    ELSE user_fname
                END user_fname,
                CASE WHEN u.user_fname <> 'glomp!' 
                    THEN
                        CONCAT(UPPER(LEFT(u.user_lname, 1)), LOWER(SUBSTRING(u.user_lname, 2)))
                    ELSE user_lname
                END user_lname,
                concat(u.user_fname,' ',u.user_lname)as user_name,
                u.user_email,u.user_profile_pic,u.user_gender,r.region_name
                                FROM gl_user u,gl_region r
                                WHERE
                                    u.user_city_id = r.region_id
                                    AND u.user_email = '$email'
            ";

        $result = $this->db->query($sql);
        $rec = $result->row();
        return json_encode($rec);
    }

    function user_short_info($user_id) {
        $sql = "SELECT u.user_id,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                        CONCAT(UPPER(LEFT(u.user_fname, 1)), LOWER(SUBSTRING(u.user_fname, 2)))
                ELSE user_fname
                END user_fname,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                    CONCAT(UPPER(LEFT(u.user_lname, 1)), LOWER(SUBSTRING(u.user_lname, 2)))
                ELSE user_lname
                END user_lname,
		concat(u.user_fname,' ',u.user_lname)as user_name,
		u.user_email,u.user_profile_pic,u.user_gender,r.region_name
				FROM gl_user u,gl_region r
				WHERE
					u.user_city_id = r.region_id
					AND u.user_id = '$user_id'
				";
        $result = $this->db->query($sql);
        $rec = $result->row();
        return json_encode($rec);
    }

    function user_short_info1($user_id) {
        $sql = "SELECT u.user_id,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                        CONCAT(UPPER(LEFT(u.user_fname, 1)), LOWER(SUBSTRING(u.user_fname, 2)))
                ELSE user_fname
                END user_fname,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                    CONCAT(UPPER(LEFT(u.user_lname, 1)), LOWER(SUBSTRING(u.user_lname, 2)))
                ELSE user_lname
                END user_lname,
                user_hash_key,
		concat(u.user_fname,' ',u.user_lname)as user_name,
		u.user_email,u.user_profile_pic,u.user_gender,r.region_name
				FROM gl_user u,gl_region r
				WHERE
					u.user_city_id = r.region_id
					AND u.user_id = '$user_id'
				";
                
        $params = array(
            'cache_name' => 'user_short_info_'.$user_id, #unique name
            'cache_table_name' => array('gl_user','gl_region'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$result = $this->db->query($sql);
        $result = get_create_cache($params);        
        $rec = $result->row();
        return json_encode($rec);
    }

    function user_short_info2($user_id) {
        $sql = "SELECT u.user_id,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                        CONCAT(UPPER(LEFT(u.user_fname, 1)), LOWER(SUBSTRING(u.user_fname, 2)))
                ELSE user_fname
                END user_fname,
                CASE WHEN u.user_fname <> 'glomp!' 
                THEN
                    CONCAT(UPPER(LEFT(u.user_lname, 1)), LOWER(SUBSTRING(u.user_lname, 2)))
                ELSE user_lname
                END user_lname,
                user_hash_key,
        concat(u.user_fname,' ',u.user_lname)as user_name,
        u.user_email,u.user_profile_pic,u.user_gender
                FROM gl_user u
                WHERE
                    u.user_id = '$user_id'
                ";
                
        $params = array(
            'cache_name' => 'user_short_info_'.$user_id, #unique name
            'cache_table_name' => array('gl_user','gl_region'), #database table
            'result' => $sql, #the result 
            'result_type' => 'result', #if from DB result object
        ); 
        //$result = $this->db->query($sql);
        $result = get_create_cache($params);        
        $rec = $result->row();
        return json_encode($rec);
    }

    function user_shortinfo_by_email($email) {
        $sql = "SELECT user_id,CASE WHEN user_fname <> 'glomp!' 
                THEN
                    CONCAT(UPPER(LEFT(user_fname, 1)), LOWER(SUBSTRING(user_fname, 2)))
                ELSE user_fname
                END user_fname,
                CASE WHEN user_fname <> 'glomp!' 
                THEN
                    CONCAT(UPPER(LEFT(user_lname, 1)), LOWER(SUBSTRING(user_lname, 2)))
                ELSE user_lname
                END user_lname,
		concat(user_fname,' ',user_lname)as user_name,
		user_email,user_profile_pic,user_gender,user_hash_key
				FROM gl_user
				
				WHERE 
				user_email ='$email'
				";
        $result = $this->db->query($sql);
        $rec = $result->row();
        return json_encode($rec);
    }

    function user_balance($user_id = 0, $jsnon = false) {
        $sql = "SELECT summary_point_balance  
				FROM  gl_activity_summary
				WHERE summary_user_id = '$user_id'
				";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $res = $result->row();
            $point_balance = $res->summary_point_balance;
            $user_balance = array('summary_point_balance' => $point_balance);
        } else {
            $point_balance = 0;
            $user_balance = array('summary_point_balance' => 0);
        }
        if ($jsnon)
            return json_encode($user_balance);
        else
            return $point_balance;
    }

//end of balance

    function user_summary($user_id)
    {
        $sql = "SELECT * FROM gl_activity_summary WHERE summary_user_id = '$user_id'";
        $result = $this->db->query($sql);
        $summary = array();
        if ($result->num_rows() > 0)
        {
            $row = $result->row();
            $summary = array('user_id' => $row->summary_user_id,
                'last_glomp_date' => $row->summary_last_glomp_date,
                'point_balance' => $row->summary_point_balance,
                'total_received_glomp' => $row->summary_total_received_glomp,
                'total_sent_sent_glomp' => $row->summary_total_sent_sent_glomp,
                'summary_last_glomp_date' => $row->summary_last_glomp_date
            );
        }
        else
            $summary = array('msg' => 'No record found');
        return json_encode($summary);
    }

    function update_summary_sent_glopm($user_id, $point, $friend_id) {
        if ($point == '')
            $point = 0;
        $sql = "UPDATE gl_activity_summary SET
				summary_point_balance = (summary_point_balance-$point),
				summary_total_sent_sent_glomp = (summary_total_sent_sent_glomp+1)
				WHERE summary_user_id = '$user_id'
				";
        $this->db->query($sql);
        //now update the friend activity_summary
        $date = date('Y-m-d H:i:s');
        $sql = "UPDATE gl_activity_summary SET
				summary_last_glomp_date = '$date',
				summary_total_received_glomp = (summary_total_received_glomp+1)
				WHERE summary_user_id = '$friend_id'
				";
        $this->db->query($sql);
    }

    function update_activity_sent_glomp($user_id, $prod_name, $merchant_name, $rec_friend) {        
        if(! empty($rec_friend))
        {
            $friend_url = anchor('profile/view/' . $rec_friend->user_id, $rec_friend->user_name, 'target="_blank"');
            $log_title = $this->session->userdata('user_name') . " glomp!ed $friend_url to $merchant_name $prod_name";

            $log_details = $this->session->userdata('user_name') . " glomp!ed $friend_url to $merchant_name $prod_name";

            $arr = array('log_user_id' => $user_id,
                'log_title' => $log_title,
                'log_details' => $log_details,
                'log_timestamp' => date('Y-m-d H:i:s'),
                'log_ip' => $_SERVER['REMOTE_ADDR']
            );
            $this->db->insert('gl_activity', $arr);
            //now activity log as receiver
            $user_name = $this->session->userdata('user_name');


            $my_url = anchor('profile/view/' . $user_id, $user_name, 'target="_blank"');
            $log_title = "Received glomp!ed from $my_url of $merchant_name \'s $prod_name";
            $log_details = "Received glomp!ed from $my_url of $merchant_name \'s $prod_name";

            $arr = array('log_user_id' => $rec_friend->user_id,
                'log_title' => $log_title,
                'log_details' => $log_details,
                'log_timestamp' => date('Y-m-d H:i:s'),
                'log_ip' => $_SERVER['REMOTE_ADDR']
            );
            $this->db->insert('gl_activity', $arr);
        
        }
    }

    function sent_glomp_email($rec_friend, $prod_name, $merchant_name, $glomp_message = '', $voucher_id, $user_id) {
        $sender_name = $this->session->userdata('user_name');

        if (! empty($user_id) ) {
            $sender = $this->user_short_info1($user_id);
            $sender = json_decode($sender);
            $sender_name = $sender->user_name;
        } else {
            $user_id = $this->session->userdata('user_id');
        }

        
        $receiver_name = $rec_friend->user_name;
        $receiver_email = $rec_friend->user_email;
        $email_subject = "You've been glomp!ed!";
        $check_it_out = anchor('profile/view/' . $rec_friend->user_id, 'Check it out');

        //Load model and library
        $this->load->model('users_m');
        $this->load->library('email_templating');

        //Get logged in user info
        $user = $this->users_m->user_info_by_id($user_id);
        
        $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher_id))->row();
        $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
        $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();

        $vars = array(
            'template_name' => 'glomp_notification',
            'from_email' => $user->row()->user_email,
            'from_name' => $user->row()->user_fname . ' ' . $user->row()->user_lname,
            'lang_id' => $user->row()->user_lang_id,
            'to' => $receiver_email,
            'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
            'params' => array(
                '[insert treaters personal message]' => $glomp_message,
                '[insert merchant brand and product]' => $merchant_name . ' ' . $prod_name,
                '[insert treaters name]' => $sender_name,
                '[link]' => $check_it_out
            ),
            'others' => array(
                'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
            )
        );
        
        $e_template = $this->db->get_where('gl_email_templates', array('template_name' => $vars['template_name']));
        $args['user_email'] = $receiver_email;
        $args['type'] = 'glomp!ed';
        $args['params'] = array(
            'type' => '2',
            'id' => $user->row()->user_id
        );
        $args['message'] = $e_template->row()->subject;
        
        push_notification($args);
        $this->email_templating->config($vars);

        return $this->email_templating->send();
    }

    function sent_glomp_email_new_user($rec_friend, $prod_name, $merchant_name, $glomp_message, $voucher_id, $user_id) {
        $sender_name = $this->session->userdata('user_name');

        if (! empty($user_id) ) {
            $sender = $this->user_short_info1($user_id);
            $sender = json_decode($sender);
            $sender_name = $sender->user_name;
        } else {
            $user_id = $this->session->userdata('user_id');
        }

        $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher_id))->row();
        $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
        $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();
        
        $receiver_name = $rec_friend->user_name;
        $receiver_email = $rec_friend->user_email;
        $user_hash_key = $rec_friend->user_hash_key;

        $email_subject = "You've been glomp!ed!";
        $check_it_out = anchor('profile/view/' . $rec_friend->user_id, 'Check it out');
        $click_here = anchor('landing/preFill/' . $user_hash_key, 'sign up');

        //Load model and library
        $this->load->model('users_m');
        $this->load->library('email_templating');

        //Get logged in user info
        $user = $this->users_m->user_info_by_id($user_id);

        $vars = array(
            'template_name' => 'glomp_nonmember_w_personal_message',
            'from_email' => SITE_DEFAULT_SENDER_EMAIL,
            'reply_to' => $user->row()->user_email,
            'from_name' => $user->row()->user_fname . ' ' . $user->row()->user_lname,
            'lang_id' => $user->row()->user_lang_id,
            'to' => $receiver_email,
            'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
            'params' => array(
                '[insert treaters personal message]' => $glomp_message,
                '[insert merchant brand and product]' => $merchant_name . ' ' . $prod_name,
                '[insert treaters name]' => $sender_name,
                '[link]' => $click_here
            ),
            'others' => array(
                'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
            )
        );

        $this->email_templating->config($vars);
        $this->email_templating->set_subject($user->row()->user_fname . ' ' . $user->row()->user_lname . ' has given you a free treat!');

        return $this->email_templating->send();
    }

    /*     * ******************user glomp product to friend process************** */
#step 1: First of all logged in user should have balance so verify for the sufficient balance
#setp 2: verify friend_id is s/he really a friend.
#setp 3: product is available for this user area by merchant_id
#step 4 : verify logged in user by password 
#setp 5: after password matching start transaction
#step 6: reduce balance from the logged in user
#step 7: create voucher,reduce balance from the user 
#step 8: commint transaction or roolback
#step 9: send email 
#input user_id,friend_id,product_id

    function glomp_to_friend_fb($user_id = 0, $prduct_id = 0, $glomp_message = '', $fname, $lname, $user_fbID, $location_id) {
        $user_balance = $this->user_balance($user_id);
        /// if friend destion address is own user address

        $newuser = "";
        $sql = "SELECT * FROM gl_user WHERE user_fb_id='$user_fbID'";
        $res = $this->db->query($sql);

        if ($res->num_rows() == 0) {

            $newuser = "newuser";

            $signup_data = array('username' => $user_fbID . '@fabook.com',
                'user_fname' => $fname,
                'user_lname' => $lname,
                'user_email' => $user_fbID . '@fabook.com',
                'user_city_id' => $location_id,
                'user_fb_id' => $user_fbID,
                'user_join_date' => date('Y-m-d H:i:s'),
                'user_account_created_ip' => $this->custom_func->getUserIP(),
                'user_hash_key' => $this->mysql_uuid(),
                'user_status' => 'Pending',
                'user_last_updated_date' => $this->custom_func->datetime()
            );
            $this->db->insert('gl_user', $signup_data);
            $insert_id = $this->db->insert_id();
            $sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
            $res = $this->db->query($sql);
        }

        $rec = $res->row();
        $friend_id = $rec->user_id;


        $res_product = $this->product_m->is_user_valid_prodcut($friend_id, $prduct_id);
        if ($res_product->num_rows() == 1) {

            $res_friend = $this->user_short_info1($friend_id);
            $rec_friend = json_decode($res_friend);

            $row_prod = $res_product->row();
            $prod_point = $row_prod->prod_point;
            $prod_name = $row_prod->prod_name;
            $prod_reverse_point = $row_prod->prod_reverse_point;
            $prod_merchant_cost = $row_prod->prod_merchant_cost;
            $prod_merchant_id = $row_prod->prod_merchant_id;
            $merchant_name = $row_prod->merchant_name;
            $prod_voucher_expiry_day = $row_prod->prod_voucher_expiry_day;

            //verify user balance before glomp action
            if ($user_balance >= $prod_point) {

                //glomp to the user now
                //reduce balance from the user account
                $balance_point = $user_balance - $prod_point;
                //Start transaction
                $voucher_code = $prduct_id . '-' . $user_id . '-' . time(1111, 9999);

                $this->db->trans_begin();

                $account_txn = array('account_id' => $this->mysql_uuid(),
                    'account_user_id' => $user_id,
                    'account_txn_type' => 'DR',
                    'account_point' => $prod_point,
                    'account_balance_point' => $balance_point,
                    'account_txn_type_id' => 2,
                    'account_txnID' => $this->mysql_uuid(),
                    'account_txn_date' => date('Y-m-d H:i:s'),
                    'account_txn_ip' => $_SERVER['REMOTE_ADDR']
                );

                $account_insert_status = $this->db->insert('gl_account', $account_txn);
                /* update into summary table */
                $this->update_summary_sent_glopm($user_id, $prod_point, $friend_id);
                $this->update_activity_sent_glomp($user_id, $prod_name, $merchant_name, $rec_friend);

                if ($account_insert_status > 0) {
                    $voucher_new_id = $this->mysql_uuid();
                    $generate_voucher = array('voucher_id' => $voucher_new_id,
                        'voucher_purchaser_user_id' => $user_id,
                        'voucher_belongs_usser_id' => $friend_id,
                        'voucher_merchant_id' => $prod_merchant_id,
                        'voucher_product_id' => $prduct_id,
                        'voucher_point' => $prod_point,
                        'voucher_prod_cost' => $prod_merchant_cost,
                        'voucher_reverse_point' => $prod_reverse_point,
                        'voucher_code' => $voucher_code,
                        'voucher_purchased_date' => date('Y-m-d H:i:s'),
                        'voucher_status' => 'Consumable',
                        'voucher_type' => 'Consumable',
                        'voucher_expiry_day' => $prod_voucher_expiry_day,
                        'voucher_expiry_date' => $this->vocher_expiration_day($prod_voucher_expiry_day),
                        'voucher_sender_glomp_message' => addslashes($glomp_message)
                    );
                    $voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);
                    
                    //log activity [points_spent]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'points'=>$prod_point,
                            'balance'=>$balance_point,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'facebook'                            
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'points_spent',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [points_spent]
                    //log activity [glomp_someone]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'facebook'
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'glomp_someone',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [glomp_someone]
                    //log activity [received_a_glomp]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'glomper_id'=>$user_id,
                            'glomper_name'=>$this->session->userdata('user_name'),
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'facebook'
                            );
                    $params = array(
                            'user_id'=>$rec_friend->user_id,
                            'type' =>'received_a_glomp',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [received_a_glomp]
                    
                    
                    if ($voucher_insert_status > 0) {
                         $params = array(                        
                            'affected_tables' 
                                => array(
                                    'gl_voucher',
                                    'gl_outlet'                                    
                                ), #cache name
                            'specific_names' 
                                => array(                                    
                                    'friends_info_buzz_'.$user_id,
                                    'friends_info_buzz_'.$friend_id
                                )    
                        );
                        delete_cache($params);
                    
                    
                        $this->db->trans_commit();
                        
                        //Save to gl_invite table
                        $invite_data = array(
                          'invite_by_user_id' => $user_id,
                          'invite_through' => 'fb',  
                          'invite_voucher_id' => $generate_voucher['voucher_id'],
                          'invite_invited_id' =>  $friend_id,
                          'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                        );
                        
                        //send email to glomp receiver
                        if ($newuser == "newuser") {
                            //send email to glomp receiver
                            //$this->sent_glomp_email_new_user($rec_friend,$prod_name,$merchant_name);
                            $this->invites_m->_insert($invite_data);
                        }
                        else {
                            //$this->sent_glomp_email($rec_friend,$prod_name,$merchant_name);
                        }
                        return json_encode(array('txn_status' => "SUCCESS", 'voucher_id' => $voucher_new_id));
                    } else {
                        $this->db->trans_rollback();
                        return json_encode(array('txn_status' => "unexpected_error"));
                        exit();
                    }
                } else {
                    $this->db->trans_rollback();
                    return json_encode(array('txn_status' => "unexpected_error"));
                    exit();
                }
            } else {
                return json_encode(array('txn_status' => "insufficent_balance"));
                exit();
            }
        }//product validation check
        else {
            return json_encode(array('txn_status' => "invalid_product"));
        }
    }

    /*     * ******************user glomp product to friend process************** */
    #step 1: First of all logged in user should have balance so verify for the sufficient balance
    #setp 2: verify friend_id is s/he really a friend.
    #setp 3: product is available for this user area by merchant_id
    #step 4 : verify logged in user by password 
    #setp 5: after password matching start transaction
    #step 6: reduce balance from the logged in user
    #step 7: create voucher,reduce balance from the user 
    #step 8: commint transaction or roolback
    #step 9: send email 
    #input user_id,friend_id,product_id

    function glomp_to_sms($user_id = 0, $prduct_id = 0, $glomp_message = '', $fname, $lname, $user_mobile, $location_id) {
        $user_balance = $this->user_balance($user_id);
        /// if friend destion address is own user address

        $newuser = "";
        $sql = "SELECT * FROM gl_user WHERE user_mobile='$user_mobile'";
        $res = $this->db->query($sql);

        if ($res->num_rows() == 0) {

            $newuser = "newuser";

            $signup_data = array('username' => $user_mobile . '@glomp.it',
                'user_fname' => $fname,
                'user_lname' => $lname,
                'user_email' => $user_mobile . '@glomp.it',
                'user_city_id' => $location_id,
                'user_mobile' => $user_mobile,
                'user_join_date' => date('Y-m-d H:i:s'),
                'user_account_created_ip' => $this->custom_func->getUserIP(),
                'user_hash_key' => $this->mysql_uuid(),
                'user_status' => 'Pending',
                'user_last_updated_date' => $this->custom_func->datetime()
            );
            $this->db->insert('gl_user', $signup_data);
            $insert_id = $this->db->insert_id();
            $sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
            $res = $this->db->query($sql);
        }

        $rec = $res->row();
        $friend_id = $rec->user_id;


        $res_product = $this->product_m->is_user_valid_prodcut($friend_id, $prduct_id);
        if ($res_product->num_rows() == 1) {

            $res_friend = $this->user_short_info1($friend_id);
            $rec_friend = json_decode($res_friend);

            $row_prod = $res_product->row();
            $prod_point = $row_prod->prod_point;
            $prod_name = $row_prod->prod_name;
            $prod_reverse_point = $row_prod->prod_reverse_point;
            $prod_merchant_cost = $row_prod->prod_merchant_cost;
            $prod_merchant_id = $row_prod->prod_merchant_id;
            $merchant_name = $row_prod->merchant_name;
            $prod_voucher_expiry_day = $row_prod->prod_voucher_expiry_day;

            //verify user balance before glomp action
            if ($user_balance >= $prod_point) {

                //glomp to the user now
                //reduce balance from the user account
                $balance_point = $user_balance - $prod_point;
                //Start transaction
                $voucher_code = $prduct_id . '-' . $user_id . '-' . time(1111, 9999);

                $this->db->trans_begin();

                $account_txn = array('account_id' => $this->mysql_uuid(),
                    'account_user_id' => $user_id,
                    'account_txn_type' => 'DR',
                    'account_point' => $prod_point,
                    'account_balance_point' => $balance_point,
                    'account_txn_type_id' => 2,
                    'account_txnID' => $this->mysql_uuid(),
                    'account_txn_date' => date('Y-m-d H:i:s'),
                    'account_txn_ip' => $_SERVER['REMOTE_ADDR']
                );

                $account_insert_status = $this->db->insert('gl_account', $account_txn);
                /* update into summary table */
                $this->update_summary_sent_glopm($user_id, $prod_point, $friend_id);
                $this->update_activity_sent_glomp($user_id, $prod_name, $merchant_name, $rec_friend);

                if ($account_insert_status > 0) {
                    $voucher_new_id = $this->mysql_uuid();
                    $generate_voucher = array('voucher_id' => $voucher_new_id,
                        'voucher_purchaser_user_id' => $user_id,
                        'voucher_belongs_usser_id' => $friend_id,
                        'voucher_merchant_id' => $prod_merchant_id,
                        'voucher_product_id' => $prduct_id,
                        'voucher_point' => $prod_point,
                        'voucher_prod_cost' => $prod_merchant_cost,
                        'voucher_reverse_point' => $prod_reverse_point,
                        'voucher_code' => $voucher_code,
                        'voucher_purchased_date' => date('Y-m-d H:i:s'),
                        'voucher_status' => 'Consumable',
                        'voucher_type' => 'Consumable',
                        'voucher_expiry_day' => $prod_voucher_expiry_day,
                        'voucher_expiry_date' => $this->vocher_expiration_day($prod_voucher_expiry_day),
                        'voucher_sender_glomp_message' => addslashes($glomp_message)
                    );
                    $voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);
                    
                    //log activity [points_spent]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'points'=>$prod_point,
                            'balance'=>$balance_point,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'sms'                            
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'points_spent',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [points_spent]
                    //log activity [glomp_someone]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'sms'
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'glomp_someone',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [glomp_someone]
                    //log activity [received_a_glomp]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'glomper_id'=>$user_id,
                            'glomper_name'=>$this->session->userdata('user_name'),
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'sms'
                            );
                    $params = array(
                            'user_id'=>$rec_friend->user_id,
                            'type' =>'received_a_glomp',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [received_a_glomp]
                    
                    
                    if ($voucher_insert_status > 0) {
                         $params = array(                        
                            'affected_tables' 
                                => array(
                                    'gl_voucher',
                                    'gl_outlet'                                    
                                ), #cache name
                            'specific_names' 
                                => array(                                    
                                    'friends_info_buzz_'.$user_id,
                                    'friends_info_buzz_'.$friend_id
                                )    
                        );
                        delete_cache($params);
                    
                    
                        $this->db->trans_commit();
                        
                        //Save to gl_invite table
                        $invite_data = array(
                          'invite_by_user_id' => $user_id,
                          'invite_through' => 'sms',  
                          'invite_voucher_id' => $generate_voucher['voucher_id'],
                          'invite_invited_id' =>  $friend_id,
                          'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                        );
                        
                        //send email to glomp receiver
                        if ($newuser == "newuser") {
                            //send email to glomp receiver
                            //$this->sent_glomp_email_new_user($rec_friend,$prod_name,$merchant_name);
                            $this->invites_m->_insert($invite_data);
                        }
                        else {
                            //$this->sent_glomp_email($rec_friend,$prod_name,$merchant_name);
                        }

                        $sms_code = substr($rec->user_hash_key, 0, 4).substr($rec->user_mobile, -4);
                        return json_encode(array('txn_status' => "SUCCESS", 'voucher_id' => $voucher_new_id, 'sms_code' => $sms_code));
                    } else {
                        $this->db->trans_rollback();
                        return json_encode(array('txn_status' => "unexpected_error"));
                        exit();
                    }
                } else {
                    $this->db->trans_rollback();
                    return json_encode(array('txn_status' => "unexpected_error"));
                    exit();
                }
            } else {
                return json_encode(array('txn_status' => "insufficent_balance"));
                exit();
            }
        }//product validation check
        else {
            return json_encode(array('txn_status' => "invalid_product"));
        }
    }


    function glomp_to_friend_li($user_id = 0, $prduct_id = 0, $glomp_message = '', $fname, $lname, $user_fbID, $location_id) {
        //TODO: PLEASE CLEAN THIS CODE
        $user_balance = $this->user_balance($user_id);
        /// if friend destion address is own user address

        $newuser = "";
        $sql = "SELECT * FROM gl_user WHERE user_linkedin_id='$user_fbID'";
        $res = $this->db->query($sql);

        if ($res->num_rows() == 0) {

            $newuser = "newuser";

            $signup_data = array(
                'username' => $user_fbID . '@linkedin.com',
                'user_fname' => $fname,
                'user_lname' => $lname,
                'user_email' => $user_fbID . '@linkedin.com',
                'user_linkedin_id' => $user_fbID,
                'user_join_date' => date('Y-m-d H:i:s'),
                'user_account_created_ip' => $this->custom_func->getUserIP(),
                'user_hash_key' => $this->mysql_uuid(),
                'user_status' => 'Pending',
                'user_city_id' => $location_id,
                'user_last_updated_date' => $this->custom_func->datetime()
            );
            $this->db->insert('gl_user', $signup_data);
            $insert_id = $this->db->insert_id();
            $sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
            $res = $this->db->query($sql);
        }

        $rec = $res->row();
        $friend_id = $rec->user_id;


        $res_product = $this->product_m->is_user_valid_prodcut($friend_id, $prduct_id);
        if ($res_product->num_rows() == 1) {

            $res_friend = $this->user_short_info1($friend_id);
            $rec_friend = json_decode($res_friend);

            $row_prod = $res_product->row();
            $prod_point = $row_prod->prod_point;
            $prod_name = $row_prod->prod_name;
            $prod_reverse_point = $row_prod->prod_reverse_point;
            $prod_merchant_cost = $row_prod->prod_merchant_cost;
            $prod_merchant_id = $row_prod->prod_merchant_id;
            $merchant_name = $row_prod->merchant_name;
            $prod_voucher_expiry_day = $row_prod->prod_voucher_expiry_day;

            //verify user balance before glomp action
            if ($user_balance >= $prod_point) {

                //glomp to the user now
                //reduce balance from the user account
                $balance_point = $user_balance - $prod_point;
                //Start transaction
                $voucher_code = $prduct_id . '-' . $user_id . '-' . time(1111, 9999);

                $this->db->trans_begin();

                $account_txn = array('account_id' => $this->mysql_uuid(),
                    'account_user_id' => $user_id,
                    'account_txn_type' => 'DR',
                    'account_point' => $prod_point,
                    'account_balance_point' => $balance_point,
                    'account_txn_type_id' => 2,
                    'account_txnID' => $this->mysql_uuid(),
                    'account_txn_date' => date('Y-m-d H:i:s'),
                    'account_txn_ip' => $_SERVER['REMOTE_ADDR']
                );

                $account_insert_status = $this->db->insert('gl_account', $account_txn);
                /* update into summary table */
                $this->update_summary_sent_glopm($user_id, $prod_point, $friend_id);
                $this->update_activity_sent_glomp($user_id, $prod_name, $merchant_name, $rec_friend);

                if ($account_insert_status > 0) {
                    $voucher_new_id = $this->mysql_uuid();
                    $generate_voucher = array('voucher_id' => $voucher_new_id,
                        'voucher_purchaser_user_id' => $user_id,
                        'voucher_belongs_usser_id' => $friend_id,
                        'voucher_merchant_id' => $prod_merchant_id,
                        'voucher_product_id' => $prduct_id,
                        'voucher_point' => $prod_point,
                        'voucher_prod_cost' => $prod_merchant_cost,
                        'voucher_reverse_point' => $prod_reverse_point,
                        'voucher_code' => $voucher_code,
                        'voucher_purchased_date' => date('Y-m-d H:i:s'),
                        'voucher_status' => 'Consumable',
                        'voucher_type' => 'Consumable',
                        'voucher_expiry_day' => $prod_voucher_expiry_day,
                        'voucher_expiry_date' => $this->vocher_expiration_day($prod_voucher_expiry_day),
                        'voucher_sender_glomp_message' => addslashes($glomp_message)
                    );
                    $voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);                                        
                    
                    
                    //log activity [points_spent]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'points'=>$prod_point,
                            'balance'=>$balance_point,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'linkedIn'                            
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'points_spent',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [points_spent]
                    //log activity [glomp_someone]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'linkedIn'
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'glomp_someone',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [glomp_someone]
                    //log activity [received_a_glomp]                    
                    $details=array(
                            'voucher_id'=>$voucher_new_id,
                            'glomper_id'=>$user_id,
                            'glomper_name'=>$this->session->userdata('user_name'),
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name,
                            'glomp_through'=>'linkedIn'
                            );
                    $params = array(
                            'user_id'=>$rec_friend->user_id,
                            'type' =>'received_a_glomp',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [received_a_glomp]
                    
                    
                    if ($voucher_insert_status > 0) {
                        $params = array(                        
                            'affected_tables' 
                                => array(
                                    'gl_voucher',
                                    'gl_outlet'                                    
                                ), #cache name
                            'specific_names' 
                                => array(                                    
                                    'friends_info_buzz_'.$user_id,
                                    'friends_info_buzz_'.$friend_id
                                )    
                        );
                        delete_cache($params);
                        
                        $this->db->trans_commit();                        
                        //Save to gl_invite table
                        $invite_data = array(
                          'invite_by_user_id' => $user_id,
                          'invite_through' => 'linkedIn',  
                          'invite_voucher_id' => $generate_voucher['voucher_id'],
                          'invite_invited_id' =>  $friend_id,
                          'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                        );
                        
                        //send email to glomp receiver
                        if ($newuser == "newuser") {
                            //send email to glomp receiver
                            //$this->sent_glomp_email_new_user($rec_friend,$prod_name,$merchant_name);
                            $this->invites_m->_insert($invite_data);
                        }
                        else {
                            //$this->sent_glomp_email($rec_friend,$prod_name,$merchant_name);
                        }
                        return json_encode(array('txn_status' => "SUCCESS", 'voucher_id' => $voucher_new_id));
                    } else {
                        $this->db->trans_rollback();
                        return json_encode(array('txn_status' => "unexpected_error"));
                        exit();
                    }
                } else {
                    $this->db->trans_rollback();
                    return json_encode(array('txn_status' => "unexpected_error"));
                    exit();
                }
            } else {
                return json_encode(array('txn_status' => "insufficent_balance"));
                exit();
            }
        }//product validation check
        else {
            return json_encode(array('txn_status' => "invalid_product"));
        }
    }

    function glomp_to_friend($user_id = 0, $prduct_id = 0, $glomp_message = '', $fname, $lname, $email, $location_id) {
        $user_balance = $this->user_balance($user_id);
        /// if friend destion address is own user address
        if ($email == $this->session->userdata('user_email')) {
            return json_encode(array('txn_status' => "invalid_friend"));
            exit();
        }

        $newuser = "";
        $sql = "SELECT * FROM gl_user WHERE user_email='$email'";
        $res = $this->db->query($sql);

        if ($res->num_rows() == 0) {

            $newuser = "newuser";
            $fname = trim($fname);
            $lname = trim($lname);

            if ($lname == '' && (strpos($fname, ' ') !== FALSE)) {
                $temp = explode(' ', $fname, 2);
                $fname = $temp[0];
                $lname = $temp[1];
            }

            $signup_data = array('username' => $email,
                'user_fname' => $fname,
                'user_lname' => $lname,
                'user_email' => $email,
                'user_city_id' => $location_id,
                'user_join_date' => date('Y-m-d H:i:s'),
                'user_account_created_ip' => $this->custom_func->getUserIP(),
                'user_hash_key' => $this->mysql_uuid(),
                'user_status' => 'Pending',
                'user_last_updated_date' => $this->custom_func->datetime()
            );

            $this->db->insert('gl_user', $signup_data);
            $insert_id = $this->db->insert_id();
            $sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
            $res = $this->db->query($sql);
        }

        $rec = $res->row();
        $friend_id = $rec->user_id;


        $res_product = $this->product_m->is_user_valid_prodcut($friend_id, $prduct_id);
        if ($res_product->num_rows() == 1) {

            $res_friend = $this->user_short_info1($friend_id);
            $rec_friend = json_decode($res_friend);

            $row_prod = $res_product->row();
            $prod_point = $row_prod->prod_point;
            $prod_name = $row_prod->prod_name;
            $prod_reverse_point = $row_prod->prod_reverse_point;
            $prod_merchant_cost = $row_prod->prod_merchant_cost;
            $prod_merchant_id = $row_prod->prod_merchant_id;
            $merchant_name = $row_prod->merchant_name;
            $prod_voucher_expiry_day = $row_prod->prod_voucher_expiry_day;

            //verify user balance before glomp action
            if ($user_balance >= $prod_point) {

                //glomp to the user now
                //reduce balance from the user account
                $balance_point = $user_balance - $prod_point;
                //Start transaction
                $voucher_code = $prduct_id . '-' . $user_id . '-' . time(1111, 9999);

                $this->db->trans_begin();

                $account_txn = array('account_id' => $this->mysql_uuid(),
                    'account_user_id' => $user_id,
                    'account_txn_type' => 'DR',
                    'account_point' => $prod_point,
                    'account_balance_point' => $balance_point,
                    'account_txn_type_id' => 2,
                    'account_txnID' => $this->mysql_uuid(),
                    'account_txn_date' => date('Y-m-d H:i:s'),
                    'account_txn_ip' => $_SERVER['REMOTE_ADDR']
                );

                $account_insert_status = $this->db->insert('gl_account', $account_txn);
                /* update into summary table */
                $this->update_summary_sent_glopm($user_id, $prod_point, $friend_id);
                $this->update_activity_sent_glomp($user_id, $prod_name, $merchant_name, $rec_friend);
                
                
                

                if ($account_insert_status > 0) {
                    $generate_voucher_id =$this->mysql_uuid();
                    $generate_voucher = array('voucher_id' => $generate_voucher_id,
                        'voucher_purchaser_user_id' => $user_id,
                        'voucher_belongs_usser_id' => $friend_id,
                        'voucher_merchant_id' => $prod_merchant_id,
                        'voucher_product_id' => $prduct_id,
                        'voucher_point' => $prod_point,
                        'voucher_prod_cost' => $prod_merchant_cost,
                        'voucher_reverse_point' => $prod_reverse_point,
                        'voucher_code' => $voucher_code,
                        'voucher_purchased_date' => date('Y-m-d H:i:s'),
                        'voucher_status' => 'Consumable',
                        'voucher_expiry_day' => $prod_voucher_expiry_day,
                        'voucher_type' => 'Consumable',
                        'voucher_expiry_date' => $this->vocher_expiration_day($prod_voucher_expiry_day),
                        'voucher_sender_glomp_message' => addslashes($glomp_message)
                    );
                    $voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);
                    
                    
                    //log activity [points_spent]                    
                    $details=array(
                            'voucher_id'=>$generate_voucher_id,
                            'points'=>$prod_point,
                            'balance'=>$balance_point,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name                        
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'points_spent',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [points_spent]
                    
                    //log activity [glomp_someone]                    
                    $details=array(
                            'voucher_id'=>$generate_voucher_id,
                            'recipient_id'=>$rec_friend->user_id,
                            'recipient_name'=>$rec_friend->user_name,
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name                        
                            );
                    $params = array(
                            'user_id'=>$user_id,
                            'type' =>'glomp_someone',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [glomp_someone]
                    //log activity [received_a_glomp]                    
                    $details=array(
                            'voucher_id'=>$generate_voucher_id,
                            'glomper_id'=>$user_id,
                            'glomper_name'=>$this->session->userdata('user_name'),
                            'product_id'=>$prduct_id,
                            'product_name'=>$prod_name,
                            'merchant_id'=>$prod_merchant_id,
                            'merchant_name'=>$merchant_name                        
                            );
                    $params = array(
                            'user_id'=>$rec_friend->user_id,
                            'type' =>'received_a_glomp',
                            'details' =>json_encode($details)
                            );
                    add_activity_log($params);
                    //log activity [received_a_glomp]
                    
                    
                    $params = array(                        
                            'affected_tables' 
                                => array(
                                    'gl_voucher',
                                    'gl_outlet'
                                ), #cache name
                            'specific_names' 
                                => array(
                                    'friends_info_buzz_'.$user_id,
                                    'friends_info_buzz_'.$friend_id                                    
                                )    
                        );
                    delete_cache($params);
                    
                    
                    if ($voucher_insert_status > 0) {
                        $this->db->trans_commit();
                        
                        //Save to gl_invite table
                        $invite_data = array(
                          'invite_by_user_id' => $user_id,
                          'invite_through' => 'email',  
                          'invite_voucher_id' => $generate_voucher['voucher_id'],
                          'invite_invited_id' =>  $friend_id,
                          'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                        );
                        
                        //send email to glomp receiver
                        if ($newuser == "newuser") {
                            //send email to glomp receiver
                            $email_log_id = $this->sent_glomp_email_new_user($rec_friend, $prod_name, $merchant_name, $glomp_message, $generate_voucher['voucher_id'], $user_id);
                            $invite_data['invite_email_log_id'] = $email_log_id;
                            $this->invites_m->_insert($invite_data);
                        } else {
                            $email_log_id = $this->sent_glomp_email($rec_friend, $prod_name, $merchant_name, $glomp_message, $generate_voucher['voucher_id'], $user_id);
                        }
                        
                        
                        
                        return json_encode(array('txn_status' => "SUCCESS", 'voucher_id' => $generate_voucher_id));
                    } else {
                        $this->db->trans_rollback();
                        return json_encode(array('txn_status' => "unexpected_error"));
                        exit();
                    }
                } else {
                    $this->db->trans_rollback();
                    return json_encode(array('txn_status' => "unexpected_error"));
                    exit();
                }
            } else {
                return json_encode(array('txn_status' => "insufficent_balance"));
                exit();
            }
        }//product validation check
        else {
            return json_encode(array('txn_status' => "invalid_product"));
        }
    }

//end



    /* steps
      1. Check whether the user in the database or not(if user is not in database it will add the user in the database.)
      2. Checking the voucher (Assignable, and not expired by voucher id)

     */

    function reassignVoucher($userID, $message, $fname, $lname, $email, $voucgerID, $fb_id, $fb_fname, $fb_lname, $fb_dob, $fb_email, $location_id=0) {
        $newuser = "";
        $sql = "SELECT * FROM gl_user WHERE user_email='$email'";
        $res = $this->db->query($sql);

        if ($res->num_rows() == 0) {

            $newuser = "newuser";

            $fname = trim($fname);
            $lname = trim($lname);
            if ($lname == "" && (strpos($fname, ' ') !== FALSE)) {
                $temp = explode(' ', $fname, 2);
                $fname = $temp[0];
                $lname = $temp[1];
            }

            $signup_data = array('username' => $email,
                'user_fname' => $fname,
                'user_lname' => $lname,
                'user_email' => $email,
                'user_fb_email' => $fb_email,
                'user_fb_fname' => $fb_fname,
                'user_fb_lname' => $fb_lname,
                'user_fb_id' => $fb_id,
                'user_fb_dob' => $fb_dob,
                'user_join_date' => date('Y-m-d H:i:s'),
                'user_account_created_ip' => $this->custom_func->getUserIP(),
                'user_hash_key' => $this->mysql_uuid(),
                'user_status' => 'Pending',
                'user_city_id' => $location_id,
                'user_last_updated_date' => $this->custom_func->datetime()
            );
            $this->db->insert('gl_user', $signup_data);
            $insert_id = $this->db->insert_id();
            $sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
            $res = $this->db->query($sql);
        }

        $rec = $res->row();
        $friend_id = $rec->user_id;

        $sqlVoucher = "SELECT * FROM gl_voucher WHERE voucher_id='$voucgerID' AND 
				voucher_status ='Assigned' AND voucher_expiry_date >='" . date('Y-m-d H:i:s') . "'";
        $resVoucher = $this->db->query($sqlVoucher);
        if ($resVoucher->num_rows() > 0) {
            $recVoucher = $resVoucher->row();
            $voucher_code = mt_rand(1111, 9999);

            $res_friend = $this->user_short_info1($friend_id);
            $rec_friend = json_decode($res_friend);

            $this->db->trans_begin();

            $voucher_exp_date = $recVoucher->voucher_expiry_date;
            $prod_merchant_id = $recVoucher->voucher_merchant_id;
            $prduct_id = $recVoucher->voucher_product_id;
            $prod_point = $recVoucher->voucher_point;
            $prod_cost = $recVoucher->voucher_prod_cost;
            $prod_reverse_point = $recVoucher->voucher_reverse_point;
            $voucher_orginated_by = $recVoucher->voucher_orginated_by;
            $voucher_expiry_day = $recVoucher->voucher_expiry_day;

            $res_product = $this->product_m->is_user_valid_prodcut($friend_id, $prduct_id);
            $row_prod = $res_product->row();
            $prod_name = $row_prod->prod_name;
            $merchant_name = $row_prod->merchant_name;

            $sqlUpdateTransaction = "UPDATE gl_voucher SET voucher_status='Redeemed',
					 voucher_redemption_date='" . date('Y-m-d H:i:s') . "' 
					 WHERE voucher_id='$voucgerID'";
            $this->db->query($sqlUpdateTransaction);
            $generate_voucher_id = $this->mysql_uuid();
            $generate_voucher = array('voucher_id' => $generate_voucher_id,
                'voucher_purchaser_user_id' => $userID,
                'voucher_belongs_usser_id' => $friend_id,
                'voucher_merchant_id' => $prod_merchant_id,
                'voucher_product_id' => $prduct_id,
                'voucher_point' => $prod_point,
                'voucher_prod_cost' => $prod_cost,
                'voucher_reverse_point' => $prod_reverse_point,
                'voucher_code' => $voucher_code,
                'voucher_purchased_date' => date('Y-m-d H:i:s'),
                'voucher_status' => 'Consumable',
                'voucher_type' => 'Pending',
                'voucher_orginated_by' => $voucher_orginated_by,
                'voucher_expiry_date' => $this->vocher_expiration_day(7),
                'voucher_expiry_day' => $voucher_expiry_day,
                'voucher_sender_glomp_message' => addslashes($message)
            );
            $voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);

            /* update into summary table */
            $this->update_summary_sent_glopm($userID, $prod_point, $friend_id);
            $this->update_activity_sent_glomp($userID, $prod_name, $merchant_name, $rec_friend);

            if ($voucher_insert_status > 0) {
                $this->db->trans_commit();
                
                //Save to gl_invite table
                $invite_data = array(
                  'invite_by_user_id' => $userID,
                  'invite_through' => 'email',  
                  'invite_voucher_id' => $generate_voucher['voucher_id'],
                  'invite_invited_id' =>  $friend_id,
                  'invite_notclaim_voucher_message' =>  'not_claimed_voucher_message_1', //sets voucher message when not claimed
                );

                //send email to glomp receiver
                if ($newuser == "newuser") {
                    //send email to glomp receiver
                    $email_log_id = $this->sent_glomp_email_new_user($rec_friend, $prod_name, $merchant_name, $message, $userID);
                    $invite_data['invite_email_log_id'] = $email_log_id;
                    $this->invites_m->_insert($invite_data);
                } else {
                    $email_log_id = $this->sent_glomp_email($rec_friend, $prod_name, $merchant_name, $message, $userID);
                }
                    
                return json_encode(array('txn_status' => "SUCCESS", 'voucher_id' => $generate_voucher_id));
            } else {
                $this->db->trans_rollback();
                return json_encode(array('txn_status' => "unexpected_error", 'voucher_id' => ''));
                exit();
            }
        } else {
            return json_encode(array('txn_status' => "invalid_voucher"));
        }
    }

    function mysql_uuid() {
        $sql = "select UUID() as uuid";
        $res = $this->db->query($sql);
        return $res->row()->uuid;
    }

    function vocher_expiration_day($add_day) {
        $date = date('Y-m-d');
        $sql = "SELECT  ADDDATE('" . $date . "','$add_day DAY')AS new_date";
        $res = $this->db->query($sql);
        $rec = $res->row();
        return $rec->new_date;
    }

    function purchase_point($user_id, $point, $amount, $payment_method_id) {
        $uuid = $this->mysql_uuid();
        $payment = array('session_uer_id' => $user_id,
            'session_txnID' => $uuid,
            'sesion_buying_point' => $point,
            'session_amount' => $amount,
            'session_date' => date('Y-m-d H:i:s'),
            'session_payment_method_id' => $payment_method_id,
            'session_status' => '1'
        );
        $this->db->insert($payment);
        $inser_id = $this->inser_id();
        $arr = array('sesion_payment_id' => $inser_id, 'session_txnID' => $uuid);
        return $arr;
    }

    function validate_voucher_to_redeem($user_id = 0, $voucher_id = 0) {
        $whr = array('voucher_id' => $voucher_id, 'voucher_belongs_usser_id' => $user_id);
        
        $this->db->select(" gl_voucher.*, 
                            gl_outlet.outlet_name, 
                            gl_merchant.merchant_name, 
                        ");
        $this->db->join("gl_merchant", "gl_merchant.merchant_id = gl_voucher.voucher_merchant_id", 'left'); 
        $this->db->join("gl_outlet", "gl_outlet.outlet_id = gl_voucher.voucher_outlet_id", 'left'); 
        
        $res = $this->db->get_where('gl_voucher', $whr);
        return $res;
    }

    function redeem_voucher($purchaser_id, $belongs_id, $voucher_id, $outlet_id, $product_id) {
        //redeem voucher
        $time = date('Y-m-d H:i:s');
        $uuid = $this->mysql_uuid();
        $ip = $_SERVER['REMOTE_ADDR'];
        //*random voucher code*/

        $bignum = hexdec(substr(sha1(microtime()), 0, 15));
        $smallnum = (string) abs($bignum % 999999999);
        $smallnum = str_pad($smallnum, 12, '0', STR_PAD_RIGHT);
        $new_num = '';
        for ($k = 1; $k <= 9; $k++) {
            $new_num .= $smallnum[$k];
            if (($k % 3) == 0) {
                $new_num .= '-';
            }
        }

        $random_voucher_code = rtrim($new_num, '-');

        /* end of rando voucher code */

        $this->db->query('BEGIN');

        $sql_update_voucher = "UPDATE gl_voucher SET
						voucher_outlet_id = '$outlet_id',
						voucher_redemption_date = '$time',
						voucher_transactionID = '$uuid',
						voucher_status = 'Redeemed',
						voucher_redemption_ip = '$ip',
						voucher_code = '$random_voucher_code'
						WHERE voucher_id = '$voucher_id'
						AND voucher_belongs_usser_id = '$belongs_id'
						";
        $this->db->query($sql_update_voucher);
        $affected_rows = $this->db->affected_rows();
        //update into activity log
        $res_purchaser = $this->user_short_info($purchaser_id);
        $rec_purchaser = json_decode($res_purchaser);
        $purchaser_name = $rec_purchaser->user_name;
        $purchaser_email = $rec_purchaser->user_email;
        //
        $res_belongs = $this->user_short_info($belongs_id);
        $rec_belongs = json_decode($res_belongs);
        $belongs_name = $rec_belongs->user_name;

        //product info 
        $res_product = $this->product_m->productAndMerchant($product_id);
        $rec_product = $res_product->row();
        $merchant_name = $rec_product->merchant_name;
        $merchant_area = $this->regions_m->region_name($rec_product->merchant_region_id);
        $product = $rec_product->prod_name;

        $res_outlet = $this->merchant_m->outletById($outlet_id);
        $res_outlet = $res_outlet->row();
        $outlet_name = $res_outlet->outlet_name;
        $outlet_area = $this->regions_m->region_name($res_outlet->outlet_region_id);
        
        
        
        //log activity [redeemed_a_voucher]                    
        $details=array(
                'voucher_id'=>$voucher_id,                
                'product_id'=>$product_id,
                'product_name'=>$product,
                'merchant_id'=>$rec_product->prod_merchant_id,
                'merchant_name'=>$merchant_name  ,
                'outlet_id'=>$outlet_id,
                'outlet_name'=>$outlet_name,
                'outlet_area'=>$outlet_area,
                );
        $params = array(
                'user_id'=>$belongs_id,
                'type' =>'redeemed_a_voucher',
                'details' =>json_encode($details)
                );
        add_activity_log($params);
        //log activity [redeemed_a_voucher]
        
        //log activity [friend_redeemed_a_voucher]                    
        $details=array(
                'voucher_id'=>$voucher_id,                
                'friend_id'=>$belongs_id,
                'friend_name'=>$belongs_name,                
                'product_id'=>$product_id,                
                'product_name'=>$product,
                'merchant_id'=>$rec_product->prod_merchant_id,
                'merchant_name'=>$merchant_name,
                'outlet_id'=>$outlet_id,
                'outlet_name'=>$outlet_name,
                'outlet_area'=>$outlet_area,
                );
        $params = array(
                'user_id'=>$purchaser_id,
                'type' =>'friend_redeemed_a_voucher',
                'details' =>json_encode($details)
                );
        add_activity_log($params);
        //log activity [friend_redeemed_a_voucher]



        $purchaser_msg = "$belongs_name redeemed your voucher of $merchant_name $product";
        $sql = "INSERT INTO gl_activity SET
			log_user_id = '$purchaser_id',
			log_title = '" . addslashes($purchaser_msg) . "',
			log_details = '" . addslashes($purchaser_msg) . "',
			log_timestamp = '$time',
			log_ip = '$ip'
			";
        $this->db->query($sql);

        //
        $purchaser_msg = "You have successfully redeemed your $merchant_name $product Burger voucher glomp!ed by $purchaser_name";
        $sql = "INSERT INTO gl_activity SET
			log_user_id = '$belongs_id',
			log_title = '" . addslashes($purchaser_msg) . "',
			log_details = '" . addslashes($purchaser_msg) . "',
			log_timestamp = '$time',
			log_ip = '$ip'
			";

        $this->db->query($sql);
        if ($affected_rows > 0) {
            $this->db->query('COMMIT');
            $redeem_status = 'success';
        } else {
            $this->db->query('ROLLBACK');
            $redeem_status = 'failed';
        }

        $result_voucher = array('purchaser_name' => $purchaser_name,
            'purchaser_email' => $purchaser_email,
            'transactionid' => $uuid,
            'redeem_status' => $redeem_status,
            'product_name' => $product,
            'reddemed_date' => $time,
            'merchant_name' => $merchant_name,
            'merchant_area' => $merchant_area,
            'outlet_name' => $outlet_name,
            'outlet_area' => $outlet_area,
            'product_name' => $product,
            'voucher_code' => $random_voucher_code            
        );
        return $result_voucher;
    }
}

//eoc
?>