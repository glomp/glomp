<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Payment
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author	Allan Jed T. Bernabe
 */
require APPPATH . 'libraries/REST_Controller.php';

class Payment extends REST_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('users_m');
        $this->load->model('payment_m');
    }

    function generate_id_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post('user_id');
            $data['session_payment_id'] = $this->payment_m->create_payment_session_visited($user_id, 1);

            return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function checkout_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        $data = array();

        if ($user) {
            $user_id = $this->input->post('user_id');
            $session_payment_visisted_id = $this->input->post('session_payment_id');
            $amount = $this->input->post('amount');
            $point = $this->input->post('point');
            
            $res = $this->payment_m->checkout_payment_session($user_id, $point, $amount, $session_payment_visisted_id);

            if ($res === FALSE) {
                $data = array();
                $data['error'] = 'An error occur';
                return $this->response(array('success' => FALSE, 'data' => $data, 't' => $user_token), 200);
            }

            $data['amount'] = $amount;

            return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function update_token_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post('user_id');
            $session_payment_visisted_id = $this->input->post('session_payment_id');
            $paypal_token = $this->input->post('paypal_token');

            $this->payment_m->update_session_token($user_id, $session_id, $paypal_token);

            return $this->response(array('success' => TRUE, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function check_points_post(){
        $this->load->library('form_validation');
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        $i = 0;
        $point_balance = 0;
        $status = FALSE;

        $this->form_validation->set_rules('session_id', '', 'trim|required|xss_clean');
        $this->form_validation->set_rules('amount', '', 'trim|required|xss_clean');
        $this->form_validation->set_rules('token', '', 'trim|required|xss_clean');
        $this->form_validation->set_rules('user_id', '', 'trim|required|xss_clean');

        if($this->form_validation->run() == TRUE) 
        {
            $session_id = $this->input->post('session_id');
            $token = $this->input->post('token');            
            $amount = $this->input->post('amount');
            $user_id = $this->input->post('user_id');

            do{            
                $i++;
                usleep(250000); //.25 secodes                                
                //check payment if received.
                $res=$this->payment_m->check_payment_if_received($user_id, $session_id, $token, $amount);
                if($res->num_rows>0){
                    //$row=$$res->row();
                    $res_header = json_decode($this->user_account_m->user_summary($user_id));
                    $point_balance = $res_header->point_balance;
                    $status = TRUE;
                    break;
                }
                if($i == 40) {
                    break;
                }
                
            }
            while(true);
        }

        return $this->response(array('success' => $status, 'data' => array(), 't' => $user_token), 200);
    }

}