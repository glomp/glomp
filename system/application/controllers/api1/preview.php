<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author		Allan Bernabe
 */
require APPPATH . 'libraries/REST_Controller.php';

class Preview extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('merchant_m');
    }
    
    function merchant_news($id) {
        $this->load->model('template_m');
        $data['template'] = $this->merchant_m->get_template( $id );
        if( isset($data['template']->id) ) {
            $data['sections'] = $this->template_m->_get_sections( $data['template']->id );
        }
        if( isset($data['sections']) ) {
            $data['widgets'] = $this->template_m->_get_widgets( $data['sections'] );
        }
        
        //Mobile, Please see view/m/profile_menu_merchant_v.php
        $this->load->view('api/profile_menu_merchant_v', $data, true, true);
    }
}