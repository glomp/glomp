<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Glomp
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author	Allan Jed T. Bernabe
 */
require APPPATH . 'libraries/REST_Controller.php';

class Glomp extends REST_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('merchant_m');
        $this->load->model('product_m');
        $this->load->model('users_m');
        $this->load->model('login_m');
        $this->load->model('user_account_m');
        $this->load->model('send_email_m');
        $this->load->model('regions_m');
        
        //Load the email templating class
        $this->load->library('email_templating');
    }

    function glompme_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {

            $user_id = $this->input->post('user_id');
            $friend_id = $this->input->post('member_id'); //optional (non-member); required (member)
            $user_fname = $this->input->post('user_fname');
            $user_lname = $this->input->post('user_lname');
            $user_email = $this->input->post('user_email');
            $product_id = $this->input->post('product_id');
            $location_id = $this->input->post('location_id'); //optional (non-member); required (member)
            $message = $this->input->post('message'); //optional
            $password = $this->input->post('password');
            $type = $this->input->post('type');

            $status = $this->login_m->password_verify($password, $user_id);

            if ($status != "ok") {
                return $this->response(array(
                            'success' => FALSE,
                            'message' => 'Invalid password',
                            'error_code' => '006',
                            't' => $user_token), 200);
            }

            if ($type == 'member' || $type == 'non-member') {
                if ($type == 'member') {
                    $friend = $this->users_m->get_record(array(
                        'select' => 'user_fname, user_lname, user_email, user_city_id',
                        'where' => array('user_id' => $friend_id),
                    ));

                    $user_fname = $friend['user_fname'];
                    $user_lname = $friend['user_lname'];
                    $user_email = $friend['user_email'];
                    $location_id = $friend['user_city_id'];
                }

                $res = $this->_glomp_to_friend(array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'message' => $message,
                    'user_fname' => $user_fname,
                    'user_lname' => $user_lname,
                    'user_email' => $user_email,
                    'location_id' => $location_id,
                ));

                if ($res->txn_status == 'SUCCESS') {
                    return $this->response(array('success' => TRUE, 't' => $user_token), 200);
                } else if ($res->txn_status == "unexpected_error") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Unexpected error',
                                'error_code' => '007',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "insufficent_balance") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Insufficient Balance',
                                'error_code' => '008',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "invalid_product") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Invalid product',
                                'error_code' => '009',
                                't' => $user_token), 200);
                }
            } else if ($type == 'non-member-fb') {
                $res = $this->_glomp_to_FB(array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'message' => $message,
                    'user_fname' => $user_fname,
                    'user_lname' => $user_lname,
                    'FB_ID' => $friend_id,
                    'location_id' => $location_id,
                ));
                
                if ($res->txn_status == 'SUCCESS') {
                    return $this->response(array('success' => TRUE, 'data' => array('voucher_id' => $res->voucher_id), 't' => $user_token), 200);
                } else if ($res->txn_status == "unexpected_error") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Unexpected error',
                                'error_code' => '007',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "insufficent_balance") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Insufficient Balance',
                                'error_code' => '008',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "invalid_product") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Invalid product',
                                'error_code' => '009',
                                't' => $user_token), 200);
                }
                
                
            } else if ($type == 'non-member-li') {
                $res = $this->_glomp_to_LI(array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'message' => $message,
                    'user_fname' => $user_fname,
                    'user_lname' => $user_lname,
                    'LI_ID' => $friend_id,
                    'location_id' => $location_id,
                ));
                
                if ($res->txn_status == 'SUCCESS') {
                    return $this->response(array('success' => TRUE, 't' => $user_token), 200);
                } else if ($res->txn_status == "unexpected_error") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Unexpected error',
                                'error_code' => '007',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "insufficent_balance") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Insufficient Balance',
                                'error_code' => '008',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "invalid_product") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Invalid product',
                                'error_code' => '009',
                                't' => $user_token), 200);
                }
                
            } else if ($type == 'non-member-sms') {
                $res = $this->_glomp_to_sms(array(
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                    'message' => $message,
                    'user_fname' => $user_fname,
                    'user_lname' => $user_lname,
                    'mobile_number' => $friend_id,
                    'location_id' => $location_id,
                ));
                
                if ($res->txn_status == 'SUCCESS') {
                    $sms_code = site_url('u/'.$res->sms_code);
                    return $this->response(array('success' => TRUE, 't' => $user_token, 'sms_code' => $sms_code), 200);
                } else if ($res->txn_status == "unexpected_error") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Unexpected error',
                                'error_code' => '007',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "insufficent_balance") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Insufficient Balance',
                                'error_code' => '008',
                                't' => $user_token), 200);
                } else if ($res->txn_status == "invalid_product") {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'Invalid product',
                                'error_code' => '009',
                                't' => $user_token), 200);
                }
                
            }
             else {
                
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function glomped_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {

            $user_id = $this->input->post('user_id');
            $offset = $this->input->post('offset');
            $limit = $this->input->post('limit');

            $glomped_me = $this->users_m->mobile_glomped_me($user_id, $offset, $limit);
            $data = array();
            if ($glomped_me->num_rows() > 0) {
                foreach ($glomped_me->result() as $rec_glomped_me) {
                    if ($rec_glomped_me->voucher_type == 'Pending') {
                        //Skip Pending voucher type; This is a reward voucher. Pending type voucher is needed to accept first in desktop version.
                        continue;
                    }
                    $raw_data['voucher_id'] = $rec_glomped_me->voucher_id;
                    $raw_data['voucher_type'] = $rec_glomped_me->voucher_type;
                    $raw_data['voucher_status'] = $rec_glomped_me->voucher_status;
                    $raw_data['voucher_orginated_by'] = $rec_glomped_me->voucher_orginated_by; // greater than 0 means reward voucher
                    $prod_id = $rec_glomped_me->voucher_product_id;

                    $raw_data['glomped_by_name'] = $rec_glomped_me->user_name;
                    $raw_data['glomped_by_photo'] = site_url($this->custom_func->profile_pic($rec_glomped_me->user_profile_pic, $rec_glomped_me->user_gender));

                    $glomped_me_prod = json_decode($this->product_m->productInfo($prod_id));
                    $raw_data['prod_name'] = $glomped_me_prod->product->$prod_id->prod_name;
                    $raw_data['product_logo'] = $this->custom_func->product_logo($glomped_me_prod->product->$prod_id->prod_image);
                    $raw_data['product_desc'] = $this->product_m->product_desc($prod_id);


                    $merchant = $this->merchant_m->selectMerchantID($rec_glomped_me->voucher_merchant_id)->row();
                    $raw_data['merchant_id'] = $rec_glomped_me->voucher_merchant_id;
                    $raw_data['merchant_name'] = $glomped_me_prod->product->$prod_id->merchant_name;
                    $raw_data['merchant_name'] = $glomped_me_prod->product->$prod_id->merchant_name;
                    $raw_data['merchant_terms'] = trim(strip_tags($merchant->merchant_terms));
                    $raw_data['merchant_logo'] = site_url($this->custom_func->merchant_logo($glomped_me_prod->product->$prod_id->merchant_logo));
                    $raw_data['remaining_days'] = $this->custom_func->compute_remaining_days(date('Y-m-d'), ($rec_glomped_me->voucher_expiry_date));

                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function redeem_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {

            $merchant_id = $this->input->post('merchant_id');
            $voucher_id = $this->input->post('voucher_id');
            $user_id = $this->input->post('user_id');
            $outlet_code = $this->input->post('outlet_code');
            
            
            $type = $this->input->post('type');
            
            $args = $voucher_id . ';' . $merchant_id . ';' . $user_id;
            
            $this->form_validation->set_rules('type', '', 'trim|required|xss_clean|callback__redeem_type_check');
            if($type=='redeem')
                $this->form_validation->set_rules('outlet_code', '', 'trim|required|xss_clean|callback__redeem_check[' . $args . ']');
            
            $this->form_validation->set_rules('merchant_id', 'Merchant ID', 'trim|required');            

            if ($this->form_validation->run() == TRUE) {
                
                
                if($type=='view')
                {
                 
                    $this->db->select("gl_voucher.voucher_id, gl_voucher.voucher_code, gl_voucher.voucher_redemption_date, 
                            gl_outlet.outlet_name, 
                            gl_merchant.merchant_name, 
                    ");
                    $this->db->join("gl_merchant", "gl_merchant.merchant_id = gl_voucher.voucher_merchant_id"); 
                    $this->db->join("gl_outlet", "gl_outlet.outlet_id = gl_voucher.voucher_outlet_id"); 
                    $voucher = $this->db->get_where('gl_voucher',array(
                                'gl_voucher.voucher_id'=> $voucher_id
                                ));
                    
                    $data['bar_code'] = $voucher->row()->voucher_code;
                    $data['voucher_id'] = $voucher->row()->voucher_id;
                    $data['redeem_timestamp'] = $voucher->row()->voucher_redemption_date;
                    
                    $data['merchant_name'] = $voucher->row()->merchant_name;
                    $data['outlet_name'] = $voucher->row()->outlet_name;
                    
                    return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
                }
                else if($type=='redeem')
                {
                    $voucher = $this->user_account_m->validate_voucher_to_redeem( $user_id, $voucher_id );
                    $outlet = $this->product_m->verify_outlet_code($merchant_id, $outlet_code);
                    
                    $result_voucher = $this->user_account_m->redeem_voucher (
                        $voucher->row()->voucher_purchaser_user_id,
                        $user_id,
                        $voucher->row()->voucher_id,
                        $outlet->row()->outlet_id,
                        $voucher->row()->voucher_product_id 
                    );

                    if ($result_voucher['redeem_status'] == 'success') {
                        $user_rec = $this->users_m->get_record(array(
                           'select' => 'user_fname, user_lname' ,
                           'where' => array('user_id' => $user_id)
                        ));
                        
                        //Send redeem email
                        //TODO: Create an email templating class
                        $purchaser_email = $result_voucher['purchaser_email'];
                        $purchaser_name = $result_voucher['purchaser_name'];
                        $transactionID = $result_voucher['transactionid'];
                        $redeemer_name = $user_rec['user_fname'] . ' ' . $user_rec['user_lname'];
                        $merchant_name = $result_voucher['merchant_name'];

                        $product_name = $result_voucher['product_name'];

                        $link = base_url() . "profile/view/" . $user_id;
                        $hyper_link = "<a href='$link'>Check it out</a>";
                        
                        #do not send email to admin. admin id is always 1
                        if ($voucher->row()->voucher_purchaser_user_id != 1) {
                            //Send redemption email
                            $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher->row()->voucher_id))->row();
                            $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
                            $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();
                            
                            $vars = array(
                                'template_name' => 'glomp_redeem',
                                'from_name' => SITE_DEFAULT_SENDER_NAME,
                                'from_email' => SITE_DEFAULT_SENDER_EMAIL,
                                'lang_id' => 1,
                                'to' => $purchaser_email,
                                'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                                'params' => array(
                                    '[insert redeemers name]' => $redeemer_name,
                                    '[insert merchant brand and product]' => $merchant_name.' '.$product_name,
                                    '[link]' => $hyper_link,
                                ),
                                'others' => array(
                                    'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                                    'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                                )
                            );
                            $this->email_templating->config($vars);
                            $this->email_templating->send();
                            //end redemption email
                        }
                        
                        $data['bar_code'] = $result_voucher['voucher_code'];
                        $data['voucher_id'] = $voucher_id;
                        $data['redeem_timestamp'] = $result_voucher['reddemed_date'];
                        
                        $data['merchant_name'] = $voucher->row()->merchant_name;
                        $data['outlet_name'] = $voucher->row()->outlet_name;
                        $data['voucher_count'] = $this->users_m->count_active_voucher($user_id);
                        
                        
                        return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
                    }
                }/*if($type=='redeem')*/
                
            } else {
                return $this->response(array('success' => FALSE,
                    'message' => $this->form_validation->error_array(),
                    'error_code' => '010',
                    't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function _redeem_type_check()
    {
        $type = $this->input->post('type');
        if(!in_array($type,array('redeem','view')))
        {
            $this->form_validation->set_message('_redeem_type_check', 'Invalid Type');
        }
        else
        {
            return true;
        }
    }
    function _redeem_check($val, $args) {
        list($voucher_id, $merchant_id, $user_id) = explode(';', $args);
        $voucher = $this->user_account_m->validate_voucher_to_redeem($user_id, $voucher_id);

        // no voucher exists
        if ($voucher->num_rows() < 1) {
            $this->form_validation->set_message('_redeem_check', 'Invalid Voucher');
            return FALSE;
        }
        
        //Check merchant ID is same as submitted merchant ID
        if ($voucher->row()->voucher_merchant_id != $merchant_id) {
            $this->form_validation->set_message('_redeem_check', 'Merchant is not associated with the voucher');
            return FALSE;
        }

        // voucher is already redeemed/expired/reversed
        if ($voucher->row()->voucher_status != 'Consumable' && $voucher->row()->voucher_status != 'UnClaimed') {
            $msg = '';

            switch ($voucher->row()->voucher_status) {
                case 'Redeemed' :
                    $msg = $this->lang->line('voucher_alrady_reddemed_msg');
                    break;
                case 'Reversed' :
                    $msg = $this->lang->line('voucher_reversed_msg');
                    break;
                case 'Expired' :
                    $msg = $this->lang->line('voucher_already_expired_msg');
                    break;
                default :
                    $msg = $this->lang->line('invalid_voucher') . ' :2' . $voucher->row()->voucher_status;
                    break;
            }

            $this->form_validation->set_message('_redeem_check', $msg);
            return FALSE;
        }

        if ($val != '') {
            $res_outlet_verify = $this->product_m->verify_outlet_code($merchant_id, $val);

            if ($res_outlet_verify->num_rows() > 0) {

                $this->db->where('voucher_id', $voucher_id);
                $this->db->update('gl_voucher', array('voucher_outlet_id' => $res_outlet_verify->row()->outlet_id));

                return TRUE;
            }

            // log activity
            $sql_activity = "INSERT INTO gl_activity SET
                    log_user_id = '" . $user_id . "',
                    log_title = 'Redeeming: Invalid Outlet Code (" . $val . ").',
                    log_details = 'Invalid Outlet Code (" . $val . ").',
                    log_ip = '" . $_SERVER['REMOTE_ADDR'] . "',
                    log_timestamp = '" . date('Y-m-d H:i:s') . "',
                    log_device = 'mobile'
                ";
            $this->db->query($sql_activity);

            $this->form_validation->set_message('_redeem_check', $this->lang->line('incorrect_outlet_code'));

            return FALSE;
        }

        $this->form_validation->set_message('_redeem_check', '%s is required');

        return FALSE;
    }

    function _glomp_to_friend($args = array()) {
        $glomp_status = $this->user_account_m->glomp_to_friend(
                $args['user_id'], $args['product_id'], $args['message'], $args['user_fname'], $args['user_lname'], $args['user_email'], $args['location_id']);

        return json_decode($glomp_status);
    }
    
    function _glomp_to_FB($args = array()) {
        $glomp_status = $this->user_account_m->glomp_to_friend_fb(
                $args['user_id'], $args['product_id'], $args['message'], $args['user_fname'], $args['user_lname'], $args['FB_ID'], $args['location_id']);

        return json_decode($glomp_status);
    }
    
    function _glomp_to_LI($args = array()) {
        $glomp_status = $this->user_account_m->glomp_to_friend_li(
                $args['user_id'], $args['product_id'], $args['message'], $args['user_fname'], $args['user_lname'], $args['LI_ID'], $args['location_id']);

        return json_decode($glomp_status);
    }

    function _glomp_to_sms($args = array()) {
        $glomp_status = $this->user_account_m->glomp_to_sms(
                $args['user_id'], $args['product_id'], $args['message'], $args['user_fname'], $args['user_lname'], $args['mobile_number'], $args['location_id']);

        return json_decode($glomp_status);
    }

}