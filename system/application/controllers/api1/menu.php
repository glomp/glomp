<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author	Allan Jed T. Bernabe
 */
require APPPATH . 'libraries/REST_Controller.php';

class Menu extends REST_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('merchant_m');
        $this->load->model('brand_m');
        $this->load->model('product_m');
        $this->load->model('users_m');
        $this->load->model('login_m');
    }

    public function index_get() {
        return $this->response(array('success' => FALSE), 200);
    }

    function favourites_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
            $user_id = $this->input->post('user_id');
            $friend_id = $this->input->post('member_id');
            
            $data = array();
            $resFev = $this->product_m->userFevProduct($friend_id);

            if ($resFev->num_rows() > 0) {
                foreach ($resFev->result() as $recFev) {
                    $raw_data['merchant_name'] = stripslashes($this->merchant_m->merchantName($recFev->merchant_id));
                    $raw_data['product_id'] = $recFev->prod_id;
                    $raw_data['product_name'] = stripslashes($recFev->prod_name);
                    $raw_data['product_image'] = base_url() . $this->custom_func->product_logo($recFev->prod_image);
                    $raw_data['product_point'] = stripslashes($recFev->prod_point);
                    $raw_data['product_details'] = stripslashes($recFev->prod_details);

                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function merchants_post() {
        $this->load->model('regions_m');
        
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
            $member_id = $this->input->post('member_id');
            $location = $this->input->post('location');// optional: when to non-member etc. FB, linkedIn
            $merchant_id = $this->input->post('merchant_id');// optional: when specified

            $data = array();
            $user_rec = $this->users_m->get_record(array(
                'select' => 'user_city_id',
                'where' => array('user_id' => $member_id),
                'resource_id' => TRUE
            ));
            
            if ( $user_rec <> FALSE ) {
                $location_id = $user_rec->row()->user_city_id;
            }
            
            if (! empty($location)) {
                $location_id = $this->regions_m->getCountryID($location);
            }
            
            if ($merchant_id > 0) {
                $resMer = $this->merchant_m->selectMerchantID($merchant_id);
            } else {
                $resMer = $this->merchant_m->regionwise_merchant($location_id);
            }

            if ($resMer->num_rows() > 0) {
                foreach ($resMer->result() as $resMer) {
                    $merchant = $this->merchant_m->selectMerchantID($resMer->merchant_id)->row();
                    $raw_data['merchant_id'] = $resMer->merchant_id;
                    $raw_data['merchant_name'] = $resMer->merchant_name;
                    $raw_data['merchant_image'] = base_url() . $this->custom_func->merchant_logo($resMer->merchant_logo);
                    $raw_data['merchant_banner'] = '';//TODO: WHEN UPLOADED base_url(). 'custom/uploads/merchants/' . $resMer->merchant_banner;
                    $raw_data['merchant_about'] = $merchant->merchant_about;
                    $raw_data['merchant_terms'] = $merchant->merchant_terms;
                    $raw_data['merchant_news'] = site_url('api1/preview/merchant_news/'.$resMer->merchant_id);
                    $raw_data['merchant_outlets'] = array();
                    $raw_data['merchant_products'] = array();
                    
                    foreach ($this->merchant_m->selectOutletsAddress($resMer->merchant_id)->result() as $row) {
                        $raw_data['merchant_outlets'][] = array(
                            'outlet_name' => $row->outlet_name,
                            'outlet_address_1' => $row->address_1,
                            'outlet_address_2' => $row->address_2,
                            'outlet_address_street' => $row->address_street,
                            'outlet_address_locality' => $row->address_locality,
                            'outlet_address_city_town' => $row->address_city_town,
                            'outlet_address_region' => $row->address_region,
                            'outlet_address_zip_code' => $row->address_zip_code,
                            'outlet_region_name' => $row->region_name,
                            'outlet_name' => $row->outlet_name,
                        );
                    }
                    foreach ($this->product_m->product_by_merchant_id($resMer->merchant_id)->result() as $row_product) {
                        $raw_data['merchant_products'][] = array(
                            'product_id' => $row_product->prod_id,
                            'product_name' => $row_product->prod_name,
                            'product_image' => base_url() . $this->custom_func->product_logo($row_product->prod_image),
                            'product_details' => $row_product->prod_details,
                            'product_points' => $row_product->prod_point,
                            'product_in_favourites' => $this->product_m->checkAddFevitemToFevList($row_product->prod_id)
                        );
                    }
                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function products_post() {
        $this->load->model('regions_m');
        
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
            $user_id = $this->input->post('user_id');
            $friend_id = $this->input->post('member_id');
            $category_id = $this->input->post('category_id');
            $location = $this->input->post('location');// optional: when to non-member etc. FB, linkedIn
            
            $user_rec = $this->users_m->get_record(array(
                'select' => 'user_city_id',
                'where' => array('user_id' => $friend_id),
                'resource_id' => TRUE
            ));
            
            if ($user_rec <> FALSE) {
                $location_id = $user_rec->row()->user_city_id;
            }
            
            if (! empty($location)) {
                $location_id = $this->regions_m->getCountryID($location);
            }
            
            $data = array();

            $users_merchant_id_list = $this->merchant_m->regionwise_merchant_list($location_id);
            $user_specfic_product = $this->product_m->users_cat_product($users_merchant_id_list, $category_id);

            if ($user_specfic_product->num_rows() > 0) {
                foreach ($user_specfic_product->result() as $row_product) {
                    $raw_data['product_id'] = $row_product->prod_id;
                    $raw_data['merchant_name'] = $this->merchant_m->merchantName($row_product->prod_merchant_id);
                    $raw_data['product_name'] = $row_product->prod_name;
                    $raw_data['product_image'] = base_url() . $this->custom_func->product_logo($row_product->prod_image);
                    $raw_data['product_details'] = $row_product->prod_details;
                    $raw_data['product_points'] = $row_product->prod_point;
                    $raw_data['product_in_favourites'] = $this->product_m->checkAddFevitemToFevList($row_product->prod_id, $friend_id); //$friend_id can be the same as $user_id
                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
            
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    function custom_brands_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);        
        $user = $this->users_m->exists(array('user_token' => $user_token));        
        if ($user) {
            $this->load->model('brand_m','Brand');
            
            $user_id = $this->input->post('user_id');
            $data = array();
            $location = $this->db->get_where('gl_user', array('user_id' => $user_id))->row()->user_city_id;

            $this->db->order_by('name', 'asc' );
            $this->db->where(array('status'=>'InActive', 'location_id' => $location));
            $q = $this->db->get( 'gl_brand' );
            $brands = FALSE;
            if( $q->num_rows() > 0 ) {
                $brands = $q->result();
            }
            
            if ($brands != FALSE) {
                foreach ($brands as $brand) {

                    $macallan = 0;
                    $snowleopard = 0;
                    $london = 0; 

                    //TODO: Should have a one configuration file
                    switch ($brand->public_alias) {
                        case 'hsbc':
                            $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
                            $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
                            $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
                            $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
                            $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
                            $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
                            $kpo =  HSBC_BRAND_PROD_ID_KPO;
                            $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
                            $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
                            break;
                        case 'uob':
                            $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
                            $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
                            $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
                            $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
                            $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
                            $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
                            $kpo =  UOB_BRAND_PROD_ID_KPO;
                            $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
                            $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
                            break;
                        case 'dbs':
                            $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
                            $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
                            $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
                            $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
                            $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
                            $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
                            $kpo =  DBS_BRAND_PROD_ID_KPO;
                            $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
                            $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
                            break;
                    }

                    $group_product = array($macallan, $snowleopard, $london);


                    $raw_data['brand_id'] = $brand->id;
                    $raw_data['brand_alias'] = $brand->public_alias;
                    $raw_data['brand_name'] = $brand->name;
                    $raw_data['brand_thumbnail'] = base_url(). 'custom/uploads/brands/' . $brand->thumbnail;
                    $raw_data['brand_description'] = $brand->description;
                    $raw_data['brand_logo'] = base_url(). 'custom/uploads/brands/'. $brand->logo;
                    $raw_data['brand_banner'] = base_url(). 'custom/uploads/brands/'. $brand->banner;
                    $raw_data['brand_page'] = site_url('brands/page/'.$brand->id.'?api');
                    $raw_data['brand_products'] = array();

                    $brand_prods = $this->brand_m->get_brandproducts($brand->id, false, false);
                    
                    if ($brand_prods == FALSE) {
                        $data[] = $raw_data;
                        continue;
                    }
                    
                    foreach ($brand_prods as $key => $brand_prod) {
                        if (in_array($brand_prod->id, $group_product)) {
                            $group = 1;
                        } else {
                            $group = 0;
                        }

                        $raw_data['brand_products'][] = array(
                            'product_merchant_id' => $brand_prod->id,
                            'product_name' => $brand_prod->name,
                            'product_logo' => base_url(). 'custom/uploads/brands/products/' . $brand_prod->image_logo,
                            'product_banner' => $brand_prod->image_banner,
                            'product_thumbnail' => $brand_prod->image_thumbnail,
                            'product_intro_text' => $brand_prod->intro_text,
                            'product_description' => $brand_prod->description,
                            'product_group' => $group,
                            'product_related' => array(),
                        );
                        
                        $brands_prods = $this->db->get_where( 'gl_brands_products', 'brandproduct_id = '.$brand_prod->id );
                        
                        if( $brands_prods->num_rows() == 0 ) {
                            
                        } else {
                            foreach ($brands_prods->result() as $bp) {
                                $prod_detail = $this->product_m->selectProductByID($bp->product_id)->row();
                                $underage_filtered = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
                                if( in_array( $prod_detail->prod_cat_id, $underage_filtered ) && $this->underage($user_id) ) continue;

                                $prod_data = get_hsbc_prod_data($prod_detail->prod_id);
                                $prod_cost = $prod_data["sale_price"];                                
                                $has_option= $prod_data["has_options"];
                                $options= isset($prod_data["options"]) ? $prod_data["options"]: '';
                                

                                $raw_data['brand_products'][$key]['product_related'][] = array(
                                    'product_id' => $prod_detail->prod_id,
                                    'product_merchant_name' => $this->merchant_m->merchantName($prod_detail->prod_merchant_id),
                                    'product_name' => $prod_detail->prod_name,
                                    'product_image' => base_url() . $this->custom_func->product_logo($prod_detail->prod_image),
                                    'product_details' => $prod_detail->prod_details,
                                    'product_points' => $prod_detail->prod_point,
                                    'product_in_favourites' => $this->product_m->checkAddFevitemToFevList($prod_detail->prod_id),
                                    'prod_cost' => $prod_cost,
                                    'has_option' => $has_option,
                                    'options' => $options,
                                    
                                    
                                );
                            }
                        }
                    }
                    
                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    /**FOR CUSTOM BRANDS**/
    
    function brands_post() {
        
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
            $this->load->model('brand_m','Brand');
            
            $user_id = $this->input->post('user_id');
            $data = array();
            $location = $this->db->get_where('gl_user', array('user_id' => $user_id))->row()->user_city_id;

            $this->db->order_by('name', 'asc' );
            $this->db->where(array('status'=>'Active', 'location_id' => $location));
            $q = $this->db->get( 'gl_brand' );
            $brands = FALSE;
            if( $q->num_rows() > 0 ) {
                $brands = $q->result();
            }
            
            if ($brands != FALSE) {
                foreach ($brands as $brand) {
                    $raw_data['brand_id'] = $brand->id;
                    $raw_data['brand_name'] = $brand->name;
                    $raw_data['brand_thumbnail'] = base_url(). 'custom/uploads/brands/' . $brand->thumbnail;
                    $raw_data['brand_description'] = $brand->description;
                    $raw_data['brand_logo'] = base_url(). 'custom/uploads/brands/'. $brand->logo;
                    $raw_data['brand_banner'] = base_url(). 'custom/uploads/brands/'. $brand->banner;
                    $raw_data['brand_page'] = site_url('brands/page/'.$brand->id.'?api');
                    $raw_data['brand_products'] = array();
                    $brand_prods = $this->brand_m->get_brandproducts($brand->id);
                    
                    if ($brand_prods == FALSE) {
                        $data[] = $raw_data;
                        continue;
                    }
                    
                    foreach ($brand_prods as $key => $brand_prod) {
                        $raw_data['brand_products'][] = array(
                            'product_name' => $brand_prod->name,
                            'product_logo' => base_url(). 'custom/uploads/brands/products/' . $brand_prod->image_logo,
                            'product_banner' => $brand_prod->image_banner,
                            'product_thumbnail' => $brand_prod->image_thumbnail,
                            'product_intro_text' => $brand_prod->intro_text,
                            'product_description' => $brand_prod->description,
                            'product_related' => array(),
                        );
                        
                        $brands_prods = $this->db->get_where( 'gl_brands_products', 'brandproduct_id = '.$brand_prod->id );
                        
                        if( $brands_prods->num_rows() == 0 ) {
                            
                        } else {
                            foreach ($brands_prods->result() as $bp) {
                                $prod_detail = $this->product_m->selectProductByID($bp->product_id)->row();
                                $underage_filtered = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
                                if( in_array( $prod_detail->prod_cat_id, $underage_filtered ) && $this->underage($user_id) ) continue;
                                $raw_data['brand_products'][$key]['product_related'][] = array(
                                    'product_id' => $prod_detail->prod_id,
                                    'product_merchant_name' => $this->merchant_m->merchantName($prod_detail->prod_merchant_id),
                                    'product_name' => $prod_detail->prod_name,
                                    'product_image' => base_url() . $this->custom_func->product_logo($prod_detail->prod_image),
                                    'product_details' => $prod_detail->prod_details,
                                    'product_points' => $prod_detail->prod_point,
                                    'product_in_favourites' => $this->product_m->checkAddFevitemToFevList($prod_detail->prod_id)
                                );
                            }
                        }
                    }
                    
                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function update_favourite_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
            $product_id = $this->input->post('product_id');
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            if ($type == 'add') {
                $this->product_m->addFevitemToFevList_api($product_id, $user_id);
            }

            if ($type == 'remove') {
                $this->product_m->removeuserFevItem_api($product_id, $user_id);
            }

            return $this->response(array('success' => TRUE, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function underage($user_id) {
        if( $this->input->post( 'automation' ) ) return false;
        
        $this->load->model('underage_m');
        $this->load->model('users_m');
        $user = $this->users_m->user_info_by_id($user_id)->row();
        return $this->underage_m->is_underage( $user->user_dob, $user->user_city_id );
    }
    
}