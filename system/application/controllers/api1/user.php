<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author		Lou Garcia
 */
require APPPATH . 'libraries/REST_Controller.php';

class User extends REST_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('users_m');
        $this->load->model('login_m');
        $this->load->model('activity_m');
        
        //dont know if check_token function is necessary
    }

    public function index_get() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        
        $params = array(
            'where' => array('user_token' => $user_token)
        );
        $user = $this->_get_user_details($params);

        return $this->response(array('success' => TRUE, 'data' => $user, 't' => $user_token), 200);
    }
    public function set_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        
        $params = json_decode($this->input->post('params'), TRUE); //required

        $where = array(
            'where' => array('user_token' => $user_token)
        );

        $this->db->update('gl_user', $params, $where['where']);

        //get details again
        $user = $this->_get_user_details($where);
        return $this->response(array('success' => TRUE, 'data' => $user, 't' => $user_token), 200);
    }
    public function deactivate_reason_list_post()
    {
        $this->deactivate_reason_list_get();
    }
    public function deactivate_reason_list_get()
    {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user)
        {
            $reason_list[1] = "I don't understand how to use glomp!";
            $reason_list[2] = "This is temporary. I'll be back.";
            $reason_list[3] = "I have a privacy concern.";
            $reason_list[4] = "I don't find glomp! useful.";
            $reason_list[5] = "I have another glomp! account.";
            $reason_list[6] = "Other, please explain further:";
            return $this->response(array('success' => TRUE, 'data' => $reason_list, 't' => $user_token), 200);    
        }
        else
        {
            return $this->response(array('success' => FALSE), 200);
        }

    }
    public function deactivate_post()
    {
        $this->deactivate_get();
    }
    public function deactivate_get()
    {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user)
        {

            $user_id = $this->input->get_post('user_id');
             $reason = $this->input->get_post('reason', TRUE);
            $comment = $this->input->get_post('comment', TRUE);
            
            
            $reason_list[1] = "I don't understand how to use glomp!";
            $reason_list[2] = "This is temporary. I'll be back.";
            $reason_list[3] = "I have a privacy concern.";
            $reason_list[4] = "I don't find glomp! useful.";
            $reason_list[5] = "I have another glomp! account.";
            $reason_list[6] = "Other, please explain further:";
            
            $insert_data = array(
                'user_id'           =>$user_id,
                'reason'            => $reason_list[$reason],
                'comment'           => $comment,
                'date_deactivated'  =>  date('Y-m-d H:i:s')
            );

            $this->db->insert('gl_feedback', $insert_data);
            
            
            $update_data = array(
                'user_status'               => 'Pending',
                'deactivated_status'        => 1
            );
            $where = array(
                    'user_id'       =>$user_id
                    );
            $this->db->update('gl_user', $update_data, $where);
            
            
            /*send email*/
            $this->load->library('email_templating');
            
            $user = $this->db->get_where('gl_user', $where);



            $user_fname = $user->row()->user_fname;
            $user_lname = $user->row()->user_lname;
            $user_email = $user->row()->user_email;

            $link = site_url();
            $click_here ="<a href='$link'>here</a>";
            $vars = array(
                'template_name' => 'close_account',
                'lang_id' => 1,
                'to' => $user_email,
                'params' => array(
                    '[firstname_lastname]' => $user_fname.' '.$user_lname,
                    '[link]' => $click_here,
                )
            );
            $this->email_templating->config($vars);
            $this->email_templating->send();
            return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);    
            /*send email*/        
        }
        else
        {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    function register_post() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        // validation rules
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
        $this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|xss_clean');
        $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|min_length[6]required|matches[pword]');
        $this->form_validation->set_rules('agree', 'Terms & Conditions', 'trim|required');
        $this->form_validation->set_rules('promo_code', 'Promo Code', 'trim|xss_clean');
        
        
        if ($this->input->post('registration_type') == 'FB') {
            return $this->_register_FB();
        }
        
        if ($this->input->post('registration_type') == 'LI') {
            return $this->_register_LI();
        }

        if ($this->input->post('registration_type') == 'SMS') {
            return $this->_register_sms();
        }

        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }
        
        if( $this->form_validation->run() == TRUE && $email_exists == FALSE) {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_gender' => $this->input->post('gender'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_start_tour' => 'Y',
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
            );
            
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));

            $this->db->insert('gl_user', $u_data);
            $user_id = $this->db->insert_id();
            
            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();
            
            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }
            
            return $this->response(array('success' => TRUE), 200);
            
        } else {
            
            //$errors = $this->form_validation->error_array();
            $errors = trim(strip_tags(validation_errors()));
            if ($email_exists) {

                if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                        //update when there's no error
                        $extra['user_salt'] = $this->login_m->generate_hash();
                        $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                        $this->_update_pending_account($user, $extra);
                    } else if ($user->row()->user_status == 'Active') {
                        $errors .= $this->lang->line( 'email_already_exist_try_new_one', 'Email address already exists. Please try new one.' );
                    }
            }
            
            return $this->response(array(
                'success' => FALSE,
                'message' => $errors ,
                //'errors' => $errors,
                'error_code' => '005',
            ), 200);
                
        }
        
    }

    public function login_post() {
        $this->load->library('form_validation');

        $data = array(
            'data' => array(),
            'error_code' => '',
            'message' => '',
            'success' => FALSE,
            'user_status' => '',
            'reactivated' => FALSE,
            't' => ''
        );

        $login_type = $this->input->post('login_type');//required
        
        if ($login_type == 'FB') 
        {
            $data = $this->login_fb($data);
            return $this->response($data, 200);
        }

        if ($login_type == 'LI') 
        {
            $data = $this->login_li($data);
            return $this->response($data, 200);
        }

        $this->form_validation->set_rules('user_name', 'E-mail', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == TRUE)
        {
            $user_name = $this->input->post('user_name');
            $user_password = $this->input->post('user_password');

            //user exists?
            $user = $this->db->get_where('gl_user', array('username' => $user_name));
            if ($user->num_rows() > 0)
            {
                $user_password = $this->login_m->hash_key_value($user->row()->user_salt, $user_password);
                //if password is the same
                if ($user_password == $user->row()->user_password)
                {
                    $data = $this->set_session($user, $data);
                    return $this->response($data, 200);
                }
                else
                {
                    //Passsword incorrect
                    $data['error_code'] = '002';
                    $data['message'] = 'Passsword incorrect';
                }
            } 
            else
            {
                //User name / Passsword incorrect
                $data['error_code'] = '003';
                $data['message'] = 'User name / Passsword incorrect';
            }
        } else
        {
            //Validation error
            $data['error_code'] = '004';
            $data['message'] = trim(strip_tags(validation_errors()));
        }

        return $this->response($data, 200);
    }

    private function set_session($user, $return_data = array())
    {
        $set_session = FALSE;
        $ip = $_SERVER['REMOTE_ADDR'];
        $cur_date = $this->custom_func->dateOnly();
        $currentTime = $this->custom_func->datetime();

        $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
        $user_token = $this->login_m->hash_key_value($salt, $user->row()->user_password);

        //Check if email verified
        if ($user->row()->user_email_verified == 'Y')
        {
            $login_session_data = array(
                'user_id' => $user->row()->user_id,
                'username' => ($user->row()->username == "") ? $user->row()->user_email : $user->row()->username,
                'user_name' => $user->row()->user_fname . ' ' . $user->row()->user_lname,
                'user_email' => $user->row()->user_email,
                'user_fname' => $user->row()->user_fname,
                'user_lname' => $user->row()->user_lname,
                'user_email_verified' => $user->row()->user_email_verified,
                'user_last_login_date' => $user->row()->user_last_login_date,
                'user_last_login_ip' => $user->row()->user_last_login_ip,
                'lang_id' => $user->row()->user_lang_id,
                'twitter_oauth_token' => $user->row()->user_twitter_token,
                'twitter_oauth_token_secret' => $user->row()->user_twitter_secret,
                'is_user_logged_in' => TRUE,
            );

            if ($user->row()->user_status == 'Pending')
            {
                
                if ($user->row()->deactivated_status == 1)
                {
                    $set_session = TRUE;
                    //update here
                    $update_data = array(
                        'user_status'        => 'Active',
                        'deactivated_status' => 0
                    );

                    $where = array(
                        'user_id' => $user->row()->user_id
                    );

                    $this->db->update('gl_user', $update_data, $where);

                    $data['reactivated'] = TRUE;
                    $data['user_status'] = $user->row()->user_status;
                } 
                else 
                {
                    $data = array(
                        'reactivated' => FALSE,
                        'user_status'     => $user->row()->user_status
                    );

                    return $data + $return_data;
                }
            }
            else
            {
                $set_session = TRUE;
                $data = array(
                    'reactivated' => FALSE,
                    'user_status'     => $user->row()->user_status
                );
            }


            if ($set_session == TRUE) 
            {

                $update_data = array(
                    'user_last_login_date' => $cur_date,
                    'user_last_login_ip' => $ip,
                    'user_token' => $user_token,
                    'user_device_token' => $this->input->post('user_device_token'),
                );

                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $update_data);

                //Additional returned data
                $additional_data = array(
                    'profile_pic' => base_url($this->custom_func->profile_pic($user->row()->user_profile_pic, $user->row()->user_gender)),
                    'birthday' => $user->row()->user_dob,
                    'user_dob_display' => $user->row()->user_dob_display,
                    'location_id' => $user->row()->user_city_id,
                    'user_fb_id' => $user->row()->user_fb_id,
                    'user_linkedin_id' => $user->row()->user_linkedin_id,
                    'promo_details' => $this->set_promo_code($user)
                );

                //return data
                $data['success'] = TRUE;
                $data['t'] = $user_token;
                $data['data'] = $login_session_data + $additional_data;
                return $data + $return_data;
            }

        }
        else
        {
            //Email unverified
            $data['error_code'] = '001';
            $data['user_status'] = $user->row()->user_status;
            $data['message'] = 'Email unverified / Resend verfication';
            
            return $data + $return_data;
        }
    }


    private function login_fb($return_data)
    {
        $fb_id = $this->input->post('id');
        $user = $this->db->get_where('gl_user', array('user_fb_id' => $fb_id));

        if ($user->num_rows() > 0) 
        {
            return $this->set_session($user, $return_data);
        } 
        else 
        {
            //We know now that FB ID is not associated in any record; we now check if the email
            $user = $this->db->get_where('gl_user', array('user_email' => $this->input->post('email')));
            if ($user->num_rows() > 0 ) 
            {
                $data = $this->set_session($user,$return_data);
                if ($data['success'] = TRUE) 
                {
                    //Update record; FB ID into it
                    $this->db->where('user_id', $user->row()->user_id);
                    $this->db->update('gl_user', array('user_fb_id' => $this->input->post('id')));
                }
                return $data;
            } 
            else 
            {
                $return_data['success'] = TRUE;
                $return_data['message'] = 'redirect register page';
                //Redirect to register
                return $return_data;
            }
            
        }
    }

    private function login_li($return_data)
    {
        $li_id = $this->input->post('id');

        $user = $this->db->get_where('gl_user', array('user_linkedin_id' => $li_id));


        if ($user->num_rows() > 0) 
        {
            return $this->set_session($user, $return_data);
        } 
        else 
        {
            //We know now that Linked ID is not associated in any record; we now check if the email
            $user = $this->db->get_where('gl_user', array('user_email' => $this->input->post('email')));
            if ($user->num_rows() > 0 ) 
            {
                $data = $this->set_session($user, $return_data);
                if ($data['success'] = TRUE) 
                {
                    //Update record; linkedIn ID into it
                    $this->db->where('user_id', $user->row()->user_id);
                    $this->db->update('gl_user', array('user_linkedin_id' => $li_id));
                }
                return $data;
            } 
            else 
            {
                //Redirect to register
                return $return_data;
            }
        }
    }


    public function logout_get() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $where = 'user_token = "' . $user_token . '"';
            $this->users_m->_update($where, array('user_token' => '', 'user_device_token' => ''));
            return $this->response(array('success' => TRUE), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function save_promo_code_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post('user_id');
            $promo_code = $this->input->post('promo_code');
            $this->users_m->promo_code_save_to_user($promo_code, $user_id);

            $user = $this->db->get_where('gl_user', array('user_id' => $user_id));
            $promo_details = $this->set_promo_code($user);

            return $this->response(array('success' => TRUE, 'data' => $promo_details, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function glomp_buzz_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            //renew token
            // $where = 'user_token = "' . $user_token . '"';
            // $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
            // $user_token = $this->login_m->hash_key_value($salt, '$0m3$3CR3tC0D3');
            // $this->users_m->_update($where, array('user_token' => $user_token));
            //end renewal

            $user_id = $buzz_user_id = $this->input->post('user_id');
            $type = $this->input->post('type');

            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');

            if ($type == 'dashboard') {
                $res = $this->users_m->glomp_buzz($user_id, $offset, $limit);
            }

            if ($type == 'profile' || $type == 'other_profile') {
                $res = $this->users_m->glomp_buzz_specific_user($user_id, $offset, $limit);
            }

            $data = array();
            if ($res->num_rows() > 0) {
                $to_a = $this->lang->line('to_a');
                foreach ($res->result() as $glomp) {
                    $from_id = $glomp->voucher_purchaser_user_id;
                    $to_id = $glomp->voucher_belongs_usser_id;
                    $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                    /* print_r($frn_info); */
                    $ago_date = $glomp->voucher_purchased_date;

                    $from_name = '';
                    if (isset($frn_info->friends->$from_id->user_name))
                        $from_name = $frn_info->friends->$from_id->user_name;
                    if ($type <> 'other_profile') {
                        if ($buzz_user_id == $from_id) {
                            $from_name = $this->lang->line('You');
                        }
                    }

                    $to_name = '';
                    if (isset($frn_info->friends->$to_id->user_name))
                        $to_name = $frn_info->friends->$to_id->user_name;
                    if ($type <> 'other_profile') {
                        if ($buzz_user_id == $to_id) {
                            $to_name = $this->lang->line('You');
                        }
                    }

                    /* product and merchant info */
                    $prod_id = $glomp->voucher_product_id;
                    $prod_info = json_decode($this->product_m->productInfo($prod_id));
                    $prod_name = $prod_info->product->$prod_id->prod_name;
                    $merchant_name = $prod_info->product->$prod_id->merchant_name;

                    $story_type = "1";
                    if ($from_id != $user_id) {
                        $story_type = "2";
                    }

                    $raw_data['from_id'] = $from_id;
                    $raw_data['from_name'] = $from_name;
                    $raw_data['glomped_text'] = 'glomp!ed';
                    $raw_data['to_id'] = $to_id;
                    $raw_data['to_name'] = str_replace('ñ', '&ntilde', $to_name);
                    $raw_data['to_a_text'] = $to_a;
                    $raw_data['merchant_name'] = $merchant_name;
                    $raw_data['product_name'] = stripslashes($prod_name);
                    $raw_data['ago_date'] = gmdate('d-m-Y H:i:s', strtotime($ago_date));
                    $raw_data['voucher_id'] = $glomp->voucher_id;
                    $data[] = $raw_data;
                }
                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function friends_post() {
        $this->load->model('regions_m'); //load region model
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            //renew token
            // $where = 'user_token = "' . $user_token . '"';
            // $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
            // $user_token = $this->login_m->hash_key_value($salt, '$0m3$3CR3tC0D3');
            // $this->users_m->_update($where, array('user_token' => $user_token));
            //end renewal

            $keywords = $this->input->post('keywords');
            $user_id = $this->input->post('user_id');
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');
            $member_id = $this->input->post('member_id'); //optional
            
            if (! empty($member_id)) {
                $friend = $this->db->get_where('gl_user', array('user_id' => $member_id));
                
                if ($friend->num_rows() > 0 ) {
                    $recFriend = $friend->row();
                    $data = array();
                    
                    $q = "SELECT count(*) As mutual FROM gl_friends AS f
                        INNER JOIN gl_friends AS mf ON f.friend_user_friend_id = mf.friend_user_friend_id
                        WHERE f.friend_user_id = '".$user_id."'
                            AND f.freind_relation_status = 2
                            AND mf.friend_user_id = '".$recFriend->user_id."'";
                    $res= $this->db->query($q);
                    
                    $data['mutual'] = $res->row()->mutual;
                    $data['is_friend'] = $this->custom_func->checkIfFriend($user_id, $recFriend->user_id);
                    $data['profile_pic'] = base_url($this->custom_func->profile_pic($recFriend->user_profile_pic, $recFriend->user_gender));
                    $data['full_name'] = $recFriend->user_fname . ' ' . $recFriend->user_lname;
                    $data['location_name'] = $this->regions_m->region_name($recFriend->user_city_id);
                    $data['member_id'] = $recFriend->user_id;
                    $data['dob'] = $recFriend->user_dob;
                    $data['user_dob_display'] = $recFriend->user_dob_display;
                    
                    return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
                } else {
                    return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
                }
            }
            

            if ($keywords != "") {
                $subQuery = "AND (user_fname LIKE '%$keywords%' OR user_Lname LIKE '%$keywords%' OR
                             CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%')";
            } else {
                $subQuery = "";
            }

            $resFriendsList = $this->users_m->searchOwnFriends_api($keywords, $user_id, $limit, $offset);
            $sort = array();
            $data = array();
            if ($resFriendsList != "" && count($resFriendsList) > 0) {
                foreach ($resFriendsList as $k => $v) {
                    $sort['friend_data'][$k] = $v->friend_data;
                    $sort['friend_data'][$k] = $v->friend_data;
                    $sort['is_friend'][$k] = $v->is_friend;
                    $sort['mutual'][$k] = $v->mutual;
                }
                # sort by is friend and then is mutual connection desc
                array_multisort($sort['is_friend'], SORT_DESC, $sort['mutual'], SORT_DESC, $resFriendsList);
                $maxLimit = 200;
                foreach ($resFriendsList as $recFriendList) {
                    $maxLimit--;
                    if ($maxLimit < 0) {
                        break;
                    }

                    $raw_data['mutual'] = $recFriendList->mutual;
                    $raw_data['is_friend'] = $recFriendList->is_friend;
                    $recFriend = $recFriendList->friend_data;

                    $raw_data['profile_pic'] = base_url($this->custom_func->profile_pic($recFriend->user_profile_pic, $recFriend->user_gender));
                    $raw_data['full_name'] = $recFriend->user_fname . ' ' . $recFriend->user_lname;
                    $raw_data['location_name'] = $this->regions_m->region_name($recFriend->user_city_id);
                    $raw_data['member_id'] = $recFriend->user_id;
                    $raw_data['dob'] = $recFriend->user_dob;
                    $raw_data['user_dob_display'] = $recFriend->user_dob_display;

                    $data[] = $raw_data;
                }

                return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => TRUE, 'data' => array(), 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function add_friend_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $friend_id = $this->input->post('friend_id');

        $user = $this->users_m->exists(array('user_token' => $user_token));
        $friend = $this->users_m->exists(array('user_id' => $friend_id));

        if ($user) {
            //renew token
            // $where = 'user_token = "' . $user_token . '"';
            // $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
            // $user_token = $this->login_m->hash_key_value($salt, '$0m3$3CR3tC0D3');
            // $this->users_m->_update($where, array('user_token' => $user_token));
            //end renewal

            if ($friend) {
                $user_id = $this->input->post('user_id');
                $this->users_m->add_friend($user_id, $friend_id);
                return $this->response(array('success' => TRUE, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => FALSE, 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function remove_friend_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $friend_id = $this->input->post('friend_id');

        $user = $this->users_m->exists(array('user_token' => $user_token));
        $friend = $this->users_m->exists(array('user_id' => $friend_id));

        if ($user) {
            //renew token
            // $where = 'user_token = "' . $user_token . '"';
            // $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
            // $user_token = $this->login_m->hash_key_value($salt, '$0m3$3CR3tC0D3');
            // $this->users_m->_update($where, array('user_token' => $user_token));
            //end renewal

            if ($friend) {
                $user_id = $this->input->post('user_id');
                $this->users_m->remove_friend($user_id, $friend_id);
                return $this->response(array('success' => TRUE, 't' => $user_token), 200);
            } else {
                return $this->response(array('success' => FALSE, 't' => $user_token), 200);
            }
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function activity_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        
        $user = $this->users_m->exists(array('user_token' => $user_token));
        
        if ($user) {
            //renew token
            // $where = 'user_token = "' . $user_token . '"';
            // $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
            // $user_token = $this->login_m->hash_key_value($salt, '$0m3$3CR3tC0D3');
            // $this->users_m->_update($where, array('user_token' => $user_token));
            //end renewal

            $user_id = $this->input->post('user_id');
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');
            $type = $this->input->post('type');
            $data = array();

            if ($type == "points") {
                $activity = $this->activity_m->get_activity($user_id, $offset, $limit, 'points');

                foreach ($activity->result() as $rec) {
                    $details = json_decode($rec->details);
                    $raw_data['log_time'] = date('h:i a F d Y', (strtotime($rec->log_timestamp)));

                    switch ($rec->log_type) {
                        case 'points_purchased':
                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_purchased_points', array(
                                'points' => $details->balance,
                                    ), '[red]Purchased[/red] {points} points');

                            $raw_data['points'] = $details->points;
                            $raw_data['balance'] = $details->balance;

                            $data[] = $raw_data;
                            break;
                        case 'points_spent':
                            $product_id = $details->product_id;
                            $recipient_id = $details->recipient_id;
                            $info = json_decode($this->users_m->friends_info_buzz($user_id . ',' . $recipient_id));
                            $recipient_name = $info->friends->$recipient_id->user_name;

                            $points = ((int) $details->points) * -1;
                            if ($points > 0) {
                                $points = " + " . abs($points);
                            } else {
                                $points = " - " . abs($points);
                            }

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_spent_points', array(
                                'reciever' => $recipient_name,
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name,
                                'points' => $points,
                                    ), '[red]glomp![/red] {reciever} a {merchant} {product} ({points})');

                            $raw_data['reciever_id'] = $recipient_id;
                            $raw_data['reciever'] = $recipient_name;
                            $raw_data['points'] = $points;
                            $raw_data['balance'] = $details->balance;

                            $data[] = $raw_data;
                            break;
                        case 'points_reversed':
                            $voucher_id = $details->voucher_id;
                            $voucher_data = $this->users_m->get_specific_voucher_details($voucher_id);
                            $voucher_data = $voucher_data->row();

                            $product_id = $voucher_data->voucher_product_id;
                            $recipient_id = $voucher_data->voucher_belongs_usser_id;

                            $info = json_decode($this->users_m->friends_info_buzz($user_id . ',' . $recipient_id));
                            $recipient_name = $info->friends->$recipient_id->user_name;

                            $prod_info = json_decode($this->product_m->productInfo($product_id));

                            $merchant_name = $prod_info->product->$product_id->merchant_name;
                            $product_name = $prod_info->product->$product_id->prod_name;

                            $points = ((int) $details->points) * -1;
                            if ($points > 0) {
                                $points = " + " . abs($points);
                            } else if ($points < 0) {
                                $points = " - " . abs($points);
                            } else {
                                $points = " " . abs($points);
                            }

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_reversed_points', array(
                                'reciever' => $recipient_name,
                                'merchant' => $merchant_name,
                                'product' => $product_name,
                                'points' => $points,
                                    ), '[red]glomp![/red] of a {merchant} {product} was [red]unredeemed[/red] by {reciever} ({points})');

                            $raw_data['reciever_id'] = $recipient_id;
                            $raw_data['reciever'] = $recipient_name;
                            $raw_data['merchant'] = $merchant_name;
                            $raw_data['product'] = $product_name;
                            $raw_data['points'] = $points;
                            $raw_data['balance'] = $details->balance;

                            $data[] = $raw_data;
                            break;
                    }
                }
            } else {
                $activity = $this->activity_m->get_activity($user_id, $offset, $limit);
                //echo $this->db->last_query();
                foreach ($activity->result() as $rec) {
                    $details = json_decode($rec->details);
                    $raw_data['log_time'] = date('h:i a F d Y', (strtotime($rec->log_timestamp)));

                    switch ($rec->log_type) {
                        case 'glomp_someone':
                            $product_id = $details->product_id;
                            $recipient_id = $details->recipient_id;
                            $recipient_name = $details->recipient_name;

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_glomped_friend', array(
                                'reciever' => $recipient_name,
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name
                                    ), '[red]glomp!ed {reciever}[/red] a {merchant} {product}');


                            $raw_data['reciever_id'] = $recipient_id;
                            $raw_data['reciever'] = $recipient_name;
                            $raw_data['merchant'] = $details->merchant_name;
                            $raw_data['product'] = $details->product_name;


                            $data[] = $raw_data;
                            break;
                        case 'received_a_glomp':
                            $product_id = $details->product_id;
                            $glomper_id = $details->glomper_id;
                            $glomper_name = $details->glomper_name;

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_glomp_recieved', array(
                                'sender' => $glomper_name,
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name
                                    ), '[red]{sender} glomp!ed[/red] a {merchant} {product}');


                            $raw_data['sender_id'] = $glomper_id;
                            $raw_data['sender'] = $glomper_name;
                            $raw_data['merchant'] = $details->merchant_name;
                            $raw_data['product'] = $details->product_name;

                            $data[] = $raw_data;
                            break;
                        case 'received_a_reward':
                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_reward_recieved', array(
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name
                                    ), '[red]Rewarded[/red] a {merchant} {product} from [red]glomp![/red]');

                            $raw_data['merchant'] = $details->merchant_name;
                            $raw_data['product'] = $details->product_name;

                            $data[] = $raw_data;
                            break;
                        case 'redeemed_a_voucher':
                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_redeemed_voucher', array(
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name
                                    ), '[red]Redeemed[/red] a {merchant} {product}');

                            $raw_data['merchant'] = $details->merchant_name;
                            $raw_data['product'] = $details->product_name;

                            $data[] = $raw_data;
                            break;
                        case 'friend_redeemed_a_voucher':
                            $friend_id = $details->friend_id;
                            $friend_name = $details->friend_name;
                            
                            $raw_data['log_text'] = $this->lang->compound('activity_friend_redeemed', array(
                                'receiver' => $friend_name,
                                'merchant' => $details->merchant_name,
                                'product' => $details->product_name
                            ), 'Your friend [red]{receiver} redeemed[/red] a {merchant} {product}');
                            
                            $raw_data['friend_id'] = $friend_id;
                            $raw_data['friend'] = $friend_name;
                            $raw_data['merchant'] = $details->merchant_name;
                            $raw_data['product'] = $details->product_name;

                            $data[] = $raw_data;
                            break;
                        case 'friend_added_you':
                            $friend_id = $details->friend_id;
                            $friend_name = $details->friend_name;

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_friend_added_you', array(
                                'friend' => $friend_name,
                                    ), '[red]{friend}[/red] Added you to their Friends list');

                            $raw_data['friend_id'] = $friend_id;
                            $raw_data['friend'] = $friend_name;

                            $data[] = $raw_data;
                            break;
                        case 'added_a_friend':
                            $friend_id = $details->friend_id;
                            $friend_name = $details->friend_name;

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_added_friend', array(
                                'friend' => $friend_name,
                                    ), '[red]Added {friend}[/red] to your Friends list');

                            $raw_data['friend_id'] = $friend_id;
                            $raw_data['friend'] = $friend_name;

                            $data[] = $raw_data;
                            break;
                        case 'unfriend_a_friend':
                            $friend_id = $details->friend_id;
                            $friend_name = $details->friend_name;

                            $raw_data['log_text'] = $this->lang->compound(
                                    'activity_unfriend', array('friend' => $friend_name), 
                                    '[red]Removed {friend}[/red] from your Friends list');

                            $raw_data['friend_id'] = $friend_id;
                            $raw_data['friend'] = $friend_name;

                            $data[] = $raw_data;
                            break;
                        case 'invited_a_friend':
                            $friend_name = $details->invited_name;

                            if ($details->invited_through == 'facebook') {
                                $raw_data['log_text'] = $this->lang->compound(
                                        'activity_invited_facebook_friend', array(
                                    'friend' => $friend_name,
                                        ), 'Invited {friend} from Facebook to join [red]glomp![/red]');
                            }

                            if ($details->invited_through == 'linkedIn') {
                                $raw_data['log_text'] = $this->lang->compound(
                                        'activity_invited_linkedin_friend', array(
                                    'friend' => $friend_name,
                                        ), 'Invited {friend} from LinkedIn to join [red]glomp![/red]');
                            }

                            if ($details->invited_through == 'email') {
                                $raw_data['log_text'] = $this->lang->compound(
                                        'activity_invited_email_friend', array(
                                    'friend' => $friend_name,
                                        ), 'Invited {friend} through email to join [red]glomp![/red]');
                            }


                            $raw_data['friend'] = $friend_name;

                            $data[] = $raw_data;
                            break;
                    }
                }
            }

            return $this->response(array('success' => TRUE, 'data' => $data, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE), 200);
        }
    }

    public function update_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post('user_id');

            $this->form_validation->set_rules('fname', 'first Name', 'trim|required|xss_clean|callback_fname_check');
            $this->form_validation->set_rules('lname', 'last Name', 'trim|required|xss_clean|callback_lname_check');
            $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
            $this->form_validation->set_rules('months', 'Months', 'trim|required');
            $this->form_validation->set_rules('year', 'Year', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check_update|xss_clean');

            if ($this->input->post('pword') != "") {
                $this->form_validation->set_rules('pword', 'Current Password', 'trim|required|min_length[6]|max_length[9]|callback_password_check|xss_clean');
            }
            if ($this->input->post('npword') != "") {
                $this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
                $this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
            }
            if ($this->form_validation->run() == TRUE) {
                $this->login_m->update_profile($user_id);
                if (strlen($this->input->post('npword')) >= 6) {
                    //change password
                    $new_password = $this->input->post('npword');
                    $this->login_m->change_password($new_password, $user_id);
                }
               //Helper to upload profile picture 
               upload_profile_pic($this, array('register' => FALSE, 'user_id' => $user_id));
               
               if (isset($photo_data['success'])) {
                   return $this->response($photo_data, 200);
               }
            } else {
                return $this->response(array(
                    'success' => FALSE,
                    'message' => trim(strip_tags(validation_errors())),
                    'error_code' => '005',
                    't' => $user_token
                ), 200);
            }
            $params = array(
                'where' => array('user_id' => $user_id)
            );
            $user = $this->_get_user_details($params);
            
            return $this->response(array('success' => TRUE, 'data' => $user,  't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE, 't' => $user_token), 200);
        }
    }

    public function invite_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post('user_id');
            $email = $this->input->post('invite_email');
            $name = $this->input->post('invite_name');
            $message = $this->input->post('message');

            $this->form_validation->set_rules('invite_email', 'E-mail', 'trim|required|valid_email|xss_clean');
            if ($this->form_validation->run() == TRUE) {
                $res_email_check = $this->users_m->email_exist($email);
                if ($res_email_check->num_rows() > 0) {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => 'E-mail exists',
                                'error_code' => '005',
                                't' => $user_token), 200);
                } else {
                    $this->load->model('send_email_m');
                    //not exist into the database send invitation email
                    $user = $this->users_m->get_record(array(
                        'select' => 'user_fname, user_lname',
                        'where' => array('user_id' => $user_id),
                    ));
                    $user_name = $user['user_fname'] . ' ' . $user['user_lname'];

                    $to_email = $email;
                    $to_name = $name;
                    $email_message = $message;
                    $invite_subject = $user_name . ' has invited you to glomp!';
                    $invite_message = "<br/>";
                    $invite_message.= 'It\'s time for a treat! ' . $user_name . ' has added you as a friend on glomp! ';
                    $invite_message .= "<p>glomp! is a new online and mobile social networking platform where you can give and receive enjoyable treats such as a coffee, ice-cream, a beer and so on from a host of quality retail and service outlets. </p><br/><br/>So get started and <a href='" . base_url() . "'>sign up now!<a/>";
                    if ($invite_message != "") {
                        $invite_message .= '<br/>';
                        $invite_message .= '<br/><p>' . $email_message . '</p>';
                    }
                    $invite_message .= '<br/>
                    Cheers.
                    <br/>
                    The glomp! team.<br/>';

                    $this->send_email_m->sendEmail(SITE_DEFAULT_SENDER_EMAIL, SITE_DEFAULT_SENDER_NAME, $to_email, $to_name, $invite_subject, $invite_message);

                    return $this->response(array('success' => TRUE, 't' => $user_token), 200);
                }
            } else {
                return $this->response(array(
                            'success' => FALSE,
                            'message' => trim(strip_tags(validation_errors())),
                            'error_code' => '005',
                            't' => $user_token)
                                , 200);
            }
        } else {
            return $this->response(array('success' => FALSE, 't' => $user_token), 200);
        }
    }

    public function resend_verification_get() {
        $this->load->helper('email');
        $this->load->library('email_templating');
        
        $to_email = $_GET['email'];
        if (valid_email($to_email)) {
            $tempResult = $this->login_m->check_if_registered_email($to_email);
            if ($tempResult->num_rows() > 0) {
                $resData = $tempResult->row();
                $to_name = $resData->user_fname . ' ' . $resData->user_lname;
                $link = site_url('landing/verify/' . $resData->user_hash_key . '?uid=' . $resData->user_id);
                $click_here = "<a href='$link'>click</a>";
                $vars = array(
                    'template_name' => 'new_signup',
                    'lang_id' => 1,
                    'to' => $to_email,
                    'params' => array(
                        '[link]' => $click_here
                    )
                );
                $this->email_templating->config($vars);
                $this->email_templating->send();

                return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('Please_check_your_welcome_email', 'Please check your welcome email to activate your account.')
                                ), 200);
            } else {
                return $this->response(array(
                            'success' => FALSE,
                            'message' => $this->lang->line('invalid_email', 'Invalid E-mail.')
                                ), 200);
            }
        } else {
            return $this->response(array(
                        'success' => FALSE,
                        'message' => $this->lang->line('invalid_email', 'Invalid E-mail.')
                            ), 200);
        }
    }

    public function reset_password_post() {
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $this->load->library('email_templating');
            $email_address = $this->input->post('user_email');
            $email_check_num = $this->login_m->email_validateion($email_address);
            if ($email_check_num == 1) {
                //email password										
                $json_msg = $this->login_m->reset_password_info($email_address);
                $msg = json_decode($json_msg);

                if ($msg->valid_email == 'true') {
                    $this->login_m->change_password($msg->plain_password, $msg->user_id);
                    //send email to user with email link
                    $to_email = $msg->user_email;
                    $to_name = $msg->user_fname;

                    $link = site_url();
                    $click_here = "<a href='$link'>Click here</a>";

                    $vars = array(
                        'template_name' => 'forgot_password',
                        'lang_id' => 1,
                        'to' => $to_email,
                        'params' => array(
                            '[link]' => $click_here,
                            '[user_fname]' => $to_name,
                            '[password]' => $msg->plain_password
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->send();

                    return $this->response(array(
                                'success' => TRUE,
                                'message' => $this->lang->line('reset_password_successful_msg', 'A new password has been sent to the email address provided')
                                    ), 200);
                } else {
                    return $this->response(array(
                                'success' => FALSE,
                                'message' => $this->lang->line('Unexpected_Error', 'Unexpected error occured. Please try again.')
                                    ), 200);
                }

                //email password
            } else {
                return $this->response(array(
                            'success' => FALSE,
                            'message' => $this->lang->line('reset_password_email_doesnot_exist', 'This email does not exist in our system')
                                ), 200);
            }
        }
    }

    public function support_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required|');
            $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

            if ($this->form_validation->run() == TRUE) {
                $this->load->model('send_email_m');
                
                $user_id = $this->input->post('user_id');

                $user_info = $this->users_m->user_info_by_id($user_id);
                $rec_user_info = $user_info->row();

                $subject = "Support Email: " . $this->input->post('subject');
                $message_ = $this->input->post('message');
                //send email
                $from_email = $rec_user_info->user_email;
                $from_name = $rec_user_info->user_name;

                //TODO: uncomment this when live
                $to_email = 'rogenesagmit@gmail.com';//SITE_DEFAULT_SUPPORT_EMAIL;
                $to_name = SITE_DEFAULT_SUPPORT_NAME;

                $message = "<p>Dear Admin,</p>";
                $message .= "<br/><br/>Support email from glomp. Sender Details<br/>
                            <p>Sender Name:$from_name <br/>
                            Sender Email: $from_email
                            </p>
                            <strong>Details Message:</strong>
                ";
                $message .= $message_;
                $message .= '<br/><br/>
                Cheers.
                <br/>
                The glomp! team.';

                $this->send_email_m->sendEmail($from_email, $from_name, $to_email, $to_name, $subject, $message);

                return $this->response(array(
                    'success' => TRUE,
                    't' => $user_token
                ), 200);

            } else {
                return $this->response(array(
                    'success' => FALSE,
                    'message' => trim(strip_tags(validation_errors())),
                    't' => $user_token
                ), 200);
            }
        }
    }
    
    public function check_friendlist_status_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        
        if ($user) {
            $social_type = array(
                'fb' => 'user_fb_id',
                'linkedin' => 'user_linkedin_id',
                'sms' => 'user_mobile',
            );
            
            $user_id = $this->input->post('user_id');
            $type = $this->input->post('type');
            $ids = explode(',', $this->input->post('ids'));
            
            $first_loop = TRUE;
            $temp_table_statment = '';
            
            foreach($ids as $id) {
                if ($first_loop) {
                    $temp_table_statment = 'SELECT "'. $id. '" AS social_id ';
                } else {
                    $temp_table_statment .= ' UNION ALL SELECT "'.$id.'"';
                }
                $first_loop = FALSE;
            }
            //TODO: I think we must set user table to utf8_general_ci as a default
            $this->db->query("SET collation_connection = latin1_swedish_ci"); // TODO: workaround on Illegal mix of collations
            $sql = "SELECT social_id, u.user_id AS friend_user_id, IF(ISNULL(f.friend_user_id),0,1) AS is_friend, IF(ISNULL(u.user_id),0,1) AS is_member
                FROM (".$temp_table_statment.") AS temp
                LEFT JOIN gl_user u ON u.".$social_type[$type]." = temp.social_id
                LEFT JOIN gl_friends f ON (friend_user_id='".$user_id."' AND friend_user_friend_id = u.user_id)";

            return $this->response(array('success' => TRUE, 'data' => $this->db->query($sql)->result_array(), 't' => $user_token), 200);
            
        } else {
            return $this->response(array(
                    'success' => FALSE,
                    't' => $user_token
                ), 200);
        }
    }
    
    function link_account_post() {
        $user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        
        if ($user) {
            if ($this->input->post('type') == 'FB') {
                return $this->_link_fb_account($user_token);
            }
            if ($this->input->post('type') == 'LI') {
                return $this->_link_li_account($user_token);
            }
            
        } else {
            return $this->response(array(
                'success' => FALSE,
                't' => $user_token
            ), 200);
        }
    }

    function _link_fb_account($user_token) {
        if (isset($_POST['id']) && isset($_POST['email'])) {
            $user_fb_id = $this->input->post('id');
            $user_email = $this->input->post('email');
            $importOnly = $this->input->post('importOnly');
            $user_id = $this->input->post('user_id');
            
            $tempResult = $this->login_m->check_if_fb_user_is_registered_fb($user_fb_id);
            if ($tempResult->num_rows() == 0) {
                $tempResult = $this->login_m->check_if_fb_user_is_registered_email($user_email);
                if ($tempResult->num_rows() == 0) {
                    $this->login_m->user_fb_login_update_account($user_id);
                    
                    if ($importOnly == 'yes') {
                        $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                        $this->login_m->updatePhotoOrig($this->_get_facebook_photo($url), $user_id);
                    }
                    
                    return $this->response(array(
                        'success' => TRUE,
                        'message' => $this->lang->line('facebook_account_already_connected','Your Facebook account is now linked. Invite your Facebook friends now.'),
                        't' => $user_token
                    ), 200);
                    
                } else {

                    $resData = $tempResult->row();
                    if ($this->session->userdata('user_id') == $resData->user_id) {
                        //this account is already registered to you.					
                        $this->login_m->user_fb_login_update_account($resData->user_id);

                        if ($importOnly == 'yes') {
                            $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                            $this->login_m->updatePhotoOrig($this->_get_facebook_photo($url), $resData->user_id);
                        }
                        
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('facebook_now_linked', 'Your Facebook account is now linked. Invite your Facebook friends now.'),
                            't' => $user_token
                        ), 200);
                    } else {
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('facebook_account_already_connected', 'This Facebook account is already connected to another glomp! account.'),
                            't' => $user_token
                        ), 200);
                    }
                }
            } else {
                $resData = $tempResult->row();
                if ($this->session->userdata('user_id') == $resData->user_id) {
                    //this account is already registered to you.										
                    if ($importOnly == 'yes') {
                        $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                        
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('facebook_profile_photo_has_been_imported', 'Your Facebook profile photo has been imported.'),
                            't' => $user_token
                        ), 200);
                        
                        $this->login_m->updatePhoto($this->_get_facebook_photo($url), $resData->user_id);
                    }//import only yes
                    else {
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('facebook_account_already_connected_your_account', 'This Facebook account is already connected to your glomp! account.'),
                            't' => $user_token
                        ), 200);
                    }
                } else {
                    return $this->response(array(
                        'success' => TRUE,
                        'message' => $this->lang->line('facebook_account_already_connected', 'This Facebook account is already connected to another glomp! account.'),
                        't' => $user_token
                    ), 200);
                }
            }
        }
    }
    
    function _link_li_account($user_token) {
        if (isset($_POST['id']) && isset($_POST['email'])) {
            $user_linkedin_id = $this->input->post('id');
            $user_email = $this->input->post('email');
            $url = $this->input->post('pic');
            $importOnly = $this->input->post('importOnly');
            $user_id = $this->input->post('user_id');

            $where = array('user_linkedin_id' => $user_linkedin_id, 'user_status' => 'Active');
            $tempResult = $this->users_m->get_record(array('where' => $where));

            if ($tempResult == FALSE) {
                $where = array('user_email' => $user_email, 'user_status' => 'Active');
                $tempResult = $this->users_m->get_record(array('where' => $where));

                if ($tempResult == FALSE) { 

                    $up_data = array(
                        'user_linkedin_id' => $user_linkedin_id
                    );

                    $where = array(
                        'user_id' => $user_id
                    );

                    $this->users_m->_update($where, $up_data);

                    if ($importOnly == 'yes') {

                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        curl_exec($ch);
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);

                        unlink($localfile);
                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }


                        $data = json_decode($filedata, true);
                        $url = $data["data"]["url"];

                        /* Extract the filename */
                        $filename = substr($url, strrpos($url, '/') + 1);
                        $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                        $ext = strrchr($safe_filename, '.');
                        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                        /* Save file wherever you want */
                        $user_photo = $new_photo_name . $ext;

                        //get data
                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        curl_exec($ch);
                        //get mime type incase there is no file extension
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);
                        unlink($localfile);
                        //get data

                        file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                        $user_photo = $new_photo_name . $ext;

                        $this->login_m->updatePhoto($user_photo, $user_id);
                        $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                        $user_photo_orig = $user_photo;
                        $this->login_m->updatePhotoOrig($user_photo_orig, $user_id);
                    }
                    return $this->response(array(
                        'success' => TRUE,
                        'message' => $this->lang->line('linkedin_now_linked','Your LinkedIn account is now linked. Invite your LinkedIn friends now.'),
                        't' => $user_token
                    ), 200);
                } else {
                    if ($user_id == $tempResult['user_id']) {
                        //this account is already registered to you.					
                        $up_data = array(
                            'user_linkedin_id' => $user_linkedin_id
                        );

                        $where = array(
                            'user_id' => $tempResult['user_id']
                        );

                        if ($importOnly == 'yes') {
                            //Dont know this
                        } else {
                            $this->users_m->_update($where, $up_data);
                            
                            return $this->response(array(
                                'success' => TRUE,
                                'message' => $this->lang->line('linkedin_now_linked','Your LinkedIn account is now linked. Invite your LinkedIn friends now.'),
                                't' => $user_token
                            ), 200);
                        }

                        if ($importOnly == 'yes') {
                            /* Extract the filename */
                            $filename = substr($url, strrpos($url, '/') + 1);
                            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                            $ext = strrchr($safe_filename, '.');
                            $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                            /* Save file wherever you want */
                            $user_photo = $new_photo_name . $ext;

                            //get data
                            $localfile = 'custom/uploads/users/mytempfilename.ext';

                            // Let's go cURLing...
                            $ch = curl_init($url);
                            $fp = fopen($localfile, 'w');

                            curl_setopt($ch, CURLOPT_FILE, $fp);
                            curl_setopt($ch, CURLOPT_HEADER, 0);

                            curl_exec($ch);
                            //get mime type incase there is no file extension
                            $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                            curl_close($ch);
                            fclose($fp);

                            // Get the data into memory and delete the temp file
                            $filedata = file_get_contents($localfile);
                            unlink($localfile);
                            //get data

                            if (empty($ext)) {
                                //get file extension base on mime type
                                $ext = mime_to_file_extension($mime_type);
                            }

                            file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);
                            $user_photo = $new_photo_name . $ext;

                            $this->login_m->updatePhoto($user_photo, $tempResult['user_id']);
                            $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                            $user_photo_orig = $user_photo;
                            $this->login_m->updatePhotoOrig($user_photo_orig, $tempResult['user_id']);
                            
                            return $this->response(array(
                                'success' => TRUE,
                                'message' => $this->lang->line('linkedin_profile_photo_has_been_imported','Your LinkedIn profile photo has been imported.'),
                                't' => $user_token
                            ), 200);
                        }
                    } else {
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('linkedin_account_already_connected','This LinkedIn account is already connected to another glomp! account.'),
                            't' => $user_token
                        ), 200);
                    }
                }
            } else {
                if ($user_id == $tempResult['user_id']) {
                    //this account is already registered to you.										
                    if ($importOnly == 'yes') {
                        $response['status'] = 'Ok';
                        $response['data'] = 'importOnly';
                        $response['message'] = 'Your LinkedIn profile photo has been imported.';

                        /* Extract the filename */
                        $filename = substr($url, strrpos($url, '/') + 1);
                        $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                        $ext = strrchr($safe_filename, '.');
                        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                        /* Save file wherever you want */
                        $user_photo = $new_photo_name . $ext;

                        //get data
                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        //get mime type incase there is no file extension
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);
                        unlink($localfile);
                        //get data

                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }

                        file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                        $user_photo = $new_photo_name . $ext;

                        $this->login_m->updatePhoto($user_photo, $tempResult['user_id']);
                        $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                        $user_photo_orig = $user_photo;
                        $this->login_m->updatePhotoOrig($user_photo_orig, $tempResult['user_id']);
                        
                        return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('linkedin_profile_photo_has_been_imported','Your LinkedIn profile photo has been imported.'),
                            't' => $user_token
                        ), 200);
                    }//import only yes
                    else {
                        return $this->response(array(
                                'success' => TRUE,
                                'message' => $this->lang->line('linkedin_account_already_connected_your_account','This LinkedIn account is already connected to your glomp! account.'),
                                't' => $user_token
                        ), 200);
                    }
                } else {
                    return $this->response(array(
                            'success' => TRUE,
                            'message' => $this->lang->line('linkedin_account_already_connected_your_account','This LinkedIn account is already connected to your glomp! account.'),
                            't' => $user_token
                    ), 200);
                }
            }
        }
    }

    /* Non API functions */

    function set_promo_code($user) {

        $promo_details = array(
            'message' => array(),
            'status' => '',
            'rule' => '',
            'voucher_type' => '',
        );

        $promo_code = $this->users_m->get_promo_code_status($user->row()->user_id);
        if ($promo_code != '') {
            $promo_code = json_decode($promo_code);
            $promo_details['status'] = $promo_code->status;
            //set corresponding message per promo status
            $messages = array(
                'Invalid' => $this->lang->line('PROMO_CODE_DESC_INVALID'),
                'Invalid_2' => $this->lang->line('PROMO_CODE_DESC_INVALID_2'), //When retried after invalid promo code
                'DoneAlready' => $this->lang->line('PROMO_CODE_DESC_ENDED_ALREADY'),
                'ForMemberOnly' => $this->lang->line('PROMO_CODE_DESC_FOR_MEMBER_ONLY'),
                'DidNoTPassedTheRule' => $this->lang->line('PROMO_CODE_DESC_FOR_MEMBER_ONLY'),
                'NoVoucherLeft' => $this->lang->line('PROMO_CODE_DESC_NO_VOUCHER_LEFT'),
                'SuccessPrimary' => $this->lang->line('PROMO_CODE_DESC_SUCCESS_PRIMARY'),
                'SuccessFollowUp' => $this->lang->line('PROMO_CODE_DESC_SUCCESS_FOLLOW_UP')
            );
            
            if(isset($promo_code->campaign_id) && is_numeric($promo_code->campaign_id)) {
                $promo_details['rule'] = $this->users_m->get_promo_rule_desc($promo_code->campaign_id);
                //Replace customized tags
                $messages[$promo_code->status] = str_replace('[rule_criteria]', $promo_details['rule'], $messages[$promo_code->status]);
            }

            if (isset($promo_code->status) &&  ($promo_code->status=='SuccessPrimary' || $promo_code->status=='SuccessFollowUp')) {
                $this->db->select('voucher_product_id, voucher_type');
                $voucher = $this->db->get_where('gl_voucher', array('voucher_id' => $promo_code->voucher_id));
                $res_info_prod = $this->product_m->productInfo_2($voucher->row()->voucher_product_id);
                $res_info_prod = json_decode($res_info_prod, TRUE);
                //Add voucher type
                $promo_details['voucher_type'] = $voucher->row()->voucher_type;
                if ($voucher->row()->voucher_type == 'Assignable') {
                    $messages[$promo_code->status] = $this->lang->line('PROMO_CODE_DESC_SUCCESS_ASSIGNABLE');
                }
                //Replace customized tags
                $messages[$promo_code->status] = str_replace('[brand_name]', $res_info_prod['product']['merchant_name'], $messages[$promo_code->status]);
                $messages[$promo_code->status] = str_replace('[product_name]', $res_info_prod['product']['prod_name'], $messages[$promo_code->status]);
            }

            //For all messages that are not Invalid
            $promo_details['message'] = array(
                $promo_code->status => stripslashes(strip_tags($messages[$promo_code->status]))
            );

            //Special case since it needs two messages
            if ($promo_code->status == 'Invalid') {
                $promo_details['message'] = array(
                    $promo_code->status => stripslashes(strip_tags($messages[$promo_code->status])),
                    'Invalid_2' => stripslashes(strip_tags($messages['Invalid_2'])),
                );
            }       
        }

        unset($promo_details['rule']); //Removing to returned data
        return $promo_details;
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = array('crop', 'l'), $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    public function check_email($email) {
        $email_chk = $this->login_m->emailExist($email);
        if ($email_chk == 0) {
            return true;
        } else {
            $this->form_validation->set_message('check_email', 'Email exists');
            return false;
        }
    }

    function email_check_update($email) {
        $email_chk = $this->login_m->update_email_exist_check($email, $this->input->post('user_id'));
        if ($email_chk == 0)
            return true;
        else {
            $this->form_validation->set_message('email_check_update', 'Email address already exist. Please try new one');
            return false;
        }
    }

    function fname_check($name) {

        $not_allowed = '+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/'; //1234567890
        if (str_contains($name, $not_allowed) == false) {
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('fname_check', 'Special characters are not allow on First Name.');
            return false;
        } else {
            //  echo "ok:".$username; 
            $not_allowed = '1234567890'; //
            if (str_contains($name, $not_allowed) == false) {
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('fname_check', 'Numbers are not allow on First Name.');
                return false;
            } else {
                //  echo "ok:".$username; 
                return true;
            }
        }
    }

    function lname_check($name) {

        $not_allowed = '+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/'; //1234567890
        if (str_contains($name, $not_allowed) == false) {
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('lname_check', 'Special characters are not allowed on Last Name.');
            return false;
        } else {
            //  echo "ok:".$username; 
            $not_allowed = '1234567890'; //
            if (str_contains($name, $not_allowed) == false) {
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('lname_check', 'Numbers are not allowed on Last Name.');
                return false;
            } else {
                //  echo "ok:".$username; 
                return true;
            }
        }
    }

    function password_check($password) {
        if ($password == "")
            return 'ok';
        //
        $password_verify = $this->login_m->password_verify($password, $this->input->post('user_id'));
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check', 'Invalid current password.');
            return false;
        }
    }

    /**
     * Checks if an inactive account with the given email
     * is already existing, then flag the form to
     * Update that record instead of inserting a new record
     */
    private function inactive_email_exists($email) {
        $this->db->where(array('user_email' => $email, 'user_status' => 'Pending'))->or_where('user_fb_email', $email);
        $q = $this->db->get('gl_user');
        if (count($q->row()))
            return $q->row();

        return FALSE;
    }

    private function generate_token() {
        $salt = $this->login_m->generate_hash() . 'glomp' . date('dmyhis');
        $user_token = $this->login_m->hash_key_value($salt, 'P@$$w0!>');
        return $user_token;
    }
    
    /**
     * upload facebook/linkedin photo
     * 
     * @todo rename this method since it is also used in linkedin
     */
    private function get_social_photo() {
        $url = $this->input->post('social_media_photo');
        $filename = substr($url, strrpos($url, '/') + 1);
        $upload_folder = str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/users/';
        $safe_filename = preg_replace(
                     array("/\s+/", "/[^-\.\w]+/"),
                     array("_", ""),
                     trim($filename));
        $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];
        file_put_contents($upload_folder.$new_photo_name.'.jpg', file_get_contents($url));
        
        $this->load->library('image_lib');
        
        $this->image_lib->initialize(array(
            'image_library' => "GD2",
            'source_image' => $upload_folder.$new_photo_name.'.jpg',
            'quality' => '80%',
            'new_image' => $upload_folder.'thumb/'.$new_photo_name.'.jpg',
            'width' => 140,
            'height' => 140
        ));
        
        $this->image_lib->resize();
        
        return $new_photo_name.'.jpg';
    }

    /* encapsulated methods */
    private function _register_FB() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }

        if( $this->form_validation->run() == FALSE || $email_exists) {
            $errors = trim(strip_tags(validation_errors()));

            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    //Assumed that FBID is not attached to other member
                    if (is_null($user->row()->user_fb_id)) {
                        //Then return status login
                        return $this->response(array(
                            'success' => TRUE,
                            'status' => 'login'
                        ), 200);
                    }

                    $errors .= $this->lang->line( 'email_already_exist_try_new_one_1', 'Email already exist please try a new one.' );

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_fb_id'] = $this->input->post('social_id');
                    $extra['user_profile_pic'] = $this->get_social_photo();
                    $extra['user_profile_pic_orig'] = $extra['user_profile_pic'];
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }

            }

            return $this->response(array(
                'success' => FALSE,
                'message' => $errors,
                'status' => 'login'
            ), 200);

        } else {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
                'user_fb_id' => $this->input->post('social_id'),
                'user_profile_pic' => $this->get_social_photo(),
            );

            $u_data['user_profile_pic_orig'] = $u_data['user_profile_pic'];
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));


            //Check if there's an existing pending account and Facebook ID
            $user = $this->db->get_where('gl_user', array('user_status' => 'Pending', 'user_fb_id' => $u_data['user_fb_id']));
            if ($user->num_rows() > 0 ) {
                //Update record
                $user_id = $user->row()->user_id;
                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $u_data);
            } else {
                $this->db->insert('gl_user', $u_data);
                $user_id = $this->db->insert_id();
            }

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();

            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }

            return $this->response(array(
                'success' => TRUE,
                'message' => '',
                'status' => 'register'
            ), 200);
        }
    }
    
    private function _register_LI() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }
        
        if( $this->form_validation->run() == FALSE || $email_exists) {
            $errors = trim(strip_tags(validation_errors()));

            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    //Assumed that LinkedIN ID is not attached to other member
                    if (is_null($user->row()->user_linkedin_id)) {
                        //Then return status login
                        return $this->response(array(
                            'success' => TRUE,
                            'status' => 'login'
                        ), 200);
                    }

                    $errors .= $this->lang->line( 'email_already_exist_try_new_one_1', 'Email already exist please try a new one.' );

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_linkedin_id'] = $this->input->post('social_id');
                    $extra['user_profile_pic'] = $this->get_social_photo();
                    $extra['user_profile_pic_orig'] = $extra['user_profile_pic'];
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }
            }

            return $this->response(array(
                'success' => FALSE,
                'message' => $errors,
                'status' => 'login'
            ), 200);
        } else {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
                'user_linkedin_id' => $this->input->post('social_id'),
                'user_profile_pic' => $this->get_social_photo(),
            );

            $u_data['user_profile_pic_orig'] = $u_data['user_profile_pic'];
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));


            //Check if there's an existing pending account and linkedIn ID
            $user = $this->db->get_where('gl_user', array('user_status' => 'Pending', 'user_linkedin_id' => $u_data['user_linkedin_id']));
            if ($user->num_rows() > 0 ) {
                //Update record
                $user_id = $user->row()->user_id;
                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $u_data);
            } else {
                $this->db->insert('gl_user', $u_data);
                $user_id = $this->db->insert_id();
            }

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();

            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }

            return $this->response(array(
                'success' => TRUE,
                'message' => '',
                'status' => 'register'
            ), 200);
        }
    }


    private function _register_sms() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        $user = $this->db->get_where('gl_user', array('user_email' => $email));

        //Prepare sms code to validate
        $sms_code = $this->input->post('sms_code');
        $hash_key = substr($sms_code, 0, 4);
        $mobile_number = substr($sms_code, -4);
        
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }

        if( $this->form_validation->run() == FALSE || $email_exists) {
            $errors = trim(strip_tags(validation_errors()));

            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    $errors .= $this->lang->line( 'email_already_exist_try_new_one_1', 'Email already exist please try a new one.' );

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }

            }

            return $this->response(array(
                'success' => FALSE,
                'message' => $errors,
                'status' => 'login'
            ), 200);

        } else {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
            );

            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));

            //Check if there's an existing pending account for SMS
            $this->db->like('user_hash_key', $hash_key, 'after');
            $this->db->like('user_mobile', $mobile_number, 'before');
            $this->db->where('user_status', 'Pending');
            $user = $this->db->get('gl_user');
            
            if ($user->num_rows() > 0 ) {
                //Update record
                $user_id = $user->row()->user_id;
                $u_data['user_hash_key'] = $user->row()->user_hash_key; //just get the existing record
                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $u_data);
            } else {
                $this->db->insert('gl_user', $u_data);
                $user_id = $this->db->insert_id();
            }

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();

            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }

            return $this->response(array(
                'success' => TRUE,
                'message' => '',
                'status' => 'register'
            ), 200);
        }
    }

    function _update_pending_account($user, $extra = array()) {
        //check if pending status then just update status into active and send an welcome email
        if ($user->row()->user_status == 'Pending') {
            $u_data = array(
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
            );

            //merge data
            $u_data = $u_data  + $extra;

            $this->db->where('user_id', $user->row()->user_id);
            $this->db->update('gl_user', $u_data);

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $user->row()->user_email,
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user->row()->user_id).'" >click</a>'
                )
            ));

            $this->email_templating->send();
            
            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user->row()->user_id);
            }
            
            return $this->response(array(
                'success' => TRUE,
                'status' => 'registered'
            ), 200);
        }
    }    
    
    private function _login_fb() {
        $user_fb_id = "";
        $user_email = "";

        if (isset($_POST['id']) && isset($_POST['email'])) {
            $user_fb_id = $_POST['id'];
            $user_email = $_POST['email'];
        }

        if ($this->login_m->check_if_fb_user_is_registered($user_fb_id) == 0) {

            $tempResult = $this->login_m->check_if_fb_user_is_registered_email($user_email);

            if ($tempResult->num_rows() == 0) {
                return $this->response(array(
                            'success' => TRUE,
                            'message' => 'redirect register page',
                                ), 200);
            } else {
                $resData = $tempResult->row();
                if ($user_fb_id == $resData->user_fb_id) {
                    $login_result = $this->login_m->login_using_fb_id($user_fb_id);
                    $login = json_decode($login_result);

                    $user_info = $this->users_m->get_record(array('where' => array('user_id' => $login->user_id), 'resource_id' => TRUE))->row();
                    $login->profile_pic = base_url($this->custom_func->profile_pic($user_info->user_profile_pic, $user_info->user_gender));
                    $login->birthday = $user_info->user_dob;
                    $login->user_dob_display = $user_info->user_dob_display;
                    $login->location_id = $user_info->user_city_id;

                    if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                        $user_token = $this->generate_token();
                        $where = 'user_id = "' . $login->user_id . '"';
                        $user_device_token = $this->input->post('user_device_token');
                        
                        $this->users_m->_update($where, array('user_token' => $user_token, 'user_device_token' => $user_device_token));

                        return $this->response(array(
                                    'success' => TRUE,
                                    'data' => $login,
                                    't' => $user_token
                                        ), 200);
                    } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                        return $this->response(array(
                                    'success' => TRUE,
                                    'message' => 'resend verification code',
                                        ), 200);
                    }
                } else {
                    //facebook email has a glomp account but not connected with facebook
                    //$data['error_msg'] = "Invalid E-mail or passworasdasdasdd";	
                    //update the user account and connect them with facebook
                    $this->login_m->user_fb_login_update_account($resData->user_id);
                    $login_result = $this->login_m->login_using_fb_id($user_fb_id);
                    $login = json_decode($login_result);

                    $user_info = $this->users_m->get_record(array('where' => array('user_id' => $login->user_id), 'resource_id' => TRUE))->row();
                    $login->profile_pic = base_url($this->custom_func->profile_pic($user_info->user_profile_pic, $user_info->user_gender));
                    $login->birthday = $user_info->user_dob;
                    $login->user_dob_display = $user_info->user_dob_display;
                    $login->location_id = $user_info->user_city_id;

                    if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                        $user_token = $this->generate_token();
                        $where = 'user_id = "' . $login->user_id . '"';
                        $this->users_m->_update($where, array('user_token' => $user_token));

                        return $this->response(array(
                                    'success' => TRUE,
                                    'data' => $login,
                                    't' => $user_token
                                        ), 200);
                    } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                        return $this->response(array(
                                    'success' => TRUE,
                                    'message' => 'resend verification code',
                                        ), 200);
                    }
                }
            }
        } else {
            //get username then login
            $login_result = $this->login_m->login_using_fb_id($user_fb_id);
            $login = json_decode($login_result);

            if ($login->user_status == 'Active') {
                if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                    $user_info = $this->users_m->get_record(array('where' => array('user_id' => $login->user_id), 'resource_id' => TRUE))->row();
                    $login->profile_pic = base_url($this->custom_func->profile_pic($user_info->user_profile_pic, $user_info->user_gender));
                    $login->birthday = $user_info->user_dob;
                    $login->user_dob_display = $user_info->user_dob_display;
                    $login->location_id = $user_info->user_city_id;
                    $login->reactivated =FALSE;
                    
                    $user_token = $this->generate_token();
                    $where = 'user_id = "' . $login->user_id . '"';
                    $this->users_m->_update($where, array('user_token' => $user_token));

                    return $this->response(array(
                                'success' => TRUE,
                                'data' => $login,
                                't' => $user_token
                                    ), 200);
                } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                    return $this->response(array(
                                'success' => TRUE,
                                'message' => 'resend verification code',
                                    ), 200);
                }
            } else if ($login->user_status == 'Pending')
            {

                 /*check if deactivated*/
                $q = $this->db->get_where( 'gl_user', "user_id = '".$login->user_id."' AND deactivated_status=1");
                if( $q->num_rows()  > 0)
                {
                    //return $q->row();
                    $update_data = array(
                        'user_status'               => 'Active',
                        'deactivated_status'        => 0
                    );
                    $where = array(
                            'user_id'       =>$login->user_id
                            );
                    $this->db->update('gl_user', $update_data, $where);
                    
                    $login_result = $this->login_m->login_using_fb_id($user_fb_id);         
                    $login = json_decode($login_result);
                    
                     $user_info = $this->users_m->get_record(array('where' => array('user_id' => $login->user_id), 'resource_id' => TRUE))->row();
                    $login->profile_pic = base_url($this->custom_func->profile_pic($user_info->user_profile_pic, $user_info->user_gender));
                    $login->birthday = $user_info->user_dob;
                    $login->user_dob_display = $user_info->user_dob_display;
                    $login->location_id = $user_info->user_city_id;
                    $login->reactivated =TRUE;

                    $user_token = $this->generate_token();
                    $where = 'user_id = "' . $login->user_id . '"';
                    $this->users_m->_update($where, array('user_token' => $user_token));

                    return $this->response(array(
                                'success' => TRUE,
                                'data' => $login,
                                't' => $user_token
                                    ), 200);
                    
                }
                //deactivated_status
                /*check if deactivated*/


                return $this->response(array(
                            'success' => TRUE,
                            'message' => 'redirect register page',
                                ), 200);
            }
        }
    }

    private function _login_li() {
        $_data = json_decode($_POST['data'], TRUE);

        $user_linkedin_id = $_data['id'];
        $data = $this->login_m->_process_linked_in('json');

        if ($data['status'] == 'login') {
            $login_result = $this->login_m->_login_using_socialapp($user_linkedin_id, 'user_linkedin_id');
            $login = json_decode($login_result);

            $user_info = $this->users_m->get_record(array('where' => array('user_id' => $login->user_id), 'resource_id' => TRUE))->row();
            $login->profile_pic = base_url($this->custom_func->profile_pic($user_info->user_profile_pic, $user_info->user_gender));
            $login->birthday = $user_info->user_dob;
            $login->user_dob_display = $user_info->user_dob_display;
            $login->location_id = $user_info->user_city_id;

            $user_token = $this->generate_token();
            $user_device_token = $this->input->post('user_device_token');
            
            $where = 'user_id = "' . $login->user_id . '"';
            $this->users_m->_update($where, array('user_token' => $user_token, 'user_device_token' => $user_device_token));

            return $this->response(array(
                        'success' => TRUE,
                        'data' => $login,
                        't' => $user_token
                            ), 200);
        } else if ($data['status'] == 'register') {
            return $this->response(array(
                        'success' => TRUE,
                        'message' => 'redirect register page',
                            ), 200);
        } else {
            return $this->response(array(
                        'success' => TRUE,
                        'message' => 'resend verification code',
                            ), 200);
        }
    }
    
    private function _get_user_details($params) {
        $user = $this->users_m->get_record($params);

        if ($user <> FALSE) {
            unset($user['user_password']);
            unset($user['user_salt']);
            unset($user['user_hash_key']);
            unset($user['user_token']);
            
            $user['profile_pic'] = base_url($this->custom_func->profile_pic($user['user_profile_pic'], $user['user_gender']));
            
            $res_header = json_decode($this->user_account_m->user_summary($user['user_id']));
            $user['voucher_count'] = $this->users_m->count_active_voucher($user['user_id']);
            $user['point_balance'] = $res_header->point_balance;
            
            return $user;
        }
        
        return array();
    }
}