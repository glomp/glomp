<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Glomp
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author	Ryan Magbanua
 */
require APPPATH . 'libraries/REST_Controller.php';

class Brand extends REST_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('Brand_m', 'Brand');
        $this->load->model('users_m');
        $this->load->model('login_m');
        $this->load->library('custom_func');
        $this->load->model('cmspages_m');
    }


    public function add_to_cart_post()
	{
		$user_token = $this->input->get_request_header("Auth-token");
        check_token($user_token, $this);
        $user = $this->users_m->exists(array('user_token' => $user_token));
        if ($user) {
    		$prod_id = $this->input->post('prod_id');
			$qty = $this->input->post('qty');
			$prod_merchant_cost = $this->input->post('prod_merchant_cost');
			$prod_data = $this->input->post('prod_data');
			$brandproduct_id = $this->input->post('brandproduct_id');

			$public_alias = $this->input->post('public_alias');

			$flavors_data = $this->input->post('flavors_data');
			$session_cart_id =$this->input->post('session_cart_id');
			
			// first check if it has a cart session
			//prod_merchant_cost
				
			//print_r($prod_data_decoded->brandproduct_id);
		
			if($session_cart_id == "")
			{
				/// create mysql_uuid() 
				$session_cart_id = $this->mysql_uuid();
			}

			
			$params = array( 
						'session_cart_id' => $session_cart_id,
						'prod_id'	  	  => $prod_id,
						'qty' 			  => $qty,
						'prod_merchant_cost' 			  => $prod_merchant_cost,
						'prod_data' 			  => $prod_data,
						'flavors_data' 			  => $flavors_data,
						'public_alias' 			  => $public_alias,
						'brandproduct_id' 			  => $brandproduct_id,
						);
			$cart_qty = $this->add_to_cart_database($params);			
			
		
			return $this->response(array(
										'success' => TRUE, 
										'cart_qty' => $cart_qty, 
										'session_cart_id' => $session_cart_id,
										'session_cart_qty' => $cart_qty,
										), 200);
        }
        else {
            return $this->response(array('success' => FALSE), 200);
        }
	}
	private function add_to_cart_database($params)
	{
		// first check if session and item already exists on the database.
		$delivery_type	= $this->get_default_delivery_type($params['brandproduct_id']);

		$sql = "SELECT * 
				FROM gl_cart 
				WHERE 
					session_cart_id = '".$params['session_cart_id']."' AND 
					prod_id='".$params['prod_id']."' AND 
					public_alias='".$params['public_alias']."' 
				";
        $result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			if($params['flavors_data'] =="") // sabe ni allan			
			{
				// just update the qty	
				$where = array(
	            'id' => $result->row()->id
				);
				
				$new_qty = $result->row()->prod_qty + $params['qty'];
				
				if($new_qty>10)
					$new_qty=10;
				
				
				$update_data = array('prod_qty' => ( $new_qty));
				$this->db->update('gl_cart', $update_data, $where);
				//echo "update";

			}
			else
			{
				/*
				1. loop sa lahat ng product cart items
				2. on each loop get product options
				3. compare sa params options.
				4. if may found, update product cart. if no, insert new data
				*/
				$found = false;
				foreach( $result->result() as $row )
				{

					$sql = "SELECT * 
							FROM gl_cart_option
							WHERE 
								session_cart_id = '".$params['session_cart_id']."' AND 
								prod_id='".$params['prod_id']."' AND
								prod_group='".$row->prod_group."' ";
		    		$cart_option = $this->db->query($sql);
		    		$flavors_data = json_decode($params['flavors_data']);
		    		$flavors_count = count($flavors_data);

		    		$same_options = false; 
		    		if($cart_option->num_rows()  == $flavors_count )
		    		{
		    			$same_options = true; 
						foreach( $cart_option->result() as $option )
						{
							$option_found= false;
							foreach($flavors_data as $flavor)
							{
								if (
										$option->option_type == 'flavor' &&
										$option->option_value == $flavor->name &&
										$option->option_qty == $flavor->qty
									)
								{
									$option_found = true;
								}

							}
							if(!$option_found)
							{
								$same_options = false;
								break;
							}
						}
		    		}
		    		if($same_options)
		    		{
	    				$found = $row;
	    				break;
		    		}
				}

				if($found === false)
				{

					// insert new product and option
					//get max group id
					$group_id = 1;
					$sql = "SELECT MAX(prod_group) as m
							FROM gl_cart
							WHERE 
								session_cart_id = '".$params['session_cart_id']."' AND 
								prod_id='".$params['prod_id']."'
								";
					$result = $this->db->query($sql);
					if($result->num_rows() > 0)
					{
						$group_id = $result->row()->m +1;
					}
					//get max group id



					//insert new data
					$insert_data = array(
						'session_cart_id'	=> $params['session_cart_id'],
						'prod_id' 			=> $params['prod_id'],
						'prod_qty' 			=> $params['qty'],
						'prod_cost'			=> $params['prod_merchant_cost'],
						'prod_details' 		=> $params['prod_data'],
						'prod_group'		=> $group_id,
						'public_alias' 		=> $params['public_alias'],
						'delivery_type' 	=> $delivery_type,
						'brandproduct_id'	=> $params['brandproduct_id']
					);
					
					$this->db->insert('gl_cart', $insert_data);

					foreach($flavors_data as $flavor)
					{
						//insert new data
						//print_r($flavor);
						$insert_data = array (
							'session_cart_id'	=> $params['session_cart_id'],
							'prod_id' 			=> $params['prod_id'],
							'option_type' 		=>'flavor',
							'option_value'		=> $flavor->name,
							'option_qty' 		=> $flavor->qty,
							'prod_group'		=> $group_id
						);
						$this->db->insert('gl_cart_option', $insert_data);

					}

					// insert new product and option

				}
				else
				{

					// update product and option
					$where = array(
		            'id' => $found->id
					);
					
					$new_qty = $found->prod_qty + $params['qty'];
					
					if($new_qty>10)
						$new_qty=10;
					
					
					$update_data = array('prod_qty' => ( $new_qty));
					$this->db->update('gl_cart', $update_data, $where);
					// update product and option

				}
			}
		}
		else
		{
			//insert new data
			$insert_data = array(
				'session_cart_id'	=> $params['session_cart_id'],
				'prod_id' 			=> $params['prod_id'],
				'prod_qty' 			=> $params['qty'],
				'prod_cost'			=> $params['prod_merchant_cost'],
				'prod_details' 		=> $params['prod_data'],
				'prod_group'		=> 1,
				'public_alias' 		=> $params['public_alias'],
				'delivery_type' 	=> $delivery_type,
				'brandproduct_id'	=> $params['brandproduct_id']
			);			
			$this->db->insert('gl_cart', $insert_data);
			//echo "insert";

			if($params['flavors_data'] !="")
			{
				$flavors_data = json_decode($params['flavors_data']);
				foreach($flavors_data as $flavor)
				{
					//insert new data
					//print_r($flavor);
					$insert_data = array (
						'session_cart_id'	=> $params['session_cart_id'],
						'prod_id' 			=> $params['prod_id'],
						'option_type' 		=>'flavor',
						'option_value'		=> $flavor->name,
						'option_qty' 		=> $flavor->qty,
						'prod_group'		=> 1,						
					);
					$this->db->insert('gl_cart_option', $insert_data);

				}
			}











		}
		
        //log campaign
        $details = array('name' => 'prod_id','value' => $params['prod_id']);
        $this->log_amex_action('added_to_cart', $details, $params['public_alias']);
        //log campaign
		
		$this->check_winery($params['session_cart_id']);
		
		
		
		//count item qty
		$sql = "SELECT 
				SUM(prod_qty) as qty 
				FROM gl_cart 
				WHERE 
					session_cart_id = '".$params['session_cart_id']."' AND
					public_alias = '".$params['public_alias']."' 
				";
        $result = $this->db->query($sql);
		return $result->row()->qty;
	
	}
	private function log_amex_action($action,$details=FALSE, $campaign_label='amex')
    {
    	return;
    	$this_device = 'ios';
        $temp = $this->session->userdata('public_user_data');
        
        $email = (isset($temp['user_email'])) ?  $temp['user_email']: '';
            
        
        $insert_data = array(
            'user_id' => $this->users_m->user_id_by_email($email),
            'campaign_label' => $campaign_label,
            'action' => $action,
            'platform' => $this_device,
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('gl_campaign_whitelabel_data', $insert_data);
        $insert_id = $this->db->insert_id();
        
        if($details!=FALSE)
        {
            $details['campaign_whitelabel_id'] = $insert_id;
            $this->db->insert('gl_campaign_whitelabel_details', $details);
        }
        
    }
	private function mysql_uuid()
	{
        $sql = "select UUID() as uuid";
        $res = $this->db->query($sql);
        return $res->row()->uuid;
    }

}