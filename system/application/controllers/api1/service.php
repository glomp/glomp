<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Service
 *
 * @package		
 * @subpackage	
 * @category	Controller
 * @author      Allan Jed T. Bernabe
 */
require APPPATH . 'libraries/REST_Controller.php';

class Service extends REST_Controller {

    function __construct() {
        parent::__Construct();
    }

    public function translation_get() {
        $this->load->model('translation_m');
        $this->load->model('lang_m');
        
        $data = array();
        $get_translation = $this->input->get('get_translation');
        $version = $this->custom_func->config_value('TRANSLATION_VERSION');
        
        if ( empty($get_translation)) {
            return $this->response(array('success' => FALSE), 200);
        }
        
        if ($this->custom_func->config_value('TRANSLATION_VERSION') == $get_translation) {
            return $this->response(array('success' => TRUE, 'data' => $data, 'message' => 'updated', 'current_version' => $version), 200);
        }

        $join_lang_details = array('table' => 'gl_trans_detail trans_details'
            , 'condition' => 'trans_details.trans_detail_trans_id = trans.trans_id');
        
        $join_lang = array('table' => 'gl_language lang'
            , 'condition' => 'lang.lang_id = trans_details.trans_detail_language_id');
        
        $params = array(
            'select' => 'lang.lang_name as LANGUAGE, trans.trans_label_name as LANG_KEY, trans_detail_translation_string as LANG_VALUE',
            'join' => array($join_lang_details, $join_lang),
            'order_by' => 'lang.lang_name DESC'
        );
        
        $languages = $this->lang_m->get_list();
        foreach ($languages as $lang) {
            $params['where'] = array('lang.lang_id' => $lang['lang_id']);
            
            $translations = $this->translation_m->get_list($params);
            $data[$lang['lang_name']] = $translations;
        }
        
        
        return $this->response(array('success' => TRUE,'data' => $data, 'message' => 'outdated', 'current_version' => $version), 200);
    }

   function update_notification_post() {
       $this->load->model('push_notifications_m');
       $this->load->model('users_m');
       
       $user_token = $this->input->get_request_header("Auth-token");
       check_token($user_token, $this);
       $user = $this->users_m->exists(array('user_token' => $user_token));

        if ($user) {
            $user_id = $this->input->post("user_id");
            $this->push_notifications_m->_update(array('user_id' => $user_id),
                array(
                    'status' => 'seen',
                    'modified' => date('Y-m-d H:i:s')
            ));
            
            return $this->response(array('success' => TRUE, 't' => $user_token), 200);
        } else {
            return $this->response(array('success' => FALSE, 't' => $user_token), 200);
        }
   }
   
   function fb_data_get() {
        $this->load->library('simple_html_dom');
        $fb_data = array();
        $param = $this->input->get("param"); //must be comma separated value
        $param = explode(',', $param);

        $url = 'https://login.facebook.com/login.php?login_attempt=1';  

        $email = 'testfour.glomp@gmail.com';  
        $pass = 'GlompTest123_fb';
        $post = 'charset_test='.htmlspecialchars("&euro;,&acute;,â‚¬,Â´,æ°´,Ð”,Ð„").'&lsd=OsC-Z&locale=en_US&email='.$email.'&pass='.$pass.'&persistent=1&default_persistent=0';

        foreach ($param as $scope_id) {
          $data = fb_curl($url, TRUE, TRUE, $post, 7, $scope_id);
          preg_match('/"entity_id":"(\d+)"/', $data, $match);
          if (count($match) == 0) continue;

          $fbid = $match[1];//str_get_html($data)->find('meta[property=al:android:url]', 0);
          $username = (str_get_html($data) == FALSE) ? '' : str_get_html($data)->find('meta[http-equiv=refresh]', 0);

          //save to an array
          $fb_data[] = array(
              'id' => $fbid,//str_replace("fb://profile/", "", $fbid->content),
              'username' => ($username == '') ? '' : trim(preg_replace(array("/0; URL=\//", "/\?_fb_noscript=1/"), "", $username->content))
          );
        }
        /*$data = array(
            'fbid' => $fbids,
            'username' => trim(preg_replace(array("/0; URL=\//", "/\?_fb_noscript=1/"), "", $username->content))
        );*/

        return $this->response(array('success' => TRUE, 'data' => $fb_data), 200);
   }
   
   function push_test_get() {
       /* TYPES:
        *  1 - REWARD
        *  2 - GLOMP!ED
        *  3 - NEWS
        *  4 - REDEEMED
        *  5 - ADDED_YOU
        *  6 - POINTS_RETURNED
       */
       
       $type = $_GET['type'];
       $id = $_GET['id']; // FRIEND ID
       
       $args['user_email'] = $_GET['email'];
       $args['type'] = $type;
       $args['message'] = "TEST";
       $args['params'] = array(
           'type' => $type,
           'id' => $id,
        );

       push_notification($args);
   }
}