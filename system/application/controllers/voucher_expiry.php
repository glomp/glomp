<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Voucher_expiry extends CI_Controller {
	function __construct()
	{
		parent::__Construct();
		$this->load->model('users_m');
	}
	function index()
	{
		#Step1	 check for expired voucher but not rededmped
		#setep2 restore the voucher reverse point into the purchaser account
		#Step3 update voucher record
		#setp4 update user summary
		$today = date('Y-m-d');
		
		  $sql_select = "SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_reverse_point,
					voucher_expiry_date,
					voucher_product_id 
					FROM gl_voucher
					WHERE
					voucher_status = 'Consumable'
					AND voucher_orginated_by = 0
					AND DATEDIFF(voucher_expiry_date,'$today')<=-1 
				";
		$res_expired_voucher = $this->db->query($sql_select);
		 $num_rows = $res_expired_voucher->num_rows();

		if($num_rows>0)
		{
			foreach($res_expired_voucher->result() as $row_voucher){
				$voucher_id = $row_voucher->voucher_id;
				$purchaser_id = $row_voucher->voucher_purchaser_user_id;
				$belongs_id = $row_voucher->voucher_belongs_usser_id;
				$product_id = $row_voucher->voucher_product_id;
				$reversed_point = (int)$row_voucher->voucher_reverse_point;
			//start transaction
			$this->db->query('BEGIN');
			$time = date('Y-m-d H:i:s');
			$ip = $_SERVER['REMOTE_ADDR'];
			//INSERT into user account and user summary table
			 $sql =  "INSERT INTO gl_account SET
							account_id =  UUID(),
							account_user_id='$purchaser_id',
							account_txn_type='CR',
							account_point='$reversed_point',
							account_balance_point = (account_balance_point+$reversed_point),
							account_txn_type_id = '3',
							account_details = 'Voucher did not reddedmed and reversed to your account',
							account_txnID = UUID(),
							account_txn_date = '$time',
							account_txn_ip = '$ip'
					";
				$account_insert_id = $this->db->query($sql);
			
			
			//now update into account summary table.
			$sql_update = "UPDATE gl_activity_summary SET
						 	summary_point_balance = (summary_point_balance+$reversed_point)
							WHERE summary_user_id = '$purchaser_id'
						 ";
			$this->db->query($sql_update);
			$summary_affected_id = $this->db->affected_rows();
			
			//update voucher
			$sql_update_voucher = "UPDATE gl_voucher SET
									voucher_status = 'Expired',
									voucher_transactionID = UUID(),
									voucher_note = 'Voucher expired and point reversed to the purchaser account at $time'
									WHERE voucher_id = '$voucher_id'
									";
			$this->db->query($sql_update_voucher);
			$voucher_affected_id = $this->db->affected_rows();
			//now insert into activity log
			$sql_activity = "INSERT INTO gl_activity SET
							log_user_id = '$purchaser_id',
							log_title = 'Voucher reversed',
							log_details = 'Voucher has been reversed and glomp $reversed_point has been added into 
												your account at $time. Reversed voucher id is: $voucher_id',
							log_ip = '$ip',
							log_timestamp = '$time',
							log_device = 'cron-job'
							";
			$this->db->query($sql_activity);
			if($voucher_affected_id>0 && $summary_affected_id>0 && $account_insert_id>0)
			{
				$this->db->query('COMMIT');	

			}else
			{
					$this->db->query('ROLLBACK');		
			}
			
			}//foreach close
		
		}
		$this->campaign_voucher();
	
	}//function end
function campaign_voucher()
	{
	
	$time =  date('Y-m-d H:i:s');
	$ip = $_SERVER['REMOTE_ADDR'];
	$today = date('Y-m-d');
	 $sql_select = "SELECT voucher_id,
					voucher_purchaser_user_id,
					voucher_belongs_usser_id,
					voucher_product_id ,
					voucher_orginated_by,
					voucher_status
					FROM gl_voucher
					WHERE
					voucher_status IN('Consumable','Assigned')
					AND voucher_orginated_by>0 
					AND DATEDIFF(voucher_expiry_date,'$today')<=-1 
				";
		$res_expired_voucher = $this->db->query($sql_select);
		$num_rows = $res_expired_voucher->num_rows();

		if($num_rows>0)
		{
			foreach($res_expired_voucher->result() as $row_voucher)
			{
				$voucher_id = $row_voucher->voucher_id;
				$winner_user_id = $row_voucher->voucher_belongs_usser_id;
				$product_id = $row_voucher->voucher_product_id;
				$campaign_id = $row_voucher->voucher_orginated_by;
				$voucher_type = ($row_voucher->voucher_status=='Consumable')?'Consumable':'Assignable';
			//start transaction
			$this->db->query('BEGIN');
			
				$sql_update_voucher = "UPDATE gl_voucher SET
									voucher_status = 'Expired',
									voucher_transactionID = UUID(),
									voucher_note = 'Voucher expired'
									WHERE voucher_id = '$voucher_id'
									";
			$this->db->query($sql_update_voucher);
			$voucher_affected_id = $this->db->affected_rows();
			//recycled the expired voucher. ie. again insert into the same campaign with same attributes
			
			$sql_insert_voucher = "INSERT INTO gl_campaign_voucher SET
					camp_voucher_id = UUID(),
					camp_campaign_id = '$campaign_id',
					camp_prod_id = '$product_id',
					camp_voucher_assign_status = 'UnAssigned',
					camp_voucher_type = '$voucher_type',
					generated_date  = '$time',
					camp_voucher_note = 'recycled voucher'
					";
			$insert_voucher = $this->db->query($sql_insert_voucher);
			
			//now insert into activity log
			$sql_activity = "INSERT INTO gl_activity SET
							log_user_id = '$winner_user_id',
							log_title = 'Voucher expired',
							log_details = 'Voucher has been expired.Expired voucher id is: $voucher_id',
							log_ip = '$ip',
							log_timestamp = '$time',
							log_device = 'cron-job'
							";
			$this->db->query($sql_activity);
			
			
			if($voucher_affected_id>0 && $insert_voucher>0)
			{
				$this->db->query('COMMIT');	

			}else
			{
					$this->db->query('ROLLBACK');		
			}
			}//foreach clsoe
			
	}//end of num rows
}//function end
}//class end
