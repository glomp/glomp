<?php

class Register extends CI_Controller {
    
    /**
     * Facebook ID
     *
     * Used when a registrant is glomp!ed from Facebook
     */
    public static $fb_id = 0;
    
    /**
     * LinkedIn ID
     *
     * Used when a registrant is glomp!ed from LinkedIn
     */
    public static $li_id = 0;

    /**
     * User ID
     *
     * When a user is glomp!ed from social media, their information
     * is recorded in the database with pending status. This is the ID
     * of that record
     */
    public static $user_id = 0;
    
    private $has_inactive_facebook_account;
    
    private $has_inactive_linkedin_account;
    
    /**
     * Class Constructor
     * 
     */
    public function __construct() {
    
        parent::__construct();
        $this->load->model('login_m');
        $this->load->model('regions_m');
        $this->load->helper('url');
        $this->load->library('user_agent');
        
//        if( !$this->session->userdata( 'id_user' ) ) redirect('/');
        if( isset( $_GET['fb_id'] ) ) {
        }
        self::$user_id = ( $this->session->userdata( 'id_user' ) ) ? $this->session->userdata( 'id_user' ) : 0;
    }
    
    /**
     * The registration page
     *
     * @todo remove Landing::register()
     * @todo under image upload, maybe resize/crop on first request?
     */
    public function index(){
        
        $by_pass_email_check = FALSE;

        // validation rules
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_check_fname');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean|callback_check_lname');
        $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_check_dob');
        $this->form_validation->set_rules('month', 'Months', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_check_email|xss_clean');
        $this->form_validation->set_rules('pword', 'Password', 'trim|required|callback_check_password|xss_clean');
        $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');
        $this->form_validation->set_rules('agree', 'Terms & Conditions', 'trim|required');
        $this->form_validation->set_rules('li_user_id', 'Merge LinkedIn Account', 'trim');
        $this->form_validation->set_rules('fb_user_id', 'Merge Facebook Account', 'trim');
        if( $this->input->post('fb_picture') == '' )
        {
            if(! isset($_POST['ie9']))
                $this->form_validation->set_rules('user_photo', 'Photo', 'trim|callback_check_user_photo');
            else
                $this->form_validation->set_rules('profile_pic_latest', 'Photo', 'trim|required');
        }
        
        
        $this->form_validation->set_rules('token', 'Token', 'trim|required|callback_check_token');
        
        if( $this->form_validation->run() == TRUE ) {
            
            /* We upload the photo first since it is very much required */
            if( ( $this->input->post('fb_user_id') || $this->input->post('li_user_id') ) && $this->input->post('fb_picture') ) {
                $photo = $this->get_facebook_photo();
            } elseif ($this->input->post('fb_picture')) {
                $photo = $this->get_facebook_photo();
            } else
            {
                if(! isset($_POST['ie9']))
                {
                        $photo = $this->upload_photo();
                }
                else {
                    $photo =TRUE;
                }
            }
            
            if( $photo === FALSE ) {
                $errors = array(
                    'user_photo' => 'Unable to upload your selected photo. Please choose another.'
                );
                echo json_encode( array('errors'=>$errors,'success'=>'false') );
                return;
            }
            
            // user data
            $user = new stdClass(); // an empty object
            $gender = $this->input->post('gender');
            
            $user->user_id = 0;
            $user->user_email = $this->input->post('email');
            $user->user_email_verified = 'N';
            $user->username = $this->input->post('email');
            $user->user_fname = $this->input->post('fname');
            $user->user_lname = $this->input->post('lname');
            $user->user_dob = $this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day');
            $user->user_gender = (empty($gender)) ? NULL : $gender;
            $user->user_city_id = $this->input->post('location');
            $user->user_salt = $this->login_m->generate_hash();
            $user->user_hash_key = $this->login_m->generate_hash(25);
            $user->user_password = $this->login_m->hash_key_value( $user->user_salt, $this->input->post('pword') );
            $user->user_status = 'Active';
            $user->user_last_updated_date = gmdate('Y-m-d H:i:s');
            $user->user_last_login_ip = $this->user_account_created_ip = $_SERVER['REMOTE_ADDR'];
            $user->user_dob_display = $this->input->post('dob_display');
            $user->user_sign_up_promo_code = $this->input->post('promo_code');
            $user->user_join_date = gmdate('Y-m-d H:i:s');
            $user->user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer');
            $user->user_profile_pic = $user->user_profile_pic_orig = $photo;
            $user->user_photo_crop = $this->input->post('coords'); // for first request resize/cropping

            // facebook fields
            $user->user_fb_id = $this->input->post('fb_user_id');
            $user->user_fb_email = $this->input->post('fb_email');
            $user->user_fb_fname = $this->input->post('fb_fname');
            $user->user_fb_mname = $this->input->post('fb_mname');
            $user->user_fb_lname = $this->input->post('fb_lname');
            $user->user_fb_gender = $this->input->post('fb_gender');
            $user->user_fb_dob = $this->input->post('fb_birthdate');
            $user->user_fb_nofify_via_fb = 'N';
            
            
            // linkedin fields
            $user->user_linkedin_id = $this->input->post('li_user_id');
            /*$user->user_li_email = $this->input->post('li_emailAddress');
            $user->user_li_fname = $this->input->post('li_firstName');
            $user->user_li_lname = $this->input->post('li_lastName');*/
            
            // check if email entered has a pending record, update that record instead
            $x_user = $this->inactive_email_exists( $this->input->post('email') , $this->input->post('fb_user_id') );
            if( $x_user ) {
                $id = $x_user->user_id;
                $user = (object) array_merge( (array) $x_user, (array) $user );
                unset($user->user_id);
                $this->db->update('gl_user',$user, array('user_id'=>$id) );
                $user->user_id = $id;
            } else {
                $this->db->insert( 'gl_user', $user );
                $user->user_id = $this->db->insert_id();
            }
            
            
            //copy photo for ie9 only
            if(isset($_POST['ie9']))
            {
                if($_POST['profile_pic_latest']!="")
                {
                    $user_photo=$this->input->post('profile_pic_latest');						
                    if(file_exists("custom/uploads/users/temp/".$user_photo))
                    {
                        copy ("custom/uploads/users/temp/".$user_photo,"custom/uploads/users/thumb/".$user_photo);
                        unlink("custom/uploads/users/temp/".$user_photo);
                    }
                    $this->login_m->updatePhoto($user_photo,$user->user_id);						 
                    
                    $user_photo_orig=$this->input->post('profile_pic_orig');
                    $this->login_m->updatePhotoOrig($user_photo_orig,$user->user_id);
                }//user photo temp check    
            }
            //copy photo for ie9 only
            
            
            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $user->user_email,
                'params' => array(
                                '[link]' => '<a href = "'.site_url('landing/verify/'.$user->user_hash_key.'?uid='.$user->user_id).'" >click</a>'
                            )
                ));
			$this->email_templating->send();
            
            /// promo code
            $promo_code = $this->input->post('promo_code');
            if($promo_code!="")
                $this->users_m->promo_code_save_to_user($promo_code,$user->user_id);
            

            
            if( ! $this->input->is_ajax_request() ) {
				
                
				if( strpos( uri_string(), 'm/' ) !== false ) {
					redirect( '/m/?ref=registered&email=' . $user->user_email );
				}
				
				redirect( '/?ref=registered&email=' . $user->user_email );
            }
            
            echo json_encode(array(
                'status' => 'ok',
                'user_id' => $user->user_id,
                'message' => 'You have successfully registered.',
				'location' => site_url(). 'm/?ref=registered&email=' . $user->user_email
            ));
			return;
            
        } else {
        
            if( $this->input->is_ajax_request() ) {
                
                $fields = array('fname','lname','email','year','month','day','gender','location','user_photo','pword','cpword','agree','token');
                $errors = array();
                foreach( $fields as $field ) {
                    if( form_error( $field ) ) {
                        $errors[$field] = form_error($field);
                    }
                }
                if( count($errors) ) {
                    echo json_encode( array('errors'=>$errors,'success'=>'false') );
                } else {
                    echo json_encode( array('success'=>'true') );
                }
                
                return;
            }

        }
        
		$data['token'] =  $this->login_m->generate_hash( 50 );
		$this->session->set_userdata( 'token', $data['token'] );
        
        if( strpos( uri_string(), 'm/' ) !== false ) {
            $this->load->view( 'm/register_v2', $data, FALSE, TRUE );
        } else {
            $this->load->view( 'register_v2', $data, FALSE, TRUE );
        }
        
    }
    
    private function upload_photo() {
    
        $photo = '';
        
        if (isset($_POST['image_64']) && ! empty($_POST['image_64']) ) {
            $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time();
        } else {
            $safe_filename = preg_replace(
                     array("/\s+/", "/[^-\.\w]+/"),
                     array("_", ""),
                     trim($_FILES['user_photo']['name']));
            $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];    
        }
        
        //$new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];
        $upload_folder = str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/users/';
        $this->load->library( 'upload', array(
            'upload_path' => $upload_folder,
            'allowed_types' => 'gif|jpg|png',
            'file_name' => $new_photo_name
        ));
        if( $this->upload->do_upload( 'user_photo' ) ) {
            $image = $this->upload->data();
            $crop = array();
            if( $this->input->post('coords') ) {
                $crop = explode( ',', $this->input->post('coords') );
            }

            $this->load->library('image_lib');

            $photo = $image['file_name'];

            // resize original
            $this->image_lib->initialize(array(
                'image_library' => 'GD2',
                'source_image' => $image['full_path'],
                'quality' => '80%',
                'width' => ( isset($crop[6]) ) ? $crop[6] : $image['image_width'],
                'height' => ( isset($crop[7]) ) ? $crop[7] : $image['image_height']
            ));
            $this->image_lib->resize();

            // clear image cache
            $this->image_lib->clear();

            // crop for thumbnail
            $this->image_lib->initialize(array(
                'image_library' => 'GD2',
                'source_image' => $image['full_path'],
                'quality' => '80%',
                'new_image' => $image['file_path'].'thumb/'.$image['file_name'],
                'width' => ( isset($crop[4]) ) ? $crop[4] : $image['image_width'],
                'height' => ( isset($crop[5]) ) ? $crop[5] : $image['image_width'],
                'x_axis' => ( isset($crop[0]) ) ? $crop[0] : 0,
                'y_axis' => ( isset($crop[1]) ) ? $crop[1] : 0
            ));
            $this->image_lib->crop();
            
            return $photo;
        } else {
            //Alternative photo
            if (isset($_POST['image_64']) && ! empty($_POST['image_64']) ) {
                //Write photo original
                $ifp = fopen($upload_folder.$new_photo_name. '.jpg', "wb");
                fwrite($ifp, base64_decode($_POST['image_64'])); 
                fclose($ifp);
                //Write thumb photo original
                $ifp = fopen($upload_folder.'thumb/'.$new_photo_name.'.jpg', "wb");
                fwrite($ifp, base64_decode($_POST['image_64'])); 
                fclose($ifp);
                
                return $new_photo_name.'.jpg';
            }
            
        }
    }
    
    /**
     * upload facebook/linkedin photo
     * 
     * @todo rename this method since it is also used in linkedin
     */
    private function get_facebook_photo() {
        $url = $this->input->post('fb_picture');
        $filename = substr($url, strrpos($url, '/') + 1);
        $upload_folder = str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/users/';
        $safe_filename = preg_replace(
                     array("/\s+/", "/[^-\.\w]+/"),
                     array("_", ""),
                     trim($filename));
        $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];
        file_put_contents($upload_folder.$new_photo_name.'.jpg', file_get_contents($url));
        
        $this->load->library('image_lib');
        
        $this->image_lib->initialize(array(
            'image_library' => "GD2",
            'source_image' => $upload_folder.$new_photo_name.'.jpg',
            'quality' => '80%',
            'new_image' => $upload_folder.'thumb/'.$new_photo_name.'.jpg',
            'width' => 140,
            'height' => 140
        ));
        
        $this->image_lib->resize();
        
        return $new_photo_name.'.jpg';
    }

    
    public function auth_option() {

        $this->load->view(MOBILE_M . '/register_landing_v2');
    }
    
    public function check_user_photo( $val ){
       //Check alternative image
       if (isset($_POST['image_64']) && ! empty($_POST['image_64']) ) {
           return true;
       }
       
        $this->form_validation->set_message('check_user_photo', 'Photo is required');
        if (empty($_FILES['user_photo']['name'])) {
            return false;
        }
        return true;
    }
    
    public function check_dob($day) {

        $year = ($this->input->post('year') == "") ? '0000' : $this->input->post('year');
        $month = ($this->input->post('month') == "") ? '00' : $this->input->post('month');
        $month = (int) $month;
        $day = (int) $day;
        $year = (int) $year;
        if (checkdate($month, $day, $year))
            return true;
        else {
            $this->form_validation->set_message('check_dob', 'Please provide valid Date of Birth');
            return FALSE;
        }
    }

    /**
     * Checks if the email is already active
     */
    public function check_email( $email ) {
        $email_chk = $this->login_m->emailExist( $email );
        if( $email_chk == 0 ) {
            return true;
        } else {
            $this->form_validation->set_message( 'check_email', $this->lang->line( 'email_already_exist_try_new_one' ) );
            return false;
        }
    }
    
    /**
     * Checks if an inactive account with the given email
     * is already existing, then flag the form to
     * Update that record instead of inserting a new record
     */
    private function inactive_email_exists( $email, $fb_id ) {
        $fb_id = ($fb_id == '') ? "0" : $fb_id;
        $this->db->where("(user_email='".$email."' AND user_status='Pending') OR (user_fb_id='".$fb_id."' AND user_status='Pending') ");
        $q = $this->db->get( 'gl_user' );
        if( count( $q->row() ) ) return $q->row();
        
        return FALSE;
    }
    
    public function inactive_linkedin_id( $val ) {
        $this->db->select( "user_id, user_linkedin_id" );
        $this->db->from( "gl_user" );
        $this->db->where( array( "user_linkedin_id" => $val ) );
        $q = $this->db->get();
        if( count( $q->row() ) ) {
            $this->has_inactive_linkedin_account = true;
            return true;
        }
        return false;
    }
    
    public function inactive_facebook_id( $val ) {
        $this->db->select( "user_id, user_fb_id" );
        $this->db->from( "gl_user" );
        $this->db->where( array( "user_fb_id" => $val ) );
        $q = $this->db->get();
        if( count( $q->row() ) ) {
            $this->has_inactive_facebook_account = true;
            return true;
        }
        return false;
    }

    /**
     * @todo message translation
     */
    public function check_token( $val ) {
        if( isset( $_POST['token'] ) && $_POST['token'] == $val ) {
            return TRUE;
        }
        
        $this->form_validation->set_message( 'check_token', $this->lang->line( 'Invalid Token' ) );
        return FALSE;
    }

    public function check_fname($name){

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('check_fname', 'Special characters are not allowed on First Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('check_fname', 'Numbers are not allowed on First Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }

    public function check_lname($name){
        
        /* $rgx = '^[+_)(*&^%$#@!~`=\[\]{};\\:"|<>?,/0-9]+$'; */

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('check_lname', 'Special characters are not allow on Last Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('check_lname', 'Numbers are not allow on Last Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }
    
    public function check_password( $val ) {
        $this->form_validation->set_message('check_password', $this->lang->line('valid_password_length','%s must be 6-9 characters in length'));
        
        if( strlen($val) < 6 || strlen($val) > 9 ) {
            return false;
        }
        
        return true;
    }
    
}