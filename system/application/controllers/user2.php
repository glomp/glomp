<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User2 extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('regions_m');
        $this->load->model('login_m');        
        $this->load->library('email_templating');
        $this->load->library('custom_func');		
    }
    
    function register($puid = 0, $registration_type = '') {
        $data = array(
            'registration_type' => '',
            'social_id' => '',
        );

        if (! empty($puid) ) {
            $user = $this->db->get_where('gl_user', array('user_id' => $puid));

            if ($user->num_rows() > 0) {
                $data = array(
                    'registration_type' => $registration_type,
                    'social_id' => $user->row()->user_mobile,
                );
            }
        }

        $email = $this->input->post('email');

        $email_exists = FALSE;

        // validation rules
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
        $this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|xss_clean');
        $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|min_length[6]|required|matches[pword]');
        $this->form_validation->set_rules('agree', 'Terms & Conditions', 'trim|required');
        $this->form_validation->set_rules('promo_code', 'Promo Code', 'trim|xss_clean');
        
        if (count($_POST) > 0  ) {
            $user = $this->db->get_where('gl_user', array('user_email' => $email));

            if ($this->input->post('registration_type') == 'FB') {
                return $this->_register_FB();
            }
            if ($this->input->post('registration_type') == 'LI') {
                return $this->_register_LI();
            }

            if ($this->input->post('registration_type') == 'SMS') {
                return $this->_register_SMS();
            }

            if ($user->num_rows() > 0) {
                $email_exists = TRUE;
            }

        }
        
        
        if( $this->form_validation->run() == TRUE && $email_exists == FALSE) {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_gender' => $this->input->post('gender'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_start_tour' => 'Y',
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
            );
            
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));

            $this->db->insert('gl_user', $u_data);
            $user_id = $this->db->insert_id();
            
            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();
            
            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }
            
            if( $this->input->is_ajax_request() ) {
                $redirect_url = site_url('?ref=registered&email=' . $u_data['user_email']);
                
                if ($this->uri->segment(1) == 'm') {
                    $redirect_url = site_url('/m/?ref=registered&email=' . $u_data['user_email']) ;
                }
            }
            
            echo json_encode(array(
                'error' => '',
                'location' => $redirect_url
            ));
            return;
        } else {
            if (count($_POST) >0 ) {
                $errors = $this->form_validation->error_array();
                $validation_errors = array();
                if (count($errors) > 0 && $errors != FALSE) {
                    foreach($errors as $key => $value) {
                        $validation_errors[] = '<p>'.$value.'</p>';
                    }
                }
                
                if ($email_exists) {
                    if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                        //update when there's no error
                        $extra['user_salt'] = $this->login_m->generate_hash();
                        $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                        $this->_update_pending_account($user, $extra);
                    } else if ($user->row()->user_status == 'Active') {
                        $validation_errors[] = '<p>'.$this->lang->line( 'email_already_exist_try_new_one_1', 'Email address already exists. Please try new one.' ).'</p>';        
                    }
                    
                }
                
                echo json_encode(array(
                    'error' => implode($validation_errors),
                    'error_list' => $errors,
                ));
                return;
            }
        }
        
        if ($this->uri->segment(1) == 'm') {
            return $this->load->view( 'm/user/register_v', $data, FALSE, TRUE );
        }
        
        $this->load->view( 'desktop/user/register_v', array(), FALSE, TRUE );
    }
    
    
    function _register_FB() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }
        
        if( $this->form_validation->run() == FALSE || $email_exists) {

            $errors = $this->form_validation->error_array();
            $validation_errors = array();
            if (count($errors) > 0 && $errors != FALSE) {
                foreach($errors as $key => $value) {
                    $validation_errors[] = '<p>'.$value.'</p>';
                }
            }
            
            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    //Assumed that FBID is not attached to other member
                    if (is_null($user->row()->user_fb_id)) {
                        //Then return status login
                        echo json_encode(array(
                            'error' => '',
                            'status' => 'login',
                        ));
                        return;
                    }

                    $validation_errors[] = '<p>'.$this->lang->line( 'email_already_exist_try_new_one_1', 'Email already exist please try a new one.' ).'</p>';

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_fb_id'] = $this->input->post('social_id');
                    $extra['user_profile_pic'] = $this->get_social_photo();
                    $extra['user_profile_pic_orig'] = $extra['user_profile_pic'];
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }
            }

            echo json_encode(array(
                'error' => implode($validation_errors),
                'error_list' => $errors,
            ));
            return;
        } else {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
                'user_fb_id' => $this->input->post('social_id'),
                'user_profile_pic' => $this->get_social_photo(),
            );

            $u_data['user_profile_pic_orig'] = $u_data['user_profile_pic'];
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));


            //Check if there's an existing pending account and Facebook ID
            $user = $this->db->get_where('gl_user', array('user_status' => 'Pending', 'user_fb_id' => $u_data['user_fb_id']));
            if ($user->num_rows() > 0 ) {
                //Update record
                $user_id = $user->row()->user_id;
                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $u_data);
            } else {
                $this->db->insert('gl_user', $u_data);
                $user_id = $this->db->insert_id();
            }

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();

            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }

            if( $this->input->is_ajax_request() ) {
                $redirect_url = site_url('?ref=registered&email=' . $u_data['user_email']);

                if ($this->uri->segment(1) == 'm') {
                    $redirect_url = site_url('/m/?ref=registered&email=' . $u_data['user_email']) ;
                }
            }

            echo json_encode(array(
                'error' => '',
                'status' => 'register',
                'location' => $redirect_url
            ));
            return;
        }
    }
    
    function _register_LI() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }
        
        if( $this->form_validation->run() == FALSE || $email_exists) {
            $errors = $this->form_validation->error_array();
            $validation_errors = array();
            if (count($errors) > 0 && $errors != FALSE) {
                foreach($errors as $key => $value) {
                    $validation_errors[] = '<p>'.$value.'</p>';
                }
            }
            
            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    //Assumed that LinkedIN ID is not attached to other member
                    if (is_null($user->row()->user_linkedin_id)) {
                        //Then return status login
                        echo json_encode(array(
                            'error' => '',
                            'status' => 'login',
                        ));
                        return;
                    }

                    $validation_errors[] = '<p>'.$this->lang->line( 'email_already_exist_try_new_one_1', 'Email address already exists. Please try new one' ).'</p>';

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_linkedin_id'] = $this->input->post('social_id');
                    $extra['user_profile_pic'] = $this->get_social_photo();
                    $extra['user_profile_pic_orig'] = $extra['user_profile_pic'];
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }
            }
            
            echo json_encode(array(
                'error' => implode($validation_errors),
                'error_list' => $errors,
            ));
            return;
        } else {
                $u_data = array(
                    'user_email' => $this->input->post('email'),
                    'user_email_verified' => 'N',
                    'username' => $this->input->post('email'),
                    'user_gender' => ucfirst($this->input->post('gender')),
                    'user_fname' => $this->input->post('fname'),
                    'user_lname' => $this->input->post('lname'),
                    'user_city_id' => $this->input->post('location'),
                    'user_salt' => $this->login_m->generate_hash(),
                    'user_hash_key' => $this->login_m->generate_hash(25),
                    'user_status' => 'Active',
                    'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                    'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                    'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                    'user_dob_display' => 'dob_display_wo_year',
                    'user_sign_up_promo_code' => $this->input->post('promo_code'),
                    'user_join_date' => gmdate('Y-m-d H:i:s'),
                    'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
                    'user_linkedin_id' => $this->input->post('social_id'),
                    'user_profile_pic' => $this->get_social_photo(),
                );

                $u_data['user_profile_pic_orig'] = $u_data['user_profile_pic'];
                $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));


                //Check if there's an existing pending account and linkedIn ID
                $user = $this->db->get_where('gl_user', array('user_status' => 'Pending', 'user_linkedin_id' => $u_data['user_linkedin_id']));
                if ($user->num_rows() > 0 ) {
                    //Update record
                    $user_id = $user->row()->user_id;
                    $this->db->where('user_id', $user->row()->user_id);
                    $this->db->update('gl_user', $u_data);
                } else {
                    $this->db->insert('gl_user', $u_data);
                    $user_id = $this->db->insert_id();
                }

                // send email verification
                $this->load->library('email_templating');
                $this->email_templating->config(array(
                    'template_name' => 'new_signup',
                    'lang_id' => 1,
                    'to' => $u_data['user_email'],
                    'params' => array(
                        '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                    )
                ));
                $this->email_templating->send();

                /// promo code
                $promo_code = $this->input->post('promo_code');
                if( ! empty($promo_code) ) {
                    $this->users_m->promo_code_save_to_user($promo_code, $user_id);
                }

                if( $this->input->is_ajax_request() ) {
                    $redirect_url = site_url('?ref=registered&email=' . $u_data['user_email']);

                    if ($this->uri->segment(1) == 'm') {
                        $redirect_url = site_url('/m/?ref=registered&email=' . $u_data['user_email']) ;
                    }
                }

                echo json_encode(array(
                    'error' => '',
                    'status' => 'register',
                    'location' => $redirect_url
                ));
                return;
            }
    }

    function _register_SMS() {
        $email = $this->input->post('email');
        $email_exists = FALSE;
        
        $user = $this->db->get_where('gl_user', array('user_email' => $email));
        if ($user->num_rows() > 0) {
            $email_exists = TRUE;
        }
        
        if( $this->form_validation->run() == FALSE || $email_exists) {

            $errors = $this->form_validation->error_array();
            $validation_errors = array();
            if (count($errors) > 0 && $errors != FALSE) {
                foreach($errors as $key => $value) {
                    $validation_errors[] = '<p>'.$value.'</p>';
                }
            }
            
            if ($email_exists) {
                if ($user->row()->user_status == 'Active') {
                    $validation_errors[] = '<p>'.$this->lang->line( 'email_already_exist_try_new_one_1', 'Email already exist please try a new one.' ).'</p>';

                } else if ($user->row()->user_status == 'Pending' && $this->form_validation->run() == TRUE) {
                    //update when there's no error
                    $extra['user_salt'] = $this->login_m->generate_hash();
                    $extra['user_password'] = $this->login_m->hash_key_value( $extra['user_salt'], $this->input->post('pword'));

                    $this->_update_pending_account($user, $extra);
                }
            }

            echo json_encode(array(
                'error' => implode($validation_errors),
                'error_list' => $errors,
            ));
            return;
        } else {
            $u_data = array(
                'user_email' => $this->input->post('email'),
                'user_email_verified' => 'N',
                'username' => $this->input->post('email'),
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_salt' => $this->login_m->generate_hash(),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
                'user_mobile' => $this->input->post('social_id'),
            );
            $u_data['user_password'] = $this->login_m->hash_key_value( $u_data['user_salt'], $this->input->post('pword'));

            //Check if there's an existing pending account for SMS
            $this->db->where('user_status', 'Pending');
            $this->db->where('user_mobile', $u_data['user_mobile']);
            $user = $this->db->get('gl_user');

            if ($user->num_rows() > 0 ) {
                //Update record
                $user_id = $user->row()->user_id;
                $u_data['user_hash_key'] = $user->row()->user_hash_key; //just get the existing record
                $this->db->where('user_id', $user->row()->user_id);
                $this->db->update('gl_user', $u_data);
            } else {
                $this->db->insert('gl_user', $u_data);
                $user_id = $this->db->insert_id();
            }

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $u_data['user_email'],
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user_id).'" >click</a>'
                )
            ));
            $this->email_templating->send();

            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user_id);
            }

            if( $this->input->is_ajax_request() ) {
                $redirect_url = site_url('?ref=registered&email=' . $u_data['user_email']);

                if ($this->uri->segment(1) == 'm') {
                    $redirect_url = site_url('/m/?ref=registered&email=' . $u_data['user_email']) ;
                }
            }

            echo json_encode(array(
                'error' => '',
                'status' => 'register',
                'location' => $redirect_url
            ));
            return;
        }
    }

    function _update_pending_account($user, $extra = array()) {
        //check if pending status then just update status into active and send an welcome email
        if ($user->row()->user_status == 'Pending') {
            $u_data = array(
                'user_gender' => ucfirst($this->input->post('gender')),
                'user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_city_id' => $this->input->post('location'),
                'user_hash_key' => $this->login_m->generate_hash(25),
                'user_status' => 'Active',
                'user_last_updated_date' => gmdate('Y-m-d H:i:s'),
                'user_last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'user_account_created_ip' => $_SERVER['REMOTE_ADDR'],
                'user_dob_display' => 'dob_display_wo_year',
                'user_sign_up_promo_code' => $this->input->post('promo_code'),
                'user_join_date' => gmdate('Y-m-d H:i:s'),
                'user_signup_device' => ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone') : 'Computer'),
            );

            //merge data
            $u_data = $u_data  + $extra;

            $this->db->where('user_id', $user->row()->user_id);
            $this->db->update('gl_user', $u_data);

            // send email verification
            $this->load->library('email_templating');
            $this->email_templating->config(array(
                'template_name' => 'new_signup',
                'lang_id' => 1,
                'to' => $user->row()->user_email,
                'params' => array(
                    '[link]' => '<a href = "'.site_url('landing/verify/'.$u_data['user_hash_key'].'?uid='.$user->row()->user_id).'" >click</a>'
                )
            ));

            $this->email_templating->send();
            
            /// promo code
            $promo_code = $this->input->post('promo_code');
            if( ! empty($promo_code) ) {
                $this->users_m->promo_code_save_to_user($promo_code, $user->row()->user_id);
            }
            
            if( $this->input->is_ajax_request() ) {
                $redirect_url = site_url('?ref=registered&email=' . $user->row()->user_email);
                
                if ($this->uri->segment(1) == 'm') {
                    $redirect_url = site_url('/m/?ref=registered&email=' . $user->row()->user_email) ;
                }
            }
            
            echo json_encode(array(
                'error' => '',
                'location' => $redirect_url
            ));
            exit;
        }
    }
    
    function welcome_incomplete_profile() {
        $this->load->library('user_agent');
        $user_id = $this->session->userdata('user_id');
        $segement_1 = '';
        
        if ($this->uri->segment(1) == 'm') {
            $segement_1 = 'm/';
        }
        
        //Do not show pop up when member is on update page
        $by_pass = ($this->agent->referrer() == site_url($segement_1 .  'user/update')) ? TRUE : FALSE;
        $this->db->select('user_profile_pic AS profile_pic, user_dob, user_password, user_city_id AS location, user_start_tour');
        $user = $this->db->get_where('gl_user', array('user_id' => $user_id));
        $_user = array();
        foreach($user->row_array() as $key => $row) {
            //255 Default location if not set
            // if ($key == 'location' && $row == '255') {
            //     $_user[$key] = FALSE;
            //     continue;
            // }
            
            $_user[$key] = (is_null($row) || empty($row)) ? FALSE : TRUE;
        }
        //We don't need user_start_tour to display in the result
        unset($_user['user_start_tour']);
        
        //We first show the tour?
        //$by_pass = ($user->row()->user_start_tour == 'Y') ? TRUE : $by_pass;
        
        echo json_encode(array(
            'data' => $_user,
            'msg' => array(
                'body' => 'Thank you for joining <b>glomp!</b>. <br /> <br /> Your profile is not yet fully complete. Please take a couple of seconds to complete it now. Otherwise, you are welcome to use <b>glomp!</b> in a limited capacity but you will not be able to receive any treat vouchers.',
                'ok' => 'Complete registration',
                'later' => 'Later'
            ),
            'incomplete' => (in_array(FALSE, $_user)) ? TRUE : FALSE,
            'bypass' => $by_pass,
            'is_update_page' => ($this->agent->referrer() == site_url($segement_1. 'user/update')) ? TRUE : FALSE,
        ));
        return;
    }
     /**
     * upload facebook/linkedin photo
     * 
     * @todo rename this method since it is also used in linkedin
     */
    private function get_social_photo() {
        $url = $this->input->post('profile_pic');
        $filename = substr($url, strrpos($url, '/') + 1);
        $upload_folder = str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/users/';
        $safe_filename = preg_replace(
                     array("/\s+/", "/[^-\.\w]+/"),
                     array("_", ""),
                     trim($filename));
        $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];
        file_put_contents($upload_folder.$new_photo_name.'.jpg', file_get_contents($url));
        
        $this->load->library('image_lib');
        
        $this->image_lib->initialize(array(
            'image_library' => "GD2",
            'source_image' => $upload_folder.$new_photo_name.'.jpg',
            'quality' => '80%',
            'new_image' => $upload_folder.'thumb/'.$new_photo_name.'.jpg',
            'width' => 140,
            'height' => 140
        ));
        
        $this->image_lib->resize();
        
        return $new_photo_name.'.jpg';
    }
}