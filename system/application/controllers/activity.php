<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Activity extends CI_Controller
{

    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;

    function __construct() {
        parent::__Construct();
        $this->load->library('user_login_check');
        $this->load->model('cmspages_m');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->model('merchant_m');
        $this->load->model('regions_m');
        $this->load->model('login_m');
        $this->load->model('send_email_m');
        $this->load->model('activity_m');
        $this->load->library('email_templating');
        $this->load->library('custom_func');		
    }

    function index(){
        $user_id = $this->session->userdata('user_id');                
        $data['activity'] =$this->activity_m->get_activity($user_id,0,10);        
        $data['activity_points'] =$this->activity_m->get_activity($user_id, 0, 10, 'points');
        $data['page_title'] = $this->session->userdata('user_name').' Activity';
        $this->load->view('activity_v', $data);
    }
    function getMoreActivity($type="")
    {
        if($_POST && isset($_POST['start_count']))
        {
            $start_count=(int)$this->input->post('start_count');
            $user_id = $this->session->userdata('user_id');            
            $data['activity'] =$this->activity_m->get_activity($user_id, $start_count, 5,$type);             
            $data['start_count'] = $start_count;            
            $this->load->view('activity_more_v', $data);
        }        
    }
}
//eoc