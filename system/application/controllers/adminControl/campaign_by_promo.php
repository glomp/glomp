<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  Author: Shree
  Date: Aug-18-2013
  Desc: Campaign
 */

class Campaign_by_promo extends CI_Controller {

    private $pagination_per_page = 20;

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('campaign_m');
        $this->load->library('lib_pagination');
        $this->load->model('product_m');
        $this->load->model('regions_m');
        $this->load->model('users_m');
        $this->load->model('merchant_m');
        $this->load->model('send_email_m');
        $this->load->model('brand_m');
    }

    function index() {
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Campaign (by promo code)";
        $data['page_subtitle'] = "View All";
        $start = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $per_page = $this->pagination_per_page;
        $pagination_base_url = admin_url("campaign_by_promo/index/");
        $num_rows = $this->campaign_m->selectCampaign('by_promo')->num_rows();

        $data['links'] = $this->lib_pagination->DoPagination($pagination_base_url, $num_rows, $per_page, "4");
        $data['res_campaign'] = $this->campaign_m->selectCampaign('by_promo');

        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');

        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');
        $this->load->view(ADMIN_FOLDER . '/campaign_by_promo_v', $data);
    }

    function AddCampaign() {
        $data['page_action'] = "add";
        $data['page_title'] = "Create New Campaign (by promo code)";
        $data['page_subtitle'] = "Create New Campaign";
        $data['follow_up_checked'] = '';
        $data['enable_time_limit'] = '';

        $merchants = $this->db->select('merchant_id,merchant_name,merchant_region_id')->order_by('merchant_name')->get('gl_merchant')->result();
        foreach($merchants as $m){
            $products = $this->db->get_where('gl_product',array('prod_merchant_id'=>$m->merchant_id))->result();
            $data['merchants'][$m->merchant_id] = array(
                'name'=>$m->merchant_name,
                'region'=>$m->merchant_region_id,
                'products' => $products
            );
        }
        $sponsor = $this->db->get_where('gl_categories',array('cat_name'=>'Sponsor'));
        if($sponsor->num_rows() > 0) {
            $sponsor_id = $sponsor->row()->cat_id;
            $this->db->join('gl_merchant','gl_merchant.merchant_id = gl_product.prod_merchant_id');
            $this->db->join('gl_product_inventory', 'gl_product_inventory.prod_id = gl_product.prod_id','left');
            $sponsored = $this->db->select('gl_product.*,gl_merchant.*,gl_product_inventory.current_qty,gl_product_inventory.change_qty')->from('gl_product')->where('prod_cat_id = ' . $sponsor_id )->get();
            if( $sponsored->num_rows() > 0 ) {
                $data['sponsor'] = $sponsored->result();
            }
        }

        $data['regions'] = $this->db->order_by('region_name')->get('gl_region')->result();


        if (isset($_POST['campaign_name'])) {
            
            $this->form_validation->set_rules('campaign_name', 'Campaign Name', 'required');
            $this->form_validation->set_rules('promo_code', 'Promo Code', 'required|callback_check_if_promo_code_available');
            $this->form_validation->set_rules('campaign_type', 'Campaign type', 'required');
            $this->form_validation->set_rules('expiry_day', 'Expiry day', 'required|integer|is_natural_no_zero');



            $this->form_validation->set_rules('campaign_for', 'Campaign For', 'required');
            if (isset($_POST['enable_time_limit']) && $_POST['enable_time_limit'] == 'Y') {
                $this->form_validation->set_rules('expiry_date', 'Campaign Expiry Date', 'required|callback_check_if_date');
                $this->form_validation->set_rules('start_date', 'Campaign Start Date', 'required|callback_check_if_date');
            }


            $this->form_validation->set_rules('product_id[]', 'Product', 'required|integer|is_natural_no_zero');


            $this->form_validation->set_rules('rule_desc', 'Rule Description', 'required|');
            $this->form_validation->set_rules('min_age', 'Minimum age', 'required|integer|');
            $this->form_validation->set_rules('max_age', 'Maxminum age', 'required|integer');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            //$this->form_validation->set_rules('regions_id_selected[]', 'Regions', 'required');

            $extra_rule = "";
            $extra_rule_invite = "";
            /* if ($_POST['campaign_type'] == 'Both') {
              $extra_rule = "|callback_check_divisible";
              $extra_rule_invite = "|required|callback_check_divisible_invite";
              $this->form_validation->set_rules('invite_email[]', 'Invite by Email', 'valid_email' . $extra_rule_invite);
              } */
            $this->form_validation->set_rules('voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);



            if (isset($_POST['follow_up_campaign']) && $_POST['follow_up_campaign'] == 'enable') {
                $this->form_validation->set_rules('f_voucher_qty[]', 'Follow up Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
                $this->form_validation->set_rules('f_product_id[]', 'Follow up Product', 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('follow_up_campagin_type', 'Follow up Campaign type', 'required');

                $data['follow_up_checked'] = 'checked';
            }
            
            
            if (isset($_POST['whitelabel_id_checked']))
                $this->form_validation->set_rules('whitelabel_id_selected[]', 'White Labels', 'required');
            
            if (isset($_POST['whitelabel_actions_checked']))
                $this->form_validation->set_rules('whitelabel_actions_selected[]', 'Actions', 'required');
            
            if (isset($_POST['whitelabel_merchants_checked']))
                $this->form_validation->set_rules('whitelabel_merchants_selected[]', 'Merchants', 'required');
            
            if (isset($_POST['whitelabel_products_checked']))
                $this->form_validation->set_rules('whitelabel_products_selected[]', 'Products', 'required');
            
            if (isset($_POST['date_from_checked']))
            {
                $this->form_validation->set_rules('whitelabel_date_from', 'Log Date From', 'required|callback__check_log_date');
                $this->form_validation->set_rules('whitelabel_date_to', 'Log Date To', 'required|callback__check_log_date');
            }

            if ($this->form_validation->run() == TRUE) {
                $insert_id = $this->campaign_m->create_campaign_by_promo();
                if ($insert_id > 0) {
                    //
                    $cnt = count($_POST['product_id']);
                    for ($i = 0; $i < $cnt; $i++) {
                        $prod_id = $_POST['product_id'][$i];
                        $qty = $_POST['voucher_qty'][$i];
                        $type = 1;
                        if ($_POST['campaign_type'] == 'Both') {
                            $type = ($qty % 2 == 0) ? 1 : 0;
                        }
                        if ($prod_id > 0 && $qty > 0 && $type > 0) {

                            $this->campaign_m->create_campaign_details($insert_id, $prod_id, $qty, 'by_promo_primary');
                        }
                    }

                    if (!isset($_POST['follow_up_campaign'])) {
                        
                    } else if ($_POST['follow_up_campaign'] == 'enable') {

                        $cnt = count($_POST['f_product_id']);
                        for ($i = 0; $i < $cnt; $i++) {
                            $prod_id = $_POST['f_product_id'][$i];
                            $qty = $_POST['f_voucher_qty'][$i];
                            $type = 1;
                            if ($_POST['campaign_type'] == 'Both') {
                                $type = ($qty % 2 == 0) ? 1 : 0;
                            }
                            if ($prod_id > 0 && $qty > 0 && $type > 0) {

                                $this->campaign_m->create_campaign_details($insert_id, $prod_id, $qty, 'by_promo_follow_up');
                            }
                        }
                    }

                    $this->campaign_m->update_campaign_rule($insert_id);

                    $this->session->set_flashdata('msg', "Record successfully added.");
                    echo json_encode(array('error' => '', 'redirect' => ADMIN_FOLDER . "/campaign_by_promo/"));
                    return;
                }
            } else {
                echo json_encode(array('error' => $this->form_validation->error_array()));
                return;
            }
        }
        $this->db->select('bp.id, bp.name');
        $this->db->from('gl_brandproduct bp');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $white_label_merchants = $this->db->get();

        $this->db->select('p.id,p.product_id, bp.name, gl_p.prod_name');
        $this->db->from('gl_brands_products p');
        $this->db->join('gl_product gl_p', 'gl_p.prod_id = p.product_id', 'left');
        $this->db->join('gl_brandproduct bp', 'p.brandproduct_id = bp.id', 'left');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $this->db->order_by("bp.name ASC, gl_p.prod_name ASC");
        $query = $this->db->get(); 
        $white_label_products = $query;
        
        
        //print_r($data['res_camp_rule']->row());
        
        $rule_details = '';//json_decode($data['res_camp_rule']->row()->rule_details);
        
        //print_r($rule_details);
        $map = array(
                'all_list' => $this->campaign_m->white_label_list,
                'selected_list' => isset($_POST['whitelabel_id_selected']) ? ($_POST['whitelabel_id_selected']):'',
                
                'all_actions' => $this->campaign_m->white_label_actions,
                'selected_actions' => isset($_POST['whitelabel_actions_selected']) ? ($_POST['whitelabel_actions_selected']):'',
                
                'all_merchants' => $white_label_merchants,
                'selected_merchants' => isset($_POST['whitelabel_merchants_selected']) ? ($_POST['whitelabel_merchants_selected']):'',
                
                'all_products' => $white_label_products,
                'selected_products'=> isset($_POST['whitelabel_products_selected']) ? ($_POST['whitelabel_products_selected']):'',
                
                'date_from' => isset($_POST['whitelabel_date_from']) ? ($_POST['whitelabel_date_from']):'',
                'date_to' =>isset($_POST['whitelabel_date_to']) ? ($_POST['whitelabel_date_to']):''
        );
        $white_label = new stdClass();
        foreach($map as $k => $v)
            $white_label->$k = $v;
        $data['white_label'] = $white_label;
        
        $data['whitelabel_id_checked'] = ( isset($_POST['whitelabel_id_checked']) && $_POST['whitelabel_id_checked'] !='') ? 'checked' : '';
        $data['whitelabel_actions_checked'] = ( isset($_POST['whitelabel_actions_checked']) && $_POST['whitelabel_actions_checked'] !='') ? 'checked' : '';
        $data['whitelabel_merchants_checked'] = ( isset($_POST['whitelabel_merchants_checked']) && $_POST['whitelabel_merchants_checked'] !='') ? 'checked' : '';
        $data['whitelabel_products_checked'] = ( isset($_POST['whitelabel_products_checked']) && $_POST['whitelabel_products_checked'] !='') ? 'checked' : '';
        $data['date_from_checked'] = ( isset($_POST['date_from_checked']) && $_POST['date_from_checked'] !='' && $_POST['date_from_checked'] !='0') ? 'checked' : '';
        
        
        $this->load->view(ADMIN_FOLDER . '/campaign_by_promo_v', $data);
    }

    function editCampaign($camp_id) {
        $camp_id = (int) $camp_id;
        $res_camp = $this->campaign_m->campaign_by_id_by_promo($camp_id);
        $data['camp_id'] = $camp_id;
        if ($res_camp->num_rows() != 1) {
            redirect(ADMIN_FOLDER . "/campaign_by_promo/");
            exit();
        }

        $rec_campaign = $res_camp->row();
        $data['rec_campaign'] = $rec_campaign;
        $data['res_camp_rule'] = $this->campaign_m->select_campaign_rule($camp_id);
        //Note if campaign is already executed then do not allow to edit the campaign deails tabale. It will effect into so many places.
        if ($rec_campaign->campaign_by_type != "by_promo") {
            $this->session->set_flashdata('error_msg', "Sorry, this is not a campaign by promo code.");
            return redirect(admin_url("campaign_by_promo"));
        } else if ($rec_campaign->campaign_by_promo_status == "Running") {
            $this->session->set_flashdata('error_msg', "Sorry, this campaign is already running and not allowed to change the parameter.");
            return redirect(admin_url("campaign_by_promo"));
        }

        $merchants = $this->db->select('merchant_id,merchant_name,merchant_region_id')->order_by('merchant_name')->get('gl_merchant')->result();
        foreach($merchants as $m){
            $products = $this->db->get_where('gl_product',array('prod_merchant_id'=>$m->merchant_id))->result();
            $data['merchants'][$m->merchant_id] = array(
                'name'=>$m->merchant_name,
                'region'=>$m->merchant_region_id,
                'products' => $products
            );
        }
        $sponsor = $this->db->get_where('gl_categories',array('cat_name'=>'Sponsor'));
        if($sponsor->num_rows() > 0) {
            $sponsor_id = $sponsor->row()->cat_id;
            $this->db->join('gl_merchant','gl_merchant.merchant_id = gl_product.prod_merchant_id');
            $this->db->join('gl_product_inventory', 'gl_product_inventory.prod_id = gl_product.prod_id','left');
            $sponsored = $this->db->select('gl_product.*,gl_merchant.*,gl_product_inventory.current_qty,gl_product_inventory.change_qty')->from('gl_product')->where('prod_cat_id = ' . $sponsor_id )->get();
            if( $sponsored->num_rows() > 0 ) {
                $data['sponsor'] = $sponsored->result();
            }
        }

        $data['regions'] = $this->db->order_by('region_name')->get('gl_region')->result();


        $data['page_action'] = "edit";
        $data['page_title'] = "Edit Campaign (by promo code)";
        $data['page_subtitle'] = "";
        if (isset($_POST['campaign_name'])) {
            $extra_rule = "";
            $this->session->set_userdata('camp_id', $camp_id);

            $this->form_validation->set_rules('campaign_name', 'Campaign Name', 'required');
            $this->form_validation->set_rules('promo_code', 'Promo Code', 'required|callback_check_if_promo_code_available_update');
            $this->form_validation->set_rules('campaign_type', 'Campaign type', 'required');
            $this->form_validation->set_rules('expiry_day', 'Expiry day', 'required|integer|is_natural_no_zero');



            $this->form_validation->set_rules('campaign_for', 'Campaign For', 'required');
            if (isset($_POST['enable_time_limit']) && $_POST['enable_time_limit'] == 'Y') {
                $this->form_validation->set_rules('expiry_date', 'Campaign Expiry Date', 'required|callback_check_if_date');
                $this->form_validation->set_rules('start_date', 'Campaign Start Date', 'required|callback_check_if_date');
            }



            $this->form_validation->set_rules('product_id[]', 'Product', 'required|integer|is_natural_no_zero');

            $this->form_validation->set_rules('rule_desc', 'Rule Description', 'required|');
            $this->form_validation->set_rules('min_age', 'Minimum age', 'required|integer|');
            $this->form_validation->set_rules('max_age', 'Maxminum age', 'required|integer');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            //$this->form_validation->set_rules('regions_id_selected[]', 'Regions', 'required');

            $extra_rule = "";
            $extra_rule_invite = "";
            /* if ($_POST['campaign_type'] == 'Both') {
              $extra_rule = "|callback_check_divisible";
              $extra_rule_invite = "|required|callback_check_divisible_invite";
              $this->form_validation->set_rules('invite_email[]', 'Invite by Email', 'valid_email' . $extra_rule_invite);
              } */
            $this->form_validation->set_rules('voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);

            if (isset($_POST['follow_up_campaign']) && $_POST['follow_up_campaign'] == 'enable') {
                $this->form_validation->set_rules('f_voucher_qty[]', 'Follow up Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
                $this->form_validation->set_rules('f_product_id[]', 'Follow up Product', 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('follow_up_campagin_type', 'Follow up Campaign type', 'required');
                $data['follow_up_checked'] = 'checked';
            }
            
            $cnt = isset($_POST['new_product_id']) ? count($_POST['new_product_id']) : 0;
            if ($cnt > 0) {
                $this->form_validation->set_rules('new_product_id[]', 'Product', 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('new_voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
            }

            $cnt = isset($_POST['f_new_product_id']) ? count($_POST['f_new_product_id']) : 0;
            if ($cnt > 0) {
                $this->form_validation->set_rules('f_new_product_id[]', 'Follow up Product', 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('f_new_voucher_qty[]', 'Follow up  Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
            }
            
            
            if (isset($_POST['whitelabel_id_checked']))
                $this->form_validation->set_rules('whitelabel_id_selected[]', 'White Labels', 'required');
            
            if (isset($_POST['whitelabel_actions_checked']))
                $this->form_validation->set_rules('whitelabel_actions_selected[]', 'Actions', 'required');
            
            if (isset($_POST['whitelabel_merchants_checked']))
                $this->form_validation->set_rules('whitelabel_merchants_selected[]', 'Merchants', 'required');
            
            if (isset($_POST['whitelabel_products_checked']))
                $this->form_validation->set_rules('whitelabel_products_selected[]', 'Products', 'required');
            
            if (isset($_POST['date_from_checked']))
            {
                $this->form_validation->set_rules('whitelabel_date_from', 'Log Date From', 'required|callback__check_log_date');
                $this->form_validation->set_rules('whitelabel_date_to', 'Log Date To', 'required|callback__check_log_date');
            }
            
            

            if ($this->form_validation->run() == TRUE) {
                $this->campaign_m->update_campaign_by_promo($camp_id);
                //update existing record intot the database
                $cnt = count($_POST['product_id']);
                for ($i = 0; $i < $cnt; $i++) {
                    $prod_id = $_POST['product_id'][$i];
                    $qty = $_POST['voucher_qty'][$i];
                    $details_id = $_POST['cam_details_id'][$i];
                    $type = 1;
                    if ($_POST['campaign_type'] == 'Both') {
                        $type = ($qty % 2 == 0) ? 1 : 0;
                    }
                    if ($prod_id > 0 && $qty > 0 && $type > 0) {

                        $this->campaign_m->update_campaign_details($details_id, $prod_id, $qty, 'by_promo_primary');
                    }
                }
                //insert for new details
                $cnt = isset($_POST['new_product_id']) ? count($_POST['new_product_id']) : 0;
                for ($i = 0; $i < $cnt; $i++) {
                    $prod_id = $_POST['new_product_id'][$i];
                    $qty = $_POST['new_voucher_qty'][$i];
                    $type = 1;
                    if ($_POST['campaign_type'] == 'Both') {
                        $type = ($qty % 2 == 0) ? 1 : 0;
                    }
                    if ($prod_id > 0 && $qty > 0 && $type > 0) {

                        $this->campaign_m->create_campaign_details($camp_id, $prod_id, $qty, 'by_promo_primary');
                    }
                }

                if (!isset($_POST['follow_up_campaign'])) {
                    
                } elseif ($_POST['follow_up_campaign'] == 'enable') {

                    //update existing record into  the database follow up
                    $cnt = count($_POST['f_product_id']);
                    for ($i = 0; $i < $cnt; $i++) {
                        $prod_id = $_POST['f_product_id'][$i];
                        $qty = $_POST['f_voucher_qty'][$i];
                        $details_id = $_POST['f_cam_details_id'][$i];
                        $type = 1;
                        if ($_POST['campaign_type'] == 'Both') {
                            $type = ($qty % 2 == 0) ? 1 : 0;
                        }
                        if ($prod_id > 0 && $qty > 0 && $type > 0) {

                            $this->campaign_m->update_campaign_details($details_id, $prod_id, $qty, 'by_promo_follow_up');
                        }
                    }
                    //insert for new details
                    $cnt = count($_POST['f_new_product_id']);
                    for ($i = 0; $i < $cnt; $i++) {
                        $prod_id = $_POST['f_new_product_id'][$i];
                        $qty = $_POST['f_new_voucher_qty'][$i];
                        $type = 1;
                        if ($_POST['campaign_type'] == 'Both') {
                            $type = ($qty % 2 == 0) ? 1 : 0;
                        }
                        if ($prod_id > 0 && $qty > 0 && $type > 0) {

                            $this->campaign_m->create_campaign_details($camp_id, $prod_id, $qty, 'by_promo_follow_up');
                        }
                    }
                }
                //
                //update rule 
                $this->campaign_m->update_campaign_rule($camp_id);

                $this->session->set_flashdata('msg', "Record successfully updated.");
                echo json_encode(array('error' => '', 'redirect' => ADMIN_FOLDER . "/campaign_by_promo/"));
                return;
            } else {
                echo json_encode(array('error' => $this->form_validation->error_array()));
                return;
            }
        }
        
        $this->db->select('bp.id, bp.name');
        $this->db->from('gl_brandproduct bp');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $white_label_merchants = $this->db->get();

        $this->db->select('p.id,p.product_id, bp.name, gl_p.prod_name');
        $this->db->from('gl_brands_products p');
        $this->db->join('gl_product gl_p', 'gl_p.prod_id = p.product_id', 'left');
        $this->db->join('gl_brandproduct bp', 'p.brandproduct_id = bp.id', 'left');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $this->db->order_by("bp.name ASC, gl_p.prod_name ASC");
        $query = $this->db->get(); 
        $white_label_products = $query;
        
        
        //print_r($data['res_camp_rule']->row());
        
        $rule_details = json_decode($data['res_camp_rule']->row()->rule_details);
        
        //print_r($rule_details);
        $map = array(
                'all_list' => $this->campaign_m->white_label_list,
                'selected_list' => isset($rule_details->selected_list) ? $rule_details->selected_list:'',
                
                'all_actions' => $this->campaign_m->white_label_actions,
                'selected_actions' => isset($rule_details->selected_actions) ? $rule_details->selected_actions:'',
                
                'all_merchants' => $white_label_merchants,
                'selected_merchants' => isset($rule_details->selected_merchants) ? $rule_details->selected_merchants:'',
                
                'all_products' => $white_label_products,
                'selected_products'=> isset($rule_details->selected_products) ? $rule_details->selected_products:'',
                
                'date_from' => isset($rule_details->date_from) ? $rule_details->date_from:'',
                'date_to' =>isset($rule_details->date_to) ? $rule_details->date_to:''
        );
        $white_label = new stdClass();
        foreach($map as $k => $v)
            $white_label->$k = $v;
        $data['white_label'] = $white_label;
        
        $data['whitelabel_id_checked'] = ( isset($rule_details->selected_list) && $rule_details->selected_list !='') ? 'checked' : '';
        $data['whitelabel_actions_checked'] = ( isset($rule_details->selected_actions) && $rule_details->selected_actions !='') ? 'checked' : '';
        $data['whitelabel_merchants_checked'] = ( isset($rule_details->selected_merchants) && $rule_details->selected_merchants !='') ? 'checked' : '';
        $data['whitelabel_products_checked'] = ( isset($rule_details->selected_products) && $rule_details->selected_products !='') ? 'checked' : '';
        $data['date_from_checked'] = ( isset($rule_details->date_from) && $rule_details->date_from !='' && $rule_details->date_from !='0') ? 'checked' : '';
        
       
//       foreach($this->campaign_m->select_people_by_rule(10, "")->result() as $row)
       //{
            //echo ($row->user_id);
       //}
       
        $this->load->view(ADMIN_FOLDER . '/campaign_by_promo_v', $data);
    }

    function details($camp_id) {

        $camp_id = (int) $camp_id;
        $data['camp_id'] = $camp_id;
        $data['page_action'] = "Details";
        $data['page_title'] = "Campaign Details (by promo code)";
        $data['page_subtitle'] = "";


        $res_camp = $this->campaign_m->campaign_by_id_by_promo($camp_id);
        if ($res_camp->num_rows() != 1) {
            redirect(ADMIN_FOLDER . "/campaign_by_promo/");
            exit();
        }
        $rec_campaign = $res_camp->row();
        if ($rec_campaign->campaign_by_type != "by_promo") {
            $this->session->set_flashdata('error_msg', "Sorry, this is not a campaign by promo code.");
            redirect(admin_url("campaign_by_promo"));
            exit();
        }
        $data['rec_camp'] = $rec_campaign;
        $data['res_camp_details_f'] = $this->campaign_m->select_voucher_product($camp_id, 'by_promo_follow_up');
        $data['res_camp_details'] = $this->campaign_m->select_voucher_product($camp_id, 'by_promo_primary');
        $data['res_camp_rule'] = $this->campaign_m->select_campaign_rule($camp_id);

        $this->load->view(ADMIN_FOLDER . '/campaign_by_promo_details_v', $data);
    }
    
    function close_campaign($id) {
        $this->db->_protect_identifiers=false;
        $this->db->select('camp_details_id, camp_prod_id, camp_voucher_qty');
        $this->db->join('gl_categories', 'cat_name = "Sponsor"');
        $this->db->join('gl_product', 'prod_id = camp_prod_id AND prod_cat_id = cat_id');
        $campaign_details = $this->db->get_where('gl_campaign_details', array('camp_campaign_id' => $id));
        
        $this->db->where('campaign_id', $id);
        $this->db->update('gl_campaign_details_by_promo', array('campaign_by_promo_status' => 'Done'));

//TODO: ENABLE WHEN NEED TO HAVE SPONSOR INVENTORY
//        if ($campaign_details->num_rows() > 0)
//        {
//           foreach ($campaign_details->result() as $row)
//           {
//                $this->db->select('COUNT(camp_voucher_id) AS assigned_qty');
//                $campaign_voucher = $this->db->get_where('gl_campaign_voucher', array(
//                    'camp_campaign_id' => $id,
//                    'camp_prod_id' => $row->camp_prod_id,
//                    'camp_voucher_assign_status' => 'Assigned'
//                ));
//                
//                if ($campaign_voucher->num_rows() == 0) {
//                    $assigned_qty = 0;
//                } else {
//                    $assigned_qty = $campaign_voucher->row()->assigned_qty;
//                }
//                
//                $available = $row->camp_voucher_qty - $assigned_qty;
//                
//                $this->db->order_by("prod_inv_id", "desc");
//                $product_inventory = $this->db->get_where('gl_product_inventory', array(
//                    'prod_id' => $row->camp_prod_id,
//                ));
//                
//                $data = array(
//                    'prod_id'=> $row->camp_prod_id,
//                    'change_qty' => $available,
//                    'current_qty' => $product_inventory->row()->current_qty + $available,
//                    'details'=> 'Added ('. $available .') due to closing of campaign',
//                    'timestamp' => date('Y-m-d H:i:s')
//                );
//
//                $this->db->insert('gl_product_inventory', $data); 
//           }
//        }
        
        $this->session->set_flashdata('msg', "Record successfully updated.");
        redirect(admin_url("campaign_by_promo"));
    }

    function check_if_date($date) {
        $month = (int) substr($date, 0, 2);
        $day = (int) substr($date, 3, 2);
        $year = (int) substr($date, 6, 4);
        return checkdate($month, $day, $year);
    }

    function check_if_promo_code_available($code) {
        $temp = $this->campaign_m->get_this_promo_code($code);
        if ($temp->num_rows() > 0) {
            $this->form_validation->set_message('check_if_promo_code_available', 'This promo code already in use.');
            return false;
        }
        else
            return true;
    }

    function check_if_promo_code_available_update($code) {
        $camp_id = $this->session->userdata('camp_id');
        $temp = $this->campaign_m->get_this_promo_code_update($code, $camp_id);
        if ($temp->num_rows() > 0) {
            $this->form_validation->set_message('check_if_promo_code_available_update', 'This promo code already in use.');
            return false;
        }
        else
            return true;
    }

    function remove_cam_details($id = 0) {
        $this->campaign_m->remove_campaign_details($id);
        die('success');
    }

}
