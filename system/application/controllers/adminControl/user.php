<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Prakash
Date: Aug-06-2013
Desc: User
*/

class User extends CI_Controller
{
	private $pagination_per_page= 25;
	function User()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('users_m');
		$this->load->model('regions_m');
		$this->load->library('lib_pagination');
		$this->load->model('login_m');
	}
	function index($parent_region=0)
	{
		
		$data['page_action']="view";
		$data['page_title']="Manage User : Glomp";
		$subQuery=" AND user_id !='1' ";
		$queryString="";
		
		if(isset($_GET['Search']))
		{
		$keywords 		= isset($_GET['keywords'])?urlencode($_GET['keywords']):"";
		if($keywords!="")
			{
			$subQuery='AND (user_email LIKE "%'.$keywords.'%" OR username LIKE "%'.$keywords.'%")';
			$queryString="&keywords=$keywords";
			}
		}
			$res_total_row  = $this->users_m->selectUser($subQuery);
			if(isset($_GET['per_page']) && is_numeric($_GET['per_page']))
			$page = intval($_GET['per_page']);
			else
			$page =0;
			$totalRow = $res_total_row->num_rows();
			$offset = $page;
			$row_per_page = $this->pagination_per_page;
			$data['resUser']= $this->users_m->selectUser($subQuery."ORDER BY user_last_updated_date DESC LIMIT " .$offset.",".$row_per_page);
			$config['base_url'] = base_url(ADMIN_FOLDER."/user/?q=q&$queryString");		
			$config['total_rows'] = $totalRow;
			$config['per_page'] = $row_per_page;
			$config['page_query_string'] = TRUE;
			$this->pagination->initialize($config);
			if($this->session->flashdata('msg'))
		$data['msg'] = $this->session->flashdata('msg');
		if($this->session->flashdata('err_msg'))
		$data['error_msg']=$this->session->flashdata('err_msg');
		$this->load->view(ADMIN_FOLDER.'/user_v',$data);
			
	}

	function deleteRecord($userID=0)
	{
			$userID = (int)($userID);
			$this->users_m->delete($userID);			
			redirect(ADMIN_FOLDER."/user/index/");
			exit();
	}
	
	
	function updateStatus($userID=0,$status)
	{
			$userID = (int)($userID);
			$this->users_m->updateStatus($userID,$status);
			$this->session->set_flashdata('msg',"Record updated successfully");
			redirect(ADMIN_FOLDER."/user/index/");
			exit();
	}
	
	
	
	function viewDetails($userID)
		{
			$userID = (int)($userID);
			$res=$this->users_m->selectUser("AND user_id='".$userID."'");
			if($res->num_rows()>0)
			{
				
				$data['page_action']="viewDetails";
				$data['page_title']="User Details: Glomp";
				$data['resUser']=$res;
				
				//
				$data['resSummary']=$this->user_account_m->user_summary($userID);
				
					$res_total_row  = $this->users_m->selectActivityLogByID($userID,$subQuery='');
					$totalRow = $res_total_row->num_rows();
					if(isset($_GET['per_page']) && is_numeric($_GET['per_page']))
					$page = intval($_GET['per_page']);
					else
					$page =0;
					$offset = $page;
					$row_per_page = $this->pagination_per_page;
					$data['resActivity']= $this->users_m->selectActivityLogByID($userID, ' LIMIT '.$offset.",".$row_per_page);
					$config['base_url'] = base_url(ADMIN_FOLDER."/user/viewDetails/$userID/?");		
					$config['total_rows'] = $totalRow;
					$config['per_page'] = $row_per_page;
					$config['page_query_string'] = TRUE;
					$this->pagination->initialize($config);
			
				//
				
				$this->load->view(ADMIN_FOLDER.'/user_v',$data);
			}
			else
			{
			redirect(ADMIN_FOLDER."/user/index/");
			exit();
			}
		}
	function edit($userID)
		{
			$userID = (int)($userID);
			$user_id = $userID;
			
			if(isset($_POST['update_profile']))
			{
			$this->form_validation->set_rules('fname', 'first Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lname', 'last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
			$this->form_validation->set_rules('months', 'Months', 'trim|required');
			$this->form_validation->set_rules('year', 'Year', 'trim|required');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
			$this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
			
			
			if($this->input->post('new_pword')!="")
			{
			$this->form_validation->set_rules('new_pword', 'New Password', 'trim|required|min_length[6]|max_length[20]|xss_clean');
			$this->form_validation->set_rules('confirm_pword', 'Confirm Password', 'trim|required|matches[new_pword]');
			}
				if($this->form_validation->run() == TRUE)
				{
					$this->login_m->update_profile($user_id);
					//
					$data['success_msg'] = "user profile updated successfully";
					if(strlen($this->input->post('new_pword'))>5)
					{
						//change password
						$new_password = $this->input->post('new_pword');
						$this->login_m->change_password($new_password,$user_id);
					}
				//update profile pic
						/*upload photo start*/
			if($_FILES['user_photo']['tmp_name']!="")
				{
				 $config = array(
						'allowed_types' => 'jpeg|jpg|gif|png',
						'upload_path' => 'custom/uploads/users/', 
						'maximum_filesize' => 2
				   );
				   
				   $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
				   $uploadError=preg_match("/^Error/", $user_photo)?$user_photo:NULL;
					if(empty($uploadError)){
						
						$this->login_m->updatePhoto($user_photo,$user_id);
						 $this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
					}
					else
					{
						$uploadError; 
						//exit();
					}
				}//user photo temp check
			/******upload photo end***/
				}
			
			}
		
			
			
			$res=$this->users_m->selectUser("AND user_id='".$userID."'");
			if($res->num_rows()>0)
			{
				
				$data['page_action']="edit";
				$data['page_title']="Update User";
				$data['user_record']=$res->row();
				$data['user_id'] = $userID;
				
				
				$this->load->view(ADMIN_FOLDER.'/user_v',$data);
			}
			else
			{
			redirect(ADMIN_FOLDER."/user/index/");
			exit();
			}
		}
		
	function email_check($email)
	{
		$user_id = $this->input->post('user_id');
		$email_chk = $this->login_m->update_email_exist_check($email,$user_id);
		if($email_chk==0)
		return true;
		else
		{
			$this->form_validation->set_message('email_check', 'Email address already exists. Please try new one');
			return false;
		}
	}
}
?>