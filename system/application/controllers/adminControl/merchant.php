<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  Author: Shree
  Date: July-28-2012
  Desc: Merchant
 */

class Merchant extends CI_Controller {

    private $pagination_per_page = 20;

    function Merchant() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('merchant_m');
        $this->load->model('merchant_portal_m');
        $this->load->model('regions_m');
        $this->load->library('lib_pagination');
        $this->load->model('address_m');
    }

    function index($parent_region = 0) {


        $temp = ($this->address_m->address_format('address 1', 'address 2', 'address 3', 'address 4', 'address 5', 'address 6', 'address 7', 'hong kong'));
        //echo $temp->address_formatted;
        $parent_region = (int) ($parent_region);
        //echo $this->session->flashdata('msg'); die();
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Merchant : Glomp";
        $data['view_as'] = "";
        $data['search_q'] = "";
        
        $search_q = $this->input->get('search_q');
        $search_query = '';
        
        if (!empty($search_q)) {
            $data['search_q'] = $search_q;
            $search_query = '&search_q=' . $search_q;
        }

        $location_id = isset($_GET['location_id']) ? urlencode($_GET['location_id']) : 0;
        $subQuery = '';
        if ($location_id > 0) {
            $subQuery.=" region_id ='" . $location_id . "' AND ";
            /* pagination starts */
            if ($this->uri->segment(5))
                $start = $this->uri->segment(5);
            else
                $start = 0;
            $per_page = $this->pagination_per_page;
            $pagination_base_url = base_url(ADMIN_FOLDER . "/merchant/index/" . $parent_region . "/");
            $num_rows = $this->merchant_m->selectMerchants(0, 0, $subQuery, $search_q)->num_rows();
            
            $config['base_url'] = $pagination_base_url;
            $config['total_rows'] = $num_rows;
            $config['enable_query_strings'] = true;
            $config['query_string'] = '?location_id=' . $location_id.$search_query;

            if ($per_page > 0)
                $config['per_page'] = $per_page;
            else
                $config['per_page'] = PAGINATION_PER_PAGE;

            $config['first_url'] = $pagination_base_url;
            $config['uri_segment'] = 5;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['cur_tag_open'] = '<span class="active">';
            $config['cur_tag_close'] = '</span>';
            $this->pagination->initialize($config);
            $data['links'] = $this->pagination->create_links();
            
            
            $data['res_merchant'] = $this->merchant_m->selectMerchants($start, $per_page, $subQuery, $search_q)->result();
            /* pagination ends */
        }
        $data['location_id'] = $location_id;
        
        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');
        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');

        $this->load->view(ADMIN_FOLDER . '/merchant_v', $data);
    }

    function generate_outlet_code($region_id = 0) {
        echo $this->merchant_m->generate_outlet_code($region_id);
    }

    function grid() {
        $data['page_action'] = "grid";
        $data['page_title'] = "Manage Merchant : Glomp";
        $data['view_as'] = "grid";

        $data['search_q'] = "";
        $search_q = $this->input->get('search_q');
        if (!empty($search_q)) {
            $data['search_q'] = $search_q;
        }

        $location_id = isset($_GET['location_id']) ? urlencode($_GET['location_id']) : 0;
        $subQuery = '';
        if ($location_id > 0) {
            $subQuery.=" region_id ='" . $location_id . "' AND ";
        }
        $data['location_id'] = $location_id;
        /* pagination starts */
        if ($this->uri->segment(5))
            $start = $this->uri->segment(5);
        else
            $start = 0;
        $data['res_merchant'] = $this->merchant_m->selectMerchants(0, 0, $subQuery, $search_q)->result();
        /* pagination ends */

        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');
        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');

        $this->load->view(ADMIN_FOLDER . '/merchant_v', $data);
    }

    function updateRanks() {
        if ($_POST) {
            $sortedIDs = $_POST['sortedIDs'];
            $location_id = $_POST['location_id'];
            $this->merchant_m->updateRanking($sortedIDs, $location_id);
        }
    }

    function viewInMerchantPortal($merchant_id = 0) {
        $merchant_id = (int) $merchant_id;
        if ($this->merchant_m->selectMerchantID($merchant_id)->num_rows() > 0) {
            $merchant_name = $this->merchant_portal_m->getMerchantName($merchant_id);
            //echo MERCHANT_FOLDER; die();
            $data = array('merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'merchant_user_id' => '0',
                'merchant_username' => $this->session->userdata('admin_name'),
                'is_merchant_users' => true,
                'is_logged_in' => true
            );

            $this->session->set_userdata($data);
            $merchant_id = $this->session->userdata('merchant_id');

            //die("password matched");
            // $this->session->set_userdata('login_attempt',0);
            // $this->session->set_userdata('since_logged_in',time());
            redirect(base_url('index.php/' . MERCHANT_FOLDER . '/dashboard'));
            exit();
        } else {
            show_404();
        }
    }

    function deleteRecord($contentID = 0, $outlet_id = 0) {
        $contentID = (int) ($contentID);
        $outlet_id = (int) $outlet_id;
        if ($outlet_id > 0) {
            $res = $this->merchant_m->deleletOutlet($outlet_id);
            $this->session->set_flashdata('msg', "Record deleted successfully");
            /*
              if($res=="TRUE")
              $this->session->set_flashdata('msg',"Record deleted successfully");
              else
              else
              $this->session->set_flashdata('error_msg',"Sorry, this record is not allowed to delete");


             */
            redirect(ADMIN_FOLDER . "/merchant/editRecord/" . $contentID);
        } else {
            $this->merchant_m->delete($contentID);
            $this->session->set_flashdata('msg', "Record deleted successfully");
            redirect(ADMIN_FOLDER . "/merchant/index/");
        }
        exit();
    }

    function addMerchant() {
        $data['page_action'] = "add";
        $data['page_title'] = "Manage Merchant : glomp";
        $data['view_as'] = "";
        $data['location_id'] = "0";
        $data['search_q'] = "";
        if (isset($_POST['addPage'])) {
            $this->form_validation->set_rules('merchant_country', 'Merchant Country', 'required');

            $this->form_validation->set_rules('merchant_email', 'Merchant Email', 'required|email');
            $this->form_validation->set_rules('merchant_name', 'Merchant Name', 'required');
            //$this->form_validation->set_rules('merchant_rank', 'Order Position', 'required|numeric');
            $this->form_validation->set_rules('merchant_contact_name', 'Key Contact Name', 'required');
            $this->form_validation->set_rules('accounts_contact_email', 'Accounts Contact E-mail', 'email');

            $this->form_validation->set_rules('notify_excempt_amount', 'Minimum account settlement', 'numeric');
            $this->form_validation->set_rules('notify_top_limit', 'Credit limit', 'numeric');

            $this->form_validation->set_rules('merchant_username', 'Username', 'trim|required|callback_check_merchant_username_if_exists_add');
            $this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
            $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');

            $this->form_validation->set_rules('region_id', 'Merchant City', 'required');

            $this->form_validation->set_rules('merchant_logo', 'Merchant Logo', 'callback_logoUploadCheck');
            $this->form_validation->set_rules('merchant_contact', 'Merchant Contact', 'required');

            $this->form_validation->set_rules('merchant_address_1', 'Merchant Address', 'required');
            //$this->form_validation->set_rules('merchant_street', 'Merchant Street', 'required');

            if (isset($_POST['merchant_locality_required']) && $_POST['merchant_locality_required'] == "1") {
                //$this->form_validation->set_rules('merchant_locality_name', 'Merchant Locality Name', 'required');			
                //$this->form_validation->set_rules('merchant_locality', 'Merchant Locality', 'required');			
            }

            //$this->form_validation->set_rules('merchant_city', 'Merchant City', 'required');
            //$this->form_validation->set_rules('merchant_zip', 'Merchant Zip/Postal Code', 'required');			


            $this->form_validation->set_rules('merchant_about', 'Merchant About', 'required');
            $this->form_validation->set_rules('merchant_terms', 'Merchant Terms and Conditions', '');

            if ($this->form_validation->run() == FALSE) {
                $data['merchant_street'] = $this->input->post('merchant_street');
                $data['merchant_city'] = $this->input->post('merchant_city');
                $this->load->view(ADMIN_FOLDER . "/merchant_v", $data);
            } else {
                $insert_id = $this->merchant_m->addRecord();
                //upload logo

                if ($_FILES['merchant_logo'] != "") {

                    $config = array(
                        'allowed_types' => 'jpeg|jpg|gif|png',
                        'upload_path' => 'custom/uploads/merchant/',
                        'maximum_filesize' => 2
                    );

                    $logo_name = $this->custom_func->custom_upload($config, $_FILES['merchant_logo']);
                    $uploadError = preg_match("/^Error/", $logo_name) ? $logo_name : NULL;
                    if (empty($uploadError)) {

                        $old_image = $this->input->post('old_image');
                        $this->merchant_m->updateLogo($logo_name, $insert_id, $old_image);
                        $this->resizeImg("custom/uploads/merchant/" . $logo_name, "custom/uploads/merchant/thumb/" . $logo_name, 160, 160);
                    }
                }
                $this->session->set_flashdata('msg', "Record successfully added.");
                redirect(ADMIN_FOLDER . "/merchant/index/?location_id=" . $this->input->post('region_id'));
                exit();
            }
        } else {
            $this->load->view(ADMIN_FOLDER . "/merchant_v", $data);
        }
    }

    function editRecord($contentID = 0, $outlet_id = 0) {
        $contentID = (int) ($contentID);
        if ($this->merchant_m->selectMerchantID($contentID)->num_rows() > 0) {
            $data['search_q'] = "";
            $data['rec_merchant'] = $this->merchant_m->selectMerchantID($contentID)->row();
            $data['rec_merchant_address'] = $this->merchant_m->getAddress($data['rec_merchant']->merchant_address_id)->row();
            $data['page_action'] = "edit";
            $data['outlet_code'] = "";
            $data['page_title'] = "Manage Merchant : Glomp";
            $data['view_as'] = "";
            $data['msg'] = $this->session->flashdata('msg');
            $data['location_id'] = "0";
            $data['res_outlet'] = $this->merchant_m->selectOutlet($contentID);

            if ($outlet_id > 0) {
                $data['res_outlet_by_id'] = $this->merchant_m->outletById($outlet_id)->row();
                //                        echo '<pre>';
                //                        print_r($data['res_outlet_by_id']);
                //                        exit;
                $data['res_outlet_address'] = $this->merchant_m->getAddress($data['res_outlet_by_id']->outlet_address_id)->row();
                $data['outlet_edit'] = true;
            }
            if (isset($_POST['add_outlet'])) {

                $this->form_validation->set_rules('outlet_name', 'Outlet Name', 'required');
                if ($_POST['outlet_id'] == 0)
                    $this->form_validation->set_rules('outlet_code', 'Outlet Code', 'required|callback_codeCheck');

                $this->form_validation->set_rules('outlet_phone_number', 'Outlet Phone Number', 'required');
                $this->form_validation->set_rules('outlet_country', 'Outlet Country', 'required');
                //$this->form_validation->set_rules('outlet_address_id', 'Merchant Address ID', 'required');						
                $this->form_validation->set_rules('outlet_address_1', 'Outlet Address', 'required');
                //$this->form_validation->set_rules('outlet_street', 'Outlet Street', 'required');

                if (isset($_POST['outlet_locality_required']) && $_POST['outlet_locality_required'] == "1") {
                    //$this->form_validation->set_rules('outlet_locality_name', 'Outlet Locality Name', 'required');			
                    //$this->form_validation->set_rules('outlet_locality', 'Outlet Locality', 'required');			
                }

                //$this->form_validation->set_rules('outlet_city', 'Outlet City', 'required');
                //$this->form_validation->set_rules('outlet_zip', 'Outlet Zip/Postal Code', 'required');	


                if ($this->form_validation->run() == FALSE) {
                    $this->load->view(ADMIN_FOLDER . "/merchant_v", $data);
                } else {
                    if ($_POST['outlet_id'] == 0) {
                        $this->merchant_m->addOutlet($contentID);
                        $data['msg'] = "Record successfully added.";
                    } else {
                        $this->merchant_m->updateOutlet($_POST['outlet_id']);
                        $data['msg'] = "Record successfully updated.";
                    }
                }
            }
            $data['res_outlet'] = $this->merchant_m->selectOutlet($contentID);

            if (isset($_POST['editPage'])) {
                $this->form_validation->set_rules('merchant_country', 'Merchant Country', 'required');
                $this->form_validation->set_rules('merchant_name', 'Merchant Name', 'required');
                $this->form_validation->set_rules('merchant_email', 'Merchant Email', 'required|email');
                //$this->form_validation->set_rules('merchant_rank', 'Order Position', 'required|numeric');

                $this->form_validation->set_rules('merchant_contact_name', 'Key Contact Name', 'required');
                $this->form_validation->set_rules('accounts_contact_email', 'Accounts Contact E-mail', 'email');

                $this->form_validation->set_rules('notify_excempt_amount', 'Minimum account settlement', 'numeric');
                $this->form_validation->set_rules('notify_top_limit', 'Credit limit', 'numeric');

                //$this->form_validation->set_rules('region_id', 'Merchant City', 'required');
                $this->form_validation->set_rules('merchant_username', 'Username', 'trim|required|xss_clean|callback_check_merchant_username_if_exists');

                if ($_POST['npword'] != "") {
                    $this->session->set_userdata('current_merchant_id', $contentID);
                    $this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
                    $this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
                }

                $this->form_validation->set_rules('merchant_contact', 'Merchant Contact', 'required');

                $this->form_validation->set_rules('merchant_address_id', 'Merchant Address ID', 'required');
                $this->form_validation->set_rules('merchant_address_1', 'Merchant Address', 'required');
                //$this->form_validation->set_rules('merchant_street', 'Merchant Street', 'required');

                if (isset($_POST['merchant_locality_required']) && $_POST['merchant_locality_required'] == "1") {
                    //$this->form_validation->set_rules('merchant_locality_name', 'Merchant Locality Name', 'required');			
                    //$this->form_validation->set_rules('merchant_locality', 'Merchant Locality', 'required');			
                }

                //$this->form_validation->set_rules('merchant_city', 'Merchant City', 'required');
                //$this->form_validation->set_rules('merchant_zip', 'Merchant Zip/Postal Code', 'required');

                $this->form_validation->set_rules('merchant_about', 'Merchant About', 'required');



                //echo $this->MY_Input->post('merchant_about',false);





                if ($this->form_validation->run() == FALSE) {
                    $data['merchant_street'] = $this->input->post('merchant_street');
                    $data['merchant_city'] = $this->input->post('merchant_city');
                    $this->load->view(ADMIN_FOLDER . "/merchant_v", $data);
                } else {
                    $this->merchant_m->editRecord($contentID);

                    //upload logo
                    if ($_FILES['merchant_logo'] != "") {

                        $config = array(
                            'allowed_types' => 'jpeg|jpg|gif|png',
                            'upload_path' => 'custom/uploads/merchant/',
                            'maximum_filesize' => 2
                        );

                        $logo_name = $this->custom_func->custom_upload($config, $_FILES['merchant_logo']);
                        $uploadError = preg_match("/^Error/", $logo_name) ? $logo_name : NULL;
                        if (empty($uploadError)) {

                            $old_image = $this->input->post('old_image');
                            $this->merchant_m->updateLogo($logo_name, $contentID, $old_image);
                            $this->resizeImg("custom/uploads/merchant/" . $logo_name, "custom/uploads/merchant/thumb/" . $logo_name, 160, 160);
                        }
                    }

                    // upload banner
                    if( $_FILES['banner'] != '' ) {

                        $config = array(
                            'allowed_types' => 'jpeg|jpg|gif|png',
                            'upload_path' => 'custom/uploads/merchant/',
                            'maximum_filesize' => 4
                        );

                        $banner_name = $this->custom_func->custom_upload($config, $_FILES['banner']);
                        $uploadError = preg_match("/^Error/", $banner_name) ? $banner_name : NULL;
                        if (empty($uploadError)) {

                            $old_image = $this->input->post('old_image');
                            $this->db->where(array('merchant_id'=>$contentID));
                            $this->db->update('gl_merchant',array('banner'=>$banner_name));
                            $this->resizeImg("custom/uploads/merchant/" . $banner_name, "custom/uploads/merchant/thumb/" . $banner_name, 160, 160);

                        }
                    }

                    $this->session->set_flashdata('msg', "Record successfully updated.");
                    //redirect(ADMIN_FOLDER."/merchant/index/");
                    redirect(ADMIN_FOLDER . "/merchant/index/?location_id=" . $this->input->post('region_id'));
                    exit();
                }
            } else {
                $this->load->view(ADMIN_FOLDER . "/merchant_v", $data, false, false);
            }
        } else {
            show_404();
        }
    }

    function reports() {
        $data['page_title'] = "Merchant : Reports";
        $data['current_week'] = isset($_GET['week']) ? $_GET['week'] : date('W');
        $data['current_year'] = isset($_GET['year']) ? $_GET['year'] : date('Y');
        $data['current_month_year'] = isset($_GET['month_year']) ? urldecode($_GET['month_year']) : date('F') . ' ' . date('Y');

        $data['weekly_sel'] = isset($_GET['week']) ? 'true' : 'false';
        $data['monthly_sel'] = isset($_GET['month_year']) ? 'true' : 'false';

        $this->load->view(ADMIN_FOLDER . "/merchant_reports_v", $data, false, false);
    }

    function get_merchant_reports() {
        $this->load->model('voucher_m');

        if (isset($_POST['report_filter'])) {
            if ($_POST['report_filter'] == 'monthly')
                return $this->_get_merchant_monthly_reports();
        }
        //Pagination
        $page = $this->input->post('page', TRUE);
        $rows = $this->input->post('rows', TRUE);
        //Sort
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'merchant_id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $page = isset($page) ?
                intval($page) : 1;
        $rows = isset($rows) ? intval($rows) : 10;

        //$from_date = date('m/d/y', strtotime('last week monday -1 day', strtotime('2014W27')));
        //$last_week = date('m/d/y', strtotime('last week sunday -1 day', strtotime('2014W27')));

        $week_formatter = array(
            '1' => '01',
            '2' => '02',
            '3' => '03',
            '4' => '04',
            '5' => '05',
            '6' => '06',
            '7' => '07',
            '8' => '08',
            '9' => '09',
        );

        $week = (array_key_exists($_POST['week'], $week_formatter)) ? $week_formatter[$_POST['week']] : $_POST['week'];
        $year = $_POST['year'];

        $from_date = date('Y/m/d', strtotime('monday -1 day', strtotime($year . 'W' . $week))) . ' 00:00:00';
        $to_date = date('Y/m/d', strtotime('sunday -1 day', strtotime($year . 'W' . $week))) . ' 23:59:59';

        $args = array($sort, $order, $rows, $page);
        $rec = $this->merchant_m->get_weekly_report_list($from_date, $to_date, $args, TRUE);

        $items = array();
        foreach ($rec->result() as $row) {
            array_push($items, array(
                'merchant_id' => $row->merchant_id,
                'location' => $row->location,
                'merchant_name' => $row->merchant_name,
                'merchant_contact_name' => $row->merchant_contact_name,
                'accounts_contact_name' => $row->accounts_contact_name,
                'amount' => number_format($row->amount, 2, '.', ''),
            ));
        }

        $result["rows"] = $items;
        $result["total"] = $rec->num_rows();
        echo json_encode($result);
    }

    function _get_merchant_monthly_reports() {
        //Pagination
        $page = $this->input->post('page', TRUE);
        $rows = $this->input->post('rows', TRUE);
        //Sort
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'merchant_id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $page = isset($page) ?
                intval($page) : 1;
        $rows = isset($rows) ? intval($rows) : 1;

        $month_year = explode(' ', $_POST['month_year']);
        list($month, $year) = $month_year;

        $from_date = date('Y/m/d', strtotime('first day of this month', strtotime($year . ' ' . $month))) . ' 00:00:00';
        $to_date = date('Y/m/d', strtotime('last day of this month', strtotime($year . ' ' . $month))) . ' 23:59:59';

        $args = array($sort, $order, $rows, $page);
        $rec = $this->merchant_m->get_monthly_report_list($from_date, $to_date, $args, TRUE);

        $items = array();
        foreach ($rec->result() as $row) {
            array_push($items, array(
                'merchant_id' => $row->merchant_id,
                'location' => $row->location,
                'merchant_name' => $row->merchant_name,
                'merchant_contact_name' => $row->merchant_contact_name,
                'accounts_contact_name' => $row->accounts_contact_name,
                'amount' => number_format($row->amount, 2, '.', ''),
                'notify_excempt_amount' => $row->notify_excempt_amount,
            ));
        }

        $result["rows"] = $items;
        $result["total"] = $rec->num_rows();
        echo json_encode($result);
    }

    function download_report() {
        $this->load->library('excel');

        $weekly_sel = isset($_GET['week']) ? 'true' : 'false';
        $monthly_sel = isset($_GET['month_year']) ? 'true' : 'false';

        if ($monthly_sel == 'true') {
            return $this->download_report_monthly();
        }

        $filename = date('m-d-Y-H:i:s') . '-weekly.xlsx';
        $path = FCPATH . "custom/uploads/merchant/" . $filename;

        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Cache-Control: max-age=0");

        $week_formatter = array(
            '1' => '01',
            '2' => '02',
            '3' => '03',
            '4' => '04',
            '5' => '05',
            '6' => '06',
            '7' => '07',
            '8' => '08',
            '9' => '09',
        );

        $week = (array_key_exists($_GET['week'], $week_formatter)) ? $week_formatter[$_GET['week']] : $_GET['week'];
        $year = $_GET['year'];

        $from_date = date('Y/m/d', strtotime('monday -1 day', strtotime($year . 'W' . $week))) . ' 00:00:00';
        $to_date = date('Y/m/d', strtotime('sunday -1 day', strtotime($year . 'W' . $week))) . ' 23:59:59';

        $rec = $this->merchant_m->get_weekly_report_list($from_date, $to_date, array(), FALSE);

        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("glomp!");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Weekly Report: ' . date('F d, Y', strtotime($from_date)) . ' - ' . date('F d, Y', strtotime($to_date)));
        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Location');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Merchant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', 'Key Contact Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('D3', 'Accounts Contact Person');
        $objPHPExcel->getActiveSheet()->SetCellValue('E3', 'Amount');

        $row_incremental = 4;
        $total_amount = (float) 0;
        foreach ($rec->result() as $r) {
            $format_amount = (string) number_format($r->amount, 2, '.', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $row_incremental, $r->location);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $row_incremental, $r->merchant_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $row_incremental, $r->merchant_contact_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $row_incremental, $r->accounts_contact_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $row_incremental, $format_amount);
            $total_amount = $total_amount + $format_amount; //Sum
            $row_incremental++;
        }
        //Footer
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $row_incremental, 'TOTAL AMOUNT');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $row_incremental, $total_amount);

        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);

        $objPHPExcel->getActiveSheet()
                ->getStyle("E")
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()
                ->getStyle("A3:E3")
                ->applyFromArray(array("font" => array("bold" => true)));

        $objPHPExcel->getActiveSheet()
                ->getStyle("A1")
                ->applyFromArray(array("font" => array("bold" => true)));

        //Footer
        $objPHPExcel->getActiveSheet()
                ->getStyle("A" . $row_incremental . ":E" . $row_incremental)
                ->applyFromArray(array("font" => array("bold" => true)));

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Report');

        // Save Excel 2007 file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        ob_end_clean();
        $objWriter->save('php://output'); //($path);
    }

    function download_report_monthly() {
        $filename = date('m-d-Y-H:i:s') . '-monthly.xlsx';
        $path = FCPATH . "custom/uploads/merchant/" . $filename;

        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Cache-Control: max-age=0");

        $month_year = explode(' ', $_GET['month_year']);
        list($month, $year) = $month_year;

        $from_date = date('Y/m/d', strtotime('first day of this month', strtotime($year . ' ' . $month))) . ' 00:00:00';
        $to_date = date('Y/m/d', strtotime('last day of this month', strtotime($year . ' ' . $month))) . ' 23:59:59';

        $rec = $this->merchant_m->get_monthly_report_list($from_date, $to_date, array(), FALSE);

        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("glomp!");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Monthly Report: ' . $_GET['month_year']);
        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Location');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Merchant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', 'Key Contact Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('D3', 'Accounts Contact Person');
        $objPHPExcel->getActiveSheet()->SetCellValue('E3', 'Amount');

        $row_incremental = 4;
        $total_amount = (float) 0;
        foreach ($rec->result() as $r) {
            $format_amount = (string) number_format($r->amount, 2, '.', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $row_incremental, $r->location);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $row_incremental, $r->merchant_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $row_incremental, $r->merchant_contact_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $row_incremental, $r->accounts_contact_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $row_incremental, $format_amount);
            $total_amount = $total_amount + $format_amount; //Sum
            $row_incremental++;
        }
        //Footer
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $row_incremental, 'TOTAL AMOUNT');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $row_incremental, $total_amount);

        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);

        $objPHPExcel->getActiveSheet()
                ->getStyle("E")
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()
                ->getStyle("A:D")
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $objPHPExcel->getActiveSheet()
                ->getStyle("A3:E3")
                ->applyFromArray(array("font" => array("bold" => true)));

        $objPHPExcel->getActiveSheet()
                ->getStyle("A1")
                ->applyFromArray(array("font" => array("bold" => true)));

        //Footer
        $objPHPExcel->getActiveSheet()
                ->getStyle("A" . $row_incremental . ":E" . $row_incremental)
                ->applyFromArray(array("font" => array("bold" => true)));

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Report');

        // Save Excel 2007 file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        ob_end_clean();
        $objWriter->save('php://output');
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'exact', $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    function password_check($password) {
        $merchant_id = $this->session->userdata('current_merchant_id');
        if ($password == "")
            return 'ok';
        //
        $password_verify = $this->merchant_m->password_verify($password, $merchant_id);
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check', 'Invalid current password.');
            return false;
        }
    }
    function check_merchant_username_if_exists_add($username)
    {   
        $q = $this->db->get_where( 'gl_merchant', "merchant_username = '".$username."' ");
        if( $q->num_rows()  > 0)
        {
            $this->form_validation->set_message('check_merchant_username_if_exists_add', 'Username already exists.');
            return false;
        }
        else
            return true;
    }
    function check_merchant_username_if_exists($username)
    {
        $merchant_id = $this->input->post('current_merchant_id');
        
        $q = $this->db->get_where( 'gl_merchant', "merchant_username = '".$username."' AND merchant_id <> '".$merchant_id."' ");
        if( $q->num_rows()  > 0)
        {
            $this->form_validation->set_message('check_merchant_username_if_exists', 'Username already exists.');
            return false;
        }
        else
            return true;
    }
    function codeCheck($code) {
        $outlet_id = $this->input->post('outlet_id');
        $merchant_id = $this->input->post('merchant_id');
        if ($outlet_id == 0)
            $sql = "SELECT 1 FROM gl_outlet WHERE outlet_merchant_id = '$merchant_id' AND outlet_code = '$code'";
        else
            $sql = "SELECT 1 FROM gl_outlet WHERE outlet_merchant_id = '$merchant_id' 
				AND outlet_code = '$code'
				AND outlet_id !='$outlet_id'
				";
        $result = $this->db->query($sql);

        $num = $result->num_rows();
        if ($num > 0) {
            $this->form_validation->set_message('codeCheck', 'Please enter unique outlet code.');
            return false;
        }
        else
            return true;
    }

    function logoUploadCheck($logo) {

        if ($_FILES['merchant_logo']["size"] == 0) {
            $this->form_validation->set_message('logoUploadCheck', 'Please add a logo.');
            return false;
        }
        else
            return true;
    }

    /**
     *
     * @param $id Integer brand_id
     */
    public function template( $brand_id = 0 ) {

        $data['template'] = $this->merchant_m->get_or_create_template( $brand_id );

        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            return;
        }

    }

}
