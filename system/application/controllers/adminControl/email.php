<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 */

class Email extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('email_templates_m');
        $this->load->model('lang_m');
    }

    function index() {

        //Data to be pass to the main header view
        $main_header['page_title'] = "Manage Email Template : Glomp";

        $subQuery = "";
        $queryString = "";

        if (isset($_GET['Search'])) {
            $keywords = isset($_GET['keywords']) ? urlencode($_GET['keywords']) : "";
            if ($keywords != "") {
                $subQuery = 'AND (user_email LIKE "%' . $keywords . '%" OR user_name LIKE "%' . $keywords . '%")';
                $queryString = "&keywords=$keywords";
            }
        }
        
        $join_lang = array('table' => 'gl_language lang'
            , 'condition' => 'lang.lang_id = et.lang_id');

        $records = $this->email_templates_m->get_list(array(
            'select' => 'id, date_created, template_name, lang.lang_name',
            'join' => array($join_lang),
        ));

        $res_total_row = count($records);

        if (isset($_GET['per_page']) && is_numeric($_GET['per_page'])) {
            $page = intval($_GET['per_page']);
        } else {
            $page = 0;
        }

        $totalRow = $res_total_row;
        $offset = $page;
        $row_per_page = 20;
        $data['email_templates'] = $records;
        $config['base_url'] = base_url(ADMIN_FOLDER . "/email/?q=q&$queryString");
        $config['total_rows'] = $totalRow;
        $config['per_page'] = $row_per_page;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        //Main body
        $this->load->view(ADMIN_FOLDER . '/email/list_v', $data);
    }

    function add() {
        $main_header['page_title'] = "Add Email Template : Glomp";
        
        $data['languages'] = $this->lang_m->get_list(array(
            'where' => "lang_active = 'Y' AND lang_deleted = 'N'",
            'order_by' => 'lang_id ASC'
        ));

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        $this->load->view(ADMIN_FOLDER . '/email/add_v', $data);
    }

    function edit($id = 0) {
        $result = $this->email_templates_m->exists(array('id' => $id));
        if (!$result) {
            show_404();
            return;
        }

        $main_header['page_title'] = "Edit Email Template : Glomp";
        
        $data['languages'] = $this->lang_m->get_list(array(
            'where' => "lang_active = 'Y' AND lang_deleted = 'N'",
            'order_by' => 'lang_id ASC'
        ));
        

        $data['email_template'] = $this->email_templates_m->get_record(array(
            'where' => array('id' => $id)
        ));

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        
        $this->load->view(ADMIN_FOLDER . '/email/edit_v', $data, FALSE, FALSE);
    }

    function save() {
        $this->load->library('form_validation');

        $id = $this->input->post('id', TRUE);

        $response = array(
            'success' => FALSE,
            'errors' => array()
        );

        //Set rules
        if (empty($id)) {
            $this->form_validation->set_rules('template_name', 'Template Name', 'required|callback__template_exists');
        }
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('body', 'Content', 'required');
        $this->form_validation->set_rules('language', 'Language', 'required|callback__langage_exists');

        $this->form_validation->run();

        //With error it will display the error message
        //Without error it will display empty
        $response['errors']['template_name'] = form_error('template_name');
        $response['errors']['subject'] = form_error('subject');
        $response['errors']['cke_body'] = form_error('body');
        $response['errors']['language'] = form_error('language');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode($response);
            exit;
        }

        $rec['subject'] = $this->input->post('subject', TRUE);
        $rec['body'] = $this->input->post_vars['body'];//$this->input->post('body', TRUE);
        $rec['lang_id'] = $this->input->post('language', TRUE);

        if (!empty($id)) {
            $rec['date_modified'] = date("Y-m-d H:i:s");
            $rec['modified_by'] = $this->session->userdata('admin_id');

            $this->email_templates_m->_update(array('id' => $id), $rec);
        } else {
            $rec['template_name'] = $this->input->post('template_name', TRUE);
            $rec['date_created'] = date("Y-m-d H:i:s");
            $rec['created_by'] = $this->session->userdata('admin_id');

            $this->email_templates_m->_insert($rec);
        }


        //echo '<pre>';
        //print_r($obj);

        $response['success'] = TRUE;

        echo json_encode($response);
    }
    function test() {

        echo "asd";
        $this->load->library('email_templating');
        
        $vars = array(
            'template_name' => 'new_signup',
            'lang_id' => '1',
            'to' => 'ryan@glomp.it',
        );

        $this->email_templating->config($vars);
        
        if ( ! $this->email_templating->send() ) {
            echo json_encode(FALSE);
            exit;
        }
    }
    function email_test() {
        $id = $this->input->post('id', TRUE);
        $to_email = $this->input->post('to_email', TRUE);
        
        if ( empty($to_email) ) {
            $to_email = $this->session->userdata('admin_email');
        }
        
        $result = $this->email_templates_m->exists(array('id' => $id));
        
        if (!$result) {
            show_404();
            return;
        }

        $email_template = $this->email_templates_m->get_record(array(
            'where' => array('id' => $id)
        ));
        
        $this->load->library('email_templating');
        
        $vars = array(
            'template_name' => $email_template['template_name'],
            'lang_id' => $email_template['lang_id'],
            'to' => $to_email,
        );

        $this->email_templating->config($vars);
        
        if ( ! $this->email_templating->send() ) {
            echo json_encode(FALSE);
            exit;
        }
        
        echo json_encode(TRUE);
        exit;
    }

    function _template_exists() {
        $id = $this->input->post('id');
        $language = $this->input->post('language', TRUE);
        $template_name = $this->input->post('template_name', TRUE);

        //On update
        //else on create
        if (!empty($id)) {
            $rec = $this->email_templates_m->exists('id !="' . $id . '" AND lang_id ="' . $language . '" AND template_name = "' . $template_name . '"');

            if ($rec) {
                $this->form_validation->set_message('_template_exists', 'The %s field is already exists in the record.');
                return FALSE;
            }

            return TRUE;
        } else {
            $rec = $this->email_templates_m->exists('lang_id ="' . $language . '" AND template_name = "' . $template_name . '"');

            if ($rec) {
                $this->form_validation->set_message('_template_exists', 'The %s field is already exists in the record.');
                return FALSE;
            }

            return TRUE;
        }
    }

    function _language_exists() {
        $id = $this->input->post('id');
        $language = $this->input->post('language', TRUE);
        $template_name = $this->input->post('template_name', TRUE);

        //On update
        //else on create
        if (!empty($id)) {
            $rec = $this->email_templates_m->exists('id !="' . $id . '" AND lang_id ="' . $language . '" AND template_name = "' . $template_name . '"');

            if ($rec) {
                $this->form_validation->set_message('_language_exists', 'The %s field is already exists in the record.');
                return FALSE;
            }

            return TRUE;
        } else {
            $rec = $this->email_templates_m->exists('lang ="' . $language . '" AND template_name = "' . $template_name . '"');

            if ($rec) {
                $this->form_validation->set_message('_language_exists', 'The %s field is already exists in the record.');
                return FALSE;
            }

            return TRUE;
        }
    }

}