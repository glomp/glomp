<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  Author: Shree
  Date: July-26-2013
  Desc: Admin
 */

class Admin extends CI_Controller {

    private $pagination_per_page = 20;

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('admin_m');
    }

    function index() {
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Admins";
        $data['page_subtitle'] = "View All";
        $data['res_admin'] = $this->admin_m->selectAdmin(array('admin_type' => 'Admin'));



        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');

        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');
        $this->load->view(ADMIN_FOLDER . '/admins_v', $data);
    }

    function UpdateDisplayOrder() {
        if (isset($_POST['updateDispOrder'])) {
            $count = count($_POST['cat_id']);
            for ($i = 0; $i < $count; $i++) {
                $this->admin_m->UpdateDisplayOrder($_POST['disp_order'][$i], $_POST['cat_id'][$i]);
            }
            $this->session->set_flashdata('msg', "Record updated successfully");
        }
        redirect(admin_url("admin/index"));
        exit();
    }

    function addAdmin() {
        $data['page_action'] = "add";
        $data['page_title'] = "Add New Admin";
        $data['page_subtitle'] = "Add New Admin";
        if (isset($_POST['add'])) {
            $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
            $this->form_validation->set_rules('email_address', 'Email Address', 'required||is_unique[gl_admin.admin_email]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
            if ($this->form_validation->run() == TRUE) {
                $this->admin_m->addNewAdmin();
                $this->session->set_flashdata('error_msg', "Record successfully added.");
                redirect(admin_url("admin/index"));
            }
        }
        $this->load->view(ADMIN_FOLDER . "/admins_v", $data);
    }

    function editAdmin($admin_id = 0) {
        $admin_id = (int) ($admin_id);
        $data['res_admin'] = $this->admin_m->selectAdmin(array('admin_id' => $admin_id))->row();
        $data['page_action'] = "edit";
        $data['page_title'] = "Manage Admin";
        $data['page_subtitle'] = "Edit Admin";
        if (isset($_POST['edit'])) {
            echo $this->emailCheck($this->input->post('email_address'));
            $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
            $this->form_validation->set_rules('email_address', 'Email Address', 'required|callback_emailCheck');

            if (isset($_POST['reset_password'])) {
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
            }

            if ($this->form_validation->run() == TRUE) {
                $this->admin_m->editAdmin($admin_id);
                $this->session->set_flashdata('msg', "Record successfully updated.");
                echo "i m here";
                redirect(admin_url("admin"));
            }
        }
        $this->load->view(ADMIN_FOLDER . "/admins_v", $data);
    }

    function deleteAdmin($admin_id = '') {
        if ($admin_id > 0) {
            $result = $this->admin_m->deleteAdmin($admin_id);
            if ($result == 'TRUE')
                $this->session->set_flashdata('msg', "Record successfully deleted.");
            else
                $this->session->set_flashdata('err_msg', 'Sorry, this cateogires is not allowed to delete.');
        }
        redirect(admin_url('admin'));
    }

    function emailCheck($email = '') {
        $result = $this->admin_m->adminEmailCheck($email, $this->input->post('admin_id'));
        $num = $result->num_rows();
        if ($num > 0) {
            $this->form_validation->set_message('emailCheck', 'This email address is already exist.');
            return false;
        }
        else
            return true;
    }

    function generateAdmin() {
        
//        $users = array(
//            array(
//                'name' => 'admin',
//                'email' => 'admin@glomp.it'
//            ),
//        );

        $user = array();

        if (empty($user)) {
            echo 'Cannot populate database.';
            exit;
        }

        foreach ($users as $val) {
            $admin_salt = $this->admin_m->generate_hash();
            $admin_password = $this->admin_m->hash_key_value($admin_salt, 'admin');
            $content = array(
                'admin_name' => $val['name'],
                'admin_email' => $val['email'],
                'admin_added' => date('Y-m-d H:i:s'),
                'admin_status' => 'Active',
                'admin_salt' => $admin_salt,
                'admin_password' => $admin_password
            );

            $id = $this->admin_m->_insert($content);
            if (!empty($id)) {
                echo 'Admin: ' . $val['name'] . ' is added to the database.';
            }
        }
    }

}

//class end
?>