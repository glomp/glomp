<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Translation extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('admin_login_check');
	}
	function index($lang=null)
	{
		if($lang==='en'){
			$this->lang->load('en_label', 'english');
			$this->session->set_userdata(array('translate_lang'=>'English'));
		}
		else if($lang==='cn'){
			$this->lang->load('cn_label', 'chinese');
			$this->session->set_userdata(array('translate_lang'=>'Chinese'));
		}
		else{
			$this->lang->load('en_label', 'english');
			$this->session->set_userdata(array('translate_lang'=>'English'));
		}
		
		$data['page_title'] = 'Glomp Translate';
		
		$dir = _CONTROLLER_DIR_;
		$dir2 = _VIEW_DIR_;
		$dir3 = _VIEW_INCLUDES_DIR_;
		//for mobile
		$dir4 = _CONTROLLER_DIR__MOBILE_;
		$dir5 = _VIEW_DIR_MOBILE_;
		$dir6 = _VIEW_INCLUDES_DIR_MOBILE_;
/// Open a known directory, and proceed to read its contents
		if (is_dir($dir) && is_dir($dir2) && is_dir($dir3)&& is_dir($dir4)&& is_dir($dir5)&& is_dir($dir6)) {
		if (opendir($dir) && opendir($dir2) && opendir($dir3)&& opendir($dir4)&& opendir($dir5)&& opendir($dir6)) {
				$dh=opendir($dir);
				$dh2=opendir($dir2);
				$dh3=opendir($dir3);
				$dh4 = opendir($dir4);
				$dh5 = opendir($dir5);
				$dh6 = opendir($dir6);
				
				$files = scandir($dir);
				$files2 = scandir($dir2, 1);
				$files3 = scandir($dir3, 1);
				$files4 = scandir($dir4, 1);
				$files5 = scandir($dir5, 1);
				$files6 = scandir($dir6, 1);
				$text=array();
				foreach($files as $file)
				{
					if(is_file($dir.$file)){
						$lines = file($dir.$file);
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							 	$filter_key = strip_tags($matches[3][0]);
								$filter_key = $this->remEntities(htmlentities($filter_key));
								if($filter_key && strlen($filter_key)>1){
									$key=$filter_key;
								}
							  
							  
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
				
				foreach($files2 as $file2)
				{
					if(is_file($dir2.$file2)){
						$lines = file($dir2.$file2);
						
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
							
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							  $filter_key = strip_tags($matches[3][0]);
							  $filter_key = $this->remEntities(htmlentities($filter_key));
							  if($filter_key && strlen($filter_key)>1){
								$key=$filter_key;
							  }
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
				
				foreach($files3 as $file3)
				{
					if(is_file($dir3.$file3)){
						$lines = file($dir3.$file3);
						
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
							
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							  $filter_key = strip_tags($matches[3][0]);
							  $filter_key = $this->remEntities(htmlentities($filter_key));
							  if($filter_key && strlen($filter_key)>1){
								$key=$filter_key;
							  }
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
				#file4
				foreach($files4 as $file4)
				{
					if(is_file($dir4.$file4)){
						$lines = file($dir4.$file4);
						
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
							
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							  $filter_key = strip_tags($matches[3][0]);
							  $filter_key = $this->remEntities(htmlentities($filter_key));
							  if($filter_key && strlen($filter_key)>1){
								$key=$filter_key;
							  }
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
			#file 5
			foreach($files5 as $file5)
				{
					if(is_file($dir5.$file5)){
						$lines = file($dir5.$file5);
						
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
							
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							  $filter_key = strip_tags($matches[3][0]);
							  $filter_key = $this->remEntities(htmlentities($filter_key));
							  if($filter_key && strlen($filter_key)>1){
								$key=$filter_key;
							  }
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
				#file 6
				foreach($files6 as $file6)
				{
					if(is_file($dir6.$file6)){
						$lines = file($dir6.$file6);
						
					}
					if(isset($lines) && count($lines)>0){
						foreach($lines as $line)
						{
							
						  $re1='.*?';	# Non-greedy match on filler
						  $re2='((?:[a-z][a-z0-9_]*))';	# Variable Name 1
						  $re3='.*?';	# Non-greedy match on filler
						  $re4='(line)';	# Word 1
						  $re5='.*?';	# Non-greedy match on filler
						  $re6='(\\\'.*?\\\')';	# Single Quote String 1
						
						  if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6."/is", $line, $matches))
						  {
							  $filter_key = strip_tags($matches[3][0]);
							  $filter_key = $this->remEntities(htmlentities($filter_key));
							  if($filter_key && strlen($filter_key)>1){
								$key=$filter_key;
							  }
						  }
							//preg_match("/".$re1.$re2."/is", $line, $results);
							//print_r($results);
							if(isset($key) && !empty($key)){
								//echo $results[1];
								$key=trim($key, "'");
								if(!in_array($key, $text)){
									$text[]=$key;
								}
							}
		
						}
						
					}
				}
			closedir($dh);
			closedir($dh2);
			closedir($dh3);
			closedir($dh4);
			closedir($dh5);
			closedir($dh6);
			$data['key'] = $text;
		}
	}
		$this->load->view(ADMIN_FOLDER.'/translation_v',$data);
	}
	
	function save(){
		//die('hello');
		$bind='';
		if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['lang'])){
			$bind.='<?php'."\r\n"."/* 
Author: Kishan Mainali
Young Minds
Date: ".strftime('%d %b %Y', strtotime(date('Y-m-d H:i:s')))."
Desc: Auto generated language label
Language: ".$_POST['lang']."
*/ \r\n";
			foreach($_POST as $key => $val){
				if($key!='saveTranslation'){
					$bind.='$lang["'.$key.'"]="'.addslashes($val).'"'."; \r\n";
				}
			}
			$bind=rtrim($bind, "\r\n");
			echo _EN_LANG_FILE_;
			if($_POST['lang']=='English'){
				$fp = fopen(_EN_LANG_FILE_,"w"); 
			}
			else if($_POST['lang']=='Chinese'){
				$fp = fopen(_CN_LANG_FILE_,"w"); 
			}
			else{
				redirect(ADMIN_FOLDER.'/translation');
			}
			$upload = fwrite($fp, $bind);
			fclose($fp);
			if($upload){
				if($_POST['lang']=='English')
				{
					$this->session->set_flashdata('success_message','Translation has been saved.');
				}
				else if($_POST['lang']=='Chinese')
				{
					$this->session->set_flashdata('success_message','已保存翻译');
				}
				$redirect=isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:''.ADMIN_FOLDER.'/translation';
				redirect($redirect);
			}
			
		}
		else{
				redirect('index.php/'.ADMIN_FOLDER.'/translation');
			}
	}
	
	function remEntities($str) {
	  if(substr_count($str, '&') && substr_count($str, ';')) {
		// Find amper
		$amp_pos = strpos($str, '&');
		//Find the ;
		$semi_pos = strpos($str, ';');
		// Only if the ; is after the &
		if($semi_pos > $amp_pos) {
		  //is a HTML entity, try to remove
		  return false;
		}
	  }
	  return $str;
	}
}