<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 */

class Templates extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->library('templating');
        $this->load->model('section_m');
        $this->load->model('widget_m');
    }

    function index($template_id = 1) {
        $vars = array(
            'actions' => array(
                'section_type_save' => site_url('adminControl/templates/save_section_type'),
                'section_delete_save' => site_url('adminControl/templates/delete_section'),
                'section_sort_save' => site_url('adminControl/templates/sort_section'),
                'widget_add_save' => site_url('adminControl/templates/widget_add_save'),
                'widget_load_options' => site_url('adminControl/templates/widget_load_options'),
                'widget_delete_save' => site_url('adminControl/templates/widget_delete_save'),
            ),
        );
        
        $this->templating->config($vars);
        $data['template'] = $this->templating->load_template($template_id);
    
        //Main body
        $this->load->view(ADMIN_FOLDER . '/templates_index_v', $data, FALSE, FALSE, FALSE);
    }
    
    function save_section_type() {
        $type = $this->input->post('type');
        $order = $this->input->post('order');
        $template_id = $this->input->post('template_id');
        
        $section_id = $this->section_m->_insert(array(
            'type' => $type,
            'sort_order' => $order,
            'template_id' => $template_id
        ));
        
        $data['success'] = TRUE;
        $data['section_id'] = $section_id;
        
        echo json_encode($data);
    }
    function sort_section() {
        $section_ids = $this->input->post('section_ids');
        $count = 0;
        foreach(explode('|', $section_ids) as $id) {
            $count++;
            $this->section_m->_update(array(
                'id' => $id
            ),array(
                'sort_order' => $count,
            ));
        }
        $data['success'] = TRUE;
        
        echo json_encode($data);
    }
    function delete_section() {
        $section_id = $this->input->post('section_id');
        
        //Check if there is save widget
        $widgets = $this->widget_m->get_list(array(
            'where' => array('section_id' => $section_id),
            'resource_id' => TRUE
        ));
        
        //Delete widget save options
        if ($widgets->num_rows() > 0) {
            foreach($widgets->result() as $w) {
                $this->db->delete('gl_widget_image', array('widget_id' => $w->id)); 
                $this->db->delete('gl_widget_text', array('widget_id' => $w->id)); 
                $this->db->delete('gl_widget_title', array('widget_id' => $w->id)); 
                $this->db->delete('gl_widget_photostory', array('widget_id' => $w->id));
            }
        }
        
        //Delete widget main table
        $this->widget_m->_delete(array('section_id' => $section_id));
     
        //Delete section
        $this->section_m->_delete(array('id' => $section_id));
        
        $data['success'] = TRUE;
        
        echo json_encode($data);
    }
    function widget_add_save() {
        $widget_type = $this->input->post('widget_type');
        $section_id = $this->input->post('section_id');
        $position = $this->input->post('position');
        
        $widget_id = $this->widget_m->_insert(array(
           'widget_type' => $widget_type, 
           'section_id' => $section_id, 
           'position' => $position, 
        ));
        
        switch ($widget_type) {
            case 'title': 
                $widget_sub_table = 'gl_widget_title';
                break;
            case 'image':
                $widget_sub_table = 'gl_widget_image';
                break;
            case 'text':
                $widget_sub_table = 'gl_widget_text';
                break;
            case 'photostory':
                $widget_sub_table = 'gl_widget_photostory';
                break;
        }
        
        //Add to certain sub-table
        $this->db->insert($widget_sub_table, array('widget_id' => $widget_id));
        $widget_sub_id = $this->db->insert_id();
        
        $data['success'] = TRUE;
        $data['widget_id'] = $widget_id.'_'.$widget_sub_id;
        $data['widget_type'] = $widget_type;
        
        echo json_encode($data);
    }
    function widget_delete_save() {
        $widget_type = $this->input->post('widget_type');
        $widget_id = $this->input->post('widget_id');
        
        list($widget_id, $widget_sub_id) = explode('_', $widget_id);
        
        $this->widget_m->_delete(array(
           'id' => $widget_id,
        ));
        
        switch ($widget_type) {
            case 'title': 
                $widget_sub_table = 'gl_widget_title';
                break;
            case 'image':
                $widget_sub_table = 'gl_widget_image';
                break;
            case 'text':
                $widget_sub_table = 'gl_widget_text';
                break;
            case 'photostory':
                $widget_sub_table = 'gl_widget_photostory';
                break;
        }
        
        //Delete certain sub-table
        $this->db->where(array('widget_id' => $widget_sub_id))->delete($widget_sub_table);
        $data['success'] = TRUE;
        
        echo json_encode($data);
    }
    
    function widget_load_options() {
        $widget_type = $this->input->post('widget_type');
        $widget_id = $this->input->post('widget_id');
        $data = array();
        list($widget_id, $widget_sub_id) = explode('_', $widget_id);
        
        switch ($widget_type) {
            case 'title': 
                $template = 'template/widget_type_options/_option_title_v';
                $data = array(
                  'widget_option_id' => $widget_sub_id,
                  'main_text' => '',
                  'sub_text' => ''
                );
                
                $widget_sub_table = 'gl_widget_title';
                $q = $this->db->get_where($widget_sub_table, array('id' => $widget_sub_id));
                if ($q->num_rows() > 0) {
                    $data['main_text'] = $q->row()->main_text;
                    $data['sub_text'] = $q->row()->sub_text;
                }
                
                break;
            case 'image':
                $template = 'template/widget_type_options/_option_image_v';
                $data = array(
                  'widget_option_id' => $widget_sub_id,
                  'source' => '',
                );
                
                $widget_sub_table = 'gl_widget_image';
                $q = $this->db->get_where($widget_sub_table, array('id' => $widget_sub_id));
                if ($q->num_rows() > 0) {
                    $data['source'] = $q->row()->source;
                }
                break;
            case 'text':
                $template = 'template/widget_type_options/_option_text_v';
                $data = array(
                  'widget_option_id' => $widget_sub_id,
                  'main_text' => '',
                );
                
                $widget_sub_table = 'gl_widget_text';
                $q = $this->db->get_where($widget_sub_table, array('id' => $widget_sub_id));
                if ($q->num_rows() > 0) {
                    $data['main_text'] = $q->row()->main_text;
                }
                break;
            case 'photostory':
                $template = 'template/widget_type_options/_option_photostory_v';
                $data = array(
                  'widget_option_id' => $widget_sub_id,
                  'title' => '',
                  'subtitle' => '',
                  'image' => '',
                  'context' => '',
                  'bg_color' => '',
                  'brand_product_button_id' => ''
                );
                
                $widget_sub_table = 'gl_widget_photostory';
                $q = $this->db->get_where($widget_sub_table, array('id' => $widget_sub_id));
                if ($q->num_rows() > 0) {
                    $data['title'] = $q->row()->title;
                    $data['subtitle'] = $q->row()->subtitle;
                    $data['image'] = $q->row()->image;
                    $data['context'] = $q->row()->context;
                    $data['bg_color'] = $q->row()->bg_color;
                    $data['type'] = $q->row()->type;
					$data['brand_product_button_id'] = $q->row()->brand_product_button_id;
                }
                break;
        }
        
        $data['widget_edit_save'] = site_url('adminControl/templates/widget_edit_save');
        echo $this->load->view($template, $data, TRUE, FALSE, FALSE);
    }
    
    function widget_edit_save() {
		$widget = new stdClass();
        $widget_id = $this->input->post('widget_option_id');
        $widget_type = $this->input->post('widget_type');
        
        $data = array();
        $r_data = array();
        
        switch ($widget_type) {
            case 'title':
                $r_data = array(
                  'main_text' => $this->input->post('main_text'),
                  'sub_text' => $this->input->post('sub_text')
                );
                $widget_sub_table = 'gl_widget_title';
                break;
            case 'image':
                $image = $this->input->post('source');
                /*
                 * @todo delete previous image file from storage
                 */
                $upload_dir = dirname(BASEPATH).'/custom/uploads/images/';
                $this->load->library('upload', array(
                    'upload_path'   => $upload_dir,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '2000',
                    'max_width'     => '2000',
                    'max_height'    => '2000'
                ));
                if( $this->upload->do_upload('upload') ) {
                    $data['upload_data']['upload'] = $this->upload->data();
                    $image = '/custom/uploads/images/'.$data['upload_data']['upload']['file_name'];
                }
                $r_data = array(
                  'source' => $image,
                );
                $widget_sub_table = 'gl_widget_image';
                break;
            case 'text':
                $r_data = array(
                  'main_text' => $this->input->post('main_text'),
                );
                $widget_sub_table = 'gl_widget_text';
                break;
            case 'photostory':
                $image = $this->input->post('image');
                /*
                 * @todo delete previous image file from storage
                 */
                $upload_dir = dirname(BASEPATH).'/custom/uploads/images/';
                $this->load->library('upload', array(
                    'upload_path'   => $upload_dir,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '2000',
                    'max_width'     => '2000',
                    'max_height'    => '2000'
                ));
                if( $this->upload->do_upload('upload') ) {
                    $data['upload_data']['upload'] = $this->upload->data();
                    $image = '/custom/uploads/images/'.$data['upload_data']['upload']['file_name'];
                }
                $r_data = array(
                  'title' => $this->input->post('title'),
                  'subtitle' => $this->input->post('subtitle'),
                  'image' => $image,
                  'context' => $this->input->post('context'),
                  'bg_color' => $this->input->post('bg_color'),
                  'type' => $this->input->post('type'),
				  'brand_product_button_id' => $this->input->post('brand_product_button_id')
                );
                $widget_sub_table = 'gl_widget_photostory';
                break;
        }
        
        $this->db->where('id', $widget_id);
        $this->db->update($widget_sub_table, $r_data);
        
        $data['success'] = TRUE;
        $data['message'] = 'Saved!';
        $data['widget']['data'] = $this->db->get_where('gl_widget_'.$widget_type,array('id'=>$widget_id))->row();
        $data['widget']['info'] = $this->db->get_where('gl_widget', array('id'=>$data['widget']['data']->widget_id))->row();
        $widget->data = $data['widget']['data'];
        $data['widget']['template'] = $this->load->view('includes/widget_type_'.$widget_type.'.php',array(
            'widget' => $widget
        ),TRUE,FALSE,FALSE);
        echo json_encode($data);
    }
    
    function _source_loader($src = array()) {
        $sources = '';
        
        if (isset($src['css'])) {
            foreach ($src['css'] as $css) {
                $sources .= '<link rel="stylesheet" type="text/css" href="'.$css.'">';
            }
        } 
        if (isset($src['js'])) {
            foreach ($src['js'] as $js) {
                $sources .= '<script src="'.$js.'" type="text/javascript"></script>';
            }
        } 
        
        return $sources;
    }

}
