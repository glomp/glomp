<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
		
	
	public function __construct ()
	{
        parent::__Construct();
		$this->load->model('admin_m');
	}
	
	function index($msg="")
	{
        if($this->session->userdata('admin_id') >0){                    
			redirect('adminControl/dashboard');
			exit();
		}
		
		$data['page_title']="Login : Glomp";
		if($msg==1)
			$data['msg'] = "Session has been expired. Please login again";
		else
			$data['msg'] = '';
		if($this->session->userdata('login_attempt')=="")
				$this->session->set_userdata('login_attempt',0);
		$data['login_attempt'] = $this->session->userdata('login_attempt');
		
			
		$this->load->view(ADMIN_FOLDER."/login_v",$data);		
	}
	function captcha() /// it genereate captcha
		{
			$this->load->library('SimpleCaptcha'); 
			$captcha = new SimpleCaptcha();
			$captcha->CreateImage();
		}
	function validate_credentials()
	{
		
		// field name, error message, validation rules
			$this->session->set_userdata('login_attempt',($this->session->userdata('login_attempt')+1));
			
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			//
			if($this->session->userdata('login_attempt')>5){
			$captcha_session = $this->session->userdata('captcha');
			if(empty($captcha_session) || trim(strtolower($_POST['image_token'])) != $captcha_session) 
			{
			$this->session->set_flashdata('err', "Invalid CAPTCHA Image");
			redirect(base_url(ADMIN_FOLDER.'/login'));
			exit();
			}//captcha condtion checking
			}//captcha count checking
		//
		$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');	
		if($this->session->userdata('login_attempt')>5)
		{
			$this->form_validation->set_rules('image_token', 'CAPTCHA', 'required');
		}
		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('err', "Invalid username or password");
			redirect(base_url('index.php/'.ADMIN_FOLDER.'/login'));
			exit();
		}
		else
		{	
			$login_check = $this->admin_m->loginValidation();
		
			if($login_check == true) // if the user's credentials validated...
			{
				//echo ADMIN_FOLDER; die();
				$this->session->set_userdata('login_attempt',0);
				$this->session->set_userdata('since_logged_in',time());
				redirect(base_url('index.php/'.ADMIN_FOLDER.'/dashboard'));
				exit();
			}
			else // incorrect username or password
			{
				$this->session->set_flashdata('err', "Invalid Email address or Password");
				
			}
			redirect(base_url('index.php/'.ADMIN_FOLDER.'/login'));
		}
		}
	}
	function logout($msg="")
	{
		if($this->session->userdata('user_id') && $this->session->userdata('is_user_logged_in')==true)
        {
            $array_items = array(
                'admin_id' => '',
                'is_logged_in' => ''            
            );
            $this->session->unset_userdata($array_items);
        }
        else
        {        
            $this->session->sess_destroy();
        }
		redirect(base_url('index.php/'.ADMIN_FOLDER.'/login/index/'.$msg));
		exit();
	}
}
?>