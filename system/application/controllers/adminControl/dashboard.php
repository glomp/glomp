<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function dashboard()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
	}
	
	public function index()
	{
		$data['page_title']="Dashboard : Glomp";
		$this->load->view(ADMIN_FOLDER."/dashboard_v",$data);
	}
}

