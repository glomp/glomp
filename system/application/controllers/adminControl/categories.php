<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-25-2013
Desc: Categories
*/

class Categories extends CI_Controller 
{
	private $pagination_per_page = 20;
	
	function __construct()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('categories_m');
		$this->load->library('lib_pagination');
	}
	
	function index()
	{
		$data['page_action']="view";
		$data['page_title']="Manage Categories";
		$data['page_subtitle'] = "View All";
		$start=($this->uri->segment(4))?$this->uri->segment(4):0;
		$per_page=$this->pagination_per_page;
		$pagination_base_url=admin_url("categories/index/");
		$num_rows=$this->categories_m->selectCategories(0,0)->num_rows();
		$data['links']=$this->lib_pagination->DoPagination($pagination_base_url,$num_rows,$per_page,"4");
		$data['res_categories']= $this->categories_m->selectCategories($start,$per_page)->result();
		/*pagination ends*/
		
		if($this->session->flashdata('msg'))
			$data['msg'] = $this->session->flashdata('msg');
			
		if($this->session->flashdata('err_msg'))
			$data['error_msg']=$this->session->flashdata('err_msg');
		$this->load->view(ADMIN_FOLDER.'/categories_v',$data);
	}
	
	function UpdateDisplayOrder()
	{
		if(isset($_POST['updateDispOrder']))
		{
			$count=count($_POST['cat_id']);
			for($i=0;$i<$count;$i++)
			{
				$this->categories_m->UpdateDisplayOrder($_POST['disp_order'][$i],$_POST['cat_id'][$i]);
			}
			$this->session->set_flashdata('msg',"Record updated successfully");
		}
		redirect(admin_url("categories/index"));
		exit();
	}
	
	
	function publish($catID, $catStatus)
	{
		$catID = (int)($catID);
		if($this->categories_m->selectCategoriesByID($catID,DEFAULT_LANG_ID)->num_rows()>0)
		{	
			$catStatus = ($catStatus=='Y')?'Active':'InActive';
			$this->categories_m->publish($catID,$catStatus);
			$this->session->set_flashdata('msg',"Status changed successfully");
		}
		redirect(admin_url("categories"));
	}
	
	function addCategories()
	{
		$data['page_action']="add";
		$data['page_title']="Add Categories";
		$data['page_subtitle'] = "Add Categories";
		if(isset($_POST['add']))
		{
			$this->form_validation->set_rules('cat_name[]','Category Name','required');
			if($this->form_validation->run() == TRUE)
			{
				$this->categories_m->addCategories();
				$this->session->set_flashdata('error_msg',"Record successfully added.");
				redirect(admin_url("categories/index"));
			}
		}
		$this->load->view(ADMIN_FOLDER."/categories_v", $data);
	}
	
	function editCategories($catID=0)
	{
		$catID=(int)($catID);
		$data['res_categories']= $this->categories_m->selectCategoriesByID($catID)->row();
		$data['page_action']="edit";
		$data['page_title']="Manage Categories";
		$data['page_subtitle'] = "Edit Categories";
		if(isset($_POST['edit']))
		{
			$this->form_validation->set_rules('cat_name', 'Category Name', 'required');
			if($this->form_validation->run() == TRUE)
			{
				$this->categories_m->editCategories($catID);
				$this->session->set_flashdata('msg',"Record successfully updated.");
				redirect(admin_url("categories"));
			}
		}
		$this->load->view(ADMIN_FOLDER."/categories_v", $data);
	}
	
	function deleteCategories($catID='')
	{
		if($catID>0)
		{

			$result = $this->categories_m->deleteCategories($catID);
			if($result == 'TRUE')
				$this->session->set_flashdata('msg',"Record successfully deleted.");
			else
				$this->session->set_flashdata('err_msg','Sorry, this cateogires is not allowed to delete.');
		}
		redirect(admin_url('categories'));
	}
}
?>