<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 */

class Email_logs extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('email_logs_m');
    }

    function index() {

        //Data to be pass to the main header view
        $main_header['page_title'] = "Manage Email Logs : Glomp";

        $subQuery = "";
        $queryString = "";

        if (isset($_GET['Search'])) {
            $keywords = isset($_GET['keywords']) ? urlencode($_GET['keywords']) : "";
            if ($keywords != "") {
                $subQuery = 'AND (user_email LIKE "%' . $keywords . '%" OR user_name LIKE "%' . $keywords . '%")';
                $queryString = "&keywords=$keywords";
            }
        }
        
        if (isset($_GET['per_page']) && is_numeric($_GET['per_page'])) {
            $page = intval($_GET['per_page']);
        } else {
            $page = 0;
        }
        
        $offset = $page;
        $row_per_page = 20;
        
        $records = $this->email_logs_m->get_list(array(
            'select' => 'email_log_id AS id, subject, from_email, to_email, response, date_created',
        ));
        
        $res_total_row = count($records);
        
        $records = $this->email_logs_m->get_list(array(
            'select' => 'email_log_id AS id, subject, from_email, to_email, response, date_created',
            'order_by' => 'date_created DESC',
            'offset' => $page,
            'limit' => $row_per_page
        ));

        $totalRow = $res_total_row;
        
        $data['email_logs'] = $records;
        $config['base_url'] = base_url(ADMIN_FOLDER . "/email_logs/?q=q&$queryString");
        $config['total_rows'] = $totalRow;
        $config['per_page'] = $row_per_page;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        //Main body
        $this->load->view(ADMIN_FOLDER . '/email/email_logs/list_v', $data);
    }
    
    function view($id = 0) {
        $record = $this->email_logs_m->get_record(array(
            'where' => 'email_log_id = "'.$id.'"'
        ));
        
        if (count($record) == 0) {
            return show_404();
        }
        
        //Data to be pass to the main header view
        $main_header['page_title'] = "Manage Email Logs : Glomp";
        
        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        //Main body
        $this->load->view(ADMIN_FOLDER . '/email/email_logs/view_v', $data);
        
    }

    function resend() {
        $id = $this->input->get('id');
        $pass=$this->input->get('pass');

        if($pass!='123')
        {
            echo "error";
            exit();
        }
            

         $result = $this->email_logs_m->exists(array('email_log_id' => $id));
        
        if (!$result) {
            show_404();
            return;
        }


        $email_log = $this->email_logs_m->get_record(array(
            'where' => array('email_log_id' => $id)
        ));

        $this->load->library('email_templating');

        $vars = array(            
            'from_name' => $email_log['from_name'],
            'from_email' => $email_log['from_email'],
            'reply_to' => $email_log['reply_to'],
            'to' => $email_log['to_email'],
            'subject' => $email_log['subject'],
            'templated_body' => $email_log['email_body'],
        );
        

        $this->email_templating->config($vars);
        $this->email_templating->set_subject($email_log['subject']);
        $this->email_templating->send(FALSE,TRUE);
       // print_r($email_log);
    }

    function email_test() {
        $id = $this->input->post('id', TRUE);
        $to_email = $this->input->post('to_email', TRUE);
        
        if ( empty($to_email) ) {
            $to_email = $this->session->userdata('admin_email');
        }
        
       
        $result = $this->email_templates_m->exists(array('id' => $id));
        
        if (!$result) {
            show_404();
            return;
        }

        $email_template = $this->email_templates_m->get_record(array(
            'where' => array('id' => $id)
        ));
        
        $this->load->library('email_templating');
        
        $vars = array(
            'template_name' => $email_template['template_name'],
            'lang_id' => $email_template['lang_id'],
            'to' => $to_email,
        );

        $this->email_templating->config($vars);
        
        if ( ! $this->email_templating->send() ) {
            echo json_encode(FALSE);
            exit;
        }
        
        echo json_encode(TRUE);
        exit;
    }
}