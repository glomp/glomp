<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Desc: Admin
*/

class Setting extends CI_Controller 
{
	
	function __construct()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('admin_m');
	}
	private function selectAll()
	{
		$sql = "SELECT * FROM gl_config WHERE config_status = 'Active' order by config_display_order";
		$res = $this->db->query($sql);
		return $res;
	
	}
	function index()
	{
		$data['page_action']="view";
		$data['page_title']="Website Setting";
		$data['page_subtitle'] = "";
		if(isset($_POST['Update']))
		{
            $sel = $this->selectAll();


            foreach($sel->result() as $rule)
            {
                $this->form_validation->set_rules($rule->config_key,$rule->config_name,$rule->config_value_rule);
            }

            if($this->form_validation->run() == TRUE)
            {

                    foreach($sel->result() as $row)
                    {
                        $value = $this->input->post($row->config_key);
                        $this->update($row->config_key,$value);
                    }

                $this->update_point_table();
                $data['msg'] = 'Records updated successfully';
            }
		
		}
        
        $data['age_limits'] = $this->get_age_limits();
        $data['categories'] = $this->get_product_categories();
        
		$data['res_all'] = $this->selectAll();
		$this->load->view(ADMIN_FOLDER.'/setting_v',$data);
	}
	
	private function update($key,$value)
	{
		
		$sql = "UPDATE gl_config set
				config_value = '$value'
				WHERE
				config_key = '$key'
				";
		$this->db->query($sql);
		
	}
	function update_point_table()
	{
		$rate = $this->custom_func->config_value('GLOMP_POINT_PER_AMOUNT');
		$sql = "UPDATE gl_package_point SET
				package_price = $rate*package_point
				";
		$this->db->query($sql);
	}
    
    private function get_age_limits() {
        
        $this->db->select('id, gl_underage.region_id, age_limit, gl_region.region_name');
        $this->db->join('gl_region','gl_underage.region_id = gl_region.region_id');
        $this->db->order_by('gl_region.region_name');
        $q = $this->db->get('gl_underage');
        if( $q->num_rows() > 0 ) {
            return $q->result();
        }
        return false;
    }
    
    /**
     * Adds new age limit for a selected region
     * AJAX only
     */
    public function add_age_limit() {
        $data = array();
        $this->form_validation->set_rules('region_id', 'Region ID', 'trim|required|numeric');
        $this->form_validation->set_rules('age_limit', 'Age Limit', 'trim|required|numeric');
        
        if ($this->form_validation->run() == TRUE) {
            
            $this->load->model('underage_m');
            $id = $this->underage_m->save(array(
                'region_id' => $this->input->post('region_id'),
                'age_limit' => (int) $this->input->post('age_limit')
            ));
            
            $this->db->select('id, gl_underage.region_id, age_limit, gl_region.region_name');
            $this->db->join('gl_region','gl_underage.region_id = gl_region.region_id');
            $q = $this->db->get_where('gl_underage',array('id'=>$id));
            if( $q->num_rows() > 0 ) {
                $data['record'] = $q->row_array();
            }
            $data['status'] = 'success';
            $data['message'] = 'Record Saved!';
            
            
        } else {
            $data['status'] = 'failed';
            $data['message'] = validation_errors();
        }
        
        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            exit;
        }
        
    }
    
    public function add_category_to_limit() {
        $data = array();
        
        $cats = ( $this->input->post('category_id') ) ? implode(',',$this->input->post('category_id')) : '';
        
        if( $this->db->update('gl_underage_sections',array('product_categories' => $cats)) ){
            $data['status'] = 'success';
            $data['message'] = 'Record Saved!';
        } else {
            $data['status'] = 'failed';
            $data['message'] = 'There was an error updating the record. Refresh this page and try again.';
        }

        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            exit;
        }

        exit;
    }
    
    /**
     * delete underage record
     * AJAX only
     */
    public function delete_age_limit( $id ) {
        $data = array();
        $this->load->model('underage_m');
        if( $this->underage_m->delete( $id ) ) {
            
            $data['status'] = 'success';
            $data['message'] = 'Record Deleted!';
        } else {
            $data['status'] = 'failed';
            $data['message'] = 'There was an error deleting the record. Refresh this page and try again.';
        }
        
        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            exit;
        }
    }
    
    /**
     * return a list of region based on key string
     */
    public function search_region() {
    
        $key = $this->input->post('key');
        
        $this->db->select('region_id, region_parent_id, region_status, region_name');
        
        $this->db->like('region_name', $this->input->post('key'), 'after');
        $this->db->order_by('region_name');
        $q = $this->db->get('gl_region');
        
        if( $q->num_rows() > 0 ) {
            echo json_encode( $q->result_array() );
            exit;
        }
    }
    
    private function get_product_categories(){
        
        $this->db->where(array('gl_categories.cat_status'=>'Active'));
        $q = $this->db->get('gl_categories');
        if( $q->num_rows() > 0 ) {
            
            $result = $q->result();
            $q2 = $this->db->get_where('gl_underage_sections');
            if( $q2->num_rows() > 0 ) {
                $cat_ids = explode(',',$q2->row()->product_categories);
                foreach( $result as $r ) {
                    $r->is_selected = ( in_array( $r->cat_id, $cat_ids ) ) ? 1 : 0;
                }
            }
            
            return $result;
        }
        return false;
    }
    
}//class end