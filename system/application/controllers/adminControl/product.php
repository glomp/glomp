<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  Author: Shree
  Date: July-28-2012
  Desc: product
 */

class Product extends CI_Controller {

    private $pagination_per_page = 20;

    function Product() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('merchant_m');
        $this->load->model('regions_m');
        $this->load->model('categories_m');
        $this->load->model('product_m');
        $this->load->model('product_inventory_m');
        $this->load->library('lib_pagination');
    }

    function index($filter_by = '', $filter_key = '') {
        //echo $this->session->flashdata('msg'); die();
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Product : Glomp";
        $data['view_as']="";


        /* /*pagination starts
          if($this->uri->segment(5))
          $start=$this->uri->segment(5);
          else
          $start=0;
          $per_page=$this->pagination_per_page;
          $pagination_base_url=base_url(ADMIN_FOLDER."/product/index/");
          $num_rows= $this->product_m->selectProducts(0,0)->num_rows();
          $data['links']=$this->lib_pagination->DoPagination($pagination_base_url,$num_rows,$per_page,"5");
          $data['res_product']= $this->product_m->selectProducts($start,$per_page)->result();
          /*pagination ends */

        $subQuery = "";
        $queryString = "";

        $merchant_id = isset($_GET['merchant_id']) ? urlencode($_GET['merchant_id']) : 0;
        if ($merchant_id > 0) {
            $subQuery.="AND prod_merchant_id ='$merchant_id'";
            $queryString.="&merchant_id=$merchant_id";
        }
        $location_id = isset($_GET['location_id']) ? urlencode($_GET['location_id']) : 0;
        if ($location_id > 0) {
            $subQuery.=" AND region_parent_path_id LIKE'/%" . $location_id . "%/'";
            $queryString.="&location_id=$location_id";
        }
        $category_id= isset($_GET['category_id']) ? urlencode($_GET['category_id']) : 0;
        if ($category_id > 0) {
            $subQuery.=" AND prod_cat_id ='" . $category_id . "'";
            $queryString.="&category_id=$category_id";
        }
        
        $data['search_q']="";
        $search_q=$this->input->get('search_q');        
        if( !empty($search_q))
        {
            $data['search_q']=$search_q;            
            $subQuery.=" AND prod_name LIKE '%" . $search_q."%'";
        }
        
        $data['location_id']=$location_id;
        $data['merchant_id']=$merchant_id;
        $data['category_id']=$category_id;
            
			$res_total_row  = $this->product_m->selectProductsForAdmin($subQuery," GROUP BY prod_id");
			if(isset($_GET['per_page']) && is_numeric($_GET['per_page']))
			$page = intval($_GET['per_page']);
			else
			$page =0;
			$totalRow = $res_total_row->num_rows();
			$offset = $page;
			$row_per_page = $this->pagination_per_page;
			$data['res_product']= $this->product_m->selectProductsForAdmin($subQuery." GROUP BY prod_id ORDER BY FIELD(prod_status,'Active','Inactive','Pending for Deletion','Deleted'), prod_rank, prod_name, prod_id DESC  LIMIT " .$offset.",".$row_per_page)->result();
			$config['base_url'] = base_url(ADMIN_FOLDER."/product/?q=q&$queryString");		
			$config['total_rows'] = $totalRow;
			$config['per_page'] = $row_per_page;
			$config['page_query_string'] = TRUE;
			$this->pagination->initialize($config);
			
			if($this->session->flashdata('msg'))
		$data['msg'] = $this->session->flashdata('msg');
		if($this->session->flashdata('err_msg'))
		$data['error_msg']=$this->session->flashdata('err_msg');		
		$this->load->view(ADMIN_FOLDER.'/product_v',$data);
	}
    function grid()
    {
        $data['page_action'] = "grid";
        $data['page_title'] = "Manage Product : Glomp";
        $data['view_as']="grid";
        $subQuery = "";
        $queryString = "";

        $merchant_id = isset($_GET['merchant_id']) ? urlencode($_GET['merchant_id']) : 0;
        if ($merchant_id > 0) {
            $subQuery.="AND prod_merchant_id ='$merchant_id'";
            $queryString.="&merchant_id=$merchant_id";
        }
        $location_id = isset($_GET['location_id']) ? urlencode($_GET['location_id']) : 0;
        if ($location_id > 0) {
            $subQuery.=" AND region_parent_path_id LIKE'%/" . $location_id . "/%'";
            $queryString.="&location_id=$location_id";
        }
        $category_id= isset($_GET['category_id']) ? urlencode($_GET['category_id']) : 0;
        $order_by="prod_rank";
        if ($category_id > 0) {
            $subQuery.=" AND prod_cat_id ='" . $category_id . "'";
            $queryString.="&category_id=$category_id";
            $order_by="prod_rank_cat";
        }
        
        $data['search_q']="";
        $search_q=$this->input->get('search_q');        
        if( !empty($search_q))
        {
            $data['search_q']=$search_q;            
            $subQuery.=" AND prod_name LIKE '%" . $search_q."%'";
        }
        
        
        $data['location_id']=$location_id;
        $data['merchant_id']=$merchant_id;
        $data['category_id']=$category_id;
        
            
			//$res_total_row  = $this->product_m->selectProductsForAdmin($subQuery," GROUP BY prod_id");
			if(isset($_GET['per_page']) && is_numeric($_GET['per_page']))
			$page = intval($_GET['per_page']);
			else
			$page =0;
			//$totalRow = $res_total_row->num_rows();
			$offset = $page;
			$row_per_page = $this->pagination_per_page;
			$data['res_product']= $this->product_m->selectProductsForAdmin($subQuery." GROUP BY prod_id ORDER BY ".$order_by." ASC LIMIT " .$offset.",".$row_per_page)->result();
			$config['base_url'] = base_url(ADMIN_FOLDER."/product/?q=q&$queryString");		
			//$config['total_rows'] = $totalRow;
			$config['per_page'] = $row_per_page;
			$config['page_query_string'] = TRUE;
			$this->pagination->initialize($config);
			
			if($this->session->flashdata('msg'))
		$data['msg'] = $this->session->flashdata('msg');
		if($this->session->flashdata('err_msg'))
		$data['error_msg']=$this->session->flashdata('err_msg');		
		$this->load->view(ADMIN_FOLDER.'/product_v',$data);
    }
    function updateRanks()
    {
        if($_POST)
        {
            $sortedIDs= $_POST['sortedIDs'];
            $location_id= $_POST['location_id'];            
            $merchant_id= $_POST['merchant_id'];            
            $category_id= $_POST['category_id'];            
            $this->product_m->updateRanking($sortedIDs,$location_id,$merchant_id,$category_id);            
        }    
    }
	function deleteRecord($contentID=0,$delete_now="")
	{
        $search_q=$this->input->get('search_q');
        $location_id=$this->input->get('location_id');
        $merchant_id=$this->input->get('merchant_id');
        $category_id=$this->input->get('category_id');
        
        
        $data['search_q']= $this->input->get('search_q');
        $data['view_as']="";
        $data['location_id']='0';
        $data['merchant_id']='0';
        $data['category_id']='0';
        
        if($delete_now=="delete_now")
        {
            $this->product_m->delete($contentID);            
            $this->session->set_flashdata('msg', "Record deleted successfully");
            redirect(ADMIN_FOLDER . "/product/index/?location_id=".$location_id.'&category_id='.$category_id.'&search_q='.$search_q.'&merchant_id='.$merchant_id);
        }
        
        if($_POST && isset($_POST['delete_type']))
        {
            $delete_type=$this->input->post('delete_type');
            $num_days=$this->input->post('num_days');
            if($delete_type=='delete_after_num_days')
            {            
                $this->form_validation->set_rules('num_days', 'Number of Days', 'integer|is_natural_no_zero');                
                if ($this->form_validation->run() == TRUE)
                {                 
                    $res = $this->product_m->setThisProductToPendingForDeletion($contentID,$delete_type,$num_days);                    
                    redirect(ADMIN_FOLDER."/product/index/");
                }
            }
            else
            {            
                if($num_days=="")
                    $num_days=0;
                $res = $this->product_m->setThisProductToPendingForDeletion($contentID,$delete_type,$num_days);                                
                redirect(ADMIN_FOLDER."/product/index/");
            }
            
        }
        
        $contentID = (int)($contentID);
        $data['contentID'] = $contentID;
        $data['page_action']="delete";
		$data['page_title']="Manage Product : Glomp";
        
        $res = $this->product_m->checkIfThereIsExistingVoucherForThisProduct($contentID);
        $data['conflict_voucher']=$res->num_rows();
        
        $res = $this->product_m->checkIfThereIsExistingCampaignByRuleForThisProduct($contentID);
        $data['conflict_campaign_by_rule']=$res->num_rows();
        
        
        $res = $this->product_m->checkIfThereIsExistingCampaignByPromoCodeForThisProduct($contentID);
        $data['conflict_campaign_by_promo_code']=$res->num_rows();        
        
          ($this->product_m->selectProductByID($contentID)->row());
        $data['rec_product']= $this->product_m->selectProductByID($contentID)->row();        
        $this->load->view(ADMIN_FOLDER.'/product_v',$data);
        
        
        
        
    }

    function addProduct() {
        $data['page_action'] = "add";
        $data['page_title'] = "Manage Product : glomp";
        $data['view_as']="";
        $data['location_id']='0';
        $data['merchant_id']='0';
        $data['category_id']='0';
        $data['search_q']="";
        if (isset($_POST['addPage'])) {
            $this->form_validation->set_rules('prod_name', 'Product Name', 'required');
            $this->form_validation->set_rules('prod_point', 'Product Point', 'required|numeric');
            $this->form_validation->set_rules('prod_reverse_point', 'Product Reverse Point', 'required|numeric');
            $this->form_validation->set_rules('prod_merchant_cost', 'Product Merchant Cost', 'required|numeric');
            $this->form_validation->set_rules('prod_rank', 'Merchant Order Rank', 'numeric');
            $this->form_validation->set_rules('prod_rank_cat', 'Category Order Rank', 'numeric');
            $this->form_validation->set_rules('prod_cat_id', 'Product Category', 'required');
            $this->form_validation->set_rules('prod_merchant_id', 'Merchant Name', 'required');
            $this->form_validation->set_rules('prod_voucher_expiry_day', 'Voucher Expiry Day', 'required|numeric');
            $this->form_validation->set_rules('prod_details', 'Product Details', '');
            $this->form_validation->set_rules('prod_terms_condition', 'Terms & Condition', '');
            $this->form_validation->set_rules('prod_warning', 'Warning Message', '');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view(ADMIN_FOLDER . "/product_v", $data);
            } else {
                $insert_id = $this->product_m->addRecord();
                //upload logo

                if ($_FILES['prod_image']['tmp_name'] != "") {

                    $config = array(
                        'allowed_types' => 'jpeg|jpg|gif|png',
                        'upload_path' => 'custom/uploads/products/',
                        'maximum_filesize' => 2
                    );

                    $logo_name = $this->custom_func->custom_upload($config, $_FILES['prod_image']);
                    $uploadError = preg_match("/^Error/", $logo_name) ? $logo_name : NULL;
                    if (empty($uploadError)) {

                        $this->product_m->updateLogo($logo_name, $insert_id);
                        $this->resizeImg("custom/uploads/products/" . $logo_name, "custom/uploads/products/thumb/" . $logo_name, 160, 160);
                    }
                }
                $this->session->set_flashdata('msg', "Record successfully added.");
                redirect(ADMIN_FOLDER . "/product/index/?location_id=".$this->merchant_m->getLocationIDOfThisMerchant($this->input->post('prod_merchant_id')));
                exit();
            }
        } else {
            $this->load->view(ADMIN_FOLDER . "/product_v", $data);
        }
    }
    
    function addStock($id) {
        $data['page_title'] = 'Manage Product Stocks';
        $data['res_product'] = $this->product_inventory_m->get_list(array(
            'where' => array('prod_id' => $id),
            'order_by' => 'prod_inv_id DESC'
        ));
        
        $data['product'] = $this->product_m->selectProductByID($id)->row();
        $data['prod_id'] = $id;
        
        if (isset($_POST['update_stock_btn'])) {
            $current_qty = $_POST['current_qty'];
            $details = $_POST['details'];
            
            if ($_POST['current_qty'] == '') {
               $current_qty = 0;
            }
            
            if ($_POST['details'] == '') {
                $details = 'Set new qty. of '. $current_qty;
            }
            
            $ins_data = array(
                'prod_id'=> $id,
                'change_qty'=> 0,
                'current_qty'=> $current_qty,
                'details'=> $details,
                'timestamp' => date('Y-m-d H:i:s'),
            );
            
            $this->product_inventory_m->_insert($ins_data);
            
            return redirect(ADMIN_FOLDER. '/product/addStock/'.$id);
        }
        
        $this->load->view(ADMIN_FOLDER . "/product_stock_v", $data);
    }

    function editRecord($contentID = 0) {
        $contentID = (int) ($contentID);        
        if($this->product_m->selectProductByID($contentID)->num_rows()>0 )
        {
            $data['contentID'] = $contentID;
            $data['rec_product'] = $this->product_m->selectProductByID($contentID)->row();
            $data['page_action'] = "edit";
            $data['page_title'] = "Manage Product : Glomp";
            $data['view_as']="";
            $data['search_q']="";
            $data['location_id']='0';
            $data['merchant_id']='0';
            $data['category_id']='0';



            if (isset($_POST['editPage'])) {
                $this->form_validation->set_rules('prod_name', 'Product Name', 'required');
                $this->form_validation->set_rules('prod_point', 'Product Point', 'required|numeric');
                $this->form_validation->set_rules('prod_reverse_point', 'Product Reverse Point', 'required|numeric');
                $this->form_validation->set_rules('prod_cat_id', 'Product Category', 'required');
                $this->form_validation->set_rules('prod_merchant_id', 'Merchant Name', 'required');
                $this->form_validation->set_rules('prod_rank', 'Merchant Order Rank', 'numeric');
                $this->form_validation->set_rules('prod_rank_cat', 'Category Order Rank', 'numeric');
                $this->form_validation->set_rules('prod_voucher_expiry_day', 'Voucher Expiry Day', 'required|numeric');

                if ($this->form_validation->run() == FALSE) {
                    
                } else {
                    $this->product_m->editRecord($contentID);
                    //upload logo

                    if ($_FILES['prod_image']['tmp_name'] != "") {
                        $config = array(
                            'allowed_types' => 'jpeg|jpg|gif|png',
                            'upload_path' => 'custom/uploads/products/',
                            'maximum_filesize' => 2
                        );

                        $logo_name = $this->custom_func->custom_upload($config, $_FILES['prod_image']);
                        $uploadError = preg_match("/^Error/", $logo_name) ? $logo_name : NULL;
                        if (empty($uploadError)) {

                            $old_image = $this->input->post('old_image');
                            $this->product_m->updateLogo($logo_name, $contentID, $old_image);
                            $this->resizeImg("custom/uploads/products/" . $logo_name, "custom/uploads/products/thumb/" . $logo_name, 160, 160);
                        } else {
                            echo $uploadError;
                            exit();
                        }
                    }//prod image



                    $this->session->set_flashdata('msg', "Record successfully updated.");
                    redirect(ADMIN_FOLDER . "/product/index/?location_id=".$this->merchant_m->getLocationIDOfThisMerchant($this->input->post('prod_merchant_id')));
                    exit();
                }
            }
            $this->load->view(ADMIN_FOLDER . "/product_v", $data);
        }
        else
        {
            show_404();
        }
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'exact', $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    function checkIfFloat($num) {
        if (!is_float($num)) {
            $this->form_validation->set_message('checkIfFloat', $num . 'Product Merchant Cost should be in float value.');
            return false;
        } else {
            return true;
        }
    }

}

?>