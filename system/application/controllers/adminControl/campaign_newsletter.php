<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Campaign_newsletter extends CI_Controller {

    private $pagination_per_page = 20;

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('campaign_newsletter_m');
        $this->load->library('lib_pagination');
        $this->load->model('regions_m');
        $this->load->model('users_m');
        $this->load->model('newsletter_m');
        $this->load->model('campaign_newsletter_rule_m');
        $this->load->model('campaign_m');
        $this->load->model('brand_m');
    }
	
    function index() {
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Campaign Newsletter";
        $data['page_subtitle'] = "View All";
        $start = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $per_page = $this->pagination_per_page;
        $pagination_base_url = admin_url("campaign_newsletter/index/");
        //$num_rows = $this->campaign_newsletter_m->selectCampaign('by_rule')->num_rows();
        $num_rows = count($this->campaign_newsletter_m->get_list());
        
        $data['links'] = $this->lib_pagination->DoPagination($pagination_base_url, $num_rows, $per_page, "4");
        $data['campaign_newsletters'] = $this->campaign_newsletter_m->get_list(array(
            'order_by' => 'campaign_newsletter_id DESC'
        ));

        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');

        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');
        $this->load->view(ADMIN_FOLDER . '/campaign_newsletter/list_v', $data);
    }
    
    function add() {
        $data['page_title'] = "Create New Campaign Newsletter";
        $data['page_subtitle'] = "Create New Campaign";
        
        $data['newsletter_templates'] = $this->newsletter_m->get_list();
        
        if (isset($_POST['create_campaign_newsletter'])) {
            $this->_submit('add', 0);
        }
        
        $this->load->view(ADMIN_FOLDER . '/campaign_newsletter/add_v', $data);
    }
    function edit($newsletter_camp_id)
    {
        $data['page_title'] = "Edit Campaign Newsletter";
        
        $data['newsletter_templates'] = $this->newsletter_m->get_list();
        $data['campaign_newsletter'] = $this->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $newsletter_camp_id)
        ));
        
        $data['newsletter_template'] = $data['campaign_newsletter']['newsletter_template'];
        $data['newsletter_name'] = $data['campaign_newsletter']['newsletter_name'];
        $data['campaign_newsletter_id'] = $newsletter_camp_id;
        
        if (isset($_POST['update_campaign_newsletter'])) {
            $this->_submit('update', $newsletter_camp_id);
        }        
        
        
        $this->db->select('bp.id, bp.name');
        $this->db->from('gl_brandproduct bp');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $white_label_merchants = $this->db->get();

        $this->db->select('p.id,p.product_id, bp.name, gl_p.prod_name');
        $this->db->from('gl_brands_products p');
        $this->db->join('gl_product gl_p', 'gl_p.prod_id = p.product_id', 'left');
        $this->db->join('gl_brandproduct bp', 'p.brandproduct_id = bp.id', 'left');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $this->db->order_by("bp.name ASC, gl_p.prod_name ASC");
        $query = $this->db->get(); 
        $white_label_products = $query;
        
        
        //print_r($data['res_camp_rule']->row());
        $temp = $this->campaign_newsletter_rule_m->select_campaign_rule($newsletter_camp_id);
        $rule_details = isset($temp->row()->rule_details)? json_decode($temp->row()->rule_details) : '';
        
        //print_r($rule_details);
        $map = array(
                'all_list' => $this->campaign_m->white_label_list,
                'selected_list' => isset($rule_details->selected_list) ? $rule_details->selected_list:'',
                
                'all_actions' => $this->campaign_m->white_label_actions,
                'selected_actions' => isset($rule_details->selected_actions) ? $rule_details->selected_actions:'',
                
                'all_merchants' => $white_label_merchants,
                'selected_merchants' => isset($rule_details->selected_merchants) ? $rule_details->selected_merchants:'',
                
                'all_products' => $white_label_products,
                'selected_products'=> isset($rule_details->selected_products) ? $rule_details->selected_products:'',
                
                'date_from' => isset($rule_details->date_from) ? $rule_details->date_from:'',
                'date_to' =>isset($rule_details->date_to) ? $rule_details->date_to:''
        );
        $white_label = new stdClass();
        foreach($map as $k => $v)
            $white_label->$k = $v;
        $data['white_label'] = $white_label;
        
        $data['whitelabel_id_checked'] = ( isset($rule_details->selected_list) && $rule_details->selected_list !='') ? 'checked' : '';
        $data['whitelabel_actions_checked'] = ( isset($rule_details->selected_actions) && $rule_details->selected_actions !='') ? 'checked' : '';
        $data['whitelabel_merchants_checked'] = ( isset($rule_details->selected_merchants) && $rule_details->selected_merchants !='') ? 'checked' : '';
        $data['whitelabel_products_checked'] = ( isset($rule_details->selected_products) && $rule_details->selected_products !='') ? 'checked' : '';
        $data['date_from_checked'] = ( isset($rule_details->date_from) && $rule_details->date_from !='' && $rule_details->date_from !='0') ? 'checked' : '';
        
        $this->load->view(ADMIN_FOLDER . '/campaign_newsletter/edit_v', $data);
    }
    
    function _submit($action = 'add', $newsletter_camp_id) {
        $this->form_validation->set_rules('campaign_newsletter_name', 'Campaign Newsletter Name', 'required');
        $this->form_validation->set_rules('campaign_newsletter_template', 'Template', 'required');
        
        if ($action == 'update') {
            if (isset($_POST['min_age_checked'])) {
                $this->form_validation->set_rules('min_age', 'Minimum age', 'required|integer|');
            }
            if (isset($_POST['max_age_checked'])) {
                $this->form_validation->set_rules('max_age', 'Maximum age', 'required|integer|');
            }
            if (isset($_POST['regions_checked'])) {
                $this->form_validation->set_rules('regions_id_selected[]', 'Regions', 'required');
            }
            if (isset($_POST['gender'])) {
                $this->form_validation->set_rules('gender', 'Gender', 'required');
            }
        }
        
        if ($this->form_validation->run() == TRUE) {
            if ($action== 'update') {
                $newsletter_id = $this->campaign_newsletter_m->_update(array('campaign_newsletter_id' => $newsletter_camp_id),
                    array(
                        'newsletter_name' => $this->input->post('campaign_newsletter_name'),
                        'newsletter_template' => $this->input->post('campaign_newsletter_template'),
                    ));
                
                $this->campaign_newsletter_rule_m->update_campaign_rule($newsletter_camp_id);
                
                if ($_POST['update_campaign_newsletter'] == 'Save and Run') {
                    $this->run_campaign($newsletter_camp_id);
                    $this->session->set_flashdata('msg', 'Campaign run succesfully');
                    
                    return redirect(ADMIN_FOLDER. '/campaign_newsletter/');
                }
                
            } else {
                $newsletter_camp_id = $this->campaign_newsletter_m->_insert(array(
                    'newsletter_name' => $this->input->post('campaign_newsletter_name'),
                    'newsletter_template' => $this->input->post('campaign_newsletter_template'),
                    'newsletter_created_date' => $this->custom_func->datetime(),
                    'newsletter_create_by' => $this->session->userdata('admin_id'),
                ));
            }
            
            return redirect(ADMIN_FOLDER. '/campaign_newsletter/edit/'.$newsletter_camp_id);
        }
    }
    
    function preview_template($campaign_newsletter_id = 0) {
        $this->load->helper('html');
        
        $result = $this->campaign_newsletter_m->exists('campaign_newsletter_id = "'. $campaign_newsletter_id. '"');
        
        if (!$result) {
            show_404();
            return;
        }
        
        $camp['newsletter'] = $this->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $campaign_newsletter_id)
        ));

        $template = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $camp['newsletter']['newsletter_template'])
        ));        
        $template_data['campaign_newsletter_id']=$campaign_newsletter_id;
        $template_data['attributes'] = json_decode($camp['newsletter']['newsletter_template_value'], TRUE);
        
        
        //layout
        
        $data['template'] = $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $template['name'], $template_data, TRUE);
        $data['newsletter'] = $camp['newsletter'];
        
        $this->load->view(ADMIN_FOLDER . '/campaign_newsletter/preview_v', $data);
    }
    
    function get_data() {
        $id = $this->input->post('id', TRUE);

        $result = $this->campaign_newsletter_m->exists(array('campaign_newsletter_id' => $id));
        if (!$result) {
            return;
        }

        $attr_name = $this->input->post('attr_name', TRUE);

        $result = $this->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $id)
        ));

        $value = json_decode($result['newsletter_template_value'], TRUE);
        
        $value = (! isset($value[$attr_name])) ? '' : $value[$attr_name];
        
        echo json_encode(array('value' => $value));
    }
    
    function set_data() {
        if (isset($_FILES['upload_image'])) {
            $this->_upload_image();
        }
        
        $id = $this->input->post('id', TRUE);

        $result = $this->campaign_newsletter_m->exists(array('campaign_newsletter_id' => $id));
        if (!$result) {
            return;
        }

        $result = $this->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $id)
        ));

        $attr_name = $this->input->post('attr_name', TRUE);
        $data = json_decode($result['newsletter_template_value'], TRUE);
        $data[$attr_name] = $this->input->post_vars['value'];

        $rec['newsletter_template_value'] = json_encode($data);

        $id = $this->campaign_newsletter_m->_update(array('campaign_newsletter_id' => $id), $rec);

        if ($id) {
            echo json_encode(TRUE);
        }
    }    
    
    function update_data() {
        $id = $this->input->post('camp_newsletter_id', TRUE);

        $result = $this->campaign_newsletter_m->exists(array('campaign_newsletter_id' => $id));
        if (!$result) {
            return;
        }
        
        $rec['newsletter_template'] = $this->input->post('template_id');
        $rec['newsletter_template_value'] = '';

        $id = $this->campaign_newsletter_m->_update(array('campaign_newsletter_id' => $id), $rec);

        if ($id) {
            echo json_encode(TRUE);
        }
    }
    
    function _upload_image() {
        
        $config['upload_path'] = FCPATH .'/assets/images/newsletters/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('upload_image'))
        {
            echo json_encode(FALSE);
            exit;
        }
        else
        {
            $up_data = $this->upload->data();
            $id = $this->input->post('id', TRUE);

            $result = $this->campaign_newsletter_m->exists(array('campaign_newsletter_id' => $id));
            if (!$result) {
                return;
            }

            $result = $this->campaign_newsletter_m->get_record(array(
                'where' => array('campaign_newsletter_id' => $id)
            ));

            $attr_name = $this->input->post('attr_name', TRUE);
            $data = json_decode($result['newsletter_template_value'], TRUE);
            $data[$attr_name] = base_url('assets/images/newsletters/'.$up_data['file_name']);
            $data[$attr_name.'_filename'] = $up_data['file_name'];

            $rec['newsletter_template_value'] = json_encode($data);
            
            //Remove old pic
            $old_data = json_decode($result['newsletter_template_value'], TRUE);
            $old_data = (! isset($old_data[$attr_name.'_filename'])) ? 'temp' : $old_data[$attr_name.'_filename'];
            
            if (file_exists($config['upload_path'].$old_data)) {
                unlink($config['upload_path'].$old_data);
            }

            $id = $this->campaign_newsletter_m->_update(array('campaign_newsletter_id' => $id), $rec);

            if ($id) {
                echo json_encode(TRUE);
                exit;
            }
        }
        
        
        
    }
    
    function update_campaign_norule() {
        $no_rule = $this->input->post('no_rule');
        $campaign_newsletter_id = $this->input->post('campaign_id');
        
        $data = array(
            'newsletter_no_rule' => $no_rule
        );
        
        $whr = array('campaign_newsletter_id' => $campaign_newsletter_id);
        
        $this->campaign_newsletter_m->_update($whr, $data);
        
        echo json_encode(TRUE);
    }

    function details($camp_id) {
        $this->load->model('campaign_rule_m');
        
        $camp_id = (int) $camp_id;
        $data['camp_id'] = $camp_id;
        $data['page_action'] = "Details";
        $data['page_title'] = "Campaign Details (by rule)";
        $data['page_subtitle'] = "";


        $res_camp = $this->campaign_m->cmapaign_by_id($camp_id);
        if ($res_camp->num_rows() != 1) {
            redirect(ADMIN_FOLDER . "/campaign/");
            exit();
        }
		$rec_campaign = $res_camp->row();
		if ($rec_campaign->campaign_by_type != "by_rule")
		{
			$this->session->set_flashdata('error_msg', "Sorry, this is not a campaign by rule.");
            redirect(admin_url("campaign"));
            exit();
		}
        $data['rec_camp'] = $res_camp->row();
        $data['res_camp_details'] = $this->campaign_m->select_voucher_product($camp_id);

        if (isset($_POST['submit_rule'])) {
            if (isset($_POST['min_age_checked'])) {
                $this->form_validation->set_rules('min_age', 'Minimum age', 'required|integer|');
            }
            if (isset($_POST['max_age_checked'])) {
                $this->form_validation->set_rules('max_age', 'Maximum age', 'required|integer|');
            }
            if (isset($_POST['regions_checked'])) {
                $this->form_validation->set_rules('regions_id_selected[]', 'Regions', 'required');
            }
            
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            if ($this->form_validation->run() == TRUE) {
                $this->campaign_m->update_campaign_rule($camp_id);
            }
        }
        $data['res_camp_rule'] = $this->campaign_m->select_campaign_rule($camp_id);
        $data['no_rule'] = $rec_campaign->campaign_no_rule;

        $this->load->view(ADMIN_FOLDER . '/campaign_details_v', $data);
    }
   
    function rule_result($camp_id, $no_rule = 0) {
        $camp_id = (int) $camp_id;
        $res_people = $this->campaign_m->select_people_by_rule($camp_id, "");
        $num_rows = $res_people->num_rows();
        
        //Add custom invites if exists
        $query = $this->db->query("SELECT camp_inv_usr_id FROM gl_campaign_invite_users WHERE camp_campaign_id =".$camp_id);
        if ($query->num_rows() > 0 && $no_rule == 1) {
            $num_rows = $query->num_rows();
        }
        
        echo $num_rows;
    }

    function run_campaign($camp_id = 0) {
        $this->load->model('user_notification_settings_m');
        
        $res_people = $this->campaign_newsletter_rule_m->select_people_by_rule($camp_id);
        
        $record = $this->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $camp_id)
        ));
        
        $this->load->library('email_newsletter_templating');
//        echo '<pre>';
//        print_r($res_people->result());
//        exit;
        foreach ($res_people->result() as $p) {
            
            if (! $this->user_notification_settings_m->is_checked(4, $p->user_id)) {
                //Skip email notification
                continue;
            }
            
            $vars = array(
                'from_name' => 'glomp!',
                'newsletter_id' => $record['newsletter_template'],                
                'campaign_newsletter_id' => $camp_id,
                'to' => $p->user_email,
            );
            
            $this->email_newsletter_templating->config($vars);
            $this->email_newsletter_templating->set_subject($record['newsletter_name']);
            $this->email_newsletter_templating->send();
        }
    }

}

//class end
?>