<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 */

class Newsletter extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('newsletter_m');
    }

    function index() {

        //Data to be pass to the main header view
        $main_header['page_title'] = "Manage Newsletter : Glomp";

        $subQuery = "";
        $queryString = "";

        if (isset($_GET['Search'])) {
            $keywords = isset($_GET['keywords']) ? urlencode($_GET['keywords']) : "";
            if ($keywords != "") {
                $subQuery = 'AND (user_email LIKE "%' . $keywords . '%" OR user_name LIKE "%' . $keywords . '%")';
                $queryString = "&keywords=$keywords";
            }
        }

        $records = $this->newsletter_m->get_list(array(
            'select' => 'newsletter_id, date_modified, name, lang',
        ));

        $res_total_row = count($records);

        if (isset($_GET['per_page']) && is_numeric($_GET['per_page'])) {
            $page = intval($_GET['per_page']);
        } else {
            $page = 0;
        }

        $totalRow = $res_total_row;
        $offset = $page;
        $row_per_page = 20;
        $data['newsletters'] = $records;
        $config['base_url'] = base_url(ADMIN_FOLDER . "/email/?q=q&$queryString");
        $config['total_rows'] = $totalRow;
        $config['per_page'] = $row_per_page;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        //Main body
        $this->load->view(ADMIN_FOLDER . '/newsletter/list_v', $data);
    }

    function edit($id = 0) {
        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            show_404();
            return;
        }

        $main_header['page_title'] = "Edit Newsletter : Glomp";

        $data['newsletter'] = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));

        //Set blocks views
        $data['main_header'] = $this->load->view(ADMIN_FOLDER . '/includes/header_main', $main_header, TRUE);
        $data['header'] = $this->load->view(ADMIN_FOLDER . '/includes/header', array(), TRUE);
        $data['main_menu'] = $this->load->view(ADMIN_FOLDER . '/includes/main-menu', array(), TRUE);
        $data['footer'] = $this->load->view(ADMIN_FOLDER . '/includes/footer', array(), TRUE);

        $this->load->view(ADMIN_FOLDER . '/newsletter/edit_v', $data);
    }

    function preview($id = 0) {
        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            show_404();
            return;
        }

        $template['newsletter'] = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));
        $template['attributes'] = json_decode($template['newsletter']['attributes'], TRUE);

        $data['template'] = $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $template['newsletter']['name'], $template, TRUE);
        $data['newsletter'] = $template['newsletter'];

        $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/preview', $data);
    }

    function preview_full($id = 0) {
        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            show_404();
            return;
        }

        $template['newsletter'] = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));

        $template['attributes'] = json_decode($template['newsletter']['attributes'], TRUE);

        $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $template['newsletter']['name'], $template);
    }

    function get_data() {
        $id = $this->input->post('id', TRUE);

        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            return;
        }

        $attr_name = $this->input->post('attr_name', TRUE);

        $result = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));

        $value = json_decode($result['attributes'], TRUE);

        echo json_encode(array('value' => $value[$attr_name]));
    }

    function set_data() {
        $id = $this->input->post('id', TRUE);

        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            return;
        }

        $result = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));

        $attr_name = $this->input->post('attr_name', TRUE);
        $data = json_decode($result['attributes'], TRUE);
        $data[$attr_name] = $this->input->post('value');

        $rec['attributes'] = json_encode($data);

        $id = $this->newsletter_m->_update(array('newsletter_id' => $id), $rec);

        if ($id) {
            echo json_encode(TRUE);
        }
    }

    function test_send($id = 0) {
        $result = $this->newsletter_m->exists(array('newsletter_id' => $id));
        if (!$result) {
            return;
        }

        $template = $this->newsletter_m->get_record(array(
            'where' => array('newsletter_id' => $id)
        ));
        
        $this->load->library('email');

        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'glompzac',
            'smtp_pass' => '6lomp!DOT1+_send',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html'
        ));
        
        $template['attributes'] = json_decode($template['attributes'], TRUE);

        $body = $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $template['name'], $template, TRUE);

        $this->email->from(SITE_DEFAULT_SENDER_EMAIL, SITE_DEFAULT_SENDER_NAME);
        $this->email->to(SITE_DEFAULT_SENDER_EMAIL);
        $this->email->subject('Test Newsletter');
        $this->email->message($body);
        $this->email->send();
        
        echo 'Test Email Sent! to: '. SITE_DEFAULT_SENDER_EMAIL;
    }

}