<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Brands Admin
 *
 */
class Brands extends CI_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('Brand_m', 'Brand');
        $this->load->model('regions_m');
    }

    public function index( $page = 1 ) {
        $data = array();
        $str = '';
        $limit = 10;
        $offset = $page * $limit - $limit;

        $this->load->library('parser');
        $this->load->library('pagination');

        if( $this->input->get('key')) {
            $this->db->like('name',$this->input->get('key'));
            $this->db->or_like('description',$this->input->get('key'));
        }

        $this->db->order_by('gl_brand.created', 'desc');
        $this->db->select('gl_brand.*, gl_template.id as `template_id`');
        $this->db->join( 'gl_template', 'gl_template.referer_id = gl_brand.id', 'left' );
        $this->db->where('gl_brand.status !=','Deleted');
        $q = $this->db->get( 'gl_brand', $limit, $offset );
        
        if( $this->input->get('key')) {
            $this->db->like('name',$this->input->get('key'));
            $this->db->or_like('description',$this->input->get('key'));
        }
        $q2 = $this->db->get_where( 'gl_brand', array('status !='=>'Deleted') );
        $this->pagination->initialize(array(
            'base_url' => '/adminControl/brands/index/',
            'total_rows' => $q2->num_rows(),
            'per_page' => $limit,
            'uri_segment' => 4,
            'use_page_numbers' => TRUE,
            'num_links' => 9
        ));

        $template = $this->load->view('adminControl/brands_index_brandrow_v',null,true,false,false);

        $data['rows'] = $q->num_rows();
        $data['total'] = $q2->num_rows();
        $data['paging'] = $this->pagination->create_links();
        $data['records'] = $this->parser->parse_string($template, array('brands' => $q->result_array()), 1 );

        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
        }

        $this->load->view('adminControl/brands_index_v', $data);
    }


    public function create() {

        $data = array();

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
       // $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('logo', 'Logo', 'trim');
        $this->form_validation->set_rules('banner', 'Banner', 'trim');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('type', 'Type', 'trim|required');
		
		
		if( $this->input->post('type')=='public')
		{
			$this->form_validation->set_rules('public_alias', 'Public Alias', 'trim|required');
		}
        
        //HK default?
        $data['locations'] = $this->regions_m->getAllRegionDropdown(1);

        if ($this->form_validation->run() == TRUE) {

            // load upload file library
            $this->load->library('upload', array(
                'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'max_size'      => '2000',
                'max_width'     => '1200',
                'max_height'    => '1200'
            ));
            
            // upload logo before saving data
            if( $this->upload->do_upload('logo') ) {

                $data['upload_data']['logo'] = $this->upload->data();

                // save to database
                $id = $this->Brand->save(array(
                    'name'        => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'status'      => $this->input->post('status'),
                    'type'      => $this->input->post('type'),
					'public_alias'      => $this->input->post('public_alias'),
					'location_id' => $this->input->post('location_id'),
                    'logo'        => $data['upload_data']['logo']['file_name'],
                    'created_by'  => $this->session->userdata('user_id'),
                    'modified_by' => $this->session->userdata('user_id')
                ));
                
                
                // banner
                if( $_FILES['banner']['name'] != '' ) {
                    // load upload file library
                    $this->load->library('upload', array(
                        'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => '2000',
                        'max_width'     => '1200',
                        'max_height'    => '1200'
                    ));

                    // upload logo before after saving data then update record for banner
                    if( $this->upload->do_upload( 'banner' ) ) {
                        $data['upload_data']['banner'] = $this->upload->data();
                        $this->Brand->save(array(
                            'banner'        => $data['upload_data']['banner']['file_name']
                        ), $id );
                    } else {
                        $data['error'] = $this->upload->display_errors();
                    }

                    $this->load->library('image_lib');

                    // check for cropping
                    if( $this->input->post('x') != '' ) {
                        $this->image_lib->initialize(array(
                            'source_image' => $data['upload_data']['banner']['full_path'],
                            'maintain_ratio' => false,
                            'width' => $this->input->post('w'),
                            'height' => $this->input->post('h'),
                            'x_axis' => $this->input->post('x'),
                            'y_axis' => $this->input->post('y')
                        ));
                        $this->image_lib->crop();
                        $this->image_lib->clear();
                    }

                    // auto-thumbnail for cms banner
                    $this->image_lib->initialize(array(
                        'source_image' => $data['upload_data']['banner']['full_path'],
                        'new_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/thumbnail/cms_'.$data['upload_data']['banner']['file_name'],
                        'width' => '200',
                        'height' => '200',
                        'maintain_ratio' => true
                    ));
                    $this->image_lib->resize();
                }
                
                // check if a file is selected
                if( $_FILES['thumbnail']['name'] != '' ) {
                    // load upload file library
                    $this->load->library('upload', array(
                        'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'max_size'      => '1000',
                        'max_width'     => '300',
                        'max_height'    => '300'
                    ));

                    if( $this->upload->do_upload( 'thumbnail' ) ) {
                        $data['upload_data']['thumbnail'] = $this->upload->data();
                        $this->Brand->save(array(
                            'thumbnail'        => $data['upload_data']['thumbnail']['file_name']
                        ), $id );
                    } else {
                        $data['error'] = $this->upload->display_errors();
                    }
                }

                // redirect to main brand admin page
                redirect('adminControl/brands?status=new_brand_saved&id='.$id);

            } else {

                $data['error'] = $this->upload->display_errors();
            }

        } else {
            if (! isset($_POST['location_id'])) {
                $location_id = 1;
            } else {
                $location_id =$_POST['location_id'];
            }
            
            $data['locations'] = $this->regions_m->getAllRegionDropdown($location_id);
            $data['error'] = validation_errors();
        }

        $this->load->view('adminControl/brands_create_v', $data);
    }

    public function edit( $id ) {
        $data = array();

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('logo', 'Logo', 'trim');
        $this->form_validation->set_rules('banner', 'Banner', 'trim');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		$this->form_validation->set_rules('type', 'Type', 'trim|required');
		
		
		if( $this->input->post('type')=='public')
		{
			$this->form_validation->set_rules('public_alias', 'Public Alias', 'trim|required');
		}
        
        if ($this->form_validation->run() == TRUE) {

            // save to database
            $this->Brand->save(array(
                'name'        => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'location_id' => $this->input->post('location_id'),
                'status'      => $this->input->post('status'),
				 'type'      => $this->input->post('type'),
				'public_alias'      => $this->input->post('public_alias'),
                'modified_by' => $this->session->userdata('user_id')
            ), $id );

            // check if a file is selected
            if( $_FILES['logo']['name'] != '' ) {
                // load upload file library
                $this->load->library('upload', array(
                    'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '2000',
                    'max_width'     => '1200',
                    'max_height'    => '1200'
                ));

                // upload logo before after saving data then update record for logo
                if( $this->upload->do_upload( 'logo' ) ) {
                    $data['upload_data']['logo'] = $this->upload->data();
                    $this->Brand->save(array(
                        'logo'        => $data['upload_data']['logo']['file_name']
                    ), $id );
                } else {
                    $data['error'] = $this->upload->display_errors();
                }
                
            }
            // check if a file is selected
            if( $_FILES['banner']['name'] != '' ) {
                // load upload file library
                $this->load->library('upload', array(
                    'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '2000',
                    'max_width'     => '1200',
                    'max_height'    => '1200'
                ));

                // upload logo before after saving data then update record for banner
                if( $this->upload->do_upload( 'banner' ) ) {
                    $data['upload_data']['banner'] = $this->upload->data();
                    $this->Brand->save(array(
                        'banner'        => $data['upload_data']['banner']['file_name']
                    ), $id );
                } else {
                    $data['error'] = $this->upload->display_errors();
                }

                // auto-thumbnail for CMS use only
                $this->load->library('image_lib', array(
                    'source_image' => $data['upload_data']['banner']['full_path'],
                    'new_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/thumbnail/cms_'.$data['upload_data']['banner']['file_name'],
                    'width' => '200',
                    'height' => '200'
                ));
                $this->image_lib->resize();
            }
            // check if a file is selected
            if( $_FILES['thumbnail']['name'] != '' ) {
                // load upload file library
                $this->load->library('upload', array(
                    'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size'      => '1000',
                    'max_width'     => '300',
                    'max_height'    => '300'
                ));

                // upload logo before after saving data then update record for banner
                if( $this->upload->do_upload( 'thumbnail' ) ) {
                    $data['upload_data']['thumbnail'] = $this->upload->data();
                    $this->Brand->save(array(
                        'thumbnail'        => $data['upload_data']['thumbnail']['file_name']
                    ), $id );
                } else {
                    $data['error'] = $this->upload->display_errors();
                }
            }

            // redirect to main brand admin page
            if( !isset($data['error']) )
                redirect('adminControl/brands/edit/'.$id.'?status=brand_saved');

        } else {
            if (isset($_POST['location_id'])) {
                //HK default?
                $data['locations'] = $this->regions_m->getAllRegionDropdown($_POST['location_id']);
            }
            
            $data['error'] = validation_errors();
        }
        
        $data['brand'] = $this->Brand->get_by_id( $id );
        //HK default?
        $data['locations'] = $this->regions_m->getAllRegionDropdown($data['brand']->location_id);
        
        if (! empty($data['error'])) {
            //HK default?
            $data['locations'] = $this->regions_m->getAllRegionDropdown($_POST['location_id']);    
        }
        
        $data['products'] = $this->get_brandproducts( $id );

        $this->load->view('adminControl/brands_edit_v', $data);
    }

    /**
     * Returns brandproducts with linked merchant products data
     *
     * @param $id brand_id
     */
    private function get_brandproducts( $id ) {
        $products = $this->Brand->get_brandproducts( $id );
        if( $products ) {
            foreach( $products as $p ) {
                $this->db->select('gl_brands_products.id,gl_product.prod_id,gl_product.prod_name,gl_product.prod_image,gl_merchant.merchant_name');
                $this->db->join('gl_product',"gl_brands_products.product_id = gl_product.prod_id");
                $this->db->join('gl_merchant',"gl_product.prod_merchant_id = gl_merchant.merchant_id");
                $q1 = $this->db->get_where('gl_brands_products',array('brandproduct_id'=>$p->id));
                $p->linked_products_count = $q1->num_rows();
                $p->linked_products = $q1->result();
                
            }
        }
        return $products;
    }

    /**
     *
     * @param $id Integer brand_id
     * @param $confirm Integer 1|0
     */
    public function delete( $id, $confirm = 0 ) {
        $this->load->library('parser');
        $data = array();

        if( $confirm == 1 ) {

            if( $this->Brand->delete($id) ) {
                redirect('/adminControl/brands/');
            }

            // some error occured
            $data['error'] = '<p>Unable to delete record. Please try again after a while.</p>';
        }

        $data['brand'] = $this->Brand->get_by_id( $id );
        $data['confirm'] = $this->parser->parse_string(
            $this->load->view('adminControl/brands_delete_confirm_v',null,true,false,false),
            array(
                'error'=>(isset($data['error']))?$data['error']:'',
                'id' => $data['brand']->id,
                'name' => $data['brand']->name
            ),
            1
        );

        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            return;
        }

        $this->load->view('adminControl/brands_delete_v', $data);
    }

    /**
     *
     * @param $id Integer brand_id
     */
    public function template( $brand_id = 0 ) {

        $data['template'] = $this->Brand->get_or_create_template( $brand_id );

        if( $this->input->is_ajax_request() ) {
            echo json_encode($data);
            return;
        }

    }

    /**
     * ajax function for search products to add in a brand
     *
     */
    public function search_for_products() {

        $key = $this->input->post('product_search_key');

        $this->db->select('prod_id, prod_name, prod_details, prod_status, prod_image, prod_merchant_id, gl_merchant.merchant_name');
        $this->db->like('prod_name', $key);
        $this->db->or_like('prod_details', $key);
        $this->db->join('gl_merchant','gl_product.prod_merchant_id = gl_merchant.merchant_id', 'left');
        $q = $this->db->get('gl_product');
        if( $q->num_rows() > 0 ) {
            echo json_encode( array('result'=>'success','products'=>$q->result() ) );
            exit;
        }
        echo json_encode( array('result'=>'error','message'=>'No Record Found.') );
        exit;
    }

    /**
     * Links an existing product to a brandproduct
     *
     * @param Int $brand_id
     * @todo filter already linked product_id
     */
    public function link_products() {
        $data = array();

        $product_ids = $this->input->get('product_id');
        $brandproduct_id = $this->input->get('brandproduct_id');
        
        if( is_array( $product_ids ) && count($product_ids) > 0 ) {
            $ids = array();
            foreach( $product_ids as $id ) {
                $this->db->insert('gl_brands_products', array(
                    'brandproduct_id' => $brandproduct_id,
                    'product_id'=> $id
                ));
                $ids[] = $this->db->insert_id();
            }
            $data['ids'] = $ids;
            $data['result'] = 'success';
            echo json_encode($data);
            exit;
        }
        $data['result'] = 'failed';
        $data['message'] = 'There was a problem adding the product. Refresh this window and try again.';
        echo json_encode($data);
        exit;
    }


    public function delete_product( $record_id ) {

        if( $this->db->delete('gl_brandproduct',array('id'=>$record_id)) ) {
            $data['result'] = 'success';
            echo json_encode($data);
            exit;
        }

        $data['record_id'] = $record_id;
        $data['result'] = 'error';
        $data['message'] = 'There was a problem deleting the product. Refresh this window and try again.';

        echo json_encode($data);
        exit;
    }
    
    public function save_brandproduct() {
        $data = array();
        $result = 'saved';
        
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        $this->form_validation->set_rules('brand_id','Brand','required');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('description','Description','required');
        $this->form_validation->set_rules('intro_text','intro_text','trim');
        
        if( !$this->input->post('id') ) {
            $this->form_validation->set_rules('image_logo','Logo','callback__check_logo');

        } else {
            $data['id'] = $this->input->post('id');
            $result = 'updated';
        }

        if ($this->form_validation->run() == TRUE) {
            
            $now = gmdate('Y-m-d H:i:s');
            
            $data['brand_id'] = $this->input->post('brand_id');
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['intro_text'] = $this->input->post('intro_text');
            $data['created'] = $now;
            $data['modified'] = $now;
            $data['created_by'] = $this->session->userdata('admin_id');
            $data['modified_by'] = $this->session->userdata('admin_id');
            
            // insert record before uploading images
            $data['id'] = $this->Brand->save_brandproduct( $data );
            
            // upload logo
            $image_logo = $this->upload_brandproduct_image( 'image_logo', '2000', '1024', '1024' );
            if( is_array( $image_logo ) ) {
                $this->Brand->save_brandproduct(array(
                    'image_logo'        => $image_logo['file_name'],
                    'id' => $data['id']
                ));
            } else {
                $data['error'][] = $this->upload->display_errors();
            }
            
            $p = $this->Brand->get_brandproduct( $data['id'] );
            $data['image_logo'] = $p->image_logo;
            $data['image_banner'] = $p->image_banner;
            $data['image_thumbnail'] = $p->image_thumbnail;
            
            echo json_encode(array(
                'result' => $result,
                'message' => $this->lang->line('brandproduct_record_saved', 'Record Saved!'),
                'data' => $data
            ));
            
            exit;
            
        } else {
            
            echo json_encode(array(
                'result' => 'failed',
                'message' => validation_errors(),
                'errors' => array(
                    'name' => form_error('name'),
                    'description' => form_error('description'),
                    'intro_text' => form_error('intro_text'),
                    'image_logo' => form_error('image_logo'),
                    'image_banner' => form_error('image_banner'),
                    'image_thumbnail' => form_error('image_thumbnail')
                )
            ));
            
            exit;
        }
        
    }
    
    /**
     * Used for uploading brandproduct images
     */
    private function upload_brandproduct_image( $field_name, $max_size, $max_width, $max_height ) {
    
        $this->load->library('upload', array(
            'upload_path'   => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/products/',
            'allowed_types' => 'gif|jpg|png',
            'max_size'      => $max_size,
            'max_width'     => $max_width,
            'max_height'    => $max_height
        ));

        if( $this->upload->do_upload( $field_name ) ) {
            
            // auto-thumbnail from image_logo upload field
            if( $field_name == 'image_logo' ) {
                $upload_data = $this->upload->data();
                $this->load->library('image_lib', array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/products/thumb_'.$upload_data['file_name'],
                    'width' => '200',
                    'height' => '200'
                ));
                $this->image_lib->resize();
            }

            return $this->upload->data();
        }
        
        return FALSE;
    }
    
    private function check_file_input( $field ) {
        if(isset($_FILES) and array_key_exists($field,$_FILES))
            return ( $_FILES[$field]['name'] != '' ) ? TRUE : FALSE;
        return FALSE;
    }
    
    public function _check_logo() {
        $this->form_validation->set_message('_check_logo', 'The %s field is required a');
        return $this->check_file_input( 'image_logo' );
    }
    
    public function _check_banner() {
        $this->form_validation->set_message('_check_banner', 'The %s field is required a');
        return $this->check_file_input( 'image_banner' );
    }
    
    public function _check_thumbnail() {
        $this->form_validation->set_message('_check_thumbnail', 'The %s field is required a');
        return $this->check_file_input( 'image_thumbnail' );
    }
    
    public function list_linked_merchant_products() {
        $brandproduct_id = $this->input->get('brandproduct_id');
        $this->db->select('gl_brands_products.id, gl_product.prod_name, gl_product.prod_image, gl_merchant.merchant_name, gl_merchant.merchant_logo');
        $this->db->from('gl_brands_products');
        $this->db->join('gl_product','gl_brands_products.product_id = gl_product.prod_id');
        $this->db->join('gl_merchant','gl_product.prod_merchant_id = gl_merchant.merchant_id');
        $this->db->where('gl_brands_products.brandproduct_id = ' . $brandproduct_id);
        $this->db->order_by('gl_brands_products.id','desc');
        $q = $this->db->get();
        
        if( $q->num_rows() > 0 ) {
            $data['result'] = 'success';
            $data['products'] = $q->result_array();
            echo json_encode($data);
            exit;
        }
        
        $data['result'] = 'failed';
        $data['products'] = array();
        $data['message'] = 'No Records Found';
        echo json_encode($data);
        exit;
    }
    
    public function unlink_product() {
        
        if( $this->db->delete('gl_brands_products',array('id'=>$this->input->get('id'))) ){
            $data['result'] = 'success';
            $data['message'] = 'Product Unlinked';
            echo json_encode($data);
            exit;
        }
        $data['result'] = 'falied';
        $data['message'] = 'There was a problem unlinking the product. Please refresh this page and try again.';
        echo json_encode($data);
        exit;
    }
    
    public function crop_banner() {
        
        $banner = $this->input->post('banner');
        $brand_id = $this->input->post('brand_id');
        $x = $this->input->post('x');
        $y = $this->input->post('y');
        $x2 = $this->input->post('x2');
        $y2 = $this->input->post('y2');
        $w = $this->input->post('w');
        $h = $this->input->post('h');

        $this->load->library('image_lib', array(
            'maintain_ratio' => FALSE,
            'source_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/'.$banner,
            'width' => $w,
            'height' => $h,
            'x_axis' => $x,
            'y_axis' => $y
        ));
        $this->image_lib->crop();
        
        echo $this->image_lib->display_errors();
        
        exit;
    }
    
    public function crop_banner_thumbnail() {
    
        $banner = $this->input->post('banner');
        $brand_id = $this->input->post('brand_id');

        $this->load->library('image_lib', array(
            'source_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/'.$banner,
            'new_image' => str_replace( SELF, '', $_SERVER['SCRIPT_FILENAME']) . 'custom/uploads/brands/thumbnail/cms_'.$banner,
            'width' => '200',
            'height' => '200'
        ));
        
        $this->image_lib->resize();
        
        echo $this->image_lib->display_errors();
        
        exit;
    }
}
