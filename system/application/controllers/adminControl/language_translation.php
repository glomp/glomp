<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Ryan
Desc: Admin
*/

class Language_translation extends CI_Controller 
{
	
	function __construct()
	{
		parent::__Construct();		
		$this->load->library('admin_login_check');
		$this->load->model('admin_m');
	}
	function index()
	{
		$data['page_action']="view";
		$data['page_title']="Manage Admins";
		$data['page_subtitle'] = "View All";
		$data['res_admin']= $this->admin_m->selectAdmin(array('admin_type'=>'Admin'));


		
		if($this->session->flashdata('msg'))
			$data['msg'] = $this->session->flashdata('msg');
			
		if($this->session->flashdata('err_msg'))
			$data['error_msg']=$this->session->flashdata('err_msg');		
        $this->load->view(ADMIN_FOLDER.'/language_translation_v',$data, false, false);
	}
}//class end
?>