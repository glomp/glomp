<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-25-2013
Desc: My Account page

*/

class Myaccount extends CI_Controller
{
	private $INSTANCE = 'gl_admin';
	function myaccount()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('myaccount_m');
	}
	
	function index()
	{
		$data['page_action']="edit";
		$data['page_title']="glomp : My Profile";
		
		$admin_info = $this->db->get_where($this->INSTANCE,array('admin_id'=>$this->session->userdata('admin_id')));
		$row = $admin_info->row();
		$data['row'] = $row;
		
		$admin_salt = $row->admin_salt;
		$admin_hash_pwd = $row->admin_password;
		$admin_pwod = $this->input->post('old_pword');
		$admin_entered_has_pwod = $this->myaccount_m->hash_key_value($admin_salt,$admin_pwod);
		
		
		if(isset($_POST['UpdatePword']))
		{	
			$this->form_validation->set_rules('admin_name', 'Name', 'required');
			$this->form_validation->set_rules('old_pword', 'Old Password', 'required');
			
			if(isset($_POST['change_password']))
			{
				
				$this->form_validation->set_rules('new_pword', 'New Password', 'required');
				$this->form_validation->set_rules('retype_new_pword', 'Retype New Password', 'required|matches[new_pword]');
			}
			
			
			if($this->form_validation->run() == FALSE)
			{
				$this->load->view(ADMIN_FOLDER."/myaccount_v", $data);
			}
			else
			{
				if($admin_hash_pwd == $admin_entered_has_pwod)
				{
					//update into database
					$update_data =  array('admin_name'=>$this->input->post('admin_name'));
					$this->db->where('admin_id',$this->session->userdata('admin_id'));
					$this->db->update($this->INSTANCE, $update_data); 
				$this->session->set_flashdata('msg',"Profile updated successfully.");
				if(isset($_POST['change_password']))
					{
						$admin_new_password = $this->input->post('new_pword');
						$new_hash_pwod =  $this->myaccount_m->hash_key_value($admin_salt,$admin_new_password);
						$this->myaccount_m->passwordChange($this->session->userdata('admin_id'),$new_hash_pwod);
						$this->session->set_flashdata('msg',"Password changed successfully.");
					}
				}
				else
				{
					$this->session->set_flashdata('err_msg',"Please enter your correct Password");
				}
				
				redirect(ADMIN_FOLDER."/myaccount");
				exit();
			}//else validation is okey
		}//update password end
		else
		{
			if($this->session->flashdata('msg'))
			$data['msg']=$this->session->flashdata('msg');
			
			if($this->session->flashdata('err_msg'))
			$data['error_msg']=$this->session->flashdata('err_msg');
			$this->load->view(ADMIN_FOLDER."/myaccount_v", $data);
		}
		
	}
}
?>