<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-26-2013
Desc: cms pages
*/

class Cmspages extends CI_Controller
{
	private $pagination_per_page= 20;
	
	function cmspages()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('cmspages_m');		
		$this->load->library('lib_pagination');		
	}
	
	function index()
	{
		//echo $this->session->flashdata('msg'); die();
		$data['page_action']="view";
		$data['page_title']="Manage Pages : CMS Pages";
		
		/*pagination starts*/
		if($this->uri->segment(4))
		$start=$this->uri->segment(4);
		else
		$start=0;
		$per_page=$this->pagination_per_page;
		$pagination_base_url=base_url(ADMIN_FOLDER."/cmspages/index/");
		$num_rows=$this->cmspages_m->selectCmspages(0,0,DEFAULT_LANG_ID)->num_rows();
		$data['links']=$this->lib_pagination->DoPagination($pagination_base_url,$num_rows,$per_page,"4");
		$data['res_cmspages']= $this->cmspages_m->selectCmspages($start,$per_page,DEFAULT_LANG_ID)->result();
		/*pagination ends*/
		
		if($this->session->flashdata('msg'))
		$data['msg'] = $this->session->flashdata('msg');
		if($this->session->flashdata('err_msg'))
		$data['error_msg']=$this->session->flashdata('err_msg');
		
		$this->load->view(ADMIN_FOLDER.'/cmspages_v',$data);
	}
	
	function publish($contentID, $contentStatus)
	{
		$contentID = (int)($contentID);
		if($this->cmspages_m->selectCmspageByID($contentID,DEFAULT_LANG_ID)->num_rows()>0 && ($contentStatus=="Y" || $contentStatus=="N"))
		{	
			$this->cmspages_m->publish($contentID,$contentStatus);
			$this->session->set_flashdata('msg',"Status changed successfully");
		}
		redirect(ADMIN_FOLDER."/cmspages/index/");
		exit();
	}
	
	function deletePage($contentID=0)
	{
		$contentID = (int)($contentID);
		if($this->cmspages_m->selectCmspageByID($contentID,DEFAULT_LANG_ID)->num_rows()>0)
		{
			$result = $this->cmspages_m->delete($contentID);
			if($result!='TRUE')
			{
				$this->session->set_flashdata('err_msg',"Sorry, this record is not allowed to delete.");
			}else
			{
				$this->session->set_flashdata('msg',"Record deleted successfully");
			}
		}
		redirect(ADMIN_FOLDER."/cmspages/index/");
		exit();
	}
	
	function UpdateDisplayOrder()
	{
		if(isset($_POST['updateDispOrder']))
		{
			$count=count($_POST['cms_id']);
			for($i=0;$i<$count;$i++)
			{
				$this->cmspages_m->UpdateDisplayOrder($_POST['disp_order'][$i],$_POST['cms_id'][$i]);
			}
			$this->session->set_flashdata('msg',"Record updated successfully");
		}
		redirect(ADMIN_FOLDER."/cmspages/index/");
		exit();
	}
	
	function addPage()
	{
		$data['page_action']="add";
		$data['page_title']="Manage Pages ";
		if(isset($_POST['addPage']))
		{
			$this->form_validation->set_rules('menu_id', 'Menu', 'required');
			$this->form_validation->set_rules('page_name', 'Page Name', 'required');
			$this->form_validation->set_rules('page_title', 'Page Title', 'required');
			$this->form_validation->set_rules('contents', 'Contents', 'required');
			if($this->form_validation->run() == FALSE)
			{
				$this->load->view(ADMIN_FOLDER."/cmspages_v", $data);
			}
			else
			{
				$this->cmspages_m->addPage();
				$this->session->set_flashdata('msg',"Record successfully added.");
				redirect(ADMIN_FOLDER."/cmspages/index/");
				exit();
			}
		}
		else
		{
			$this->load->view(ADMIN_FOLDER."/cmspages_v", $data);
		}
	}
	
	function editPage($contentID=0,$langID=0)
	{
		$contentID=(int)($contentID);
		$langID=(int)($langID);
		if(!$this->cmspages_m->selectCmspageByID($contentID)->num_rows()>0)
		{
			redirect(ADMIN_FOLDER."/cmspages/index/");
			exit();
		}
		
		if($langID>0)
		$language=$langID;
		else
		$language=DEFAULT_LANG_ID;
		
		$data['res_cmspages']= $this->cmspages_m->selectCmspageByID($contentID,$language)->row();
                
		$data['page_action']="edit";
		$data['page_title']="Manage Pages ";
		if(isset($_POST['editPage']))
		{
			
			$this->form_validation->set_rules('lang_id', 'Language', 'required');
			$this->form_validation->set_rules('menu_id', 'Menu', 'required');
			$this->form_validation->set_rules('page_name', 'Page Name', 'required');
			$this->form_validation->set_rules('page_title', 'Page Title', 'required');
			$this->form_validation->set_rules('contents', 'Contents', 'required');
			if($this->form_validation->run() == FALSE)
			{
				$this->load->view(ADMIN_FOLDER."/cmspages_v", $data);
			}
			else
			{				
				$this->cmspages_m->editPage($contentID);
				$this->session->set_flashdata('msg',"Record successfully updated.");
			//	redirect(ADMIN_FOLDER."/cmspages/index/");
				exit();
			}
		}
		else
		{
			$this->load->view(ADMIN_FOLDER."/cmspages_v", $data);
		}
	}
	
}
?>