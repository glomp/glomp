<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Ryan
Desc: Admin
*/
//error_reporting(0);

class post extends CI_Controller {	
	function __construct(){
		parent::__Construct();				
		$this->load->model('admin_m');
		$this->load->model('translation_m');
		$this->load->model('lang_m');		
	}
	function index(){		
		$data['page_title']="Post";
		$data['response']="";
		$loginCheck=$this->admin_m->checkLogin();
		if($loginCheck->status=="Ok"){
			
			$data['response']=$loginCheck;
		}
		else{//not log in or session expired
			$data['response']=$loginCheck;
		}
		$this->load->view(ADMIN_FOLDER.'/post_v',$data);
	}
	function country(){
		$response=$this->admin_m->checkLogin();
		if($response->status=="Ok"){
			$resData= (object) array('status' => 'Ok', 'message' => 'Ok', 'data' => '');						
			switch(($this->uri->segment(4, 0)))
			{
				case 'generate':
					$this->translation_m->generateCountry();
					//$this->translation_m->generateLanguage();					
				break;
			}
		}
	}
        function generateTable() {
            $this->translation_m->generateTable();
        }
	function translation(){	
		$response=$this->admin_m->checkLogin();
		if($response->status=="Ok"){
			$resData= (object) array('status' => 'Ok', 'message' => 'Ok', 'data' => '');						
			switch(($this->uri->segment(4, 0)))
			{				
				case 'generateFiles':
					$this->translation_m->generateFile();
				break;
				case 'checkTermIfAvailable':
					if( isset($_POST['term_name']) ){
						$resData->data=$this->translation_m->checkIfTermNameAvailable($_POST['term_name']);													
						if($resData->data==1){
							$resData->status='Error';
							$resData->message='Term name already in used.';
						}						
					}
					else{
						$resData->status='Error';
						$resData->message='term_name is not set';
					}
				break;
				case 'addNewTerm':						
					if( isset($_POST['term_name']) && isset($_POST['term_desc'])	){
						$resData->data=$this->translation_m->addNewTerm();
						if($resData->data==1){
							$resData->status='Error';
							$resData->message='Some fields are not set.';
						}
						
					}
					else{
						$resData->status='Error';
						$resData->message='trans_eng_name is not set';
					}
				break;
				case 'updateTransDetails':
					$resData->data=$this->translation_m->updateTransDetails();
				break;
				case 'deleteTransDetails':
					$resData->data=$this->translation_m->deleteTransDetails();
				break;				
				case 'getLanguageList':				
					$resData->data=$this->lang_m->listAllLang();
					if($resData->data==false){
							$resData->status='Error';
							$resData->message='No language on the list.';
					}
				break;
				case 'checkIfTransNameAvailable':
					if(isset($_POST['trans_eng_name'])){
						$resData->data=$this->translation_m->checkIfTransNameAvailable($_POST['trans_eng_name']);													
						if($resData->data==1){
							$resData->status='Error';
							$resData->message='Translation name already exist.';
						}
						
					}
					else{
						$resData->status='Error';
						$resData->message='trans_eng_name is not set';
					}
				break;
				case 'addNewTranslation':
					if(isset($_POST['trans_eng_name']) && isset($_POST['trans_native_name'])){
						$resData->data=$this->translation_m->addNewTranslation();
					}
					else{
						$resData->status='Error';
						$resData->message='trans_eng_name/ trans_native_name is not set';
					}
				break;
				case 'populateTranslationTable':
					if(isset($_POST['startRow']) && isset($_POST['numRow'])){
						$resData->data=$this->translation_m->populateTranslationTable();
						if($resData->data==false){
								$resData->status='Error';
								$resData->message='No language on the list.';
						}
					}
					else{
						$resData->status='Error';
						$resData->message='startRow/ numRow is not set';
					}					
				break;
				default:
					$resData->status='Error';
					$resData->message='Invalid Function';
					
				
			}
			$response->data=$resData;
			$data['response']=$response;
		}
		else{//not log in or session expired
			$data['response']=$response;
		}		
        $this->load->view(ADMIN_FOLDER.'/post_v',$data,false, false, false);		
			
	}
}//class end
?>