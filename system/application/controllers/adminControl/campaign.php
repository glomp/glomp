<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  Author: Shree
  Date: Aug-18-2013
  Desc: Campaign
 */

class Campaign extends CI_Controller {

    private $pagination_per_page = 20;
    
    

    function __construct() {
        parent::__Construct();
        $this->load->library('admin_login_check');
        $this->load->model('campaign_m');
        $this->load->library('lib_pagination');
        $this->load->model('product_m');
        $this->load->model('regions_m');
        $this->load->model('users_m');
        $this->load->model('merchant_m');
        $this->load->model('brand_m');
        $this->load->model('send_email_m');
    }
	
    function index() {
        $data['page_action'] = "view";
        $data['page_title'] = "Manage Campaign (by rule)";
        $data['page_subtitle'] = "View All";
        $start = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $per_page = $this->pagination_per_page;
        $pagination_base_url = admin_url("campaign/index/");
        $num_rows = $this->campaign_m->selectCampaign('by_rule')->num_rows();

        $data['links'] = $this->lib_pagination->DoPagination($pagination_base_url, $num_rows, $per_page, "4");
        $data['res_campaign'] = $this->campaign_m->selectCampaign('by_rule');



        if ($this->session->flashdata('msg'))
            $data['msg'] = $this->session->flashdata('msg');

        if ($this->session->flashdata('err_msg'))
            $data['error_msg'] = $this->session->flashdata('err_msg');
        $this->load->view(ADMIN_FOLDER . '/campaign_v', $data);
    }

    function AddCampaign() {
        $data['page_action'] = "add";
        $data['page_title'] = "Create New Campaign (by rule)";
        $data['page_subtitle'] = "Create New Campaign";
        
        
        $merchants = $this->db->select('merchant_id,merchant_name,merchant_region_id')->order_by('merchant_name')->get('gl_merchant')->result();
        foreach($merchants as $m){
            $products = $this->db->get_where('gl_product',array('prod_merchant_id'=>$m->merchant_id))->result();
            $data['merchants'][$m->merchant_id] = array(
                'name'=>$m->merchant_name,
                'region'=>$m->merchant_region_id,
                'products' => $products
            );
        }
        $sponsor = $this->db->get_where('gl_categories',array('cat_name'=>'Sponsor'));
        if($sponsor->num_rows() > 0) {
            $sponsor_id = $sponsor->row()->cat_id;
            $this->db->join('gl_merchant','gl_merchant.merchant_id = gl_product.prod_merchant_id');
            $this->db->join('gl_product_inventory', 'gl_product_inventory.prod_id = gl_product.prod_id','left');
            $sponsored = $this->db->select('gl_product.*,gl_merchant.*,gl_product_inventory.current_qty,gl_product_inventory.change_qty')->from('gl_product')->where('prod_cat_id = ' . $sponsor_id )->get();
            if( $sponsored->num_rows() > 0 ) {
                $data['sponsor'] = $sponsored->result();
            }
        }

        $data['regions'] = $this->db->order_by('region_name')->get('gl_region')->result();

        if (isset($_POST['create_campaign'])) {

            $this->form_validation->set_rules('campaign_name', 'Campaign Name', 'required');
            $this->form_validation->set_rules('campaign_type', 'Campaign type', 'required');
            $this->form_validation->set_rules('expiry_day', 'Expiry day', 'required|integer|is_natural_no_zero');
            $this->form_validation->set_rules('product_id[]', 'Product', 'required|integer|is_natural_no_zero');

            $extra_rule = "";
            $extra_rule_invite = "";
            $extra_rule_invite2 = "";
            
            if (isset($_POST['invite_email'])) {
                if (count($_POST['invite_email']) > 0) {
                    foreach ($_POST['invite_email'] as $e) {
                        if (! empty($e)) {
                            $extra_rule_invite = "valid_email|required|callback_check_voucher_qty";
                        }
                    }
                    
                    $this->form_validation->set_rules('invite_email[]', 'Invite by Email',  $extra_rule_invite);
                }
            }
            
            if (isset($_POST['invite_sender_email'])) {
                if (count($_POST['invite_sender_email']) > 0) {
                    foreach ($_POST['invite_sender_email'] as $e) {
                        if (! empty($e)) {
                            $extra_rule_invite2 = "valid_email|required|callback_check_sender_email";
                        }
                    }
                    
                    $this->form_validation->set_rules('invite_sender_email[]', 'Sender Email',  $extra_rule_invite2);
                }
            }
            
            
            if ($_POST['campaign_type'] == 'Both') {
                $extra_rule = "|callback_check_divisible";
                if (isset($_POST['invite_email']) && count($_POST['invite_email']) > 0) {
                    foreach ($_POST['invite_email'] as $e) {
                        if (! empty($e)) {
                            $extra_rule_invite .= "|callback_check_divisible_invite";
                        }
                    }
                    $this->form_validation->set_rules('invite_email[]', 'Invite by Email', $extra_rule_invite);
                }
            }
            
            $this->form_validation->set_rules('voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
            
            if ($this->form_validation->run() == TRUE) {
                $insert_id = $this->campaign_m->create_campaign();
                if ($insert_id > 0) {
                    //
                    $cnt = count($_POST['product_id']);
                    for ($i = 0; $i < $cnt; $i++) {
                        $prod_id = $_POST['product_id'][$i];
                        $qty = $_POST['voucher_qty'][$i];
                        $type = 1;
                        if ($_POST['campaign_type'] == 'Both') {
                            $type = ($qty % 2 == 0) ? 1 : 0;
                        }
                        if ($prod_id > 0 && $qty > 0 && $type > 0) {

                            $this->campaign_m->create_campaign_details($insert_id, $prod_id, $qty);
                        }
                    }
                   
                    #NEW! ADD PRE-SELECTED USER: 10/29/2013
                    $cnt = count($_POST['invite_email']);
                    for ($i = 0; $i < $cnt; $i++) {
                        //If not provided dont save
                        if ( empty($_POST['invite_email'][$i])) break;
                        $this->campaign_m->create_invite_details($insert_id, $i);
                    }
                    
                    $this->session->set_flashdata('msg', "Record successfully added.");
                    redirect(ADMIN_FOLDER . "/campaign/details/$insert_id/");
                }
            }
        }
        $this->load->view(ADMIN_FOLDER . '/campaign_v', $data);
    }
    function _check_log_date($date) {

        $date = explode("/",$date);
        
        $month = (int) isset($date[0])? $date[0]:"";
        $day = (int) isset($date[1])? $date[1]:"";
        $year = (int) isset($date[2])? $date[2]:"";
        if (checkdate($month, $day, $year))
            return true;
        else {
            $this->form_validation->set_message('_check_log_date', 'Please provide valid From/To Log Date');
            return FALSE;
        }
    }
    function details($camp_id) {
    
        $this->load->model('campaign_rule_m');

        $camp_id = (int) $camp_id;
        $data['camp_id'] = $camp_id;
        $data['page_action'] = "Details";
        $data['page_title'] = "Campaign Details (by rule)";
        $data['page_subtitle'] = "";


        $res_camp = $this->campaign_m->cmapaign_by_id($camp_id);
        if ($res_camp->num_rows() != 1) {
            redirect(ADMIN_FOLDER . "/campaign/");
            exit();
        }
		$rec_campaign = $res_camp->row();
		if ($rec_campaign->campaign_by_type != "by_rule")
		{
			$this->session->set_flashdata('error_msg', "Sorry, this is not a campaign by rule.");
            redirect(admin_url("campaign"));
            exit();
		}
        $data['rec_camp'] = $res_camp->row();
        $data['res_camp_details'] = $this->campaign_m->select_voucher_product($camp_id);
        
        
        
        if (isset($_POST['submit_rule'])) {
            $this->form_validation->set_rules('min_age', 'Minimum age', 'required|integer|');
            $this->form_validation->set_rules('max_age', 'Maxminum age', 'required|integer');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('regions_id_selected[]', 'Regions', 'required');
            
            if (isset($_POST['whitelabel_id_checked']))
                $this->form_validation->set_rules('whitelabel_id_selected[]', 'White Labels', 'required');
            
            if (isset($_POST['whitelabel_actions_checked']))
                $this->form_validation->set_rules('whitelabel_actions_selected[]', 'Actions', 'required');
            
            if (isset($_POST['whitelabel_merchants_checked']))
                $this->form_validation->set_rules('whitelabel_merchants_selected[]', 'Merchants', 'required');
            
            if (isset($_POST['whitelabel_products_checked']))
                $this->form_validation->set_rules('whitelabel_products_selected[]', 'Products', 'required');
            
            if (isset($_POST['date_from_checked']))
            {
                $this->form_validation->set_rules('whitelabel_date_from', 'Log Date From', 'required|callback__check_log_date');
                $this->form_validation->set_rules('whitelabel_date_to', 'Log Date To', 'required|callback__check_log_date');
            }
            
            if ($this->form_validation->run() == TRUE) {
                $this->campaign_m->update_campaign_rule($camp_id);
            }
        }
        $data['res_camp_rule'] = $this->campaign_m->select_campaign_rule($camp_id);
        $data['no_rule'] = $rec_campaign->campaign_no_rule;
        
        
        $this->db->select('bp.id, bp.name');
        $this->db->from('gl_brandproduct bp');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $white_label_merchants = $this->db->get();

        $this->db->select('p.id,p.product_id, bp.name, gl_p.prod_name');
        $this->db->from('gl_brands_products p');
        $this->db->join('gl_product gl_p', 'gl_p.prod_id = p.product_id', 'left');
        $this->db->join('gl_brandproduct bp', 'p.brandproduct_id = bp.id', 'left');
        $this->db->join('gl_brand b', 'b.id = bp.brand_id', 'left');
        $this->db->where("b.public_alias <> '' ");
        $this->db->order_by("bp.name ASC, gl_p.prod_name ASC");
        $query = $this->db->get(); 
        $white_label_products = $query;    
        
        $rule_details = json_decode($data['res_camp_rule']->row()->rule_details);
        
        //print_r($rule_details);
        $map = array(
                'all_list' => $this->campaign_m->white_label_list,
                'selected_list' => isset($rule_details->selected_list) ? $rule_details->selected_list:'',
                
                'all_actions' => $this->campaign_m->white_label_actions,
                'selected_actions' => isset($rule_details->selected_actions) ? $rule_details->selected_actions:'',
                
                'all_merchants' => $white_label_merchants,
                'selected_merchants' => isset($rule_details->selected_merchants) ? $rule_details->selected_merchants:'',
                
                'all_products' => $white_label_products,
                'selected_products'=> isset($rule_details->selected_products) ? $rule_details->selected_products:'',
                
                'date_from' => isset($rule_details->date_from) ? $rule_details->date_from:'',
                'date_to' =>isset($rule_details->date_to) ? $rule_details->date_to:''
        );
        $white_label = new stdClass();
        foreach($map as $k => $v)
            $white_label->$k = $v;
        $data['white_label'] = $white_label;
        
        $data['whitelabel_id_checked'] = ( isset($rule_details->selected_list) && $rule_details->selected_list !='') ? 'checked' : '';
        $data['whitelabel_actions_checked'] = ( isset($rule_details->selected_actions) && $rule_details->selected_actions !='') ? 'checked' : '';
        $data['whitelabel_merchants_checked'] = ( isset($rule_details->selected_merchants) && $rule_details->selected_merchants !='') ? 'checked' : '';
        $data['whitelabel_products_checked'] = ( isset($rule_details->selected_products) && $rule_details->selected_products !='') ? 'checked' : '';
        $data['date_from_checked'] = ( isset($rule_details->date_from) && $rule_details->date_from !='' && $rule_details->date_from !='0') ? 'checked' : '';

        
        $this->load->view(ADMIN_FOLDER . '/campaign_details_v', $data);
    }
    function update_campaign_norule() {
        $no_rule = $this->input->post('no_rule');
        $campaign_id = $this->input->post('campaign_id');
        
        $data = array(
            'campaign_no_rule' => $no_rule
        );
        
        $whr = array('campaign_id' => $campaign_id);
        //TODO: need to make gl campaign global
        $this->db->update('gl_campaign', $data, $whr);
        
        echo json_encode(TRUE);
    }

    function voucher_details($camp_id, $recently_distributed_voucher = 0) {
        $camp_id = (int) $camp_id;
        $data['camp_id'] = $camp_id;
        $data['page_action'] = "view";
        $data['page_title'] = "Voucher Details (by rule)";
        $data['page_subtitle'] = "";
        $data['recently_distributed_voucher'] = $recently_distributed_voucher;

        $res_camp = $this->campaign_m->voucherDetails($camp_id);
        if ($res_camp->num_rows() == 0) {
            redirect(ADMIN_FOLDER . "/campaign/");
            exit();
        }
        $data['res_camp'] = $res_camp;

        $res_camp_ = $this->campaign_m->cmapaign_by_id($camp_id);
        $data['rec_camp_'] = $res_camp_->row();

        $this->load->view(ADMIN_FOLDER . '/campaign_voucher_details_v', $data);
    }

    function editCampaign($camp_id) {
        $camp_id = (int) $camp_id;
        $res_camp = $this->campaign_m->cmapaign_by_id($camp_id);
        $data['camp_id'] = $camp_id;
        if ($res_camp->num_rows() != 1) {
            redirect(ADMIN_FOLDER . "/campaign/");
            exit();
        }

        $rec_campaign = $res_camp->row();
        $data['rec_campaign'] = $rec_campaign;

        //Note if campaign is already executed then do not allow to edit the campaign deails tabale. It will effect into so many places.
		if ($rec_campaign->campaign_by_type != "by_rule")
		{
			$this->session->set_flashdata('error_msg', "Sorry, this is not a campaign by rule.");
            redirect(admin_url("campaign"));
            exit();
		}
        else if ($rec_campaign->campaign_last_executed_date != "") {
            $this->session->set_flashdata('error_msg', "Sorry, this campaign is already executed and not allowed to change the paramter.");
            redirect(admin_url("campaign"));
            exit();
        }

        $merchants = $this->db->select('merchant_id,merchant_name,merchant_region_id')->order_by('merchant_name')->get('gl_merchant')->result();
        foreach($merchants as $m){
            $products = $this->db->get_where('gl_product',array('prod_merchant_id'=>$m->merchant_id))->result();
            $data['merchants'][$m->merchant_id] = array(
                'name'=>$m->merchant_name,
                'region'=>$m->merchant_region_id,
                'products' => $products
            );
        }
        $sponsor = $this->db->get_where('gl_categories',array('cat_name'=>'Sponsor'));
        if($sponsor->num_rows() > 0) {
            $sponsor_id = $sponsor->row()->cat_id;
            $this->db->join('gl_merchant','gl_merchant.merchant_id = gl_product.prod_merchant_id');
            $this->db->join('gl_product_inventory', 'gl_product_inventory.prod_id = gl_product.prod_id','left');
            $sponsored = $this->db->select('gl_product.*,gl_merchant.*,gl_product_inventory.current_qty,gl_product_inventory.change_qty')->from('gl_product')->where('prod_cat_id = ' . $sponsor_id )->get();
            if( $sponsored->num_rows() > 0 ) {
                $data['sponsor'] = $sponsored->result();
            }
        }

        $data['regions'] = $this->db->order_by('region_name')->get('gl_region')->result();

        $data['page_action'] = "edit";
        $data['page_title'] = "Edit Campaign (by rule)";
        $data['page_subtitle'] = "";
        if (isset($_POST['update_campaign'])) {
            $extra_rule = "";
            if ($_POST['campaign_type'] == 'Both')
                $extra_rule = "|callback_check_divisible";

            $this->form_validation->set_rules('campaign_name', 'Campaign Name', 'required');
            $this->form_validation->set_rules('campaign_type', 'Campaign type', 'required');
            $this->form_validation->set_rules('expiry_day', 'Expiry day', 'required|integer|is_natural_no_zero');
            $this->form_validation->set_rules('product_id[]', 'Product', 'required|integer|is_natural_no_zero');
            $this->form_validation->set_rules('voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);

            $cnt = isset($_POST['new_product_id']) ? count($_POST['new_product_id']) : 0;
            if ($cnt > 0) {
                $this->form_validation->set_rules('new_product_id[]', 'Product', 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('new_voucher_qty[]', 'Voucher Quantity', 'required|integer|is_natural_no_zero' . $extra_rule);
                $this->form_validation->set_rules('product_id[]', 'Product', 'integer|is_natural_no_zero');
                $this->form_validation->set_rules('voucher_qty[]', 'Voucher Quantity', 'integer|is_natural_no_zero' . $extra_rule);
            }

            if ($this->form_validation->run() == TRUE) {
                $this->campaign_m->update_campaign($camp_id);
                //update existing record intot the database
                $cnt = count($_POST['product_id']);
                for ($i = 0; $i < $cnt; $i++) {
                    $prod_id = $_POST['product_id'][$i];
                    $qty = $_POST['voucher_qty'][$i];
                    $details_id = $_POST['cam_details_id'][$i];
                    $type = 1;
                    if ($_POST['campaign_type'] == 'Both') {
                        $type = ($qty % 2 == 0) ? 1 : 0;
                    }
                    if ($prod_id > 0 && $qty > 0 && $type > 0) {

                        $this->campaign_m->update_campaign_details($details_id, $prod_id, $qty);
                    }
                }
                //insert for new details
                $cnt = count($_POST['new_product_id']);
                for ($i = 0; $i < $cnt; $i++) {
                    $prod_id = $_POST['new_product_id'][$i];
                    $qty = $_POST['new_voucher_qty'][$i];
                    $type = 1;
                    if ($_POST['campaign_type'] == 'Both') {
                        $type = ($qty % 2 == 0) ? 1 : 0;
                    }
                    if ($prod_id > 0 && $qty > 0 && $type > 0) {

                        $this->campaign_m->create_campaign_details($camp_id, $prod_id, $qty);
                    }
                }
                //
                $this->session->set_flashdata('msg', "Record successfully updated.");
                redirect(admin_url("campaign"));
                exit();
            }
        }
        $this->load->view(ADMIN_FOLDER . '/campaign_v', $data);
    }

    function remove_cam_details($id = 0) {
        $this->campaign_m->remove_campaign_details($id);
        die('success');
    }
    
    function check_divisible_invite() {
        if ( (count($_POST['invite_email']) % 2) == 0 ) {
            return true;
        } else {
            $this->form_validation->set_message('check_divisible_invite', 'Please add more email invites (Must be even on "Both" type voucher)');
            return false;
        }
    }
    
    function check_voucher_qty() {
        $qty = 0;
        if ( count($_POST['voucher_qty']) > 0 ) {
            foreach($_POST['voucher_qty'] as $v_qty) {
                $qty = $v_qty + $qty;
            }
            
            if (isset($_POST['invite_email'])) {
                if (count($_POST['invite_email']) > $qty ) {
                    $this->form_validation->set_message('check_voucher_qty', 'Voucher qty must be greater than or equal to the number of email that has been set.');
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        return true;
    }
    
    function check_sender_email($val) {
        if ( empty($val) ) {
            return true;
        }
        
        $res = $this->users_m->checkEmailExists($val);
        if ($res->num_rows() > 0) {
            return true;
        }
        
        $this->form_validation->set_message('check_sender_email', 'Sender email is not yet registered. Please choose another email.');
        return false;
    }    

    function check_divisible($qty) {
        if (($qty % 2) == 0) {
            return true;
        } else {
            $this->form_validation->set_message('check_divisible', 'Please enter even number for Both type of voucher.');
            return false;
        }
    }

    function rule_result($camp_id, $no_rule = 0) {
        $camp_id = (int) $camp_id;
        $res_people = $this->campaign_m->select_people_by_rule($camp_id, "");
        $num_rows = $res_people->num_rows();
        
        //Add custom invites if exists
        $query = $this->db->query("SELECT camp_inv_usr_id FROM gl_campaign_invite_users WHERE camp_campaign_id =".$camp_id);
        if ($query->num_rows() > 0 && $no_rule == 1) {
            $num_rows = $query->num_rows();
        }
        
        echo $num_rows;
    }

    function generate_voucher($camp_id) {
        $camp_id = (int) $camp_id;
        $res_voucher = $this->campaign_m->campaign_voucher_by_camp_id($camp_id);
        $num_rows = $res_voucher->num_rows();
        if ($num_rows > 0)
            echo "Sorry, The vouchers are already generated.";
        else {
            $result = $this->campaign_m->generate_voucher_by_camp_id($camp_id);
            echo "Voucher successfully generated.";
        }
    }

    function run_campaign($camp_id = 0, $rule_disabled = 0) {
        if($rule_disabled != 0) {
            $rule_disabled = TRUE;
        }
        else {
            $rule_disabled = FALSE;
        }
        
        $camp_id = (int) $camp_id;
        $res_available = $this->campaign_m->campaign_voucher_by_camp_id($camp_id, "AND camp_voucher_assign_status = 'UnAssigned'");
        $num_rows = $res_available->num_rows();
        if ($num_rows > 0) {
            $num_voucher = $this->campaign_m->run_campaign($camp_id, $rule_disabled);
            $this->session->set_flashdata('campaign_run_message', '<div class="alert alert-success"><i class="icon-ok"></i>Campaign ran successfully. ' . $num_voucher . ' voucher(s) has been distributed successfully to winners.</div>');
            redirect(ADMIN_FOLDER . '/campaign/voucher_details/' . $camp_id . '/' . $num_voucher);
        } else {
            $this->session->set_flashdata('campaign_run_message', '<div class="alert alert-info"><i class="icon-info"></i> Vouchers are exhausted, no more available for further distribution.</div>');
            redirect(ADMIN_FOLDER . '/campaign/details/' . $camp_id);
        }
        //$this->load->view(ADMIN_FOLDER.'/campaign_details_v',$data);	
    }

}

//class end
?>
