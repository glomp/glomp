<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Author: Shree
Date: July-26-2012
Desc: Regions/City
*/

class Regions extends CI_Controller
{
	private $pagination_per_page= 20;
	private $region_child = true;
	function Regions()
	{
		parent::__Construct();
		$this->load->library('admin_login_check');
		$this->load->model('regions_m');
        $this->load->model('merchant_m');  
		$this->load->library('lib_pagination');
	}
	
	function index($parent_region=0)
	{
		$parent_region=(int)($parent_region);
		//echo $this->session->flashdata('msg'); die();
		$data['page_action']="view";
		$data['page_title']="Manage Region : Glomp";
        $data['search_q']="";            
        $search_q=$this->input->get('search_q');
        if( !empty($search_q))
         {
            $data['search_q']=$search_q;            
         }
        
		if($parent_region>0)
		{
			$parent_region_detail=$this->regions_m->selectRegionByID($parent_region)->row();
			$data['breadcum']="&raquo; ".$parent_region_detail->region_name;
		}
		$data['parent_id'] = $parent_region;
		/*pagination starts*/
		if($this->uri->segment(5))
		$start=$this->uri->segment(5);
		else
		$start=0;
		$per_page= $this->pagination_per_page;
		$pagination_base_url=base_url(ADMIN_FOLDER."/regions/index/".$parent_region."/");
		$num_rows= $this->regions_m->selectRegions($parent_region,0,0,0,$search_q)->num_rows();
		$data['links']=$this->lib_pagination->DoPagination($pagination_base_url,$num_rows,$per_page,"5","",$search_q);
		$data['res_region']= $this->regions_m->selectRegions($parent_region,$start,$per_page,0,$search_q)->result();
		/*pagination ends*/
		
		if($this->session->flashdata('msg'))
		$data['msg'] = $this->session->flashdata('msg');
		if($this->session->flashdata('err_msg'))
		$data['error_msg']=$this->session->flashdata('err_msg');
		
		$data['region_child'] = $this->region_child;
		
		$this->load->view(ADMIN_FOLDER.'/regions_v',$data);
	}
	
	function publish($contentID, $contentStatus)
	{
		$contentID = (int)($contentID);
		if($this->regions_m->selectRegionByID($contentID,DEFAULT_LANG_ID)->num_rows()>0 && ($contentStatus=="Y" || $contentStatus=="N"))
		{	
			$contentStatus = ($contentStatus == 'Y')?'Active':'InActive';
			$this->regions_m->publish($contentID,$contentStatus);
			$this->session->set_flashdata('msg',"Status changed successfully");
		}
		redirect(ADMIN_FOLDER."/regions/index/");
		exit();
	}
	
	function deletePage($contentID=0)
	{
		$contentID = (int)($contentID);
		if($this->regions_m->selectRegionByID($contentID,DEFAULT_LANG_ID)->num_rows()>0)
		{
			$result = $this->regions_m->delete($contentID);
			if($result == 'TRUE')
				$this->session->set_flashdata('msg',"Record deleted successfully");
			else
				$this->session->set_flashdata('err_msg',"Sorry, this record is not allowed to delete or it has child record.");
		}
		redirect(ADMIN_FOLDER."/regions/index/");
		exit();
	}
	
	
	function addPage()
	{
		$data['page_action']="add";
		$data['page_title']="Manage Region : glomp";
		$data['region_child'] = $this->region_child;
		if(isset($_POST['addPage']))
		{
            $this->form_validation->set_rules('region_name', 'Region Name', 'required');            
            $this->form_validation->set_rules('region_outlet_prefix[]', 'Outlet Codes Prefix', 'required|integer|is_natural_no_zero|callback_check_if_three_digit_and_available|callback_check_if_array_has_dupes');
            /*
            $cnt = isset($_POST['new_region_outlet_prefix']) ? count($_POST['new_region_outlet_prefix']) : 0;
            if ($cnt > 0) {
                $this->form_validation->set_rules('new_region_outlet_prefix[]', 'Outlet Codes Prefix', 'required|integer|is_natural_no_zero|callback_check_if_three_digit');
            }            
            */
 			if($this->form_validation->run() == FALSE)
 			{
                $data['region_outlet_prefix']=$_POST['region_outlet_prefix'];
 				$this->load->view(ADMIN_FOLDER."/regions_v", $data);
 			}
 			else
 			{
            
                /*if (count($_POST['region_outlet_prefix']) > 0) {
                    foreach ($_POST['region_outlet_prefix'] as $p) {                        
                        echo $p."<br>";
                        
                    }
                    
                    
                }
                */
				$this->regions_m->addPage();
				$this->session->set_flashdata('msg',"Record successfully added.");
				redirect(ADMIN_FOLDER."/regions/index/");
				exit();
			}
		}
		else
		{
			$this->load->view(ADMIN_FOLDER."/regions_v", $data);
		}
	}
    function check_if_array_has_dupes() {
       
       if(isset($_POST['region_outlet_prefix']))
        $array=$_POST['region_outlet_prefix'];
       else if(isset($_POST['new_region_outlet_prefix']))
        $array=$_POST['new_region_outlet_prefix'];
       if( count($array) !== count(array_unique($array)) )
       {
            $this->form_validation->set_message('check_if_array_has_dupes', 'Each outlet code prefixes should be a unique number.');
            return false;       
       }
       else
       {
            return true;
       }
    }
    
    function check_if_three_digit_and_available($num) {            
        if ($num >=100 && $num<=999) {
            //check on table if this number is available
            if($this->merchant_m->get_this_prefix_code($num) >0)
            {
                $this->form_validation->set_message('check_if_three_digit_and_available', 'The outlet code prefix "'.$num.'" is already in use.');
                return false;
            }
            else
                return true;
        } else {
            $this->form_validation->set_message('check_if_three_digit_and_available', 'Please enter a 3 digit number for outlet code prefix.');
            return false;
        }
    }
	function remove_outlet_code_prefix($prefix_id, $prefix)
    {             
        if($this->merchant_m->check_if_prefix_in_use($prefix)>0)
        {
            //if yes.. do not delete.
            echo "in_use";
        }
        else
        {            
            //if no delete it
            $this->merchant_m->delete_prefix($prefix_id);
            echo "deleted";
        }

    }
	
	function editPage($contentID=0)
	{
		$contentID=(int)($contentID);
		$data['region_child'] = $this->region_child;
		
		if(!$this->regions_m->selectRegionByID($contentID)->num_rows()>0)
		{
			redirect(ADMIN_FOLDER."/regions/index/");
			exit();
		}
		
		
        $data['res_region']= $this->regions_m->selectRegionByID($contentID)->row();            
        $data['region_outlet_prefix']=$this->merchant_m->get_prefix_outlet_codes($contentID);
		$data['page_action']="edit";
		$data['page_title']="Manage Region : Glomp";
		if(isset($_POST['editPage']))
		{
			$this->form_validation->set_rules('region_name', 'Region Name', 'required');
            $this->form_validation->set_rules('new_region_outlet_prefix[]', 'Outlet Codes Prefix', 'required|integer|is_natural_no_zero|callback_check_if_three_digit_and_available|callback_check_if_array_has_dupes');
			if($this->form_validation->run() == FALSE)
			{
                $data['new_region_outlet_prefix']=$_POST['new_region_outlet_prefix'];
				$this->load->view(ADMIN_FOLDER."/regions_v", $data);
			}
			else
			{
				$this->regions_m->editPage($contentID);
				$this->session->set_flashdata('msg',"Record successfully updated.");
				redirect(ADMIN_FOLDER."/regions/index/".$this->input->post('parent_id'));
				exit();
			}
		}
		else
		{
			$this->load->view(ADMIN_FOLDER."/regions_v", $data);
		}
	}
	
}
?>