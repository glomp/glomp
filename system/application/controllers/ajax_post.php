<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//error_reporting(0);
class Ajax_post extends CI_Controller {

    private $INSTANCE = "gl_user";
    private $INSTANCE_Region = "gl_region";
    private $INSTANCE_OUTLET = "gl_outlet";
    private $INSTANCE_VOUCHER = "gl_voucher";
    private $INSTANCE_Product = "gl_product";
    private $CODE_INVITEEM = "INVITEEM";
    private $CODE_INVITEFB = "INVITEFB";
    private $INSTANCE_Activity = "gl_activity";
    private $INSTANCE_Invite = "gl_invite";
    private $INSTANCE_FRIENDS = "gl_friends";

    function __construct() {
        parent::__Construct();
        $this->load->helper(array('url'));
        $this->load->model('users_m');
        $this->load->model('invites_m');
        $this->load->model('merchant_m');
        $this->load->model('product_m');
        $this->load->model('regions_m');
        $this->load->model('login_m');
        $this->load->model('campaign_m');
        $this->load->library('custom_func');
        $this->load->model('payment_m');
        $this->load->model('user_account_m');
    }

    public function index() {
        $this->load->view('ajax_post_v.php');
    }
    
    function deactivateAccount()
    {
        $response= (object) array('status' => 'Error', 'message' => '');
        $user_id = $this->session->userdata('user_id');
        if($_POST)
        {
            
            
            
            $reason = $this->input->post('reason', TRUE);
            $comment = $this->input->post('comment', TRUE);
            
            
            $reason_list[1] = "I don't understand how to use glomp!";
            $reason_list[2] = "This is temporary. I'll be back.";
            $reason_list[3] = "I have a privacy concern.";
            $reason_list[4] = "I don't find glomp! useful.";
            $reason_list[5] = "I have another glomp! account.";
            $reason_list[6] = "Other, please explain further:";
            
            $insert_data = array(
				'user_id' 			=>$user_id,
				'reason' 		    => $reason_list[$reason],
				'comment' 			=> $comment,
				'date_deactivated' 	=>  date('Y-m-d H:i:s')
			);

			$this->db->insert('gl_feedback', $insert_data);
            
            
            $update_data = array(
				'user_status'               => 'Pending',
                'deactivated_status'        => 1
			);
			$where = array(
					'user_id' 		=>$user_id
					);
			$this->db->update('gl_user', $update_data, $where);
            $response->status='Deactivated';
            
            
            /*send email*/
            $this->load->library('email_templating');
            
            $user_fname = $this->session->userdata('user_fname');
            $user_lname = $this->session->userdata('user_lname');
            $user_email = $this->session->userdata('user_email');
            $link = site_url();
            $click_here ="<a href='$link'>here</a>";
            $vars = array(
                'template_name' => 'close_account',
                'lang_id' => 1,
                'to' => $user_email,
                'params' => array(
                    '[firstname_lastname]' => $user_fname.' '.$user_lname,
                    '[link]' => $click_here,
                )
            );
            $this->email_templating->config($vars);
            $this->email_templating->send();
            
            /*send email*/
            
        }
        echo json_encode($response);
    }

    function checkPoints() {
        $i = 0;
        $point_balance = 0;
        $status = 'error';
        if (isset($_GET['session_id']) && isset($_GET['token']) && isset($_GET['amount'])) {
            $session_id = $this->input->get('session_id');
            $token = $this->input->get('token');
            $amount = $this->input->get('amount');
            $user_id = $this->input->get('user_id');
            do {
                $i++;
                //usleep(2500000); //2.5 seconds 
                usleep(10000); //.1 seconds                                
                //check payment if received.
                $res = $this->payment_m->check_payment_if_received($user_id, $session_id, $token, $amount);
                if ($res->num_rows > 0) {
                    //$row=$$res->row();
                    $res_header = json_decode($this->user_account_m->user_summary($user_id));
                    $point_balance = $res_header->point_balance;
                    $status = 'success';
                    break;
                }
                if ($i == 40) {
                    break;
                }
            } while (true);
        }
        die("{status:'$status',points:'$point_balance'}");
    }

    function promo_code_save_to_user() {
        $user_id = $this->session->userdata('user_id');
        $promo_code = $this->input->post('promo_code', TRUE);
        $this->users_m->promo_code_save_to_user($promo_code, $user_id);

        $response = (object) array('status' => 'Error', 'message' => '', 'res_info_prod' => '', 'rule_desc' => '', 'user_promo_code' => '');
        $user_promo_code = json_decode($this->users_m->get_promo_code_status($user_id));
        $response->user_promo_code = ($user_promo_code);
        if (isset($user_promo_code->status) && ($user_promo_code->status == 'SuccessPrimary' || $user_promo_code->status == 'SuccessFollowUp')) {

            $sql = "SELECT voucher_product_id FROM gl_voucher WHERE voucher_id ='" . $user_promo_code->voucher_id . "'";
            $result = $this->db->query($sql);
            $result = $result->row();
            $res_info_prod = $this->product_m->productInfo_2($result->voucher_product_id);

            $response->res_info_prod = json_decode($res_info_prod);
            $response->status = 'OK';
        }

        $promo_rule_desc = '';
        if (isset($user_promo_code->campaign_id) && is_numeric($user_promo_code->campaign_id))
            $promo_rule_desc = $this->users_m->get_promo_rule_desc($user_promo_code->campaign_id);
        $response->rule_desc = $promo_rule_desc;
        //$this->clear_promo_from_user();
        echo json_encode($response);
    }

    public function clear_promo_from_user() {
        $user_id = $this->session->userdata('user_id');
        $sql = "UPDATE gl_user SET  user_sign_up_promo_code='' WHERE user_id='$user_id'";
        $this->db->query($sql);
        $this->db->query($sql);
        $response = (object) array('status' => 'OK', 'message' => '');
        echo json_encode($response);
    }

    public function searchForFriendsAjax() {
        $name = $this->input->post('name', TRUE);
        $email = $this->input->post('email', TRUE);
        $location_id = $this->input->post('location_id', TRUE);
        if (empty($name)) {
            $name = '';
        }
        $data['resFriendList'] = $this->users_m->searchForFriends($name, $email, $location_id);

        $this->load->view('includes/search_for_friend_ajax', $data);
    }

    function assignVoucherCheck() {
        $user_id = $this->session->userdata('user_id');
        $email = $this->input->post('email', TRUE);
        $voucher_id = $this->input->post('voucher_id');
        $sql = 'SELECT voucher_id 
                    FROM  gl_voucher
                    WHERE voucher_id = "' . $voucher_id . '" 
                        AND voucher_belongs_usser_id = "' . $user_id . '"';

        $res = $this->db->query($sql);

        if ($res->num_rows() > 0) {

            $response = array(
                'success' => FALSE,
                'errors' => array()
            );

            $response['email_exists'] = 0;

            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
            $this->form_validation->set_rules('location_id', 'Location', 'trim|required');

            $this->form_validation->run();
            //With error it will display the error message
            //Without error it will display empty
            $response['errors']['name'] = form_error('name');
            $response['errors']['email'] = form_error('email');
            $response['errors']['location_id'] = form_error('location_id');

            if ($this->form_validation->run() == FALSE) {
                echo json_encode($response);
                return;
            }

            $email_chk = $this->login_m->user_friend_and_exists($email);

            if ($email_chk->num_rows == 0) {
                //$response['email_exists'] = 1;
            } else {
                $response['email_exists'] = 1;
                $response['user']['fname'] = $email_chk->row()->user_fname;
                $response['user']['lname'] = $email_chk->row()->user_lname;
                $response['user']['id'] = $email_chk->row()->user_id;
                $response['user']['friend_id'] = $email_chk->row()->friend_user_friend_id;
            }
        } else {
            $response['errors']['voucher'] = 'Voucher is not valid';
            $response['success'] = FALSE;
            echo json_encode($response);
            return;
        }

        //Show glomp pop up menu
        $response['success'] = TRUE;
        echo json_encode($response);
    }

    function email_check($email) {
        $sql = 'SELECT 1 FROM gl_user WHERE user_email = "' . $email . '" AND user_id ="' . $this->session->userdata('user_id') . '"';
        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            $this->form_validation->set_message('email_check', 'You can\'t use your own email address.');
            return FALSE;
        } else {
            return TRUE;
        }

        //Exist show a pop up to tell that you should add a friend
//            $email_chk = $this->login_m->emailExist($email);
//            if ($email_chk == 0)
//                return TRUE;
//            else {
//                $this->form_validation->set_message('email_check', '1');
//                return FALSE;
//            }
    }

    function crop_update() {
        $response = (object) array('status' => 'Error', 'message' => '', 'thumb' => '', 'orig' => '');
        $user_id = (int) $this->session->userdata('user_id');
        $tempRes = $this->login_m->getPhotoOrig($user_id);
        if ($tempRes->num_rows() > 0) {
            $tempData = $tempRes->row();
            $user_photo = $tempData->user_profile_pic_orig;
            $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $user_photo;
            $response->orig = $user_photo;
            $src = "custom/uploads/users/" . $user_photo; //'demo_files/pool.jpg';

            $x1 = $_POST['x'];
            $y1 = $_POST['y'];
            $x2 = ($_POST['x'] + $_POST['w']);
            $y2 = ($_POST['y'] + $_POST['h']);


            $this->load->library('image_moo');
            $this->image_moo
                    ->load($src)
                    ->set_jpeg_quality(90)
                    ->crop($x1, $y1, $x2, $y2)
                    ->save("custom/uploads/users/temp/" . $new_photo_name);

            $response->status = 'OK';
            $response->message = 'x1=' . $x1 . ' y1=' . $y1 . ' x2=' . $x2 . ' y2=' . $y2;
            $response->thumb = $new_photo_name;


            if (isset($_POST['autoSave']) && $_POST['autoSave'] == 'true') {
                $user_photo = $new_photo_name;
                $user_id = $this->session->userdata('user_id');
                if (file_exists("custom/uploads/users/temp/" . $user_photo)) {
                    copy("custom/uploads/users/temp/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo);
                    //unlink("custom/uploads/users/temp/".$user_photo);
                    $this->login_m->updatePhoto($user_photo, $user_id);
                }
            }
        } else {
            $response->message = 'No image file';
        }
        echo json_encode($response);
    }

    function upload_temp() {
        //Change into language system
        $response = (object) array('status' => 'Error', 'message' => 'Sorry, this image is over 2MB. Please select a smaller image.', 'img' => '', 'w' => '0', 'h' => '0');

        if ($_FILES['user_photo']['tmp_name'] != "") {
            $config = array(
                'allowed_types' => 'jpeg|jpg|gif|png',
                'upload_path' => 'custom/uploads/users/temp/',
                'maximum_filesize' => 2
            );
            //  sanatize file name
            //     - remove extra spaces/convert to _,
            //     - remove non 0-9a-Z._- characters,
            //     - remove leading/trailing spaces
            //  check if under 2MB,
            //  check file extension for legal file types
            $safe_filename = preg_replace(
                    array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($_FILES['user_photo']['name']));
            $ext = strrchr($safe_filename, '.');
            $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $safe_filename[0];


            $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
            $uploadError = preg_match("/^Error/", $user_photo) ? $user_photo : NULL;
            if (empty($uploadError)) {

                try {
                    list($width, $height) = getimagesize('custom/uploads/users/temp/' . $user_photo);
                    $response->w = $width;
                    $response->h = $height;

                    $newName = mt_rand(1, 10000) . "_" . $user_photo;
                    if ($width > 1024) {
                        $this->resizeImg('custom/uploads/users/temp/' . $user_photo, 'custom/uploads/users/temp/' . $newName, 1024, 720, 3);
                    }
                    if ($height > 720)
                        $this->resizeImg('custom/uploads/users/temp/' . $user_photo, 'custom/uploads/users/temp/' . $newName, 1024, 720, 3);
                } catch (Exception $e) {
                    if (($this->config->item('log_threshold')) > 0) {
                        $this->log_message('error', $e->getMessage());
                    }
                }
                $response->status = 'OK';
                if (file_exists("custom/uploads/users/temp/" . $newName)) {
                    try {
                        list($width, $height) = getimagesize('custom/uploads/users/temp/' . $newName);

                        $response->w = $width;
                        $response->h = $height;
                        $response->img = $newName;
                    } catch (Exception $e) {
                        if (($this->config->item('log_threshold')) > 0) {
                            $this->log_message('error', $e->getMessage());
                        }
                    }
                }
                else
                    $response->img = $user_photo;
            }
            else {
                $response->message = $uploadError;
                //exit();
            }
        }//user photo temp check
        echo json_encode($response);
    }

    function crop_register() {
        $response = (object) array('status' => 'Error', 'message' => '', 'thumb' => '', 'orig' => '');
        if ($_FILES['user_photo']['tmp_name'] != "") {
            $config = array(
                'allowed_types' => 'jpeg|jpg|gif|png',
                'upload_path' => 'custom/uploads/users/',
                'maximum_filesize' => 2
            );
            //  sanatize file name
            //     - remove extra spaces/convert to _,
            //     - remove non 0-9a-Z._- characters,
            //     - remove leading/trailing spaces
            //  check if under 2MB,
            //  check file extension for legal file types
            $safe_filename = preg_replace(
                    array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($_FILES['user_photo']['name']));
            $ext = strrchr($safe_filename, '.');
            $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $safe_filename[0];


            $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
            $uploadError = preg_match("/^Error/", $user_photo) ? $user_photo : NULL;
            if (empty($uploadError)) {
                try {
                    list($width, $height) = getimagesize('custom/uploads/users/' . $user_photo);
                    $newName = mt_rand(1, 10000) . "_" . $user_photo;
                    if ($width > 1024) {
                        $this->resizeImg('custom/uploads/users/' . $user_photo, 'custom/uploads/users/' . $newName, 1024, 720, 3);
                    }
                    if ($height > 720)
                        $this->resizeImg('custom/uploads/users/' . $user_photo, 'custom/uploads/users/' . $newName, 1024, 720, 3);

                    if (file_exists("custom/uploads/users/" . $newName)) {
                        $user_photo = $newName;
                    } else {
                        $user_photo = $user_photo;
                    }
                } catch (Exception $e) {
                    if (($this->config->item('log_threshold')) > 0) {
                        $this->log_message('error', $e->getMessage());
                    }
                }


                $response->orig = $user_photo;
                //$this->login_m->updatePhoto($user_photo,$user_id);
                $targ_w = $_POST['w'];
                $targ_h = $_POST['h'];
                //$targ_w = $targ_h = 770;
                $jpeg_quality = 90;

                $src = "custom/uploads/users/" . $user_photo; //'demo_files/pool.jpg';
                /* $img_r = imagecreatefromjpeg($src);
                  $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

                  imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
                  $targ_w,$targ_h,$_POST['w'],$_POST['h']);



                  //header('Content-type: image/jpeg');
                  imagejpeg($dst_r,"custom/uploads/users/temp/".$new_photo_name.$ext,$jpeg_quality);
                 */

                $this->load->library('image_moo');
                $this->image_moo
                        ->load($src)
                        ->set_jpeg_quality(90)
                        ->crop($_POST['x'], $_POST['y'], ($_POST['x'] + $_POST['w']), ($_POST['y'] + $_POST['h']))
                        ->save("custom/uploads/users/temp/" . $new_photo_name . $ext);

                $response->status = 'OK';
                $response->thumb = $new_photo_name . $ext;

                if (isset($_POST['autoSave']) && $_POST['autoSave'] == 'true' && 1 == 0) {
                    $user_photo = $new_photo_name . $ext;
                    $user_id = $this->session->userdata('user_id');
                    if (file_exists("custom/uploads/users/temp/" . $user_photo) && is_numeric($user_id)) {
                        copy("custom/uploads/users/temp/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo);
                        //unlink("custom/uploads/users/temp/".$user_photo);
                        $this->login_m->updatePhoto($user_photo, $user_id);
                    }
                }

                //$this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
            } else {
                $response->message = $uploadError;
                //exit();
            }
        }//user photo temp check
        echo json_encode($response);
    }

    function resetRegister() {
        $array_items = array(
            'temp_user_fb_id' => '',
            'temp_fname' => '',
            'temp_lname' => '',
            'temp_mname' => '',
            'temp_email' => '',
            'temp_gender' => '',
            'temp_day' => '',
            'temp_months' => '',
            'temp_year' => ''
        );
        $this->session->unset_userdata($array_items);
    }

    function fb_log_invite() {
        $invitedIDs = explode(',', $this->input->post('invitedIDs'));
        $invitedNames = explode(',', $this->input->post('invitedNames'));
        $profile_id = (int) $this->session->userdata('user_id');
        $i = 0;
        foreach ($invitedIDs as $id) {
            $name = $invitedNames[$i];
            $i++;
            //Save to gl_invite table
            $invite_data = array(
                'invite_by_user_id' => $this->session->userdata('user_id'),
                'invite_through' => 'fb',
                'invite_invited_id' => $id,
                'invite_invited_name' => $name,
                'invite_date' => date('Y-m-d H:i:s'),
            );
            $insert_id = $this->invites_m->_insert($invite_data);
            //Save to gl_invite table            
            //log activity [invited_a_friend] through email
            $details = array(
                'invite_id' => $insert_id,
                'invite_facebook_id' => $id,
                'invited_name' => $name,
                'invited_through' => 'facebook'
            );
            $params = array(
                'user_id' => $this->session->userdata('user_id'),
                'type' => 'invited_a_friend',
                'details' => json_encode($details)
            );
            add_activity_log($params);
            //log activity [invited_a_friend] through email
        }
    }

    function li_log_invite() {
        $invitedIDs = explode(',', $this->input->post('invitedIDs'));
        $invitedNames = explode(',', $this->input->post('invitedNames'));
        $profile_id = (int) $this->session->userdata('user_id');
        $i = 0;
        foreach ($invitedIDs as $id) {
            $name = $invitedNames[$i];
            $i++;
            //Save to gl_invite table
            $invite_data = array(
                'invite_by_user_id' => $this->session->userdata('user_id'),
                'invite_through' => 'linkedIn',
                'invite_invited_id' => $id,
                'invite_invited_name' => $name,
                'invite_date' => date('Y-m-d H:i:s'),
            );
            $insert_id = $this->invites_m->_insert($invite_data);
            //Save to gl_invite table            
            //log activity [invited_a_friend] through email
            $details = array(
                'invite_id' => $insert_id,
                'invite_linked_id' => $id,
                'invited_name' => $name,
                'invited_through' => 'linkedIn'
            );
            $params = array(
                'user_id' => $this->session->userdata('user_id'),
                'type' => 'invited_a_friend',
                'details' => json_encode($details)
            );
            add_activity_log($params);
            //log activity [invited_a_friend] through email
        }
    }

    function inviteThisFriend() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $subQuery = "";
        //$countExistingUser = 1; 
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $location = $this->input->post('location');
        $message = $this->input->post('message');
        $location_id = (int) $this->input->post('location_id');
        $location_id = ($location != "") ? $location_id : 0;

        $this->load->library('email_templating');

        if ($name == "") {
            $response->message = 'Name is required.';
        } else if ($email == "") {
            $response->message = 'Email is required.';
        } else if ($location == "") {
            $response->message = 'Location is required.';
        } else {

            //$subQuery.= "AND (user_fname LIKE '%$name%' OR user_lname LIKE '%$name%' OR CONCAT(user_fname,' ',user_lname) LIKE '%$name%' OR user_email = '$email')";
            $res_existing_user = $this->users_m->searchForFriends($name);
            $countExistingUser = $res_existing_user->num_rows();

            $user_id_of_entered_email = $this->users_m->user_id_by_email($email);
            $is_freind = $this->users_m->is_friend($this->session->userdata('user_id'), $user_id_of_entered_email);
            if ($this->custom_func->emailValidation($email) == "false") {/// check email validation
                $response->message = $this->lang->line('invalid_email_address');
            } else if ($user_id_of_entered_email == $this->session->userdata('user_id')) {
                $response->message = $this->lang->line('sorry_you_entered_own_email_address');
            } else if ($is_freind > 0) {
                $response->message = $this->lang->line('already_in_friend_list');
            } else if ($countExistingUser == 0) {




                $send_again = "";
                ////send email$
                $rec_user = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
                $link = site_url('landing/invitePreFill/?name=' . $name . '&location=' . $location . '&email=' . $email);
                $vars = array(
                    'template_name' => 'notice_tosignup_nonmember',
                    'lang_id' => $rec_user->row()->user_lang_id,
                    'from_name' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                    'from_email' => SITE_DEFAULT_SENDER_EMAIL,
                    'reply_to' => $rec_user->row()->user_email,
                    'to' => $email,
                    'params' => array(
                        '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                        '[signup]' => "<a href='" . $link . "'>sign up</a>"
                    )
                );
                if (!empty($message)) {
                    $vars['template_name'] = 'notice_tosignup_nonmember_personal';
                    $vars['params'] = array(
                        '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                        '[signup]' => "<a href='" . $link . "'>sign up</a>",
                        '[personal message]' => $message
                    );
                    $invite_data['invite_added_message'] = $message;
                }


                //Save to gl_invite table
                $invite_data = array(
                    'invite_by_user_id' => $this->session->userdata('user_id'),
                    'invite_through' => 'email',
                    'invite_invited_email' => $email,
                    'invite_invited_location' => $location,
                    'invite_invited_name' => $name,
                    'invite_date' => date('Y-m-d H:i:s'),
                );

                $this->email_templating->config($vars);
                $this->email_templating->set_subject($rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname . ' has invited you to glomp!');
                $email_log_id = $this->email_templating->send();

                $invite_data['invite_email_log_id'] = $email_log_id;

                $this->invites_m->_insert($invite_data);

                //log activity [invited_a_friend] through email
                $details = array(
                    'invite_id' => $insert_id,
                    'invited_name' => $name,
                    'invited_email' => $email,
                    'invited_through' => 'email'
                );
                $params = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'type' => 'invited_a_friend',
                    'details' => json_encode($details)
                );
                add_activity_log($params);
                //log activity [invited_a_friend] through email




                if ($email_log_id) {
                    $response->message = $this->lang->line('invitation_email_msg_sent');
                    $response->status = 'Ok';
                }

                //$this->sendInviteEmail($email, $name);
            } else {

                $res_email_check = $this->users_m->email_exist2($email);
                $res_email_check->num_rows();
                if ($res_email_check->num_rows() > 0) {
                    $response->message = $this->lang->line('already_exists_see_potential_matches');
                } else {
                    $send_again = "true";
                    if ($this->custom_func->emailValidation($email) == "false") {/// check email validation
                        $$response->message = $this->lang->line('invalid_email_address');
                    } else {
                        ////send email$
                        $link = site_url('landing/invitePreFill?name=' . $name . '&location=' . $location . '&email=' . $email);
                        $rec_user = $this->users_m->user_info_by_id($this->session->userdata('user_id'));

                        //Save to gl_invite table
                        $invite_data = array(
                            'invite_by_user_id' => $this->session->userdata('user_id'),
                            'invite_through' => 'email',
                            'invite_invited_email' => $email,
                            'invite_invited_location' => $location,
                            'invite_invited_name' => $name,
                            'invite_date' => date('Y-m-d H:i:s'),
                        );

                        $vars = array(
                            'template_name' => 'notice_tosignup_nonmember',
                            'lang_id' => $rec_user->row()->user_lang_id,
                            'from_name' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                            'from_email' => SITE_DEFAULT_SENDER_EMAIL,
                            'reply_to' => $rec_user->row()->user_email,
                            'to' => $email,
                            'params' => array(
                                '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                                '[signup]' => "<a href='" . $link . "'>sign up</a>"
                            )
                        );

                        if (!empty($message)) {
                            $vars['template_name'] = 'notice_tosignup_nonmember_personal';
                            $vars['params'] = array(
                                '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                                '[signup]' => "<a href='" . $link . "'>sign up</a>",
                                '[personal message]' => $message
                            );
                            $invite_data['invite_added_message'] = $message;
                        }

                        $this->email_templating->config($vars);
                        $this->email_templating->set_subject($rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname . ' has invited you to glomp!');
                        $email_log_id = $this->email_templating->send();

                        $invite_data['invite_email_log_id'] = $email_log_id;

                        $insert_id = $this->invites_m->_insert($invite_data);

                        //log activity [invited_a_friend] through email
                        $details = array(
                            'invite_id' => $insert_id,
                            'invited_name' => $name,
                            'invited_email' => $email,
                            'invited_through' => 'email'
                        );
                        $params = array(
                            'user_id' => $this->session->userdata('user_id'),
                            'type' => 'invited_a_friend',
                            'details' => json_encode($details)
                        );
                        add_activity_log($params);
                        //log activity [invited_a_friend] through email

                        if ($email_log_id) {
                            $response->message = $this->lang->line('invitation_email_msg_sent');
                            $response->status = 'Ok';
                        }
                    }/// end of if($user_id_of_entered_email == $this->session->userdata('user_id'))
                }//// end of  if($this->custom_func->emailValidation($email)=="false")		
            }
        }

        echo json_encode($response);
    }

    function getCountryID() {
        $country = $this->input->post('country');
        $location_id = $this->regions_m->getCountryID($country);
        echo json_encode((int) $location_id);
    }

    function getGlompFBFrame() {
        $data["location_id"] = "1";
        $subQuery = "";
        //$countExistingUser = 1; 
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $country = $this->input->post('country');
        if (is_numeric($country)) {
            $location_id = $country;
        } else {
            $location_id = $this->regions_m->getCountryID($country);
        }
        //echo $location_id."asdasd";
        $location = $this->regions_m->getCountryName($location_id);
        //$location_id = (int) $this->input->post('location_id');
        $data['name'] = $name;
        $data['email'] = $email;
        $data['location'] = $location;
        $data['location_id'] = $location_id;
        $location_id = ($location != "") ? $location_id : 1;







        //This will be moved to different method
        $data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($location_id);
        //end
        $user_id = $this->session->userdata('user_id');
        $this->load->view('includes/getGlompFBFrame_v', $data);
    }

    function addThisFriend() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['fbID'])) {
            $fbID = $this->input->post('fbID');
            $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $fbID));
            if (($result->num_rows()) > 0) {
                $data = $result->row();
                $friendID = $data->user_id;
                //first of all verify user_id
                $res_num = $this->users_m->user_info_by_id($friendID);
                $rec_num = $res_num->num_rows();
                if ($rec_num > 0) {
                    $friend_added = $this->users_m->add_friend($this->session->userdata('user_id'), $friendID);
                    $response->status = $friendID;
                }
            }
        } else {
            $linkedInId = $this->input->post('linkedInId');
            $result = $this->db->get_where($this->INSTANCE, array('user_linkedin_id' => $linkedInId));
            if (($result->num_rows()) > 0) {
                $data = $result->row();
                $friendID = $data->user_id;
                //first of all verify user_id
                $res_num = $this->users_m->user_info_by_id($friendID);
                $rec_num = $res_num->num_rows();
                if ($rec_num > 0) {
                    $friend_added = $this->users_m->add_friend($this->session->userdata('user_id'), $friendID);
                    $response->status = $friendID;
                }
            }
        }

        echo json_encode($response);
    }

    function addThisFriendNotFB() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['id'])) {
            $fID = $this->input->post('id');
            $result = $this->db->get_where($this->INSTANCE, array('user_id' => $fID));
            if (($result->num_rows()) > 0) {
                $data = $result->row();
                $friendID = $data->user_id;
                //first of all verify user_id
                $res_num = $this->users_m->user_info_by_id($friendID);
                $rec_num = $res_num->num_rows();
                if ($rec_num > 0) {
                    $friend_added = $this->users_m->add_friend($this->session->userdata('user_id'), $friendID);
                    $response->status = $friendID;
                }
            }
        }
        echo json_encode($response);
    }

    function linkThisFacebookAccount() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['id']) && isset($_POST['email'])) {
            $user_id = $this->session->userdata('user_id');
            $user_fb_id = $_POST['id'];
            $user_email = $_POST['email'];
            $importOnly = $_POST['importOnly'];

            $tempResult = $this->db->get_where('gl_user', array('user_fb_id' => $user_fb_id));
            if ($importOnly == 'yes') {
                //Don't link just import photo
                $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                $this->login_m->updatePhoto($this->_get_facebook_photo($url), $user_id);
                $response->message = $this->lang->line('facebook_profile_photo_has_been_imported', 'Your Facebook profile photo has been imported.');
                $response->status = 'Ok';
            } else {
                if ($tempResult->num_rows() == 0) {
                    //Link FB ID
                    $this->login_m->user_fb_login_update_account($user_id);
                    $response->status = 'Ok';
                    $response->message = $this->lang->line('facebook_now_linked','Your Facebook account is now linked. Invite your Facebook friends now.');
                } else {
                    $response->message = $this->lang->line('facebook_account_already_connected', 'This Facebook account is already connected to another glomp! account.');
                }
            }


/*            
TODO: DELETE COMMENT AFTER TEST
$tempResult = $this->login_m->check_if_fb_user_is_registered_fb($user_fb_id);
            if ($tempResult->num_rows() == 0) {
                $tempResult = $this->login_m->check_if_fb_user_is_registered_email($user_email);
                if ($tempResult->num_rows() == 0) {
                    $response->status = 'Ok';
                    $response->message = $this->lang->line('facebook_now_linked','Your Facebook account is now linked. Invite your Facebook friends now.');
                    $user_id = $this->session->userdata('user_id');
                    $this->login_m->user_fb_login_update_account($user_id);
                    
                    if ($importOnly == 'yes') {
                        $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                        $this->login_m->updatePhotoOrig($this->_get_facebook_photo($url), $user_id);
                    }
                } else {

                    $resData = $tempResult->row();
                    if ($this->session->userdata('user_id') == $resData->user_id) {
                        //this account is already registered to you.					
                        $response->status = 'Ok';
                        $response->message = $this->lang->line('facebook_now_linked', 'Your Facebook account is now linked. Invite your Facebook friends now.');
                        $this->login_m->user_fb_login_update_account($resData->user_id);

                        if ($importOnly == 'yes') {
                            $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                            $this->login_m->updatePhotoOrig($this->_get_facebook_photo($url), $resData->user_id);
                        }
                    } else {
                        $response->message = $this->lang->line('facebook_account_already_connected', 'This Facebook account is already connected to another glomp! account.');
                    }
                }
            } else {
                $resData = $tempResult->row();
                if ($this->session->userdata('user_id') == $resData->user_id) {
                    //this account is already registered to you.										
                    if ($importOnly == 'yes') {
                        $url = "http://graph.facebook.com/$user_fb_id/picture?width=140&height=140&redirect=true";
                        $response->status = 'Ok';
                        $response->data = 'importOnly';
                        $response->message = $this->lang->line('facebook_profile_photo_has_been_imported', 'Your Facebook profile photo has been imported.');
                        
                        $this->login_m->updatePhoto($this->_get_facebook_photo($url), $resData->user_id);
                    }//import only yes
                    else {
                        $response->message = $this->lang->line('facebook_account_already_connected_your_account', 'This Facebook account is already connected to your glomp! account.');
                    }
                } else {
                    $response->message = $this->lang->line('facebook_account_already_connected', 'This Facebook account is already connected to another glomp! account.');
                }
            }*/
            //memcached clear
            $userID = $this->session->userdata('user_id');
            $params = array(
                'specific_names'
                => array(
                    'user_info_by_id_' . $userID,
                    'getUserFname_' . $userID,
                    'user_short_info_' . $userID,
                )
            );
            delete_cache($params);
            //memcached clear
        }
        echo json_encode($response);
    }

    function linkedIn_callback() {
        $this->load->library('linkedin_api');
        $redirect_li = $this->session->userdata('redirect_li');
        $data = array(
            'values' => array(),
            'redirect_window' => '',
        );

        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
//                print $_GET['error'] . ': ' . $_GET['error_description'];
//                exit;
            if (!empty($redirect_li)) {
                //clear the redirect
                $this->session->unset_userdata('redirect_li');
                //redirect where the page sets
                $redirect_li = preg_replace('/\?.*/', '', $redirect_li);
                return redirect($redirect_li);
            }
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            $state_li = $this->session->userdata('state_li');
            if ($state_li == $_GET['state']) {
                // Get token so you can make API calls
                $this->linkedin_api->getAccessToken();
                //Will go here if return from linkedIn page
                if (!empty($redirect_li)) {
                    //clear the redirect
                    $this->session->unset_userdata('redirect_li');

                    //redirect where the page sets
                    //$data['redirect_window'] = $redirect_li;
                    //echo $redirect_li. '?js_func_li='.$_POST['js_func_li'];
                    return redirect($redirect_li);
                    //echo json_encode($data);
                    //exit;
                }
            } else {
                // CSRF attack? Or did you mix up your states?
                exit;
            }
        } else {
            $expires_at_li = $this->session->userdata('expires_at_li');
            $access_token_li = $this->session->userdata('access_token_li');

            if ((empty($expires_at_li)) || (time() > $expires_at_li)) {
                // Token has expired, clear the state
                $this->linkedin_api->clear_session();
            }
            if (empty($access_token_li)) {
                // Start authorization process
                $url = $this->linkedin_api->getAuthorizationCode();
                //Sets where the page would return after authentication
                if (isset($_POST['redirect_li'])) {
                    if (parse_url($_POST['redirect_li'], PHP_URL_QUERY)) {
                        $concat_querystring = '&';
                    } else {
                        $concat_querystring = '?';
                    }

                    $redirect_li = $this->session->set_userdata('redirect_li', $_POST['redirect_li'] . $concat_querystring . 'js_func_li=' . $_POST['js_func_li']);
                }
                $data['redirect_window'] = $url;
                echo json_encode($data);
                exit;
            }
        }

        if (!empty($_POST['mail'])) {
            $res = $this->linkedin_api->send('POST', $_POST['api'], $_POST['body']);
            $data['values'] = array($res);
            //print_r($data);
            echo json_encode($data);
            exit;
        }

        $res = $this->linkedin_api->fetch('GET', $_POST['api']);
        $data['values'] = array($res);
        //print_r($data);
        echo json_encode($data);
    }

    /**
     * linkThisLinkedInAccount
     * 
     * Link 3rd party app
     * #TODO: MAKE THIS GENERIC FUNCTION
     * 
     * @access public
     * @param array
     * @return string
     */
    function linkThisLinkedInAccount() {
        $response = array('status' => 'Error', 'message' => '', 'data' => '');
        $response['status'] = 'Error';

        if (isset($_POST['id']) && isset($_POST['email'])) {
            $user_linkedin_id = $_POST['id'];
            $user_email = $_POST['email'];
            $url = $_POST['pic'];
            $importOnly = $_POST['importOnly'];

            $where = array('user_linkedin_id' => $user_linkedin_id, 'user_status' => 'Active');
            $tempResult = $this->users_m->get_record(array('where' => $where));

            if ($tempResult == FALSE) {
                $where = array('user_email' => $user_email, 'user_status' => 'Active');
                $tempResult = $this->users_m->get_record(array('where' => $where));

                if ($tempResult == FALSE) {
                    $response['status'] = 'Ok';
                    $response['message'] = $this->lang->line('linkedin_now_linked','Your LinkedIn account is now linked. Invite your LinkedIn friends now.');

                    $user_id = $this->session->userdata('user_id');

                    $up_data = array(
                        'user_linkedin_id' => $user_linkedin_id
                    );

                    $where = array(
                        'user_id' => $user_id
                    );

                    $this->users_m->_update($where, $up_data);

                    if ($importOnly == 'yes') {

                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        curl_exec($ch);
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);

                        unlink($localfile);
                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }


                        $data = json_decode($filedata, true);
                        $url = $data["data"]["url"];

                        /* Extract the filename */
                        $filename = substr($url, strrpos($url, '/') + 1);
                        $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                        $ext = strrchr($safe_filename, '.');
                        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                        /* Save file wherever you want */
                        $user_photo = $new_photo_name . $ext;

                        //get data
                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        curl_exec($ch);
                        //get mime type incase there is no file extension
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);
                        unlink($localfile);
                        //get data

                        file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                        $user_photo = $new_photo_name . $ext;

                        $this->login_m->updatePhoto($user_photo, $user_id);
                        $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                        $user_photo_orig = $user_photo;
                        $this->login_m->updatePhotoOrig($user_photo_orig, $user_id);
                    }
                } else {
                    if ($this->session->userdata('user_id') == $tempResult['user_id']) {
                        //this account is already registered to you.					
                        $response['status'] = 'Ok';
                        $response['message'] = $this->lang->line('linkedin_now_linked','Your LinkedIn account is now linked. Invite your LinkedIn friends now.');
                        $up_data = array(
                            'user_linkedin_id' => $user_linkedin_id
                        );

                        $where = array(
                            'user_id' => $tempResult['user_id']
                        );

                        if ($importOnly == 'yes') {
                            $response['status'] = 'Ok';
                            $response['data'] = 'importOnly';
                            $response['message'] = $this->lang->line('linkedin_profile_photo_has_been_imported','Your LinkedIn profile photo has been imported.');
                        } else {
                            $this->users_m->_update($where, $up_data);
                        }

                        if ($importOnly == 'yes') {
                            /* Extract the filename */
                            $filename = substr($url, strrpos($url, '/') + 1);
                            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                            $ext = strrchr($safe_filename, '.');
                            $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                            /* Save file wherever you want */
                            $user_photo = $new_photo_name . $ext;

                            //get data
                            $localfile = 'custom/uploads/users/mytempfilename.ext';

                            // Let's go cURLing...
                            $ch = curl_init($url);
                            $fp = fopen($localfile, 'w');

                            curl_setopt($ch, CURLOPT_FILE, $fp);
                            curl_setopt($ch, CURLOPT_HEADER, 0);

                            curl_exec($ch);
                            //get mime type incase there is no file extension
                            $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                            curl_close($ch);
                            fclose($fp);

                            // Get the data into memory and delete the temp file
                            $filedata = file_get_contents($localfile);
                            unlink($localfile);
                            //get data

                            if (empty($ext)) {
                                //get file extension base on mime type
                                $ext = mime_to_file_extension($mime_type);
                            }

                            file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);
                            $user_photo = $new_photo_name . $ext;

                            $this->login_m->updatePhoto($user_photo, $tempResult['user_id']);
                            $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                            $user_photo_orig = $user_photo;
                            $this->login_m->updatePhotoOrig($user_photo_orig, $tempResult['user_id']);
                        }
                    } else {
                        $response['message'] = $this->lang->line('linkedin_account_already_connected','This LinkedIn account is already connected to another glomp! account.');
                    }
                }
            } else {
                if ($this->session->userdata('user_id') == $tempResult['user_id']) {
                    //this account is already registered to you.										
                    if ($importOnly == 'yes') {
                        $response['status'] = 'Ok';
                        $response['data'] = 'importOnly';
                        $response['message'] = 'Your LinkedIn profile photo has been imported.';

                        /* Extract the filename */
                        $filename = substr($url, strrpos($url, '/') + 1);
                        $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                        $ext = strrchr($safe_filename, '.');
                        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                        /* Save file wherever you want */
                        $user_photo = $new_photo_name . $ext;

                        //get data
                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        //get mime type incase there is no file extension
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);
                        unlink($localfile);
                        //get data

                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }

                        file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                        $user_photo = $new_photo_name . $ext;

                        $this->login_m->updatePhoto($user_photo, $tempResult['user_id']);
                        $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);

                        $user_photo_orig = $user_photo;
                        $this->login_m->updatePhotoOrig($user_photo_orig, $tempResult['user_id']);
                    }//import only yes
                    else {
                        $response['message'] = $this->lang->line('linkedin_account_already_connected_your_account','This LinkedIn account is already connected to your glomp! account.');
                    }
                } else {
                    $response['message'] = $this->lang->line('linkedin_account_already_connected','This LinkedIn account is already connected to another glomp! account.');
                }
            }

            //memcached clear
            $userID = $this->session->userdata('user_id');
            $params = array(
                'specific_names'
                => array(
                    'user_info_by_id_' . $userID,
                    'getUserFname_' . $userID,
                    'user_short_info_' . $userID,
                )
            );
            delete_cache($params);
            //memcached clear
        }

        echo json_encode($response);
    }

    function getOriginalImage() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '', 'w' => '0', 'h' => '0');
        $user_id = (int) $this->session->userdata('user_id');
        $tempRes = $this->login_m->getPhotoOrig($user_id);
        if ($tempRes->num_rows() > 0) {
            $tempData = $tempRes->row();
            if ($tempData->user_profile_pic_orig != null || $tempData->user_profile_pic_orig != '') {
                $response->data = $tempData->user_profile_pic_orig;
                $user_photo = $tempData->user_profile_pic_orig;
                try {
                    list($width, $height) = getimagesize('custom/uploads/users/' . $user_photo);
                    $response->w = $width;
                    $response->h = $height;
                    $response->status = 'Ok';
                } catch (Exception $e) {
                    if (($this->config->item('log_threshold')) > 0) {
                        $this->log_message('error', $e->getMessage());
                    }
                    $response->message = 'An unknown error occured.';
                }
            } else {
                $response->message = 'No image file.';
            }
        } else {
            $response->message = 'No image file.';
        }
        echo json_encode($response);
    }

    function setTourStat() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['tour_stat'])) {
            $tour_stat = $this->input->post('tour_stat');
            $profile_id = (int) $this->session->userdata('user_id');
            $sql = "UPDATE   " . $this->INSTANCE . " 		
			SET user_start_tour='" . $tour_stat . "' WHERE user_id='" . $profile_id . "' LIMIT 1";
            $result = $this->db->query($sql);
            $response->status = 'Ok';
        }
        echo json_encode($response);
    }

    function checkThisFriendListStatus() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['idList'])) {
            $response->status = 'Ok';
            $idList = explode(',', $this->input->post('idList'));
            $profile_id = (int) $this->session->userdata('user_id');
            foreach ($idList as $id) {
                //$rowIDs[$i]=array('fbid' 		=> $id, $id	=> 1);0 is not registered,  1-is registred, 2 is your friend				
                $status = "-1:-1";
                $fbID = $id;
                ///get user id of this fbID
                $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $fbID));
                if (($result->num_rows()) > 0) {
                    $data = $result->row();
                    $thisUserID = $data->user_id;
                    //check if this is your friend
                    $sql = "SELECT * FROM  " . $this->INSTANCE_FRIENDS . " 		
					WHERE (friend_user_id='" . $profile_id . "' AND friend_user_friend_id='" . $thisUserID . "') OR		
					(friend_user_id='" . $thisUserID . "' AND friend_user_friend_id='" . $profile_id . "') LIMIT 1";
                    $result = $this->db->query($sql);
                    if (($result->num_rows()) > 0) {
                        $status = $thisUserID . ":" . $thisUserID;
                    } else {
                        $status = "0:" . $thisUserID;
                    }
                }
                $name = 'fbid_' . $id;
                $response->$name = $status;
            }
            //$response->data=$rowIDs;
        }
        echo json_encode($response);
    }
	
	function checkThisFriendListStatus_new() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['idList'])) {
            $response->status = 'Ok';
            $idList = explode(',', $this->input->post('idList'));
            $profile_id = (int) $this->session->userdata('user_id');
            foreach ($idList as $id) {
                $status = "not_registered";
				$fb_name = 'none';
                $fbID = $id;
				$user_id = '';
                $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $fbID));
                if (($result->num_rows()) > 0)
				{
					$status =  'registered';
					$result = $result->row();
					$user_id = $result->user_id;
					$fb_name = $result->user_fname.' '.$result->user_lname;
                }
                $name = 'fbid_' . $id;
				$fbid__name = 'fbid__name_' . $id;
				$fbid_user_id = 'fbid_user_id' . $id;
				
                $response->$name = $status;
				$response->$fbid__name = $fb_name;
				$response->$fbid_user_id = $user_id;
            }
            //$response->data=$rowIDs;
        }
        echo json_encode($response);
    }

    function checkThisFriendListStatus_2() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['idList'])) {
            $response->status = 'Ok';
            $idList = $this->input->post('idList');
            $profile_id = (int) $this->session->userdata('user_id');
            foreach ($idList as $id) {
                $status = "-1:-1";
                $appID = $id;
                ///get user id of this fbID
                $result = $this->db->get_where($this->INSTANCE, array($this->input->post('field') => $appID));

                if (($result->num_rows()) > 0) {
                    $data = $result->row();
                    $thisUserID = $data->user_id;
                    //check if this is your friend
                    $sql = "SELECT * FROM  " . $this->INSTANCE_FRIENDS . " 		
					WHERE (friend_user_id='" . $profile_id . "' AND friend_user_friend_id='" . $thisUserID . "') OR		
					(friend_user_id='" . $thisUserID . "' AND friend_user_friend_id='" . $profile_id . "') LIMIT 1";
                    $result = $this->db->query($sql);
                    if (($result->num_rows()) > 0) {
                        $status = $thisUserID . ":" . $thisUserID;
                    } else {
                        $status = "0:" . $thisUserID;
                    }
                }
                $name = 'linkedInId_' . $id;
                $response->$name = $status;
            }
            //$response->data=$rowIDs;
        }
        echo json_encode($response);
    }

    function checkThisPersonStatus() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if (isset($_POST['fbID'])) {
            $profile_id = (int) $this->session->userdata('user_id');
            $fbID = $this->input->post('fbID');

            ///get user id of this fbID
            $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $fbID));
            if (($result->num_rows()) > 0) {
                $data = $result->row();
                $thisUserID = $data->user_id;

                //check if this is your friend
                $sql = "SELECT * FROM  " . $this->INSTANCE_FRIENDS . " 		
				WHERE (friend_user_id='" . $profile_id . "' AND friend_user_friend_id='" . $thisUserID . "') OR		
				(friend_user_id='" . $thisUserID . "' AND friend_user_friend_id='" . $profile_id . "') LIMIT 1";
                $result = $this->db->query($sql);
                if (($result->num_rows()) > 0) {
                    $response->status = $thisUserID;
                } else {
                    $response->status = 0;
                }
            } else {
                $response->status = -1;
            }
            echo json_encode($response);
        }
    }

    function saveInvitedFriendsFb() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        if (isset($_POST['request']) && isset($_POST['to'])) {
            $profile_id = (int) $this->session->userdata('user_id');
            $invite_added_message = $this->input->post('personalMessage');
            $fb_reques_id = $this->input->post('request');
            foreach ($this->input->post('to') as $id) {
                $data = array(
                    'invite_by_user_id' => $profile_id,
                    'invite_through' => "fb",
                    'invite_invited_id' => $id,
                    'invite_invited_fullname' => '',
                    'invite_added_message' => $invite_added_message,
                    'invite_date' => date('Y-m-d H:i:s')
                );
                $this->db->insert($this->INSTANCE_Invite, $data);

                $log_details = $this->session->userdata('user_name') . " invited a friend on Facebook to join Glomp!";

                $data = array(
                    'log_user_id' => $profile_id,
                    'log_title' => ("Facebook Invite"),
                    'log_details' => $log_details,
                    'log_timestamp' => date('Y-m-d H:i:s'),
                    'log_ip' => $_SERVER['REMOTE_ADDR'],
                    'log_ac_txn_trancode' => ($this->CODE_INVITEFB)
                );
                $this->db->insert($this->INSTANCE_Activity, $data);
                echo "OK";
            }
        }
    }

    function getCountryList() {
        $res = $this->regions_m->getAllCountryDropdown(0);
        echo $res;
    }

    function loadLocation($loc = "") {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $list = "";
        $res = $this->regions_m->selectRegionKeywords($loc);
        if ($res->num_rows() > 0) {
            $list.="<ul>";
            foreach ($res->result() as $rec) {
                $list.="<li><a href='javascript:void(0);' title='" . $rec->region_id . "'>" . $rec->region_name . "</a>";
            }
            $list.="</ul>";
        } else {
            $list = "No match found";
        }
        die($list);
    }

    public function checkIfUserHasAnExistingAcct() {
        if (isset($_POST['fbID']) && is_numeric($_POST['fbID'])) {
            $response = (object) array('status' => 'Error', 'message' => '', 'data' => '', 'data_status' => '',);
            $fbID = $_POST['fbID'];
            $result = $this->db->get_where($this->INSTANCE, array('user_fb_id' => $fbID));
            $response->status = 'Ok';
            if ($result->num_rows() == 1) {
                $temp = $result->row();
                $response->data_status = $temp->user_status;
            }
            $response->data = $result->num_rows();
            echo json_encode($response);
        }
    }

    function checkIfIntegrated() {
        $where = array($_POST['field'] => $_POST['id'], 'user_status' => 'Active');
        $user = $this->users_m->get_record(array('where' => $where));

        if ($user == FALSE) {
            echo json_encode(FALSE);
            return;
        }
        echo json_encode(TRUE);
        return;
    }

    /**
     * processIntegratedApp
     * 
     * Process/login integrated app
     * 
     * POST format: 
     *  'mode' => 'value', #value is integration mode ex: linkedin.
     * 
     * 
     * @access public
     * @param array
     * @return bool
     */
    public function processIntegratedApp() {
        echo json_encode($this->login_m->login_using_social_app($_POST['mode']));
    }

    public function checkIfLocalityIsApplicable() {
        if (isset($_POST['country_id']) && is_numeric($_POST['country_id'])) {
            $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
            $country_id = $_POST['country_id'];
            $result = $this->db->get_where($this->INSTANCE_Region, array('region_id' => $country_id));
            $response->status = 'Ok';
            $temp = $result->row();
            $region_name = $temp->region_name;
            if ($region_name == 'Philippines')
                $response->data = 1;
            else
                $response->data = 0;
            echo json_encode($response);
        }
    }

    public function getMerchantReportDetails() {
        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $outletID = (int) $_POST['outletID'];
        $dateFrom = date("Y-m-d H:i:s", strtotime($_POST['dateFrom'] . "00:00:00"));
        $dateTo = date("Y-m-d H:i:s", strtotime($_POST['dateTo'] . "23:59:59"));
        $whereOutlet = '';
        if ($outletID != 0) {
            $whereOutlet.=' voucher_outlet_id=' . $outletID . ' AND ';
        }
        $sql = "SELECT " . $this->INSTANCE_VOUCHER . ".*, " . $this->INSTANCE_OUTLET . ".outlet_name,
		" . $this->INSTANCE_Product . ".prod_name," . $this->INSTANCE_Product . ".prod_point		
		FROM " . $this->INSTANCE_VOUCHER . " 
		JOIN " . $this->INSTANCE_OUTLET . " ON " . $this->INSTANCE_VOUCHER . ".voucher_outlet_id = " . $this->INSTANCE_OUTLET . ".outlet_id
		JOIN " . $this->INSTANCE_Product . " ON " . $this->INSTANCE_VOUCHER . ".voucher_product_id = " . $this->INSTANCE_Product . ".prod_id
		WHERE voucher_status= 'Redeemed' AND
		voucher_merchant_id= '" . $this->session->userdata('merchant_id') . "' AND 
		" . $whereOutlet . "
		(voucher_redemption_date between '" . $dateFrom . "' AND '" . $dateTo . "') 
		ORDER BY voucher_redemption_date";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $rows[] = (object) array(
                            'redemption_date' => (date("d-M-y", strtotime($row->voucher_redemption_date))),
                            'redemption_time' => (date("h:i A", strtotime($row->voucher_redemption_date))),
                            'outlet' => $row->outlet_name,
                            'voucher_num' => $row->voucher_transactionID,
                            'product' => $row->prod_name,
                            'verification_num' => $row->voucher_code,
                            'purchase_date' => (date("d-M-y", strtotime($row->voucher_purchased_date))),
                            'purchase_time' => (date("h:i A", strtotime($row->voucher_purchased_date))),
                            'amount' => $row->voucher_prod_cost
                );
            }
            $response->status = 'Ok';
            $response->data = $rows;
        }

        echo json_encode($response);
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'exact', $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    function save_system_log() {
        $this->load->model('system_log_m');
        $this->load->library('user_agent');

        $logs = $_POST['logs'];

        foreach ($logs as $log) {
            $log = json_decode($log);

            if (isset($log->from_url) && $log->from_url != '') {
                $this->session->set_userdata('facebok_callback_from_url', $log->from_url);
                $this->session->set_userdata('facebok_callback_what_to_do', $log->from_what_to_do);
            }
            $data = array(
                'log_type' => $log->type,
                'log_event' => $log->event,
                'log_message' => $log->log_message,
                'browser_info' => $this->agent->agent_string(),
                'log_url' => $log->url,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'log_details' => $log->response,
                'log_timestamp' => date('Y-m-d H:i:s')
            );

            $this->system_log_m->_insert($data);
        }
    }

    /**
     * check_fb_match
     * 
     * - Check if account is connected if not save it and continue
     * - If already connected check whether it FB ID saved on glomp! database is match to the fetch FB ID
     * 
     * @access public
     * @param string
     * @return string
     */
    public function check_fb_match() {
        $fetch_FB_ID = $_POST['fetch_fb_id'];
        $user_id = $this->session->userdata('user_id');
        $user_short_info = $this->user_account_m->getUserFname($user_id);

        if (empty($user_short_info->user_fb_id)) {
            //get back to auto link
            echo json_encode(array('check_fb_id' => 'prompt'));
            return;
        }

        if ($user_short_info->user_fb_id == $fetch_FB_ID) {
            echo json_encode(array('check_fb_id' => 'true'));
            return;
        }

        echo json_encode(array('check_fb_id' => 'false'));
    }

    /**
     * auto_link_fb
     * 
     * - Check if account is connected if not save it and continue
     * 
     * @access public
     * @param string
     * @return string
     */
    public function auto_link_fb() {
        $fetch_FB_ID = $_POST['fetch_fb_id'];
        $user_id = $this->session->userdata('user_id');
        $user_short_info = $this->user_account_m->getUserFname($user_id);
        //When exist or not 
        $where = 'user_fb_id = "' . $fetch_FB_ID . '" AND user_id <> "' . $user_id . '"';
        if ($this->users_m->exists($where)) {
            echo json_encode(array('success' => 'false'));
            return;
        }

        if (empty($user_short_info->user_fb_id)) {
            $where = 'user_id = "' . $user_id . '"';
            $this->users_m->_update($where, array(
                'user_fb_id' => $fetch_FB_ID
            ));

            //memcached clear
            $params = array(
                'specific_names'
                => array(
                    'user_info_by_id_' . $user_id,
                    'getUserFname_' . $user_id,
                    'user_short_info_' . $user_id,
                )
            );
            delete_cache($params);
            //memcached clear
            echo json_encode(array('success' => 'true'));
            return;
        }

        echo json_encode(array('success' => 'true'));
        return;
    }

    /**
     * _get_facebook_photo
     * 
     * Grab FB photo
     * TODO: Create a class or helper
     * 
     * @access private
     * @param none
     * @return string
     */
    function _get_facebook_photo($url) {
        $filename = substr($url, strrpos($url, '/') + 1);
        $upload_folder = FCPATH.  'custom/uploads/users/';
        $safe_filename = preg_replace(
                array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $safe_filename[0];
        file_put_contents($upload_folder . $new_photo_name . '.jpg', file_get_contents($url));

        $this->load->library('image_lib');

        $this->image_lib->initialize(array(
            'image_library' => "GD2",
            'source_image' => $upload_folder . $new_photo_name . '.jpg',
            'quality' => '80%',
            'new_image' => $upload_folder . 'thumb/' . $new_photo_name . '.jpg',
            'width' => 140,
            'height' => 140
        ));

        $this->image_lib->resize();

        return $new_photo_name . '.jpg';
    }

}

//eoc
