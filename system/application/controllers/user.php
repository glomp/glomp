<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;

    function __construct() {
        parent::__Construct();
        $this->load->library('user_login_check');
        $this->load->model('cmspages_m');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->model('merchant_m');
        $this->load->model('regions_m');
        $this->load->model('login_m');
        $this->load->model('send_email_m');
        $this->load->model('invites_m');        
        $this->load->library('email_templating');
        $this->load->library('custom_func');		
    }

    function index() {
        $this->dashboard();
    }
    

    function redirectMe() {
        $friend_id = $this->session->userdata('redirect_friends_profile_id');
        $redirect_noti = $this->session->userdata('redirect_notification');
        if (! empty($friend_id)) {
            //When email recieve from added as a friend, then redirect to friend profile page
            $url = site_url('profile/view/'.$friend_id);
            //remove session when redirected.
            $this->session->unset_userdata('redirect_friends_profile_id');
            return redirect($url);
        }
        
        if (! empty($redirect_noti)) {
            //When email recieve then clicked on unsubscribe link redirect to settings page
            $url = site_url('user/update/?show_notification=1#email');
            //remove session when redirected.
            $this->session->unset_userdata('redirect_notification');
            return redirect($url);
        }
    }
    function getMoreStories()
    {
        if($_POST && isset($_POST['start_count']))
        {
            $start_count=$this->input->post('start_count');
            $user_id = $this->session->userdata('user_id');
            $glomp_buzz = $this->users_m->glomp_buzz($user_id, $start_count, 5);
            $data['glomp_buzz'] = $glomp_buzz;
            $data['start_count'] = $start_count;
            $data['page_type']      = 'dashboard';
            // underage filter  
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
            
            $this->load->view('user_more_stories_v', $data);
        }
    }
    public function dashboard() {
        $this->redirectMe();
        
        $data['page_title'] = $this->session->userdata('user_name');
        $user_id = $this->session->userdata('user_id');
        //select 10 random friends
        $data['random_friends'] = $this->users_m->random_friends($user_id);
        //
        $glomp_buzz = $this->users_m->glomp_buzz($user_id, 0, 10);
        $data['buzz_user_id'] = $user_id;
        $data['glomp_buzz'] = $glomp_buzz; //data passed to view
        $glom_buzz_count = $glomp_buzz->num_rows();
        $data['glom_buzz_count'] = $glom_buzz_count; //data passed to view
        //jiofreed@facebook.com
        //received glomped or glomped  me
        $data['glomped_me'] = $this->users_m->glomped_me($user_id); //zachary.lai
        $data['user_start_tour'] = $this->users_m->get_tour_status($user_id); //iam.mee.7923@facebook.com//
		$user_promo_code = $this->users_m->get_promo_code_status($user_id); //iam.mee.7923@facebook.com//
		
		if($user_promo_code=="")
		{
		
		}
		else{
			$user_promo_code=json_decode($user_promo_code);
		}
		$res_info_prod="";		
		if( isset($user_promo_code->status) && 	($user_promo_code->status=='SuccessPrimary' || $user_promo_code->status=='SuccessFollowUp'))
		{
			$sql ="SELECT voucher_product_id, voucher_type FROM gl_voucher WHERE voucher_id ='".$user_promo_code->voucher_id."'";
			$result = $this->db->query($sql);					
			$result	= $result->row();
			$res_info_prod = $this->product_m->productInfo_2($result->voucher_product_id);
                        $res_info_prod = json_decode($res_info_prod, TRUE);
                        //Add voucher type
                        $res_info_prod['product']['voucher_type'] = $result->voucher_type;
		}		                
        $promo_rule_desc='';
        if(isset($user_promo_code->campaign_id) && is_numeric($user_promo_code->campaign_id))
            $promo_rule_desc = $this->users_m->get_promo_rule_desc($user_promo_code->campaign_id);						
		$data['res_info_prod'] = $res_info_prod;
		$data['user_promo_code']=$user_promo_code;
        $data['promo_rule_desc']=$promo_rule_desc;
        
        $data['is_connected_to_fb'] = $this->users_m->is_connected_to_fb($user_id); //iam.mee.7923@facebook.com//
        //
		$data['inTour'] = $this->input->get('inTour');
                
        $not_claim_voucher = $this->session->userdata('not_claim_voucher_notification');
        
        if (empty($not_claim_voucher)) {
            $not_claim_voucher = $not_claim_voucher;
        }
        //unset no claim message
        $this->session->unset_userdata('not_claim_voucher_notification');
        $data['not_claim_message'] = $not_claim_voucher;
        $this->load->view('user_dashboard_v', $data, true, true);
    }

    public function giveVoucher() {
        $subQuery = "";
        //$countExistingUser = 1; 
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $location = $this->input->post('location');
        $location_id = (int) $this->input->post('location_id');
        $data['name'] = $name;
        $data['email'] = $email;
        $data['location'] = $location;
        $data['location_id'] = $location_id;
        $location_id = ($location != "") ? $location_id : 0;

        $data['voucher_id'] = $this->input->post('voucher_id');
        //$data['resFriendList'] = $this->users_m->searchForFriends($subQuery . "ORDER BY user_fname ASC");

        $this->load->view('give_voucher_v', $data, FALSE, FALSE, FALSE);
    }

    public function searchFriends() {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['location'] = $this->input->post('location');
        $data['location_id'] = $this->input->post('location_id');
        $data['message'] = $this->input->post('message');        
        
        

        //If glomp button clicked show the "invite_friends_2" view
        if (isset($_POST['invite_glomp'])) {
            return $this->inviteFriends();
        }

        if (isset($_POST['invite_by_email'])) {
            //Get data from invite friends and push it to this array data
            foreach ($this->inviteFriends() as $key => $val) {
                $data[$key] = $val;
            }
        }

//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->load->model('country_m');
        
        $keywords = $this->input->post('keywords');
        $data['keywords'] = $keywords;
        if ($keywords != "") {
            $subQuery = "AND (user_fname LIKE '%$keywords%' OR user_Lname LIKE '%$keywords%' OR
					 CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%')";
            $data['page_level'] = "Search Results";
        } else {
            $subQuery = "";
            $data['page_level'] = "Friends";
        }
        if ($this->input->get('getFb') == 'true') {
            $data['resFriendList'] = $this->users_m->searchOwnFriends($keywords); // $subQuery."GROUP BY u.user_id	 ORDER BY user_fname ASC");
            $data['getFb'] = true;
        } else {
            $data['resFriendList'] = $this->users_m->searchOwnFriends($keywords); // $subQuery."GROUP BY u.user_id	 ORDER BY user_fname ASC");
        }
        $data['inTour'] = $this->input->get('inTour');


        $user_id = $this->session->userdata('user_id');
        $data['user_short_info'] = ($this->user_account_m->getUserFname($user_id));
        //$data['user_short_info']=json_decode($data['user_short_info']);

        $user_balance = $this->user_account_m->user_balance();
		$data['countries_json'] = $this->country_m->get_json_list();
        $this->load->view('user_search_friends_v', $data, false, false);
    }

    public function inviteFriends() {

        $subQuery = "";
        //$countExistingUser = 1; 
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $location = $this->input->post('location');
        $location_id = (int) $this->input->post('location_id');
        $data['name'] = $name;
        $data['email'] = $email;
        $data['location'] = $location;
        $data['location_id'] = $location_id;
        $data['message'] = $this->input->post('message');
        $location_id = ($location != "") ? $location_id : 0;
		
		
		$data['resFriendList'] = $this->users_m->searchForFriends($name,$email,$location_id);		

		 //$data['resFriendList'] = $this->users_m->searchForFriends($subQuery . "GROUP BY user_id ORDER BY user_fname ASC");
		
        
        ///============================================================================================================
        //$res_num = $this->users_m->searchForFriends($subQuery . " GROUP BY user_id ORDER BY user_fname ASC");
        //$res_num->num_rows();

        //if invite is clicked send email
        if (isset($_POST['invite_by_email'])) {
            $this->load->library('email_templating');

            if ($name != "" && $email != "") {
                //$subQuery.= "AND (user_fname LIKE '%$name%' OR user_lname LIKE '%$name%' OR CONCAT(user_fname,' ',user_lname) LIKE '%$name%' OR user_email = '$email')";
                $res_existing_user = $this->users_m->searchForFriends($name);// . " GROUP BY user_id ORDER BY user_fname ASC");
                $countExistingUser = $res_existing_user->num_rows();
                $user_id_of_entered_email = $this->users_m->user_id_by_email($email);
                $is_freind = $this->users_m->is_friend($this->session->userdata('user_id'), $user_id_of_entered_email);
                if ($this->custom_func->emailValidation($email) == "false") {/// check email validation
                    $data['invite_error_msg'] = $this->lang->line('invalid_email_address');
                } else if ($user_id_of_entered_email == $this->session->userdata('user_id')) {
                    $data['invite_error_msg'] = $this->lang->line('sorry_you_entered_own_email_address');
                } else if ($is_freind > 0) {
                    $data['invite_error_msg'] = $this->lang->line('already_in_friend_list');
                } else if ($countExistingUser == 0) {
                    $data['send_again'] = "";
                    ////send email$
                    $rec_user = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
                    $vars = array(
                        'template_name' => 'notice_tosignup_nonmember',
                        'lang_id' => $rec_user->row()->user_lang_id,
                        'from_name' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                        'from_email' => SITE_DEFAULT_SENDER_EMAIL,                        
                        'reply_to' => $rec_user->row()->user_email,
                        'to' => $email,
                        'params' => array(
                            '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                            '[signup]' => "<a href='" . site_url() . "'>sign up</a>"
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->set_subject($rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname . ' has invited you to glomp!');

                    if ($this->email_templating->send())

                    //$this->sendInviteEmail($email, $name);
                        $data['invite_message_sent'] = $this->lang->line('invitation_email_msg_sent');
                    $name = "";
                    $email = "";
                    $location = "";
                    $message = "";
                    /// to clear the fields after sending email.
                    $data['name'] = "";
                    $data['email'] = "";
                    $data['location'] = "";
                    $data['location_id'] = "";
                } else {
                    $res_email_check = $this->users_m->email_exist2($email);
                    $res_email_check->num_rows();
                    if ($res_email_check->num_rows() > 0) {
                        $data['invite_error_msg'] = $this->lang->line('already_exists_see_potential_matches');
                    } else {
                        $data['send_again'] = "true";
                    }
                    if (isset($data['send_again']) && $data['send_again'] == 'true') {
                        if ($this->custom_func->emailValidation($email) == "false") {/// check email validation
                            $data['invite_error_msg'] = $this->lang->line('invalid_email_address');
                        } else {
                            ////send email$
                            $rec_user = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
                            $vars = array(
                                'template_name' => 'notice_tosignup_nonmember',
                                'lang_id' => $rec_user->row()->user_lang_id,
                                'from_name' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                                'from_email' => SITE_DEFAULT_SENDER_EMAIL,
                                'reply_to' => $rec_user->row()->user_email,
                                'to' => $email,
                                'params' => array(
                                    '[insert member name]' => $rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname,
                                    '[signup]' => "<a href='" . site_url() . "'>sign up now!</a>"
                                )
                            );

                            $this->email_templating->config($vars);
                            $this->email_templating->set_subject($rec_user->row()->user_fname . ' ' . $rec_user->row()->user_lname . ' has invited you to glomp!');
                            
                            $email_log_id = $this->email_templating->send();                                
                            $invite_data['invite_email_log_id'] = $email_log_id;
                            $insert_id  = $this->invites_m->_insert($invite_data);
                            //log activity [invited_a_friend] through email
                            $details=array(                    
                                    'invite_id'=>$insert_id,
                                    'invited_name'=>$name,
                                    'invited_email'=>$email,
                                    'invited_through'=>'email'
                                    
                                    );
                            $params = array(
                                    'user_id'=>$this->session->userdata('user_id'),
                                    'type' =>'invited_a_friend',
                                    'details' =>json_encode($details)
                                    );
                            add_activity_log($params);
                            //log activity [invited_a_friend] through email
                                    
                            if ($email_log_id>0)

                            //$this->sendInviteEmail($email, $name);
                                $data['invite_message_sent'] = $this->lang->line('invitation_email_msg_sent');
                            $name = "";
                            $email = "";
                            $location = "";
                            $message = "";
                            /// to clear the fields after sending email.
                            $data['name'] = "";
                            $data['email'] = "";
                            $data['location'] = "";
                            $data['location_id'] = "";
                        }/// end of if($user_id_of_entered_email == $this->session->userdata('user_id'))
                    }//// end of  if($this->custom_func->emailValidation($email)=="false")		
                }
            }

            //$this->load->view('user_search_friends_v', $data);
            return $data;
        }

        //$data['resFriendList'] = $this->users_m->searchForFriends($subQuery . "GROUP BY user_id ORDER BY user_fname ASC");

        //This will be moved to different method
        $data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($location_id);
        //end
        $user_id = $this->session->userdata('user_id');
        //$data['view'] = $data;
        $this->load->view('user_search_for_friends_v', $data, TRUE, TRUE);
    }

    public function merchantProduct($id) {
        $user_id = $this->session->userdata('user_id');
        $this->load->view('user_merchant_product_v');
    }

    public function logOut() {
        $this->session->set_userdata('is_user_logged_in', false);
        if($this->session->userdata('admin_id') && $this->session->userdata('is_logged_in')==true)
        {
            
            $array_items = array(            
                'user_id' => '',
                'username' => '',
                'user_name' => '',
                'user_email' => '',
                'user_fname' => '',
                'user_lname' => '',
                'user_email_verified' => '',
                'user_last_login_date' => '',
                'user_last_login_ip' => '',
                'lang_id' => '',
                'twitter_oauth_token' => '',
                'twitter_oauth_token_secret' => '',
                'is_user_logged_in' => '',   
            );
            $this->session->unset_userdata($array_items);                
        }
        else
        {
            $this->session->sess_destroy();
        }

        $this->load->library('user_agent');
        if ($this->agent->is_referral())
        {
            redirect($this->agent->referrer());
        }
        else
            redirect('landing');
    }

    public function update() {
        $user_id = $this->session->userdata('user_id');
        $this->load->helper('url');
        $this->load->library('user_agent');
        //Load models
        $this->load->model('notifications_m');
        $this->load->model('user_notification_settings_m', 'user_not_sett');
        
        if (isset($_POST['update_profile'])) {
            $user = $this->db->get_where('gl_user', array('user_id'=> $user_id));
            $user_photo = ($user->row()->user_profile_pic == NULL) ? $user->row()->user_profile_pic_orig : $user->row()->user_profile_pic;
            $user_photo = ($user_photo == NULL) ? FALSE : TRUE;


            $this->form_validation->set_rules('fname', 'first Name', 'trim|required|xss_clean|callback_fname_check');
            $this->form_validation->set_rules('lname', 'last Name', 'trim|required|xss_clean|callback_lname_check');
            $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
            $this->form_validation->set_rules('months', 'Months', 'trim|required');
            $this->form_validation->set_rules('year', 'Year', 'trim|required');
            //$this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
            $this->form_validation->set_rules('location_text', 'Location', 'trim|required|xss_clean');
            $this->form_validation->set_rules('user_lang_id', $this->lang->line('language', 'Language'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
            
            if($_POST['profile_pic_latest'] == "" && $user_photo == FALSE) {
                $this->form_validation->set_rules('profile_pic_latest', 'Profile Photo', 'required');
            }
            if ($this->input->post('current_pword') != "") {
                $this->form_validation->set_rules('current_pword', 'Current Password', 'trim|required|min_length[6]|max_length[9]|callback_password_check|xss_clean');
            }
            if ($this->input->post('new_pword') != "") {
                $this->form_validation->set_rules('new_pword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
                $this->form_validation->set_rules('confirm_pword', 'Confirm Password', 'trim|required|matches[new_pword]');
            }
            if ($this->form_validation->run() == TRUE) {
                $locationOK=false;
                $location_id=$this->input->post('location');
                $location_text=$this->input->post('location_text');
                if(is_numeric($location_id))
                {
                    $locationOK=true;
                }
                else
                {                    
                    $result= $this->regions_m->getCountryID_Check($location_text);            
                    if($result->num_rows()>0){
                        $locationOK=true;
                        $res=$result->row();
                        $location_id=$res->region_id;
                    }
                }
                if(!$locationOK)
                {   
                    $data['error_msg'] = "Invalid location.";
                }
                else
                {
                    $this->login_m->update_profile($user_id);
                    $notification_setting = array();
                    if (isset($_POST['notfication_settings'])) {
                        $notification_setting = $_POST['notfication_settings'];
                    }
                    
                    $this->user_not_sett->set_notifcations($notification_setting, $user_id);
                    //
                    $data['success_msg'] = $this->lang->line('your_profile_udpated_successfully');
                    if (strlen($this->input->post('new_pword')) >= 6) {
                        //change password
                        $new_password = $this->input->post('new_pword');
                        $this->login_m->change_password($new_password, $user_id);
                    }
                    //update profile pic
                    /* upload photo start */
                    
                    if($_POST['profile_pic_latest']!="")
                    {
                        
                        $user_photo=$this->input->post('profile_pic_latest');						
                        if(file_exists("custom/uploads/users/temp/".$user_photo))
                        {
                            copy ("custom/uploads/users/temp/".$user_photo,"custom/uploads/users/thumb/".$user_photo);
                            unlink("custom/uploads/users/temp/".$user_photo);
                            $this->login_m->updatePhoto($user_photo,$user_id);	
                        }
                        $user_photo_orig=$this->input->post('profile_pic_orig');
                        $this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
                        
                    }//user photo temp check				                
                    /*                 * ****upload photo end** */
                }
            }
        }
        $rec_user = $this->users_m->user_info_by_id($user_id);
        $user_recod = $rec_user->row();
        $data['user_record'] = $user_recod;
        $data['user_id'] = $user_id;
        $data['notifications'] = $this->notifications_m->get_list(array(
            'where' => 'status = "active"'
        ));

        $this->load->view('update_profile_v', $data, false, false);
    }

    function email_check($email) {
        $email_chk = $this->login_m->update_email_exist_check($email, $this->session->userdata('user_id'));
        if ($email_chk == 0)
            return true;
        else {
            $this->form_validation->set_message('email_check', 'Email address already exists. Please try new one');
            return false;
        }
    }

    function password_check($password) {
        if ($password == "")
            return 'ok';
        //
        $password_verify = $this->login_m->password_verify($password, $this->session->userdata('user_id'));
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check', 'Invalid current password.');
            return false;
        }
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'crop', $sharpen = false) {//crop
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    function sendInviteEmail($email, $name) {
        //not exist into the database send invitation email]
        $to_email = $email;
        $to_name = $name;
        $email_message = $this->input->post('message');
        $invite_subject = $this->session->userdata('user_name') . ' has invited you to glomp!';
        $invite_message = "<br/>";
        $invite_message.= 'It\'s time for a treat! ' . $this->session->userdata('user_name') . ' has added you as a friend on glomp! ';
        $invite_message .= "<p>glomp! is a new online and mobile social networking platform where you can give and receive enjoyable treats such as a coffee, ice-cream, a beer and so on from a host of quality retail and service outlets. </p><br/><br/>So get started and <a href='" . base_url() . "'>sign up now!<a/>";
        if ($invite_message != "") {
            $invite_message .= '<br/>';
            $invite_message .= '<br/><p>' . $email_message . '</p>';
        }
        $invite_message .= '<br/>
Cheers.
<br/>
The glomp! team.<br/>';

        $this->send_email_m->sendEmail($this->site_sender_email, $this->site_sender_name, $to_email, $to_name, $invite_subject, $invite_message);
    }
    
    function fname_check($name){

    $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
    if (str_contains($name, $not_allowed) == false)
    {    
        //    echo "Error: Not allowed letters: ".$not_allowed;
        //todo language translation
        $this->form_validation->set_message('fname_check', 'Special characters are not allow on First Name.');
        return false;
    }
    else{
        //  echo "ok:".$username; 
        $not_allowed='1234567890';//
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            $this->form_validation->set_message('fname_check', 'Numbers are not allow on First Name.');
            return false;
        }
        else{
          //  echo "ok:".$username; 
          return true;                
        }            
    }    
}
    function lname_check($name){

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('lname_check', 'Special characters are not allowed on Last Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('lname_check', 'Numbers are not allowed on Last Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }

}
function dob_check($day)
{
	$year = ($this->input->post('year')=="")?'0000':$this->input->post('year');
	$month = ($this->input->post('months')=="")?'00':$this->input->post('months');
	$month = (int)$month;
	$day = (int)$day;
	$year  = (int) $year;
	$cur_date = date('Y-m-d');
	$entered_date = $year.'-'.$month.'-'.$day;
	if(checkdate($month,$day,$year))
	{
		//check for future date
		$sql = "SELECT DATEDIFF('$cur_date','$entered_date') AS future_date";
		$res = $this->db->query($sql);
		$rec = $res->row();
		$future = $rec->future_date;

		if($future<0)
		{
			$this->form_validation->set_message('dob_check', $this->lang->line('dob_can_not_future_date') );
			return false;
		}
		else
			return true;
	}
		
	else{
			$this->form_validation->set_message('dob_check', $this->lang->line('plz_provide_valid_dob') );
			//'Please provide valid Date of Birth'
			return FALSE;
		}

}
//eoc

