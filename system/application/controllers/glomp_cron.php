<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Glomp_cron extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('users_m');
        $this->load->model('user_account_m');
        $this->load->model('product_m');
        $this->load->model('product_inventory_m');
        $this->load->model('campaign_m');
        $this->load->model('merchant_m');
        $this->load->model('invites_m');
        $this->load->model('voucher_m');
        $this->load->model('user_notification_settings_m');

        //Load the email templating class
        $this->load->library('email_templating');
    }

    /**
     * voucher_not_accepted_notice
     * 
     * Get all voucher that are about to pending through invite table and send an email
     * to member
     * 
     * @params none
     * @params string
     */
    function voucher_not_accepted_notice() {
        $today = date('Y-m-d');
        $where = 'invite_voucher_id <> "" AND invite_responded = "N"';
        $invites = $this->invites_m->get_list(array(
            'where' => $where
        ));

        //print_r($invites);
        //echo count($invites);
        //exit;

        if (count($invites) > 0) {
            //print_r($invites);
            //exit;
            foreach ($invites as $i) {
                $where = 'voucher_id = "' . $i['invite_voucher_id'] . '" 
                    AND voucher_type = "Pending" 
                    AND DATEDIFF(voucher_expiry_date,"' . $today . '") = 1';

                $voucher = $this->voucher_m->get_record(array(
                    'where' => $where
                ));
                //print_r($voucher);
                if ($voucher !== FALSE) {
                    $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher['voucher_merchant_id']))->row();
                    $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher['voucher_product_id']))->row();
                    
                    $user = $this->users_m->user_info_by_id($i['invite_invited_id']);
                    $inviter = $this->users_m->user_info_by_id($i['invite_by_user_id']);
                    $link = anchor('landing/preFill/' . $user->row()->user_hash_key, 'sign up');

                    if ($i['invite_through'] == 'FB') {
                        $vars = array(
                            'template_name' => 'voucher_not_claimed_reminder_fb',
                            'lang_id' => $user->row()->user_lang_id,
                            'to' => $inviter->row()->user_email,
                            'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                            'params' => array(
                                '[insert recipients name]' => $user->row()->user_fname . ' ' . $user->row()->user_lname,
                            ),
                            'others' => array(
                                'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                                'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                            )
                        );

                        $this->email_templating->config($vars);
                        $this->email_templating->send();
                    } else {
                        $vars = array(
                            'template_name' => 'voucher_not_claimed_reminder_email',
                            'lang_id' => $user->row()->user_lang_id,
                            'to' => $user->row()->user_email,
                            'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                            'params' => array(
                                '[link]' => $link
                            ),
                            'others' => array(
                                'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                                'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                            )
                        );

                        $this->email_templating->config($vars);
                        $this->email_templating->send();
                    }
                }
            }
        }
    }

    /**
     * re_send_invite
     * 
     * Get all invites with no respond and send them an email after 7 days; Re-send email will be send only once
     * 
     * @params none
     * @params string
     */
    function re_send_invite() {
        $today = date('Y-m-d');
        $where = 'invite_voucher_id IS NULL AND invite_responded = "N" 
            AND DATEDIFF("' . $today . '", DATE_FORMAT(invite_date,"%Y-%m-%d")) > 7';

        $invites = $this->invites_m->get_list(array(
            'where' => $where
        ));

//        echo '<pre>';
//        print_r($invites);
//        echo count($invites);
//        exit;

        if (count($invites) > 0) {
            //print_r($invites);
            //exit;
            foreach ($invites as $i) {
                $name = $i['invite_invited_name'];
                $location = $i['invite_invited_location'];
                $email = $i['invite_invited_email'];

                //$user = $this->users_m->user_info_by_id($i['invite_invited_id']);
                $inviter = $this->users_m->user_info_by_id($i['invite_by_user_id']);
                $link = anchor(site_url('landing/invitePreFill?name=' . $name . '&location=' . $location . '&email=' . $email), 'sign up');

                if ($i['invite_added_message'] != '') {
                    $vars = array(
                        'template_name' => 're_send_invitation',
                        'lang_id' => $inviter->row()->user_lang_id,
                        'from_email' => $inviter->row()->user_email,
                        'from_name' => $inviter->row()->user_fname . ' ' . $inviter->row()->user_lname,
                        'to' => $email,
                        'params' => array(
                            '[insert members name]' => $inviter->row()->user_fname . ' ' . $inviter->row()->user_lname,
                            '[personal message]' => $i['invite_added_message'],
                            '[sign up]' => $link,
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->set_subject($inviter->row()->user_fname . ' ' . $inviter->row()->user_lname . ' has invited you to glomp!');
                    $this->email_templating->send();
                } else {
                    $vars = array(
                        'template_name' => 're_send_invitation',
                        'lang_id' => $inviter->row()->user_lang_id,
                        'from_email' => $inviter->row()->user_email,
                        'from_name' => $inviter->row()->user_fname . ' ' . $inviter->row()->user_lname,
                        'to' => $email,
                        'params' => array(
                            '[insert members name]' => $inviter->row()->user_fname . ' ' . $inviter->row()->user_lname,
                            '[sign up]' => $link,
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->set_subject($inviter->row()->user_fname . ' ' . $inviter->row()->user_lname . ' has invited you to glomp!');
                    $this->email_templating->send();
                }

                $invite_where = array('invite_invited_email' => $i['invite_invited_email']);

                $this->invites_m->_update($invite_where, array(
                    'invite_responded' => 'Y',
                    'invite_responded_date' => $this->custom_func->datetime(),
                ));
            }
        }
    }

    /**
     * voucher_twodays_notice
     * 
     * Get all voucher that are about to expire in one day and send an email
     * to user
     * 
     * @params none
     * @params string
     */
    function voucher_twodays_notice() {
        //Also run this
        $this->voucher_not_accepted_notice();
        $this->re_send_invite();
        

        $today = date('Y-m-d');

        $sql_select = "SELECT voucher_id,
            voucher_purchaser_user_id,
            voucher_belongs_usser_id,
            voucher_reverse_point,
            voucher_expiry_date,
            voucher_product_id 
            FROM gl_voucher
            WHERE DATEDIFF(voucher_expiry_date,'$today') = 3
            AND (voucher_status = 'Assigned' OR voucher_status = 'Consumable') 
            AND voucher_type <> 'Pending'
        ";
//        echo $sql_select;
//        exit;

        $res = $this->db->query($sql_select);
        $num_rows = $res->num_rows();

        if ($num_rows > 0) {
            foreach ($res->result() as $row_voucher) {
                $voucher_id = $row_voucher->voucher_id;
                $purchaser_id = $row_voucher->voucher_purchaser_user_id;
                $belongs_id = $row_voucher->voucher_belongs_usser_id;
                $product_id = $row_voucher->voucher_product_id;

                $user = $this->users_m->user_info_by_id($belongs_id);
                //echo '<pre>';
                //echo $user->row()->user_email;

                $product = $this->product_m->selectProductByID($product_id);
                //echo $product->row()->prod_name;

                $merchant = $this->merchant_m->selectMerchantID($product->row()->prod_merchant_id);

                //print_r($merchant->row()->merchant_name);

                if (!$this->user_notification_settings_m->is_checked(1, $user->row()->user_id)) {
                    //Skip email notification
                    continue;
                }

                $vars = array(
                    'template_name' => 'voucher_twodays_notice',
                    'lang_id' => $user->row()->user_lang_id,
                    'to' => $user->row()->user_email,
                    'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                    'params' => array(
                        '[insert merchant brand and product]' => $merchant->row()->merchant_name . ' ' . $product->row()->prod_name,
                        '[redeem]' => '<a href= "' . site_url() . '">redeem</a>'
                    ),
                    'others' => array(
                        'image_1' => base_url() . $this->custom_func->merchant_logo($merchant->row()->merchant_logo),
                        'image_2' => base_url() . $this->custom_func->product_logo($product->row()->prod_image)
                    )
                );
                
                $e_template = $this->db->get_where('gl_email_templates', array('template_name' => $vars['template_name']));
                $args['user_email'] = $vars['to'];
                $args['type'] = 'glomp!ed';
                $args['params'] = array(
                    'type' => '2',
                    'id' => $user->row()->user_id
                );
                $args['message'] = $e_template->row()->subject;

                push_notification($args);

                $this->email_templating->config($vars);

                if ($this->email_templating->send()) {
                    echo 'Succcess!';
                }
            }//foreach close
        }
        
        //Also run this
        $this->check_expired_campaign_by_promo();
        $this->check_products_pending_to_delete();
    }

    /**
     * notice_tosignup_nonmember
     * 
     * Get all invite user that has invite_responded "N" status then send an 
     * email to them every 1 week/7 days until status has been updated to "Y"
     * 
     * @params none
     * @params string
     */
    function notice_tosignup_nonmember() {

        if ($num_rows > 0) {
            foreach ($res->result() as $row_invites) {
                $email = $row_invites->invite_invited_id;

                if ($row_invites->invite_through == 'fb') {
                    $fb_user = $this->facebook->api('/' . $row_invites->invite_invited_id);
                    $email = $fb_user['username'] . '@facebook.com';
                }

                $inviter_id = $row_invites->invite_by_user_id;
                $user = $this->users_m->user_info_by_id($inviter_id);

                $vars = array(
                    'template_name' => 'notice_tosignup_nonmember',
                    'lang_id' => $user->row()->user_lang_id,
                    'to' => $email,
                    'params' => array(
                        '[insert member name]' => $user->row()->user_fname . ' ' . $user->row()->user_lname
                    )
                );

                $this->email_templating->config($vars);
                $this->email_templating->set_subject($user->row()->user_fname . ' ' . $user->row()->user_lname . ' has invited you to glomp!');

                if ($this->email_templating->send()) {
                    echo 'Succcess!';
                }
            }//foreach close
        }
    }

    /**
     * capitalize_fname_lname
     * 
     * Capital first letter on first name and last name
     * Cron runs at 12mn
     * @params none
     */
    function capitalize_fname_lname() {
        //Get all other than glomp
        $rec = $this->db->query('SELECT user_id, user_fname, user_lname 
            FROM gl_user
            WHERE user_id <> 1
        ');

        foreach ($rec->result() as $row) {
            $data = array(
                'user_fname' => ucfirst($row->user_fname),
                'user_lname' => ucfirst($row->user_lname)
            );

            $this->db->where('user_id', $row->user_id);
            $this->db->update('gl_user', $data);
        }

        echo 'Success!';
    }

    /**
     * voucher_expire
     * 
     * Get all voucher that has been expired and change their status into expired
     * 
     * @params none
     */
    function voucher_expire() {
        #Step1	 check for expired voucher but not rededmped
        #setep2 restore the voucher reverse point into the purchaser account
        #Step3 update voucher record
        #setp4 update user summary
        $today = date('Y-m-d');

        $sql_select = "SELECT voucher_id,
            voucher_purchaser_user_id,
            voucher_belongs_usser_id,
            voucher_reverse_point,
            voucher_expiry_date,
            voucher_product_id 
            FROM gl_voucher
            WHERE
            voucher_status = 'Consumable'
            AND voucher_orginated_by = 0
            AND DATEDIFF(voucher_expiry_date,'$today')<=-1 
        ";
        $res_expired_voucher = $this->db->query($sql_select);
        $num_rows = $res_expired_voucher->num_rows();

        if ($num_rows > 0) {
            foreach ($res_expired_voucher->result() as $row_voucher) {
                $voucher_id = $row_voucher->voucher_id;
                $purchaser_id = $row_voucher->voucher_purchaser_user_id;
                $belongs_id = $row_voucher->voucher_belongs_usser_id;
                $product_id = $row_voucher->voucher_product_id;
                $reversed_point = (int) $row_voucher->voucher_reverse_point;
                //start transaction
                $this->db->query('BEGIN');
                $time = date('Y-m-d H:i:s');
                $ip = $_SERVER['REMOTE_ADDR'];
                //INSERT into user account and user summary table
                $sql = "INSERT INTO gl_account SET
                    account_id =  UUID(),
                    account_user_id='$purchaser_id',
                    account_txn_type='CR',
                    account_point='$reversed_point',
                    account_balance_point = (account_balance_point+$reversed_point),
                    account_txn_type_id = '3',
                    account_details = 'Voucher did not reddedmed and reversed to your account',
                    account_txnID = UUID(),
                    account_txn_date = '$time',
                    account_txn_ip = '$ip'
                ";
                $account_insert_id = $this->db->query($sql);


                //now update into account summary table.
                $sql_update = "UPDATE gl_activity_summary SET
                    summary_point_balance = (summary_point_balance+$reversed_point)
                    WHERE summary_user_id = '$purchaser_id'
                ";
                $this->db->query($sql_update);
                $summary_affected_id = $this->db->affected_rows();

                //update voucher
                $sql_update_voucher = "UPDATE gl_voucher SET
                    voucher_status = 'Expired',
                    voucher_transactionID = UUID(),
                    voucher_note = 'Voucher expired and point reversed to the purchaser account at $time'
                    WHERE voucher_id = '$voucher_id'
                ";

                $this->db->query($sql_update_voucher);
                $voucher_affected_id = $this->db->affected_rows();
                //now insert into activity log
                $sql_activity = "INSERT INTO gl_activity SET
                    log_user_id = '$purchaser_id',
                    log_title = 'Voucher reversed',
                    log_details = 'Voucher has been reversed and glomp $reversed_point has been added into 
                                                            your account at $time. Reversed voucher id is: $voucher_id',
                    log_ip = '$ip',
                    log_timestamp = '$time',
                    log_device = 'cron-job'
                ";                
                $this->db->query($sql_activity);
                
                
                //log activity [points_reversed]                    
                $details=array(
                        'voucher_id'=>$voucher_id,
                        'points'=>$reversed_point,
                        'balance'=>$this->user_balance($purchaser_id)
                        );
                        
                $query = "INSERT INTO gl_activity_log SET
                            user_id = '$purchaser_id',
                            log_type = 'points_reversed',
                            details = '".json_encode($details)."',						
                            log_timestamp = '".date('Y-m-d H:i:s')."'
                            ";                
                $this->db->query($query);
                //log activity [points_reversed] 
                
                
                if ($voucher_affected_id > 0 && $summary_affected_id >= 0 && $account_insert_id > 0) {
                    echo 'SUCCESS';
                    $this->db->query('COMMIT');
                    
                    $purchaser = $this->user_account_m->user_short_info($purchaser_id);
                    $purchaser = json_decode($purchaser);
                    $receipient = $this->user_account_m->user_short_info($belongs_id);
                    $receipient = json_decode($receipient);
                    $res_product = $this->product_m->is_user_valid_prodcut($belongs_id, $product_id);
                    $row_prod = $res_product->row();
                    
                    $vars = array(
                        'template_name' => 'reverse_points',
                        'lang_id' => 1,
                        'to' => $purchaser->user_email,
                        'params' => array(
                            '[purchaser]' => $purchaser->user_fname.' '.$purchaser->user_lname,
                            '[brand and product name]' => $row_prod->merchant_name.' '.$row_prod->prod_name,
                            '[recipient]' => $receipient->user_fname.' '.$receipient->user_lname,
                            '[returned points]' => $reversed_point,
                            '[link]' => '<a target="_blank" href="'.site_url('profile/view/'. $belongs_id).'">now</a>',
                        )
                    );
                    $this->email_templating->config($vars);
                    $this->email_templating->send();
                    
                    
                    $e_template = $this->db->get_where('gl_email_templates', array('template_name' => $vars['template_name']));
                    $args['user_email'] = $purchaser->user_email;
                    $args['type'] = 'points_returned';
                    $args['params'] = array(
                        'type' => '6',
                        'id' => 0
                    );
                    $args['message'] = $e_template->row()->subject;

                    push_notification($args);
                    
                } else {
                    echo 'ROLLBACK';
                    $this->db->query('ROLLBACK');
                }
            }//foreach close
        }
        echo 'No EXPIRING VOUCHER';
        $this->campaign_voucher();
        //Run this also
        $this->capitalize_fname_lname();
    }

//function end
    function user_balance($user_id = 0, $jsnon = false) {
        $sql = "SELECT summary_point_balance  
				FROM  gl_activity_summary
				WHERE summary_user_id = '$user_id'
				";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $res = $result->row();
            $point_balance = $res->summary_point_balance;
            $user_balance = array('summary_point_balance' => $point_balance);
        } else {
            $point_balance = 0;
            $user_balance = array('summary_point_balance' => 0);
        }
        if ($jsnon)
            return json_encode($user_balance);
        else
            return $point_balance;
    }
    
    function campaign_voucher() {
        $time = date('Y-m-d H:i:s');
        $ip = $_SERVER['REMOTE_ADDR'];
        $today = date('Y-m-d');
        $sql_select = "SELECT voucher_id,
            voucher_purchaser_user_id,
            voucher_belongs_usser_id,
            voucher_product_id ,
            voucher_orginated_by,
            voucher_status
            FROM gl_voucher
            WHERE
            voucher_status IN('Consumable','Assigned')
            AND voucher_orginated_by > 0 
            AND DATEDIFF(voucher_expiry_date,'$today')<=-1 
        ";
        
        $res_expired_voucher = $this->db->query($sql_select);
        $num_rows = $res_expired_voucher->num_rows();

        if ($num_rows > 0) {
            foreach ($res_expired_voucher->result() as $row_voucher) {
                $voucher_id = $row_voucher->voucher_id;
                $winner_user_id = $row_voucher->voucher_belongs_usser_id;
                $product_id = $row_voucher->voucher_product_id;
                $campaign_id = $row_voucher->voucher_orginated_by;
                $voucher_type = ($row_voucher->voucher_status == 'Consumable') ? 'Consumable' : 'Assignable';
                //start transaction
                $this->db->query('BEGIN');

                $sql_update_voucher = "UPDATE gl_voucher SET
                    voucher_status = 'Expired',
                    voucher_transactionID = UUID(),
                    voucher_note = 'Voucher expired'
                    WHERE voucher_id = '$voucher_id'
                ";
                $this->db->query($sql_update_voucher);
                $voucher_affected_id = $this->db->affected_rows();
                //recycled the expired voucher. ie. again insert into the same campaign with same attributes

                $sql_insert_voucher = "INSERT INTO gl_campaign_voucher SET
                    camp_voucher_id = UUID(),
                    camp_campaign_id = '$campaign_id',
                    camp_prod_id = '$product_id',
                    camp_voucher_assign_status = 'UnAssigned',
                    camp_voucher_type = '$voucher_type',
                    generated_date  = '$time',
                    camp_voucher_note = 'recycled voucher'
                ";
                $insert_voucher = $this->db->query($sql_insert_voucher);

                //now insert into activity log
                $sql_activity = "INSERT INTO gl_activity SET
                    log_user_id = '$winner_user_id',
                    log_title = 'Voucher expired',
                    log_details = 'Voucher has been expired.Expired voucher id is: $voucher_id',
                    log_ip = '$ip',
                    log_timestamp = '$time',
                    log_device = 'cron-job'
                ";
                $this->db->query($sql_activity);

//TODO: ENABLE WHEN NEED TO HAVE SPONSOR INVENTORY
//                $this->db->_protect_identifiers=false;
//                $this->db->join('gl_categories', 'cat_name = "Sponsor" AND prod_cat_id = cat_id');
//                $product = $this->db->get_where('gl_product', array('prod_id' => $product_id));
//                
//                if ($product->num_rows() > 0) {
//                
//                    $this->db->order_by("prod_inv_id", "desc");
//                    $product_inventory = $this->db->get_where('gl_product_inventory', array(
//                        'prod_id' => $product_id,
//                    ));
//
//                    $data = array(
//                        'prod_id'=> $product_id,
//                        'change_qty' => 1,
//                        'current_qty' => $product_inventory->row()->current_qty + 1,
//                        'details'=> 'Added (1) due to voucher expiry. Campaign no: '.$campaign_id,
//                        'timestamp' => date('Y-m-d H:i:s')
//                    );
//
//                    $this->db->insert('gl_product_inventory', $data); 
//                    
//                }


                if ($voucher_affected_id > 0 && $insert_voucher > 0) {
                    $this->db->query('COMMIT');
                } else {
                    $this->db->query('ROLLBACK');
                }
            }//foreach clsoe
        }//end of num rows
    }

//function end    
    function send_againinvite() {
        $this->load->model('email_logs_m');

        $join_user = array('table' => 'gl_user u'
            , 'condition' => 'u.user_email <> el.to_email');

        $email_logs = $this->email_logs_m->get_list(array(
            'join' => array($join_user),
            'where' => 'el.subject LIKE "%has invited you to glomp!%"',
            'group_by' => 'el.to_email'
        ));

        if (count($email_logs) > 0) {
            $url = 'http://sendgrid.com/';
            $user = 'glompzac';
            $pass = '6lomp!DOT1+_send';
            
            foreach ($email_logs as $e) {
                
                $link = site_url('landing/invitePreFill?name=""&location=""&email='. $e['to_email']);
                $rec_user = $this->users_m->user_info_by_email($e['from_email']);
                
                if ($rec_user->num_rows > 0 ) {
                     $vars = array(
                        'template_name' => 'notice_tosignup_nonmember',
                        'lang_id' => $rec_user->row()->user_lang_id,
                        'from_name' => $rec_user->row()->user_fname.' '.$rec_user->row()->user_lname,
                        'from_email' => $rec_user->row()->user_email,
                        'to' => $e['to_email'],
                        'params' => array(
                            '[insert member name]' => $rec_user->row()->user_fname.' '.$rec_user->row()->user_lname,
                            '[signup]' => "<a href='" . $link . "'>sign up</a>"
                        )
                    );
                
                    $this->email_templating->config($vars);
                    $this->email_templating->set_subject($e['subject']);
                    $this->email_templating->send();
                }
            }
        }
    }
    
    function check_expired_campaign_by_promo()
    {
        $rec_campaign   = $this->campaign_m->selectCampaign('by_promo');
        foreach ($rec_campaign->result() as $rec_campaign) {
            $res_voucher = $this->campaign_m->campaign_voucher($rec_campaign->campaign_id);
            $num_rows = $res_voucher->num_rows();
            $used_voucher = $num_rows;//$total_voucher
            
            $total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($rec_campaign->campaign_id);
            

            $available_voucher = $total_voucher-$used_voucher;
            
            $today = date('Y-m-d');
            $campaign_by_promo_end_date=$rec_campaign->campaign_by_promo_end_date;
            
            if($available_voucher<=0 || ( $rec_campaign->campaign_by_promo_enable_time_limit=='Y' && (strtotime($campaign_by_promo_end_date) < strtotime($today))  ) )
            {
                $sql="UPDATE  gl_campaign_details_by_promo SET campaign_by_promo_status='Done' WHERE campaign_id='".$rec_campaign->campaign_id."'";
                $result = $this->db->query($sql);
            }
            
        }
    }   
    function check_products_pending_to_delete()
    {
        $where = array('prod_status'=>'Pending for Deletion');
        $data= $this->db->get_where('gl_product',$where);
        if($data->num_rows()>0)
		{
			foreach($data->result() as $row)
            {
				$res= $this->product_m->tryToDeleteThisProduct($row->prod_id);
                if($res)
                    echo "deleted";
                else
                    echo "not deleted";
                echo '<br>';
            }
			
		}
        else
        {
            echo 'no products to delete.';
        }
    }

    /**
     * notify_weekly_merchant_report
     * 
     * Get all purchased voucher per merchant and send an email to account personnel
     * See get_merchant_reports on adminControl - Controllers- merchant.php - function: get_merchant_reports
     * 
     * @params none
     */
    function notify_weekly_merchant_report() {
        $week_formatter = array(
            '1' => '01',
            '2' => '02',
            '3' => '03',
            '4' => '04',
            '5' => '05',
            '6' => '06',
            '7' => '07',
            '8' => '08',
            '9' => '09',
        );

        //Enable for test
        //$week = date('W', strtotime('06/16/2014'));
        //$year = date('Y', strtotime('06/16/2014'));

        $week = date('W');
        $year = date('Y');

        $week = (array_key_exists($week, $week_formatter)) ? $week_formatter[$week] : $week;

        //Get previous week date range from today
        $from_date = date('Y/m/d', strtotime('last week monday -1 day', strtotime($year . 'W' . $week))) . ' 00:00:00'; //12 AM
        $to_date = date('Y/m/d', strtotime('last week sunday -1 day', strtotime($year . 'W' . $week))) . ' 23:59:59'; // 11:59 PM
        $where = "voucher.voucher_orginated_by = 0 AND voucher.voucher_purchased_date BETWEEN  '$from_date' AND '$to_date'";


        $join_voucher = array(
            'table' => 'gl_voucher voucher',
            'condition' => "merchant.merchant_id = voucher.voucher_merchant_id  AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_redemption_date BETWEEN  '$from_date' AND '$to_date'",
            'join' => 'LEFT',
        );

        $join_region = array(
            'table' => 'gl_region gr',
            'condition' => "merchant.merchant_region_id = gr.region_id AND merchant.merchant_status = 'Active'",
        );

        $rec = $this->merchant_m->get_list(array(
            'select' => '
                   voucher.voucher_status,
                   merchant.merchant_id, 
                   merchant.merchant_name, 
                   merchant.merchant_email,
                   merchant.merchant_contact_name, 
                   merchant.accounts_contact_name,  
                   SUM(voucher.voucher_prod_cost) AS amount,
                   gr.region_name AS location,
                   merchant.notify_top_limit',
            'join' => array(
                $join_voucher,
                $join_region
            ),
            #'where' => $where,
            'group_by' => 'merchant.merchant_id, merchant.merchant_region_id',
            'resource_id' => FALSE
        ));

        //echo $this->db->last_query();
        //exit;
        $rec = $this->db->query('
            SELECT * FROM (' . $this->db->last_query() . ') summary 
            WHERE amount >= notify_top_limit AND notify_top_limit IS NOT NULL AND notify_top_limit <> 0
        ');
        //echo $this->db->last_query();
        //exit;

        if ($rec->num_rows() > 0) {
            $link = site_url('adminControl/merchant/reports?week=' . date('W', strtotime($to_date)) . '&year=' . date('Y', strtotime($to_date)));
            $this->_send_notification_personnel($link);
            foreach ($rec->result() as $row) {
                $link = site_url('merchantControl/dashboard/reports?week=' . date('W', strtotime($to_date)) . '&year=' . date('Y', strtotime($to_date)));
                //$date = $from_date.' - '.$to_date;
                //$this->_send_notification_merchant($row, $link, $date);
                $this->_send_notification_merchant($row, $link, $from_date, $to_date);
            }
            exit;
        }
        echo 'No record.';
    }

    /**
     * notify_monthly_merchant_report
     * 
     * Get all purchased voucher per merchant and send an email to account personnel
     * See get_merchant_reports on adminControl - Controllers- merchant.php - function: get_merchant_reports
     * 
     * @params none
     */
    function notify_monthly_merchant_report() {
        //Uncomment for testing
        //$year = '2014';
        //$month = 'August';
        $this->_log_process('Starting notify_monthly_merchant_report', TRUE);
        
        $year = date('Y');
        $month = date('F');
        
        
        $from_date = date('Y/m/d', strtotime('first day of last month', strtotime($year . ' ' . $month))) . ' 00:00:00';
        $to_date = date('Y/m/d', strtotime('last day of last month', strtotime($year . ' ' . $month))) . ' 23:59:59';
        
        $join_voucher = array(
            'table' => 'gl_voucher voucher',
            'condition' => "merchant.merchant_id = voucher.voucher_merchant_id  AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_redemption_date BETWEEN  '$from_date' AND '$to_date'",
            'join' => 'LEFT',
        );

        $join_region = array(
            'table' => 'gl_region gr',
            'condition' => "merchant.merchant_region_id = gr.region_id AND merchant.merchant_status = 'Active'",
        );
        
        $where = "merchant.merchant_status = 'Active' AND voucher.voucher_orginated_by = 0 AND voucher.voucher_purchased_date BETWEEN  '$from_date' AND '$to_date'";

        $rec = $this->merchant_m->get_list(array(
            'select' => '
                   voucher.voucher_status,
                   merchant.merchant_id, 
                   merchant.merchant_name, 
                   merchant.merchant_email,
                   merchant.merchant_contact_name, 
                   merchant.accounts_contact_name,  
                   SUM(voucher.voucher_prod_cost) AS amount,
                   gr.region_name AS location,
                   merchant.notify_excempt_amount,
                   merchant.notify_top_limit',
            'join' => array(
                $join_voucher,
                $join_region 
            ),
            #'where' => $where,
            'group_by' => 'merchant.merchant_id, merchant.merchant_region_id',
            'resource_id' => TRUE
        ));
        //echo $this->db->last_query();
        //exit;

        if ($rec->num_rows() > 0) {
//            echo '<pre>';
//            print_r($rec->result());
            //Notify glomp! accounts personnel 
            $link = site_url('adminControl/merchant/reports?month_year=' . urlencode(date('F', strtotime($to_date)) . ' ' . date('Y', strtotime($to_date))));
            $this->_send_notification_personnel($link);
            $this->_log_process('Sending notification to personnel ... DONE');
//            echo '<pre>';
//            echo print_r($rec->result());
//            exit;
//            exit;
            foreach ($rec->result() as $row) {
                $link = site_url('merchantControl/dashboard/reports?month_year=' . urlencode(date('F', strtotime($to_date)) . ' ' . date('Y', strtotime($to_date))));
                $date = date('F', strtotime($to_date)) . ' ' . date('Y', strtotime($to_date));
                $this->_send_notification_merchant($row, $link, $from_date, $to_date);
            }
            $this->_log_process('Sending notification to merchants ... DONE');
            exit;
        }
        echo 'No record.';
    }

    function _send_notification_personnel($link) {
        
        $click_here = "<a href='$link'>here</a>";
        //Uncomment for testing
        //$recipients = array(
            //'allan.bernabe@gmail.com',
            //'ajedbernabe@yahoo.com'
        //);
        //Please change this to dynamic
        $recipients = array(
            'z@glomp.it',
            'accounts@glomp.it'
        );
        foreach($recipients as $to_email) {
            $vars = array(
                'template_name' => 'notify_personnel_report',
                'lang_id' => 1,
                'to' => $to_email,
                'params' => array(
                    '[link]' => $click_here,
                ),
            );

            $this->email_templating->config($vars);
            $this->email_templating->send();
            echo 'Success!: '.$to_email.'<br />';
        }
    }

    function _send_notification_merchant($row, $link, $from_date, $to_date) {
        $files = $this->merchant_report_2($row, $from_date, $to_date);//Another version
        $email = $row->merchant_email;
        $contact_name = '';
        
        if (! empty($row->merchant_contact_name)) {
            $contact_name = 'Dear '.$row->merchant_contact_name.',';
        }
        
        $click_here = "<a href='$link'>here</a>";
        
        if (!empty($row->accounts_contact_email)) $email = $row->accounts_contact_email;
        if (!empty($row->accounts_contact_name)) $contact_name = 'Dear '.$row->accounts_contact_name.',';
        //$email = 'allan.bernabe@gmail.com';//Uncomment for testing;
        //No records at all
        if (empty($files)) {
            $vars = array(
                'template_name' => 'notify_merchant_payment_notification_3',
                'lang_id' => 1,
                'to' => $email,
                'params' => array(
                    '[link]' => $click_here,
                    '[name of contact]' => $contact_name,
                ),
            );

            $this->email_templating->config($vars);
            $this->email_templating->send();
            echo 'No record: ' . $row->merchant_id . '<br />';
            return;
        }
        
        $attachments = array($files);
        $vars = array(
            'template_name' => 'notify_merchant_payment_notification',
            'lang_id' => 1,
            'to' => $email,
            'params' => array(
                '[link]' => $click_here,
                '[name of contact]' => $contact_name,
            ),
            'files' => $attachments,
        );
        
        //Minimum account settlement didn't reach
        if (empty($row->notify_excempt_amount) || $row->notify_excempt_amount == '0.00') {
            //run default
            if ($row->amount <= 100) {
                $vars['template_name'] = 'notify_merchant_payment_notification_2'; 
            }
        } else if ($row->amount <= $row->notify_excempt_amount) {
            $vars['template_name'] = 'notify_merchant_payment_notification_2'; 
        }

        $this->email_templating->config($vars);
        $this->email_templating->send();
        
        //Delete temporary file
        unlink($files['path']);
        echo 'Success!: ' . $row->merchant_id . '<br />';
    }

    function merchant_report($row, $date) {
        $this->load->library('excel');
        $filename = $row->merchant_id.'-'.date('mdY'). '-Statement.xlsx'; 
        $path = FCPATH. "custom/uploads/merchant/".$filename; 
        
//        header("Content-Type: application/vnd.ms-excel");
//        header("Content-Disposition: attachment; filename=\"$filename.xls\"");
//        header("Cache-Control: max-age=0");
        
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("glomp!");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STATMENT OF ACCOUNT');
        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Amount');
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A4', $date);
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', $row->amount);
        
        $objPHPExcel->getActiveSheet()
                ->getStyle("B")
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $objPHPExcel->getActiveSheet()
                ->getStyle("A3:B3")
                ->applyFromArray(array("font" => array( "bold" => true)));
        
        $objPHPExcel->getActiveSheet()
                ->getStyle("A1")
                ->applyFromArray(array("font" => array( "bold" => true)));

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Report');

        // Save Excel 2007 file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($path);
        
        return array(
            'filename' => $filename,
            'path' => $path,
        );
    }
    
    function merchant_report_2($row, $from_date, $to_date) {
        $this->load->library('excel');
        $filename = $row->merchant_id.'-'.date('mdY'). '-Statement.xlsx'; 
        $path = FCPATH. "custom/uploads/merchant/".$filename;
        
        $join_voucher = array(
            'table' => 'gl_voucher voucher',
            'condition' => "merchant.merchant_id = voucher.voucher_merchant_id AND voucher.voucher_status = 'Redeemed' AND voucher.voucher_redemption_date BETWEEN  '$from_date' AND '$to_date'",
            'join' => 'LEFT',
        );
        
        $join_region = array(
            'table' => 'gl_region gr',
            'condition' => 'merchant.merchant_region_id = gr.region_id',
        );
        
        $join_product = array(
            'table' => 'gl_product gp',
            'condition' => 'voucher.voucher_product_id = gp.prod_id',
        );
        
        $join_outlet = array(
            'table' => 'gl_outlet gout',
            'condition' => 'voucher.voucher_outlet_id = gout.outlet_id',
        );
        
        //Uncomment for testing
        //$year = '2014';
        //$month = 'August';

        
        $year = date('Y');
        $month = date('F');
        

        //$from_date = date('Y/m/d', strtotime('first day of last month', strtotime($year . ' ' . $month))) . ' 00:00:00';
        //$to_date = date('Y/m/d', strtotime('last day of last month', strtotime($year . ' ' . $month))) . ' 23:59:59';
        $where = "merchant.merchant_id = '$row->merchant_id'";

        $rec = $this->merchant_m->get_list(array(
            'select' => '
                   voucher.voucher_redemption_date, 
                   voucher.voucher_purchased_date, 
                   voucher.voucher_transactionID, 
                   voucher.voucher_code, 
                   voucher.voucher_prod_cost AS amount,
                   gout.outlet_name,
                   gp.prod_name,
                   gr.region_name AS location',
            'join' => array(
                $join_voucher,
                $join_product,
                $join_outlet,
                $join_region
            ),
            'where' => $where,
            'order_by' => 'voucher.voucher_redemption_date DESC',
            'resource_id' => TRUE
        ));
        
        if ($rec->num_rows() == 0) {
            return 0;
        }
        
        //echo $this->db->last_query();
        //exit;
        
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("glomp!");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STATEMENT OF ACCOUNT');
        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Redemption');
        $objPHPExcel->getActiveSheet()->SetCellValue('G3', 'Purchase');
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Outlet');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Voucher No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Product');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Verification No.');
        //Purchase
        $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Time');
        //Amount
        $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Amount');
        
        $row_incremental = 5;
        $total_amount = (float) 0;
        foreach($rec->result() as $r) {
            $format_amount = (string) number_format($r->amount, 2, '.', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row_incremental, date('d-M-y', strtotime($r->voucher_redemption_date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row_incremental, date('h:i A', strtotime($r->voucher_redemption_date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row_incremental, $r->outlet_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row_incremental, $r->voucher_transactionID);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row_incremental, $r->prod_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row_incremental, $r->voucher_code);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row_incremental, date('d-M-y', strtotime($r->voucher_purchased_date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row_incremental, date('h:i A', strtotime($r->voucher_purchased_date)));
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row_incremental, $format_amount);
            $total_amount = $total_amount + $format_amount;//Sum
            $row_incremental++;
        }
        //Footer
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row_incremental, 'Subtotal');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row_incremental, $total_amount);
        
        $bank_transter_fee = 12; // bank transfer fee; can be change
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.($row_incremental + 1), 'Bank Transfer Fee');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.($row_incremental + 1), $bank_transter_fee); 
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.($row_incremental + 2), 'TOTAL');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.($row_incremental + 2), $total_amount - $bank_transter_fee);
        
        //Auto size
        foreach(range('A','I') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        $objPHPExcel->getActiveSheet()
                ->getStyle("I")
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        //Redemption Title
        $objPHPExcel->getActiveSheet()->mergeCells('A3:F3');
        $objPHPExcel->getActiveSheet()
                ->getStyle("A3:F3")
                ->applyFromArray(
                        array(
                            'font' => array( "bold" => true),
                            'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        )
                  );
        
        //Purchase Title
        $objPHPExcel->getActiveSheet()->mergeCells('G3:I3');
        $objPHPExcel->getActiveSheet()
                ->getStyle("G3:I3")
                ->applyFromArray(
                        array(
                            'font' => array( "bold" => true),
                            'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        )
                  );
        
        //HEADERS
        $objPHPExcel->getActiveSheet()
                ->getStyle("A4:I4")
                ->applyFromArray(array("font" => array( "bold" => true)));
        
        //STATEMENT OF ACCOUNT TITLE
        $objPHPExcel->getActiveSheet()
                ->getStyle("A1")
                ->applyFromArray(array("font" => array( "bold" => true)));
        
        //Footer
        $objPHPExcel->getActiveSheet()->mergeCells("A".$row_incremental.":H".$row_incremental);
        $objPHPExcel->getActiveSheet()->mergeCells("A".($row_incremental + 1).":H".($row_incremental + 1));
        $objPHPExcel->getActiveSheet()->mergeCells("A".($row_incremental + 2).":H".($row_incremental + 2));
        $objPHPExcel->getActiveSheet()
                ->getStyle("A".$row_incremental.":I".($row_incremental + 2))
                ->applyFromArray(array("font" => array( "bold" => true)));
        
        
        $styleArray = array(
            'borders' => array(
              'inside'     => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array(
                  'argb' => '00000000'
                )
              ),
              'outline'     => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array(
                   'argb' => '00000000'
                )
              )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A3:I'.($row_incremental + 2))->applyFromArray($styleArray);
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Report');

        // Save Excel 2007 file
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($path);
        
        return array(
            'filename' => $filename,
            'path' => $path,
        );
    }
    
    function upload_me() {
        $q= $this->db->query("select merchant_id, merchant_name, gp.prod_name,gp.prod_image, gp.prod_id 
from gl_product gp
JOIN gl_merchant ON gp.prod_merchant_id = gl_merchant.merchant_id 
where prod_id in (
48, 
112,
255, 
147, 
148, 
158, 
159, 
183, 
184, 
185, 
195, 
196, 
197, 
198, 
200, 
205, 
206, 
207,
 218, 
 219,
  220,
   221,
    222,
     223,
      224,
       225,
        226,
         227,
          171,
           172,
            228,
             32)
order by FIELD (prod_id,
48, 
112,
255, 
147, 
148, 
158, 
159, 
183, 
184, 
185, 
195, 
196, 
197, 
198, 
200, 
205, 
206, 
207,
 218, 
 219,
  220,
   221,
    222,
     223,
      224,
       225,
        226,
         227,
          171,
           172,
            228,
             32
);");
        
        foreach($q->result_array() as $row) {
            $filename = 'beer 2_edit_1.jpg';
            $current = file_get_contents(FCPATH. 'custom/uploads/' . $filename);

            $safe_filename = preg_replace(
                array("/\s+/", "/[^-\.\w]+/"),
                array("_", ""),
                trim($filename));

            $new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0]. '.jpg';

            file_put_contents(FCPATH. 'custom/uploads/products/'. $new_photo_name, $current);
            
            $this->product_m->updateLogo($new_photo_name, $row['prod_id']);
            $this->resizeImg("custom/uploads/products/" . $new_photo_name, "custom/uploads/products/thumb/" . $new_photo_name, 160, 160);

        }
        
        clear_cached();
        
    }
    
    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'exact', $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }
    
    function _log_process($message = '', $delete = FALSE) {
        $write_or_append = ($delete) ? 'w' : 'a';
        $myfile = fopen(FCPATH . "notification_merchant_log.txt", $write_or_append) or die("Unable to open file!");
        $txt = date('Y-m-d H:i:s') . " ---> " . $message . "\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

}

//class end
