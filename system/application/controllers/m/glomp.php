<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Glomp extends CI_Controller {
	function __construct()
	{
		parent::__Construct();
		$this->load->library('user_login_check_m');
		$this->load->model('cmspages_m');
		$this->load->model('users_m');
		$this->load->model('product_m');		
	}
	function index()
	{
		$data['resPoints'] ="";
		$user_id = $this->session->userdata('user_id');
		$data['glomped_me'] = $this->users_m->mobile_glomped_me($user_id);
		$data['user_id'] = $user_id;
		$this->load->view(MOBILE_M . '/glomp_v', $data);
	}	
}//eoc
