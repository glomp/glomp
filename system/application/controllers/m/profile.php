<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->library('user_login_check_m');
        $this->load->model('cmspages_m');
        $this->load->model('product_m');
        $this->load->model('users_m');
        $this->load->model('merchant_m');
        $this->load->model('regions_m');
        $this->load->model('send_email_m');
		$this->load->model('address_m');		
    }
    /**
     * index
     * 
     * User profile landing page
     * 
     * @access public
     * @param none
     * @return view
     */
    function getMoreStories()
    {
        if($_POST && isset($_POST['start_count']))
        {
            
            $start_count=$this->input->post('start_count');
            $profile_id=$this->input->post('profile_id');            
            $from_view=$this->input->post('from_view');            
            
            $glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id,$start_count,5);
            $data['glomp_buzz']     = $glomp_buzz;
            $data['start_count']    = $start_count;
            $data['page_type']      = $from_view;
            $data['buzz_user_id'] = $this->session->userdata('user_id');
		
            // underage filter
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
        
            $this->load->view(MOBILE_M.'/user_more_stories_v', $data);
        }
    } 
    function index() {
        $profile_id = (int) $this->session->userdata('user_id');

        $start_count=0;    
        $data['start_count'] = $start_count;    
        $glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id, 0, 10);
        $glom_buzz_count = $glomp_buzz->num_rows();
        $rec_user = $this->users_m->user_info_by_id($profile_id);

        $data['buzz_user_id'] = $profile_id;
        $data['glomp_buzz'] = $glomp_buzz; //data passed to view
        $data['glom_buzz_count'] = $glom_buzz_count; //data passed to view


        if ($rec_user->num_rows() == 1) {
            $user_recod = $rec_user->row();
            $res_header = json_decode($this->user_account_m->user_summary($profile_id));
            
            //Get remaining points
            if (isset($res_header->point_balance))
                $data['points'] = $res_header->point_balance;
            else
                $data['points'] = "0";

            $profile_pic = $this->custom_func->profile_pic($user_recod->user_profile_pic, $user_recod->user_gender);

            $data['user_record'] = $user_recod;
            $data['profile_pic'] = $profile_pic;
            //list of merchant according to user regions
            $data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($user_recod->user_city_id);

            $this->load->view(MOBILE_M . '/profile_v', $data);
        } else {
            $data['error_404'] = $this->lang->line('page_404_error');
            $this->load->view('404_v',$data);
        }
    }	
	public function menu($profile_id=0)
    {
		//from=non-friend&name=sample&email=sample@sample.com&location=1
		$data="";
		$profile_id = (int) $profile_id;
		$data['thisIsOtherProfile']=true;
		$data['inTour']=$this->input->get('inTour') ;
		if($profile_id !=0)
		{				
			if ($profile_id == $this->session->userdata('user_id')) {
				$data['thisIsOtherProfile']=false;
			}			
		}
		else{
			if(isset($_GET['from']) && ($_GET['from']=='non-friend'  || $_GET['from']=='non-friend-fb' || $_GET['from']=='non-friend-li'  )){
				
			}
			else{
				$profile_id =$this->session->userdata('user_id');
				$data['thisIsOtherProfile']=false;
			}
		}				
		if($profile_id!=0){
			$rec_user = $this->users_m->user_info_by_id($profile_id);		
			if($rec_user->num_rows()==1){
				$user_recod = $rec_user->row();
				$data['user_record'] = $user_recod;
				//list of merchant according to user regions
				$data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($user_recod->user_city_id);
                // check user_record photo
                if( file_exists(dirname(BASEPATH).'/custom/uploads/users/'.$data['user_record']->user_profile_pic) ) {
                    $data['user_record']->user_icon_big = '/custom/uploads/users/'.$data['user_record']->user_profile_pic;
                    $data['user_record']->user_icon_small = '/custom/uploads/users/thumb/'.$data['user_record']->user_profile_pic;
                } else {
                    $data['user_record']->user_icon_big = '/custom/uploads/users/logo.png';
                    $data['user_record']->user_icon_small = '/custom/uploads/users/thumb/Male.png';
                }
                // user_record location string
                $q = $this->db->get_where('gl_region',array('region_id'=>$data['user_record']->user_city_id));
                if($q->num_rows() > 0 ){
                    $data['user_record']->location_string = $q->row()->region_name;
                    if($q->row()->region_parent_id != 0) {
                        $q = $this->db->get_where('gl_region',array('region_id'=>$q->row()->region_parent_id));
                        $data['user_record']->location_string = $data['user_record']->location_string . ', ' . $q->row()->region_name;
                    }
                }
			}
		}
		else{			
			
		}
		
        // underage filter
        $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
        
        $data['tabSelected']="";
		$data['tabSelectedSub']="";		
		$data['cat_id']=1;
		if( ($this->input->get('tab')=='merchants' || $this->input->get('tab')=='products' || $this->input->get('tab')=='favourites')  ){
			$data['tabSelected']=$this->input->get('tab');
		}
		if( ($this->input->get('sub')=='drinks' || $this->input->get('sub')=='snacks' || $this->input->get('sub')=='cocktails'  || $this->input->get('sub')=='sweets'  || $this->input->get('sub')=='others' )  ){
			$data['tabSelectedSub']=$this->input->get('sub');
		}
		if( ($this->input->get('sub')=='drinks' || $this->input->get('sub')=='')){ $data['cat_id']=1;}
		if( ($this->input->get('sub')=='snacks')){ $data['cat_id']=2;}
		if( ($this->input->get('sub')=='cocktails')){ $data['cat_id']=3;}
		if( ($this->input->get('sub')=='sweets')){ $data['cat_id']=4;}
		if( ($this->input->get('sub')=='others')){ $data['cat_id']=5;}
		
		$merchant_id=$this->input->get('mID');
		$mDetail="";
		$data['selectedMerchantID']=$this->input->get('mID');
		if(is_numeric($merchant_id)  &&   $this->merchant_m->is_mercahnt($merchant_id)){
			$data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
			$data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);
			$data['merchant_outlet']= $this->merchant_m->selectOutletsAddress($merchant_id);			
			
			if( ($this->input->get('mDetail')=='info' || $this->input->get('mDetail')=='locations' || $this->input->get('mDetail')=='tnc')  ){
				$mDetail=$this->input->get('mDetail');
			}
			$data['mDetail']=$mDetail;
            
            //templating
            $this->load->model('template_m');
            $data['template'] = $this->merchant_m->get_template( $merchant_id );
            if( isset($data['template']->id) ) {
                $data['sections'] = $this->template_m->_get_sections( $data['template']->id );
            }
            if( isset($data['sections']) ) {
                $data['widgets'] = $this->template_m->_get_widgets( $data['sections'] );
            }
		}
		
		//favourites action
			$data['action']=$this->input->get('action');
			$itemID =$this->input->get('del');
			if(is_numeric($itemID) ){
				$this->product_m->removeuserFevItem($itemID);				
			}
			else{				
				$itemID =$this->input->get('add');	
				if(is_numeric($itemID) ){
					$this->product_m->addFevitemToFevList($itemID);
				}
			}
		//favourites action
		//window.location.href=GLOMP_BASE_URL+'m/profile/menu/?tab=merchants&from=non-friend-fb&fbID='+id+'&first_name='+first_name+'&last_name='+last_name+'&location='+country;
		if(isset($_GET['from']) && ($_GET['from']=='non-friend'  || $_GET['from']=='non-friend-fb' || $_GET['from']=='non-friend-li'  )){
			$non_name="";			if(isset($_GET['name']))			$non_name=$_GET['name'];
			$non_first_name="";	if(isset($_GET['first_name']))	$non_first_name=$_GET['first_name'];
			$non_last_name="";	if(isset($_GET['last_name']))	$non_last_name=$_GET['last_name'];
			$non_fbID="";			if(isset($_GET['fbID']))				$non_fbID=$_GET['fbID'];
            $non_liID="";			if(isset($_GET['liID']))				$non_liID=$_GET['liID'];            
			$non_location="";		if(isset($_GET['location']))		$non_location=$_GET['location'];			
			$non_email="";		if(isset($_GET['email']))				$non_email=$_GET['email'];			
			
			if(is_numeric($non_location)){
				$location_id= $non_location;
			}
			else{
				$location_id= $this->regions_m->getCountryID($non_location);
				
			}
			$non_location_name= $this->regions_m->getCountryName($location_id);
			$data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($location_id);					
			
			$data['non_name'] 				=$non_name;
			$data['non_first_name'] 		=$non_first_name;
			$data['non_last_name'] 		=$non_last_name;
			$data['non_fbID'] 					=$non_fbID;
            $data['non_liID'] 					=$non_liID;
			$data['non_location'] 				=$location_id;
			$data['non_email'] 					=$non_email;
			$data['non_location_name'] 	=$non_location_name;
			$data['non_from'] 					= $_GET['from'];			
			//print_r( $data);
			$this->load->view(MOBILE_M . '/menu_non_member_v', $data, false);
		}
		else{
			$this->load->view(MOBILE_M . '/menu_v', $data, true, true);
		}
		
	}
    public function removeFevItem($itemID) {
        $this->product_m->removeuserFevItem($itemID);
        die('success');
    }

    public function addFevitemToFevList($itemID) {
        $this->product_m->addFevitemToFevList($itemID);
        die('success');
    }

    public function view($profile_id = 0) {
        $user_profile_id = (int) $this->session->userdata('user_id');
        $profile_id = (int) $profile_id;
        
        if($user_profile_id == $profile_id) return $this->index();
        
        $start_count=0;
		$glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id,$start_count,10);
        $data['start_count'] = $start_count;
        $glom_buzz_count = $glomp_buzz->num_rows();
        $rec_user = $this->users_m->user_info_by_id($profile_id);

        $data['buzz_user_id'] = $user_profile_id;
        $data['profile_id'] = $profile_id;
        $data['glomp_buzz'] = $glomp_buzz; //data passed to view
        $data['glom_buzz_count'] = $glom_buzz_count; //data passed to view

        // underage filter
        $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
        

        if ($rec_user->num_rows() == 1) {
            $user_recod = $rec_user->row();
            $res_header = json_decode($this->user_account_m->user_summary($profile_id));
            
            //Get remaining points
            if (isset($res_header->point_balance))
                $data['points'] = $res_header->point_balance;
            else
                $data['points'] = "0";

            $profile_pic = $this->custom_func->profile_pic($user_recod->user_profile_pic, $user_recod->user_gender);
			
            $data['user_record'] = $user_recod;
            $data['profile_pic'] = $profile_pic;
            //list of merchant according to user regions
            $data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($user_recod->user_city_id);

             if($this->session->userdata('is_user_logged_in')== false)
            {  // public profile
                $this->load->view(MOBILE_M . '/profile_friend_public_v', $data, false);
            }
            else
            {
                $this->load->view(MOBILE_M . '/profile_friend_v', $data, true, true);
            }
        } else {
            $data['error_404'] = $this->lang->line('page_404_error');
            $this->load->view('404_v',$data);
        }
    }

    function add_friend($friend_id) {
        //first of all verify user_id
        $res_num = $this->users_m->user_info_by_id($friend_id);
        $rec_num = $res_num->num_rows();
        if ($rec_num > 0) {
            $friend_added = $this->users_m->add_friend($this->session->userdata('user_id'), $friend_id);
            redirect(MOBILE_M.'/profile/view/' . $friend_id);
            exit();
        }
    }

    function add_friend2($friend_id) {
        //first of all verify user_id
        $res_num = $this->users_m->user_info_by_id($friend_id);
        $rec_num = $res_num->num_rows();
        if ($rec_num > 0) {
            $friend_added = $this->users_m->add_friend($this->session->userdata('user_id'), $friend_id);
            die('success');
        }
    }

    //This is for other user, logged in user can glomp any listed product from this merchant
    function merchantProduct($profile_id, $merchant_id) {
        $profile_id = (int) $profile_id;
        $merchant_id = (int) $merchant_id;
        $data['profile_id'] = $profile_id;
        //verify user id

        $rec_user = $this->users_m->user_info_by_id($profile_id);

        if ($this->users_m->is_user($profile_id) && $this->merchant_m->is_mercahnt($merchant_id)) {
            $data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
            $data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);
            $this->load->view('user_merchant_product_v', $data);
        } else {
            echo "";
        }
    }

    //this is for login user so we do not need user id. User just add product as favourite
    function merchantProductlist($merchant_id) {
        $user_id = $this->session->userdata('user_id');
        $merchant_id = (int) $merchant_id;
        $data['profile_id'] = $user_id;
        //verify user id

        $rec_user = $this->users_m->user_info_by_id($user_id);

        if ($this->users_m->is_user($user_id) && $this->merchant_m->is_mercahnt($merchant_id)) {
            $data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
            $data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);

            $this->load->view('my_merchant_product_v', $data);
        } else {
            echo "";
        }
    }
	function glompToNonUser(){
		 $message = $this->input->post('message');
        $password = $this->input->post('password');
        $non_first_name = $this->input->post('non_first_name');
        $non_last_name = $this->input->post('non_last_name');
		$non_name = $this->input->post('non_name');
        $non_fbID = $this->input->post('non_fbID');
        $non_liID = $this->input->post('non_liID');        
		$non_location = $this->input->post('non_location');		
		$non_email = $this->input->post('non_email');
		$non_from = $this->input->post('non_from');		
        $product_id = $this->input->post('product_id');
		
		$this->load->model('login_m');
        $userID = $this->session->userdata('user_id');
		
		
		if ($non_first_name == "" && $non_from=='non-friend-fb') {
			die("{status:'error',msg:'" . $this->lang->line('first_name_blank') . "'}");
		} else if ($non_last_name == "" && $non_from=='non-friend-fb') {
			die("{status:'error',msg:'" . $this->lang->line('last_name_blank') . "'}");
		} else if ($non_name == "" && $non_from=='non-friend') {
			die("{status:'error',msg:'" . $this->lang->line('first_name_blank') . "'}");				
		} else if ($non_from=='non-friend' && $this->custom_func->emailValidation($non_email) == "false") {
			die("{status:'error',msg:'" . $this->lang->line('invalid_email_address') . "'}");
		}		
		
		$status = $this->login_m->password_verify($password, $userID);
        if ($status == "ok") {
			if($non_from=='non-friend')
				$glomp_status = $this->user_account_m->glomp_to_friend($userID, $product_id, $message, $non_name, $non_last_name, $non_email,$non_location);
			else if($non_from=='non-friend-fb')
				$glomp_status = $this->user_account_m->glomp_to_friend_fb($userID , $product_id , $message,$non_first_name,$non_last_name,$non_fbID,$non_location);
            else if($non_from=='non-friend-li')
				$glomp_status = $this->user_account_m->glomp_to_friend_li($userID , $product_id , $message,$non_first_name,$non_last_name,$non_liID,$non_location);
            $key = json_decode($glomp_status);
            $glomp_status = $key->txn_status;
            if ($glomp_status == "invalid_friend") {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Friend') . "'}");
            } else if ($glomp_status == "SUCCESS")
            
            {
				$voucher_id="";
				if(isset($key->voucher_id))
					$voucher_id=$key->voucher_id;
                    
                $voucher_result = $this->users_m->get_specific_voucher_details($voucher_id);
                $merchant_name = $to_name_real = $form_name_real = $from_fb_id = $to_fb_id = $story_type = $prod_name = $product_logo = "";
                if($voucher_result->num_rows()>0)
                {
                    
                    $voucher_data = $voucher_result->row();
                    
                    $from_id = $voucher_data->voucher_purchaser_user_id;
                    $to_id = $voucher_data->voucher_belongs_usser_id;
                    $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                    
                    $form_name_real = $frn_info->friends->$from_id->user_name;
                    $to_name_real = $frn_info->friends->$to_id->user_name;
                    
                    $from_fb_id = $frn_info->friends->$from_id->user_linkedin_id;
                    $to_fb_id = $frn_info->friends->$to_id->user_linkedin_id;
                    
                    $story_type="1";
                    
                    //product and merchant info
                    $prod_id = $voucher_data->voucher_product_id;
                    $prod_info = json_decode($this->product_m->productInfo($prod_id));
                    $prod_name = $prod_info->product->$prod_id->prod_name;
                    $prod_image= $prod_info->product->$prod_id->prod_image;        
                    $product_logo = $this->custom_func->product_logo($prod_image);                    
                    $merchant_name = $prod_info->product->$prod_id->merchant_name;
                    
                    $details =array ('voucher_id'=>$voucher_id,
                                    'merchant_name'=>$merchant_name,
                                    'to_name_real'=>$to_name_real,
                                    'form_name_real'=>$form_name_real,
                                    'from_li_id'=>$from_fb_id,
                                    'to_fb_id'=>$to_fb_id,
                                    'story_type'=>$story_type,
                                    'prod_name'=>$prod_name,
                                    'product_logo'=>$product_logo,
                                    'status' => 'success',
                                    'msg' => $this->lang->line('Successfully_Glomped'));
                } 
                die(json_encode($details));
				//die("{status:'success',voucher_id:'".$voucher_id."',msg:'".$this->lang->line('Successfully_Glomped')."'}");                
                
                
            } else if ($glomp_status == "unexpected_error") {
                die("{status:'error',msg:'" . $this->lang->line('Unexpected_Error') . "'}");
            } else if ($glomp_status == "insufficent_balance") {
                die("{status:'error',msg:'" . $this->lang->line('Insufficent_Balance') . "'}");
            } else if ($glomp_status == "invalid_product") {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Product') . "'}");
            }
        } else {
            die("{status:'error',msg:'" . $this->lang->line('Invalid_Password') . "'}");
        }
	}

    function glompToUser($friendID = 0){
        $message = $this->input->post('message');
        $password = $this->input->post('password');
        $user_fname = $this->input->post('user_fname');
        $user_lname = $this->input->post('user_lname');
        $user_email = $this->input->post('user_email');
        $product_id = $this->input->post('product_id');
		$location_id=0;
        $this->load->model('login_m');
        $userID = $this->session->userdata('user_id');

        if ($user_fname == "") {
            die("{status:'error',msg:'" . $this->lang->line('first_name_blank') . "'}");
        } else if ($user_lname == "") {
            die("{status:'error',msg:'" . $this->lang->line('last_name_blank') . "'}");
        } else if ($this->custom_func->emailValidation($user_email) == "false") {
            die("{status:'error',msg:'" . $this->lang->line('invalid_email_address') . "'}");
        }


        $status = $this->login_m->password_verify($password, $userID);
        if ($status == "ok") {
            $glomp_status = $this->user_account_m->glomp_to_friend($userID, $product_id, $message, $user_fname, $user_lname, $user_email,$location_id);            
            $key = json_decode($glomp_status);
            $glomp_status = $key->txn_status;            
            if ($glomp_status == "invalid_friend") {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Friend') . "'}");
            } else if ($glomp_status == "SUCCESS")
            {
                $voucher_id=$key->voucher_id;   
                $voucher_result = $this->users_m->get_specific_voucher_details($voucher_id);
                $merchant_name = $to_name_real = $form_name_real = $from_fb_id = $to_fb_id = $story_type = $prod_name = $product_logo = "";
                if($voucher_result->num_rows()>0)
                {
                    
                    $voucher_data = $voucher_result->row();
                    
                    $from_id = $voucher_data->voucher_purchaser_user_id;
                    $to_id = $voucher_data->voucher_belongs_usser_id;
                    $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                    
                    $form_name_real = $frn_info->friends->$from_id->user_name;
                    $to_name_real = $frn_info->friends->$to_id->user_name;
                    
                    $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                    $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
                    
                    $story_type="1";
                    
                    //product and merchant info
                    $prod_id = $voucher_data->voucher_product_id;
                    $prod_info = json_decode($this->product_m->productInfo($prod_id));
                    $prod_name = $prod_info->product->$prod_id->prod_name;
                    $prod_image= $prod_info->product->$prod_id->prod_image;        
                    $product_logo = $this->custom_func->product_logo($prod_image);                    
                    $merchant_name = $prod_info->product->$prod_id->merchant_name;
                    
                    $details =array ('voucher_id'=>$voucher_id,
                                    'merchant_name'=>$merchant_name,
                                    'to_name_real'=>$to_name_real,
                                    'form_name_real'=>$form_name_real,
                                    'from_fb_id'=>$from_fb_id,
                                    'to_fb_id'=>$to_fb_id,
                                    'story_type'=>$story_type,
                                    'prod_name'=>$prod_name,
                                    'product_logo'=>$product_logo,
                                    'status' => 'success',
                                    'msg' => $this->lang->line('Successfully_Glomped'));
                    }                
                    die(json_encode($details));
                
            } else if ($glomp_status == "unexpected_error") {
                die("{status:'error',msg:'" . $this->lang->line('Unexpected_Error') . "'}");
            } else if ($glomp_status == "insufficent_balance") {
                die("{status:'error',msg:'" . $this->lang->line('Insufficent_Balance') . "'}");
            } else if ($glomp_status == "invalid_product") {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Product') . "'}");
            }
        } else {
            die("{status:'error',msg:'" . $this->lang->line('Invalid_Password') . "'}");
        }
    }

    function reassignVoucher() {
        $message = $this->input->post('message');
        $password = $this->input->post('password');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $voucgerID = $this->input->post('voucger_id');
        if ($fname == "") {
            die("{status:'error',msg:'" . $this->lang->line('first_name_blank') . "'}");
        } else if ($lname == "") {
            die("{status:'error',msg:'" . $this->lang->line('last_name_blank') . "'}");
        } else if ($this->custom_func->emailValidation($email) == "false") {
            die("{status:'error',msg:'" . $this->lang->line('invalid_email_address') . "'}");
        }

        $this->load->model('login_m');
        $userID = $this->session->userdata('user_id');
        $status = $this->login_m->password_verify($password, $userID);
        if ($status == "ok") {
            $glomp_status = $this->user_account_m->reassignVoucher($userID, $message, $fname, $lname, $email, $voucgerID);
            $key = json_decode($glomp_status);
            $glomp_status = $key->txn_status;

            if ($glomp_status == "SUCCESS") {
                die("{status:'success',msg:'" . $this->lang->line('Successfully_Glomped') . "'}");
            } else if ($glomp_status == "invalid_voucher") {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Product') . "'}");
            }
        } else {
            die("{status:'error',msg:'" . $this->lang->line('Invalid_Password') . "'}");
        }
    }

    /**
     * Use as /m/profile/menu/brands/$profile_id
     * 
     */
    public function brands( $profile_id, $brand_id = 0, $show_products = '', $product_id = 0 ) {
        
        include_once(FCPATH.'/system/application/controllers/brands.php');
        $brand_controller = new Brands;

        $profile_id = $this->uri->segments[5];
        $brand_id = isset($this->uri->segments[6])?$this->uri->segments[6]:0;
        $show_products = isset($this->uri->segments[7])?$this->uri->segments[7]:'';
        $product_id = isset($this->uri->segments[8])?$this->uri->segments[8]:0;
//        debug($profile_id);
//        debug($brand_id);
//        debug($show_products);
//        debug($product_id);exit;
        if($profile_id != 0) {
            $brand_controller->set_glompee( $profile_id );
        } else {
            return show_404();
        }
        
        if( $brand_id == 0 ) {
            return $brand_controller->index();
        } else {
        
            if( $show_products == '' ) {
                
                return $brand_controller->page( $brand_id );
            } else {
            
                if( $product_id == 0 ) {
                    $brand_controller->set_subpage('products');
                    return $brand_controller->page( $brand_id );
                } else {
                    
                    $brand_controller->set_subpage('products');
                    $brand_controller->set_product_id( $product_id );
                    return $brand_controller->page( $brand_id );
                }
            }
        }
        
        
    }
}
