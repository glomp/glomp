<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_api extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		$this->load->library('custom_func');		
		$this->load->library('curl');
		$this->load->library('facebook');
    }
	
    function login()
    {
		$fb_config = array(
			'appId'  => ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS)),
			'secret' => ($this->custom_func->config_value('FB_API_APP_SECRET_'.FACEBOOK_API_STATUS)),
			'fileUpload' => true,
			'cookie' => true
		);
		$facebook  = new Facebook($fb_config);		
		$user = $this->facebook->getUser();			
		if ($user) {
			try {
			// Proceed knowing you have a logged in user who's authenticated.
				$user_profile = $facebook->api('/me');
			} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
		}
		// Login or logout url will be needed depending on current user state.
		if ($user) {
			$logoutUrl 	= $facebook->getLogoutUrl(
				array(
					'redirect_uri'  => base_url(MOBILE_M.'/logout')
				));
		} else {
			$loginUrl 		=  $facebook->getLoginUrl(
				array(
					'scope'         => 'publish_actions,email',
					'redirect_uri'  => base_url(MOBILE_M)
				)
			);
		}
        //$this->load->view('facebook_api_v',$data);
    }
}//eoc