<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;

    function __construct()
    {
        parent::__Construct();
        $this->load->library('user_login_check_m');
        $this->load->model('cmspages_m');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->model('regions_m');
        $this->load->model('login_m');
        $this->load->model('send_email_m');
        $this->load->library('email_templating');
		$this->load->library('custom_func');
        $this->load->model('country_m');
        
    }

    function index() {

        $this->dashboard();
    }
    function getUserDetails($user_id)
    {
        $user_id=(int) $user_id;
        
        if($this->session->userdata('from_android')=='true' && $this->session->userdata('user_id')==$user_id )
        {
            $rec_user = $this->users_m->user_info_by_id($user_id);
            $user_record = $rec_user->row();
            $dob = explode('-',$user_record->user_dob);
            if(count($dob)!==3){
                $dob[0] = 0;$dob[1] = 0;$dob[2] = 0;
            }
            
            if (empty($user_record->user_dob_display)) {
                $user_record->user_dob_display = 'dob_display_w_year';
            }
            //print_r($user_record);
            $profile_pic = base_url().$this->custom_func->profile_pic($user_record->user_profile_pic,$user_record->user_gender);
            
            
            $data[]= array('elem_id' => 'fname',                'val' => $user_record->user_fname);
            $data[]= array('elem_id' => 'lname',                'val' => $user_record->user_lname);
            
            $data[]= array('elem_id' => 'day',                  'val' => $dob[2]);
            $data[]= array('elem_id' => 'months',               'val' => $dob[1]);
            $data[]= array('elem_id' => 'year',                 'val' => $dob[0]);
            
            $data[]= array('elem_id' => 'dob_display_w_year',   'val' => $user_record->user_dob_display);//dob_display_wo_year
            $data[]= array('elem_id' => 'location',             'val' => $user_record->user_city_id);
            $data[]= array('elem_id' => 'profile_pic',          'val' => $profile_pic);        
            $data[]= array('elem_id' => 'email',                'val' => $user_record->user_email);
            
            
            $response = array(
                'success' => 'OK',
                'errors' => $this->session->userdata('from_android').'0',
                'data' => $data
                
            );
            echo 'populateUserDetails('.json_encode($response).');';        
        }
        
    }
    public function updateJSONP() {
        $user_id = $this->session->userdata('user_id');
         $response = array(
            'success' => FALSE,
            'errors' => array()
        );
        
        $_POST = $_GET;
        $this->form_validation->set_rules('fname', 'first Name', 'trim|required|xss_clean|callback_fname_check');
        $this->form_validation->set_rules('lname', 'last Name', 'trim|required|xss_clean|callback_lname_check');
        $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
        $this->form_validation->set_rules('months', 'Months', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');            
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
        if ($this->input->post('pword') != "")
        {
            $this->form_validation->set_rules('pword', 'Current Password', 'trim|required|min_length[6]|max_length[9]|callback_password_check|xss_clean');
        }
        if ($this->input->post('npword') != "")
        {
            $this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
            $this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
        }
        $this->form_validation->run();
        $response['errors']['fname'] = form_error('fname');
        $response['errors']['lname'] = form_error('lname');
        $response['errors']['day'] = form_error('day');
        $response['errors']['months'] = form_error('months');
        $response['errors']['year'] = form_error('year');
        $response['errors']['location'] = form_error('location');
        $response['errors']['email'] = form_error('email');
        $response['errors']['pword'] = form_error('pword');
        $response['errors']['cpword'] = form_error('cpword');
        $response['errors']['agree'] = form_error('agree');
        $response['errors']['profile_pic'] = '';        
                
       
        if ($this->form_validation->run() == FALSE)
        {
            echo 'process_error('.json_encode($response).');';        
            
        }
        else
        {// update profile
            $this->login_m->update_profile($user_id);
             $response['success'] = TRUE;
            $response['location'] = site_url(MOBILE_M . '/landing/');
            $response['user_id'] = $user_id;

            echo 'process_succcess('.json_encode($response).');'; 
        
        }// update profile
    
    }
    
    function redirectMe() {
        $friend_id = $this->session->userdata('redirect_friends_profile_id');
        $redirect_noti = $this->session->userdata('redirect_notification');
        if (! empty($friend_id)) {
            //When email recieve from added as a friend, then redirect to friend profile page
            $url = site_url(MOBILE_M.'/profile/view/'.$friend_id);
            //remove session when redirected.
            $this->session->unset_userdata('redirect_friends_profile_id');
            return redirect($url);
        }
        if (! empty($redirect_noti)) {
            //When email recieve then clicked on unsubscribe link redirect to settings page
            $url = site_url(MOBILE_M.'/user/update/?show_notification=1#email');
            //remove session when redirected.
            $this->session->unset_userdata('redirect_notification');
            return redirect($url);
        }        
    }
    function getMoreStories()
    {
        if($_POST && isset($_POST['start_count']))
        {
            $start_count=$this->input->post('start_count');
            $user_id = $this->session->userdata('user_id');
            $glomp_buzz = $this->users_m->glomp_buzz($user_id, $start_count, 5);
            $data['glomp_buzz'] = $glomp_buzz;
            $data['start_count'] = $start_count;
            $data['buzz_user_id'] = $user_id;
            $data['page_type']      = 'dashboard';
            // underage filter  
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
            $this->load->view(MOBILE_M . '/user_more_stories_v', $data);
        }
    }
    public function dashboard() {
        $this->redirectMe();
        
        $data['page_title'] = $this->session->userdata('user_name');
        $user_id = $this->session->userdata('user_id');
        //select 10 random friends
        $data['random_friends'] = $this->users_m->random_friends($user_id);
        //
        $glomp_buzz = $this->users_m->glomp_buzz($user_id, 0, 10);
        $data['buzz_user_id'] = $user_id;
        $data['start_count'] = 0;
        $data['glomp_buzz'] = $glomp_buzz; //data passed to view
        $glom_buzz_count = $glomp_buzz->num_rows();
        $data['glom_buzz_count'] = $glom_buzz_count; //data passed to view
        //jiofreed@facebook.com
        //received glomped or glomped  me
        $data['glomped_me'] = $this->users_m->glomped_me($user_id); //zachary.lai
        $data['user_start_tour'] = $this->users_m->get_tour_status($user_id); //iam.mee.7923@facebook.com//
		$user_promo_code = $this->users_m->get_promo_code_status($user_id); //iam.mee.7923@facebook.com//
		
		if($user_promo_code=="")
		{
		
		}
		else{
			$user_promo_code=json_decode($user_promo_code);
		}
		$res_info_prod="";		
		if( isset($user_promo_code->status) && 	($user_promo_code->status=='SuccessPrimary' || $user_promo_code->status=='SuccessFollowUp'))
		{
			$sql ="SELECT voucher_product_id, voucher_type FROM gl_voucher WHERE voucher_id ='".$user_promo_code->voucher_id."'";
			$result = $this->db->query($sql);					
			$result	= $result->row();
			$res_info_prod = $this->product_m->productInfo_2($result->voucher_product_id);
                        $res_info_prod = json_decode($res_info_prod, TRUE);
                        //Add voucher type
                        $res_info_prod['product']['voucher_type'] = $result->voucher_type;
			
		}		
		$promo_rule_desc='';
        if(isset($user_promo_code->campaign_id) && is_numeric($user_promo_code->campaign_id))
            $promo_rule_desc = $this->users_m->get_promo_rule_desc($user_promo_code->campaign_id);						
		$data['res_info_prod']=$res_info_prod;
		$data['user_promo_code']=$user_promo_code;
        $data['promo_rule_desc']=$promo_rule_desc;
		
		$data['inTour']=$this->input->get('inTour') ;
		$data['is_connected_to_fb'] = $this->users_m->is_connected_to_fb($user_id); //iam.mee.7923@facebook.com//
        $not_claim_voucher = $this->session->userdata('not_claim_voucher_notification');
        
        if (empty($not_claim_voucher)) {
            $not_claim_voucher = '';
        }
        //unset no claim message
        $this->session->unset_userdata('no_claim_message');
		$data['not_claim_message'] = $not_claim_voucher;
        $this->load->view(MOBILE_M . '/user_dashboard_v', $data, true);
    }
	public function facebookFriends(){
		$user_id = $this->session->userdata('user_id');		
		$data['user_short_info']=($this->user_account_m->getUserFname($user_id));
        $data['countries_json'] = $this->country_m->get_json_list();
        $data['get_type']='facebook';
		$this->load->view(MOBILE_M . '/user_facebook_friends_v', $data, false);
	}
    public function linkedinFriends(){
		$user_id = $this->session->userdata('user_id');		
		$data['user_short_info']=($this->user_account_m->getUserFname($user_id));
        $data['countries_json'] = $this->country_m->get_json_list();
        $data['get_type']='linkedin';
		$this->load->view(MOBILE_M . '/user_facebook_friends_v', $data, false);
	}
    
    
    public function searchFriends() {
	
		//remove friend from friend list
		if(isset($_GET['remove_friend_id']))
		{
				$remove_friend_id = $_GET['remove_friend_id'];
				$removed = $this->users_m->remove_friend($this->session->userdata('user_id'), $remove_friend_id);		
		}
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['location'] = $this->input->post('location');
        $data['location_id'] = $this->input->post('location_id');
        $data['edit'] = isset($_GET['edit']) ? '1' : '0';
        $data['glomp'] = isset($_GET['glomp']) ? '1' : '0';
        
        $keywords = $this->input->get('keywords');
			$data['keywords'] = $keywords;
			if($keywords!="")
			{
			$subQuery  = "AND (user_fname LIKE '%$keywords%' OR user_Lname LIKE '%$keywords%' OR
					 CONCAT(user_fname,' ',user_lname) LIKE '%$keywords%')";
			$data['page_level']  = "Search Results";
			}
			else
			{
				$subQuery ="";
				$data['page_level']  = "Friends";
			}
		
			$data['resFriendList'] = $this->users_m->searchOwnFriends($keywords);// $subQuery."GROUP BY u.user_id	 ORDER BY user_fname ASC");		
		$user_id = $this->session->userdata('user_id');		
		$data['user_short_info']=($this->user_account_m->getUserFname($user_id));
        
        
        
        //$data['resFriendList'] = $this->users_m->searchOwnFriends($subQuery . "ORDER BY user_fname ASC");
        
        $user_id = $this->session->userdata('user_id');        
        $this->load->view(MOBILE_M . '/user_friends_v', $data);
    }

    public function inviteFriends() {

        $subQuery = "";
        $submit_btn = $this->input->post('submit_btn');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $location = $this->input->post('location');
		$message = $this->input->post('message');
        
        $data['glomp_only'] = (isset($_GET['glomp_only']) && $_GET['glomp_only'] == '1') ? '1' : '0';
        $data['name'] = $name;
        $data['email'] = $email;
        $data['location'] = $location;
		$data['message'] = $message;
        $location = ($location != "") ? $location : 0;

        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
        
        //if invite is clicked send email
        if ($submit_btn == 'invite') {
            if ($this->form_validation->run() == TRUE) {
                //check email address which is alread in database or not	

                $res_email_check = $this->users_m->email_exist($email);
                if ($res_email_check->num_rows() > 0) {
                    $subQuery = " AND user_email = '$email'";
                    $data['email_exist_true'] = true;
                    $data['alert'] = $this->lang->line('already_in_friend_list');
                } else {
                    //not exist into the database send invitation email]
                    $to_email = $email;
                    $to_name = $name;
                    $email_message = $this->input->post('message');
                    $invite_subject = $this->session->userdata('user_name') . ' has invited you to glomp!';
                    $invite_message = "<br/>";
                    $invite_message.= 'It\'s time for a treat! ' . $this->session->userdata('user_name') . ' has added you as a friend on glomp! ';
                    $invite_message .= "<p>glomp! is a new online and mobile social networking platform where you can give and receive enjoyable treats such as a coffee, ice-cream, a beer and so on from a host of quality retail and service outlets. </p><br/><br/>So get started and <a href='" . base_url() . "'>sign up now!<a/>";
                    if ($invite_message != "") {
                        $invite_message .= '<br/>';
                        $invite_message .= '<br/><p>' . $email_message . '</p>';
                    }
                    $invite_message .= '<br/>
                    Cheers.
                    <br/>
                    The glomp! team.<br/>';

                    $this->send_email_m->sendEmail($this->site_sender_email, $this->site_sender_name, $to_email, $to_name, $invite_subject, $invite_message);
                    $data['alert'] = $this->lang->line('invitation_email_msg_sent');
                    $name = "";
                    $email = "";
                    $location = "";
                    $message = "";

                    /// to clear the fields after sending email.
                    $data['name'] = "";
                    $data['email'] = "";
                    $data['location'] = "";
                    $data['location_id'] = "";
                }
            }
        }
        
        if ($submit_btn == 'glomp') {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|xss_clean');
            //$this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            
            if ($this->form_validation->run() == TRUE) {
                return $this->glompToNonMember($data);
            }
        }
        
        $user_id = $this->session->userdata('user_id');

        $this->load->view(MOBILE_M.'/user_friends_invite_v', $data);
        
    }
    
    
    function glompToNonMember($data = array()) {
        
        $subQuery = "";
        $name = $data['name'];
        $email = $data['email'];
        $location = $data['location'];
		
		
        
        if ($data['name'] != "") {
            $subQuery.= "AND (user_fname LIKE '%$name%' OR user_lname LIKE '%$name%')";
        }
        if ($email != "") {
                $subQuery.= "AND user_email LIKE '%$email%'";
        }
        if ($location > 0) {
            $subQuery.= "AND user_city_id = '$location'";
        }

        if ($location > 0) {
            $sql = "SELECT region_parent_path_id FROM gl_region WHERE region_id='" . $location . "'";
            $result = $this->db->query($sql);
            $recPath = $result->row();
            $path = $recPath->region_parent_path_id;
            $subQuery.=" AND region_parent_path_id LIKE '$path%'";
        }
        
        $data['resFriendList'] = $this->users_m->searchForFriends($name,$email,$location );        
        $data['resFriendList_count'] = $data['resFriendList']->num_rows();
        
//        echo '<pre>';
//        print_r($data['resFriendList']->result());
        
        $region = $this->regions_m->selectRegionByID($location)->row();
		$data['name'] = $name;
        $data['email'] = $email;
        $data['location'] = $location;
        $data['location_name'] = $region->region_name;
        

        $this->load->view(MOBILE_M.'/user_nonfriends_glomp_v', $data);
    }

    public function merchantProduct($id) {
        $user_id = $this->session->userdata('user_id');
        $this->load->view('user_merchant_product_v');
    }

    public function logOut() {
        $this->session->set_userdata('is_user_logged_in', false);
        if($this->session->userdata('admin_id') && $this->session->userdata('is_logged_in')==true)
        {
            
            $array_items = array(            
                'user_id' => '',
                'username' => '',
                'user_name' => '',
                'user_email' => '',
                'user_fname' => '',
                'user_lname' => '',
                'user_email_verified' => '',
                'user_last_login_date' => '',
                'user_last_login_ip' => '',
                'lang_id' => '',
                'twitter_oauth_token' => '',
                'twitter_oauth_token_secret' => '',
                'is_user_logged_in' => '',   
            );
            $this->session->unset_userdata($array_items);                
        }
        else
        {
                $array_items = array(
                    'user_data' => '',
                    'lang_id' => '',
                    'user_id' => '',
                    'username' => '',
                    'user_name' => '',
                    'user_fname' => '',
                    'user_email' => '',
                    'user_last_login_date' => '',
                    'user_last_login_ip' => '',
                    'twitter_oauth_token' => '',   
                    'twitter_oauth_token_secret' => '',   
                    'is_user_logged_in' => '',   
                    'since_user_logged_in' => '',   
                    'since_logged_in' => '',   
                );
                
                $this->session->unset_userdata($array_items);
                //$this->session->sess_destroy();
        }
        $this->load->library('user_agent');
        if ($this->agent->is_referral())
        {
            //redirect($this->agent->referrer());
            redirect(MOBILE_M . '/landing');
        }
        else
            redirect(MOBILE_M . '/landing');
    }

    public function update() {
        $user_id = $this->session->userdata('user_id');

        if (isset($_POST['update_profile'])) {
            $user = $this->db->get_where('gl_user', array('user_id'=> $user_id));
            $user_photo = ($user->row()->user_profile_pic == NULL) ? $user->row()->user_profile_pic_orig : $user->row()->user_profile_pic;
            $user_photo = ($user_photo == NULL) ? FALSE : TRUE;

            $this->form_validation->set_rules('fname', 'first Name', 'trim|required|xss_clean|callback_fname_check');
            $this->form_validation->set_rules('lname', 'last Name', 'trim|required|xss_clean|callback_lname_check');
            $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
            $this->form_validation->set_rules('months', 'Months', 'trim|required');
            $this->form_validation->set_rules('year', 'Year', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
            $this->form_validation->set_rules('user_lang_id', $this->lang->line('language', 'Language'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');

            if ($this->input->post('pword') != "") {
                $this->form_validation->set_rules('pword', 'Current Password', 'trim|required|min_length[6]|max_length[9]|callback_password_check|xss_clean');
            }
            if ($this->input->post('npword') != "") {
                $this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
                $this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
            }
            
            if ( isset($_FILES['user_photo']['tmp_name']) && $_FILES['user_photo']['tmp_name'] == "" && $user_photo == FALSE) {
                $this->form_validation->set_rules('user_photo', 'Profile Photo', 'required');
            }

            if ($this->form_validation->run() == TRUE) {
                $this->login_m->update_profile($user_id);
                //
                $data['success_msg'] = $this->lang->line('your_profile_udpated_successfully').'';
                if (strlen($this->input->post('npword')) >= 6) {
                    //change password
                    $new_password = $this->input->post('npword');
                    $this->login_m->change_password($new_password, $user_id);
                }
                //update profile pic
                /* upload photo start */
                if (isset($_FILES['user_photo']['tmp_name'] ) && $_FILES['user_photo']['tmp_name'] != "") {
				//$data['success_msg'] = $this->lang->line('your_profile_udpated_successfully').'asdasd';
                    $config = array(
                        'allowed_types' => 'jpeg|jpg|gif|png',
                        'upload_path' => 'custom/uploads/users/',
                        'maximum_filesize' => 2
                    );

                    $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
                    $uploadError = preg_match("/^Error/", $user_photo) ? $user_photo : NULL;
                    if (empty($uploadError)) {
                        try
                        {
                            list($width, $height) = getimagesize('custom/uploads/users/'.$user_photo);                        
                            $newName=mt_rand(1,10000)."_".$user_photo;
                            
                            
                            if($height<140 || $width<140)
                            {
                                $uploadError="Invalid image file size.";
                                
                            }
                            
                            if($width>1024){
                                $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                    
                            }
                            if($height>720) 
                                $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);
                            
                            if(file_exists("custom/uploads/users/".$newName)){
                                $user_photo=$newName;                    
                            }
                            else
                            {
                                $user_photo=$user_photo;
                            }                            
                        }
                        catch (Exception $e)
                        {
                            if( ($this->config->item('log_threshold')) > 0 )
                            {
                                $this->log_message('error', $e->getMessage());
                            }
                        }
                    
                        
                        
                        if(!isset($uploadError) || (isset($uploadError) && $uploadError=="") )
                        {
                            $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);
                            
                            
                            $this->login_m->updatePhoto($user_photo, $user_id);
                            
                            $user_photo_orig=$user_photo;
                            $this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
                        }
                        else
                        {
                            $data['uploadEerror'] = $uploadError;
                            unset($data['success_msg']);
                            
                        }
                    } else {
                        $data['uploadEerror'] = $uploadError;
                        unset($data['success_msg']);
                        //exit();
                    }
                }//user photo temp check
                /*                 * ****upload photo end** */
            }
        }
        $rec_user = $this->users_m->user_info_by_id($user_id);
        $user_recod = $rec_user->row();
        $data['user_record'] = $user_recod;
        $this->load->view(MOBILE_M.'/update_profile_v', $data, false);
    }

    function email_check($email) {
        $email_chk = $this->login_m->update_email_exist_check($email, $this->session->userdata('user_id'));
        if ($email_chk == 0)
            return true;
        else {
            $this->form_validation->set_message('email_check', 'Email address already exist. Please try new one');
            return false;
        }
    }

    function password_check($password) {
        if ($password == "")
            return 'ok';
        //
        $password_verify = $this->login_m->password_verify($password, $this->session->userdata('user_id'));
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check', 'Invalid current password.');
            return false;
        }
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption =  array('crop', 'l'), $sharpen = true) {        
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }
    function fname_check($name){

    $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
    if (str_contains($name, $not_allowed) == false)
    {    
        //    echo "Error: Not allowed letters: ".$not_allowed;
        //todo language translation
        $this->form_validation->set_message('fname_check', 'Special characters are not allow on First Name.');
        return false;
    }
    else{
        //  echo "ok:".$username; 
        $not_allowed='1234567890';//
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            $this->form_validation->set_message('fname_check', 'Numbers are not allow on First Name.');
            return false;
        }
        else{
          //  echo "ok:".$username; 
          return true;                
        }            
    }    
}
    function lname_check($name){

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('lname_check', 'Special characters are not allowed on Last Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('lname_check', 'Numbers are not allowed on Last Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }

}

//eoc
