<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing extends CI_Controller {

    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;
    
    private $site_support_email = SITE_DEFAULT_SUPPORT_EMAIL;
    private $site_support_name = SITE_DEFAULT_SUPPORT_NAME;

    function __construct() {
        parent::__Construct();
        $this->load->model('login_m');
        $this->load->model('regions_m');
        $this->load->model('cmspages_m');
        $this->load->model('send_email_m');
		$this->load->model('users_m');

        $this->load->library('custom_func');
        $this->load->library('email_templating');
		
		$this->load->model('campaign_m');
		$this->load->model('product_m');		
        $this->load->library('user_agent');

    }
    function facebook_callback()
    {
        if ( isset($_GET['from_android']) && $_GET['from_android']=='true')
        {
           $this->session->set_userdata('from_android', $_GET['from_android']);
        }
        $data['page_title'] = $this->lang->line('landing_page_title');
        
                
        if($this->session->userdata('facebok_callback_from_url')!='')
        {
            $what_to_do=$this->session->userdata('facebok_callback_what_to_do');                     
            $array_items = array(        
                'facebok_callback_what_to_do' => '',
                'facebok_callback_from_url' => ''                
            );
            $this->session->unset_userdata($array_items);
            
            redirect(base_url(MOBILE_M.'/user/facebookFriends?what_to_do='.$what_to_do));            
        }
        else
        {
            
            $this->load->view(MOBILE_M . '/facebook_callback_v', $data, false, false);
        }        
    }
    function test_upload()
    {
        $data[''] ="";
        $this->load->view(MOBILE_M . '/test_upload', $data,false, false);    
    }

    function verify($code)
    {
        $user_id = 0;
        if (isset($_GET['uid']) && is_numeric($_GET['uid']))
            $user_id = $_GET['uid'];

        //echo $user_id.'-'.$code; 
        if (!$this->login_m->_verify($code, $user_id))
        {
            //return show_404();
        }
        $this->session->set_flashdata('verified', 'Email has been verified. Please login to your account');
        return redirect(site_url() . 'm');
    }

    public function resendVerification() {
        $to_email = '';
        if (isset($_GET['email']))
            $to_email = $_GET['email'];


        $data['user_name'] = $to_email;
        $data['Page_title'] = $this->lang->line('landing_page_title');
        $this->load->helper('email');
        if (valid_email($to_email)) {
            $tempResult = $this->login_m->check_if_registered_email($to_email);
            if ($tempResult->num_rows() > 0) {
                $resData = $tempResult->row();
                $to_name = $resData->user_fname . ' ' . $resData->user_lname;
                $link = site_url('m/landing/verify/' . $resData->user_hash_key . '?uid=' . $resData->user_id);
                $click_here = "<a href='$link'>click</a>";
                $vars = array(
                    'template_name' => 'new_signup',
                    'lang_id' => 1,
                    'to' => $to_email,
                    'params' => array(
                        '[link]' => $click_here
                    )
                );
                $this->email_templating->config($vars);
                $this->email_templating->send();
                $this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
                redirect(MOBILE_M . '/landing/');
            } else {
                $data['error_msg'] = "Invalid E-mail.";
            }
        } else {
            $data['error_msg'] = "Invalid E-mail.";
        }
        $this->load->view(MOBILE_M . '/landing_v', $data);
    }

    public function index($hash = 0) {
        
        if (isset($_GET['deactivated']))
        {  
            $this->session->set_flashdata('deactivated', 'Your account has been deactivated.');
            redirect(base_url(MOBILE_M));
        }
        
        //check if the connection is coming from android app
        if ( isset($_GET['from_android']) && $_GET['from_android']=='true')
        {
           $this->session->set_userdata('from_android', $_GET['from_android']);
        }
        
        
        #if user loged in redirect to dashboard
        if ($this->session->userdata('is_user_logged_in')) {
            redirect(MOBILE_M . '/user/dashboard');
            exit();
        }

        $data['page_title'] = $this->lang->line('landing_page_title');
        if (isset($_POST['user_login'])) {
            $this->form_validation->set_rules('user_name', 'E-mail', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');

            if ($this->form_validation->run() == TRUE) {
                $user_name = $this->input->post('user_name');
                $user_password = $this->input->post('user_password');
                $login_result = $this->login_m->loginValidation($user_name, $user_password);

                $login = json_decode($login_result);
                if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
				
					if($this->input->post('referrer') !="")
					{
						redirect($this->input->post('referrer'));
						exit();					
					}
					
                    redirect(MOBILE_M . '/user/dashboard');
                    exit();
                }
                else if(isset($login->deactivated_status) && $login->deactivated_status==1 && $login->user_status == 'Pending')
                {
                    /*if deactivated*/
                    $update_data = array(
                        'user_status'               => 'Active',
                        'deactivated_status'        => 0
                    );
                    $where = array(
                            'user_id' 		=>$login->user_id
                            );
                    $this->db->update('gl_user', $update_data, $where);
                    
                    $login_result = $this->login_m->loginValidation($user_name,$user_password);
                    $login = json_decode($login_result);
                    
                    $this->session->set_userdata('reactivated', 'true');
                    redirect('m/user/dashboard/');
                    exit();
                } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                    $link = base_url('/m/landing/resendVerification/?email=' . $user_name);
                    $click_here = "<a href='$link'>Click here</a>";
                    $data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                } else {
                    $data['error_msg'] = "Invalid E-mail or password";
                }
            }
        } else if (isset($_POST['fb_login'])) {
            /* foreach ($_POST as $key => $value) {      
              echo "<div>";
              echo $key;
              echo ":";
              echo $value;
              echo "</div>";
              } */

            $user_fb_id = "";
            $user_email = "";
            $user_name = $this->input->post('user_email');
            if (isset($_POST['id']) && isset($_POST['email'])) {
                $user_fb_id = $_POST['id'];
                $user_email = $_POST['email'];
            }

            if ($this->login_m->check_if_fb_user_is_registered($user_fb_id) == 0) {
                $tempResult = $this->login_m->check_if_fb_user_is_registered_email($user_email);
                if ($tempResult->num_rows() == 0) {
                    $this->session->set_userdata('temp_user_fb_id', $_POST['id']);
                    $this->session->set_userdata('temp_fname', $_POST['first_name']);
                    $this->session->set_userdata('temp_mname', $_POST['middle_name']);
                    $this->session->set_userdata('temp_lname', $_POST['last_name']);

                    $this->session->set_userdata('temp_gender', ucfirst($_POST['gender']));
                    $this->session->set_userdata('temp_email', $_POST['email']);

                    $dob = strtotime($_POST['birthday']);
                    $this->session->set_userdata('temp_day', date('d', $dob));
                    $this->session->set_userdata('temp_months', date('m', $dob));
                    $this->session->set_userdata('temp_year', date('Y', $dob));
                    redirect(MOBILE_M . '/user/register/facebook?fb_user_id='.$this->input->post('id').'&fb_email='.$this->input->post('email').'&fb_fname='.$this->input->post('first_name').'&fb_mname='.$this->input->post('middle_name').'&fb_lname='.$this->input->post('last_name').'&fb_gender='.$this->input->post('gender').'&fb_birthdate='.$dob);
                } else {
                    $resData = $tempResult->row();
                    if ($user_fb_id == $resData->user_fb_id) {
                        $login_result = $this->login_m->login_using_fb_id($user_fb_id);
                        $login = json_decode($login_result);
                        if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                            redirect(MOBILE_M . '/user/dashboard');
                            exit();
                        } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                            $link = base_url('m/landing/resendVerification/?email=' . $user_email);
                            $click_here = "<a href='$link'>Click here</a>";
                            $data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                        } else {
                            $data['error_msg'] = "Invalid E-mail or password";
                        }
                    } else {
                        //facebook email has a glomp account but not connected with facebook
                        //$data['error_msg'] = "Invalid E-mail or passworasdasdasdd";	
                        //update the user account and connect them with facebook
                        $this->login_m->user_fb_login_update_account($resData->user_id);
                        $login_result = $this->login_m->login_using_fb_id($user_fb_id);
                        $login = json_decode($login_result);
                        if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                            redirect(MOBILE_M . '/user/dashboard');
                            exit();
                        } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                            $link = base_url('m/landing/resendVerification/?email=' . $user_email);
                            $click_here = "<a href='$link'>Click here</a>";
                            $data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                        } else {
                            $data['error_msg'] = "Invalid E-mail or password";
                        }
                    }
                }
            } else {
                //get username then login
                $login_result = $this->login_m->login_using_fb_id($user_fb_id);
                $login = json_decode($login_result);
                if($login_result==false )
                {
                    $this->session->set_userdata('temp_user_fb_id', $_POST['id']);
                    $this->session->set_userdata('temp_fname', $_POST['first_name']);
                    $this->session->set_userdata('temp_mname', $_POST['middle_name']);
                    $this->session->set_userdata('temp_lname', $_POST['last_name']);

                    $this->session->set_userdata('temp_gender', ucfirst($_POST['gender']));
                    $this->session->set_userdata('temp_email', $_POST['email']);

                    $dob = strtotime($_POST['birthday']);
                    $this->session->set_userdata('temp_day', date('d', $dob));
                    $this->session->set_userdata('temp_months', date('m', $dob));
                    $this->session->set_userdata('temp_year', date('Y', $dob));
                    redirect(MOBILE_M . '/user/register/facebook?fb_user_id='.$this->input->post('id').'&fb_email='.$this->input->post('email').'&fb_fname='.$this->input->post('first_name').'&fb_mname='.$this->input->post('middle_name').'&fb_lname='.$this->input->post('last_name').'&fb_gender='.$this->input->post('gender').'&fb_birthdate='.$dob);
                    //redirect(MOBILE_M . '/landing/register_form');
                }
                else if($login->user_status=='Active')
                {
                    if (isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE) {
                        redirect(MOBILE_M . '/user/dashboard');
                        exit();
                    } else if (isset($login->user_email_verified) && $login->user_email_verified == 'N') {
                        $link = base_url('m/landing/resendVerification/?email=' . $user_email);
                        $click_here = "<a href='$link'>Click here</a>";
                        $data['error_msg'] = "Please check your welcome email to activate your account. " . $click_here . " to resend the verification.";
                    } else {
                        $data['error_msg'] = "Invalid E-mail or password";
                    }
                }
                else if($login->user_status=='Pending')
                {
                    /*check if deactivated*/
                    $q = $this->db->get_where( 'gl_user', "user_id = '".$login->user_id."' AND deactivated_status=1");
                    if( $q->num_rows()  > 0)
                    {
                        //return $q->row();
                        $update_data = array(
                            'user_status'               => 'Active',
                            'deactivated_status'        => 0
                        );
                        $where = array(
                                'user_id' 		=>$login->user_id
                                );
                        $this->db->update('gl_user', $update_data, $where);
                        
                        $login_result = $this->login_m->login_using_fb_id($user_fb_id);			
                        $login = json_decode($login_result);
                        
                        $this->session->set_userdata('reactivated', 'true');
                        redirect('m/user/dashboard/');
                        exit();
                        
                    }
                    //deactivated_status
                    /*check if deactivated*/
                    
                    $this->session->set_userdata('temp_user_fb_id', $_POST['id']);
                    $this->session->set_userdata('temp_fname', $_POST['first_name']);
                    $this->session->set_userdata('temp_mname', $_POST['middle_name']);
                    $this->session->set_userdata('temp_lname', $_POST['last_name']);

                    $this->session->set_userdata('temp_gender', ucfirst($_POST['gender']));
                    $this->session->set_userdata('temp_email', $_POST['email']);

                    $dob = strtotime($_POST['birthday']);
                    $this->session->set_userdata('temp_day', date('d', $dob));
                    $this->session->set_userdata('temp_months', date('m', $dob));
                    $this->session->set_userdata('temp_year', date('Y', $dob));
                    redirect(MOBILE_M . '/user/register/facebook?fb_user_id='.$this->input->post('id').'&fb_email='.$this->input->post('email').'&fb_fname='.$this->input->post('first_name').'&fb_mname='.$this->input->post('middle_name').'&fb_lname='.$this->input->post('last_name').'&fb_gender='.$this->input->post('gender').'&fb_birthdate='.$dob);
                
                }
            }
        }
        
        //When posted to URL
        //Please follow: controller: ajax_post; func: checkIntegratedAppConnected
        if(isset($_POST['linkedin_login'])) $data = array_merge($data, $_POST);
        
        $data['is_voucher_okay']=false;
        $data['already_member']='false';
        $data['thru_sms'] = 'false';
        $data['puid'] = '';
        $data['voucher_data'] = '';
        $data['recipient_email'] = '';
        $data['voucher_from'] = '';

        //testing of already member dialog box
        /*$data['is_voucher_okay']=true;
        $data['already_member']='true';
        $data['thru_sms'] = 'false';
		$data['puid'] = '';
		$data['voucher_data'] = '';
        $data['recipient_email'] = 'allan.bernabe@gmail.com';
        $data['voucher_from'] = '';*/

		if(isset($_GET['voucher'])){
			$voucher=$_GET['voucher'];
			$voucherCheck=$this->login_m->checkIfThisVoucherExist($voucher);
			if($voucherCheck->num_rows()>0){
				$tempData=$voucherCheck->row();
				$data['is_voucher_okay']=true;
				$from_user=$this->users_m->user_info_by_id($tempData->voucher_purchaser_user_id);
				$from_user=$from_user->row();
				$from_user=$from_user->user_fname;
				$data['voucher_from']=$from_user;
			}			
		}

        //glomp thru sms
        if ( ! empty($hash) ) {
            $this->load->model('voucher_m'); //load voucher model

            //prepare code
            $hash_key = substr($hash, 0, 4);
            $mobile_number = substr($hash, -4);

            //Check if there's an existing account for SMS
            $this->db->like('user_hash_key', $hash_key, 'after');
            $this->db->like('user_mobile', $mobile_number, 'before');
            $user = $this->db->get('gl_user');

            if ($user->num_rows() == 0) {
                //do nothing
                return;
            }

            $data['is_voucher_okay'] = TRUE;
            if ($user->row()->user_status == 'Active') {
                $data['already_member'] = 'true';
                $data['recipient_email'] = $user->row()->user_email;

                return $this->load->view(MOBILE_M . '/landing_v', $data, false, false);
            }

            $voucherCheck = $this->voucher_m->get_record(array(
                'select' => 'voucher_belongs_usser_id, voucher_purchaser_user_id',
                'where' => array('voucher_belongs_usser_id' => $user->row()->user_id),
                'order_by' => 'voucher_purchased_date DESC',
            ));

            $from_user=$this->users_m->user_info_by_id($voucherCheck['voucher_purchaser_user_id']);
            $from_user=$from_user->row();
            $from_user=$from_user->user_fname;

            $data['voucher_from']=$from_user;
            $data['puid']= $user->row()->user_id; //pending user_id
            $data['thru_sms'] = 'true';
            

        }

        $this->load->view(MOBILE_M . '/landing_v', $data, false, false);
    }

    public function register_landing() {
        $data['page_title'] = 'Register';
        $this->load->view(MOBILE_M . '/register_landing_v', $data, false);
    }

    public function what_is_glomp() {
        $data['page_title'] = 'What is Glomp';
        $this->load->view(MOBILE_M . '/what_is_glomp_v', $data);
    }

    public function faq() {
        $data['page_title'] = 'Frequently Asked Questions';
        $res_privacy = $this->cmspages_m->selectCmspageByID(3, DEFAULT_LANG_ID);
        if ($res_privacy->num_rows() > 0) {
            $data['res_privacy'] = $res_privacy->row();
            $this->load->view(MOBILE_M . '/faq_v', $data);
        } else {
            redirect(base_url());
            exit();
        }
    }

    public function terms_and_conditions() {
        $data['page_title'] = 'Terms and Conditions';

        $res_privacy = $this->cmspages_m->selectCmspageByID(2, DEFAULT_LANG_ID);
        if ($res_privacy->num_rows() > 0) {
            $data['res_privacy'] = $res_privacy->row();
            $this->load->view(MOBILE_M . '/terms_and_conditions_v', $data);
        } else {
            redirect(base_url());
            exit();
        }
    }
    
    public function policy() {
        $data['page_title'] = 'Privacy Policy ';

        $res_privacy = $this->cmspages_m->selectCmspageByID(4, DEFAULT_LANG_ID);
        if ($res_privacy->num_rows() > 0) {
            $data['res_privacy'] = $res_privacy->row();
            $this->load->view(MOBILE_M . '/privacy_policy_v', $data);
        } else {
            redirect(base_url().MOBILE_M);
            exit();
        }
    }

    public function support() {
        $data['page_title'] = 'Contact Support';
        $page_title = 'glomp: Support';
        if ($this->session->userdata('user_id')) {
            $data['res_pages'] = $this->cmspages_m->selectCmsPageNotByID(0, DEFAULT_LANG_ID);
            if (isset($_POST['sendMessage'])) {
                $this->form_validation->set_rules('subject', 'Subject', 'trim|required|');
                $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

                if ($this->form_validation->run() == TRUE) {
                    $this->load->model('users_m');
                    $user_info = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
                    $rec_user_info = $user_info->row();

                    $subject = "Support Email: " . $this->input->post('subject');
                    $message_ = $this->input->post('message');
                    //send email
                    $from_email = $rec_user_info->user_email;
                    $from_name = $rec_user_info->user_name;
                    $to_email = $this->site_sender_email;
                    $to_name = $this->site_sender_name;
                    
                    $to_email = $this->site_support_email;
                    $to_name = $this->site_support_name;

    
                    $message = "<p>Dear Admin ,</p>";
                    $message .= "<br/><br/>Support email from glomp.Sender Details<br/>
									<p>Sender Name:$from_name <br/>
										Sender Email: $from_email
									</p>
									<strong>Details Message:</strong>
									";
                    $message .= $message_;
                    $message .= '<br/><br/>
									Cheers.
									<br/>
									The glomp! team.
									';
                    $this->send_email_m->sendEmail($from_email, $from_name, $to_email, $to_name, $subject, $message);

                    $data['success_msg'] = $this->lang->line('support_msg_sent_successfully');
                }
            }
            $this->load->view(MOBILE_M . '/contact_support_v', $data);
        } else {
            redirect(MOBILE_M . '/landing');
        }
    }

    public function register_form() {
        $main_header['Page_title'] = 'What is Glomp';
        //Set blocks views
        //$data['main_header'] = $this->load->view(MOBILE_M . '/includes/landing_header_main', $main_header, TRUE);
        //$data['footer'] = $this->load->view(MOBILE_M . '/includes/footer', array(), TRUE);
        $data="";    
        //Main body
        $this->load->view(MOBILE_M . '/register_form_v', $data,false);
    }
    
    public function register2_form() {
        return $this->register_form();
    }

    public function check_login() {
        $response = array(
            'success' => FALSE,
            'errors' => array()
        );

        $user_name = $this->input->post('user_name');
        $user_password = $this->input->post('user_password');

        $login_result = $this->login_m->loginValidation($user_name, $user_password);
        $login = json_decode($login_result);

        $login = isset($login->user_id) && $login->user_id > 0 && $login->is_user_logged_in == TRUE;

        //Set rules
        $this->form_validation->set_rules('user_name', 'E-mail', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');

        $this->form_validation->run();
        //With error it will display the error message
        //Without error it will display empty
        $response['errors']['user_name'] = form_error('user_name');
        $response['errors']['user_password'] = form_error('user_password');
        $response['errors']['error_msg'] = "";

        if ($this->form_validation->run() == FALSE) {
            echo json_encode($response);
            return;
        }

        if ($login == FALSE) {
            $response['errors']['error_msg'] = "Invalid E-mail or password";
            echo json_encode($response);
            return;
        }

        $response['success'] = TRUE;
        $response['location'] = MOBILE_FOLDER2 . '/user/dashboard';

        echo json_encode($response);
    }
    public function getLocationListJSONP()
    {
        $temp=$this->regions_m->getAllCountryDropdown(0);
        
        echo "append_location(\"".addslashes($temp)."\");";
    }
    public function uploadPhotoFromApp($user_id=0)
    {
        $user_id= (int) $user_id;        
        
        $response = array(
            'success' => FALSE,
            'errors' => array()
        );
        $config = array(
            'allowed_types' => 'jpeg|jpg|gif|png',
            'upload_path' => 'custom/uploads/users/', 
            'maximum_filesize' => 2
        );

        $user_photo = $this->custom_func->custom_upload($config, $_FILES['file']);
        $uploadError=preg_match("/^Error/", $user_photo)?$user_photo:NULL;
        if(empty($uploadError)){
        
            try
            {
                list($width, $height) = getimagesize('custom/uploads/users/'.$user_photo);
                $newName=mt_rand(1,10000)."_".$user_photo;
                if($width>1024){
                    $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                    
                }
                if($height>720) 
                    $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                            
            }
            catch (Exception $e)
            {
                if( ($this->config->item('log_threshold')) > 0 )
                {
                    $this->log_message('error', $e->getMessage());
                }
            }
            
            
            
            
            if(file_exists("custom/uploads/users/".$newName)){
                $user_photo=$newName;                    
            }
            else
            {
                $user_photo=$user_photo;
            }
                        

            $this->login_m->updatePhoto($user_photo,$user_id);							
            $this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
            
            
            $user_photo_orig=$user_photo;
            $this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
        }
        else
        {
            $response['errors']['profile_pic'] = $uploadError;
            echo 'process_error('.json_encode($response).');';                    
            return;            
        }

        
    }
    public function register3()
    {
         $response = array(
            'success' => FALSE,
            'errors' => array()
        );
         $_POST = $_GET;
         $this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_fname_check');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean|callback_lname_check');
        
        $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
        $this->form_validation->set_rules('months', 'Months', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
         
        
        
        
        $email = $this->input->post('email');
        $temp = $this->login_m->checkThisEmailIfExistsAndPending($email);
        $user_id = 0;          
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
        $email = $this->input->post('email');
        $temp=$this->login_m->checkThisEmailIfExistsAndPending($email);
        $by_pass_email_check = FALSE;
        
        if($temp->num_rows()==1){
            $tempData=$temp->row();
            $user_id=$tempData->user_id;
            $user_hash_key=$tempData->user_hash_key;
        } 
        
        $this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');
        $this->form_validation->set_rules('agree', 'Terms & Condtion', 'trim|required');  

        
         $this->form_validation->run();
        //With error it will display the error message
        //Without error it will display empty
        $response['errors']['fname'] = form_error('fname');
        $response['errors']['lname'] = form_error('lname');
        $response['errors']['day'] = form_error('day');
        $response['errors']['months'] = form_error('months');
        $response['errors']['year'] = form_error('year');
        $response['errors']['location'] = form_error('location');
        $response['errors']['email'] = form_error('email');
        $response['errors']['pword'] = form_error('pword');
        $response['errors']['cpword'] = form_error('cpword');
        $response['errors']['agree'] = form_error('agree');
        $response['errors']['profile_pic'] = '';        
                
        if (isset($_POST['user_photo']) && $_POST['user_photo'] == 'false')
        {
            $response['errors']['profile_pic'] = 'Must upload a profile photo';
            echo 'process_error('.json_encode($response).');';        
            return;
        }
        if ($this->form_validation->run() == FALSE)
        {
            echo 'process_error('.json_encode($response).');';        
            
        } 
        else
        {//else process signup
            if ($this->login_m->ckeckPrefillEmail($user_id) == 1) 
            {/// if email is already thre in database with pending state//
                $this->login_m->user_signup_update($user_id);
                
				
				$promo_code = $this->input->post('promo_code');
					if($promo_code!="")
						$this->users_m->promo_code_save_to_user($promo_code,$user_id);
                        
                //send email
                #user is successfully created their account
                # send welcome email
                # and redirect to landing
                $to_email = $this->input->post('email');
                $to_name = $this->input->post('fname') . ' ' . $this->input->post('lname');
                $link = site_url('landing/verify/' . $user_hash_key . '?uid=' . $user_id);
                $click_here = "<a href='$link'>click</a>";

//				new_signup
                $sign_up_subject = $this->lang->line('signup_email_subject');
                $sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>
	
                                <p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please " . $click_here . " to learn.</p>
                                <br/>
                                Cheers.
                                <br/>
                                The glomp! team.
                                ";
                //$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);


                $vars = array(
                    'template_name' => 'new_signup',
                    'lang_id' => 1,
                    'to' => $to_email,
                    'params' => array(
                        '[link]' => $click_here
                    )
                );

                $this->email_templating->config($vars);
                $this->email_templating->send();
                $this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');                    


                //send email
                
            } else
            {
                $result = json_decode($this->login_m->user_signup());
                if ($result->signup == 'success') {
                    $user_id = $result->insert_id;
					/// promo code
					$promo_code = $this->input->post('promo_code');
					if($promo_code!="")
						$this->users_m->promo_code_save_to_user($promo_code,$user_id);							
					/// promo code
                    
                    
                    //send email
                    $user_hash_key = $result->user_hash_key;
                    #user is successfully created their account
                    # send welcome email
                    # automatically make login and redirect to user/dashboard
                    $to_email = $this->input->post('email');
                    $to_name = $this->input->post('fname') . ' ' . $this->input->post('lname');
                    $link = site_url('landing/verify/' . $user_hash_key . '?uid=' . $result->insert_id);
                    $click_here = "<a href='$link'>click</a>";

                    //				new_signup
                    $sign_up_subject = $this->lang->line('signup_email_subject');
                    $sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>		
									<p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please " . $click_here . " to learn.</p>
									<br/>
									Cheers.
									<br/>
									The glomp! team.
									";
                    //$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);


                    $vars = array(
                        'template_name' => 'new_signup',
                        'lang_id' => 1,
                        'to' => $to_email,
                        'params' => array(
                            '[link]' => $click_here
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->send();
                    
                    $this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
                    
                                


                    
                } else {
                    $data['errors']['error_msg'] = "Unexpected error occured. Please try again";
                    echo 'process_error('.json_encode($response).');';        
                    return;
                }//signup else failed
            }

            $response['success'] = TRUE;
            $response['location'] = site_url(MOBILE_M . '/landing/');
            $response['user_id'] = $user_id;

            echo 'process_succcess('.json_encode($response).');';        
        
        
        }//else process signup   
    }
    public function register2() {        
    
    
    
        $response = array(
            'success' => FALSE,
            'errors' => array()
        );
        
        $user_id = ($this->session->userdata('id_user'))?$this->session->userdata('id_user'):0;				

        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_fname_check');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean|callback_lname_check');
        
        $this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
        $this->form_validation->set_rules('months', 'Months', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
        
        
        
       if($user_id==0)
        {
            $email = $this->input->post('email');
            $temp = $this->login_m->checkThisEmailIfExistsAndPending($email);
            $user_id = 0;          
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
            $email = $this->input->post('email');
            $temp=$this->login_m->checkThisEmailIfExistsAndPending($email);
            $by_pass_email_check = FALSE;
            
            if($temp->num_rows()==1){
                $tempData=$temp->row();
                $user_id=$tempData->user_id;
                $user_hash_key=$tempData->user_hash_key;
            }          
            else
            {
                $field="";

                if (isset($_POST['user_linkedin_id'])) {
                    $field = 'user_linkedin_id';
                    $value = $_POST['user_linkedin_id'];
                }
                
                if (isset($_POST['fb_user_id'])) {
                    $field = 'user_fb_id';
                    $value = $_POST['fb_user_id'];
                }
                if($field!="")
                {
                
                    $temp = $this->users_m->get_record(array(
                        'where' => array(
                            $field => $value
                        ),
                        'resource_id' => TRUE
                    ));
                                    
                    if( is_bool($temp) == FALSE){
                        $tempData = $temp->row();
                        $user_id=$tempData->user_id;
                        $user_hash_key=$tempData->user_hash_key;
                        $by_pass_email_check = TRUE;
                    }
                }
            }
        }
        else
        {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
        }        
        
        $this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');
        $this->form_validation->set_rules('agree', 'Terms & Condtion', 'trim|required');        
        
        

        $this->form_validation->run();
        //With error it will display the error message
        //Without error it will display empty
        $response['errors']['fname'] = form_error('fname');
        $response['errors']['lname'] = form_error('lname');
        $response['errors']['day'] = form_error('day');
        $response['errors']['months'] = form_error('months');
        $response['errors']['year'] = form_error('year');
        $response['errors']['location'] = form_error('location');
        $response['errors']['email'] = form_error('email');
        $response['errors']['pword'] = form_error('pword');
        $response['errors']['cpword'] = form_error('cpword');
        $response['errors']['agree'] = form_error('agree');
        $response['errors']['profile_pic'] = '';
        
        if ( isset($_POST['fb_user_photo']) && $_POST['fb_user_photo'] == '' && ! isset($_FILES['user_photo'])) {
            //TODO: Must be in language translation
            $response['errors']['profile_pic'] = 'Must upload a profile photo';
            echo json_encode($response);
            return;
        }
        
        if (! isset($_FILES['user_photo']) && isset($_POST['fb_user_photo']) && $_POST['fb_user_photo'] == '') {
            $response['errors']['profile_pic'] = 'Must upload a profile photo';
            echo json_encode($response);
            return;
        }
                
        if ($this->form_validation->run() == FALSE) {
            echo json_encode($response);
            return;
        } else
        {
            if ($this->login_m->ckeckPrefillEmail($user_id) == 1) 
            {/// if email is already thre in database with pending state//
                $this->login_m->user_signup_update($user_id);
                
				
				$promo_code = $this->input->post('promo_code');
					if($promo_code!="")
						$this->users_m->promo_code_save_to_user($promo_code,$user_id);
                        
                //send email
                #user is successfully created their account
                # send welcome email
                # and redirect to landing
                $to_email = $this->input->post('email');
                $to_name = $this->input->post('fname') . ' ' . $this->input->post('lname');
                $link = site_url('landing/verify/' . $user_hash_key . '?uid=' . $user_id);
                $click_here = "<a href='$link'>click</a>";

//				new_signup
                $sign_up_subject = $this->lang->line('signup_email_subject');
                $sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>
	
                                <p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please " . $click_here . " to learn.</p>
                                <br/>
                                Cheers.
                                <br/>
                                The glomp! team.
                                ";
                //$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);


                $vars = array(
                    'template_name' => 'new_signup',
                    'lang_id' => 1,
                    'to' => $to_email,
                    'params' => array(
                        '[link]' => $click_here
                    )
                );

                $this->email_templating->config($vars);
                $this->email_templating->send();
                $this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');                    


                //send email
                        
                
                
                if(isset($_FILES['user_photo']))
                {
                    $config = array(
                        'allowed_types' => 'jpeg|jpg|gif|png',
                        'upload_path' => 'custom/uploads/users/', 
                        'maximum_filesize' => 2
                   );

                   $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
                   $uploadError=preg_match("/^Error/", $user_photo)?$user_photo:NULL;
                    if(empty($uploadError)){
                    
                        try
                        {
                            list($width, $height) = getimagesize('custom/uploads/users/'.$user_photo);
                            $newName=mt_rand(1,10000)."_".$user_photo;
                            if($width>1024){
                                $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                    
                            }
                            if($height>720) 
                                $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                            
                        }
                        catch (Exception $e)
                        {
                            if( ($this->config->item('log_threshold')) > 0 )
                            {
                                $this->log_message('error', $e->getMessage());
                            }
                        }
                        
                        
                        
                        
                        if(file_exists("custom/uploads/users/".$newName)){
                            $user_photo=$newName;                    
                        }
                        else
                        {
                            $user_photo=$user_photo;
                        }
                                    

                        $this->login_m->updatePhoto($user_photo,$user_id);							
                        $this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
                        
                        
                        $user_photo_orig=$user_photo;
                        $this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
							

                    }
                    else
                    {
                            $response['errors']['profile_pic'] = $uploadError;
                            echo json_encode($response);
                            return;
                            //exit();
                    }
                }
                //Start grab FB photo
                elseif (isset($_POST['fb_user_photo']) && $_POST['fb_user_photo'] != '') {
                    $url = $_POST['fb_user_photo'];
                    /* Extract the filename */
                    $filename = substr($url, strrpos($url, '/') + 1);
                    $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                    $ext = strrchr($safe_filename, '.');
                    $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                    /* Save file wherever you want */
                    $user_photo = $new_photo_name . $ext;

                    //get data
                    $localfile = 'custom/uploads/users/mytempfilename.ext';

                    // Let's go cURLing...
                    $ch = curl_init($url);
                    $fp = fopen($localfile, 'w');

                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);

                    curl_exec($ch);
                    $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                    curl_close($ch);
                    fclose($fp);

                    // Get the data into memory and delete the temp file
                    $filedata = file_get_contents($localfile);
                    unlink($localfile);
                     if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }
                    //get data

                    file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                    $user_photo = $new_photo_name . $ext;

                    $this->login_m->updatePhoto($user_photo, $user_id);
                    $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);
					
					
					$user_photo_orig=$user_photo;
					$this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
                }
                //End grab FB photo
                
                //$result->signup == 'success';
                //redirect(MOBILE_M.'/landing/');
                //exit();
            } else
            {
                $result = json_decode($this->login_m->user_signup());
                if ($result->signup == 'success') {
                    $user_id = $result->insert_id;
					/// promo code
					$promo_code = $this->input->post('promo_code');
					if($promo_code!="")
						$this->users_m->promo_code_save_to_user($promo_code,$user_id);							
					/// promo code
                    
                    
                    //send email
                    $user_hash_key = $result->user_hash_key;
                    #user is successfully created their account
                    # send welcome email
                    # automatically make login and redirect to user/dashboard
                    $to_email = $this->input->post('email');
                    $to_name = $this->input->post('fname') . ' ' . $this->input->post('lname');
                    $link = site_url('landing/verify/' . $user_hash_key . '?uid=' . $result->insert_id);
                    $click_here = "<a href='$link'>click</a>";

                    //				new_signup
                    $sign_up_subject = $this->lang->line('signup_email_subject');
                    $sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>		
									<p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please " . $click_here . " to learn.</p>
									<br/>
									Cheers.
									<br/>
									The glomp! team.
									";
                    //$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);


                    $vars = array(
                        'template_name' => 'new_signup',
                        'lang_id' => 1,
                        'to' => $to_email,
                        'params' => array(
                            '[link]' => $click_here
                        )
                    );

                    $this->email_templating->config($vars);
                    $this->email_templating->send();
                    
                    $this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
                    
                    //send email
                    
                    if(isset($_FILES['user_photo']))
                    {
                        $config = array(
                            'allowed_types' => 'jpeg|jpg|gif|png',
                            'upload_path' => 'custom/uploads/users/', 
                            'maximum_filesize' => 2
                       );

                       $user_photo = $this->custom_func->custom_upload($config, $_FILES['user_photo']);
                       $uploadError=preg_match("/^Error/", $user_photo)?$user_photo:NULL;
                        if(empty($uploadError)){
                        
                            try
                            {
                                list($width, $height) = getimagesize('custom/uploads/users/'.$user_photo);                                                        
                                $newName=mt_rand(1,10000)."_".$user_photo;
                                if($width>1024){
                                    $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);                    
                                }
                                if($height>720) 
                                    $this->resizeImg('custom/uploads/users/'.$user_photo,'custom/uploads/users/'.$newName,1024,720,3);
                                
                                if(file_exists("custom/uploads/users/".$newName)){
                                    $user_photo=$newName;                    
                                }
                                else
                                {
                                    $user_photo=$user_photo;
                                }
                                
                            }
                            catch (Exception $e)
                            {
                                if( ($this->config->item('log_threshold')) > 0 )
                                {
                                    $this->log_message('error', $e->getMessage());
                                }
                            }

                            
                                
                            $this->login_m->updatePhoto($user_photo,$user_id);
                            $this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
                            
                            
                            $user_photo_orig=$user_photo;
                            $this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
								
                        }
                        else
                        {
                                $response['errors']['profile_pic'] = $uploadError;
                                echo json_encode($response);
                                exit;
                                //exit();
                        }
                    }
                    //Start grab FB photo
                    elseif (isset($_POST['fb_user_photo']) && $_POST['fb_user_photo'] != '')
                    {
                        $url = $_POST['fb_user_photo'];
                        /* Extract the filename */
                        $filename = substr($url, strrpos($url, '/') + 1);
                        $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($filename));
                        $ext = strrchr($safe_filename, '.');
                        $new_photo_name = "" . mt_rand(1, 10000) . "_" . md5(session_id()) . "_" . time() . "_" . $filename[0];
                        /* Save file wherever you want */
                        $user_photo = $new_photo_name . $ext;

                        //get data
                        $localfile = 'custom/uploads/users/mytempfilename.ext';

                        // Let's go cURLing...
                        $ch = curl_init($url);
                        $fp = fopen($localfile, 'w');

                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);

                        curl_exec($ch);
                        //get mime type incase there is no file extension
                        $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        fclose($fp);

                        // Get the data into memory and delete the temp file
                        $filedata = file_get_contents($localfile);
                        unlink($localfile);
                        if (empty($ext)) {
                            //get file extension base on mime type
                            $ext = mime_to_file_extension($mime_type);
                        }
                        
                        //get data                        

                        file_put_contents('custom/uploads/users/' . $new_photo_name . $ext, $filedata);

                        $user_photo = $new_photo_name . $ext;

                        $this->login_m->updatePhoto($user_photo, $user_id);
                        $this->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);
						
						$user_photo_orig=$user_photo;
						$this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
                    }
                    //End grab FB photo                    


                    
                } else {
                    $data['errors']['error_msg'] = "Unexpected error occured. Please try again";
                    echo json_encode($response);
                    return;
                }//signup else failed
            }



            $response['success'] = TRUE;
            $response['location'] = site_url(MOBILE_M . '/landing/');

            echo json_encode($response);
        }
    }
    function fname_check($name){

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('fname_check', 'Special characters are not allowed on First Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('fname_check', 'Numbers are not allowed on First Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }
    function lname_check($name){

        $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            //todo language translation
            $this->form_validation->set_message('lname_check', 'Special characters are not allow on Last Name.');
            return false;
        }
        else{
            //  echo "ok:".$username; 
            $not_allowed='1234567890';//
            if (str_contains($name, $not_allowed) == false)
            {    
                //    echo "Error: Not allowed letters: ".$not_allowed;
                $this->form_validation->set_message('lname_check', 'Numbers are not allow on Last Name.');
                return false;
            }
            else{
              //  echo "ok:".$username; 
              return true;                
            }            
        }    
    }

    function dob_check($day) {

        $year = ($this->input->post('year') == "") ? '0000' : $this->input->post('year');
        $month = ($this->input->post('months') == "") ? '00' : $this->input->post('months');
        $month = (int) $month;
        $day = (int) $day;
        $year = (int) $year;
        if (checkdate($month, $day, $year))
            return true;
        else {
            $this->form_validation->set_message('dob_check', 'Please provide valid Date of Birth');
            return FALSE;
        }
    }

    function email_check($email) {

        $email_chk = $this->login_m->emailExist($email);
        if ($email_chk == 0)
            return true;
        else {
            $this->form_validation->set_message('email_check', 'Email address already exist. Please try new one');
            return false;
        }
    }

    function resizeImg($orginalImg, $thumbImg, $newWidth, $newHeight, $resizeOption = 'exact', $sharpen = true) {
        $this->load->library('resize');
        $resizeImg = new Resize($orginalImg);
        $resizeImg->resizeImage($newWidth, $newHeight, $resizeOption, $sharpen = true);
        $resizeImg->saveImage($thumbImg, 100);
    }

    function forgotPassword() {

        $data['page_title'] = 'glomp: ' . $this->lang->line('reset_password');
        if (isset($_POST['reset_password'])) {
            $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');
            if ($this->form_validation->run() == TRUE) {
                $email_address = $this->input->post('user_email');
                $email_check_num = $this->login_m->email_validateion($email_address);
                if ($email_check_num == 1) {
                    $data['email_address'] = $email_address;
                    //email password										
                    $json_msg = $this->login_m->reset_password_info($email_address);
                    $msg = json_decode($json_msg);

                    if ($msg->valid_email == 'true') {
                        $this->login_m->change_password($msg->plain_password, $msg->user_id);
                        //send email to user with email link
                        $from_email = $this->site_sender_email;
                        $from_name = $this->site_sender_name;
                        $to_email = $msg->user_email;
                        $to_name = $msg->user_fname;
                        $user_id = $msg->user_id;
                        $subject = $this->lang->line('reset_password_email_subject');

                        $link = site_url();
                        $click_here = "<a href='$link'>Click here</a>";

                        $vars = array(
                            'template_name' => 'forgot_password',
                            'lang_id' => 1,
                            'to' => $to_email,
                            'params' => array(
                                '[link]' => $click_here,
                                '[user_fname]' => $to_name,
                                '[password]' => $msg->plain_password
                            )
                        );

                        $this->email_templating->config($vars);
                        $this->email_templating->send();
                        $data['success_msg'] = $this->lang->line('reset_password_successful_msg');
                    } else {
                        $data['error_msg'] = $this->lang->line('Unexpected_Error');
                    }

                    //email password
                } else {
                    $data['error_msg'] = $this->lang->line('reset_password_email_doesnot_exist');
                }
            }
        }
        $this->load->view(MOBILE_M . '/forgot_password_v', $data);
    }

    function update_password() {

        $email_address = $this->input->post('email');
        $json_msg = $this->login_m->reset_password_info($email_address);
        $msg = json_decode($json_msg);
        if ($msg->valid_email == 'true') {
            $this->login_m->change_password($msg->plain_password, $msg->user_id);

            //send email to user with email link
            $from_email = $this->site_sender_email;
            $from_name = $this->site_sender_name;
            $to_email = $msg->user_email;
            $to_name = $msg->user_name;
            $user_id = $msg->user_id;
            $subject = $this->lang->line('reset_password_email_subject');


            $message = "<p>Dear $to_name,</p>";
            $message .= "<br/><br/>Your new password:<br/><br/>";
            $message .= "Email:" . $msg->user_email . " <br/>
							Password:" . $msg->plain_password . "
							<br/><br/><br/>";
            $message .= 'PS : This is an unattended mail box, do not reply to this email.<br/><br/>';
            $message .= '<br/>
								Cheers.
								<br/>
								The glomp! team.
								';
            $this->send_email_m->sendEmail($from_email, $from_name, $to_email, $to_name, $subject, $message);
            $msg_ = '<div class="alert alert-success">' . $this->lang->line('reset_password_successful_msg') . '</div';
            die($msg_);
        } else {
            $msg_ = '<div class="alert alert-error">' . $this->lang->line('Unexpected_Error') . '</div';
            die($msg_);
        }
    }

    function logout($from="") {
        if($this->session->userdata('admin_id') && $this->session->userdata('is_logged_in')==true)
        {
            
            $array_items = array(            
                'user_id' => '',
                'username' => '',
                'user_name' => '',
                'user_email' => '',
                'user_fname' => '',
                'user_lname' => '',
                'user_email_verified' => '',
                'user_last_login_date' => '',
                'user_last_login_ip' => '',
                'lang_id' => '',
                'twitter_oauth_token' => '',
                'twitter_oauth_token_secret' => '',
                'is_user_logged_in' => '',   
            );
            $this->session->unset_userdata($array_items);
        }
        else
        {
            $this->session->sess_destroy();
        }
        
        if($from=="deactivated")
        {
            $this->session->set_flashdata('deactivated', 'Your account has been deactivated.');
            //echo "Your account has been deactivated";
        }
        redirect(base_url(MOBILE_M.'?'.$from));
        //exit();
    }
        

}