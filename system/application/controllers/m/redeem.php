<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Redeem extends CI_Controller {

    private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;

    function __construct() {
        parent::__Construct();
        $this->load->library('user_login_check_m');
        $this->load->model('login_m');
        $this->load->model('cmspages_m');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->model('send_email_m');
        $this->load->model('regions_m');
        $this->load->model('merchant_m');
        
        //Load the email templating class
        $this->load->library('email_templating');
    }
    
    /**
     * Return data as output
     * - automatically check for an ajax request
     * 
     * @param $data Array - keys: status(Error|Success), template, data
     * @todo this should be generic among all controllers
     * @todo integrate server response codes
     */
    function out( $data = array() ) {

        if( $this->input->is_ajax_request() ) {
            echo json_encode( $data );
            exit;
        }
        
        if( isset($data['template']) ) {
            $this->load->view( $data['template'], $data );
        }
    }
    
    /**
     * Shows voucher information and a form submission
     * for redeeming the voucher
     *
     */
    public function voucher2( $voucher_id = 0 ) {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
        // voucher id must always exist
        if( !$voucher_id ) {
            $this->out(array(
                'status' => 'Error',
                'template' => 'm/voucher_invalid_v2',
                'message' => $this->lang->line('invalid_voucher') . ' 0'
            ));
            return;
        }
            
        $voucher_id = strip_tags( $voucher_id );
        $user_id = $this->session->userdata('user_id');

        $voucher = $this->user_account_m->validate_voucher_to_redeem( $user_id, $voucher_id );

        // no voucher exists
        if( $voucher->num_rows() < 1 ) {
            $this->out(array(
                'status' => 'Error',
                'template' => 'm/voucher_invalid_v2',
                'message' => $this->lang->line('invalid_voucher') . ' 1',
                'voucher' => $voucher->row(0,'array')
            ));
            return FALSE;
        }
        
        // voucher is already redeemed/expired/reversed
        if( ( $voucher->row()->voucher_status != 'Consumable'  && $voucher->row()->voucher_status != 'UnClaimed' )
            && ( $voucher->row()->voucher_status == 'Redeemed' && $this->input->is_ajax_request())
           ){
            $msg = '';

            switch( $voucher->row()->voucher_status ) {
                case 'Redeemed' :
                    $msg = $this->lang->line('voucher_alrady_reddemed_msg');
                    break;
                case 'Reversed' :
                    $msg = $this->lang->line('voucher_reversed_msg');
                    break;
                case 'Expired'  :
                    $msg = $this->lang->line('voucher_already_expired_msg');
                    break;
                default         :
                    $msg = $this->lang->line('invalid_voucher') . ' 2' . $voucher->row()->voucher_status;
                    break;
            }

            $this->out(array(
                'status' => 'Error',
                'template' => 'm/voucher_invalid_v2',
                'message' => $msg,
                'voucher' => $voucher->row(0,'array')
            ));
            return FALSE;
        }

        // this block of code is the same as when
        // the form is submitted
        if( $this->input->post('automation') ) {
            $result_voucher = $this->_process_voucher( $voucher );
            if ($result_voucher['redeem_status'] == 'success') {
                $this->_send_redemption_email( $voucher, $result_voucher );
            }
            return;
        }
        
        $this->form_validation->set_rules('outlet_code', 'Outlet Code', 'trim|callback__check_outlet_code');
        $this->form_validation->set_rules('merchant_id', 'Merchant ID', 'trim|required');
        
        if( $this->form_validation->run() == FALSE ) {
            $this->out(array(
                'status' => 'Error',
                'template' => 'm/redeem_v2',
                'message' => validation_errors(),
                'voucher' => $voucher->row(0,'array')
            ));
            return;
        } else {
            // process redemption
            $result_voucher = $this->_process_voucher( $voucher );

            if ($result_voucher['redeem_status'] == 'success') {
                
                $this->_send_redemption_email( $voucher, $result_voucher );

                if( !$this->input->is_ajax_request() ) {
                    redirect( base_url().'m/validated2/'.$voucher_id);
                }

                $this->out(array(
                    'status' => 'Success',
                    'voucher' => array(
                        'voucher_id' => $voucher->row()->voucher_id
                    )
                ));
            }

            return;
        }
        
        // voucher view for redemption
        $this->out(array(
            'status' => 'Ok',
            'template' => 'm/redeem_v2',
            'voucher' => $voucher->row(0,'array')
        ));
    }
    
    /**
     * return processed voucher during redemption
     */
    function _process_voucher( $voucher ) {

        $outlet = $this->product_m->verify_outlet_code($this->input->post( 'merchant_id' ), $this->input->post( 'outlet_code' ) );

        return $this->user_account_m->redeem_voucher (
                $voucher->row()->voucher_purchaser_user_id,
                $this->session->userdata('user_id'),
                $voucher->row()->voucher_id,
                $outlet->row()->outlet_id,
                $voucher->row()->voucher_product_id );
    }

    function _send_redemption_email( $voucher, $result_voucher ) {

        $purchaser_email = $result_voucher['purchaser_email'];
        $purchaser_name = $result_voucher['purchaser_name'];
        $transactionID = $result_voucher['transactionid'];
        $redeemer_name = $this->session->userdata('user_name');
        $merchant_name = $result_voucher['merchant_name'];

        $product_name = $result_voucher['product_name'];
        $subject = "Your friend has redeemed!";

        $link = base_url() . "profile/view/" . $this->session->userdata('user_id');
        $hyper_link = "<a href='$link'>Check it out</a>";
        
        #do not send email to admin. admin id is always 1
        if ($voucher->row()->voucher_purchaser_user_id != 1) {
            //Send redemption email
            $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher->row()->voucher_id))->row();
            $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
            $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();

            $vars = array(
                'template_name' => 'glomp_redeem',
                'from_name' => $this->site_sender_name,
                'from_email' => $this->site_sender_email,
                'lang_id' => 1,
                'to' => $purchaser_email,
                'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                'params' => array(
                    '[insert redeemers name]' => $redeemer_name,
                    '[insert merchant brand and product]' => $merchant_name.' '.$product_name,
                    '[link]' => $hyper_link,
                ),
                'others' => array(
                    'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                    'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                )
            );
            $this->email_templating->config($vars);
            $this->email_templating->send();
            //end redemption email   
        }
    }

    function _check_outlet_code( $val ) {
        if( $val != '' ) {
            
            $res_outlet_verify = $this->product_m->verify_outlet_code($this->input->post('merchant_id'), $val);
            
            if( $res_outlet_verify->num_rows() > 0 ) {
                
                $this->db->where('voucher_id',$this->uri->segment(4));
                $this->db->update('gl_voucher',array('voucher_outlet_id'=>$res_outlet_verify->row()->outlet_id));
                
                return TRUE;
            }

            // log activity
            $sql_activity = "INSERT INTO gl_activity SET
                    log_user_id = '".$this->session->userdata('user_id')."',
                    log_title = 'Redeeming: Invalid Outlet Code (" . $val . ").',
                    log_details = 'Invalid Outlet Code (" . $val . ").',
                    log_ip = '".$_SERVER['REMOTE_ADDR']."',
                    log_timestamp = '".date('Y-m-d H:i:s')."',
                    log_device = 'mobile'
                ";
            $this->db->query($sql_activity);

            $this->form_validation->set_message('_check_outlet_code', $this->lang->line('incorrect_outlet_code') );

            return FALSE;

        }
        
        $this->form_validation->set_message('_check_outlet_code', '%s is required');
        
        return FALSE;
    }

    /**
     * old voucher redemption
     * 
     * @todo remove this in favor of $this::voucher2
     */
    function voucher($voucher_id = 0) {
        $voucher_id = strip_tags($voucher_id);
        $user_id = $this->session->userdata('user_id');
        $data['voucher_id'] = $voucher_id;

        $voucher_info = $this->user_account_m->validate_voucher_to_redeem($user_id, $voucher_id);

        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if ($voucher_info->num_rows() > 0) {
            $row_voucher = $voucher_info->row();
            $merchat_id = $this->product_m->merchant_id_by_product($row_voucher->voucher_product_id);

            $product_id = $row_voucher->voucher_product_id;
            $purchaser_id = $row_voucher->voucher_purchaser_user_id;
            $voucher_status = $row_voucher->voucher_status;

            //if already reddmed
            $data['transactionid'] = $row_voucher->voucher_transactionID;
            $data['bar_code'] = $row_voucher->voucher_code;
            $data['redeem_timestamp'] = $row_voucher->voucher_redemption_date;


            $data['voucher_status'] = $voucher_status;
            $data['row_voucher'] = $row_voucher;
            $data['prod_id'] = $row_voucher->voucher_product_id;

            if ($_POST && isset($_POST['outlet_code'])) {//&& isset($_POST['glomp_password']) ){
                $outlet_code = $this->input->post('outlet_code');
                //$user_password = $this->input->post('glomp_password');
                //now verify the outlet	
                $res_outlet_verify = $this->product_m->verify_outlet_code($merchat_id, $outlet_code);
                $outlet_verify = false;
                if ($res_outlet_verify->num_rows() > 0) {
                    $rec_outlet_verify = $res_outlet_verify->row();
                    if ($rec_outlet_verify->outlet_code == $outlet_code) {
                        $outlet_verify = true;
                        $outlet_id = $rec_outlet_verify->outlet_id;
                    }
                }
                if (!$outlet_verify) {
                    /* log activity here */
                    $time = date('Y-m-d H:i:s');
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $sql_activity = "INSERT INTO gl_activity SET
                            log_user_id = '$user_id',
                            log_title = 'Redeeming: Invalid Outlet Code (" . $outlet_code . ").',
                            log_details = 'Invalid Outlet Code (" . $outlet_code . ").',
                            log_ip = '$ip',
                            log_timestamp = '$time',
                            log_device = 'mobile'
                        ";
                    $this->db->query($sql_activity);
                    /* log activity here */
                    $response->message = $this->lang->line('incorrect_outlet_code');
                } else {
                    //now for password verification
                    /* $password_verify = $this->login_m->password_verify($user_password,$user_id);
                      if($password_verify!="ok")
                      {
                      $response->message=$this->lang->line('incorrect_password');
                      }
                      else{
                      $response->status='Ok';
                      } */
                    $response->status = 'Ok';
                }
                echo json_encode($response);
            } else {
                $this->load->view(MOBILE_M . '/redeem_v', $data);
            }
        }//valid voucher
        else {//invalid voucher
            $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
        }//invalid voucher
    }

    /**
     * old function
     * 
     * @todo remove this. not used in refactored codes
     */
    function validating($voucher_id = 0) {
        $outlet_code = $this->input->post('outlet_code');
        $user_password = $this->input->post('glomp_password');
        $voucher_id = strip_tags($voucher_id);
        $data['voucher_id'] = $voucher_id;
        $data['outlet_code'] = $outlet_code;
        $data['user_password'] = $user_password;
        $this->load->view(MOBILE_M . '/validating_v', $data);
    }

    function test($voucher_id = 0) {
        $voucher_id = strip_tags($voucher_id);
        $user_id = $this->session->userdata('user_id');
        $data['voucher_id'] = $voucher_id;
        $data['bar_code'] = '123-456-789';

        $data['merchant_name'] = 'merchant_name';
        $data['merchant_area'] = 'merchant_area';

        $data['outlet_name'] = 'outlet_name';
        $data['outlet_area'] = 'outlet_area';


        $data['redeem_timestamp'] = date('Y-m-d H:i:s');


        $this->load->view(MOBILE_M . '/redeem_success_v', $data, false);
    }
    
    function validated2( $voucher_id ) {
        
        $voucher = $this->user_account_m->validate_voucher_to_redeem( $this->session->userdata('user_id'), $voucher_id );
        $merchant = $this->db->get_where('gl_merchant',array('merchant_id'=>$voucher->row()->voucher_merchant_id));
        $outlet = $this->db->get_where('gl_outlet',array('outlet_id'=>$voucher->row()->voucher_outlet_id));
        
        $data = array();
        
        $data['response'] = 'Success';
        $data['bar_code'] = $voucher->row()->voucher_code;
        $data['merchant_name'] = $merchant->row()->merchant_name;
        $data['voucher_id'] = $voucher_id;
        $data['outlet_name'] = $outlet->row()->outlet_name;
        $data['redeem_timestamp'] = $voucher->row()->voucher_redemption_date;
        
        $this->load->view(MOBILE_M . '/redeem_success_v', $data, false);
    }

    /**
     * old function
     * 
     * @todo remove this in favor of $this::validated2
     */
    function validated($voucher_id = 0) {
        
        $voucher_id = strip_tags($voucher_id);
        $user_id = $this->session->userdata('user_id');
        $data['voucher_id'] = $voucher_id;

        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error'; 

        $voucher_info = $this->user_account_m->validate_voucher_to_redeem($user_id, $voucher_id);

        $response = (object) array('status' => 'Error', 'message' => '', 'data' => '');
        $response->status = 'Error';
        if ($voucher_info->num_rows() > 0) {

            $row_voucher = $voucher_info->row();
            $merchat_id = $this->product_m->merchant_id_by_product($row_voucher->voucher_product_id);

            $product_id = $row_voucher->voucher_product_id;
            $purchaser_id = $row_voucher->voucher_purchaser_user_id;
            $voucher_status = $row_voucher->voucher_status;

            //if already reddmed
            $data['transactionid'] = $row_voucher->voucher_transactionID;
            $data['bar_code'] = $row_voucher->voucher_code;
            $data['redeem_timestamp'] = $row_voucher->voucher_redemption_date;

            if ($voucher_status == 'Consumable') {
                //$this->db->query('SET innodb_lock_wait_timeout=5');

                if ($_POST && isset($_POST['outlet_code'])) {//&& isset($_POST['glomp_password']) )
                    $outlet_code = $this->input->post('outlet_code');
                    //$user_password = $this->input->post('glomp_password');
                    //now verify the outlet	
                    $res_outlet_verify = $this->product_m->verify_outlet_code($merchat_id, $outlet_code);
                    $outlet_verify = false;
                    $notOk = true;
                    if ($res_outlet_verify->num_rows() > 0) {
                        $rec_outlet_verify = $res_outlet_verify->row();
                        if ($rec_outlet_verify->outlet_code == $outlet_code) {
                            $outlet_verify = true;
                            $outlet_id = $rec_outlet_verify->outlet_id;
                        }
                    }
                    if (!$outlet_verify) {
                        /* log activity here */
                        $time = date('Y-m-d H:i:s');
                        $ip = $_SERVER['REMOTE_ADDR'];
                        $sql_activity = "INSERT INTO gl_activity SET
                                log_user_id = '$user_id',
                                log_title = 'Redeeming: Invalid Outlet Code (" . $outlet_code . ").',
                                log_details = 'Invalid Outlet Code (" . $outlet_code . ").',
                                log_ip = '$ip',
                                log_timestamp = '$time',
                                log_device = 'mobile'
                            ";
                        $this->db->query($sql_activity);
                        /* log activity here */
                        $response->message = $this->lang->line('incorrect_outlet_code');
                    } else {
                        //now for password verification
                        $password_verify = 'ok'; // $this->login_m->password_verify($user_password,$user_id);
                        if ($password_verify != "ok") {
                            $response->message = $this->lang->line('incorrect_password');
                        } else {
                            //valid vouceher, outlet code and password
                            if ($outlet_verify == true && $password_verify == 'ok') {
                                # voucher is alrady verified to go for redeemption
                                $result_voucher = $this->user_account_m->redeem_voucher($purchaser_id, $user_id, $voucher_id, $outlet_id, $product_id);
                                //send email to purchaser to notify your voucher is reedemed
                                if ($result_voucher['redeem_status'] == 'success') {
                                    //clear cache
                                    $params = array(
                                        'affected_tables'
                                        => array(
                                            'gl_voucher'
                                        ), #cache name
                                        'specific_names'
                                        => array(
                                            'glomped_me_' . $user_id,
                                            'friends_info_buzz_' . $user_id,
                                            'glomp_buzz_specific_user_' . $user_id,
                                        ) #cache name
                                    );
                                    delete_cache($params);
                                    //clear cache

                                    $purchaser_email = $result_voucher['purchaser_email'];
                                    $purchaser_name = $result_voucher['purchaser_name'];
                                    $transactionID = $result_voucher['transactionid'];
                                    $redeemer_name = $this->session->userdata('user_name');
                                    $merchant_name = $result_voucher['merchant_name'];
                                    $product_name = $result_voucher['product_name'];

                                    $link = base_url() . "profile/view/" . $user_id;
                                    $hyper_link = "<a href='$link'>Check it out.</a>";
                                    
                                    #do not send email to admin. admin id is always 1
                                    if ($purchaser_id != 1) {
                                        //Send redemption email
                                        $voucher_res = $this->db->get_where('gl_voucher', array('voucher_id' => $voucher_id))->row();
                                        $merchant_res = $this->db->get_where('gl_merchant', array('merchant_id' => $voucher_res->voucher_merchant_id))->row();
                                        $prod_res = $this->db->get_where('gl_product', array('prod_id' => $voucher_res->voucher_product_id))->row();

                                        $vars = array(
                                            'template_name' => 'glomp_redeem',
                                            'from_name' => $this->site_sender_name,
                                            'from_email' => $this->site_sender_email,
                                            'lang_id' => 1,
                                            'to' => $purchaser_email,
                                            'template' => ADMIN_FOLDER . '/email/_layouts/2-Column_1-Row Layout_2_responsive',
                                            'params' => array(
                                                '[insert redeemers name]' => $redeemer_name,
                                                '[insert merchant brand and product]' => $merchant_name.' '.$product_name,
                                                '[link]' => $hyper_link,
                                            ),
                                            'others' => array(
                                                'image_1' => base_url() . $this->custom_func->merchant_logo($merchant_res->merchant_logo),
                                                'image_2' => base_url() . $this->custom_func->product_logo($prod_res->prod_image)
                                            )
                                        );
                                        $this->email_templating->config($vars);
                                        $this->email_templating->send();
                                        //end redemption email
                                    }
                                    //load the success voucher message
                                    $voucher_info = $this->user_account_m->validate_voucher_to_redeem($user_id, $voucher_id);
                                    $row_voucher = $voucher_info->row();
                                    $data['bar_code'] = $row_voucher->voucher_code;
                                    $data['transactionid'] = $result_voucher['transactionid'];

                                    $data['merchant_name'] = $result_voucher['merchant_name'];
                                    $data['merchant_area'] = $result_voucher['merchant_area'];

                                    $data['outlet_name'] = $result_voucher['outlet_name'];
                                    $data['outlet_area'] = $result_voucher['outlet_area'];


                                    $data['redeem_timestamp'] = $result_voucher['reddemed_date'];

                                    $voucher_status = $row_voucher->voucher_status;

                                    //die("{status:'success'}");

                                    $response->status = 'Ok';
                                    $notOk = false;
                                    $data['response'] = $response;
                                    $this->load->view(MOBILE_M . '/redeem_success_v', $data, false);
                                }//voucher success
                                else {
                                    //throw exception message	
                                    //$data['error_msg'] = $this->lang->line('unexpected_error_occurred_try_again');									
                                    $response->message = $this->lang->line('unexpected_error_occurred_try_again');
                                    $data['response'] = $response;
                                    $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
                                }
                                //valid vouceher, outlet code and password							
                            }
                        }
                    }
                    if ($notOk) {
                        $data['response'] = $response;
                        $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
                    }
                } else {
                    $response->message = $this->lang->line('') . 'Invalid submission process';
                    $data['response'] = $response;
                    $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
                }
            }//consumable
            else {
                if ($voucher_status == 'UnClaimed') {
                    //redeem the voucher it will display the filed to enter password and outlet
                } else if ($voucher_status == 'Redeemed') {
                    // display message voucher has been alrady redeemed
                    $res_product = $this->product_m->productAndMerchant($product_id);
                    $rec_product = $res_product->row();
                    $merchant_name = $rec_product->merchant_name;

                    $res_outlet = $this->merchant_m->outletById($row_voucher->voucher_outlet_id);
                    $res_outlet = $res_outlet->row();
                    $outlet_name = $res_outlet->outlet_name;
                    
                    $data['merchant_name'] = $merchant_name;
                    $data['outlet_name'] = $outlet_name;
                    
                    $data['response'] = $this->lang->line('voucher_alrady_reddemed_msg');
                    return $this->load->view(MOBILE_M . '/redeem_success_v', $data);
                } else if ($voucher_status == 'Reversed') {
                    //display message as voucher has been reversed
                    $data['voucher_status_msg'] = $this->lang->line('voucher_reversed_msg');
                } else if ($voucher_status == 'Expired') {
                    //display message as voucher has been reversed
                    $data['voucher_status_msg'] = $this->lang->line('voucher_already_expired_msg');
                } else {
                    $data['voucher_status_msg'] = $this->lang->line('invalid_voucher');
                }

                $response->message = $data['voucher_status_msg'];
                $data['response'] = $response;
                $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
            }
        } else {
            $response->message = $this->lang->line('invalid_voucher');
            $data['response'] = $response;
            $this->load->view(MOBILE_M . '/voucher_invalid_v', $data);
        }
    }

}

//eoc
