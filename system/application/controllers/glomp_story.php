<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Glomp_story extends CI_Controller {	
	private $INSTANCE="gl_voucher";
	private $INSTANCE_USER="gl_user";
	function __construct()
	{		
		parent::__Construct();
		$this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->library('custom_func');        
	}
    function share_redeem($voucher_id="")
    {   
        $voucher=$voucher_id;
        $story_type=$this->input->get('st');
        $result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
        if(($result->num_rows())>0){
            $res=$result->row();
            $purchaserID=$res->voucher_purchaser_user_id;
            $belongsID=$res->voucher_belongs_usser_id;
            //first of all verify user_id				
            $purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
            $purchaser_data=$purchaser_rec->row();
            
            $belongs_rec = $this->users_m->user_info_by_id($belongsID);				
            $belongs_data=$belongs_rec->row();
            
            $data['purchaser_data']=$purchaser_data;
            $data['belongs_data']=$belongs_data;
            $data['product_id']=$res->voucher_product_id;
            $data['story_type']=$story_type;            
            
            //echo $belongs_rec->num_rows()."-".$belongsID."<br>";
            //echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
                        
    
            
            $data['voucher']	=$voucher;                
            $this->load->view('glomp_story_share_redeem_v',$data);
        }        
    }
    function share($voucher_id="")
    {   
        $voucher=$voucher_id;
        $story_type=$this->input->get('st');
        $result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
        if(($result->num_rows())>0){
            $res=$result->row();
            $purchaserID=$res->voucher_purchaser_user_id;
            $belongsID=$res->voucher_belongs_usser_id;
            //first of all verify user_id				
            $purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
            $purchaser_data=$purchaser_rec->row();
            
            $belongs_rec = $this->users_m->user_info_by_id($belongsID);				
            $belongs_data=$belongs_rec->row();
            
            $data['purchaser_data']=$purchaser_data;
            $data['belongs_data']=$belongs_data;
            $data['product_id']=$res->voucher_product_id;
            $data['story_type']=$story_type;            
            
            //echo $belongs_rec->num_rows()."-".$belongsID."<br>";
            //echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
                        
    
            
            $data['voucher']	=$voucher;                
            $this->load->view('glomp_story_share_v',$data);
        }        
    }
    function tweet_redeem($voucher_id="")
    {   
        $voucher=$voucher_id;        
        $result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
        if(($result->num_rows())>0){
            $res=$result->row();
            $purchaserID=$res->voucher_purchaser_user_id;
            $belongsID=$res->voucher_belongs_usser_id;
            //first of all verify user_id				
            $purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
            $purchaser_data=$purchaser_rec->row();
            
            $belongs_rec = $this->users_m->user_info_by_id($belongsID);				
            $belongs_data=$belongs_rec->row();
            
            $data['purchaser_data']=$purchaser_data;
            $data['belongs_data']=$belongs_data;
            $data['product_id']=$res->voucher_product_id;                   
            
            //echo $belongs_rec->num_rows()."-".$belongsID."<br>";
            //echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
                        
    
            
            $data['voucher']	=$voucher;                
            $this->load->view('glomp_story_tweet_redeem_v',$data);
        }        
    }
    function tweet($voucher_id="")
    {   
        $voucher=$voucher_id;        
        $result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
        if(($result->num_rows())>0){
            $res=$result->row();
            $purchaserID=$res->voucher_purchaser_user_id;
            $belongsID=$res->voucher_belongs_usser_id;
            //first of all verify user_id				
            $purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
            $purchaser_data=$purchaser_rec->row();
            
            $belongs_rec = $this->users_m->user_info_by_id($belongsID);				
            $belongs_data=$belongs_rec->row();
            
            $data['purchaser_data']=$purchaser_data;
            $data['belongs_data']=$belongs_data;
            $data['product_id']=$res->voucher_product_id;                   
            
            //echo $belongs_rec->num_rows()."-".$belongsID."<br>";
            //echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
                        
    
            
            $data['voucher']	=$voucher;                
            $this->load->view('glomp_story_tweet_v',$data);
        }        
    }    
	function index()
	{
		$voucher=$this->input->get('voucher');
		$fbID=$this->input->get('fbID');
		if($voucher!="" && $fbID!=""){
			//check this voucher and fbID if exists			
			$result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
			//check this voucher and fbID if exists
			if(($result->num_rows())>0){
				$res=$result->row();
				$purchaserID=$res->voucher_purchaser_user_id;
				$belongsID=$res->voucher_belongs_usser_id;
				//first of all verify user_id				
				$purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
				$purchaser_data=$purchaser_rec->row();
				
				$belongs_rec = $this->users_m->user_info_by_id($belongsID);				
				$belongs_data=$belongs_rec->row();
				
				$data['purchaser_data']=$purchaser_data;
				$data['belongs_data']=$belongs_data;
				
				//echo $belongs_rec->num_rows()."-".$belongsID."<br>";
				//echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
				
				//check belongs fb id of match
				if($belongs_data->user_fb_id==$fbID)
				{
					$data['voucher']	=$voucher;
					$data['fbID']		=$fbID;
					$this->load->view('glomp_story_v',$data);
				}				
				//check belongs fb id of match
				
				
			}
			
		}
		else{

		}
	}
	function linkedIn()
	{
		$voucher=$this->input->get('voucher');
                $linkedInID=$this->input->get('linkedInID');
                
		if($voucher!=""){
			//check this voucher and linkedInID if exists			
			$result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
			//check this voucher and linkedInID if exists
			if(($result->num_rows())>0){
				$res=$result->row();
				$purchaserID=$res->voucher_purchaser_user_id;
				$belongsID=$res->voucher_belongs_usser_id;
				//first of all verify user_id				
				$purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
				$purchaser_data=$purchaser_rec->row();
				
				$belongs_rec = $this->users_m->user_info_by_id($belongsID);				
				$belongs_data=$belongs_rec->row();
				
				$data['purchaser_data']=$purchaser_data;
				$data['belongs_data']=$belongs_data;
				
				//echo $belongs_rec->num_rows()."-".$belongsID."<br>";
				//echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
                                
                                $data['voucher'] = $voucher;
                                
                                if($linkedInID != "")
                                {
                                  return redirect(site_url('landing/?voucher='.$voucher.'&linkedInID='.$linkedInID));  
                                }
                                
                                return redirect(site_url('landing/?voucher='.$voucher));
			}
			
		}
		else{

		}
	}

}//eoc
