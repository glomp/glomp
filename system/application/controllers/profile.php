<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__Construct();
		$this->load->library('user_login_check');
		$this->load->model('cmspages_m');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('merchant_m');
		$this->load->model('regions_m');
		$this->load->model('send_email_m');
		$this->load->model('push_notifications_m');
		
	}
	function getMoreStories()
    {
        if($_POST && isset($_POST['start_count']))
        {
            
            $start_count=$this->input->post('start_count');
            $profile_id=$this->input->post('profile_id');            
            $glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id,$start_count,5);
            $data['glomp_buzz']     = $glomp_buzz;
            $data['start_count']    = $start_count;
            $data['page_type']      = 'profile';
            $data['buzz_user_id'] = $this->session->userdata('user_id');
            // underage filter
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
			
            $this->load->view('user_more_stories_v', $data);
        }
    }	
	function index($tab='tabBuz')
	{
		$profile_id = (int) $this->session->userdata('user_id');
		$tab = '#'.$tab.'_';
		$data['tab'] = $tab;
        $start_count=0;
		$glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id,0,10);
        $data['start_count'] = $start_count;
		$data['buzz_user_id'] = $profile_id;
		$data['glomp_buzz'] = $glomp_buzz;//data passed to view
		$glom_buzz_count = $glomp_buzz->num_rows();
		$data['glom_buzz_count'] = $glom_buzz_count;//data passed to view
		$rec_user = $this->users_m->user_info_by_id($profile_id);
		if($rec_user->num_rows()==1){
			$user_recod = $rec_user->row();
			$data['user_record'] = $user_recod;
			//list of merchant according to user regions
			$data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($user_recod->user_city_id);
			
			$data['inTour']=$this->input->get('inTour') ;
			$data['favAdded']=$this->input->get('favAdded') ;
			
            // brands
            $this->load->model('brand_m','Brand');
            $this->db->order_by( 'name', 'asc' );
            $this->db->where(array('status'=>'Active', 'location_id' => $user_recod->user_city_id));
            $q = $this->db->get( 'gl_brand' );
            $brands = FALSE;
            if( $q->num_rows() > 0 ) {
                $brands = $q->result();
            }
            $data['brands'] = $brands;
            
            // underage filter
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
			
            $this->load->view('user_my_profile_v', $data, true, true);
			
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v');	
		}
	
	}
	
	public function removeFevItem($itemID)
		{
			$this->product_m->removeuserFevItem($itemID);
			die('success');
		}
		
	public function addFevitemToFevList($itemID){
			$this->product_m->addFevitemToFevList($itemID);
			die('success');
	}
	public function view($profile_id=0)
	{
		$profile_id = (int) $profile_id;
		
        
		$start_count=0;		
        $data['start_count'] = $start_count;
		$glomp_buzz = $this->users_m->glomp_buzz_specific_user($profile_id,$start_count,10);
		$data['buzz_user_id'] = $profile_id;
		$data['glomp_buzz'] = $glomp_buzz;//data passed to view
		$glom_buzz_count = $glomp_buzz->num_rows();
		$data['glom_buzz_count'] = $glom_buzz_count;//data passed to view
		
        //remove friend from friend list
        if(isset($_POST['remove_friend_id']))
        {
            $remove_friend_id = $this->input->post('remove_friend_id');
            $removed = $this->users_m->remove_friend($this->session->userdata('user_id'),$profile_id);		
            
        }
		
		$rec_user = $this->users_m->user_info_by_id($profile_id);
		if($profile_id == $this->session->userdata('user_id'))
		{
			
			$showbrandproducts = $this->input->get('showbrandproducts', TRUE);
			if($showbrandproducts!='')
				redirect('/profile?showbrandproducts='.$showbrandproducts);
			else
				redirect('/profile');
			exit();
		}
		if($rec_user->num_rows()==1){
			$user_recod = $rec_user->row();
			$data['user_record'] = $user_recod;			
			//list of merchant according to user regions			
			$data['region_wise_merchant'] = $this->merchant_m->regionwise_merchant($user_recod->user_city_id);
            
            // brands
            $this->load->model('brand_m','Brand');
            $this->db->order_by( 'name', 'asc' );
            $this->db->where(array('status'=>'Active', 'location_id' => $user_recod->user_city_id));
            $q = $this->db->get( 'gl_brand' );
            $brands = FALSE;
            if( $q->num_rows() > 0 ) {
                $brands = $q->result();
            }
            $data['brands'] = $brands;
			
            // underage filter
            $data['underage_filtered'] = explode( ',', $this->db->get('gl_underage_sections')->row()->product_categories );
			
            if($this->session->userdata('is_user_logged_in')== false)
            {  // public profile
                $this->load->view('user_profile_public_v', $data, false, false);
            }
            else
            {
                $this->load->view('user_profile_v', $data, true, true);
            }
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v');	
		}
	}
	function add_friend($friend_id)
	{
		//first of all verify user_id
		$res_num = $this->users_m->user_info_by_id($friend_id);
		$rec_num = $res_num->num_rows();
		if($rec_num>0)
		{
			$friend_added = $this->users_m->add_friend($this->session->userdata('user_id'),$friend_id);
		redirect('/profile/view/'.$friend_id);
			exit();
		}
	}
	
	function add_friend2($friend_id)
	{
		//first of all verify user_id
		$res_num = $this->users_m->user_info_by_id($friend_id);
		$rec_num = $res_num->num_rows();
		if($rec_num>0)
		{
			$friend_added = $this->users_m->add_friend($this->session->userdata('user_id'),$friend_id);
		die('success');
		}
	}
	//This is for other user, logged in user can glomp any listed product from this merchant
	function merchantProduct($profile_id,$merchant_id)
	{
		$profile_id = (int) $profile_id;
		$merchant_id = (int) $merchant_id;
		$data['profile_id'] = $profile_id;
		//verify user id
		
		$rec_user = $this->users_m->user_info_by_id($profile_id);
		
		if($this->users_m->is_user($profile_id) && $this->merchant_m->is_mercahnt($merchant_id))
		{
			$data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
			$data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);			
			$this->load->view('user_merchant_product_v',$data);	
		}else
		{
			echo "";
		}
	}
	//Merchant list for non member
	function merchantProductNonMember($merchant_id)
	{
		$merchant_id = (int) $merchant_id;

                $data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
                $data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);			
                $this->load->view('user_merchant_product_nonmember_v',$data);
	}
	//this is for login user so we do not need user id. User just add product as favourite
	function merchantProductlist($merchant_id)
	{
		$user_id = $this->session->userdata('user_id');
		$merchant_id = (int) $merchant_id;
		$data['profile_id'] = $user_id;
		//verify user id
		
		$rec_user = $this->users_m->user_info_by_id($user_id);
		
		if($this->users_m->is_user($user_id) && $this->merchant_m->is_mercahnt($merchant_id))
		{
			$data['res_merchant'] = $this->merchant_m->selectMerchantID($merchant_id)->row();
			$data['merchant_product'] = $this->product_m->product_by_merchant_id($merchant_id);
			
			$this->load->view('my_merchant_product_v',$data);	
		}else
		{
			echo "";
		}	
	}
function glompToUserFacebook(){
	$message = $this->input->post('message');
	$password = $this->input->post('password');
	$user_fname = $this->input->post('user_fname');
	$user_lname = $this->input->post('user_lname');
	$user_fbID = $this->input->post('user_fbID');	
	$country = $this->input->post('location_id');	
	$product_id = $this->input->post('product_id');	
	$this->load->model('login_m');
	$userID = $this->session->userdata('user_id');
	
	
		if(is_numeric($country)){
			$location_id= $country;
		}
		else{
			$location_id= $this->regions_m->getCountryID($country);
			
		}
	
	if($user_fname == "")
	{
		die("{status:'error',msg:'".$this->lang->line('first_name_blank')."'}");
	}
	else if($user_lname == "")
	{
		die("{status:'error',msg:'".$this->lang->line('last_name_blank')."'}");
	}		
	$status = $this->login_m->password_verify($password,$userID);
	if($status=="ok")
		{
			$glomp_status = $this->user_account_m->glomp_to_friend_fb($userID , $product_id , $message,$user_fname,$user_lname,$user_fbID,$location_id);
			$key = json_decode($glomp_status);
			$glomp_status  = $key->txn_status;
			if($glomp_status == "invalid_friend")
			{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Friend')."'}");
			}
			else if($glomp_status == "SUCCESS")
				{
					$voucher_id=$key->voucher_id;
					die("{status:'success',voucher_id:'".$voucher_id."',msg:'".$this->lang->line('Successfully_Glomped')."'}");
				}
			else if($glomp_status == "unexpected_error")
				{
					die("{status:'error',msg:'".$this->lang->line('Unexpected_Error')."'}");
				}
				else if($glomp_status == "insufficent_balance")
				{
					die("{status:'error',msg:'".$this->lang->line('Insufficent_Balance')."'}");
				}
				
			else if($glomp_status == "invalid_product")
				{
					die("{status:'error',msg:'".$this->lang->line('Invalid_Product')."'}");
				}
		}
	else
		{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Password')."'}");
		}
	
}	
function glompToUserLinkedIn(){
	$message = $this->input->post('message');
	$password = $this->input->post('password');
	$user_fname = $this->input->post('user_fname');
	$user_lname = $this->input->post('user_lname');
	$user_linkedInID = $this->input->post('user_linkedInID');	
	$country = $this->input->post('location_id');	
	$product_id = $this->input->post('product_id');	
	$this->load->model('login_m');
	$userID = $this->session->userdata('user_id');
	
	
		if(is_numeric($country)){
			$location_id= $country;
		}
		else{
			$location_id= $this->regions_m->getCountryID($country);
			
		}
	
	if($user_fname == "")
	{
		die("{status:'error',msg:'".$this->lang->line('first_name_blank')."'}");
	}
	else if($user_lname == "")
	{
		die("{status:'error',msg:'".$this->lang->line('last_name_blank')."'}");
	}		
	$status = $this->login_m->password_verify($password,$userID);
	if($status=="ok")
		{
			$glomp_status = $this->user_account_m->glomp_to_friend_li($userID , $product_id , $message,$user_fname,$user_lname,$user_linkedInID,$location_id);
			$key = json_decode($glomp_status);
			$glomp_status  = $key->txn_status;
			if($glomp_status == "invalid_friend")
			{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Friend')."'}");
			}
			else if($glomp_status == "SUCCESS")
				{
					$voucher_id=$key->voucher_id;
					die("{status:'success',voucher_id:'".$voucher_id."',msg:'".$this->lang->line('Successfully_Glomped')."'}");
				}
			else if($glomp_status == "unexpected_error")
				{
					die("{status:'error',msg:'".$this->lang->line('Unexpected_Error')."'}");
				}
				else if($glomp_status == "insufficent_balance")
				{
					die("{status:'error',msg:'".$this->lang->line('Insufficent_Balance')."'}");
				}
				
			else if($glomp_status == "invalid_product")
				{
					die("{status:'error',msg:'".$this->lang->line('Invalid_Product')."'}");
				}
		}
	else
		{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Password')."'}");
		}
	
}	

function vocher_expiration_day($add_day) {
	$date = date('Y-m-d');
	$sql = "SELECT  ADDDATE('" . $date . "','$add_day DAY')AS new_date";
	$res = $this->db->query($sql);
	$rec = $res->row();
	return $rec->new_date;
}
function mysql_uuid()
{
	$sql = "select UUID() as uuid";
	$res = $this->db->query($sql);
	return $res->row()->uuid;
}
function glompToUser($friendID=0)
	{
	$message = $this->input->post('message');
	$password = $this->input->post('password');
	$user_fname = $this->input->post('user_fname');
	$user_lname = $this->input->post('user_lname');
	$user_email = $this->input->post('user_email');
	$product_id = $this->input->post('product_id');	
	$location_id = $this->input->post('location_id');	
	$this->load->model('login_m');
	$userID = $this->session->userdata('user_id');
	
	if($user_fname == "")
			{
			die("{status:'error',msg:'".$this->lang->line('first_name_blank')."'}");
			}
		else if($user_lname == "")
			{
			die("{status:'error',msg:'".$this->lang->line('last_name_blank')."'}");
			}
			
		else if($this->custom_func->emailValidation($user_email) == "false")
			{
			die("{status:'error',msg:'".$this->lang->line('invalid_email_address')."'}");
			}
			
			
	$status = $this->login_m->password_verify($password,$userID);
	if($status=="ok")
		{
			$glomp_status = $this->user_account_m->glomp_to_friend($userID , $product_id , $message,$user_fname,$user_lname,$user_email,$location_id);
			$key = json_decode($glomp_status);
			$glomp_status  = $key->txn_status;
			if($glomp_status == "invalid_friend")
			{
                die("{status:'error',msg:'".$this->lang->line('Invalid_Friend')."'}");
			}
			else if($glomp_status == "SUCCESS")
            {
                $voucher_id=$key->voucher_id;   
                $voucher_result = $this->users_m->get_specific_voucher_details($voucher_id);
                $merchant_name = $to_name_real = $form_name_real = $from_fb_id = $to_fb_id = $story_type = $prod_name = $product_logo = "";
                if($voucher_result->num_rows()>0)
                {
                    
                    $voucher_data = $voucher_result->row();
                    
                    $from_id = $voucher_data->voucher_purchaser_user_id;
                    $to_id = $voucher_data->voucher_belongs_usser_id;
                    $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                    
                    $form_name_real = $frn_info->friends->$from_id->user_name;
                    $to_name_real = $frn_info->friends->$to_id->user_name;
                    
                    $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                    $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
                    
                    $story_type="1";
                    
                    //product and merchant info
                    $prod_id = $voucher_data->voucher_product_id;
                    $prod_info = json_decode($this->product_m->productInfo($prod_id));
                    $prod_name = $prod_info->product->$prod_id->prod_name;
                    $prod_image= $prod_info->product->$prod_id->prod_image;        
                    $product_logo = $this->custom_func->product_logo($prod_image);                    
                    $merchant_name = $prod_info->product->$prod_id->merchant_name;
                    
                    $details =array ('voucher_id'=>$voucher_id,
                                    'merchant_name'=>$merchant_name,
                                    'to_name_real'=>$to_name_real,
                                    'form_name_real'=>$form_name_real,
                                    'from_fb_id'=>$from_fb_id,
                                    'to_fb_id'=>$to_fb_id,
                                    'story_type'=>$story_type,
                                    'prod_name'=>$prod_name,
                                    'product_logo'=>$product_logo,
                                    'status' => 'success',
                                    'msg' => $this->lang->line('Successfully_Glomped'));
                    }
                    
                    die(json_encode($details));
                //die("{status:'success',msg:'".$this->lang->line('Successfully_Glomped')."'}");
            }
			else if($glomp_status == "unexpected_error")
				{
					die("{status:'error',msg:'".$this->lang->line('Unexpected_Error')."'}");
				}
				else if($glomp_status == "insufficent_balance")
				{
					die("{status:'error',msg:'".$this->lang->line('Insufficent_Balance')."'}");
				}
				
			else if($glomp_status == "invalid_product")
				{
					die("{status:'error',msg:'".$this->lang->line('Invalid_Product')."'}");
				}
		}
	else
		{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Password')."'}");
		}
	}
	

function reassignVoucher()
	{
	$message = $this->input->post('message');
	$password = $this->input->post('password');
	$fname = $this->input->post('fname');	
	$lname = $this->input->post('lname');	
	$email = $this->input->post('email');	
	$voucgerID = $this->input->post('voucger_id');
	$location_id = $this->input->post('location_id');
		if($fname == "")
			{
			die("{status:'error',msg:'".$this->lang->line('first_name_blank')."'}");
			}
			
			else if($lname == "")
			{
			die("{status:'error',msg:'".$this->lang->line('last_name_blank')."'}");
			}
			
			else if($this->custom_func->emailValidation($email) == "false")
			{
			die("{status:'error',msg:'".$this->lang->line('invalid_email_address')."'}");
			}
		
	$this->load->model('login_m');
	$userID = $this->session->userdata('user_id');
	$status = $this->login_m->password_verify($password,$userID);
	if($status=="ok")
		{
			$glomp_status = $this->user_account_m->reassignVoucher($userID ,$message, $fname , $lname, $email,$voucgerID, $location_id);
			$key = json_decode($glomp_status);
			$glomp_status  = $key->txn_status;
			
			if($glomp_status == "SUCCESS")
				{
					die("{status:'success',msg:'".$this->lang->line('Successfully_Glomped')."'}");
				}
			
				
			else if($glomp_status == "invalid_voucher")
				{
					die("{status:'error',msg:'".$this->lang->line('Invalid_Product')."'}");
				}
		}
	else
		{
			die("{status:'error',msg:'".$this->lang->line('Invalid_Password')."'}");
		}
	}

    function reassignVoucherNew() {
            $message = $this->input->post('message');
            $password = $this->input->post('password');
            $fname = $this->input->post('fname');
            $email = $this->input->post('email');
            $voucher_id = $this->input->post('voucher_id');
            $fb_id = $this->input->post('fb_id');
            $fb_fname = $this->input->post('fb_fname');
            $fb_lname = $this->input->post('fb_lname');
            $fb_dob = $this->input->post('fb_dob');
            $location_id = $this->input->post('location_id');
            $fb_email = $this->input->post('fb_email');

            if ($fname == "") {
                die("{status:'error',msg:'" . $this->lang->line('name_blank') . "'}");
            } else if ($this->custom_func->emailValidation($email) == "false") {
                die("{status:'error',msg:'" . $this->lang->line('invalid_email_address') . "'}");
            }

            $this->load->model('login_m');
            $userID = $this->session->userdata('user_id');
            $status = $this->login_m->password_verify($password, $userID);
            if ($status == "ok") {
                $glomp_status = $this->user_account_m->reassignVoucher($userID, $message, $fname, $fb_lname, $email, $voucher_id, $fb_id, $fb_fname, $fb_lname, $fb_dob, $fb_email, $location_id);
                $key = json_decode($glomp_status);
                $glomp_status = $key->txn_status;
                $voucher_id = $key->voucher_id;

                if ($glomp_status == "SUCCESS") {
                    die("{status:'success',msg:'" . $this->lang->line('Successfully_Glomped') . "',voucher_id:'" . $voucher_id . "'}");
                } else if ($glomp_status == "invalid_voucher") {
                    die("{status:'error',msg:'" . $this->lang->line('Invalid_Product') . "'}");
                }
            } else {
                die("{status:'error',msg:'" . $this->lang->line('Invalid_Password') . "'}");
            }
        }


    /**
     * Use as /profile/menu/brands/$profile_id
     * 
     */
    public function brands( $profile_id, $brand_id = 0, $show_products = '', $product_id = 0 ) {
        include_once(BASEPATH.'application/controllers/brands.php');
        $brand_controller = new Brands;
        
        $profile_id = $this->uri->segments[4];
        $brand_id = isset($this->uri->segments[5])?$this->uri->segments[5]:0;
        $show_products = isset($this->uri->segments[6])?$this->uri->segments[6]:0;
        $product_id = isset($this->uri->segments[7])?$this->uri->segments[7]:0;
        
        if($profile_id != 0) {
            $brand_controller->set_glompee( $profile_id );
        } else {
            return show_404();
        }
        
        if( $brand_id == 0 ) {
            return $brand_controller->index();
        } else {
        
            if( $show_products == '' ) {
                return $brand_controller->page( $brand_id );
            } else {
            
                if( $product_id == 0 ) {
                    $brand_controller->set_subpage('products');
                    return $brand_controller->page( $brand_id );
                } else {
                    
                    $brand_controller->set_subpage('products');
                    $brand_controller->set_product_id( $product_id );
                    return $brand_controller->page( $brand_id );
                }
            }
        }
        
        
    }
}//eoc
