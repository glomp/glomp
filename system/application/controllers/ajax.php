<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct()
	{
		parent::__Construct();
		//$this->load->library('user_login_check');
		$this->load->model('cmspages_m');
		$this->load->model('users_m');
		$this->load->model('regions_m');
	}
function loadLocation($loc="")
	{
	$list="";
	$res = $this->regions_m->selectRegionKeywords($loc);
	if($res->num_rows()>0)
	{
	foreach($res->result() as $rec)
	{	
    $list['searchResult'][] = array('id' => $rec->region_id, 'name' => stripslashes($rec->region_name));
	}
	echo (json_encode($list));
	}
	else
		{
			echo "0";
		}
	}
	
function loadLocation1($loc="")
	{
	$list="";
	$res = $this->regions_m->selectRegionKeywords1($loc);
	if($res->num_rows()>0)
	{
	$list="1";
	}
	else
	{
		$list="0";
	}
	die($list);
	}
	
}//eoc
