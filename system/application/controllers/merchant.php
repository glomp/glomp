<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->library('user_login_check');
		$this->load->model('cmspages_m');
		$this->load->model('product_m');
		$this->load->model('users_m');
		$this->load->model('merchant_m');
		$this->load->model('regions_m');
		$this->load->model('send_email_m');
		$this->load->model('address_m');
        $this->load->model('template_m');
	}		
	function index()	{						
		$data['error_404'] =  $this->lang->line('page_404_error');
		$this->load->view('404_v');			
	}
	function about(){		
		$merchantID=$this->uri->segment(3, 0);		
		if (is_numeric($merchantID)){			
			
            $result= $this->merchant_m->selectMerchantID($merchantID);
            $data['merchant'] = $result->row();
            $data['merchant_record'] = $result->row();
            //Just include the region name on the merchant outlet
			$data['merchant_outlet']= $this->merchant_m->selectOutletsAddress($merchantID);

            $data['template'] = $this->merchant_m->get_template( $merchantID );
            if( isset($data['template']->id) ) {
                $data['sections'] = $this->template_m->_get_sections( $data['template']->id );
            }
            if( isset($data['sections']) ) {
                $data['widgets'] = $this->template_m->_get_widgets( $data['sections'] );
            }
            
            $this->load->view('merchant_about_v2',$data);
		}
		else{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v');
		}
	}
}