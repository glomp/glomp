<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_api extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		$this->load->library('custom_func');		
		$this->load->library('curl');
    }
	
    function index()
    {
	
        $fb_config = array(
            'appId'  => ($this->custom_func->config_value('FB_API_APP_ID_DEV_'.FACEBOOK_API_STATUS)),
            'secret' => ($this->custom_func->config_value('FB_API_APP_SECRET_DEV_'.FACEBOOK_API_STATUS))
        );
		
        $this->load->library('facebook', $fb_config);

        $user = $this->facebook->getUser();

        if ($user) {
            try {
                $data['user_profile'] = $this->facebook
                    ->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }

        if ($user) {
            $data['logout_url'] = $this->facebook->getLogoutUrl();
			
			
        } else {
            $data['login_url'] = $this->facebook->getLoginUrl();
			
        }		
		
        $this->load->view('facebook_api_v',$data);
    }
}//eoc