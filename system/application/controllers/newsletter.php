<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	
	function __construct()
	{
		parent::__Construct();  
        $this->load->model('campaign_newsletter_m');
        $this->load->model('newsletter_m');
        $this->load->model('users_m');
		
	}
	public function view($campaign_newsletter_id=0)
	{			
       $this->load->helper('html');
        
        $result = $this->campaign_newsletter_m->exists('campaign_newsletter_id = "'. $campaign_newsletter_id. '"');
        
        if (!$result)
        {
            $data['error_404'] =  $this->lang->line('page_404_error');
            $this->load->view('404_v');			
        }
        else
        {
        
            $camp['newsletter'] = $this->campaign_newsletter_m->get_record(array(
                'where' => array('campaign_newsletter_id' => $campaign_newsletter_id)
            ));

            $template = $this->newsletter_m->get_record(array(
                'where' => array('newsletter_id' => $camp['newsletter']['newsletter_template'])
            ));
            $template_data['campaign_newsletter_id']=$campaign_newsletter_id;
            $template_data['attributes'] = json_decode($camp['newsletter']['newsletter_template_value'], TRUE);
            
            //layout
            $data['template'] = $this->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $template['name'], $template_data, TRUE);
            $data['newsletter'] = $camp['newsletter'];
            echo $data['template'];
        }
        //$this->load->view('/newsletter_v', $data);
    }
}//eocs