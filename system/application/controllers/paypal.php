<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class paypal extends CI_Controller {
    var $PAYPAL_ENVIRONMENT='live';//live or sandbox
	function __construct()
	{
		parent::__Construct();
	/*	$this->load->model('cmspages_m');
		$this->load->model('users_m');
		$this->load->model('product_m');
		$this->load->model('regions_m');
		$this->load->model('login_m');*/
	}
	
	function payment()
	{
		$message = date('Y-m-d H:i:s')."=>IPN email";
		$a = $this->sendEmail('shree@youngminds.com.np','shree','shree@youngminds.com.hk','shree HK','paypal msg check',$message);
		print_r($a);
		
	}
	private function sendEmail($from_email,$from_name,$to_email,$to_name,$subject,$message)
	{
			$this->load->library('email');
			
			$this->email->initialize(array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'smtp.sendgrid.net',
			  'smtp_user' => 'glompzac',
			  'smtp_pass' => '6lomp!DOT1+_send',
			  'smtp_port' => 587,
			  'crlf' => "\r\n",
			  'newline' => "\r\n",
			  'mailtype' => 'html'
			));

			/*$param = array($to_name,$message)*/
			$message_array = array('to_name'=>$to_name,'message'=>$message);
			$bind_message = $this->custom_func->emailTemplate($message_array);

			$this->email->from($from_email, $from_name);
			$this->email->to($to_email,$to_name);
			$this->email->subject($subject);
			$this->email->message($bind_message);
			$this->email->send();
	//
	//		echo $this->email->print_debugger();


	}	//sendEmail

	function hsbc_ipn($public_alias = 'hsbc') {
		$environment = $this->PAYPAL_ENVIRONMENT;

		switch ($public_alias) {
		    case 'hsbc':
	            $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
	            $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
	            $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
	            $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
	            $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	            $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
	            $kpo =  HSBC_BRAND_PROD_ID_KPO;
	            $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
	            $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	            $valrhona = '0';
	            $laurent_perrier =  '0';
		        $providore =  '0';
		        $glenfiddich =  '0';
		        $moet_chandon =  '0';
		        $kusmi_tea =  '0';
		        $euraco = '0';
	            break;
	        case 'uob':
	            $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
	            $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
	            $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
	            $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
	            $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	            $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
	            $kpo =  UOB_BRAND_PROD_ID_KPO;
	            $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
	            $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	            $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
	            $laurent_perrier =  '0';
	            $providore =  '0';
		        $glenfiddich =  '0';
		        $moet_chandon =  '0';
		        $kusmi_tea =  '0';
		        $euraco = '0';
	            
	            break;
	        case 'dbs':
	            $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
	            $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
	            $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
	            $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
	            $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	            $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
	            $kpo =  DBS_BRAND_PROD_ID_KPO;
	            $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
	            $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	            $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
	            $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
	            $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
		        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
		        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
		        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
		        $euraco = DBS_BRAND_PROD_ID_EURACO;
	            break;
		}

		define('BRAND_PROD_ID_MACALLAN',$macallan);
		define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
		define('BRAND_PROD_ID_LONDON_DRY',$london);

		define('BRAND_PROD_ID_SINGBEV',$singbev);
		define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
		define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

		define('BRAND_PROD_ID_KPO',$kpo);
		define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
		define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);	
		define('BRAND_PROD_ID_VALRHONA',$valrhona);
		define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
		define('BRAND_PROD_ID_PROVIDORE',$providore);
		define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
		define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
		define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
		define('BRAND_PROD_ID_EURACO',$euraco);

		
		$this->load->model('Brand_m');
		$paypal_txn_verified = FALSE;
		$sesion_payment_id = isset($_POST['custom']) ? $_POST['custom']: 0;
		$gateway_txn_id = isset($_POST['txn_id']) ? $_POST['txn_id']: 0;

		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2) {
				$myPost[$keyval[0]] = urldecode($keyval[1]);
			}
		}
		// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		$payPalURL = "https://www.paypal.com/webscr";
        if("sandbox" === $environment || "beta-sandbox" === $environment) {
            $payPalURL = "https://SHA2-test-ipnpb.sandbox.paypal.com/cgi-bin/webscr";                
        }

		// STEP 2: POST IPN data back to PayPal to validate
		$ch = curl_init($payPalURL);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		 
		if( !($res = curl_exec($ch)) ) {
			//redirect to brands paypal error message
		    curl_close($ch);
		    //return redirect();
		}
		curl_close($ch);

		 
		// STEP 3: Inspect IPN validation result and act accordingly
		if (strcmp ($res, "VERIFIED") == 0) {
			// The IPN is verified, process it:
			$paypal_txn_verified = TRUE;
		} else if (strcmp ($res, "INVALID") == 0) {
			//echo "The response from IPN was: <b>" .$res ."</b>";
			$txn_invalid = TRUE;
		}

		//echo $sesion_payment_id . ' --->' . $paypal_txn_verified;

		if ($paypal_txn_verified) {
			$where = "session_token_id = '".$sesion_payment_id."'
			AND session_status != 'Completed'";
			$query = $this->db->get_where('gl_campaign_payment_session', $where);
			
			if ($query->num_rows() > 0) {
				$session_detail = json_decode($query->row()->session_details);
				$this->load->model('login_m');
				$voucher_code = strtoupper($this->login_m->generate_hash(6));

				$group_voucher_id = $this->mysql_uuid();
				$generate_voucher_1 = array(
					'group_voucher_id' => $group_voucher_id,
					'session_cart_id' => $query->row()->session_cart_id,
					'forex' => BRAND_FOREX_AMEX,
					'transaction_type' => $session_detail->transaction_type,
					'from_email' => $session_detail->buyer->email,
					'from_first_name' => $session_detail->buyer->fname,
					'from_last_name' => $session_detail->buyer->lname,
					'email' => $session_detail->glompee->email,
					'first_name' => $session_detail->glompee->fname,
					'last_name' => $session_detail->glompee->lname,
					'contact_num' => $session_detail->buyer->contact_num,
					'delivery_postcode' => $session_detail->buyer->post_code,
					'delivery_address' => $session_detail->buyer->street_address,
					'delivery_type' => $session_detail->delivery_type,
					'response_data' => json_encode($res),
					'voucher_code' => $voucher_code
				);
				$voucher_insert_status = $this->db->insert('gl_voucher_brand', $generate_voucher_1);

				//Buyer
				$buyer = $this->db->get_where('gl_user', array('user_email' => $session_detail->buyer->email));

				if ($buyer->num_rows() == 0) 
				{
					$signup_data = array('username' => $session_detail->buyer->email,
						'user_fname' => $session_detail->buyer->fname,
						'user_lname' => $session_detail->buyer->lname,
						'user_email' => $session_detail->buyer->email,
						'user_city_id' => 1,
						'user_join_date' => date('Y-m-d H:i:s'),
						'user_account_created_ip' => $this->custom_func->getUserIP(),
						'user_hash_key' => $this->mysql_uuid(),
						'user_status' => 'Pending',
						'user_last_updated_date' => $this->custom_func->datetime()
					);

					$this->db->insert('gl_user', $signup_data);
					$insert_id = $this->db->insert_id();
					$sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
					$buyer = $this->db->query($sql);
				}
				//getting record
				$rec = $buyer->row();
				$voucher_purchaser_user_id = $rec->user_id;
				$glompee = $this->db->get_where('gl_user', array('user_email' => $session_detail->glompee->email));

				if ($glompee->num_rows() == 0) 
				{
					$signup_data = array('username' => $session_detail->glompee->email,
						'user_fname' => $session_detail->glompee->fname,
						'user_lname' => $session_detail->glompee->lname,
						'user_email' => $session_detail->glompee->email,
						'user_city_id' => 1,
						'user_join_date' => date('Y-m-d H:i:s'),
						'user_account_created_ip' => $this->custom_func->getUserIP(),
						'user_hash_key' => $this->mysql_uuid(),
						'user_status' => 'Pending',
						'user_last_updated_date' => $this->custom_func->datetime()
					);

					$this->db->insert('gl_user', $signup_data);
					$insert_id = $this->db->insert_id();
					$sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
					$glompee = $this->db->query($sql);
				}
				$rec = $glompee->row();
				$voucher_belongs_usser_id = $rec->user_id;
				
				$glomp_message ='';
				$voucher_code = time(1111, 9999) . '-' . time(1111, 9999) . '-' . time(1111, 9999);
				$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$generate_voucher_1['session_cart_id']."' ";
				$result = $this->db->query($sql);
				foreach($result->result() as $row) {
					// on each prod_qty
					// on each prod_qty
					for($x=1; $x <= $row->prod_qty; $x++)
					{
						$product = json_decode($row->prod_details);
						$generate_voucher_id =$this->mysql_uuid();
						$generate_voucher = array('voucher_id' => $generate_voucher_id,
							'voucher_purchaser_user_id' => $voucher_purchaser_user_id,
							'voucher_belongs_usser_id' => $voucher_belongs_usser_id,
							'voucher_merchant_id' => $product->merchant_id,
							'voucher_product_id' => $product->product_id,
							'voucher_point' => $product->prod_point,
							'voucher_prod_cost' => $product->prod_merchant_cost,
							'voucher_reverse_point' => $product->prod_reverse_point,
							'voucher_code' => $voucher_code,
							'voucher_purchased_date' => date('Y-m-d H:i:s'),
							'voucher_status' => 'Consumable',
							'voucher_expiry_day' => 9999,
							'voucher_type' => 'Consumable',
							'voucher_expiry_date' => $this->vocher_expiration_day(9999),
							'voucher_sender_glomp_message' => addslashes($glomp_message)
						);
						$voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);
						
						$insert_data = array(
							'group_voucher_id' => $group_voucher_id,
							'voucher_id' => $generate_voucher_id
						);
						$voucher_insert_status = $this->db->insert('gl_voucher_brand_group_list', $insert_data);
					}
				}

				$update_data = array(
					'session_status' => 'Completed'
				);

				$this->db->where('session_token_id', $sesion_payment_id);
				$this->db->update('gl_campaign_payment_session', $update_data); 

				//return;
				//Send emails
				$this->load->library('email_templating');

				$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
	 				'group_voucher_id' => $group_voucher_id
				));
				if ($voucher_brand->num_rows() > 0) {
					// get cart_details
					$sql = "SELECT *,
						CASE brandproduct_id
							WHEN '".BRAND_PROD_ID_MACALLAN."' THEN 0
							WHEN '".BRAND_PROD_ID_SNOW_LEOPARD."' THEN 0
							WHEN '".BRAND_PROD_ID_LONDON_DRY."' THEN 0
							ELSE brandproduct_id
						END as group_brandproduct_id
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
							public_alias = '".$public_alias."' 		
						GROUP BY group_brandproduct_id
						";
					
					$result = $this->db->query($sql);
					$cart_data="";
					foreach( $result->result() as $row)
					{
						if($row->group_brandproduct_id==0)
						{
							$group_brand_array = BRAND_PROD_ID_MACALLAN.','.BRAND_PROD_ID_SNOW_LEOPARD.','.BRAND_PROD_ID_LONDON_DRY;
							$sql = "SELECT *
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								public_alias = '".$public_alias."' AND		
								brandproduct_id IN (".$group_brand_array.") 
							";
						}
						else
						{
							$sql = "SELECT *
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								public_alias = '".$public_alias."' AND		
								brandproduct_id = '".$row->brandproduct_id."'
							";
						}
						$prod_details = json_decode($row->prod_details);
						$product_list="";
						foreach($this->db->query($sql)->result() as $product)
						{
							$sql = "SELECT *
							FROM gl_cart_option
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								prod_id = '".$product->prod_id."' AND		
								prod_group = '".$product->prod_group."'
							";

							$option = $this->db->query($sql)->result();

							$product_list[] = array (
								'product' => $product,
								'option' => $option,
								);

						}

						$cart_data [] = array (
							'brand_id' => $prod_details->brandproduct_id,
							'brand_name' => $this->Brand_m->get_brandproduct($prod_details->brandproduct_id)->name,
							'prod_list' => $product_list
						);
					
					}
				}

				//Create product details table
				foreach ($cart_data as $brand_row) {
					$brand_id = $brand_row['brand_id'];
					$delivery_type = '';
					$per_brand_total = 0;
					$per_brand_qty = 0;
					$grand_total = 0;
					$grand_total_2 = 0;
					$delivery_total = 0;
					$table="";
					$table2="";
					$has_delivery = FALSE;
					$delivery = '';
					$prouct_merchant_name_arr = array();
					foreach ($brand_row['prod_list'] as $row) {
						$options = $row['option'];	
						$row = $row['product'];
						$cost_price = get_hsbc_prod_data($row->prod_id);
						$delivery_type = $row->delivery_type;
						$product = json_decode($row->prod_details);
						$grand_total += (($row->prod_cost ) * $row->prod_qty);
						$grand_total_2 += (($cost_price['cost_price'] ) * $row->prod_qty);
						$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
						$per_brand_qty += $row->prod_qty;

						$product_merchant_name = '<b>' .$product->prod_name.'</b> <br />'.$product->merchant_name;
						$product_merchant_name_2 = '<b>' .$product->prod_name.'</b> ('.$product->merchant_name.')';
						$prouct_merchant_name_arr[] = $product_merchant_name_2;
						if(count($options) > 0)	{
							$product_merchant_name .= '<br /><br />';
							foreach ($options as $p) {
								$product_merchant_name .= '&nbsp;&nbsp;&nbsp;'. $p->option_qty .'-'. $p->option_value .'<br />';
							}
						}

						$table .='
						<tr>
							<td><b>'.$product_merchant_name.'</td>
							<td align="right">$ '.number_format(($row->prod_cost),2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($row->prod_cost) * ($row->prod_qty),2).'</td>
						</tr>';

						//merchant
						$table2 .='
						<tr>
							<td><b>'.$product_merchant_name.'</td>
							<td align="right">$ '.number_format($cost_price['cost_price'],2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($cost_price['cost_price']) * ($row->prod_qty),2).'</td>
						</tr>';

					}


					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$has_delivery = 16;
							break;
						case BRAND_PROD_ID_SINGBEV:
							if($per_brand_total < 350)
							{
								$has_delivery = 35;
							}	
							break;
						case BRAND_PROD_ID_VALRHONA:
							if($per_brand_total < 300)
							{
								$has_delivery = 35;
							}	
							break;
						case BRAND_PROD_ID_LAURENT_PERRIER:
							if($per_brand_qty < 3)
							{
								$has_delivery = 20;
							}	
							break;
						case BRAND_PROD_ID_PROVIDORE:
							if($per_brand_total < 200)
							{
								$has_delivery = 20;
							}	
							break;
						case BRAND_PROD_ID_EURACO:
							if($per_brand_total < 300)
							{
								$has_delivery = 30;
							}	
							break;
						case BRAND_PROD_ID_DELIGHTS_HEAVEN:
							$has_delivery = 25;
							break;
					}

					if ($delivery_type == 'pickup') {
						$has_delivery = 0;							
					}

					$grand_total_no_delivery = $grand_total;
					$grand_total = $grand_total + $has_delivery;

					if ($has_delivery > 0) {
						$delivery = ' (Delivery charge: S$ '.number_format($has_delivery, 2).')';	
					}

					$product_details ='
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table.'
					</table>';

					//merchant
					$product_details_merchant ='
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table2.'
					</table>';

					$template = '';
					$pick_up_details = '';
					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_MACALLAN:
						case BRAND_PROD_ID_SNOW_LEOPARD:
						case BRAND_PROD_ID_LONDON_DRY:
							$pick_up_details = '
								Edrington Singapore located at 12 Marina View, Level 24-01 Asia Square Tower 2, Singapore 018961 during office hours. <br />
								Please allow 3 working days after order placement.';
							break;
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$pick_up_details = '
								Main Outlet: <br />
								8 Rodyk Street, #01-08 <br />
								Singapore 238216 <br />
								<br />
								Specialty Outlet:<br />
								200 Turf Club Road, #03-07 The Grandstand<br />
								(Chillax Market)<br />
								Singapore 287994<br />
								<br />
								11am - 7pm every day';
							break;
						case BRAND_PROD_ID_SINGBEV:
							$pick_up_details = '
								470 North Bridge Road, #05-09 Bugis Cube Singapore 188735, 
								<br />
								<br />
								Mon - Sat 12pm - 8pm';
						case BRAND_PROD_ID_VALRHONA:
							$pick_up_details = '
								Euraco Finefood Pte Ltd Blk 219 Henderson Road #01-03 Henderson Industrial Park Singapore 159556, 
								<br />
								<br />
								T: +65 6276 5433
								<br />
								Mon - Fri 9:00am-12:00pm, 2:00pm-4:30pm';
							break;
						case BRAND_PROD_ID_PROVIDORE:
							$pick_up_details = 'Pick up your purchases at any of The Providore stores. Please go to www.theprovidore.com/store/ for address and opening hours.';
							break;
					}
					if ($generate_voucher_1['transaction_type'] == 'buy') {
						$template = 'self_purchase_delivery'; //send to purchaser
						$vars['to'] = $generate_voucher_1['email'];
						$vars['params'] = array(
							'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
							'[product_details]' => $product_details,
							'[total_sg_usd]' => 'S$ '.number_format($grand_total,2). $delivery,
							'[gst]' => 'S$ '.number_format($grand_total_no_delivery * 0.07, 2),
							'[total]' => 'S$ '.number_format($grand_total + ($grand_total_no_delivery * 0.07), 2),
							'[delivery_address]' => $generate_voucher_1['delivery_address'],
							'[contact_no]' => $generate_voucher_1['contact_num'],
						);

						if ($delivery_type == 'pickup') {
							$template = 'self_purchase_pickup';
							$vars['params'] = array(
								'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
								'[product_details]' => $product_details,
								'[total_sg_usd]' => 'S$ '.number_format($grand_total,2),
								'[gst]' => 'S$ '.number_format($grand_total_no_delivery * 0.07, 2),
								'[total]' => 'S$ '.number_format($grand_total + ($grand_total_no_delivery * 0.07), 2),
								'[pick_up_details]' => 	$pick_up_details
							);
						}

						if (BRAND_PROD_ID_KPO == $brand_row['brand_id'] || BRAND_PROD_ID_NASSIM_HILL ==$brand_row['brand_id']) {
							//pick up only
							$vars['params']['[merchant_name]'] = $brand_row['brand_name'];
							$template = 'confirmation_selfpurchase_kpo_nassam';
						}

					} else if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend' || $generate_voucher_1['transaction_type'] == 'glomp_to_fb') {
						$recipient_name = $generate_voucher_1['first_name']. ' '.$generate_voucher_1['last_name'];
						if ($generate_voucher_1['transaction_type'] == 'glomp_to_fb') {
							$temp = explode("@",$generate_voucher_1['email']);
							$recipient_name = 'facebook <a href="https://www.facebook.com/'.$temp[0].'" > friend </a>';
						}

						$template = 'notification_glomper_order'; //send to purchaser
						$vars['to'] = $generate_voucher_1['from_email'];
						$vars['params']['[product_details]'] = $product_details;
						$vars['params']['[total_sg_usd]'] = 'S$ '.number_format($grand_total,2). $delivery;
						$vars['params']['[recipient_name]'] = $recipient_name;
						$subject = '';
						if ($delivery_type == 'delivery') {
							if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') {
								$template2 = 'notification_glompee_delivery'; //send to glompee; pickup
								$subject = $generate_voucher_1['from_first_name']. ' has given you a treat!';
								$link = '<a href= "'.site_url('m/brands/acceptance/'.$public_alias.'/'.$generate_voucher_1['group_voucher_id']).'"> here </a>';
								$vars2['params'] = array(
									'[recipient_fname]' => $generate_voucher_1['from_first_name'],
									'[personal_message]' => '',//TODO
									'[merchant_product_1]' => implode(',', $prouct_merchant_name_arr),
									'[merchant_product_2]' => implode(',', $prouct_merchant_name_arr),
									'[bank_name]' => strtoupper($public_alias),
									'[here]' => $link,
								);
							}
						} else {
							if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') { //do not send email for a glomp_to_fb
								$template2 = 'notification_glompee_pickup'; // send to glompee; pick up
								$vars2['params'] = array(
									'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
									'[personal_message]' => '',//TODO
									'[merchant_product_1]' => implode(',', $prouct_merchant_name_arr),
									'[merchant_product_2]' => implode(',', $prouct_merchant_name_arr),
									'[bank_name]' => strtoupper($public_alias),
									'[recipient_fname]' => $generate_voucher_1['from_first_name'],
									'[pick_up_details]' => $pick_up_details,
								);

								if (BRAND_PROD_ID_KPO == $brand_row['brand_id'] || BRAND_PROD_ID_NASSIM_HILL ==$brand_row['brand_id']) { 
									$template2 = 'acceptance_glompee_kpo_nassam';

									$vars2['params'] = array(
										'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
										'[product_details]' => $product_details,
										'[total_sg_usd]' => 'S$ '.number_format($grand_total,2)
									);

									if (BRAND_PROD_ID_KPO == $brand_row['brand_id']) {
										$vars2['params']['[merchant_name]'] = 'KPO';
									} else {
										$vars2['params']['[merchant_name]'] = 'Nassim Hill';
									}
								}
							}
						}
					}

					//send to purchaser
					$vars['template_name'] = $template;
					$vars['lang_id'] = 1;
					$this->email_templating->config($vars);
					$this->email_templating->send();
					echo '<pre>';
					print_r($vars);
					//send to glompee
					if (! empty($template2)) {
						$vars2['template_name'] = $template2;
						$vars2['lang_id'] = 1;
						$vars2['to'] = $generate_voucher_1['email'];
						echo '<pre>';
						print_r($vars2);
						$this->email_templating->config($vars2);
						if (! empty($subject)) {
							echo 'subject:' . $subject;
							$this->email_templating->set_subject($subject);
						}
						$this->email_templating->send();										
					}


					//send merchant
					//do not send to merchant if glomp to fb
					if ($generate_voucher_1['transaction_type'] == 'glomp_to_fb') {
						continue;
					}

					$vars = array(
						'template_name' => 'confirmation_order_erdington',
						'from_name' => SITE_DEFAULT_SENDER_NAME,
						'from_email' => SITE_DEFAULT_SENDER_EMAIL,
						'lang_id' => 1,
						'to' => 'allan.bernabe@gmail.com', //TODO: update sample only
						'params' => array(
							'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
							'[product_details]' => $product_details_merchant,
							'[total_sg]' => 'S$ '.number_format($grand_total_2,2) . $delivery,
							'[0.07_total]' => 'S$ '.number_format(($grand_total_2 * 0.07),2),
							'[total_gst_plus_total]' => 'S$ '. number_format(($grand_total_2 * 0.07) + $grand_total_2, 2),
							//'[total_gst_plus_total]' => 'S$ '. number_format($grand_total_2, 2),
							'[customer_name]' => $generate_voucher_1['from_first_name']. ' '. $generate_voucher_1['from_last_name'],
							'[delivery_address]' => $generate_voucher_1['delivery_address'],
							'[contact_no]' => $generate_voucher_1['contact_num'],
						)
					);
					

					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_MACALLAN:
						case BRAND_PROD_ID_SNOW_LEOPARD:
						case BRAND_PROD_ID_LONDON_DRY:
							$vars['to'] = 'Cynthia.Tan@edrington.com';
							$vars['template_name'] = 'confirmation_order_erdington';
							break;
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$vars['to'] = 'order@swirls.com.sg';
							$vars['template_name'] = 'confirmation_order_swirls';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2). $delivery;
							break;
						case BRAND_PROD_ID_SINGBEV:
							//send email for delivery
							$vars['to'] = 'sabrina@octopusgroup.com.sg';
							$vars['template_name'] = 'confirmation_order_singbev';
							break;
						case BRAND_PROD_ID_VALRHONA:
							//send email for delivery
							$vars['to'] = 'katrina@euraco.com.sg';
							$vars['template_name'] = 'confirmation_order_valrhona';
							break;
						case BRAND_PROD_ID_LAURENT_PERRIER:
							//send email for delivery
							$vars['to'] = 'william@magnum.com.sg';
							$vars['template_name'] = 'confirmation_order_laurent_perrier';
							break;
						case BRAND_PROD_ID_PROVIDORE:
							//send email for delivery
							$vars['to'] = 'fiona@theprovidore.com';
							$vars['template_name'] = 'confirmation_order_providore';
							break;
						case BRAND_PROD_ID_GLENFIDDICH:
							//send email for delivery
							$vars['to'] = 'care@asherbws.com';
							$vars['template_name'] = 'confirmation_order_glenfiddich';
							break;
						case BRAND_PROD_ID_MOET_CHANDON:
							//send email for delivery
							$vars['to'] = 'care@asherbws.com';
							$vars['template_name'] = 'confirmation_order_moet_chandon';
							break;
						case BRAND_PROD_ID_KUSMI_TEA:
							//send email for delivery
							$vars['to'] = 'johanna@globalfoodconsulting.com';
							$vars['template_name'] = 'confirmation_order_kusmi_tea';
							break;
						case BRAND_PROD_ID_EURACO:
								//send email for delivery
								$vars['to'] = 'katrina@euraco.com.sg';
								$vars['template_name'] = 'confirmation_order_euraco';
								break;
						case BRAND_PROD_ID_DELIGHTS_HEAVEN:
							//send email for pick up or delivery
							$vars['to'] = 'thedelightsheaven@gmail.com';
							$vars['template_name'] = 'confirmation_delights_heaven';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2) .$delivery;
							break;
						case BRAND_PROD_ID_DAILY_JUICE:
							//send email delivery is freee
							$vars['to'] = 'roger@dailyjuice.sg';
							$vars['template_name'] = 'confirmation_daily_juice';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							break;
						case BRAND_PROD_ID_KPO:
							//send email pickup only
							$vars['to'] = 'audreyliu@imaginings.com.sg';
							$vars['template_name'] = 'confirmation_order_nassam';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							$vars['params']['[merchant_name]'] = 'KPO';
							break;
						case BRAND_PROD_ID_NASSIM_HILL:
							//send email pickup only
							$vars['to'] = 'audreyliu@imaginings.com.sg';
							$vars['template_name'] = 'confirmation_order_nassam';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							$vars['params']['[merchant_name]'] = 'Nassim Hill';
							break;
					}

					
					if ($generate_voucher_1['transaction_type'] == 'buy' || $generate_voucher_1['transaction_type'] == 'glomp_to_friend') {

						if ($delivery_type == 'pickup') {
							$vars['params']['delivery_address'] = '';
						}
						
						if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') {
							if ($delivery_type == 'pickup') {
								$vars['params']['[customer_name]'] = $generate_voucher_1['first_name']. ' '. $generate_voucher_1['last_name'];
								print_r($vars);
								$this->email_templating->config($vars);
								$this->email_templating->send();
							}
						} else {
							$this->email_templating->config($vars);
							$this->email_templating->send();
							//send email again for this brand
							switch ($brand_row['brand_id']) {
								case BRAND_PROD_ID_MACALLAN:
								case BRAND_PROD_ID_SNOW_LEOPARD:
								case BRAND_PROD_ID_LONDON_DRY:
									//$vars['to'] = 'sulina@magnum.com.sg';
									$this->email_templating->config($vars);
									$this->email_templating->send();
									break;
							}
						}
					}

					//copy
					$vars['to'] = 'allan.bernabe@gmail.com';
					$this->email_templating->config($vars);
					$this->email_templating->send();

					$vars['to'] = 'z@zzo-creative.com';
					$this->email_templating->config($vars);
					$this->email_templating->send();
				}
			}
		}
	}

	function send_email_purchaser() {






	}

	function mysql_uuid()
	{
        $sql = "select UUID() as uuid";
        $res = $this->db->query($sql);
        return $res->row()->uuid;
    }
	function vocher_expiration_day($add_day)
	{
		$date = date('Y-m-d');
		$sql = "SELECT  ADDDATE('" . $date . "','$add_day DAY')AS new_date";
		$res = $this->db->query($sql);
		$rec = $res->row();
		return $rec->new_date;
	}

}