<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BuyPoints extends CI_Controller {
    var $PAYPAL_ENVIRONMENT='live';
    function __construct()
    {
        parent::__Construct();
        $this->load->library('user_login_check');
        $this->load->model('cmspages_m');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->load->model('payment_m');
        $this->load->model('user_account_m');        
    }
    function index()
    {
        $data['resPoints'] = $this->payment_m->selectPoints();
        $data['session_payment_visisted_id'] =  $this->payment_m->create_payment_session_visited($this->session->userdata('user_id'),$payment_method_id=1);
        $this->load->view('buy_points_v',$data);    
    }
    function checkPoints(){
        $i=0;
        $point_balance=0;
        $status='error';
        if($_POST && isset($_POST['session_id']) && isset($_POST['token']) && isset($_POST['amount'])) 
        {
            $session_id=$this->input->post('session_id');
            $token=$this->input->post('token');            
            $amount=$this->input->post('amount');                        
            do{            
                $i++;
                usleep(250000); //.25 secodes                                
                //check payment if received.
                $res=$this->payment_m->check_payment_if_received($this->session->userdata('user_id'),$session_id,$token,$amount);
                if($res->num_rows>0){
                    //$row=$$res->row();
                    $res_header = json_decode($this->user_account_m->user_summary($this->session->userdata('user_id')));
                    $point_balance=$res_header->point_balance;
                    $status='success';
                    break;
                }
                if($i==40)
                {
                    break;
                }
                
            }
            while(true);
        }
        die("{status:'$status',points:'$point_balance'}");
    }
    function addPoints()
    {
            
            $point = $this->input->post('_value');
            $amount = $this->input->post('_cost');
            $session_payment_visisted_id = $this->input->post('session_payment_visisted_id');
            $res = $this->payment_m->checkout_payment_session($this->session->userdata('user_id'),$point,$amount,$session_payment_visisted_id);
                        
                        if($res === FALSE) {
                            echo json_encode(array('status' => 'error'));
                            return;
                        }
                        
            $insertedID = $res['session_payment_id'];
            $cost = $res['session_amount'];
            die("{status:'success',inserted_id:'".($insertedID)."',cost:".($cost)."}");
    }
    function express_checkout()
    {   
        if($_POST && isset($_POST['custom']) && isset($_POST['amount']))
        {        
            $session_id=$this->input->post('custom');
            $amount =$this->input->post('amount');
            $session_payment_visisted_id = $session_id;
            $user_id = $this->input->post('user_id');

            $returnURL = base_url('buyPoints/get_checkout');
            $cancelURL = base_url('buyPoints/cancel_checkout');

            if ( empty($user_id) ) {
                $user_id = $this->session->userdata('user_id');
                //$returnURL = base_url(MOBILE_M.'/buyPoints/get_checkout/'.$user_id.'/1?_points=glomp!buypoints');
            } else {
                //mobile native app bypass
                //To buypass mobile native app buying points
                $buy_points_by_pass = $this->input->get('_points'); //hardcode value glomp!buypoints

                if ( ! empty ($buy_points_by_pass) ) {
                    if ($buy_points_by_pass == 'glomp!buypoints') {
                        $returnURL = base_url(MOBILE_M.'/buyPoints/get_checkout/'.$user_id.'/1?_points='.$buy_points_by_pass);
                        $cancelURL = base_url(MOBILE_M.'/buyPoints/cancel_checkout?_points='.$buy_points_by_pass);
                    }
                }
            }
           // $res = $this->payment_m->checkout_payment_session($this->session->userdata('user_id'),$point,$amount,$session_payment_visisted_id);
            
            
            
            $environment=$this->PAYPAL_ENVIRONMENT;
            // Set request-specific fields.
            $paymentAmount = urlencode($amount);
            $currencyID = urlencode('USD');                         // or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
            $paymentType = urlencode('Sale');               // or 'Sale' or 'Order'

            /*$returnURL = urlencode(base_url('buyPoints/get_checkout'));
            $cancelURL = urlencode(base_url('buyPoints/cancel_checkout'));*/        

            $returnURL = urlencode(($returnURL));
            $cancelURL = urlencode(($cancelURL));
            
            $nvpStr="";
            $nvpStr .="&RETURNURL=$returnURL";
            $nvpStr .="&CANCELURL=$cancelURL";
            $nvpStr .="&PAYMENTREQUEST_0_PAYMENTACTION=$paymentType";
            $nvpStr .="&PAYMENTREQUEST_0_AMT=$amount";
            $nvpStr .="&PAYMENTREQUEST_0_CURRENCYCODE=$currencyID";
            
            $nvpStr .="&L_PAYMENTREQUEST_0_NAME0=Purchase glomp!point";
            $nvpStr .="&L_PAYMENTREQUEST_0_QTY0=1";
            $nvpStr .="&L_PAYMENTREQUEST_0_AMT0=$amount";        
            
            $nvpStr .="&LANDINGPAGE=Billing";
            $nvpStr .="&BRANDNAME=glomp!";
            $nvpStr .="&PAYFLOWCOLOR=DEE6EB";
            $nvpStr .="&SOLUTIONTYPE=Sole";  
            $nvpStr .="&REQCONFIRMSHIPPING=0";
            $nvpStr .="&NOSHIPPING=1";
            $nvpStr .="&ADDROVERRIDE=0";
            $nvpStr .="&CARTBORDERCOLOR=DEE6EB";
            $nvpStr .="&LOGOIMG=".urlencode("http://glomp.it/assets/images/logo_small_new.png!");        
            
            // Execute the API operation; see the PPHttpPost function above.
            $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $nvpStr);

            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                // Redirect to paypal.com.
                $token = urldecode($httpParsedResponseAr["TOKEN"]);
                $this->payment_m->update_session_token($user_id,$session_id,$token);
                
                
                $payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
                if("sandbox" === $environment || "beta-sandbox" === $environment) {
                    $payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&token=$token";                
                }
                header("Location: $payPalURL");
                exit;
            } else  {
                //exit('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
                $data='';
                $this->load->view('buy_points_express_checkout_failed_v',$data);            
            }
        }
        else
        {
            $data['error_404'] =  $this->lang->line('page_404_error');
            $this->load->view('404_v',$data);   
        }   
    }
    function cancel_checkout(){
        if(!isset($_GET['token'])) {
            $data['error_404'] =  $this->lang->line('page_404_error');
            $this->load->view('404_v',$data);   
        }
        else
        {
            $token = urlencode(htmlspecialchars($_GET['token']));
            $data='';
            $data['session_id']='sample';
            $data['token']='token';
            $data['amount']='amount';
            $data['points']='points';            
            $retry=$this->input->get('retry');            
            $data['retry']=$this->input->get('retry');
            
            $this->load->view('buy_points_cancelled_v',$data);
            //$this->load->view('buy_points_success_v',$data);
        }
        
    }
    function get_checkout()
    {
        if(!isset($_GET['token'])) {
            $data['error_404'] =  $this->lang->line('page_404_error');
            return $this->load->view('404_v',$data);    
        }

        // Set request-specific fields.
        $token = urlencode(htmlspecialchars($_GET['token']));
        // Add request-specific fields to the request string.
        $nvpStr = "&TOKEN=$token";
        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $nvpStr);
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            // Extract the response details.
            //print_r($httpParsedResponseAr, true); 
            //exit('get_checkout success: ' . print_r($httpParsedResponseAr, true));
            $session_id_data =$this->payment_m->get_session_id($this->session->userdata('user_id'),$token);
            if($session_id_data->num_rows>0){
                $session_id_row=$session_id_data->row();
                $session_id=$session_id_row->sesion_payment_id;
                $payerID = $httpParsedResponseAr['PAYERID'];
                $amount=$session_id_row->session_amount;
                $points=$session_id_row->sesion_buying_point;

                        
                $this->do_checkout($payerID,$token,$session_id,$amount,$points);
            }
            
            //exit('Get Express Checkout Details Completed Successfully: '.print_r($httpParsedResponseAr, true));
        } else  {
            $data='';
            $this->load->view('buy_points_get_checkout_failed_v',$data);            
            //exit('GetExpressCheckoutDetails failed: ' . print_r($httpParsedResponseAr, true));
        }
    }
    private function do_checkout($payerID,$token,$session_id,$amount,$points){        

        $paymentType = urlencode("SALE");           // or 'Sale' or 'Order'
        $paymentAmount = urlencode($amount);
        $currencyID = urlencode("USD");                     // or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

        // Add request-specific fields to the request string.
        $nvpStr = "&TOKEN=$token&PAYERID=$payerID&PAYMENTACTION=$paymentType&AMT=$paymentAmount&CURRENCYCODE=$currencyID";
        $nvpStr .="&custom=$session_id";
        
        

        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpStr);

        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            //echo "TRANSACTIONID".$httpParsedResponseAr["TRANSACTIONID"];
        
            //exit('Express Checkout Payment Completed Successfully: '.print_r($httpParsedResponseAr, true));            
            $data['session_id']=$session_id;
            $data['token']=$token;
            $data['amount']=$amount;
            $data['points']=$points;
            $data['retry']=$this->input->get('retry');
            $this->load->view('buy_points_success_v',$data);
        } else  {
            $data='';
            $this->load->view('buy_points_do_checkout_failed_v',$data);            
        }
    
    }
    
    function PPHttpPost($methodName_, $nvpStr_) {
        
        $environment=$this->PAYPAL_ENVIRONMENT;
            // Set up your API credentials, PayPal end point, and API version.
        $API_UserName = urlencode('paid_api1.glomp.it');
        $API_Password = urlencode('ACGRWSM353ZZQ638');
        $API_Signature = urlencode('AX6acVWmmDx.b8w1sO4JgRtSNme1AXwFnp2Nf2hCgdKCaU.FuANfLj9t');
        
        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if("sandbox" === $environment || "beta-sandbox" === $environment)
        {
            $API_UserName = urlencode('allan.bernabe-facilitator_api1.gmail.com');
            $API_Password = urlencode('TSTCNYANYT9TXTF8');
            $API_Signature = urlencode('AiGm97m9-5fDHpUVCtqv7lJHEDEbAxOgZCs5DXZ3DrlSkqUJtbci.K20');  
            $API_Endpoint = "https://api-3t.sandbox.paypal.com/nvp"; //old endpoint
            // $API_Endpoint = "https://SHA2-test-api-3t.sandbox.paypal.com/nvp";
            // $API_Endpoint = "https://api.sandbox.paypal.com/nvp"; 

        }
        $version = urlencode('93.0');

        // // Set the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_HEADER, $API_Endpoint);
        // curl_setopt($ch, CURLOPT_VERBOSE, 1);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSLVERSION, 1);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // // Set the API operation, version, and API signature in the request.
        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

        // // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        // // Get response from the server.
        $httpResponse = curl_exec($ch);


        if(!$httpResponse) {            
            //exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
            redirect('/buyPoints/http_post_failed');            
            exit();
        }
        // curl_close($curl);
        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);
        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if(sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }
        if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
           //exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
            redirect('/buyPoints/http_post_failed');            
            exit();
        }
        return $httpParsedResponseAr;
    }
    function http_post_failed()
    {
        $data='';
        $this->load->view('buy_points_http_post_failed_v',$data);
    }
    

}//eoc
