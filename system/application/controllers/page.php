<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    private $site_sender_email = 'support@glomp.it'; //SITE_DEFAULT_SENDER_EMAIL;
    private $site_sender_name = SITE_DEFAULT_SENDER_NAME;
    private $site_support_email = SITE_DEFAULT_SUPPORT_EMAIL;
    private $site_support_name = SITE_DEFAULT_SUPPORT_NAME;

    function __construct() {
        parent::__Construct();
        $this->load->model('cmspages_m');
        $this->load->model('send_email_m');
        $this->load->model('users_m');
    }

    function index($page_id = 0) {
        $user = $this->users_m->get_record(array('select' => 'user_lang_id', 'where' => array('user_id' => $this->session->userdata('user_id'))));
        
        $res_privacy = $this->cmspages_m->selectCmspageByID($page_id, $user['user_lang_id']);

        //$data['res_pages'] = $this->cmspages_m->selectCmsPageNotByID($page_id,DEFAULT_LANG_ID);

        if ($res_privacy->num_rows() > 0) {
            $data['rec_privacy'] = $res_privacy->row();
            $this->load->view('page_v', $data);
        } else {
            redirect(base_url());
            exit();
        }
    }

    function support() {
        $page_title = 'glomp: Support';
        if ($this->session->userdata('user_id')) {
            $data['res_pages'] = $this->cmspages_m->selectCmsPageNotByID(0, DEFAULT_LANG_ID);
            if (isset($_POST['sendMessage'])) {
                $this->form_validation->set_rules('subject', 'Subject', 'trim|required|');
                $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

                if ($this->form_validation->run() == TRUE) {
                    $this->load->model('users_m');
                    $user_info = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
                    $rec_user_info = $user_info->row();

                    $subject = "Support Email: " . $this->input->post('subject');
                    $message_ = $this->input->post('message');
                    //send email
                    $from_email = $rec_user_info->user_email;
                    $from_name = $rec_user_info->user_name;
                    $to_email = $this->site_sender_email;
                    $to_name = $this->site_sender_name;

                    $to_email = $this->site_support_email;
                    $to_name = $this->site_support_name;


                    $message = "<p>Dear Admin ,</p>";
                    $message .= "<br/><br/>Support email from glomp.Sender Details<br/>
								<p>Sender Name:$from_name <br/>
									Sender Email: $from_email
								</p>
								<strong>Details Message:</strong>
								";
                    $message .= $message_;
                    $message .= '<br/><br/>
								Cheers.
								<br/>
								The glomp! team.
								';
                    $this->send_email_m->sendEmail($from_email, $from_name, $to_email, $to_name, $subject, $message);

                    $data['success_msg'] = $this->lang->line('support_msg_sent_successfully');
                }
            }
            $this->load->view('support_v', $data);
        } else {
            redirect('landing');
        }
    }

}

//eoc
