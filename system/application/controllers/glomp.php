<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Glomp extends CI_Controller {
	function __construct()
	{
		parent::__Construct();
		$this->load->library('user_login_check');
		$this->load->model('cmspages_m');
		$this->load->model('users_m');
		$this->load->model('user_account_m');
		$this->load->model('invites_m');
		$this->load->model('product_m');	
		$this->load->model('voucher_m');	
	}
	function index()
	{
		$data['resPoints'] ="";
		$user_id = $this->session->userdata('user_id');
		$data['my_glomp_details'] = $this->users_m->my_glomp_details($user_id);
		$data['user_id'] = $user_id;
		$this->load->view('glomp_v',$data);	
	}
        function acceptVoucher() {
            $where = 'voucher_status NOT IN ("Expired", "Redeemed") 
                AND voucher_id = "'.$this->input->post('voucher_id').'"';
            $voucher = $this->voucher_m->get_record(array(
                'where' => $where
            ));
            
            //delete memcache
            $user_id = $this->session->userdata('user_id');
            $params = array(               
                'specific_names' 
                    => array(
                        'my_glomp_details_'.$user_id.'_0_0',                        
                    )    
            );
            delete_cache($params);
            //delete memcache
            
            
            if ($voucher !== FALSE) {
                $time = date('Y-m-d H:i:s');
                
                $where = 'voucher_id = "'.$this->input->post('voucher_id').'"';
                $type = ($voucher['voucher_status'] == 'Assigned') ? 'Assignable' : $voucher['voucher_status'];
                $data = array(
                    'voucher_type' => $type,
                    'voucher_updated' => $time,
                    'voucher_expiry_date' => $this->user_account_m->vocher_expiration_day($voucher['voucher_expiry_day']),
                );
                
                $id = $this->voucher_m->_update($where, $data);
                
                $where = 'invite_voucher_id = "'.$this->input->post('voucher_id').'"';
                $invite = $this->invites_m->get_record(array(
                    'where' => $where
                ));
                
                if ($invite !== FALSE) {
                    $data = array(
                        'invite_responded_date' => $time,
                        'invite_responded' => 'Y',
                    );
                
                    $id = $this->invites_m->_update($where, $data);                    
                }
                
                if ($id) {
                    return redirect(site_url('glomp/glomp'));
                }
            }
            
            return redirect(site_url('glomp/glomp'));
        }
}//eoc
