<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {
	
    
	function image($banner_id=1){						
        //$banner_id=1; 
        if($banner_id=='all' || (is_numeric($banner_id) &&  $banner_id>=1 && $banner_id<=13 ))
        {
            $image_url[1]="http://glomp.it/assets/images/banners/glomp_Img_LO_1a.png";
            $image_url[2]="http://glomp.it/assets/images/banners/glomp_Img_LO_1b.png";
            $image_url[3]="http://glomp.it/assets/images/banners/glomp_Img_LT_1.png";
            $image_url[4]="http://glomp.it/assets/images/banners/glomp_Img_LT_1a.png";
            $image_url[5]="http://glomp.it/assets/images/banners/glomp_Img_LT_1b.png";
            $image_url[6]="http://glomp.it/assets/images/banners/glomp_Img_LT_2a.png";
            $image_url[7]="http://glomp.it/assets/images/banners/glomp_Img_LT_2b.png";
            $image_url[8]="http://glomp.it/assets/images/banners/glomp_Img_LT_3a.png";
            $image_url[9]="http://glomp.it/assets/images/banners/glomp_Img_LT_3b.png";
            $image_url[10]="http://glomp.it/assets/images/banners/glomp_Img_LT_4a.png";
            $image_url[11]="http://glomp.it/assets/images/banners/glomp_Img_LT_4b.png";
            $image_url[12]="http://glomp.it/assets/images/banners/glomp_Img_LT_4c.png";
            $image_url[13]="http://glomp.it/assets/images/banners/glomp_Img_LT_4d.png";

            $data['banner_id'] = $banner_id;
            $data['image_url'] =$image_url;
            $this->load->view('banner_v',$data);
        }
	}
	
    
}