<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

	private $site_sender_email = SITE_DEFAULT_SENDER_EMAIL;
	private $site_sender_name = SITE_DEFAULT_SENDER_NAME;
	
	function __construct()
	{
		parent::__Construct();
		$this->load->model('login_m');
		$this->load->model('regions_m');
		$this->load->model('cmspages_m');
		$this->load->model('send_email_m');	
		$this->load->model('users_m');	
		$this->load->library('email_templating');
        $this->load->library('custom_func');
		$this->load->model('campaign_m');
		$this->load->model('product_m');
	}
    function hsbc()
    {
        redirect('');
    }
    function clear_cache() {
        clear_cached();
        echo 'Success!';
    }
        
    function test_email()
    {
        $data['content'] = 'test';
        $this->load->view('adminControl/email/_layouts/standard_responsive',$data);
    }
    function profile_img($profil_id=0){
        if($profil_id>0)
        {
            $temp=$this->users_m->get_image($profil_id);
            if($temp->num_rows()>0)
            {
                $row= $temp->row();
                $profile_pic = base_url($this->custom_func->profile_pic($row->user_profile_pic,$row->user_gender));
                return redirect($profile_pic);
            }
        }
    }
    function viewEmail($view_id = 0) {
        $this->load->model('email_logs_m');
        
        $rec = $this->email_logs_m->get_record(array(
            'where' => array('email_log_id' => $view_id)
        ));
        
        echo $rec['email_body'];
    } 

    function verify($code) {
        $user_id=0;
        if(isset($_GET['uid']) && is_numeric($_GET['uid'])) 
            $user_id=$_GET['uid'];
            
        //echo $user_id.'-'.$code; 
        if ($this->login_m->_verify($code,$user_id)  === TRUE){
            //return show_404();

            $this->session->set_flashdata('verified', 'Email has been verified. Please login to your account');
            return redirect(site_url());
        }
        else
        {
            return show_404();
        }

    }
    public function resendVerification(){
		$to_email='';
		if(isset($_GET['email']))			
				$to_email=$_GET['email'];

				
		$data['user_name'] =$to_email;
		$data['Page_title'] = $this->lang->line('landing_page_title');	
		$this->load->helper('email');
		if (valid_email($to_email))
		{
			$tempResult=$this->login_m->check_if_registered_email($to_email);					
			if($tempResult->num_rows() > 0)
			{	
				$resData=$tempResult->row();
				$to_name =$resData->user_fname.' '.$resData->user_lname;
				$link = site_url('landing/verify/'.$resData->user_hash_key.'?uid='.$resData->user_id);
				$click_here ="<a href='$link'>click</a>";
				$vars = array(
					'template_name' => 'new_signup',
					'lang_id' => 1,
					'to' => $to_email,
					'params' => array(
						'[link]' => $click_here
					)
				);
				$this->email_templating->config($vars);
				$this->email_templating->send();
				$this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
				redirect('landing/');
			
			}
			else{
				$data['error_msg'] = "Invalid E-mail.";
			}			
		}
		else{
			$data['error_msg'] = "Invalid E-mail.";	
		}
		$this->load->view('landing_v',$data, FALSE);
	}    
	public function index()
	{				
        if (isset($_GET['deactivated']))
        {  
            $this->session->set_flashdata('deactivated', 'Your account has been deactivated.');
            redirect(base_url());
        }
    
		//redirect to mobile
		$device = $user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone'): 'Computer');
		if($device!='Computer')
		{
			redirect('m/');	
		}
		#if user loged in redirect to dashboard
		if($this->session->userdata('is_user_logged_in')){
                    $verified = $this->session->flashdata('verified');
                    if (! empty($verified)) {
                        //Set flash data again
                        $this->session->set_flashdata('verified', 'Email Verified!');
                    }
			redirect('user/dashboard');
			exit();
		}
		
		
		$data['user_name'] =$this->input->post('user_name');
		$data['Page_title'] = $this->lang->line('landing_page_title');		
		if(isset($_POST['user_login']))
		{
				$this->form_validation->set_rules('user_name', 'E-mail', 'trim|required|valid_email|xss_clean');
				$this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');
			
			if($this->form_validation->run() == TRUE)
			{
				$user_name = $this->input->post('user_name');
				$user_password = $this->input->post('user_password');
				$login_result = $this->login_m->loginValidation($user_name,$user_password);
                $login = json_decode($login_result);

				if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
				{
					//print_r($login);
					if($this->input->post('referrer') !="")
					{
						redirect($this->input->post('referrer'));
						exit();					
					}
					
					redirect('user/dashboard');
					exit();					
				}
                else if(isset($login->deactivated_status) && $login->deactivated_status==1 && $login->user_status == 'Pending')
                {
                    /*if deactivated*/
                    $update_data = array(
                        'user_status'               => 'Active',
                        'deactivated_status'        => 0
                    );
                    $where = array(
                            'user_id' 		=>$login->user_id
                            );
                    $this->db->update('gl_user', $update_data, $where);
                    
                    $login_result = $this->login_m->loginValidation($user_name,$user_password);
                    $login = json_decode($login_result);
                    
                    $this->session->set_userdata('reactivated', 'true');
                    redirect('user/dashboard/');
                    exit();
                }
				else if(isset($login->user_email_verified) && $login->user_email_verified== 'N')
				{
					$link = base_url('landing/resendVerification/?email='.$user_name);
					$click_here ="<a href='$link'>Click here </a> ";
					$data['error_msg'] = "Please check your welcome email to activate your account. ".$click_here." to resend the verification.";	
				}
				else
				{						
					$data['error_msg'] = "Invalid E-mail or password";	
				}
			}
		}
		else if(isset($_POST['fb_login']) )
		{/*	
		 foreach ($_POST as $key => $value) {      
			echo "<div>";
			echo $key;
			echo ":";        
			echo $value;        
			echo "</div>";
				
		}*/
				$data['user_name'] =$this->input->post('user_name');
				$user_name=$this->input->post('user_name');
				$user_fb_id="";
				$user_email="";
				if( isset($_POST['id'])  && isset($_POST['email']) ){
					$user_fb_id=$_POST['id'];		
					$user_email=$_POST['email'];				
				}
				
				if( $this->login_m->check_if_fb_user_is_registered($user_fb_id) == 0)
				{
					$tempResult=$this->login_m->check_if_fb_user_is_registered_email($user_email);
					
					if($tempResult->num_rows() == 0)
					{   
                        /* @todo remove sessions */
                        $this->session->set_userdata('temp_user_fb_id',$_POST['id']);
						$this->session->set_userdata('temp_fname',$_POST['first_name']);
						$this->session->set_userdata('temp_mname',$_POST['middle_name']);
						$this->session->set_userdata('temp_lname',$_POST['last_name']);
						
						$this->session->set_userdata('temp_gender',ucfirst($_POST['gender']));
						$this->session->set_userdata('temp_email',$_POST['email']);					
						
						$dob=strtotime($_POST['birthday']);
						$this->session->set_userdata('temp_day',date('d',$dob));
						$this->session->set_userdata('temp_months',date('m',$dob));
						$this->session->set_userdata('temp_year',date('Y',$dob));
						redirect('landing/register/facebook?fb_user_id='.$this->input->post('id').'&fb_email='.$this->input->post('email').'&fb_fname='.$this->input->post('first_name').'&fb_mname='.$this->input->post('middle_name').'&fb_lname='.$this->input->post('last_name').'&fb_gender='.$this->input->post('gender').'&fb_birthdate='.$dob);
					}
					else
					{
						$resData=$tempResult->row();
						if($user_fb_id==$resData->user_fb_id)
						{
							$login_result = $this->login_m->login_using_fb_id($user_fb_id);			
							$login = json_decode($login_result);
							if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
							{
								if($this->input->post('referrer') !="")
								{
									redirect($this->input->post('referrer'));
									exit();					
								}
								redirect('user/dashboard');
								exit();					
							}
							else if(isset($login->user_email_verified) && $login->user_email_verified== 'N')
							{
								$link = base_url('landing/resendVerification/?email='.$user_email);
								$click_here ="<a href='$link'>Click here</a> ";
								$data['error_msg'] = "Please check your welcome email to activate your account. ".$click_here." to resend the verification.";	
							}
							else
							{						
								$data['error_msg'] = "Invalid E-mail or password";	
							}						
						}
						else{
							//facebook email has a glomp account but not connected with facebook
							//$data['error_msg'] = "Invalid E-mail or passworasdasdasdd";	
							//update the user account and connect them with facebook
							$this->login_m->user_fb_login_update_account($resData->user_id);
							$login_result = $this->login_m->login_using_fb_id($user_fb_id);			
							$login = json_decode($login_result);
							if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
							{
								redirect('user/dashboard');
								exit();					
							}
							else if(isset($login->user_email_verified) && $login->user_email_verified== 'N')
							{
								$link = base_url('landing/resendVerification/?email='.$user_email);
								$click_here ="<a href='$link'>Click here</a> ";
								$data['error_msg'] = "Please check your welcome email to activate your account. ".$click_here." to resend the verification.";	
							}
							else
							{						
								$data['error_msg'] = "Invalid E-mail or password";	
							}
						}
						
					}					
				}
                                
				else{
					//get username then login
					$login_result = $this->login_m->login_using_fb_id($user_fb_id);			
					$login = json_decode($login_result);
                    
                    if($login->user_status=='Active')
                    {
                        if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
                        {
							if($this->input->post('referrer') !="")
							{
								redirect($this->input->post('referrer'));
								exit();					
							}
                            redirect('user/dashboard');
                            exit();					
                        }
                        else if(isset($login->user_email_verified) && $login->user_email_verified== 'N')
                        {
                            $link = base_url('landing/resendVerification/?email='.$user_email);
                            $click_here ="<a href='$link'>Click here</a> ";
                            $data['error_msg'] = "Please check your welcome email to activate your account. ".$click_here." to resend the verification.";	
                        }
                        else
                        {						
                            $data['error_msg'] = "Invalid E-mail or password";	
                        }
                    
                    }
                    else if($login->user_status=='Pending')
                    {
                        /*check if deactivated*/
                        $q = $this->db->get_where( 'gl_user', "user_id = '".$login->user_id."' AND deactivated_status=1");
                        if( $q->num_rows()  > 0)
                        {
                            //return $q->row();
                            $update_data = array(
                                'user_status'               => 'Active',
                                'deactivated_status'        => 0
                            );
                            $where = array(
                                    'user_id' 		=>$login->user_id
                                    );
                            $this->db->update('gl_user', $update_data, $where);
                            
                            $login_result = $this->login_m->login_using_fb_id($user_fb_id);			
                            $login = json_decode($login_result);
                            
                            $this->session->set_userdata('reactivated', 'true');
                            redirect('user/dashboard/');
                            exit();
                            
                        }
                        //deactivated_status
                        /*check if deactivated*/
                        
                        
                        /* @todo remove sessions */
                        $this->session->set_userdata('temp_user_fb_id',$_POST['id']);
						$this->session->set_userdata('temp_fname',$_POST['first_name']);
						$this->session->set_userdata('temp_mname',$_POST['middle_name']);
						$this->session->set_userdata('temp_lname',$_POST['last_name']);
						
						$this->session->set_userdata('temp_gender',ucfirst($_POST['gender']));
						$this->session->set_userdata('temp_email',$_POST['email']);					
						
						$dob=strtotime($_POST['birthday']);
						$this->session->set_userdata('temp_day',date('d',$dob));
						$this->session->set_userdata('temp_months',date('m',$dob));
						$this->session->set_userdata('temp_year',date('Y',$dob));
						redirect('landing/register/facebook?fb_user_id='.$this->input->post('id').'&fb_email='.$this->input->post('email').'&fb_fname='.$this->input->post('first_name').'&fb_mname='.$this->input->post('middle_name').'&fb_lname='.$this->input->post('last_name').'&fb_gender='.$this->input->post('gender').'&fb_birthdate='.$dob);
                    
                    }
					
				}
			
		}
                
                //When posted to URL
                //Please follow: controller: ajax_post; func: checkIntegratedAppConnected
                if(isset($_POST['linkedin_login'])) $data = array_merge($data, $_POST);
                
		$data['is_voucher_okay']=false;
		$data['voucher_data']="";
		if(isset($_GET['voucher'])){
			$voucher=$_GET['voucher'];
			$voucherCheck=$this->login_m->checkIfThisVoucherExist($voucher);
			if($voucherCheck->num_rows()>0){
				$tempData=$voucherCheck->row();
				$data['is_voucher_okay']=true;
				$from_user=$this->users_m->user_info_by_id($tempData->voucher_purchaser_user_id);
				$from_user=$from_user->row();
				$from_user=$from_user->user_fname;
				$data['voucher_from']=$from_user;
			}			
		}
		$this->load->view('landing_v',$data, TRUE, TRUE);
	}
	
	
	public function register()
	{
		$this->load->helper('url');
                $this->load->library('user_agent');
                //This is the default;
                $data['dob_display'] = 'dob_display_w_year';
                
		if(isset($_POST['Register']))
		{			
			
			$user_id = ($this->session->userdata('id_user'))?$this->session->userdata('id_user'):0;				
			
			$this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_fname_check');
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean|callback_lname_check');
			$this->form_validation->set_rules('day', 'D.O.B', 'trim|required|callback_dob_check');
			$this->form_validation->set_rules('months', 'Months', 'trim|required');
			$this->form_validation->set_rules('year', 'Year', 'trim|required');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
			$this->form_validation->set_rules('location_text', 'Location', 'trim|required|xss_clean');
			$by_pass_email_check = FALSE;
            if($user_id==0)
			{
				$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_check|xss_clean');
				$email = $this->input->post('email');
				$temp=$this->login_m->checkThisEmailIfExistsAndPending($email);
				if($temp->num_rows()>0){
					$tempData=$temp->row();
					$user_id=$tempData->user_id;
					$user_hash_key=$tempData->user_hash_key;                    
				}
                else
                {
                    $field="";
    
                    if (isset($_POST['user_linkedin_id'])) {
                        $field = 'user_linkedin_id';
                        $value = $_POST['user_linkedin_id'];
                    }
                    
                    if (isset($_POST['fb_user_id'])) {
                        $field = 'user_fb_id';
                        $value = $_POST['fb_user_id'];
                    }
                    if($field!="")
                    {
                    
                        $temp = $this->users_m->get_record(array(
                            'where' => array(
                                $field => $value
                            ),
                            'resource_id' => TRUE
                        ));
                                        
                        if( is_bool($temp) == FALSE){
                            $tempData = $temp->row();
                            $user_id=$tempData->user_id;
                            $user_hash_key=$tempData->user_hash_key;
                            $by_pass_email_check = TRUE;
                        }
                    }
                }                   
			}
			else
			{
				$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_email_prefill_check|xss_clean');
			}
			$this->form_validation->set_rules('pword', 'Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
			$this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');
			$this->form_validation->set_rules('agree', 'Terms & Condtion', 'trim|required');
			
			//check photo
                        $data['dob_display'] = $this->input->post('dob_display', TRUE);
			if($this->form_validation->run() == TRUE)
			{
                $noPhoto=false;
                $locationOK=false;
                
                $location_id=$this->input->post('location');
                $location_text=$this->input->post('location_text');
                if(is_numeric($location_id))
                {
                    $locationOK=true;
                }
                else
                {                    
                    $result= $this->regions_m->getCountryID_Check($location_text);            
                    if($result->num_rows()>0){
                        $locationOK=true;
                        $res=$result->row();
                        $location_id=$res->region_id;
                    }
                }
                
                
				if($_POST['profile_pic_latest']!="")
                {
                    $noPhoto=true;
                }
                else if(isset($_POST['fb_user_photo']) && $_POST['fb_user_photo']!='' )
                {
					$noPhoto=true;
				}
                
                
                if(!$locationOK)
                {   
                    $data['error_msg'] = "Invalid location.";
                }
				else if(!$noPhoto)
                {
					$data['error_msg'] = "Profile photo required.";
				}
				else if(isset($_POST['token'])&& $_POST['token'] == $this->session->userdata('token'))
				{
					$email = $this->input->post('email');
				
					if($this->login_m->ckeckPrefillEmail($user_id)==1 || $by_pass_email_check)/// if email is already there in database with pending state//
					{					
						//now signup user
						$this->login_m->user_signup_update($user_id);
						
						
						/// promo code
						$promo_code = $this->input->post('promo_code');
						if($promo_code!="")
							$this->users_m->promo_code_save_to_user($promo_code,$user_id);
						/// promo code
						
						/*upload photo start*/
						if($_POST['profile_pic_latest']!="")
							{
								$user_photo=$this->input->post('profile_pic_latest');						
								if(file_exists("custom/uploads/users/temp/".$user_photo))
								{
									copy ("custom/uploads/users/temp/".$user_photo,"custom/uploads/users/thumb/".$user_photo);
									unlink("custom/uploads/users/temp/".$user_photo);
								}
								$this->login_m->updatePhoto($user_photo,$user_id);						 
								
								
								$user_photo_orig=$this->input->post('profile_pic_orig');
								$this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
							}//user photo temp check
							else if(isset($_POST['fb_user_photo']) && $_POST['fb_user_photo']!='' )
							{
								$url =$_POST['fb_user_photo'];
								/* Extract the filename */					
								

								/* Extract the filename */
								$filename = substr($url, strrpos($url, '/') + 1);
								$safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($filename));
								$ext = strrchr($safe_filename, '.');
								$new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$filename[0];
								/* Save file wherever you want */				
								$user_photo=$new_photo_name.$ext;						
								
								//get data
								$localfile = 'custom/uploads/users/mytempfilename.ext';

								// Let's go cURLing...
								$ch = curl_init($url);
								$fp = fopen($localfile,'w');

								curl_setopt($ch, CURLOPT_FILE, $fp);
								curl_setopt($ch, CURLOPT_HEADER, 0);

								curl_exec($ch);
								curl_close($ch);
								fclose($fp);

								// Get the data into memory and delete the temp file
								$filedata = file_get_contents($localfile);
								unlink($localfile);
								//get data
								
								file_put_contents('custom/uploads/users/'.$new_photo_name.$ext, $filedata);
								
								$user_photo=$new_photo_name.$ext;
								
								$this->login_m->updatePhoto($user_photo,$user_id);
								$this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
								
								
								$user_photo_orig=$user_photo;
								$this->login_m->updatePhotoOrig($user_photo_orig,$user_id);
								
							}
						/******upload photo end***/
                        $user_hash_key='';
						$temp=$this->login_m->getUserHashKey($user_id);                        
                        $tempData=$temp->row();                            
                        $user_hash_key=$tempData->user_hash_key;
						#user is successfully created their account
						# send welcome email
						# and redirect to landing
						$to_email = $this->input->post('email');
						$to_name = $this->input->post('fname').' '.$this->input->post('lname');
						$link = site_url('landing/verify/'.$user_hash_key.'?uid='.$user_id);
						$click_here ="<a href='$link'>click</a>";
						
		//				new_signup
						$sign_up_subject = $this->lang->line('signup_email_subject');				 
						$sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>
			
										<p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please ".$click_here." to learn.</p>
										<br/>
										Cheers.
										<br/>
										The glomp! team.
										";
						//$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);
						
						
						$vars = array(
							'template_name' => 'new_signup',
							'lang_id' => 1,
							'to' => $to_email,
							'params' => array(
													'[link]' => $click_here
							)
						);
						
						$this->email_templating->config($vars);
						$this->email_templating->send();				
						

						//$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);
						
					
								//if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
								{
								
								
								
										$this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
										redirect('landing/');
										exit();
										
								}
								//else
								{
									
								//		$data['error_msg'] = "Unexpected error occured. Please try again";
								}
					}
					else
					{
										//now signup user
						$result = json_decode($this->login_m->user_signup());
						if($result->signup == 'success')
						{
							
							/// promo code
							$promo_code = $this->input->post('promo_code');
							if($promo_code!="")
								$this->users_m->promo_code_save_to_user($promo_code,$result->insert_id);							
							/// promo code
						
						
						/*upload photo start*/
							if($_POST['profile_pic_latest']!="")
							{
								$user_photo=$this->input->post('profile_pic_latest');
								if(file_exists("custom/uploads/users/temp/".$user_photo))
								{
									copy ("custom/uploads/users/temp/".$user_photo,"custom/uploads/users/thumb/".$user_photo);
									unlink("custom/uploads/users/temp/".$user_photo);
								}						
								$this->login_m->updatePhoto($user_photo,$result->insert_id);	
								
								$user_photo_orig=$this->input->post('profile_pic_orig');
								$this->login_m->updatePhotoOrig($user_photo_orig,$result->insert_id);
								
							}//user photo temp check				
							else if(isset($_POST['fb_user_photo']) && $_POST['fb_user_photo']!='' )
							{
							
                                $url =$_POST['fb_user_photo'];
								/* Extract the filename */

								/* Extract the filename */
								$filename = substr($url, strrpos($url, '/') + 1);
								$safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($filename));
								$ext = strrchr($safe_filename, '.');
								$new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$filename[0];
								/* Save file wherever you want */				
								$user_photo=$new_photo_name.$ext;						
								
								//get data
								$localfile = 'custom/uploads/users/mytempfilename.ext';

								// Let's go cURLing...
								$ch = curl_init($url);
								$fp = fopen($localfile,'w');

								curl_setopt($ch, CURLOPT_FILE, $fp);
								curl_setopt($ch, CURLOPT_HEADER, 0);

								curl_exec($ch);
                                                                //get mime type incase there is no file extension
                                                                $mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
								curl_close($ch);
								fclose($fp);

								// Get the data into memory and delete the temp file
								$filedata = file_get_contents($localfile);
								unlink($localfile);
                                                                
                                                                if (empty($ext)) {
                                                                    //get file extension base on mime type
                                                                    $ext = mime_to_file_extension($mime_type);
                                                                }
								//get data
								file_put_contents('custom/uploads/users/'.$new_photo_name.$ext, $filedata);
								
								$user_photo=$new_photo_name.$ext;
								
								$this->login_m->updatePhoto($user_photo,$result->insert_id);
								$this->resizeImg("custom/uploads/users/".$user_photo,"custom/uploads/users/thumb/".$user_photo,USER_PHOTO_THUMB_WIDTH,USER_PHOTO_THUMB_HEIGHT);
								
								$user_photo_orig=$user_photo;
								$this->login_m->updatePhotoOrig($user_photo_orig,$result->insert_id);
							}
						/******upload photo end***/
										$user_hash_key = $result->user_hash_key;
										
						#user is successfully created their account
						# send welcome email
						# automatically make login and redirect to user/dashboard
						$to_email = $this->input->post('email');
						$to_name = $this->input->post('fname').' '.$this->input->post('lname');
						$link = site_url('landing/verify/'.$user_hash_key.'?uid='.$result->insert_id);
						$click_here ="<a href='$link'>click</a>";
						
		//				new_signup
						$sign_up_subject = $this->lang->line('signup_email_subject');				 
						$sign_up_message = "<p>You're about to start sharing and receiving treats with friends on glomp! Treat (we say glomp!) someone today!</p> <br/>
			
										<p>If you haven't already received a treat (we say you've been glomp!ed), it won't be long we're sure. Before you get started, it's important for you to learn how to redeem treats, so please ".$click_here." to learn.</p>
										<br/>
										Cheers.
										<br/>
										The glomp! team.
										";
						//$this->send_email_m->sendEmail($this->site_sender_email,$this->site_sender_name,$to_email,$to_name,$sign_up_subject,$sign_up_message);
						
						
						$vars = array(
							'template_name' => 'new_signup',
							'lang_id' => 1,
							'to' => $to_email,
							'params' => array(
													'[link]' => $click_here
							)
						);
						
						$this->email_templating->config($vars);
						$this->email_templating->send();
						
						//$user_password = $this->input->post('pword');
						//$login_result = $this->login_m->loginValidation($to_email,$user_password);
						//$login = json_decode($login_result);
						
						//if(isset($login->user_id)&& $login->user_id>0 && $login->is_user_logged_in == TRUE)
						{
								$this->session->set_flashdata('verified', 'Please check your welcome email to activate your account.');
								redirect('landing/');
								exit();						
						}
						/*else
						{
							
								$data['error_msg'] = "Unexpected error occured. Please try again";
							}*/
						
						}//signup success
						else
						{
							$data['error_msg'] = "Unexpected error occured. Please try again";
							
						
						}//signup else failed
					}/// end of if($this->login_m->ckeckPrefillEmail($email)==1)
			
				}//token check
			
			}//validation check
			/* comment ko muna.
			else
			{
				$email = $this->input->post('email');
				if($email!="" && $this->email_check($email)==false){
					$tempErr=$this->lang->line('email_already_exist_try_new_one');
                    //$data['error_msg']=$tempErr;
					//$this->session->set_flashdata('verified', $tempErr);
					//redirect('landing/');
					//exit();
				
				}
				
			}
            */
		}//post check
		$token = $this->login_m->generate_hash(50);
		$data['token'] =  $token;
		$this->session->set_userdata('token',$token);		
        $this->load->view('register_v',$data, FALSE, TRUE);
	}

    /**
     *
     * @todo remove sessions
     */
function preFill($hashKey)
	{
            
		$result = $this->login_m->preFillSelect($hashKey);
		if($result->num_rows()>0)
		{
			$result=$result->row();
			$this->session->set_userdata('fname',$result->user_fname);
			$this->session->set_userdata('lname',$result->user_lname);
			$this->session->set_userdata('email',$result->user_email);
			$this->session->set_userdata('location', $this->regions_m->selectRegionNameByID($result->user_city_id));
			$this->session->set_userdata('id_user',$result->user_id);
			redirect('landing/register/prefill?fname='.$result->user_fname.'&lname='.$result->user_lname.'&email='.$result->user_email.'&location='.$this->regions_m->selectRegionNameByID($result->user_city_id).'&user_id='.$result->user_id);
			exit();
		}
		else
		{
			redirect('landing/register');
			exit();
		}
	}
        
    /**
     *
     * @todo remove sessions
     */
        function invitePreFill () {
            
            $fname = trim($_GET['name']);
            $lname = '';
            
            if( (strpos($fname, ' ') !== FALSE)){
                $temp=explode(' ',$fname,2);
                $fname=$temp[0];
                $lname=$temp[1];
            }
            
            $this->session->set_userdata('fname',$fname);
            $this->session->set_userdata('lname',$lname);
            $this->session->set_userdata('email',$_GET['email']);
            $this->session->set_userdata('location', $_GET['location']);
            
            redirect('landing/register/prefill?fname='.$fname.'&lname='.$lname.'&email='.$_GET['email'].'&location='.$_GET['location']);
            exit();
        }
        
        function profileRedirect($user_id = 0) {
            $this->session->set_userdata('redirect_friends_profile_id',$user_id);
            
            return redirect('landing');
        }
        function notificationRedirect() {
            $this->session->set_userdata('redirect_notification','TRUE');
            
            return redirect('landing');
        }
	
function reset_password()
{

	$data['page_title'] = $this->lang->line('reset_password').' |glomp!';
	if(isset($_POST['reset_password']))
	{
	$this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|xss_clean');
	if($this->form_validation->run() == TRUE)
	{
		$email_address = $this->input->post('user_email');
		$email_check_num = $this->login_m->verify_user($email_address);	
		$num_rows = $email_check_num->num_rows();
			if($num_rows == 1)
			{	
			$res_eml_check = $email_check_num->row();
					
				$data['reset_confirm_popup'] = true;
				$data['user_id'] = $res_eml_check->user_id;
				
			}else
			{
				$data['error_msg']	 = $this->lang->line('reset_password_email_doesnot_exist');	
			}
			
	}
	}
	$this->load->view('reset_password_v',$data);
	
}//function end

function update_password()
{
	
	$email_address = $this->input->post('email');
	$json_msg = $this->login_m->reset_password_info_id($email_address);	
	$msg = json_decode($json_msg);
	if($msg->valid_email =='true')
	{
		$this->login_m->change_password($msg->plain_password,$msg->user_id);	
	
		//send email to user with email link
				$from_email = $this->site_sender_email;
				$from_name = $this->site_sender_name;
				$to_email = $msg->user_email;
				$to_name = $msg->user_fname;
				$user_id = $msg->user_id;
				$subject = $this->lang->line('reset_password_email_subject');

				$link = site_url();
				$click_here ="<a href='$link'>Click here</a>";

                                $vars = array(
					'template_name' => 'forgot_password',
					'lang_id' => 1,
					'to' => $to_email,
					'params' => array(
                                            '[link]' => $click_here,
                                            '[user_fname]' => $to_name,
                                            '[password]' => $msg->plain_password
					)
				);

				$this->email_templating->config($vars);
				$this->email_templating->send();
                                
				
				$message = "<p>Dear $to_name,</p>";
				$message .= "<p><br/><br/>Your temporary password is: <br/><br/></p>";
				$message .= "<p>Email:".$msg->user_email."<br/>
							<p>Password:".$msg->plain_password."</p>";/*"
							<br/><br/><br/>";
				/*$message .= 'PS : This is an unattended mail box, do not reply to this email.<br/><br/>';
				$message .= '<br/>
								Cheers.
								<br/>
								The glomp! team.
								';*/
				//$this->send_email_m->sendEmail($from_email,$from_name,$to_email,$to_name,$subject,$message);
				$msg_ =  '<div class="alert alert-success">'.$this->lang->line('reset_password_successful_msg').'</div';
				die($msg_);
	}else
	{
		$msg_ =  '<div class="alert alert-error">'.$this->lang->line('Unexpected_Error').'</div';
				die($msg_);
		
		}
}

function fname_check($name){

    $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
    if (str_contains($name, $not_allowed) == false)
    {    
        //    echo "Error: Not allowed letters: ".$not_allowed;
        //todo language translation
        $this->form_validation->set_message('fname_check', 'Special characters are not allowed on First Name.');
        return false;
    }
    else{
        //  echo "ok:".$username; 
        $not_allowed='1234567890';//
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            $this->form_validation->set_message('fname_check', 'Numbers are not allowed on First Name.');
            return false;
        }
        else{
          //  echo "ok:".$username; 
          return true;                
        }            
    }    
}
function lname_check($name){

    $not_allowed='+_)(*&^%$#@!~`=[]{};\'\\:"|<>?,/';//1234567890
    if (str_contains($name, $not_allowed) == false)
    {    
        //    echo "Error: Not allowed letters: ".$not_allowed;
        //todo language translation
        $this->form_validation->set_message('lname_check', 'Special characters are not allow on Last Name.');
        return false;
    }
    else{
        //  echo "ok:".$username; 
        $not_allowed='1234567890';//
        if (str_contains($name, $not_allowed) == false)
        {    
            //    echo "Error: Not allowed letters: ".$not_allowed;
            $this->form_validation->set_message('lname_check', 'Numbers are not allow on Last Name.');
            return false;
        }
        else{
          //  echo "ok:".$username; 
          return true;                
        }            
    }    
}

function dob_check($day)
{
	$year = ($this->input->post('year')=="")?'0000':$this->input->post('year');
	$month = ($this->input->post('months')=="")?'00':$this->input->post('months');
	$month = (int)$month;
	$day = (int)$day;
	$year  = (int) $year;
	$cur_date = date('Y-m-d');
	$entered_date = $year.'-'.$month.'-'.$day;
	if(checkdate($month,$day,$year))
	{
		//check for future date
		$sql = "SELECT DATEDIFF('$cur_date','$entered_date') AS future_date";
		$res = $this->db->query($sql);
		$rec = $res->row();
		$future = $rec->future_date;

		if($future<0)
		{
			$this->form_validation->set_message('dob_check', $this->lang->line('dob_can_not_future_date') );
			return false;
		}
		else
			return true;
	}
		
	else{
			$this->form_validation->set_message('dob_check', $this->lang->line('plz_provide_valid_dob') );
			//'Please provide valid Date of Birth'
			return FALSE;
		}

}
function email_check($email)
{
	
	$email_chk = $this->login_m->emailExist($email);
	if($email_chk==0)
		return true;
	else
	{
		$this->form_validation->set_message('email_check', $this->lang->line('email_already_exist_try_new_one'));
		//'Email address already exist. Please try new one'
		return false;
	}
}

function email_prefill_check($email)
{
	
	$user_id = ($this->session->userdata('id_user'))?$this->session->userdata('id_user'):0;	
	$email_chk = $this->login_m->update_email_exist_check($email,$user_id );
	if($email_chk==0)
		return true;
	else
	{
		$this->form_validation->set_message('email_check', $this->lang->line('email_already_exist_try_new_one'));
		return false;
	}
}

function resizeImg($orginalImg,$thumbImg,$newWidth,$newHeight,$resizeOption='exact',$sharpen=true)
{
	$this->load->library('resize');
	$resizeImg = new Resize($orginalImg);
	$resizeImg->resizeImage($newWidth, $newHeight,$resizeOption,$sharpen=true);
	$resizeImg->saveImage($thumbImg, 100);
}	

function logout($from="")
{
	if($this->session->userdata('admin_id') && $this->session->userdata('is_logged_in')==true)
    {
        
        $array_items = array(            
            'user_id' => '',
            'username' => '',
            'user_name' => '',
            'user_email' => '',
            'user_fname' => '',
            'user_lname' => '',
            'user_email_verified' => '',
            'user_last_login_date' => '',
            'user_last_login_ip' => '',
            'lang_id' => '',
            'twitter_oauth_token' => '',
            'twitter_oauth_token_secret' => '',
            'is_user_logged_in' => '',   
        );
        $this->session->unset_userdata($array_items);
    }
    else
    {
        $this->session->sess_destroy();
    }
    if($from=="deactivated")
    {
        $this->session->set_flashdata('deactivated', 'Your account has been deactivated.');
        redirect(base_url('?'.$from));
    }
    else
        redirect(base_url());
	
}
}//eocs