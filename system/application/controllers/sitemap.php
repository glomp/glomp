<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sitemap extends CI_Controller {
    

    function __construct() {
        parent::__Construct();        
        $this->load->model('users_m');        
        $this->load->model('merchant_m');        
    }

    function index() {
    
        $data['merchants'] = $this->merchant_m->get_list(array(                                    
            'select' => 'merchant_id',                 
            'where' => 'merchant_status = "Active"',
            'resource_id' => true            
        ));                
        
        $data['users'] = $this->users_m->get_list(array(         
            'select' => "user_id",        
            'where' => 'user_status = "Pending" OR user_status = "Active"',
            'resource_id' => true
        ));        
        
        $this->load->view('sitemap_v', $data, fALSE, FALSE, FALSE);
    }
}