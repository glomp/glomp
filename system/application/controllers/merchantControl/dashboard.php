<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller {	
	function dashboard()
	{
		parent::__Construct();
		$this->load->library('merchant_admin_login_check');
		$this->load->model('merchant_m');
	}
	
	public function index()
	{
		$data['page_title']="Merchant Dashboard : Glomp";		
		$merchant_id = (int) $this->session->userdata('merchant_id');		
		
		$rec_merchant = $this->merchant_m->selectMerchantID($merchant_id);
		if($rec_merchant->num_rows()==1){
			$merchant_record = $rec_merchant->row();			
			$rec_merchant_outlets=$this->merchant_m->listMerchantOutlets($merchant_id);			
			$data['rec_merchant_outlets'] = $rec_merchant_outlets;
			$this->load->view(MERCHANT_FOLDER."/dashboard_v",$data);			
		}
		else
		{
			$this->load->view(MERCHANT_FOLDER.'/404_v');
		}
	}
	public function account($view="")
	{		
		$data['page_title']="Merchant Account : Glomp";		
		$data['view']=$view;		
		$showErrorPage=false;
		$merchant_id = (int) $this->session->userdata('merchant_id');		
		$rec_merchant = $this->merchant_m->selectMerchantID($merchant_id);
		if($rec_merchant->num_rows()==1){
		
			if(isset($_POST['update'])){ //account
				if($this->session->userdata('is_merchant_users')==false)
					$this->form_validation->set_rules('pword', 'Password',  'trim|required|min_length[6]|max_length[9]|callback_password_check|xss_clean');
				else
					$this->form_validation->set_rules('pword', 'Password',  'trim|required|min_length[6]|max_length[9]|callback_password_check_update2|xss_clean');				
				$this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
				$this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
				$data['for']='';		
				if($this->form_validation->run() != FALSE)					
				{
					if($this->session->userdata('is_merchant_users')==false)
						$this->merchant_m->merchantPortalUpdatePassword($merchant_id);
					else{						
						$accountID = (int) $this->session->userdata('merchant_user_id');
						$this->merchant_m->merchantPortalUpdatePasswordUSERS($accountID);
					}
					$data['msg'] = 'Successfully updated.';					
				}
			}//account			
			
			if($view=="add")
			{
				//$data['for']='users';		
				if(isset($_POST['user_add'])){
					$this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean');
					$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');
					$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[12]|callback_check_username_if_exists|xss_clean');
					$this->form_validation->set_rules('pword', 'Password',  'trim|required|min_length[6]|max_length[9]|xss_clean');
					$this->form_validation->set_rules('cpword', 'Confirm Password', 'trim|required|matches[pword]');				
					$data['for']='users';		
					if($this->form_validation->run() != FALSE)
					{
						$this->merchant_m->merchantPortalAddNewUser($merchant_id);
						$data['msg']='New user has been added.';
					}
					else
					{
						$this->session->set_userdata('temp_fname',$_POST['fname']);
						$this->session->set_userdata('temp_lname',$_POST['lname']);
						$this->session->set_userdata('temp_username',$_POST['username']);
					
					}
				}//if(isset($_POST['user_add'])){
			}
			else if($view=="edit")
			{
				$account_id= (int) $this->uri->segment(5, 0);
				$data['for']='users';
				$check_if_merchant_account=$this->merchant_m->merchantPortalCheckThisUserForThisMerchant($account_id,$merchant_id);
				if($check_if_merchant_account->num_rows > 0)
				{					
					if(isset($_POST['user_update'])){
						$this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('pword', 'Password',  'trim|required|min_length[6]|max_length[9]|callback_password_check_update|xss_clean');
						if($this->input->post('npword')!=''){							
							$this->form_validation->set_rules('npword', 'New Password', 'trim|required|min_length[6]|max_length[9]|xss_clean');
							$this->form_validation->set_rules('cnpword', 'Confirm New Password', 'trim|required|matches[npword]');
						}
						
						if($this->form_validation->run() != FALSE)
						{
							$this->merchant_m->merchantPortalUpdateUser($account_id);
							if($this->input->post('npword')!=''){
								$this->merchant_m->merchantPortalUpdateUserPassword($account_id);
							}
							$data['msg']='User has been updated.';
						}
					}
					$check_if_merchant_account=$this->merchant_m->merchantPortalCheckThisUserForThisMerchant($account_id,$merchant_id);
					$data['user_rec']=$check_if_merchant_account->row();
					
				}
				else
				{
					$showErrorPage=true;
				}
			
			}
			else{
				$data['merchantUsersLists']=$this->merchant_m->merchantPortalGetUserList($merchant_id);
			
			}		
			$merchant_record = $rec_merchant->row();						
			$data['merchant_record'] = $merchant_record;
			$rec_merchant_outlets=$this->merchant_m->listMerchantOutlets($merchant_id);			
			$data['rec_merchant_outlets'] = $rec_merchant_outlets;			
			//$data['error_msg'] = 'error_msg';
			if($showErrorPage)
			{

				$this->load->view(MERCHANT_FOLDER.'/404_v');
			}
			else
			{
				$this->load->view(MERCHANT_FOLDER."/account_v",$data);
			}
			
		}
		else
		{
			$this->load->view(MERCHANT_FOLDER.'/404_v');
		}
	}
	function password_check($password) {
		$merchant_id = (int) $this->session->userdata('merchant_id');
        if ($password == "")
            return 'ok';
        $password_verify = $this->merchant_m->password_verify($password,$merchant_id);
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check', 'Invalid current password.');
            return false;
        }
    }
	function password_check_update($password) {
		$merchant_id 	= (int) $this->session->userdata('merchant_id');
		$account_id		= (int) $this->uri->segment(5,0);
        if ($password == "")
            return 'ok';
        $password_verify = $this->merchant_m->password_verify_update($password,$merchant_id,$account_id);
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check_update', 'Invalid current password.');
            return false;
        }
    }
	function password_check_update2($password) {
		$merchant_id 	= (int) $this->session->userdata('merchant_id');
		$account_id		= (int) $this->session->userdata('merchant_user_id');
        if ($password == "")
            return 'ok';
        $password_verify = $this->merchant_m->password_verify_update($password,$merchant_id,$account_id);
        if ($password_verify == 'ok')
            return true;
        else {
            $this->form_validation->set_message('password_check_update2', 'Invalid current password.');
            return false;
        }
    }
	function check_username_if_exists($username){
		$merchant_id = (int) $this->session->userdata('merchant_id');
		if ($username== "")
            return 'ok';	
		$check_username = $this->merchant_m->check_username_if_exists($username,$merchant_id);
		if ($check_username == 'ok')
            return true;
        else {
            $this->form_validation->set_message('check_username_if_exists', 'Username already exist.');
            return false;
        }
	
	}
	function check_username_if_exists_update($username){
		$merchant_id 	= (int) $this->session->userdata('merchant_id');
		$account_id		= (int) $this->uri->segment(5,0);
		if ($username== "")
            return 'ok';
		$check_username = $this->merchant_m->check_username_if_exists_update($username,$merchant_id,$account_id);
		if ($check_username == 'ok')
            return true;
        else {
            $this->form_validation->set_message('check_username_if_exists', 'Username already exist.');
            return false;
        }	
	}
}