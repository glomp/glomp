<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twitter_api extends CI_Controller {    
    
    private $CONSUMER_KEY = 'OIbLitQ6D85SBxwusqmU9A';
	private $CONSUMER_SECRET = 'aTPrWMv28w9mOJMjbmdj1aPpTcZXm4j4PAf9LbsAo';
    private $OAUTH_CALLBACK;//site_url('twitter_api/callback/');//'http://glomp.it/twitter_api/callback/';//192.168.1.224/glomp
    private $INSTANCE="gl_voucher";
	private $INSTANCE_USER="gl_user";
    
    function __construct()
    {
        parent::__construct();		
        $this->load->library('user_login_check');
        $this->load->library('twitteroauth');
        $this->load->model('users_m');
        $this->load->model('product_m');
        $this->OAUTH_CALLBACK = site_url(). 'twitter_api/callback/';
    }
    function tweet($from="",$voucher_id="", $from_callback=false, $is_redeem="")
    {
        
        $response= (object) array('status' => 'Error', 'message' => '','data' => '');
        $voucher=$voucher_id;                
        $result=$this->db->get_where($this->INSTANCE,array('voucher_id'=>$voucher));
        if(($result->num_rows())>0){
            $res=$result->row();
            $purchaserID=$res->voucher_purchaser_user_id;
            $belongsID=$res->voucher_belongs_usser_id;
            //first of all verify user_id				
            $purchaser_rec = $this->users_m->user_info_by_id($purchaserID);
            $purchaser_data=$purchaser_rec->row();
            
            $belongs_rec = $this->users_m->user_info_by_id($belongsID);				
            $belongs_data=$belongs_rec->row();
            
            $data['purchaser_data']=$purchaser_data;
            $data['belongs_data']=$belongs_data;
            $product_id=$res->voucher_product_id;                   
            
            $prod_info = json_decode($this->product_m->productInfo($product_id));    
            $merchant_name = $prod_info->product->$product_id->merchant_name;
            $prod_name = $prod_info->product->$product_id->prod_name;
            //echo $belongs_rec->num_rows()."-".$belongsID."<br>";
            //echo $purchaser_rec->num_rows()."-".$purchaserID."<br>";
            if($this->session->userdata('twitter_oauth_token')=='' || $this->session->userdata('twitter_oauth_token_secret')=='')
            {
                return $this->clearsessions($from, $voucher_id);
            }
            $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $this->session->userdata('twitter_oauth_token'), $this->session->userdata('twitter_oauth_token_secret'));
            if($is_redeem=='redeem')
            {
                $story= $belongs_data->user_name.' has redeemed a '.$merchant_name.' '.stripslashes($prod_name).'.';
                $tweet=$story.' http://glomp.it/glomp_story/tweet_redeem/'.$voucher_id.' @glompit #glomp';        
            }
            else
            {
                $story= $purchaser_data->user_name.' glomp!ed '.$belongs_data->user_name.' to a '.$merchant_name.' '.stripslashes($prod_name).'.';
                $tweet=$story.' http://glomp.it/glomp_story/tweet/'.$voucher_id.' @glompit #glomp';        
            }
            $res=$connection->post('statuses/update', array( 'status' => $tweet));
            if( isset($res->errors))
            {
                if($res->errors[0]->code==187) {
                    $response->message = $res->errors[0]->message;                
                }  
                if($res->errors[0]->code==89)
                {
                   return $this->clearsessions($from, $voucher_id);                   
                }
            }
            else
            {
                $response->status='Ok';
                 $response->message='Tweet successfully posted.';
            }
            
        }
        echo json_encode($response);
        if($from_callback === TRUE || $from_callback === 'true')
        {
            $tweet_status   =$response->status;
            $tweet_message  =$response->message;
            $this->session->set_flashdata('tweet_status',$tweet_status);
            $this->session->set_flashdata('tweet_message',$tweet_message);
            $url = base_url('user/dashboard/');

            if($from=='dashboard')
                $url = base_url('user/dashboard/');
            else if($from=='dashboard_m')
                $url = base_url('m/user/dashboard/');
            else if($from=='glomp')
                $url = base_url('glomp/'); 
            else if($from=='searchFriends')
                $url = base_url('user/searchFriends/');                 
            else if($from=='profile')
                $url = base_url('profile/');    
            else if($from=='profile_m')
                $url = base_url('m/profile/');
            else if($from=='redeem_m')
                $url = base_url('m/redeem/validated/'.$voucher_id);           
            else 
            {
                //redirect to mobile
                $device = $user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone'): 'Computer');
                if($device!='Computer')
                {
                    $url = base_url('m/profile/view/'.$from);    
                }
                else
                {
                    $url = base_url('profile/view/'.$from);    
                }
            } 
            
                
            return redirect($url);
        }
    }
    function index($voucher_id="")
    {
        
        if($this->session->userdata('twitter_oauth_token')=='' || $this->session->userdata('twitter_oauth_token_secret')=='')
        {
           return $this->clearsessions();
        }
        $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $this->session->userdata('twitter_oauth_token'), $this->session->userdata('twitter_oauth_token_secret'));
        
        //print_r($connection);
        
        //$tweet='http://glomp.it/glomp_story/tweet/35a2f79a-5e66-11e3-9cec-bc764e1c022e -'.time().' @glompit';        
        //$res=$connection->post('statuses/update', array( 'status' => $tweet));
        $res=$connection->get('account/settings');        
        //print_r($res->screen_name);
        //print_r($res);
        
    }
    function clearsessions($from='', $voucher_id='')
    {       
        $array_items = array(        
            'twitter_oauth_token' => '',
            'twitter_oauth_token_secret' => ''           
        );
        $this->session->unset_userdata($array_items);
        $this->connect($from, $voucher_id);  
    }
    function callback($from='',$voucher_id="",$is_redeem="")
    {
        if (isset($_GET['oauth_token']) && $this->session->userdata('twitter_oauth_token') !== $_GET['oauth_token'])
        {
            return $this->clearsessions();  
        }           
        
        $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $this->session->userdata('twitter_oauth_token'), $this->session->userdata('twitter_oauth_token_secret'));
        //print_r($connection);
        
        
        /* Request access tokens from twitter */
        $access_token = $connection->getAccessToken($_GET['oauth_verifier']);

        /* Save the access tokens. Normally these would be saved in a database for future use. */
        $_SESSION['access_token'] = $access_token;
        $this->session->set_userdata('twitter_oauth_token',$access_token['oauth_token']);
        $this->session->set_userdata('twitter_oauth_token_secret',$access_token['oauth_token_secret']);

        /* Remove no longer needed request tokens 
         $array_items = array(        
            'twitter_oauth_token' => '',
            'twitter_oauth_token_secret' => ''           
        );
        $this->session->unset_userdata($array_items);*/
        
        

        /* If HTTP response is 200 continue otherwise send to connect page to retry */
        if (200 == $connection->http_code) {
          /* The user has been verified and the access tokens can be saved for future use */
          //$_SESSION['status'] = 'verified';
            return $this->tweet($from, $voucher_id,true,$is_redeem);  
        } else {
          /* Save HTTP status for error dialog on connnect page.*/
           return $this->clearsessions($from, $voucher_id);  
        }

        
    }
    function connect($from='', $voucher_id='')
    {
        $response= (object) array('status' => 'Error', 'message' => '','data' => '');
        /* Build TwitterOAuth object with client credentials. */
        $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET);
         
        /* Get temporary credentials. */
        $temp=$this->OAUTH_CALLBACK;
        $temp.=$from.'/'.$voucher_id.'/';
        $request_token = $connection->getRequestToken($temp);

        /* Save temporary credentials to session. */
        $token              = $request_token['oauth_token'];
        $oauth_token_secret =$request_token['oauth_token_secret'];
        
        $this->session->set_userdata('twitter_oauth_token',$token);
        $this->session->set_userdata('twitter_oauth_token_secret',$oauth_token_secret);
         
        $this->users_m->update_twitter_token($this->session->userdata('user_id'),$token,$oauth_token_secret);
         
        /* If last connection failed don't display authorization link. */
        switch ($connection->http_code) {
          case 200:
            /* Build authorize URL and redirect user to Twitter. */
            $url = $connection->getAuthorizeURL($token);
            $response->status='Connect';
            $response->data=$url;
            //header('Location: ' . $url); 
            break;
          default:
            /* Show notification if something went wrong. */
            echo 'Could not connect to Twitter. Refresh the page or try again later.';
        }
        echo json_encode($response);        
    
    }
}
?>