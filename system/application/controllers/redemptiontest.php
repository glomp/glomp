<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RedemptionTest extends CI_Controller {

    public $name = 'Redemption Test';

    public $interval = 360;

    public $user_email = 'lou.garcia@glomp.it';

    public $user_password = '123456';

    public $merchant_email = 'z@glomp.it';

    private $user;

    private $merchant;

    private $outlet_code;

    private $product; // the product to give

    private $item; // the item to redeem

    public function __construct() {
        parent::__construct();
    }

    public function index() {

    }

    public function process() {

        $this->glomp();
        $this->redeem();

        exit( 'DONE' );
    }

    public function glomp() {
        if( !$this->get_user() ) {
            log_message( 'error', 'Error: Redemption Test - No Test User Provided' );
            exit( 'Error: No Test User Provided' );
        }

        if( !$this->get_merchant() ) {
            log_message( 'error', 'Error: Redemption Test -  No Test Merchant Provided' );
            exit( 'Error: No Test Merchant Provided' );
        }

        if( !$this->get_product() ) {
            log_message( 'error', 'Error: Redemption Test -  No Test Product Provided' );
            exit( 'Error: No Test Product Provided' );
        }

        $this->glomp_user();
    }

    public function redeem() {
        if( !$this->get_user() ) {
            log_message( 'error', 'Error: Redemption Test - No Test User Provided' );
            exit( 'Error: No Test User Provided' );
        }
        if( !$this->get_item() ) {
            log_message( 'error', 'Error: Redemption Test - No Item to Redeem' );
            exit( 'Error: No Item to Redeem' );
        }
        if( !$this->get_outlet_code() ) {
            log_message( 'error', 'Error: Redemption Test - No Valid Outlet Code' );
            exit( 'Error: Invalid Outlet Code' );
        }

        $this->redeem_item();
    }

    /**
     * Returns Test merchant account
     * used in $this->get_product_id()
     *
     * @return array
     */
    private function get_merchant() {

        $query = $this->db->select( 'merchant_id, merchant_email' )
            ->from( 'gl_merchant' )
            ->where( "merchant_email = '$this->merchant_email'" )
            ->get();

        if( $query->num_rows > 0 ) {
            $this->merchant = $query->row();

            return true;
        }

        return false;
    }

    /**
     * Returns Test product id
     * product id's are chosen randomly
     *
     * @return int
     */
    private function get_product() {

        $query = $this->db->select('prod_id,prod_merchant_id')
            ->from( 'gl_product' )
            ->where( 'prod_merchant_id', $this->merchant->merchant_id )
            ->get();

        if( $query->num_rows > 0 ) {
            $records = $query->result();
            $key = array_rand( $records );
            $this->product = $records[ $key ];

            return true;
        }

        return false;
    }

    /**
     * Fetches user by email address
     * Returns user_id when found
     *
     * @return int
     */
    private function get_user() {

        if( $this->user ) return true;

        $query = $this->db
            ->select( 'user_id, user_email, user_lname, user_fname' )
            ->from( 'gl_user' )
            ->where( "user_email = '$this->user_email'" )
            ->get();

        if( $query->num_rows > 0 ) {
            $this->user = $query->row();
            return true;
        }

        return false;
    }

    /**
     * Imitates submitting a form for glomping users
     *
     * @return n/a
     */
    private function glomp_user() {

        error_log( "An automated test to glomp a user has started\n", 3, 'logs/automation.log' );

        $url = $this->config->item( 'base_url' ) . 'profile/glompToUser/' . $this->user->user_id . '/';
        $ch = curl_init( $url );

        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, array(
            'message' => 'Automated Test for Glomping User',
            'password' => $this->user_password,
            'user_lname' => $this->user->user_lname,
            'user_fname' => $this->user->user_fname,
            'user_email' => $this->user_email,
            'product_id' => $this->product->prod_id,
            'automation' => true,
            'user_id' => $this->user->user_id
        ) );

        $result = curl_exec( $ch );
        curl_close( $ch );
        if( $result ) {
            error_log( "The automated test to glomp a user was executed successfully\n", 3, 'logs/automation.log' );
        } else {

        }
    }

    /**
     * Returns the outlet code
     * must be called after $this->get_item()
     *
     * @return string
     */
    private function get_outlet_code() {
        $query = $this->db
                    ->select( 'outlet_id, outlet_merchant_id, outlet_code, outlet_status ')
                    ->from( 'gl_outlet' )
                    ->where(
                        array(
                            'outlet_status' => 'Active',
                            'outlet_merchant_id' => $this->item->voucher_merchant_id )
                        )
                    ->get();

        if( $query->num_rows > 0 ) {
            $outlets = $query->result();
            $key = array_rand( $outlets );
            $this->outlet_code = $outlets[ $key ]->outlet_code;
            return true;
        }

        return false;
    }

    /**
     *
     *
     */
    private function get_item() {
        $this->load->model( 'users_m' );
        $query = $this->users_m->mobile_glomped_me($this->user->user_id);

        if( $query->num_rows > 0 ) {
            $this->item = $query->row(); // first item
            return true;
        }

        return false;
    }


    /**
     *
     *
     */
    private function redeem_item() {

        error_log( "An automated test to redeem an iten has started\n", 3, 'logs/automation.log' );

        $url = $this->config->item( 'base_url' ) . 'm/redeem/voucher2/' . $this->item->voucher_id;
        $ch = curl_init( $url );

        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, array(
            'outlet_code' => $this->outlet_code,
            'automation' => true,
            'user_id' => $this->user->user_id,
            'merchant_id' => $this->item->voucher_merchant_id
        ) );


        $result = curl_exec( $ch );
        curl_close( $ch );
        if( $result ) {
            error_log( "The automated test to redeem an item was executed successfully\n", 3, 'logs/automation.log' );
        } else {

        }
    }
}
/*
codes for user_login_check.php, user_login_check_m.php
                // automated test
                if( $CI->input->post( 'automation' ) && $CI->input->post( 'user_id' ) ) {
                    $CI->session->set_userdata( 'user_id', $CI->input->post( 'user_id' ) );
                    $CI->session->set_userdata( 'is_user_logged_in', true );
                }
*/
