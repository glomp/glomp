<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends CI_Controller {
    
    public $subpage;
    
    public $glompee;
    public $device;
    
    public $product_id = 0;
    
    private $is_preview = FALSE;
	
    var $PAYPAL_ENVIRONMENT='live';//live or sandbox
    public function __construct() {
        parent::__construct();

        $this->load->model('Brand_m', 'Brand');
        $this->load->model('users_m');
        $this->load->model('login_m');
        $this->load->library('custom_func');
        $this->load->model('cmspages_m');
		
        $this->device = $user_signup_device = ($this->mobile_detect->isMobile() ? ($this->mobile_detect->isTablet() ? 'Tablet' : 'Phone'): 'Computer');
    }
    
    function brand_product_info($group_id) {
        $this->load->view( 'm/brands_public_products_info_v', array('group_id' => $group_id), FALSE, TRUE );
    }
    
    function amex() {
        return redirect(site_url('brands/view/amex'));
    }

    public function index() {
		
	
		$this->load->library('user_login_check');
        $data = array();
        $data['inTour'] = 'no';
        $data['tabSelected'] = 'brands';
        $data['user_record'] = $this->glompee;
        // brands
        $this->db->order_by( 'name', 'asc' );
        $this->db->where(array('status'=>'Active', 'location_id' => $this->glompee->user_city_id));
        $q = $this->db->get( 'gl_brand' );
        $brands = FALSE;
        if( $q->num_rows() > 0 ) {
            $brands = $q->result();
        }
        $data['brands'] = $brands;


        if( strpos( uri_string(), 'm/' ) !== false ) {
            $this->load->view( 'm/brands_index_v', $data, FALSE, TRUE );
        } else {
            $this->load->view( 'brands_index_v', $data, FALSE, TRUE );
        }

    }
	function dob_check($day)
	{
		$year = ($this->input->post('year')=="")?'0000':$this->input->post('year');
		$month = ($this->input->post('months')=="")?'00':$this->input->post('months');
		$month = (int)$month;
		$day = (int)$day;
		$year  = (int) $year;
		$cur_date = date('Y-m-d');
		$entered_date = $year.'-'.$month.'-'.$day;
		if(checkdate($month,$day,$year))
		{
			//check for future date
			$sql = "SELECT DATEDIFF('$cur_date','$entered_date') AS future_date";
			$res = $this->db->query($sql);
			$rec = $res->row();
			$future = $rec->future_date;

			if($future<0)
			{	
				$this->form_validation->set_message('dob_check', $this->lang->line('dob_can_not_future_date') );
				return false;
			}
			else if($future < (365*18))
			{
				//check if age is greater than or equal to 18
				$this->form_validation->set_message('dob_check', $this->lang->line('not_18','Sorry, you must be at least 18 years of age to enter this site.') );
				return false;
			}
			else
				return true;
		}
			
		else{
				$this->form_validation->set_message('dob_check', $this->lang->line('plz_provide_valid_dob') );
				//'Please provide valid Date of Birth'
				return FALSE;
			}

	}
	public function check_email_if_already_registered($email)
	{
		if($this->login_m->email_validateion($this->input->post('email')) >0)
		{
			$this->form_validation->set_message('check_email_if_already_registered',$this->lang->compound(	'plz_login',
									array(
										'link' => '<a href="'.site_url().'" >login</a>'
									),
									'Your email is already registered. Please login.' ));
			return false;
		}
		else 
			return true;
    
	}
	function mysql_uuid()
	{
        $sql = "select UUID() as uuid";
        $res = $this->db->query($sql);
        return $res->row()->uuid;
    }
	function vocher_expiration_day($add_day)
	{
		$date = date('Y-m-d');
		$sql = "SELECT  ADDDATE('" . $date . "','$add_day DAY')AS new_date";
		$res = $this->db->query($sql);
		$rec = $res->row();
		return $rec->new_date;
	}
	public function details($group_voucher_id="")
	{
		$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
		$result = $this->db->query($sql);
		$data['success_message']="";
		if ($result->num_rows() > 0)
		{
		
			if($_POST)
			{
				if($this->input->post('transaction_type') =='glomp_to_fb')
				{
					$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
					$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
					$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
				}
			
				$this->form_validation->set_rules('delivery_postcode', 'Delivery Postcode', 'trim|required');
				$this->form_validation->set_rules('contact_num', 'Contact Number', 'trim|required');
				$this->form_validation->set_rules('delivery_address', 'Delivery Address', 'trim|required');
				
				if($this->form_validation->run() == TRUE)
				{
					$where = array(
						'group_voucher_id' => $group_voucher_id
					);
					$update_data = array(
							'delivery_address' => $this->input->post('delivery_address'),
							'contact_num' => $this->input->post('contact_num'),
							'delivery_postcode' => $this->input->post('delivery_postcode'),
							);
					if($this->input->post('transaction_type') =='glomp_to_fb')
					{
						$update_data['email'] = $this->input->post('email');
						$update_data['first_name'] = $this->input->post('first_name');
						$update_data['last_name'] = $this->input->post('last_name');
					}		
							
					$this->db->update('gl_voucher_brand', $update_data, $where);
					
					if($this->input->post('transaction_type') =='glomp_to_fb')
					{
						//try to register this user
						$sql = "SELECT * FROM gl_user WHERE user_email='".$this->input->post('email')."'";
							$res = $this->db->query($sql);

							if ($res->num_rows() == 0) 
							{
								$signup_data = array('username' => $this->input->post('email'),
									'user_fname' => $this->input->post('first_name'),
									'user_lname' => $this->input->post('last_name'),
									'user_email' => $this->input->post('email'),
									'user_city_id' => 1,
									'user_join_date' => date('Y-m-d H:i:s'),
									'user_account_created_ip' => $this->custom_func->getUserIP(),
									'user_hash_key' => $this->mysql_uuid(),
									'user_status' => 'Pending',
									'user_last_updated_date' => $this->custom_func->datetime()
								);

								$this->db->insert('gl_user', $signup_data);
								$insert_id = $this->db->insert_id();
								
							}
							else
							{
								//update
								$where = array(
									'user_email' => $this->input->post('email')
								);
								$update_data = array(
									'user_fname' => $this->input->post('first_name'),
									'user_lname' => $this->input->post('last_name'),
								);
								$this->db->update('gl_user', $update_data, $where);
							}
						
						//try to register this user
						
					}
					
					
					/*send a notification_amex_acceptance email*/
					/*send a notification_amex_acceptance email*/
					
						$this->load->library('email_templating');
						
						
						
						$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
						$result = $this->db->query($sql);
						$gl_voucher_brand_data = $result->row();
						
						
						$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$gl_voucher_brand_data->session_cart_id."' ";
						$gl_cart_data =  $result = $this->db->query($sql);
						$table="";
						
						
						$product_merchant_list='';
						foreach($result->result() as $row)
						{
							$product = json_decode($row->prod_details);
							
							
							$table .='
								<tr>
									<td><b>'.$product->prod_name.'</b><br>'.$product->merchant_name.'</td>
									<td align="center">'.$row->prod_qty.'</td>
								</tr>
							';
							if($product_merchant_list!="")
								$product_merchant_list = $product_merchant_list.' ';
								
							$product_merchant_list .= '<b>'.$product->prod_name.'</b> ['.$product->merchant_name.'],';
						}
						$product_merchant_list = rtrim($product_merchant_list, ",");
						$product_details ='
							<table width="100%" cellpadding="2" cellspacing="2" border="1">
								<tr>
									<th>Items</th>
									<th>Quantity</th>
								</tr>
								'.$table.'
							</table>
						';
						
						
						$delivery_address = '<b>'.($gl_voucher_brand_data->delivery_address).', '.($gl_voucher_brand_data->delivery_postcode).'</b>';
						$contact_no = '<b>'.($gl_voucher_brand_data->contact_num).'</b>';
						
						
							$email_subject = 'Confirmation of your acceptance - '.$group_voucher_id;
							$vars = array(
								'template_name' => 'notification_amex_acceptance',
								'from_name' => SITE_DEFAULT_SENDER_NAME,
								'from_email' => SITE_DEFAULT_SENDER_EMAIL,
								'lang_id' => 1,
								'to' => $gl_voucher_brand_data->email,
								'params' => array(
									'[product_details]' => $product_details,
									'[delivery_address]' => $delivery_address,
									'[contact_no]' => $contact_no,
								)
							);
							$this->email_templating->config($vars);
							$this->email_templating->set_subject($email_subject);
							$this->email_templating->send();
							
							
							$this->send_email_to_merchants($group_voucher_id);
					
					
					
					
					
					$data['success_message'] = 'Successfully save. An email has been sent to you.';
					$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
					$result = $this->db->query($sql);
				}
			}
		
			$gl_voucher_brand_data = $result->row();
			$data['gl_voucher_brand_data'] = $gl_voucher_brand_data;
			
			
			$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$gl_voucher_brand_data->session_cart_id."' ";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0)
			{
				$data['cart_data'] = $result->result();
			}
			
			if($gl_voucher_brand_data->transaction_type =='glomp_to_friend' || $gl_voucher_brand_data->transaction_type =='glomp_to_fb')
			{
			
				$this->load->view( 'm/brand_public_voucher_details_v', $data, FALSE, TRUE );
			}
			else
			{
				$data['error_404'] =  $this->lang->line('page_404_error');
				$this->load->view('404_v',$data);
			}
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v',$data);
		}
	}
	public function acceptance($public_alias='hsbc', $group_voucher_id = "")
	{
		switch ($public_alias) {
		    case 'hsbc':
		        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
		        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
		        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
		        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
		        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
		        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
		        $kpo =  HSBC_BRAND_PROD_ID_KPO;
		        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
		        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
		        $valrhona = '0';
	            $laurent_perrier =  '0';
		        $providore =  '0';
		        $glenfiddich =  '0';
		        $moet_chandon =  '0';
		        $kusmi_tea =  '0';
		        $euraco =  '0';
		        break;
		    case 'uob':
		        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
		        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
		        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
		        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
		        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
		        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
		        $kpo =  UOB_BRAND_PROD_ID_KPO;
		        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
		        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
		        $laurent_perrier =  '0';
	            $providore =  '0';
		        $glenfiddich =  '0';
		        $moet_chandon =  '0';
		        $kusmi_tea =  '0';
		        $euraco =  '0';
		        break;
	        case 'dbs':
		        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
		        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
		        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
		        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
		        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
		        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
		        $kpo =  DBS_BRAND_PROD_ID_KPO;
		        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
		        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
		        $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
	            $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
		        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
		        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
		        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
		        $euraco =  DBS_BRAND_PROD_ID_EURACO;
		        break;
	        case 'amex':
		        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
		        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
		        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
		        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
		        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
		        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
		        $kpo =  AMEX_BRAND_PROD_ID_KPO;
		        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
		        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
		        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
		        $laurent_perrier =  '0';
	            $providore =  '0';
		        $glenfiddich =  '0';
		        $moet_chandon =  '0';
		        $kusmi_tea =  '0';
		        $euraco = '0';
		        break;
		}

		define('BRAND_PROD_ID_MACALLAN',$macallan);
		define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
		define('BRAND_PROD_ID_LONDON_DRY',$london);

		define('BRAND_PROD_ID_SINGBEV',$singbev);
		define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
		define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

		define('BRAND_PROD_ID_KPO',$kpo);
		define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
		define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
		define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
		define('BRAND_PROD_ID_PROVIDORE',$providore);
		define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
		define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
		define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
		define('BRAND_PROD_ID_EURACO',$euraco);
		 
		
		
		$data['success_message'] = '';
		$data['data'] = array(
			'group_voucher_id' => ''
		);

		$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
			'group_voucher_id' => $group_voucher_id
		));

		if ($voucher_brand->num_rows() > 0) {
			$data['data']['group_voucher_id'] = $group_voucher_id;
			$data['data']['gl_voucher_brand_data'] = $voucher_brand->row();
		
			if($_POST)
			{
				if($this->input->post('transaction_type') =='glomp_to_fb')
				{
					$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
					$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
					$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
				}
				
				if($voucher_brand->row()->delivery_type =='Delivery')
				{
					$this->form_validation->set_rules('delivery_postcode', 'Delivery Postcode', 'trim|required');
					$this->form_validation->set_rules('contact_num', 'Contact Number', 'trim|required');
					$this->form_validation->set_rules('delivery_address', 'Delivery Address', 'trim|required');
				}
				
				if($this->form_validation->run() == TRUE)
				{

					$delivery_address = $this->input->post('delivery_address');
					$contact_num = $this->input->post('contact_num');
					$delivery_postcode = $this->input->post('delivery_postcode');

					$delivery_address = (empty($delivery_address)) ? '&nbsp;' : $delivery_address;
					$contact_num = (empty($contact_num)) ? '&nbsp;' : $contact_num;
					$delivery_postcode = (empty($delivery_postcode)) ? '&nbsp;' : $delivery_postcode;

					$where = array(
						'group_voucher_id' => $group_voucher_id
					);
					$update_data = array(
						'delivery_address' => $delivery_address,
						'contact_num' => $contact_num,
						'delivery_postcode' => $delivery_postcode,
					);
					if($this->input->post('transaction_type') =='glomp_to_fb')
					{
						$update_data['email'] = $this->input->post('email');
						$update_data['first_name'] = $this->input->post('first_name');
						$update_data['last_name'] = $this->input->post('last_name');
					}		
							
					$this->db->update('gl_voucher_brand', $update_data, $where);
					
					if($this->input->post('transaction_type') =='glomp_to_fb')
					{
							//try to register this user
							$sql = "SELECT * FROM gl_user WHERE user_email='".$this->input->post('email')."'";
							$res = $this->db->query($sql);

							if ($res->num_rows() == 0) 
							{
								$signup_data = array('username' => $this->input->post('email'),
									'user_fname' => $this->input->post('first_name'),
									'user_lname' => $this->input->post('last_name'),
									'user_email' => $this->input->post('email'),
									'user_city_id' => 1,
									'user_join_date' => date('Y-m-d H:i:s'),
									'user_account_created_ip' => $this->custom_func->getUserIP(),
									'user_hash_key' => $this->mysql_uuid(),
									'user_status' => 'Pending',
									'user_last_updated_date' => $this->custom_func->datetime()
								);

								$this->db->insert('gl_user', $signup_data);
								$insert_id = $this->db->insert_id();
								
							}
							else
							{
								//update
								$where = array(
									'user_email' => $this->input->post('email'),
									'user_status' => 'Pending'
								);

								$update_data = array(
									'user_fname' => $this->input->post('first_name'),
									'user_lname' => $this->input->post('last_name'),
									'username' => $this->input->post('email'),
								);
								$this->db->update('gl_user', $update_data, $where);
							}
						
						//try to register this user
						
					}
					
					/*send a acceptance_glompee_delivery email*/
					$this->load->library('email_templating');
					$this->load->model('Brand_m');

					$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
		 				'group_voucher_id' => $group_voucher_id
					));
					if ($voucher_brand->num_rows() > 0) {
						// get cart_details
						$sql = "SELECT *,
							CASE brandproduct_id
								WHEN '".BRAND_PROD_ID_MACALLAN."' THEN 0
								WHEN '".BRAND_PROD_ID_SNOW_LEOPARD."' THEN 0
								WHEN '".BRAND_PROD_ID_LONDON_DRY."' THEN 0
								ELSE brandproduct_id
							END as group_brandproduct_id
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								public_alias = '".$public_alias."' 		
							GROUP BY group_brandproduct_id
						";
						
						$result = $this->db->query($sql);
						$cart_data="";
						foreach( $result->result() as $row)
						{
							if($row->group_brandproduct_id==0)
							{
								$group_brand_array = BRAND_PROD_ID_MACALLAN.','.BRAND_PROD_ID_SNOW_LEOPARD.','.BRAND_PROD_ID_LONDON_DRY;
								$sql = "SELECT *
								FROM gl_cart 
								WHERE 
									session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
									public_alias = '".$public_alias."' AND		
									brandproduct_id IN (".$group_brand_array.") 
								";
							}
							else
							{
								$sql = "SELECT *
								FROM gl_cart 
								WHERE 
									session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
									public_alias = '".$public_alias."' AND		
									brandproduct_id = '".$row->brandproduct_id."'
								";
							}
							$prod_details = json_decode($row->prod_details);
							$product_list="";
							foreach($this->db->query($sql)->result() as $product)
							{
								$sql = "SELECT *
								FROM gl_cart_option
								WHERE 
									session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
									prod_id = '".$product->prod_id."' AND		
									prod_group = '".$product->prod_group."'
								";

								$option = $this->db->query($sql)->result();

								$product_list[] = array (
									'product' => $product,
									'option' => $option,
									);

							}

							$cart_data [] = array (
								'brand_id' => $prod_details->brandproduct_id,
								'brand_name' => $this->Brand_m->get_brandproduct($prod_details->brandproduct_id)->name,
								'prod_list' => $product_list
							);
						
						}
					}

					//Create product details table
					foreach ($cart_data as $brand_row) {
						$brand_id = $brand_row['brand_id'];
						$delivery_type = '';
						$per_brand_total = 0;
						$per_brand_qty = 0;
						$grand_total = 0;
						$grand_total_2 = 0;
						$table="";
						$has_delivery = FALSE;
						$delivery = '';
						$prouct_merchant_name_arr = array();
						foreach ($brand_row['prod_list'] as $row) {
							$options = $row['option'];
							$row = $row['product'];
							$cost_price = get_hsbc_prod_data($row->prod_id);
							$delivery_type = $row->delivery_type;
							$product = json_decode($row->prod_details);
							$grand_total += (($row->prod_cost ) * $row->prod_qty);
							$grand_total_2 += (($cost_price['cost_price'] ) * $row->prod_qty);
							$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
							$per_brand_qty += $row->prod_qty;

							$product_merchant_name = '<b>' .$product->prod_name.'</b> <br />'.$product->merchant_name;
							$product_merchant_name_2 = '<b>' .$product->prod_name.'</b> ('.$product->merchant_name.')';
							$prouct_merchant_name_arr[] = $product_merchant_name_2;
							if(count($options) > 0)	{
								$product_merchant_name .= '<br /><br />';
								foreach ($options as $p) {
									$product_merchant_name .= '&nbsp;&nbsp;&nbsp;'. $p->option_qty .'-'. $p->option_value .'<br />';
								}
							}

							$table .='
							<tr>
								<td><b>'.$product_merchant_name.'</td>
								<td align="right">$ '.number_format(($row->prod_cost),2).'</td>
								<td align="right">'.$row->prod_qty.'</td>
								<td align="right">$ '.number_format(($row->prod_cost) * ($row->prod_qty),2).'</td>
							</tr>';

							//merchant
							$table2 .='
							<tr>
								<td><b>'.$product_merchant_name.'</td>
								<td align="right">$ '.number_format($cost_price['cost_price'],2).'</td>
								<td align="right">'.$row->prod_qty.'</td>
								<td align="right">$ '.number_format(($cost_price['cost_price']) * ($row->prod_qty),2).'</td>
							</tr>';
						}

						switch ($brand_row['brand_id']) {
							case BRAND_PROD_ID_SWIRLS_BAKESHOP:
								$has_delivery = 16;
								break;
							case BRAND_PROD_ID_SINGBEV:
								if($per_brand_total < 350)
								{
									$has_delivery = 35;
								}	
								break;
							case BRAND_PROD_ID_LAURENT_PERRIER:
								if($per_brand_qty < 3)
								{
									$has_delivery = 20;
								}	
								break;
							case BRAND_PROD_ID_PROVIDORE:
								if($per_brand_total < 200)
								{
									$has_delivery = 20;
								}	
								break;
							case BRAND_PROD_ID_EURACO:
								if($per_brand_total < 300)
								{
									$has_delivery = 30;
								}	
								break;
							case BRAND_PROD_ID_DELIGHTS_HEAVEN:
								$has_delivery = 25;
								break;
						}

						if ($delivery_type == 'pickup') {
							$has_delivery = 0;							
						}

						$grand_total = $grand_total + $has_delivery;
						if ($has_delivery > 0) {
							$delivery = ' (Delivery charge: S$ '.number_format($has_delivery, 2).')';	
						}

						$product_details ='
						<table width="100%" cellpadding="2" cellspacing="0" border="1">
							<tr>
								<th>Items</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total</th>
							</tr>
							'.$table.'
						</table>';

						$product_details_merchant ='
						<table width="100%" cellpadding="2" cellspacing="0" border="1">
							<tr>
								<th>Items</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total</th>
							</tr>
							'.$table2.'
						</table>';

						$template = '';						
						$pick_up_details = '';
						

						$template = 'acceptance_glompee_delivery'; 
						$vars['to'] = $voucher_brand->row()->email;
						$vars['params'] = array(
							'[voucher_code]' => '<b>'. $voucher_brand->row()->voucher_code. '</b>',
							'[product_details]' => $product_details,
							'[total_sg_usd]' => 'S$ '.number_format($grand_total,2). $delivery,
							'[delivery_address]' => $voucher_brand->row()->delivery_address,
							'[contact_no]' => $voucher_brand->row()->contact_num,
						);


						switch ($brand_row['brand_id']) {
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
								$pick_up_details = '
									Edrington Singapore located at 12 Marina View, Level 24-01 Asia Square Tower 2, Singapore 018961 during office hours. <br />
									Please allow 3 working days after order placement.';
								break;
							case BRAND_PROD_ID_SWIRLS_BAKESHOP:
								$pick_up_details = '
									Main Outlet: <br />
									8 Rodyk Street, #01-08 <br />
									Singapore 238216 <br />
									<br />
									Specialty Outlet:<br />
									200 Turf Club Road, #03-07 The Grandstand<br />
									(Chillax Market)<br />
									Singapore 287994<br />
									<br />
									11am - 7pm every day';
								break;
							case BRAND_PROD_ID_SINGBEV:
								$pick_up_details = '
									470 North Bridge Road, #05-09 Bugis Cube Singapore 188735, 
									<br />
									<br />
									Mon - Sat 12pm - 8pm';
								break;
							case BRAND_PROD_ID_PROVIDORE:
								$pick_up_details = 'Pick up your purchases at any of The Providore stores. Please go to www.theprovidore.com/store/ for address and opening hours.';
								break;
						}


						if ($voucher_brand->row()->transaction_type == 'glomp_to_fb' && $delivery_type == 'pickup') {
								$template = 'notification_glompee_pickup'; // send to glompee; pick up
								$vars['params'] = array(
									'[voucher_code]' => '<b>'. $voucher_brand->row()->voucher_code. '</b>',
									'[personal_message]' => '',//TODO
									'[merchant_product_1]' => implode(',', $prouct_merchant_name_arr),
									'[merchant_product_2]' => implode(',', $prouct_merchant_name_arr),
									'[bank_name]' => strtoupper($public_alias),
									'[recipient_fname]' => $voucher_brand->row()->from_first_name,
									'[pick_up_details]' => $pick_up_details,
								);

								//Always pick up
								if (BRAND_PROD_ID_KPO == $brand_row['brand_id'] || BRAND_PROD_ID_NASSIM_HILL == $brand_row['brand_id']) { 
									$template = 'acceptance_glompee_kpo_nassam';
									$vars['params'] = array(
										'[voucher_code]' => '<b>'. $voucher_brand->row()->voucher_code. '</b>',
										'[product_details]' => $product_details,
										'[total_sg_usd]' => 'S$ '.number_format($grand_total,2)
									);

									if (BRAND_PROD_ID_KPO == $brand_row['brand_id']) {
										$vars['params']['[merchant_name]'] = 'KPO';
									} else {
										$vars['params']['[merchant_name]'] = 'Nassim Hill';
									}

								}
						}

						//send to glompee acceptance
						$vars['template_name'] = $template;
						$vars['lang_id'] = 1;
						$this->email_templating->config($vars);
						$this->email_templating->send();

						//send merchant
						$vars = array(
							'template_name' => 'confirmation_order_erdington',
							'from_name' => SITE_DEFAULT_SENDER_NAME,
							'from_email' => SITE_DEFAULT_SENDER_EMAIL,
							'lang_id' => 1,
							'to' => 'allan.bernabe@gmail.com', //TODO: update sample only
							'params' => array(
								'[voucher_code]' => '<b>'. $voucher_brand->row()->voucher_code. '</b>',
								'[product_details]' => $product_details_merchant,
								'[total_sg]' => 'S$ '.number_format($grand_total_2,2). $delivery,
								'[0.07_total]' => 'S$ '.number_format(($grand_total_2 * 0.07),2),
								'[total_gst_plus_total]' => 'S$ '. number_format(($grand_total_2 * 0.07) + $grand_total_2, 2),
								//'[total_gst_plus_total]' => 'S$ '. number_format($grand_total_2, 2),
								'[customer_name]' => $voucher_brand->row()->first_name. ' '. $voucher_brand->row()->last_name,
								'[delivery_address]' => $voucher_brand->row()->delivery_address,
								'[contact_no]' => $voucher_brand->row()->contact_num,
							)
						);
						

						switch ($brand_row['brand_id']) {
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
								$vars['to'] = 'Cynthia.Tan@edrington.com';
								$vars['template_name'] = 'confirmation_order_erdington';
								break;
							case BRAND_PROD_ID_SWIRLS_BAKESHOP:
								$vars['to'] = 'order@swirls.com.sg';
								$vars['template_name'] = 'confirmation_order_swirls';
								$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2) . $delivery;
								break;
							case BRAND_PROD_ID_SINGBEV:
								//send email for delivery
								$vars['to'] = 'sabrina@octopusgroup.com.sg';
								$vars['template_name'] = 'confirmation_order_singbev';
								break;
							case BRAND_PROD_ID_LAURENT_PERRIER:
								//send email for delivery
								$vars['to'] = 'william@magnum.com.sg';
								$vars['template_name'] = 'confirmation_order_laurent_perrier';
								break;
							case BRAND_PROD_ID_PROVIDORE:
								//send email for delivery
								$vars['to'] = 'fiona@theprovidore.com';
								$vars['template_name'] = 'confirmation_order_providore';
								break;
							case BRAND_PROD_ID_GLENFIDDICH:
								//send email for delivery
								$vars['to'] = 'care@asherbws.com';
								$vars['template_name'] = 'confirmation_order_glenfiddich';
								break;
							case BRAND_PROD_ID_MOET_CHANDON:
								//send email for delivery
								$vars['to'] = 'care@asherbws.com';
								$vars['template_name'] = 'confirmation_order_moet_chandon';
								break;
							case BRAND_PROD_ID_KUSMI_TEA:
								//send email for delivery
								$vars['to'] = 'johanna@globalfoodconsulting.com';
								$vars['template_name'] = 'confirmation_order_kusmi_tea';
								break;
							case BRAND_PROD_ID_EURACO:
								//send email for delivery
								$vars['to'] = 'katrina@euraco.com.sg';
								$vars['template_name'] = 'confirmation_order_euraco';
								break;
							case BRAND_PROD_ID_DELIGHTS_HEAVEN:
								//send email for pick up or delivery
								$vars['to'] = 'thedelightsheaven@gmail.com';
								$vars['template_name'] = 'confirmation_delights_heaven';
								$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2) . $delivery;
								break;
							case BRAND_PROD_ID_DAILY_JUICE:
								//send email delivery is freee
								$vars['to'] = 'roger@dailyjuice.sg';
								$vars['template_name'] = 'confirmation_daily_juice';
								$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
								break;
							case BRAND_PROD_ID_KPO:
								//send email pickup only
								$vars['to'] = 'audreyliu@imaginings.com.sg';
								$vars['template_name'] = 'confirmation_order_nassam';
								$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
								$vars['params']['[merchant_name]'] = 'KPO';
								break;
							case BRAND_PROD_ID_NASSIM_HILL:
								//send email pickup only
								$vars['to'] = 'audreyliu@imaginings.com.sg';
								$vars['template_name'] = 'confirmation_order_nassam';
								$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
								$vars['params']['[merchant_name]'] = 'Nassim Hill';
								break;
						}

						$this->email_templating->config($vars);
						$this->email_templating->send();
						//send email again for this brand
						switch ($brand_row['brand_id']) {
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
								$vars['to'] = 'sulina@magnum.com.sg';
								$this->email_templating->config($vars);
								$this->email_templating->send();
								break;
						}

						//copy
						$vars['to'] = 'allan.bernabe@gmail.com';
						$this->email_templating->config($vars);
						$this->email_templating->send();

						$vars['to'] = 'z@zzo-creative.com';
						$this->email_templating->config($vars);
						$this->email_templating->send();

					}
					
					
					
					$data['success_message'] = 'Successfully save. An email has been sent to you.';
					$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
					$result = $this->db->query($sql);
				}
			}

			// get cart_details
			$sql = "SELECT gl_cart.prod_details, gl_product.prod_merchant_id
					FROM gl_cart 
					LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
					WHERE 
						session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
						public_alias = '".$public_alias."' 		
					GROUP BY gl_product.prod_merchant_id
					";
			
			$result = $this->db->query($sql);
			$cart_data="";
			foreach( $result->result() as $row)
			{
				$sql = "SELECT gl_cart.*, gl_product.prod_merchant_id
					FROM gl_cart 
					LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
					WHERE 
						session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
						public_alias = '".$public_alias."' AND		
						gl_product.prod_merchant_id = '".$row->prod_merchant_id."'
					";
				$prod_details = json_decode($row->prod_details);

				$product_list="";
				foreach($this->db->query($sql)->result() as $product)
				{
					$sql = "SELECT *
					FROM gl_cart_option
					WHERE 
						session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
						prod_id = '".$product->prod_id."' AND		
						prod_group = '".$product->prod_group."'
					";

					$option = $this->db->query($sql)->result();

					$product_list[] = array (
						'product' => $product,
						'option' => $option,
						);

				}

				$cart_data [] = array (
					'brand_id' => $prod_details->brandproduct_id,
					'brand_name' => $this->Brand->get_brandproduct($prod_details->brandproduct_id)->name,
					'prod_list' => $product_list
				);
			}

			$data['cart_data'] = $cart_data;
			$gl_voucher_brand_data = $voucher_brand->row();
			$data['gl_voucher_brand_data'] = $gl_voucher_brand_data;
			$data['public_alias'] = $public_alias;
			
			if($gl_voucher_brand_data->transaction_type =='glomp_to_friend' || $gl_voucher_brand_data->transaction_type =='glomp_to_fb')
			{
				$this->load->view( 'm/brand_public_voucher_details_hsbc_v', $data, FALSE, TRUE );
			}
			else
			{
				$data['error_404'] =  $this->lang->line('page_404_error');
				$this->load->view('404_v',$data);
			}
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v',$data);
		}
	}
	private function send_email_to_merchants($group_voucher_id="")
	{
	
		$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			$gl_voucher_brand_data = $result->row();
			
			
			$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$gl_voucher_brand_data->session_cart_id."' ";
			$gl_cart_data =  $result = $this->db->query($sql);
			/*send a confirmation_order_macallan email start*/ 
			/*send a confirmation_order_macallan email*/
			/*send a confirmation_order_macallan email*/
			/*send a confirmation_order_macallan email*/
			
			$table="";
			$brandproduct_id = 22;
			$sub_total = 0;
			
			foreach($gl_cart_data->result() as $row)
			{
				$product = json_decode($row->prod_details);
				
				if($product->brandproduct_id == MACALLAN_BRAND_PRODUCT_ID)
				{
					$table .='
						<tr>
							<td><b>'.$product->prod_name.'</b><br>'.$product->merchant_name.'</td>
							<td align="right">$ '.number_format(($row->prod_cost ),2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($row->prod_cost) * ($row->prod_qty),2).'</td>
						</tr>
					';
					$sub_total += ( ($row->prod_cost ) * $row->prod_qty);
				}
			}
			
			if($table!="")
			{
				$product_details ='
					<table width="100%" cellpadding="2" cellspacing="2" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table.'
					</table>
				';
				
				$customer_name  ='<b>'.($gl_voucher_brand_data->first_name).' '.($gl_voucher_brand_data->last_name).'</b>';
				$delivery_address = '<b>'.($gl_voucher_brand_data->delivery_address).', '.($gl_voucher_brand_data->delivery_postcode).'</b>';
				$contact_no = '<b>'.($gl_voucher_brand_data->contact_num).'</b>';
				$total_sg = '<b>S$ '.number_format($sub_total,2).'</b>';
				$total_07 = '<b>S$ '.number_format(($sub_total * 0.07),2).'</b>';
				
				$grand_total = ($sub_total) + ($sub_total * 0.07);
				$total_gst_plus_total = '<b>S$ '.number_format($grand_total,2).'</b>';
					
					
				$email_subject = 'Order confirmation - '.$group_voucher_id;
				$vars = array(
					'template_name' => 'confirmation_order_macallan',
					'from_name' => SITE_DEFAULT_SENDER_NAME,
					'from_email' => SITE_DEFAULT_SENDER_EMAIL,
					'lang_id' => 1,
					'to' => MACALLAN_EMAIL_1,
					'params' => array(
						'[customer_name]' => $customer_name,
						'[delivery_address]' => $delivery_address,
						'[contact_number]' => $contact_no,
						'[product_details]' => $product_details,
						'[total_sg]' => $total_sg,
						'[0.07_total]' => $total_07,
						'[total_gst_plus_total]' => $total_gst_plus_total,
					)
				);
				$this->email_templating->config($vars);
				$this->email_templating->set_subject($email_subject);
				$this->email_templating->send();
				
				$vars['to'] =MACALLAN_EMAIL_2;
				$this->email_templating->config($vars);
				$this->email_templating->set_subject($email_subject);
				$this->email_templating->send();
			}
			/*send a confirmation_order_macallan email*/
			/*send a confirmation_order_macallan email*/
			
			
			
			
			/*send a confirmation_order_winestore email  start*/
			/*send a confirmation_order_winestore email  start*/
			/*send a confirmation_order_winestore email  start*/
			$table="";
			$sub_total = 0;
			
			foreach($gl_cart_data->result() as $row)
			{
				$product = json_decode($row->prod_details);
				
				if($product->brandproduct_id == WINESTORE_BRAND_PRODUCT_ID)
				{
					$table .='
						<tr>
							<td><b>'.$product->prod_name.'</b><br>'.$product->merchant_name.'</td>
							<td align="right">$ '.number_format(($row->prod_cost),2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($row->prod_cost) * ($row->prod_qty),2).'</td>
						</tr>
					';
					$sub_total += ( ($row->prod_cost ) * $row->prod_qty);
				}
			}
			if($table!="")
			{
				$product_details ='
					<table width="100%" cellpadding="2" cellspacing="2" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table.'
					</table>
				';
				
				$customer_name  ='<b>'.($gl_voucher_brand_data->first_name).' '.($gl_voucher_brand_data->last_name).'</b>';
				$delivery_address = '<b>'.($gl_voucher_brand_data->delivery_address).', '.($gl_voucher_brand_data->delivery_postcode).'</b>';
				$contact_no = '<b>'.($gl_voucher_brand_data->contact_num).'</b>';
				$total_sg = '<b>S$ '.number_format($sub_total,2).'</b>';
					
				$email_subject = 'Order confirmation - '.$group_voucher_id;
				$vars = array(
					'template_name' => 'confirmation_order_winestore',
					'from_name' => SITE_DEFAULT_SENDER_NAME,
					'from_email' => SITE_DEFAULT_SENDER_EMAIL,
					'lang_id' => 1,
					'to' => WINESTORE_EMAIL_1,
					'params' => array(
						'[customer_name]' => $customer_name,
						'[delivery_address]' => $delivery_address,
						'[contact_number]' => $contact_no,
						'[product_details]' => $product_details,
						'[total]' => $total_sg
					)
				);
				$this->email_templating->config($vars);
				$this->email_templating->set_subject($email_subject);
				$this->email_templating->send();
				
				$vars['to'] =WINESTORE_EMAIL_2;
				$this->email_templating->config($vars);
				$this->email_templating->set_subject($email_subject);
				$this->email_templating->send();
			
			}
			
			/*send a confirmation_order_winestore email*/
			/*send a confirmation_order_winestore email*/
			/*send a confirmation_order_winestore email*/
		}
	}
	public function success_checkout($group_voucher_id="")
	{
		$sql = "SELECT * FROM gl_voucher_brand WHERE group_voucher_id = '".$group_voucher_id."' ";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			$gl_voucher_brand_data = $result->row();
			$data['gl_voucher_brand_data'] = $gl_voucher_brand_data;
			
			
			$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$gl_voucher_brand_data->session_cart_id."' ";
			$result = $this->db->query($sql);
			if ($result->num_rows() > 0)
			{
				$data['cart_data'] = $result->result();
			}
			
			
			if( strpos( uri_string(), 'm/' ) !== false )
			{
				$this->load->view( 'm/brand_public_return_checkout_v', $data, FALSE, TRUE );
			}
			else
			{
				$this->load->view('brand_public_return_checkout_v', $data);
			}
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v',$data);
		}
	}
	public function cart($alias="")
	{
		if($alias=="")
		{
			$data['referrer'] =site_url()."brands/view/amex";
			$alias = "amex";			
		}
		else{
			$data['referrer'] =site_url()."brands/view/".$alias;			
		}
		$data['winery_count'] =0;
		if($alias=="amex")
			$data['winery_count'] =$this->check_winery($this->session->userdata('session_cart_id'));	
		
		$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			//$data['referrer'] = $this->agent->referrer();
		}
		$data['cart_data'] ='';
		$data['public_alias'] =$alias;

		
		if($this->session->userdata('session_cart_id') != "")
		{

			if($alias=="hsbc" || $alias=="uob" || $alias == 'dbs' || $alias == 'amex')
			{

				switch ($alias) {
				    case 'hsbc':
				        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
				        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  HSBC_BRAND_PROD_ID_KPO;
				        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        break;
				    case 'uob':
				        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
				        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  UOB_BRAND_PROD_ID_KPO;
				        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        break;
			        case 'dbs':
				        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
				        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  DBS_BRAND_PROD_ID_KPO;
				        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        break;

			        case 'amex':
				        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
				        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  AMEX_BRAND_PROD_ID_KPO;
				        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
				        break;
				}

				define('BRAND_PROD_ID_MACALLAN',$macallan);
				define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
				define('BRAND_PROD_ID_LONDON_DRY',$london);

		
				// get cart_details
				$sql = "SELECT *,
						CASE brandproduct_id
							WHEN '".BRAND_PROD_ID_MACALLAN."' THEN 0
							WHEN '".BRAND_PROD_ID_SNOW_LEOPARD."' THEN 0
							WHEN '".BRAND_PROD_ID_LONDON_DRY."' THEN 0
							ELSE brandproduct_id
						END as group_brandproduct_id
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
							public_alias = '".$alias."' 		
						GROUP BY group_brandproduct_id
						";
				
				$result = $this->db->query($sql);
				$cart_data="";
				foreach( $result->result() as $row)
				{
					if($row->group_brandproduct_id==0)
					{
						$group_brand_array = BRAND_PROD_ID_MACALLAN.','.BRAND_PROD_ID_SNOW_LEOPARD.','.BRAND_PROD_ID_LONDON_DRY;
						$sql = "SELECT *
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
							public_alias = '".$alias."' AND		
							brandproduct_id IN (".$group_brand_array.") 
						";
					}
					else
					{
						$sql = "SELECT *
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
							public_alias = '".$alias."' AND		
							brandproduct_id = '".$row->brandproduct_id."'
						";
					}					
					$prod_details = json_decode($row->prod_details);
					$product_list="";
					foreach($this->db->query($sql)->result() as $product)
					{
						$sql = "SELECT *
						FROM gl_cart_option
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
							prod_id = '".$product->prod_id."' AND		
							prod_group = '".$product->prod_group."'
						";

						$option = $this->db->query($sql)->result();

						$product_list[] = array (
							'product' => $product,
							'option' => $option,
							);

					}

					$cart_data [] = array (
						'brand_id' => $prod_details->brandproduct_id,
						'brand_name' => $this->Brand->get_brandproduct($prod_details->brandproduct_id)->name,
						'prod_list' => $product_list
					);
				
				}
				
				//if ($result->num_rows() > 0)
				{
					$data['cart_data'] = $cart_data;
				}

			}
			/*
			else if($alias=="amex")
			{
				// get cart_details
				$sql = "SELECT * 
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
							public_alias = '".$alias."' 
						";
				$result = $this->db->query($sql);
				if ($result->num_rows() > 0)
				{
					$data['cart_data'] = $result->result();
				}

			}
			*/			
		}
		
		//asdasd
        //log campaign
        $this->log_amex_action('viewed_cart_page',FALSE, $alias);
        //log campaign
        
		if( strpos( uri_string(), 'm/' ) !== false )
		{
			if($alias=="hsbc" || $alias=="uob" || $alias=="dbs" || $alias=="amex")
			{
				$this->load->view('m/brand_public_cart_hsbc_v', $data);
				return;	
			}
			$this->load->view( 'm/brand_public_cart_v', $data, FALSE, TRUE );
		}
		else
		{
			if($alias=="hsbc" || $alias=="uob" || $alias=="dbs" || $alias=="amex" )
			{
				$this->load->view('brand_public_cart_hsbc_v', $data);
				return;	
			}
			$this->load->view('brand_public_cart_v', $data);
		}
	
	}
	public function check_winery($session_cart_id="")
	{
		//check winery discount
		//check winery discount
		$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$session_cart_id."' ";
        $result = $this->db->query($sql);
		$winery_count =0;
		foreach( $result->result() as $row )
		{
			//count winery products
			if(get_winestore_prod_data($row->prod_id) !=false)
			{
				$winery_count+=$row->prod_qty;
			}
		}
		if($winery_count >=4)
		{
			foreach( $result->result() as $row )
			{
				$d = get_winestore_prod_data($row->prod_id);
				if($d !=false)
				{
					
					$where = array(
					'id' => $row->id
					);
			
					$update_data = array('prod_cost' => ( $d['a_30']));
					$this->db->update('gl_cart', $update_data, $where);
				}
			
			}
		}
		else if($winery_count >=2)
		{
			foreach( $result->result() as $row )
			{
				$d = get_winestore_prod_data($row->prod_id);
				if($d !=false)
				{
					
					$where = array(
					'id' => $row->id
					);
			
					$update_data = array('prod_cost' => ( $d['a_20']));
					$this->db->update('gl_cart', $update_data, $where);
				}
			
			}
		}
		else if($winery_count ==1)
		{
			foreach( $result->result() as $row )
			{
				$d = get_winestore_prod_data($row->prod_id);
				if($d !=false)
				{
					
					$where = array(
					'id' => $row->id
					);
			
					$update_data = array('prod_cost' => ( $d['base_price']));
					$this->db->update('gl_cart', $update_data, $where);
				}
			
			}
		}
		return $winery_count;
		
		//echo $winery_count;
		//check winery discount
		//check winery discount
	}
	private function get_default_delivery_type($brandproduct_id)
	{
		switch ($brandproduct_id) {
			case HSBC_BRAND_PROD_ID_KPO:
			case HSBC_BRAND_PROD_ID_NASSIM_HILL:
			case UOB_BRAND_PROD_ID_KPO:
			case UOB_BRAND_PROD_ID_NASSIM_HILL:
				$delivery_type ='pickup';
				break;
			default:
				$delivery_type ='delivery';
				break;
		}
		return $delivery_type;
		
	}
	private function add_to_cart_database($params)
	{
		// first check if session and item already exists on the database.
		$delivery_type	= $this->get_default_delivery_type($params['brandproduct_id']);

		$sql = "SELECT * 
				FROM gl_cart 
				WHERE 
					session_cart_id = '".$params['session_cart_id']."' AND 
					prod_id='".$params['prod_id']."' AND 
					public_alias='".$params['public_alias']."' 
				";
        $result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			if($params['flavors_data'] =="") // sabe ni allan			
			{
				// just update the qty	
				$where = array(
	            'id' => $result->row()->id
				);
				
				$new_qty = $result->row()->prod_qty + $params['qty'];
				
				if($new_qty>10)
					$new_qty=10;
				
				
				$update_data = array('prod_qty' => ( $new_qty));
				$this->db->update('gl_cart', $update_data, $where);
				//echo "update";

			}
			else
			{
				/*
				1. loop sa lahat ng product cart items
				2. on each loop get product options
				3. compare sa params options.
				4. if may found, update product cart. if no, insert new data
				*/
				$found = false;
				foreach( $result->result() as $row )
				{

					$sql = "SELECT * 
							FROM gl_cart_option
							WHERE 
								session_cart_id = '".$params['session_cart_id']."' AND 
								prod_id='".$params['prod_id']."' AND
								prod_group='".$row->prod_group."' ";
		    		$cart_option = $this->db->query($sql);
		    		$flavors_data = json_decode($params['flavors_data']);
		    		$flavors_count = count($flavors_data);

		    		$same_options = false; 
		    		if($cart_option->num_rows()  == $flavors_count )
		    		{
		    			$same_options = true; 
						foreach( $cart_option->result() as $option )
						{
							$option_found= false;
							foreach($flavors_data as $flavor)
							{
								if (
										$option->option_type == 'flavor' &&
										$option->option_value == $flavor->name &&
										$option->option_qty == $flavor->qty
									)
								{
									$option_found = true;
								}

							}
							if(!$option_found)
							{
								$same_options = false;
								break;
							}
						}
		    		}
		    		if($same_options)
		    		{
	    				$found = $row;
	    				break;
		    		}
				}

				if($found === false)
				{

					// insert new product and option
					//get max group id
					$group_id = 1;
					$sql = "SELECT MAX(prod_group) as m
							FROM gl_cart
							WHERE 
								session_cart_id = '".$params['session_cart_id']."' AND 
								prod_id='".$params['prod_id']."'
								";
					$result = $this->db->query($sql);
					if($result->num_rows() > 0)
					{
						$group_id = $result->row()->m +1;
					}
					//get max group id



					//insert new data
					$insert_data = array(
						'session_cart_id'	=> $params['session_cart_id'],
						'prod_id' 			=> $params['prod_id'],
						'prod_qty' 			=> $params['qty'],
						'prod_cost'			=> $params['prod_merchant_cost'],
						'prod_details' 		=> $params['prod_data'],
						'prod_group'		=> $group_id,
						'public_alias' 		=> $params['public_alias'],
						'delivery_type' 	=> $delivery_type,
						'brandproduct_id'	=> $params['brandproduct_id']
					);
					
					$this->db->insert('gl_cart', $insert_data);

					foreach($flavors_data as $flavor)
					{
						//insert new data
						//print_r($flavor);
						$insert_data = array (
							'session_cart_id'	=> $params['session_cart_id'],
							'prod_id' 			=> $params['prod_id'],
							'option_type' 		=>'flavor',
							'option_value'		=> $flavor->name,
							'option_qty' 		=> $flavor->qty,
							'prod_group'		=> $group_id
						);
						$this->db->insert('gl_cart_option', $insert_data);

					}

					// insert new product and option

				}
				else
				{

					// update product and option
					$where = array(
		            'id' => $found->id
					);
					
					$new_qty = $found->prod_qty + $params['qty'];
					
					if($new_qty>10)
						$new_qty=10;
					
					
					$update_data = array('prod_qty' => ( $new_qty));
					$this->db->update('gl_cart', $update_data, $where);
					// update product and option

				}
			}
		}
		else
		{
			//insert new data
			$insert_data = array(
				'session_cart_id'	=> $params['session_cart_id'],
				'prod_id' 			=> $params['prod_id'],
				'prod_qty' 			=> $params['qty'],
				'prod_cost'			=> $params['prod_merchant_cost'],
				'prod_details' 		=> $params['prod_data'],
				'prod_group'		=> 1,
				'public_alias' 		=> $params['public_alias'],
				'delivery_type' 	=> $delivery_type,
				'brandproduct_id'	=> $params['brandproduct_id']
			);			
			$this->db->insert('gl_cart', $insert_data);
			//echo "insert";

			if($params['flavors_data'] !="")
			{
				$flavors_data = json_decode($params['flavors_data']);
				foreach($flavors_data as $flavor)
				{
					//insert new data
					//print_r($flavor);
					$insert_data = array (
						'session_cart_id'	=> $params['session_cart_id'],
						'prod_id' 			=> $params['prod_id'],
						'option_type' 		=>'flavor',
						'option_value'		=> $flavor->name,
						'option_qty' 		=> $flavor->qty,
						'prod_group'		=> 1,						
					);
					$this->db->insert('gl_cart_option', $insert_data);

				}
			}











		}
		
        //log campaign
        $details = array('name' => 'prod_id','value' => $params['prod_id']);
        $this->log_amex_action('added_to_cart', $details, $params['public_alias']);
        //log campaign
		
		$this->check_winery($params['session_cart_id']);
		
		
		
		//count item qty
		$sql = "SELECT 
				SUM(prod_qty) as qty 
				FROM gl_cart 
				WHERE 
					session_cart_id = '".$params['session_cart_id']."' AND
					public_alias = '".$params['public_alias']."' 
				";
        $result = $this->db->query($sql);
		return $result->row()->qty;
	
	}
	public function remove_cart_item()
	{
		if($_POST)
		{
			$prod_id = $this->input->post('prod_id');
			if($this->session->userdata('session_cart_id') != "")
			{
				$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$this->session->userdata('session_cart_id')."' AND prod_id='".$prod_id."' ";
				$result = $this->db->query($sql);
				if ($result->num_rows() > 0)
				{
					// just delete
					$this->db->delete('gl_cart',array('session_cart_id'=>$this->session->userdata('session_cart_id'), 'prod_id'=>$prod_id, ));
					//echo "update";
				}
				$grand_total = 0;
				$grand_total_usd = 0;
				$cart_qty =0;
				$sql = "SELECT prod_cost, prod_qty  FROM gl_cart WHERE session_cart_id = '".$this->session->userdata('session_cart_id')."' ";
				$result = $this->db->query($sql);
				foreach( $result->result() as $row ) {
					$grand_total += ( ($row->prod_cost) * $row->prod_qty);
					$grand_total_usd += ( ($row->prod_cost) * $row->prod_qty);
					$cart_qty +=$row->prod_qty;
				}
				
				$cart_session_data = array('session_cart_qty'=> $cart_qty);
				$this->session->set_userdata($cart_session_data);
				echo json_encode(array(
					'status' 		=> 'success',
					'cart_qty' 		=> $cart_qty,
					'grand_total' 	=> number_format(($grand_total),2),
					'grand_total_usd' 	=> number_format(($grand_total_usd*BRAND_FOREX_AMEX),2)
					));
				return;
				
			}
		}
	
	}
	public function compute_grand_total()
	{
		if($_POST)
		{
			$brands = json_decode($this->input->post('brands'));
			$public_alias = $this->input->post('public_alias');

			switch ($public_alias) {
				    case 'hsbc':
				        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
				        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  HSBC_BRAND_PROD_ID_KPO;
				        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona = '0';
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $glenfiddich =  '0';
				        $moet_chandon =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
				    case 'uob':
				        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
				        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  UOB_BRAND_PROD_ID_KPO;
				        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $glenfiddich =  '0';
				        $moet_chandon =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
			        case 'dbs':
				        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
				        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  DBS_BRAND_PROD_ID_KPO;
				        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
				        $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
				        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
				        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
				        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
				        $euraco = DBS_BRAND_PROD_ID_EURACO;
				        break;

			        case 'amex':
				        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
				        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  AMEX_BRAND_PROD_ID_KPO;
				        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $moet_chandon =  '0';
				        $glenfiddich =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
				}

				define('BRAND_PROD_ID_MACALLAN',$macallan);
				define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
				define('BRAND_PROD_ID_LONDON_DRY',$london);

				define('BRAND_PROD_ID_SINGBEV',$singbev);
				define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
				define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

				define('BRAND_PROD_ID_KPO',$kpo);
				define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
				define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
				define('BRAND_PROD_ID_VALRHONA',$valrhona);
				define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
				define('BRAND_PROD_ID_PROVIDORE',$providore);
				define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
				define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
				define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
				define('BRAND_PROD_ID_EURACO', $euraco);

				$grand_total = 0;
				$grand_total_usd = 0;
				$delivery_total =0;
				$per_brand = array();
				

				$sql = "SELECT prod_cost, prod_qty, prod_details
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
								public_alias ='".$public_alias."'
								";
					$result = $this->db->query($sql);
					foreach( $result->result() as $row ) {
						$grand_total += ( ($row->prod_cost) * $row->prod_qty);
						$grand_total_usd += ( ($row->prod_cost ) * $row->prod_qty);
						$brand_details = json_decode($row->prod_details, TRUE);
						$per_brand[$brand_details['brandproduct_id']][] = array('prod_qty' => $row->prod_qty, 'prod_cost' => ($row->prod_cost) * $row->prod_qty);
					}
			foreach( $brands as $b )
			{
					$per_brand_total = 0;
					$per_brand_qty = 0;
					//print_r($per_brand[$b->brand_id]);
					//get per brand total
					foreach($per_brand[$b->brand_id] as $_b) {
						$per_brand_total += $_b['prod_cost'] * $_b['prod_qty']; 
						$per_brand_qty += $_b['prod_qty']; 
					}

					$where = array(
						'session_cart_id' => $this->session->userdata('session_cart_id'),
						'brandproduct_id' => $b->brand_id,
					);
					$update_data = array('delivery_type' => $b->delivery_type);
					$this->db->update('gl_cart', $update_data, $where);



				$has_delivery = 0;						
				switch ($b->brand_id) {
					case BRAND_PROD_ID_MACALLAN:
					case BRAND_PROD_ID_SNOW_LEOPARD:
					case BRAND_PROD_ID_LONDON_DRY:
						break;
					case BRAND_PROD_ID_SWIRLS_BAKESHOP:
						$has_delivery = 16;
						break;
					case BRAND_PROD_ID_SINGBEV:
						$has_delivery = 35;
						break;
					case BRAND_PROD_ID_DELIGHTS_HEAVEN:
						$has_delivery = 25;
						break;
					case BRAND_PROD_ID_VALRHONA:
						$has_delivery = 35;
						break;
					case BRAND_PROD_ID_LAURENT_PERRIER:
						if($per_brand_qty < 3)
						{
							$has_delivery = 20;
						}	
						break;
					case BRAND_PROD_ID_PROVIDORE:
						if($per_brand_total < 200)
						{
							$has_delivery = 20;
						}	
						break;
					case BRAND_PROD_ID_GLENFIDDICH:
						$has_delivery = 0;
						break;						
				}
				if($has_delivery > 0 && $b->delivery_type=='delivery')
				{
					$delivery_total += $has_delivery;
				}
			}
			echo json_encode(array(					
					'status' 		=> 'success',
					'sub_total' 	=> number_format($grand_total + $delivery_total,2),
					'gst' 	=> number_format($grand_total * 0.07,2),
					'grand_total' 	=> number_format(($grand_total * 0.07) + $grand_total + $delivery_total,2),
					'grand_total_usd' 	=> number_format((($grand_total + $delivery_total)*BRAND_FOREX_AMEX),2),
					));
				return;
		}
	}
	public function update_cart()
	{
		if($_POST)
		{
			$prod_id = $this->input->post('prod_id');
			$qty = $this->input->post('qty');
			$prod_merchant_cost = $this->input->post('prod_merchant_cost');
			$public_alias = $this->input->post('public_alias');
			$prod_group = $this->input->post('prod_group');
			$brand_id = $this->input->post('brand_id');

			switch ($public_alias) {
				    case 'hsbc':
				        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
				        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  HSBC_BRAND_PROD_ID_KPO;
				        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona = '0';
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $glenfiddich =  '0';
				        $moet_chandon =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
				    case 'uob':
				        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
				        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  UOB_BRAND_PROD_ID_KPO;
				        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $glenfiddich =  '0';
				        $moet_chandon =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
			        case 'dbs':
				        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
				        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  DBS_BRAND_PROD_ID_KPO;
				        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
				        $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
				        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
				        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
				        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
				        $euraco = DBS_BRAND_PROD_ID_EURACO;
				        break;
			        case 'amex':
				        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
				        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
				        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
				        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
				        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
				        $kpo =  AMEX_BRAND_PROD_ID_KPO;
				        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
				        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
				        $laurent_perrier =  '0';
				        $providore =  '0';
				        $glenfiddich =  '0';
				        $moet_chandon =  '0';
				        $kusmi_tea =  '0';
				        $euraco = '0';
				        break;
				}

			define('BRAND_PROD_ID_MACALLAN',$macallan);
			define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
			define('BRAND_PROD_ID_LONDON_DRY',$london);

			define('BRAND_PROD_ID_SINGBEV',$singbev);
			define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
			define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

			define('BRAND_PROD_ID_KPO',$kpo);
			define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
			define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
			define('BRAND_PROD_ID_VALRHONA',$valrhona);
			define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
			define('BRAND_PROD_ID_PROVIDORE',$providore);
			define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
			define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
			define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
			define('BRAND_PROD_ID_EURACO', $euraco);


			if($this->session->userdata('session_cart_id') != "")
			{
				$sql = "SELECT * 
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND 
							prod_id='".$prod_id."' AND 
							prod_group='".$prod_group."' AND
							public_alias ='".$public_alias."'
							 ";
				$result = $this->db->query($sql);
				if ($result->num_rows() > 0)
				{
					// just update the qty	
					$where = array(
					'id' => $result->row()->id
					);
					$new_qty = $qty;
					if($new_qty>10)
						$new_qty=10;
					
					if($new_qty==0)
					{
						$this->db->delete('gl_cart',array('session_cart_id'=>$this->session->userdata('session_cart_id'), 'prod_id'=>$prod_id, 'prod_group' => $prod_group ));
						$this->db->delete('gl_cart_option',array('session_cart_id'=>$this->session->userdata('session_cart_id'), 'prod_id'=>$prod_id, 'prod_group' => $prod_group ));
					}
					else
					{
						$update_data = array('prod_qty' => ( $new_qty));
						$this->db->update('gl_cart', $update_data, $where);
						//echo "update";
					}
					
				}
				if($public_alias=='amex')
					$winery_count = $this->check_winery($this->session->userdata('session_cart_id'));
				else
					$winery_count =0;
				
				//winery_data
				//winery_data
				$winery_data = "";
				$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$this->session->userdata('session_cart_id')."' ";
				$result = $this->db->query($sql);
				foreach( $result->result() as $row )
				{
					//count winery products
					if(get_winestore_prod_data($row->prod_id) !=false)
					{
						$winery_data[] = array(
							'prod_id' => $row->prod_id,
							'prod_cost' => number_format(($row->prod_cost),2),
							'prod_total' => number_format(($row->prod_cost * $row->prod_qty),2),
						);
					}
				}
				
				//winery_data
				//winery_data
				
				$sql = "SELECT * 
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$this->session->userdata('session_cart_id')."' AND 
							prod_id='".$prod_id."' AND
							prod_group='".$prod_group."'
							 ";
				$result = $this->db->query($sql);
				if ($result->num_rows() > 0)
				{
					$result = $result->row();
					$prod_merchant_cost = $result->prod_cost;
				}
				
				
				$grand_total = 0;
				$grand_total_usd = 0;
				$delivery_total =0;
				$cart_qty =0;
				$brand_eror='';
				$singbev_del_charge = -1;
				$valrhona_del_charge = -1;
				$laurent_perrier_del_charge = -1;
				$providore_del_charge = -1;
				$glenfiddich_del_charge = 0;
				$moet_chandon_del_charge = 0;
				$kusmi_tea_del_charge = 0;
				$euraco_del_charge = -1;

				if($public_alias=="hsbc" || $public_alias=="uob" || $public_alias=="dbs")
				{
					$accumulated_300 = false;
					$sql = "SELECT SUM(prod_cost * prod_qty) AS total
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
								public_alias = '".$public_alias."' AND
								(
									brandproduct_id = '".BRAND_PROD_ID_MACALLAN."' OR
									brandproduct_id = '".BRAND_PROD_ID_SNOW_LEOPARD."' OR
									brandproduct_id = '".BRAND_PROD_ID_LONDON_DRY."'
								)
							";
					$result = $this->db->query($sql);

					if($result->row()->total >= 300)
					{
						$accumulated_300 = true;
					}

					// get cart_details
					$sql = "SELECT gl_cart.prod_details, gl_product.prod_merchant_id
							FROM gl_cart 
							LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
							WHERE 
								session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
								public_alias = '".$public_alias."' 		
							GROUP BY gl_product.prod_merchant_id
							";
					
					$result = $this->db->query($sql);
					$cart_data="";
					foreach( $result->result() as $row)
					{
						/*for each merhcant*/
						$per_brand_total =0;
						$sql = "SELECT gl_cart.*, gl_product.prod_merchant_id
							FROM gl_cart 
							LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
							WHERE 
								session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
								public_alias = '".$public_alias."' AND		
								gl_product.prod_merchant_id = '".$row->prod_merchant_id."'
							";
						$prod_details = json_decode($row->prod_details);
						

						$product_list="";
						foreach($this->db->query($sql)->result() as $product)
						{
							/*for each product*/
							$per_brand_total += ( ($product->prod_cost) * $product->prod_qty);
							$cart_qty +=$product->prod_qty;
							/*for each product*/

						}
						$has_delivery = false;						
						switch ($prod_details->brandproduct_id) {
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
									if($per_brand_total<300)
									{
										if($prod_details->brandproduct_id == $brand_id)
										{
											if($accumulated_300 === false)
											{
												$brand_eror = "
													Orders for combined orders of The Macallan whiskies, Snow Leopold Vodka and No.3 London Dry Gin should be S$300 and above to select Delivery.<br>
													Please order more items or change your Delivery Type to <b>For Pick Up</b>.
													";
											}
										}										
									}
								break;
						/*for each merhcant*/
						}
					}

					$brands = json_decode($this->input->post('brands'));
					

					
					

						$sql = "SELECT prod_cost, prod_qty 
									FROM gl_cart 
									WHERE 
										session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
										public_alias ='".$public_alias."'
										";
							$result = $this->db->query($sql);
							foreach( $result->result() as $row ) {
								$grand_total += ( ($row->prod_cost) * $row->prod_qty);
								$grand_total_usd += ( ($row->prod_cost ) * $row->prod_qty);
							}
					foreach( $brands as $b )
					{

						$has_delivery = 0;						
						switch ($b->brand_id) {
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
								break;
							case BRAND_PROD_ID_SWIRLS_BAKESHOP:
								$has_delivery = 16;
								break;
							case BRAND_PROD_ID_SINGBEV:
								$has_delivery = 35;
								break;
							case BRAND_PROD_ID_DELIGHTS_HEAVEN:
								$has_delivery = 25;
								break;
							case BRAND_PROD_ID_VALRHONA:
								$has_delivery = 35;
								break;
							case BRAND_PROD_ID_LAURENT_PERRIER:
								$has_delivery = 20;
								break;
							case BRAND_PROD_ID_PROVIDORE:
								$has_delivery = 20;
								break;
							case BRAND_PROD_ID_MOET_CHANDON:
								$has_delivery = 0;
								break;
							case BRAND_PROD_ID_KUSMI_TEA:
								$has_delivery = 0;
								break;
							case BRAND_PROD_ID_EURACO:
								$has_delivery = 30;
								break;
						}
						if($has_delivery > 0 && $b->delivery_type=='delivery')
						{
							/*check if there are items left under this brand*/
							$sql = "SELECT id, SUM(prod_cost * prod_qty) AS total, SUM(prod_qty) AS total_prod_qty  
									FROM gl_cart 
									WHERE 
										session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
										public_alias ='".$public_alias."' AND
										brandproduct_id ='".$b->brand_id."' 
										";
							$result = $this->db->query($sql);
							if($result->row()->total>0)
							{
								
								$t = (int) $result->row()->total;
								$prod_qty = (int) $result->row()->total_prod_qty;
								if($b->brand_id == BRAND_PROD_ID_SINGBEV &&  $t > 350)
								{
									$has_delivery =0;
									$singbev_del_charge = 0;

								}
								else if ($b->brand_id == BRAND_PROD_ID_SINGBEV)
								{
									$singbev_del_charge = number_format(( ($has_delivery) ),2);
								}


								else if($b->brand_id == BRAND_PROD_ID_VALRHONA &&  $t > 300)
								{
									$has_delivery =0;
									$valrhona_del_charge = 0;

								}
								else if ($b->brand_id == BRAND_PROD_ID_VALRHONA)
								{
									$valrhona_del_charge =number_format(( ($has_delivery) ),2);
								}

								//laurent perrier
								else if($b->brand_id == BRAND_PROD_ID_LAURENT_PERRIER &&  $prod_qty >= 3)
								{
									$has_delivery =0;
									$laurent_perrier_del_charge = 0;

								}
								else if ($b->brand_id == BRAND_PROD_ID_LAURENT_PERRIER)
								{
									$laurent_perrier_del_charge =number_format(( ($has_delivery) ),2);
								}

								//providore
								else if($b->brand_id == BRAND_PROD_ID_PROVIDORE &&  $t >= 200)
								{
									$has_delivery =0;
									$providore_del_charge = 0;

								}
								else if ($b->brand_id == BRAND_PROD_ID_PROVIDORE)
								{
									$providore_del_charge =number_format(( ($has_delivery) ),2);
								}

								//euraco
								else if($b->brand_id == BRAND_PROD_ID_EURACO &&  $t >= 300)
								{
									$has_delivery =0;
									$euraco_del_charge = 0;

								}
								else if ($b->brand_id == BRAND_PROD_ID_EURACO)
								{
									$euraco_del_charge =number_format(( ($has_delivery) ),2);
								}

								$delivery_total += $has_delivery;	
							}							
						}
					}

					$haystack = array(BRAND_PROD_ID_MACALLAN,BRAND_PROD_ID_SNOW_LEOPARD,BRAND_PROD_ID_LONDON_DRY);
					if( in_array($brand_id, $haystack) && $qty==0 && $brand_eror=='')
					{
						if($accumulated_300 === false)
						{
							$brand_eror = "
								Orders for <b>".$this->Brand->get_brandproduct($brand_id)->name."</b> should be atleas S$300 to accept <b>Delivery</b>.<br>
								Please order more items or change your Delivery Type to <b>For Pick Up</b>.
								";
						}
					}

				}
				else
				{
					$sql = "SELECT prod_cost, prod_qty 
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$this->session->userdata('session_cart_id')."' AND
								public_alias ='".$public_alias."'
								";
					$result = $this->db->query($sql);
					foreach( $result->result() as $row ) {
						$grand_total += ( ($row->prod_cost) * $row->prod_qty);
						$grand_total_usd += ( ($row->prod_cost ) * $row->prod_qty);
						$cart_qty +=$row->prod_qty;
					}
				}
				
				$cart_session_data = array ('session_cart_qty_'.$public_alias=> $cart_qty);

				$this->session->set_userdata($cart_session_data);

				echo json_encode(array(
					'status' 		=> 'success',
					'cart_qty' 		=> $cart_qty,
					'prod_cost'  		=> number_format(( ($prod_merchant_cost) ),2),
					'total'  		=> number_format(( ($prod_merchant_cost) * $qty),2),
					'sub_total' 	=> number_format($grand_total + $delivery_total,2),
					'gst' 	=> number_format($grand_total * 0.07,2),
					'grand_total' 	=> number_format(($grand_total * 0.07) + $grand_total + $delivery_total,2),
					'grand_total_usd' 	=> number_format((($grand_total+ $delivery_total)*BRAND_FOREX_AMEX),2),
					'winery_count' 	=> $winery_count,
					'winery_data' 	=> $winery_data,
					'brand_eror' 	=> $brand_eror,
					'singbev' 	=> array('brand_id' => BRAND_PROD_ID_SINGBEV, 'del_charge' => $singbev_del_charge),
					'valrhona' 	=> array('brand_id' => BRAND_PROD_ID_VALRHONA, 'del_charge' => $valrhona_del_charge),
					'laurent_perrier' 	=> array('brand_id' => BRAND_PROD_ID_LAURENT_PERRIER, 'del_charge' => $laurent_perrier_del_charge),
					'providore' 	=> array('brand_id' => BRAND_PROD_ID_PROVIDORE, 'del_charge' => $providore_del_charge),
					'glenfiddich' 	=> array('brand_id' => BRAND_PROD_ID_GLENFIDDICH, 'del_charge' =>$glenfiddich_del_charge),
					'moet_chandon' 	=> array('brand_id' => BRAND_PROD_ID_MOET_CHANDON, 'del_charge' =>$moet_chandon_del_charge),
					'kusmi_tea' 	=> array('brand_id' => BRAND_PROD_ID_KUSMI_TEA, 'del_charge' => $kusmi_tea_del_charge),
					'euraco' 	=> array('brand_id' => BRAND_PROD_ID_EURACO, 'del_charge' => $euraco_del_charge),
					
					));
				return;
				
			}
		}
		
	}
	public function add_to_cart()
	{
		if($_POST)
		{
			$prod_id = $this->input->post('prod_id');
			$qty = $this->input->post('qty');
			$prod_merchant_cost = $this->input->post('prod_merchant_cost');
			$prod_data = $this->input->post('prod_data');
			$brandproduct_id = $this->input->post('brandproduct_id');

			$public_alias = $this->input->post('public_alias');

			$flavors_data = $this->input->post('flavors_data');

			
			// first check if it has a cart session
			//prod_merchant_cost
				
			//print_r($prod_data_decoded->brandproduct_id);
		
			if($this->session->userdata('session_cart_id') == "")
			{
				/// create mysql_uuid() 
				$session_cart_id = $this->mysql_uuid();
			}
			else
			{
				$session_cart_id = $this->session->userdata('session_cart_id');
			}

			
			$params = array( 
						'session_cart_id' => $session_cart_id,
						'prod_id'	  	  => $prod_id,
						'qty' 			  => $qty,
						'prod_merchant_cost' 			  => $prod_merchant_cost,
						'prod_data' 			  => $prod_data,
						'flavors_data' 			  => $flavors_data,
						'public_alias' 			  => $public_alias,
						'brandproduct_id' 			  => $brandproduct_id,
						);
			$cart_qty = $this->add_to_cart_database($params);
			
			
			$cart_session_data = array (
									'session_cart_id' => $session_cart_id,
									'session_cart_qty_'.$public_alias=> $cart_qty
										);
			$this->session->set_userdata($cart_session_data);
	
			echo json_encode(array(
					'status' => 'success',
					'cart_qty' => $cart_qty
					));
			return;
		
		}
	}
	public function public_register($brand_type="")
	{
		if($_POST)
		{
		
			if($brand_type=="")
			{
				$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
				$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');	
			}
			
			
			$this->form_validation->set_rules('day', 'Day', 'trim|required|callback_dob_check');
			$this->form_validation->set_rules('months', 'Months', 'trim|required');
			$this->form_validation->set_rules('year', 'Year', 'trim|required');
			

			if($brand_type=="")
			{
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_check_email_if_already_registered');
			}
			
			if($this->form_validation->run() == TRUE)
			{
				if($brand_type=="hsbc" || $brand_type=="uob" || $brand_type=="dbs" || $brand_type=="amex")
				{
					//create a new session for this user
					$login_session_data = array('public_user_data' => 
												array(
													'user_email' => '',
													'user_fname' => '',
													'user_lname' => ''
												),
												'public_user_since_user_logged_in' => time()
											);
					$this->session->set_userdata($login_session_data);
					echo json_encode(array('status' => 'success'));
					return;

				}
				else if($brand_type=="")
				{
	                $user_dob = $this->input->post('year').'-'.$this->input->post('months').'-'.$this->input->post('day');
					$verify = $this->login_m->verify_user($this->input->post('email'));
					if($verify->num_rows()==0)
					{
					
						//save to database as pending
						$signup_data = array('username' => $this->input->post('email'),
							'user_fname' => $this->input->post('fname'),
							'user_lname' => $this->input->post('lname'),
							'user_email' => $this->input->post('email'),
	                        'user_dob	' => $user_dob,
							'user_city_id' => 1,
							'user_join_date' => date('Y-m-d H:i:s'),
							'user_account_created_ip' => $this->custom_func->getUserIP(),
							'user_hash_key' => $this->mysql_uuid(),
							'user_status' => 'Pending',
							'user_last_updated_date' => $this->custom_func->datetime()
						);

						$this->db->insert('gl_user', $signup_data);
						$insert_id = $this->db->insert_id();
					
						
						//save to database as pending
						
						//create a new session for this user
						$login_session_data = array('public_user_data' => 
													array(
														'user_email' => $this->input->post('email'),
														'user_fname' => $this->input->post('fname'),
														'user_lname' => $this->input->post('lname')
													),
													'public_user_since_user_logged_in' => time()
												);
						$this->session->set_userdata($login_session_data);
						echo json_encode(array('status' => 'success'));
						return;
						
						//echo 'inserted - pending';
						//$this->load_public_brand($brand_product_id, $data);
						//create a new session for this user
					}
					else
					{	
					
						//check if user is verified or pending
						if($verify->row()->user_status =='Pending')
					
						{
	                        $data = array('user_dob	' => $user_dob,);
							$where = array('user_id' => $verify->row()->user_id);
							$this->db->update('gl_user', $data, $where);
	                    
							//create a new session for this user
							$login_session_data = array('public_user_data' => 
														array(
															'user_email' => $this->input->post('email'),
															'user_fname' => $this->input->post('fname'),
															'user_lname' => $this->input->post('lname')
														),
														'public_user_since_user_logged_in' => time()
													);
							$this->session->set_userdata($login_session_data);
							echo json_encode(array('status' => 'success'));
							return;
							//echo 'updated - pending';
							//$this->load_public_brand($brand_product_id, $data);
							// if pending -  update user DOB and create new session so user can proceed
							//$data = array('user_profile_pic' => $user_photo);
							//$where = array('user_id' => $user_id);
							//$this->db->update($this->INSTANCE, $data, $where);
							
							// if pending -  update user DOB and create new session so user can proceed
						}
						//check if user is verified or pending
					}
				}//($brand_type=="")
				
			}
			else
			{
				echo json_encode(
					array(
						'status' => 'error',
						'msg' => validation_errors()
					)
				);
				return;
			}
			
			
		
		}
	}
	public function logout($public_alias="", $dev="")
	{
		if($dev!="")
			$dev = $dev."/";
		$session_data = array('public_user_data' => '', 'public_user_since_user_logged_in'=> '');
		$this->session->unset_userdata($session_data);
		redirect(site_url().$dev.$public_alias);
	}
	public function view( $public_alias='', $brand_product_id=0 ,$dev="") 
	{
		if($dev!="")
			$this->device = $dev;
		$this->load->library('user_agent');
		$data['Page_title'] = $this->lang->line('landing_page_title');
		$data['page_title'] = $this->lang->line('landing_page_title');
		$data['is_voucher_okay'] = false;
		$data['public_alias'] = $public_alias;

		
			
		
		/*/$this->load->helper('file');

		$image_path = FCPATH.'assets/frontend/img/glomp_Web_Landing_Img1_Temp.png';

		$this->output->set_content_type('jpeg');
		$this->output->set_output(file_get_contents($image_path));
		*/
		$data['brand']  = $this->Brand->get_by_public_alias( $public_alias );
		if($data['brand'] !=false && $public_alias!="")
		{
			//if(is_numeric($this->session->userdata('user_id')))
			//check if there is an existing sesstion
			$has_no_public_session = true;
			
			if($this->session->userdata('user_id') !='')
			{
				$has_no_public_session = false;
				
				$login_session_data = array('public_user_data' => 
											array(
												'user_email' => $this->session->userdata('user_email'),
												'user_fname' => $this->session->userdata('user_fname'),
												'user_lname' => $this->session->userdata('user_lname')
											),
											'public_user_since_user_logged_in' => time()
										);
				$this->session->set_userdata($login_session_data);
				
			}
			else if($this->session->userdata('public_user_since_user_logged_in') !='')
			{
			
				$inactive = 2400; //1200
				$session_life = time()-$this->session->userdata('public_user_since_user_logged_in');
				//echo $session_life;
				if($session_life > $inactive)
				{
					//$msg="Session has been expired";
					$has_no_public_session = true;
					//$login_session_data = array('public_user_since_user_logged_in' => '');
					//$this->session->set_userdata($login_session_data);
				}
				else
				{
					$this->session->set_userdata('public_user_since_user_logged_in',time());
					$has_no_public_session = false;
				}
			}
			
			//check if there is an existing sesstion
			
			if($has_no_public_session)
			{
			
				// user needs to fill up the form to check age.
				if($_POST)
				{
				
					$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
					$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
					
					$this->form_validation->set_rules('day', 'Day', 'trim|required|callback_dob_check');
					$this->form_validation->set_rules('months', 'Months', 'trim|required');
					$this->form_validation->set_rules('year', 'Year', 'trim|required');
					
					$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_check_email_if_already_registered');
					
					
					if($this->form_validation->run() == TRUE)
					{
					
						$verify = $this->login_m->verify_user($this->input->post('email'));
						if($verify->num_rows()==0)
						{
						
							//save to database as pending
							$signup_data = array('username' => $this->input->post('email'),
								'user_fname' => $this->input->post('fname'),
								'user_lname' => $this->input->post('lname'),
								'user_email' => $this->input->post('email'),
								'user_city_id' => 1,
								'user_join_date' => date('Y-m-d H:i:s'),
								'user_account_created_ip' => $this->custom_func->getUserIP(),
								'user_hash_key' => $this->mysql_uuid(),
								'user_status' => 'Pending',
								'user_last_updated_date' => $this->custom_func->datetime()
							);

							$this->db->insert('gl_user', $signup_data);
							$insert_id = $this->db->insert_id();
						
							
							//save to database as pending
							
							//create a new session for this user
							$login_session_data = array('public_user_data' => 
														array(
															'user_email' => $this->input->post('email'),
															'user_fname' => $this->input->post('fname'),
															'user_lname' => $this->input->post('lname')
														),
														'public_user_since_user_logged_in' => time()
													);
							$this->session->set_userdata($login_session_data);
							
							//echo 'inserted - pending';
							$this->load_public_brand($brand_product_id, $data);
							//create a new session for this user
						}
						else
						{	
						
							//check if user is verified or pending
							if($verify->row()->user_status =='Pending')
							{
								//create a new session for this user
								$login_session_data = array('public_user_data' => 
															array(
																'user_email' => $this->input->post('email'),
																'user_fname' => $this->input->post('fname'),
																'user_lname' => $this->input->post('lname')
															),
															'public_user_since_user_logged_in' => time()
														);
								$this->session->set_userdata($login_session_data);
								//echo 'updated - pending';
								$this->load_public_brand($brand_product_id, $data);
								// if pending -  update user DOB and create new session so user can proceed
								//$data = array('user_profile_pic' => $user_photo);
								//$where = array('user_id' => $user_id);
								//$this->db->update($this->INSTANCE, $data, $where);
								
								// if pending -  update user DOB and create new session so user can proceed
							}
							//check if user is verified or pending
						}
						
					}
					else
					{
						if($this->device=='Computer')
						{
							if($public_alias== "hsbc")
							{
								$this->load->view('brands_hsbc_landing_v', $data, TRUE,TRUE);
							}
							else if($public_alias== "uob")
							{
								$this->load->view('brands_uob_landing_v', $data, TRUE,TRUE);
							}
							else if($public_alias== "dbs")
							{
								$this->load->view('brands_dbs_landing_v', $data, TRUE,TRUE);
							}
							else
								$this->load->view('brands_public_landing_v', $data, TRUE,TRUE);

						}
						else{
							if($public_alias== "hsbc")
							{
								$this->load->view('m/brands_hsbc_landing_v', $data, TRUE,TRUE);
							}
							else if($public_alias== "uob")
							{
								$this->load->view('m/brands_uob_landing_v', $data, TRUE,TRUE);
							}
							else if($public_alias== "dbs")
							{
								$this->load->view('m/brands_dbs_landing_v', $data, TRUE,TRUE);
							}
							else
								$this->load->view('m/brands_public_landing_v', $data, TRUE,TRUE);
						}
					}
					
					
				
				}
				else
				{
					if($this->device=='Computer')
					{
						if($public_alias== "hsbc")
						{
							$this->load->view('brands_hsbc_landing_v', $data, TRUE,TRUE);
						}
						else if($public_alias== "uob")
						{
							$this->load->view('brands_uob_landing_v', $data, TRUE,TRUE);
						}
						else if($public_alias== "dbs")
						{
							$this->load->view('brands_dbs_landing_v', $data, TRUE,TRUE);
						}
						else
							$this->load->view('brands_public_landing_v', $data, TRUE,TRUE);

					}
					else{
						if($public_alias== "hsbc")
						{
							$this->load->view('m/brands_hsbc_landing_v', $data, TRUE,TRUE);
						}
						else if($public_alias== "uob")
						{
							$this->load->view('m/brands_uob_landing_v', $data, TRUE,TRUE);
						}
						else if($public_alias== "dbs")
						{
							$this->load->view('m/brands_dbs_landing_v', $data, TRUE,TRUE);
						}
						else
							$this->load->view('m/brands_public_landing_v', $data, TRUE,TRUE);
					}
				}
				
			}
			else
			{
				$this->load_public_brand($brand_product_id, $data);
			}
		}
		else
		{
			$data['error_404'] =  $this->lang->line('page_404_error');
			$this->load->view('404_v',$data);
		}
	 }

	 function check_email_exist($user_email) {
	 	if ($this->custom_func->emailValidation($user_email) == "false") {	
	 		$this->form_validation->set_message('check_email_exist',$this->lang->line('invalid_email_address'));
	 		return FALSE;
	 	}  else {
	 		return TRUE;
	 	}
	 }
	 function check_post_code($post_code) {
	 	if (strlen($post_code)!=6) {	
	 		$this->form_validation->set_message('check_post_code',$this->lang->line('invalid_post_code',"Please enter your correct Singaporean postcode."));
	 		return FALSE;
	 	}  else {
	 		return TRUE;
	 	}
	 }
	 function check_contact_num($contact_num) {
	 	if (strlen($contact_num)!=8) {	
	 		$this->form_validation->set_message('check_contact_num',$this->lang->line('invalid_contact_num',"Please enter your correct Singaporean mobile number."));
	 		return FALSE;
	 	}  else {
	 		return TRUE;
	 	}
	 }

 	public function glomp_hsbcsg() {
 		$mobile_pfx = '';
 		if( strpos( uri_string(), 'm/' ) !== false ) {
 			$mobile_pfx = 'm/';
 		}

 		

 		$public_alias = $this->input->post('public_alias');
 		$delivery_type = $this->input->post('delivery_type');
 		$transaction_type 	= $this->input->post('transaction_type');
 		//buyer
		$user_fname = $this->input->post('fname');
		$user_lname = $this->input->post('lname');
		$user_email = $this->input->post('email');
		$contact_num = $this->input->post('contact_num');
		$post_code = $this->input->post('post_code');
		$street_address = $this->input->post('street_address');
		$personal_message = $this->input->post('user_message');

		//glompee
		$glompee_fname = $this->input->post('glompee_fname');
		$glompee_lname = $this->input->post('glompee_lname');
		$glompee_email = $this->input->post('glompee_email');
		$card_num = $this->input->post('card_num');


		$this->form_validation->set_rules('fname', 'Purchaser First Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Purchaser Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Purchaser Email', 'trim|required|valid_email|xss_clean|callback_check_email_exist');

		$this->log_amex_action('checkout', FALSE, $public_alias); 

		if ($transaction_type == 'glomp_to_friend')
		{
			$this->form_validation->set_rules('glompee_fname', 'Reciepient First Name', 'trim|required');
			$this->form_validation->set_rules('glompee_lname', 'Reciepient Last Name', 'trim|required');
			$this->form_validation->set_rules('glompee_email', 'Reciepient Email', 'trim|required|valid_email|xss_clean|callback_check_email_exist');
		}

		if ($transaction_type === 'buy')
		{
			$this->form_validation->set_rules('contact_num', 'Purchaser Contact No', 'trim|required|callback_check_contact_num');
			if ($delivery_type == 'Delivery'){
				$this->form_validation->set_rules('post_code', 'Postcode', 'trim|required|callback_check_post_code');
				$this->form_validation->set_rules('street_address', 'Contact No', 'trim|required');
			}
		}
		$this->form_validation->set_rules('card_num', 'Card No.', 'trim|required');		
					
		if($this->form_validation->run() == TRUE)
		{
			$amount = $this->input->post('grand_total');
			if($amount > 0)
	        {
	            $environment = $this->PAYPAL_ENVIRONMENT;
	            // Set request-specific fields.
	            $paymentAmount = urlencode($amount);
	            $currencyID = urlencode('SGD');							// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
	            $paymentType = urlencode('Sale');				// or 'Sale' or 'Order'
   
	            $returnURL = urlencode(site_url($mobile_pfx.'brands/get_checkout').'/'.$public_alias);
	            $cancelURL = urlencode(site_url($mobile_pfx.'brands/cart/'.$public_alias.'?cancel=1'));    

	            //$returnURL = urlencode(('http://glomp.it/buyPoints/get_checkout'));
	            //$cancelURL = urlencode(('http://glomp.it/buyPoints/cancel_checkout'));

	            $nvpStr="";
	            $nvpStr .="&RETURNURL=$returnURL";
	            $nvpStr .="&CANCELURL=$cancelURL";
	            $nvpStr .="&PAYMENTREQUEST_0_PAYMENTACTION=$paymentType";
	            $nvpStr .="&PAYMENTREQUEST_0_AMT=$amount";
	            $nvpStr .="&PAYMENTREQUEST_0_CURRENCYCODE=$currencyID";
	            
	            $nvpStr .="&L_PAYMENTREQUEST_0_NAME0=Purchases";
	            $nvpStr .="&L_PAYMENTREQUEST_0_QTY0=1";
	            $nvpStr .="&L_PAYMENTREQUEST_0_AMT0=$amount";
	            
	            $nvpStr .="&LANDINGPAGE=Billing";
	            $nvpStr .="&BRANDNAME=glomp!";
	            $nvpStr .="&PAYFLOWCOLOR=DEE6EB";
	            $nvpStr .="&SOLUTIONTYPE=Sole";  
	            $nvpStr .="&REQCONFIRMSHIPPING=0";
	            $nvpStr .="&NOSHIPPING=1";
	            $nvpStr .="&ADDROVERRIDE=0";
	            $nvpStr .="&CARTBORDERCOLOR=DEE6EB";
	            $nvpStr .="&LOGOIMG=".urlencode("http://glomp.it/assets/images/logo_small_new.png!");        
	            
	            // Execute the API operation; see the PPHttpPost function above.
	            $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $nvpStr);
	            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
	            {
	                // Redirect to paypal.com.
	                $token = urldecode($httpParsedResponseAr["TOKEN"]);

	                //Save when to FB
					if ($transaction_type == 'glomp_to_fb') {
						$glompee_fname = 'N/A';
						$glompee_lname = 'N/A';
					}


	                $session_details = array(
	                	'delivery_type' => $delivery_type,
	                	'transaction_type' => $transaction_type,
	                	'buyer' => array(
	                		'fname' => $user_fname,
	                		'lname' => $user_lname,
	                		'email' => $user_email,
	                		'contact_num' => $contact_num,
	                		'post_code' => $post_code,
	                		'street_address' => $street_address,
                		),
                		'glompee' => array(
	                		'fname' => $glompee_fname,
	                		'lname' => $glompee_lname,
	                		'email' => $glompee_email,
                		),
                	);

                	if ($transaction_type == 'buy') {
                		$session_details['glompee'] = array(
                			'fname' => $user_fname,
	                		'lname' => $user_lname,
	                		'email' => $user_email,
            			);
                	}

	                //To payment logs
	                $payment_session = array(
						'session_cart_id' => $this->session->userdata('session_cart_id'),
						'session_amount' => $amount,
						'session_token_id' => $token,
						'session_date' => date('Y-m-d H:i:s'),
						'session_status' => 'Checkout',
						'session_details' => json_encode($session_details),
					);
					$this->db->insert('gl_campaign_payment_session', $payment_session);


	                $payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
	                if("sandbox" === $environment || "beta-sandbox" === $environment) {
	                    $payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&token=$token";                
	                }

					echo json_encode(array(
						'status'=> 'redirect',
						'location'=> $payPalURL,
						'data' 	=> array()
					));
					return;
	            } else {
	                echo json_encode(array(
						'status'=> 'error',
						'location'=> '',
						'data' 	=> array('paypal error')
					));
	            }
	        }
	        else
	        {
	            echo json_encode(array(
					'status'=> 'error',
					'location'=> '',
					'data' 	=> array('no total')
				));
				return;
	        }
		}
		else 
		{
			echo json_encode(array(
				'status'=> 'error',
				'location'=> '',
				'data' 	=> validation_errors()
			));
			return;
		}

 	}

 	public function get_checkout($public_alias = 'hsbc', $group_voucher_id = '') {
 		$data['data'] = array(
 			'success_payment' => FALSE,
 			'group_voucher_id' => '',
 			'token' => (isset($_GET['token'])) ? $_GET['token'] : '',
		);
		$data['car_data'] = '';
		$data['public_alias'] = $public_alias;

 		if ($_POST) {
 			//Javascript check
 			$session = $this->db->get_where('gl_campaign_payment_session', array(
 				'session_token_id' => $this->input->post('token'),
 				'session_status' => 'Completed'
			));

			if ($session->num_rows() > 0) {
				$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
	 				'session_cart_id' => $session->row()->session_cart_id,
				));

				$this->log_amex_action('success_transaction', FALSE, $public_alias); 

				$data['success'] = TRUE;
				$data['group_voucher_id'] = $voucher_brand->row()->group_voucher_id;
				echo json_encode($data);
				return;
			}

			$data['success'] = FALSE;
			echo json_encode($data);
			return;
 		}


 		//Payment complete
 		if (! empty($group_voucher_id)) {
 			$cart_session_data = array ('session_cart_id' => '', 'session_cart_qty_'.$public_alias => '');
			$this->session->set_userdata($cart_session_data);

 			$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
 				'group_voucher_id' => $group_voucher_id
			));

			if ($voucher_brand->num_rows() > 0) {
				$data['data']['success_payment'] = TRUE;
				$data['data']['group_voucher_id'] = $group_voucher_id;
				$data['data']['group_voucher_data'] = $voucher_brand->row();

				// get cart_details
				$sql = "SELECT gl_cart.prod_details, gl_product.prod_merchant_id
						FROM gl_cart 
						LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
						WHERE 
							session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
							public_alias = '".$public_alias."' 		
						GROUP BY gl_product.prod_merchant_id
						";
				
				$result = $this->db->query($sql);
				$cart_data="";
				foreach( $result->result() as $row)
				{
					$sql = "SELECT gl_cart.*, gl_product.prod_merchant_id
						FROM gl_cart 
						LEFT JOIN gl_product ON gl_cart.prod_id = gl_product.prod_id
						WHERE 
							session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
							public_alias = '".$public_alias."' AND		
							gl_product.prod_merchant_id = '".$row->prod_merchant_id."'
						";
					$prod_details = json_decode($row->prod_details);

					$product_list="";
					foreach($this->db->query($sql)->result() as $product)
					{
						$sql = "SELECT *
						FROM gl_cart_option
						WHERE 
							session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
							prod_id = '".$product->prod_id."' AND		
							prod_group = '".$product->prod_group."'
						";

						$option = $this->db->query($sql)->result();

						$product_list[] = array (
							'product' => $product,
							'option' => $option,
							);

					}

					$cart_data [] = array (
						'brand_id' => $prod_details->brandproduct_id,
						'brand_name' => $this->Brand->get_brandproduct($prod_details->brandproduct_id)->name,
						'prod_list' => $product_list
					);
				
				}
			}
			$data['cart_data'] = $cart_data;

 		} else {
			$session = $this->db->get_where('gl_campaign_payment_session', array(
				'session_token_id' => $data['data']['token'],
				'session_status' => 'Checkout'
			));
			$token = $data['data']['token'];
	 		$nvpStr = "&TOKEN=".$token;
	        // Execute the API operation; see the PPHttpPost function above.
	        $httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $nvpStr);
	        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	            if($session->num_rows() > 0 ) {
	                $session_token_id = $session->row()->session_token_id;
	                $payerID = $httpParsedResponseAr['PAYERID'];
	                $amount = $session->row()->session_amount;

	                $paymentType = urlencode("SALE");			// or 'Sale' or 'Order'
			        $paymentAmount = urlencode($amount);
			        $currencyID = urlencode("SGD");						// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
			        $ipn = site_url('paypal/hsbc_ipn/'.$public_alias);
			        // Add request-specific fields to the request string.
			        $nvpStr = "&TOKEN=$token&PAYERID=$payerID&PAYMENTACTION=$paymentType&AMT=$paymentAmount&CURRENCYCODE=$currencyID";
			        $nvpStr .="&custom=$session_token_id";
			        //$nvpStr .="&PAYMENTREQUEST_0_NOTIFYURL=$ipn";
			        $nvpStr .="&NOTIFYURL=$ipn";

			        // Execute the API operation; see the PPHttpPost function above.
			        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpStr);
			        /*if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
	            	{
	            	}*/
	            }
	        }
 		}

 		if( strpos( uri_string(), 'm/' ) !== false ) {
 			$this->load->view( 'm/brand_public_return_checkout_hsbc', $data);
		} else {
			$this->load->view( 'brand_public_return_checkout_hsbc', $data);	
		}
 		
 	}

 	public function PPHttpPost($methodName_, $nvpStr_) {
        $environment=$this->PAYPAL_ENVIRONMENT;
            // Set up your API credentials, PayPal end point, and API version.
        /**/
        $API_UserName = urlencode('paid_api1.glomp.it');
        $API_Password = urlencode('ACGRWSM353ZZQ638');
        $API_Signature = urlencode('AX6acVWmmDx.b8w1sO4JgRtSNme1AXwFnp2Nf2hCgdKCaU.FuANfLj9t');
        
        
        /*
        $API_UserName = urlencode('allan.bernabe-facilitator_api1.gmail.com');
        $API_Password = urlencode('TSTCNYANYT9TXTF8');
        $API_Signature = urlencode('AiGm97m9-5fDHpUVCtqv7lJHEDEbAxOgZCs5DXZ3DrlSkqUJtbci.K20');/**/
        
        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if("sandbox" === $environment || "beta-sandbox" === $environment)
        {
        	$API_UserName = urlencode('allan.bernabe-facilitator_api1.gmail.com');
            $API_Password = urlencode('TSTCNYANYT9TXTF8');
            $API_Signature = urlencode('AiGm97m9-5fDHpUVCtqv7lJHEDEbAxOgZCs5DXZ3DrlSkqUJtbci.K20');  
            $API_Endpoint = "https://SHA2-test-api-3t.sandbox.paypal.com/nvp";
        }
        $version = urlencode('93.0');

        // Set the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the API operation, version, and API signature in the request.
        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

        // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        // Get response from the server.
        $httpResponse = curl_exec($ch);

        if(!$httpResponse) {            
            exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
            redirect('brands/http_post_failed');            
            exit();
        }
        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);
        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if(sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }
        if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
           exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
            redirect('brands/http_post_failed');            
            exit();
        }
        return $httpParsedResponseAr;
    }

	public function glompToMe()
	{
		$mobile_pfx = '';
 		if( strpos( uri_string(), 'm/' ) !== false ) {
 			$mobile_pfx = 'm/';
 		}

 		

 		$public_alias = $this->input->post('public_alias');
 		$delivery_type = $this->input->post('delivery_type');
 		$transaction_type 	= $this->input->post('transaction_type');
 		//buyer
		$user_fname = $this->input->post('fname');
		$user_lname = $this->input->post('lname');
		$user_email = $this->input->post('email');
		$contact_num = $this->input->post('contact_num');
		$post_code = $this->input->post('post_code');
		$street_address = $this->input->post('street_address');
		$personal_message = $this->input->post('user_message');

		//glompee
		$glompee_fname = $this->input->post('glompee_fname');
		$glompee_lname = $this->input->post('glompee_lname');
		$glompee_email = $this->input->post('glompee_email');
		

		$card_num = $this->input->post('card_num_1').$this->input->post('card_num_2').$this->input->post('card_num_3');

		$this->form_validation->set_rules('email', 'Purchaser Email', 'trim|required|valid_email|xss_clean|callback_check_email_exist');
		$this->form_validation->set_rules('fname', 'Purchaser First Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Purchaser Last Name', 'trim|required');		

		$this->log_amex_action('checkout', FALSE, $public_alias); 

		if ($transaction_type == 'glomp_to_friend')
		{
			$this->form_validation->set_rules('glompee_fname', 'Reciepient First Name', 'trim|required');
			$this->form_validation->set_rules('glompee_lname', 'Reciepient Last Name', 'trim|required');
			$this->form_validation->set_rules('glompee_email', 'Reciepient Email', 'trim|required|valid_email|xss_clean|callback_check_email_exist');
		}

		if ($transaction_type === 'buy')
		{
			$this->form_validation->set_rules('contact_num', 'Purchaser Contact No', 'trim|required|callback_check_contact_num');
			if ($delivery_type == 'Delivery'){				
				$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required');
				$this->form_validation->set_rules('post_code', 'Postcode', 'trim|required|callback_check_post_code');
			}


		$glompee_fname = $this->input->post('fname');
		$glompee_lname = $this->input->post('lname');
		$glompee_email = $this->input->post('email');

		}
		if($this->input->post('card_num_1')=="")
			$this->form_validation->set_rules('card_num_1', 'Card No.', 'trim|required');
		else if($this->input->post('card_num_2')=="")
			$this->form_validation->set_rules('card_num_2', 'Card No.', 'trim|required');
		else if($this->input->post('card_num_3')=="")
			$this->form_validation->set_rules('card_num_3', 'Card No.', 'trim|required');
					
		if($this->form_validation->run() == TRUE)
		{
			$grand_total = 0;
			$sql = "SELECT prod_cost, prod_qty  FROM gl_cart WHERE session_cart_id = '".$this->session->userdata('session_cart_id')."' AND public_alias = '".$public_alias."'";
			$result = $this->db->query($sql);
			foreach( $result->result() as $row ) {
				$grand_total += ( ($row->prod_cost) * $row->prod_qty);
			}
			$grand_total_usd = ($grand_total * BRAND_FOREX_AMEX);
			$grand_total_usd =  number_format(($grand_total_usd),2);
			$grand_total_usd =  str_replace(".","",$grand_total_usd);
			$grand_total_usd =  str_replace(",","",$grand_total_usd);
			$params = Array(
							'vpc_AccessCode'	=> AMEX_vpc_AccessCode,
							'vpc_MerchTxnRef'	=> $this->session->userdata('session_cart_id'),
							'vpc_Merchant'	 	=> AMEX_vpc_Merchant,
							'vpc_OrderInfo' 	=> AMEX_vpc_OrderInfo,
							'vpc_Amount' 		=> $grand_total_usd,
							'secret'			=> AMEX_secret,
							
							'integration_type'			=>	2,
							'vpc_CardNum'			=>	$card_num,//'3456 729834 53416',
							'vpc_cardExp'			=>	$this->input->post('yy').$this->input->post('mm'),//'1705',
							'vpc_CardSecurityCode'			=>	$this->input->post('sec_code'),//'1234'
							);
			/**/
			
			$this->load->library('Amex',$params);
			$this->amex->pay();
			$response = $this->amex->getResponse();
			
			if(isset($response['vpc_TxnResponseCode']) && $response['vpc_TxnResponseCode']==0)
		 	{
		 		/***TRANSACTION OK STARTS HERE**/
		 		/***TRANSACTION OK STARTS HERE**/
		 		/***TRANSACTION OK STARTS HERE**/
		 		/***TRANSACTION OK STARTS HERE**/
		 		$macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
				$snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
				$london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
				$singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
				$swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
				$daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
				$kpo =  AMEX_BRAND_PROD_ID_KPO;
				$nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
				$delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
				$valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;

				define('BRAND_PROD_ID_MACALLAN',$macallan);
				define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
				define('BRAND_PROD_ID_LONDON_DRY',$london);

				define('BRAND_PROD_ID_SINGBEV',$singbev);
				define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
				define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

				define('BRAND_PROD_ID_KPO',$kpo);
				define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
				define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);	
				define('BRAND_PROD_ID_VALRHONA',$valrhona);

								
				$this->load->model('login_m');
				$voucher_code = strtoupper($this->login_m->generate_hash(6));
				$public_user_data = $this->session->userdata('public_user_data');
				$group_voucher_id = $this->mysql_uuid();
				$generate_voucher_1 = array(
					'group_voucher_id' => $group_voucher_id,
					'session_cart_id' => $this->session->userdata('session_cart_id'),
					'forex' => BRAND_FOREX_AMEX,
					'transaction_type' => $transaction_type,
					'from_email' => $public_user_data['user_email'],
					'from_first_name' => $public_user_data['user_fname'],
					'from_last_name' => $public_user_data['user_lname'],
					'email' => $user_email,
					'first_name' => $user_fname,
					'last_name' => $user_lname,
					'contact_num' => $contact_num,
					'delivery_postcode' => $post_code,
					'delivery_address' => $street_address,
					'delivery_type' => $delivery_type,
					'response_data' => json_encode($response),
					'voucher_code' => $voucher_code
				);
				$voucher_insert_status = $this->db->insert('gl_voucher_brand', $generate_voucher_1);

				//Buyer
				$buyer = $this->db->get_where('gl_user', array('user_email' => $user_email));

				if ($buyer->num_rows() == 0) 
				{
					$signup_data = array('username' => $user_email,
						'user_fname' => $user_fname,
						'user_lname' => $user_lname,
						'user_email' => $user_email,
						'user_city_id' => 1,
						'user_join_date' => date('Y-m-d H:i:s'),
						'user_account_created_ip' => $this->custom_func->getUserIP(),
						'user_hash_key' => $this->mysql_uuid(),
						'user_status' => 'Pending',
						'user_last_updated_date' => $this->custom_func->datetime()
					);

					$this->db->insert('gl_user', $signup_data);
					$insert_id = $this->db->insert_id();
					$sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
					$buyer = $this->db->query($sql);
				}
				//getting record
				$rec = $buyer->row();
				$voucher_purchaser_user_id = $rec->user_id;
				$glompee = $this->db->get_where('gl_user', array('user_email' => $glompee_email));

				if ($glompee->num_rows() == 0) 
				{
					$signup_data = array('username' => $glompee_email,
						'user_fname' => $glompee_fname,
						'user_lname' => $glompee_lname,
						'user_email' => $glompee_email,
						'user_city_id' => 1,
						'user_join_date' => date('Y-m-d H:i:s'),
						'user_account_created_ip' => $this->custom_func->getUserIP(),
						'user_hash_key' => $this->mysql_uuid(),
						'user_status' => 'Pending',
						'user_last_updated_date' => $this->custom_func->datetime()
					);

					$this->db->insert('gl_user', $signup_data);
					$insert_id = $this->db->insert_id();
					$sql = "SELECT * FROM gl_user WHERE user_id='$insert_id'";
					$glompee = $this->db->query($sql);
				}
				$rec = $glompee->row();
				$voucher_belongs_usser_id = $rec->user_id;

				$glomp_message ='';
				$voucher_code = time(1111, 9999) . '-' . time(1111, 9999) . '-' . time(1111, 9999);
				$sql = "SELECT * FROM gl_cart WHERE session_cart_id = '".$generate_voucher_1['session_cart_id']."' ";
				$result = $this->db->query($sql);
				foreach($result->result() as $row) {
					// on each prod_qty
					// on each prod_qty
					for($x=1; $x <= $row->prod_qty; $x++)
					{
						$product = json_decode($row->prod_details);
						$generate_voucher_id =$this->mysql_uuid();
						$generate_voucher = array('voucher_id' => $generate_voucher_id,
							'voucher_purchaser_user_id' => $voucher_purchaser_user_id,
							'voucher_belongs_usser_id' => $voucher_belongs_usser_id,
							'voucher_merchant_id' => $product->merchant_id,
							'voucher_product_id' => $product->product_id,
							'voucher_point' => $product->prod_point,
							'voucher_prod_cost' => $product->prod_merchant_cost,
							'voucher_reverse_point' => $product->prod_reverse_point,
							'voucher_code' => $voucher_code,
							'voucher_purchased_date' => date('Y-m-d H:i:s'),
							'voucher_status' => 'Consumable',
							'voucher_expiry_day' => 9999,
							'voucher_type' => 'Consumable',
							'voucher_expiry_date' => $this->vocher_expiration_day(9999),
							'voucher_sender_glomp_message' => addslashes($glomp_message)
						);
						$voucher_insert_status = $this->db->insert('gl_voucher', $generate_voucher);
						
						$insert_data = array(
							'group_voucher_id' => $group_voucher_id,
							'voucher_id' => $generate_voucher_id
						);
						$voucher_insert_status = $this->db->insert('gl_voucher_brand_group_list', $insert_data);
					}
				}



				//return;
				//Send emails
				$this->load->library('email_templating');

				$voucher_brand = $this->db->get_where('gl_voucher_brand', array(
						'group_voucher_id' => $group_voucher_id
				));
				if ($voucher_brand->num_rows() > 0) 
				{
					// get cart_details
					$sql = "SELECT *,
						CASE brandproduct_id
							WHEN '".BRAND_PROD_ID_MACALLAN."' THEN 0
							WHEN '".BRAND_PROD_ID_SNOW_LEOPARD."' THEN 0
							WHEN '".BRAND_PROD_ID_LONDON_DRY."' THEN 0
							ELSE brandproduct_id
						END as group_brandproduct_id
						FROM gl_cart 
						WHERE 
							session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
							public_alias = '".$public_alias."' 		
						GROUP BY group_brandproduct_id
						";
					
					$result = $this->db->query($sql);
					$cart_data="";
					foreach( $result->result() as $row)
					{
						if($row->group_brandproduct_id==0)
						{
							$group_brand_array = BRAND_PROD_ID_MACALLAN.','.BRAND_PROD_ID_SNOW_LEOPARD.','.BRAND_PROD_ID_LONDON_DRY;
							$sql = "SELECT *
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								public_alias = '".$public_alias."' AND		
								brandproduct_id IN (".$group_brand_array.") 
							";
						}
						else
						{
							$sql = "SELECT *
							FROM gl_cart 
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								public_alias = '".$public_alias."' AND		
								brandproduct_id = '".$row->brandproduct_id."'
							";
						}
						$prod_details = json_decode($row->prod_details);
						$product_list="";
						foreach($this->db->query($sql)->result() as $product)
						{
							$sql = "SELECT *
							FROM gl_cart_option
							WHERE 
								session_cart_id = '".$voucher_brand->row()->session_cart_id."' AND
								prod_id = '".$product->prod_id."' AND		
								prod_group = '".$product->prod_group."'
							";

							$option = $this->db->query($sql)->result();

							$product_list[] = array (
								'product' => $product,
								'option' => $option,
								);

						}

						$cart_data [] = array (
							'brand_id' => $prod_details->brandproduct_id,
							'brand_name' => $this->Brand->get_brandproduct($prod_details->brandproduct_id)->name,
							'prod_list' => $product_list
						);
					
					}
				}

				//Create product details table
				foreach ($cart_data as $brand_row) 
				{
					$brand_id = $brand_row['brand_id'];
					$delivery_type = '';
					$per_brand_total = 0;
					$grand_total = 0;
					$delivery_total = 0;
					$table="";
					$table2="";
					$has_delivery = FALSE;
					$delivery = '';
					$prouct_merchant_name_arr = array();
					foreach ($brand_row['prod_list'] as $row) {
						$options = $row['option'];	
						$row = $row['product'];
						$cost_price = get_hsbc_prod_data($row->prod_id);
						$delivery_type = $row->delivery_type;
						$product = json_decode($row->prod_details);
						$grand_total += (($row->prod_cost ) * $row->prod_qty);
						$grand_total_2 += (($cost_price['cost_price'] ) * $row->prod_qty);
						$per_brand_total += (($row->prod_cost ) * $row->prod_qty);

						$product_merchant_name = '<b>' .$product->prod_name.'</b> <br />'.$product->merchant_name;
						$product_merchant_name_2 = '<b>' .$product->prod_name.'</b> ('.$product->merchant_name.')';
						$prouct_merchant_name_arr[] = $product_merchant_name_2;
						if(count($options) > 0)	{
							$product_merchant_name .= '<br /><br />';
							foreach ($options as $p) {
								$product_merchant_name .= '&nbsp;&nbsp;&nbsp;'. $p->option_qty .'-'. $p->option_value .'<br />';
							}
						}

						$table .='
						<tr>
							<td><b>'.$product_merchant_name.'</td>
							<td align="right">$ '.number_format(($row->prod_cost),2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($row->prod_cost) * ($row->prod_qty),2).'</td>
						</tr>';

						//merchant
						$table2 .='
						<tr>
							<td><b>'.$product_merchant_name.'</td>
							<td align="right">$ '.number_format($cost_price['cost_price'],2).'</td>
							<td align="right">'.$row->prod_qty.'</td>
							<td align="right">$ '.number_format(($cost_price['cost_price']) * ($row->prod_qty),2).'</td>
						</tr>';

					}


					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$has_delivery = 16;
							break;
						case BRAND_PROD_ID_SINGBEV:
							if($per_brand_total < 350)
							{
								$has_delivery = 35;
							}	
							break;
						case BRAND_PROD_ID_VALRHONA:
							if($per_brand_total < 300)
							{
								$has_delivery = 35;
							}	
							break;
						case BRAND_PROD_ID_DELIGHTS_HEAVEN:
							$has_delivery = 25;
							break;
					}

					if ($delivery_type == 'pickup') {
						$has_delivery = 0;							
					}


					$grand_total = $grand_total + $has_delivery;
					if ($has_delivery > 0) {
						$delivery = ' (Delivery charge: S$ '.number_format($has_delivery, 2).')';	
					}

					$product_details ='
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table.'
					</table>';

					//merchant
					$product_details_merchant ='
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<th>Items</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
						'.$table2.'
					</table>';

					$template = '';
					$pick_up_details = '';
					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_MACALLAN:
						case BRAND_PROD_ID_SNOW_LEOPARD:
						case BRAND_PROD_ID_LONDON_DRY:
							$pick_up_details = '
								Edrington Singapore located at 12 Marina View, Level 24-01 Asia Square Tower 2, Singapore 018961 during office hours. <br />
								Please allow 3 working days after order placement.';
							break;
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$pick_up_details = '
								Main Outlet: <br />
								8 Rodyk Street, #01-08 <br />
								Singapore 238216 <br />
								<br />
								Specialty Outlet:<br />
								200 Turf Club Road, #03-07 The Grandstand<br />
								(Chillax Market)<br />
								Singapore 287994<br />
								<br />
								11am - 7pm every day';
							break;
						case BRAND_PROD_ID_SINGBEV:
							$pick_up_details = '
								470 North Bridge Road, #05-09 Bugis Cube Singapore 188735, 
								<br />
								<br />
								Mon - Sat 12pm - 8pm';
						case BRAND_PROD_ID_VALRHONA:
							$pick_up_details = '
								Euraco Finefood Pte Ltd Blk 219 Henderson Road #01-03 Henderson Industrial Park Singapore 159556, 
								<br />
								<br />
								T: +65 6276 5433
								<br />
								Mon - Fri 9:00am-12:00pm, 2:00pm-4:30pm';
							break;
					}
					if ($generate_voucher_1['transaction_type'] == 'buy') {
						$template = 'self_purchase_delivery'; //send to purchaser
						$vars['to'] = $generate_voucher_1['email'];
						$vars['params'] = array(
							'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
							'[product_details]' => $product_details,
							'[total_sg_usd]' => 'S$ '.number_format($grand_total,2). $delivery,
							'[delivery_address]' => $generate_voucher_1['delivery_address'],
							'[contact_no]' => $generate_voucher_1['contact_num'],
						);

						if ($delivery_type == 'pickup') {
							$template = 'self_purchase_pickup';
							$vars['params'] = array(
								'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
								'[product_details]' => $product_details,
								'[total_sg_usd]' => 'S$ '.number_format($grand_total,2),
								'[pick_up_details]' => 	$pick_up_details
							);
						}

						if (BRAND_PROD_ID_KPO == $brand_row['brand_id'] || BRAND_PROD_ID_NASSIM_HILL ==$brand_row['brand_id']) {
							//pick up only
							$vars['params']['[merchant_name]'] = $brand_row['brand_name'];
							$template = 'confirmation_selfpurchase_kpo_nassam';
						}

					} else if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend' || $generate_voucher_1['transaction_type'] == 'glomp_to_fb') 
					{
						$recipient_name = $generate_voucher_1['first_name']. ' '.$generate_voucher_1['last_name'];
						if ($generate_voucher_1['transaction_type'] == 'glomp_to_fb') {
							$temp = explode("@",$generate_voucher_1['email']);
							$recipient_name = 'facebook <a href="https://www.facebook.com/'.$temp[0].'" > friend </a>';
						}

						$template = 'notification_glomper_order'; //send to purchaser
						$vars['to'] = $generate_voucher_1['from_email'];
						$vars['params']['[product_details]'] = $product_details;
						$vars['params']['[total_sg_usd]'] = 'S$ '.number_format($grand_total,2). $delivery;
						$vars['params']['[recipient_name]'] = $recipient_name;
						$subject = '';
						if ($delivery_type == 'delivery') {
							if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') {
								$template2 = 'notification_glompee_delivery'; //send to glompee; pickup
								$subject = $generate_voucher_1['from_first_name']. ' has given you a treat!';
								$link = '<a href= "'.site_url('m/brands/acceptance/hsbc/'.$generate_voucher_1['group_voucher_id']).'"> here </a>';
								$vars2['params'] = array(
									'[recipient_fname]' => $generate_voucher_1['from_first_name'],
									'[personal_message]' => '',//TODO
									'[merchant_product_1]' => implode(',', $prouct_merchant_name_arr),
									'[merchant_product_2]' => implode(',', $prouct_merchant_name_arr),
									'[bank_name]' => strtoupper($public_alias),
									'[here]' => $link,
								);
							}
						} else {
							if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') { //do not send email for a glomp_to_fb
								$template2 = 'notification_glompee_pickup'; // send to glompee; pick up
								$vars2['params'] = array(
									'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
									'[personal_message]' => '',//TODO
									'[merchant_product_1]' => implode(',', $prouct_merchant_name_arr),
									'[merchant_product_2]' => implode(',', $prouct_merchant_name_arr),
									'[bank_name]' => strtoupper($public_alias),
									'[recipient_fname]' => $generate_voucher_1['from_first_name'],
									'[pick_up_details]' => $pick_up_details,
								);

								if (BRAND_PROD_ID_KPO == $brand_row['brand_id'] || BRAND_PROD_ID_NASSIM_HILL ==$brand_row['brand_id']) { 
									$template2 = 'acceptance_glompee_kpo_nassam';

									$vars2['params'] = array(
										'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
										'[product_details]' => $product_details,
										'[total_sg_usd]' => 'S$ '.number_format($grand_total,2)
									);

									if (BRAND_PROD_ID_KPO == $brand_row['brand_id']) {
										$vars2['params']['[merchant_name]'] = 'KPO';
									} else {
										$vars2['params']['[merchant_name]'] = 'Nassim Hill';
									}
								}
							}
						}
					}

					//send to purchaser
					$vars['template_name'] = $template;
					$vars['lang_id'] = 1;
					$this->email_templating->config($vars);
					$this->email_templating->send();
					//echo '<pre>';
					//print_r($vars);
					//send to glompee
					if (! empty($template2)) {
						$vars2['template_name'] = $template2;
						$vars2['lang_id'] = 1;
						$vars2['to'] = $generate_voucher_1['email'];
						//echo '<pre>';
						//print_r($vars2);
						$this->email_templating->config($vars2);
						if (! empty($subject)) {
							echo 'subject:' . $subject;
							$this->email_templating->set_subject($subject);
						}
						$this->email_templating->send();										
					}


					//send merchant
					//do not send to merchant if glomp to fb
					if ($generate_voucher_1['transaction_type'] == 'glomp_to_fb') {
						continue;
					}

					$vars = array(
						'template_name' => 'confirmation_order_erdington',
						'from_name' => SITE_DEFAULT_SENDER_NAME,
						'from_email' => SITE_DEFAULT_SENDER_EMAIL,
						'lang_id' => 1,
						'to' => 'allan.bernabe@gmail.com', //TODO: update sample only
						'params' => array(
							'[voucher_code]' => '<b>'. $generate_voucher_1['voucher_code']. '</b>',
							'[product_details]' => $product_details_merchant,
							'[total_sg]' => 'S$ '.number_format($grand_total_2,2) . $delivery,
							'[0.07_total]' => 'S$ '.number_format(($grand_total_2 * 0.07),2),
							'[total_gst_plus_total]' => 'S$ '. number_format(($grand_total_2 * 0.07) + $grand_total_2, 2),
							'[customer_name]' => $generate_voucher_1['from_first_name']. ' '. $generate_voucher_1['from_last_name'],
							'[delivery_address]' => $generate_voucher_1['delivery_address'],
							'[contact_no]' => $generate_voucher_1['contact_num'],
						)
					);
					

					switch ($brand_row['brand_id']) {
						case BRAND_PROD_ID_MACALLAN:
						case BRAND_PROD_ID_SNOW_LEOPARD:
						case BRAND_PROD_ID_LONDON_DRY:
							$vars['to'] = 'Cynthia.Tan@edrington.com';
							$vars['template_name'] = 'confirmation_order_erdington';
							break;
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							$vars['to'] = 'order@swirls.com.sg';
							$vars['template_name'] = 'confirmation_order_swirls';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2). $delivery;
							break;
						case BRAND_PROD_ID_SINGBEV:
							//send email for delivery
							$vars['to'] = 'sabrina@octopusgroup.com.sg';
							$vars['template_name'] = 'confirmation_order_singbev';
							break;
						case BRAND_PROD_ID_VALRHONA:
							//send email for delivery
							$vars['to'] = 'katrina@euraco.com.sg';
							$vars['template_name'] = 'confirmation_order_valrhona';
							break;
						case BRAND_PROD_ID_DELIGHTS_HEAVEN:
							//send email for pick up or delivery
							$vars['to'] = 'thedelightsheaven@gmail.com';
							$vars['template_name'] = 'confirmation_delights_heaven';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2) .$delivery;
							break;
						case BRAND_PROD_ID_DAILY_JUICE:
							//send email delivery is freee
							$vars['to'] = 'roger@dailyjuice.sg';
							$vars['template_name'] = 'confirmation_daily_juice';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							break;
						case BRAND_PROD_ID_KPO:
							//send email pickup only
							$vars['to'] = 'audreyliu@imaginings.com.sg';
							$vars['template_name'] = 'confirmation_order_nassam';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							$vars['params']['[merchant_name]'] = 'KPO';
							break;
						case BRAND_PROD_ID_NASSIM_HILL:
							//send email pickup only
							$vars['to'] = 'audreyliu@imaginings.com.sg';
							$vars['template_name'] = 'confirmation_order_nassam';
							$vars['params']['[total]'] = 'S$ '.number_format($grand_total_2,2);
							$vars['params']['[merchant_name]'] = 'Nassim Hill';
							break;
					}

					/**for DEV ONLY**/
					$vars['to'] = 'magbanua.ryan@gmail.com';
					/**for DEV ONLY**/		


					if ($generate_voucher_1['transaction_type'] == 'buy' || $generate_voucher_1['transaction_type'] == 'glomp_to_friend') {
						if ($generate_voucher_1['transaction_type'] == 'glomp_to_friend') {
							if ($delivery_type == 'pickup') {
								$vars['params']['[customer_name]'] = $generate_voucher_1['first_name']. ' '. $generate_voucher_1['last_name'];
								//print_r($vars);
								$this->email_templating->config($vars);
								$this->email_templating->send();
							}
						} else {
							$this->email_templating->config($vars);
							$this->email_templating->send();
							//send email again for this brand
							switch ($brand_row['brand_id']) {
								case BRAND_PROD_ID_MACALLAN:
								case BRAND_PROD_ID_SNOW_LEOPARD:
								case BRAND_PROD_ID_LONDON_DRY:
									//$vars['to'] = 'sulina@magnum.com.sg';
									$this->email_templating->config($vars);
									$this->email_templating->send();
									break;
							}
						}
					}

				}

				$cart_session_data = array ('session_cart_id' => '', 'session_cart_qty_'.$public_alias => '');
				$this->session->set_userdata($cart_session_data);


		 		/***TRANSACTION OK ENDS HERE**/
		 		/***TRANSACTION OK ENDS HERE**/
		 		/***TRANSACTION OK ENDS HERE**/
		 		/***TRANSACTION OK ENDS HERE**/
		 		/***TRANSACTION OK ENDS HERE**/
								
				echo json_encode(array(
							'status' 		=> 'success',
							'data' 	=> $group_voucher_id
							));
			}
			else
			{
				echo json_encode(array(
							'status' 		=> 'error',
							'data' 	=> getResultDescription($response['vpc_TxnResponseCode'])
							));
				
			}

		}/*validation runs true*/
		else
		{
			echo json_encode(array(
				'status'=> 'error',
				'location'=> '',
				'data' 	=> validation_errors()
			));
			return;
		}
	}
	private function load_public_brand($brand_product_id, $data)
	{
		if($brand_product_id==0)
		{
			$this->page( $data['brand']->id);
		}
		else
		{
		
			//print_r($brand_product_id);
			// brand info
			
			// products
			$q = $this->db->get_where('gl_brandproduct',array('id'=>$brand_product_id));
			$data['brandproducts'] = $q->result();
			
			
			//print_r($data['brandproducts']);
			//$data['brandproducts'] = $data['brandproducts']->row();
			if($brand_product_id == WINESTORE_BRAND_PRODUCT_ID)
			{
				$this->db->_protect_identifiers = FALSE;
				$temp = implode(',',get_winestore('prod_id'));
				$this->db->order_by('FIELD(gl_product.prod_id, '.$temp.') ');
			}
            else 
            	//if($brand_product_id == MACALLAN_BRAND_PRODUCT_ID)
            {
                $this->db->order_by("gl_product.prod_rank","ASC");

            }
			$this->db->join('gl_product','gl_brands_products.product_id = gl_product.prod_id');
			$this->db->join('gl_merchant','gl_product.prod_merchant_id=gl_merchant.merchant_id');
			
			$q = $this->db->get_where('gl_brands_products',array('brandproduct_id'=>$brand_product_id));
			$data['merchant_products'] = $q->result();
			
			
			$data['brands_preview_v'] =$data['brand'];
			$data['brand_product_id'] =$brand_product_id;
			
            //log campaign
            $details = array('name' => 'merchant_id','value' => $brand_product_id);
            $this->log_amex_action('viewed_merchant_page', $details);
            //log campaign
            
			if( strpos( uri_string(), 'm/' ) !== false )
			{
				$this->load->view( 'm/brands_public_products_v', $data, FALSE, TRUE );
			}
			else
			{
				$this->load->view( 'brands_public_products_v', $data);
			}
			
		}
	}

    public function preview( $id, $subpage = '' ) {
		$glompee = new stdClass();
        $this->is_preview = TRUE;
        $glompee->user_fname = 'Temp';
        $glompee->user_lname = '';
        $glompee->user_id = 0;
        $glompee->user_profile_pic = 'logo.png';
		$this->glompee = $glompee;
        $this->subpage = $subpage;
        $this->page( $id );
    }

    public function page( $id ) {
        
        $data['brand'] = $this->Brand->get_by_id( $id );
        $data['brandproducts'] = $this->Brand->get_brandproducts( $id );
        $data['template'] = $this->Brand->get_template( $id );
        if( isset($data['template']->id) ) {
            $data['sections'] = $this->_get_sections( $data['template']->id );
        }
        if( isset($data['sections']) ) {
            $data['widgets'] = $this->_get_widgets( $data['sections'] );
        }
        
        //Reference to API calling
        if (isset($_GET['api'])) {
            return $this->load->view( 'api/brands_preview_v', $data, FALSE, TRUE );
        }
        

        $data['user_record'] = $this->glompee;
        $data['inTour'] = 'no';
        $data['tabSelected'] = 'brands';

        // this will trigger list of products by brand page
        if( $this->subpage == 'products' ) {

            $subpage = $this->subpage;
            $product_id = $this->product_id;

            $data[$subpage] = $this->$subpage( $id, $product_id );
            if( $product_id ) {
                $data['active_brandproduct'] = $this->Brand->get_brandproduct( $product_id );
                $data['merchants'] = $this->_get_merchants_by_product_id( $product_id );
                $data['merchant_products'] = $this->_get_merchant_products_by_brandproduct_id($product_id);
            }
        }
        
        $data['is_preview'] = $this->is_preview;
        $data["public_alias"] ="";

        if( strpos( uri_string(), 'amex' ) !== false )
		{
			 $data["public_alias"] = 'amex-sg';
		}
		if( strpos( uri_string(), 'hsbc' ) !== false )
		{
			 $data["public_alias"] = 'hsbcsg';
		}
		if( strpos( uri_string(), 'uob' ) !== false )
		{
			 $data["public_alias"] = 'uobsg';
		}
		if( strpos( uri_string(), 'dbs' ) !== false )
		{
			 $data["public_alias"] = 'dbssg';
		}


        if( ( strpos( uri_string(), 'm/' ) !== false)  || ($this->device!='Computer') )
		{
			//echo uri_string()."asdasd";	
			//return;
				$this->log_amex_action('viewed_landing_page', FALSE, $data["public_alias"]);

			if( strpos( uri_string(), 'amex' ) !== false )
			{
                
				$this->load->view( 'm/brands_preview_amex_v', $data, FALSE, TRUE );
			}
			else if(( strpos( uri_string(), 'hsbc' ) !== false ) || ( strpos( uri_string(), 'uob' ) !== false ) || ( strpos( uri_string(), 'dbs' ) !== false ))
			{
				$this->load->view( 'm/brands_preview_amex_v', $data, FALSE, TRUE );
			}
			else
			{
				//echo "asdasd";
				$this->load->view( 'm/brands_preview_v', $data, FALSE, TRUE );
			}
        } else {            
           	$this->log_amex_action('viewed_landing_page', FALSE, $data["public_alias"]);            
            $this->load->view( 'brands_preview_v', $data, FALSE, TRUE );
        }

    }

    /**
     * Return product(s) by brand id
     *
     * Depending on the number of items, the template
     * should show a single product or product list layout
     *
     * @param Int $brand_id
     * @param Int $product_id
     */
    private function log_amex_action($action,$details=FALSE, $campaign_label='amex')
    {
        $temp = $this->session->userdata('public_user_data');
        
        $email = (isset($temp['user_email'])) ?  $temp['user_email']: '';
            
        
        $insert_data = array(
            'user_id' => $this->users_m->user_id_by_email($email),
            'campaign_label' => $campaign_label,
            'action' => $action,
            'platform' => $this->device,
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('gl_campaign_whitelabel_data', $insert_data);
        $insert_id = $this->db->insert_id();
        
        if($details!=FALSE)
        {
            $details['campaign_whitelabel_id'] = $insert_id;
            $this->db->insert('gl_campaign_whitelabel_details', $details);
        }
        
    }
    private function products( $brand_id, $product_id = 0 ) {
        if( $product_id != 0 ) {
            // return single product
            $this->load->model('product_m');
            return $this->product_m->selectProductByID( $product_id )->row();
        } else {
            // return multiple product
            return $this->Brand->get_brandproducts( $brand_id );
        }
    }
    /**
     * Returns Sections of a template
     *
     *
     * @todo This should be in Sections Model
     * @param $template_id Integer TemplateID
     */
    private function _get_sections( $template_id ) {
        $this->db->order_by('sort_order','asc');
        $sections = $this->db->get_where( 'gl_section', array('template_id'=>$template_id) );

        if( $sections->num_rows() > 0 ) {
            return $sections->result();
        }

        return FALSE;
    }

    /**
     * Returns Sections of a template
     *
     * @todo This should be in Sections Model
     * @param $sections Array
     */
    private function _get_widgets( $sections ) {
        $widgets = FALSE;
        if( is_array($sections) > 0 ) {
            foreach( $sections as $section ) {
                $section_id = $section->id;
                $this->db->order_by('position');
                $query = $this->db->get_where('gl_widget',array('section_id'=>$section_id));
                if( $query->num_rows() > 0 ) {
                    foreach( $query->result() as $widget ) {
                        $widget->data = $this->_get_widget_data( $widget->id, $widget->widget_type );
                        $widgets[$section->id][] = $widget;
                    }
                }
            }
        }
        return $widgets;
    }

    /**
     * Returns Widget data based on widget type
     *
     * @param $widget_id Integer
     */
    private function _get_widget_data( $widget_id, $type ) {

        $q = $this->db->get_where( 'gl_widget_'.$type, array('widget_id'=>$widget_id) );
        if( $q->num_rows() > 0 ) {
            return $q->row();
        }
        return FALSE;
    }
    
    /**
     * @todo no longer used?
     */
    private function _get_merchants_by_product_id( $product_id ) {
        $product = $this->product_m->selectProductByID( $product_id )->row();
        $q = $this->db->get_where('gl_merchant', array('merchant_id'=>$product->prod_merchant_id));
        if( $q->num_rows() > 0 ) {
            return $q->result();
        }
        
        return FALSE;
    }
    
    private function _get_merchant_products_by_brandproduct_id( $product_id ) {

        $this->db->join('gl_product','gl_brands_products.product_id = gl_product.prod_id');
        $this->db->join('gl_merchant','gl_product.prod_merchant_id=gl_merchant.merchant_id');
        $q = $this->db->get_where('gl_brands_products',array('brandproduct_id'=>$product_id));
        if( $q->num_rows() > 0 ) {
            return $q->result();
        }
        
        return FALSE;
    }
    
    public function set_glompee( $user_id ) {
        $this->glompee = $this->db->get_where( 'gl_user', array('user_id'=>$user_id) )->row();
        // check user_record photo
        if( file_exists(dirname(BASEPATH).'/custom/uploads/users/'.$this->glompee->user_profile_pic) ) {
            $this->glompee->user_icon_big = '/custom/uploads/users/'.$this->glompee->user_profile_pic;
            $this->glompee->user_icon_small = '/custom/uploads/users/thumb/'.$this->glompee->user_profile_pic;
        } else {
            $this->glompee->user_icon_big = '/custom/uploads/users/logo.png';
            $this->glompee->user_icon_small = '/custom/uploads/users/thumb/Male.png';
        }
        // user_record location string
        $q = $this->db->get_where('gl_region',array('region_id'=>$this->glompee->user_city_id));
        if($q->num_rows() > 0 ){
            $this->glompee->location_string = $q->row()->region_name;
            if($q->row()->region_parent_id != 0) {
                $q = $this->db->get_where('gl_region',array('region_id'=>$q->row()->region_parent_id));
                $this->glompee->location_string = $this->glompee->location_string . ', ' . $q->row()->region_name;
            }
        }
    }
    
    public function set_subpage() 
	{
        $this->subpage = 'products';
    }
    
    public function set_product_id( $product_id ) 
	{
        $this->product_id = $product_id;
    }
    
    public function json_brand_products() 
	{
        $data = array();
        
        // brand info
        $q = $this->db->get_where('gl_brand', array('id'=>$this->input->get('brand_id')));
        $data['brand'] = $q->row();
        
        // products
        $q = $this->db->get_where('gl_brandproduct',array('brand_id'=>$this->input->get('brand_id')));
        $data['products'] = $q->result();
        
        echo json_encode($data);
        exit;
    }
    
    public function json_merchant_products() 
	{
        $data = array();
        $this->db->join('gl_product','gl_brands_products.product_id = gl_product.prod_id');
        $this->db->join('gl_merchant','gl_product.prod_merchant_id=gl_merchant.merchant_id');
        $q = $this->db->get_where('gl_brands_products',array('brandproduct_id'=>$this->input->get('brandproduct_id')));
        $data['products'] = $q->result();
        echo json_encode($data);
        exit;
    }
}
