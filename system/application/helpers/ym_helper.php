<?php
/*
Name: YM-Helper
Author: Padam Shankhadev
Young Minds 
*/
function meta_author()
{
    $author="glomp! developers";
    return $author;
}
function meta_keywords($type="")
{
    
    $keywords='Glomp, Treat, Treating, Give, Gift, Gifting, Buy a treat, Buy a gift, Friend, Buddy, Mate, Give friend, Share, Care, Like, Greeting, Appreciate, Thank, Thank you, Cheer up, Sorry, Birthday, Wish, Thinking of you, Touch base, Coffee, Beer, Sweets, Cake, Drink';
    switch($type)
    {
        case 'profile':
            $keywords   .=', glomp! member profile';                    
        break;
        case '3':
        case 'support':
            $keywords   .='';
        break;
        case '2':
        case 'terms':
            $keywords   .='';
        break;
        case '4':
        case 'policy':
            $keywords   .='';
        break;
        case 'landing':
            $keywords   .='';
        break;
        case 'about_us':
            $keywords   .='';
        break;
        case '1':
        case 'faq':
            $keywords   .=', FAQ, Frequently Asked Questions on glomp!';
        break;    
    }
    return $keywords;
}
function add_activity_log($data = array())
{
    $CI = & get_instance();    
    if(isset($data['user_id']) && isset($data['type']) && isset($data['details']) )
    {
        $arr = array('user_id' => $data['user_id'],
            'log_type' => $data['type'],
            'details' => $data['details'],
            'log_timestamp' => date('Y-m-d H:i:s')            
        );
        $CI->db->insert('gl_activity_log', $arr);    
    }
}

function admin_url($url='')
{
	return site_url('adminControl/'.$url);
}
function db_input($string) {        
    $string=strip_tags($string);
    if (function_exists('mysql_real_escape_string')) {
      return mysql_real_escape_string($string);
    } elseif (function_exists('mysql_escape_string')) {
      return mysql_escape_string($string);
    }
    return addslashes($string);
}

function str_contains($string,$gama){
    $chars = mb_str_to_array($string);
    $gama = mb_str_to_array($gama);
    foreach($chars as $char) {
        if(in_array($char, $gama)==true)
        {
            return false;            
        }
    }
    return true;
}
function mb_str_to_array($string) {
    mb_internal_encoding("UTF-8"); // Important
    $chars = array();
    for ($i = 0; $i < mb_strlen($string); $i++) {
        $chars[] = mb_substr($string, $i, 1);
    }
    return $chars;
}
/**
 * get_create_cache
 * 
 * Get or create cache
 * data format: 
 * array(
 *      'cache_name' => '', #unique name
 *      'cache_table_name' => array(), #database table that will be affected by an update
 *      'result' => array() or object, #the result 
 *      'result_type' => 'result', #if from DB result object
 * )
 * 
 * @access public
 * @param array
 * @return void
*/
function get_create_cache($data = array()) {
    $cachetime = 0; #No expiry unless corresponding table has been updated.

    //If cache it turn on
    $CI = & get_instance();
    $CI->load->driver('cache');
    $CI->load->library('manage_cache');
    if (CACHE) {        
        
        $table_cache = array();
        //$CI->cache->memcached->clean(); #if you want to clear cache
        //exit;
        $cache = $CI->cache->memcached->get($data['cache_name']);
        if (empty($cache)) {        
            if ($data['result_type'] == 'result') {
                //echo "queried";
                $res = $CI->db->query($data['result']);
                $cache = $res;
                //Query Result
                $mc = new Manage_cache();
                $mc->set_result($res);
                $mc->set_num_rows($res->num_rows());
                if($res->num_rows()>0)
                    $mc->set_row($res->row());
                
                $CI->cache->memcached->save($data['cache_name'], $mc, $cachetime);
            }
             else {
                
                 //Array result
                 $cache = $data['result'];
                 $CI->cache->memcached->save($data['cache_name'], $data['result'], $cachetime);
             }
        }
        else
        {
            //echo "not queried";
            if(  $data['cache_name'] == 'user_info_by_id_9')
            {
                //print_r($cache);
                //echo  "<br>d".$cache->num_rows;
            }
        }
        

        foreach ($data['cache_table_name'] as $tb) {
            $table_cache = $CI->cache->memcached->get($tb);
            //Saves cache name (unique)
            //1 time only? or if the memcached server down
            if (empty($table_cache)) {
                $table_cache[] = $data['cache_name'];
                $CI->cache->memcached->save($tb, $table_cache, 0);
            } else {
                //Delete entries
                //$CI->cache->memcached->delete($tb);
                //Re-create and push new cache name to table dictionary
                //echo gettype($table_cache);                
                //print_r($table_cache)."<br>==";
                if (! in_array($data['cache_name'], $table_cache))
                {
                    $table_cache[] = $data['cache_name'];                    
                    $CI->cache->memcached->save($tb, $table_cache, 0);
                }
            }
        }
        
    } else {
        if ($data['result_type'] == 'result') {
            $cache = $CI->db->query($data['result']);
            //echo "queried";
        }
        else {
            $cache = $data['result'];
            //echo "not queried";
        }
    }

    return $cache;
}

/**
 * delete_cache
 * 
 * Delete cache that is affected by an update
 * data format: 
 * array(
 *      'affected_tables' => array()
 * )
 * 
 * @access public
 * @param array
 * @return void
*/
function delete_cache($data = array()) {    
    //Check first if cache is enable
    if (CACHE) {
        $CI = & get_instance();
        $CI->load->driver('cache');
        
        if(isset($data['affected_tables']))
        {
            foreach ($data['affected_tables'] as $at) {
                $table_cache = $CI->cache->memcached->get($at);
                if (!empty($table_cache)) {
                    foreach ($table_cache as $tc) {                        
                        if($CI->cache->memcached->delete($tc))
                        {
                            //echo $at.'='.$tc.'<br>' ;
                        }
                        else
                        {
                            //echo $at.'='.$tc.'-<br>' ;
                        }
                    }
                }
            }
        }
        
        if(isset($data['specific_names']))
        {
            foreach ($data['specific_names'] as $at){                                                
                if($CI->cache->memcached->delete($at))
                {
                   // echo $at.'<br>' ;
                }
                else
                {
                   // echo $at.'-<br>' ;
                }
            }
        }
        //exit();
    }
}

function clear_cached() {
    $CI = & get_instance();
    $CI->load->driver('cache');
    $CI->cache->memcached->clean();
}
/*
 * mime_to_file_extension
 * Get file extension base on mime type
 * 
 * @access public
 * @param string
 * @return string
 * @return bool (on default)
 */
function mime_to_file_extension($contentType)
{
    $map = array(
        'application/pdf'   => '.pdf',
        'application/zip'   => '.zip',
        'image/gif'         => '.gif',
        'image/jpeg'        => '.jpg',
        'image/png'         => '.png',
        'text/css'          => '.css',
        'text/html'         => '.html',
        'text/javascript'   => '.js',
        'text/plain'        => '.txt',
        'text/xml'          => '.xml',
    );
    if (isset($map[$contentType]))
    {
        return $map[$contentType];
    } else {
        return FALSE;
    }
}
/*
 * get_min_folder
 * Get minified resources folder
 * 
 * @access public
 * @param none
 * @return string
 */
function get_min_folder() {
    return (ENVIRONMENT == 'production') ? '.min' : '';
}

function minify($file, $type, $directory) {
    $CI = & get_instance();
    $CI->load->driver('minify');
    
//    $contents = file_get_contents('assets/frontend/css/bootstrap.css'); 
//    $pattern = '/\{glomp_version:(.+?)\}/';
//    preg_match_all($pattern, $contents, $matches);
//    
//    $pattern = '/\{glomp_last_version:(.+?)\}/';
//    preg_match_all($pattern, $contents, $matches_2);
    
    $version_file = '';
    if (file_exists($file)) {
        $version = filemtime($file);
        $file_name = basename($file);
        $file_name = basename($file, '.'.$type);
        $version_file = $directory.'/'.$file_name. '_'.$version.'.'.$type;
        if (! file_exists($version_file)) {
            $entries = scandir($directory.'/');
            $old_version = '!!!!!'; //just to make sure no file exists
            
            foreach($entries as $entry) {
                $pattern = '^'.$file_name. '_.+\.'.$type.'$';
                if (preg_match("/".$pattern."/", $entry, $matches)) {
                    //save name to variable for deletion
                    $old_version = $directory.'/'.$entry;
                }   
            }
            //save new version of the file
            $contents = $CI->minify->$type->min($file);
            $version_file = $directory.'/'.$file_name. '_'.$version.'.'.$type;
            $CI->minify->save_file($contents, $version_file);
            //convert to url path
            $version_file = base_url($directory.'/'.$file_name. '_'.$version.'.'.$type);
            
            if (file_exists($old_version)) {
                unlink($old_version); //delete file
            }
        } else {
            $version_file = base_url($directory.'/'.$file_name. '_'.$version.'.'.$type);
        }
        
        return $version_file;
    }
    return $version_file;
}
function get_protocol() {
    return ($_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http';
}
function get_hsbc_prod_data($prod_id = 0)
{
    $temp = array(
/**FOR LIVE*********************************/
/**FOR LIVE*********************************/
/**FOR LIVE*********************************/
        /*mac allan*/
        "543" => array('sale_price' => 122.50, 'cost_price' => 111.28, 'has_options' => 'no'),
        "544" => array('sale_price' => 180.1, 'cost_price' => 163.71, 'has_options' => 'no'),
        "545" => array('sale_price' => 276.6, 'cost_price' => 251.45, 'has_options' => 'no'),
        "546" => array('sale_price' => 316.7, 'cost_price' => 287.83, 'has_options' => 'no'),
        "548" => array('sale_price' => 447.3, 'cost_price' => 406.6, 'has_options' => 'no'),
        "549" => array('sale_price' => 1294.7, 'cost_price' => 1177, 'has_options' => 'no'),
        "551" => array('sale_price' => 4566.8, 'cost_price' => 4151.6, 'has_options' => 'no'),
        "553" => array('sale_price' => 6226.4, 'cost_price' => 5660.3, 'has_options' => 'no'),

        "618" => array('sale_price' => 171.90, 'cost_price' => 156.22, 'has_options' => 'no'),


        /*snow leaopard vodka*/
        "554" => array('sale_price' => 96.6, 'cost_price' => 87.74, 'has_options' => 'no'),

        /*london dry gin*/
        "556" => array('sale_price' => 101.3, 'cost_price' => 92.02, 'has_options' => 'no'),


        /*singbev*/
        "563" => array('sale_price' => 361.4, 'cost_price' => 328.49, 'has_options' => 'no'),
        "564" => array('sale_price' => 37.7, 'cost_price' => 34.24, 'has_options' => 'no'),
        "565" => array('sale_price' => 61.2, 'cost_price' => 55.64, 'has_options' => 'no'),
        "566" => array('sale_price' => 61.2, 'cost_price' => 55.64, 'has_options' => 'no'),
        "567" => array('sale_price' => 38.9, 'cost_price' => 35.31, 'has_options' => 'no'),
        "568" => array('sale_price' => 41.20, 'cost_price' => 37.45, 'has_options' => 'no'),
        "570" => array('sale_price' => 55.4, 'cost_price' => 50.29, 'has_options' => 'no'),
        "571" => array('sale_price' => 81.3, 'cost_price' => 73.83, 'has_options' => 'no'),
        "572" => array('sale_price' => 153.1, 'cost_price' => 139.1, 'has_options' => 'no'),
        "573" => array('sale_price' => 90.7, 'cost_price' => 82.39, 'has_options' => 'no'),
        "574" => array('sale_price' => 80.1, 'cost_price' => 72.76, 'has_options' => 'no'),
        "575" => array('sale_price' => 73, 'cost_price' => 66.34, 'has_options' => 'no'),
        "576" => array('sale_price' => 88.3, 'cost_price' => 80.25, 'has_options' => 'no'),
        "577" => array('sale_price' => 100.1, 'cost_price' => 90.95, 'has_options' => 'no'),
        "578" => array('sale_price' => 146, 'cost_price' => 132.68, 'has_options' => 'no'),
        "579" => array('sale_price' => 88.3, 'cost_price' => 80.25, 'has_options' => 'no'),
        "580" => array('sale_price' => 188.4, 'cost_price' => 171.2, 'has_options' => 'no'),
        "581" => array('sale_price' => 96.6, 'cost_price' => 87.74, 'has_options' => 'no'),
        "582" => array('sale_price' => 103.6, 'cost_price' => 94.16, 'has_options' => 'no'),
        "583" => array('sale_price' => 339, 'cost_price' => 308.16, 'has_options' => 'no'),
        "584" => array('sale_price' => 102.4, 'cost_price' => 93.09, 'has_options' => 'no'),
        "585" => array('sale_price' => 17.1, 'cost_price' => 24.61, 'has_options' => 'no'),
        "586" => array('sale_price' => 34.2, 'cost_price' => 31.03, 'has_options' => 'no'),
        
        "590" => array('sale_price' => 188.4, 'cost_price' => 171.2, 'has_options' => 'no'),
        "591" => array('sale_price' => 776.9, 'cost_price' => 706.2, 'has_options' => 'no'),

        "587" => array('sale_price' => 80.7, 'cost_price' => 73.295, 'has_options' => 'no'),
        "588" => array('sale_price' => 115.4, 'cost_price' => 104.86, 'has_options' => 'no'),
        "589" => array('sale_price' => 94.2, 'cost_price' => 85.6, 'has_options' => 'no'),



       /*swirls bakeshop*/
        "547" => array('sale_price' => 21.5, 'cost_price' => 19.55, 'has_options' => 'yes',
            'options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '6',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
        ),
        "550" => array('sale_price' => 42.3, 'cost_price' => 38.45, 'has_options' => 'yes','options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '12',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
            ),
        "552" => array('sale_price' => 28.5, 'cost_price' => 25.91, 'has_options' => 'yes','options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '12',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
            ),


        /*daily juice*/
        "557" => array('sale_price' => 249, 'cost_price' => 226.36, 'has_options' => 'no'),
        "558" => array('sale_price' => 422, 'cost_price' => 383.64, 'has_options' => 'no'),
        "559" => array('sale_price' => 167, 'cost_price' => 151.82, 'has_options' => 'no'),
        "560" => array('sale_price' => 280, 'cost_price' => 254.55, 'has_options' => 'no'),


        /*KPO*/
        "561" => array('sale_price' => 85.0, 'cost_price' => 77.27, 'has_options' => 'no'),


        /*NASSIM HILL*/
        "562" => array('sale_price' => 85.0, 'cost_price' => 77.27, 'has_options' => 'no'),

        /*delights heaven*/
        "555" => array('sale_price' => 31.4, 'cost_price' => 28.55, 'has_options' => 'no'),
/**FOR LIVE*********************************/
/**FOR LIVE*********************************/
/**FOR LIVE*********************************/     


/**FOR DEV*********************************/ 
/**FOR DEV*********************************/ 
/**FOR DEV*********************************/ 

        /*mac allan*/
        "434" => array('sale_price' => 117.7, 'cost_price' => 107, 'has_options' => 'no'),
        "435" => array('sale_price' => 180.1, 'cost_price' => 163.71, 'has_options' => 'no'),
        "436" => array('sale_price' => 276.6, 'cost_price' => 251.45, 'has_options' => 'no'),
        "437" => array('sale_price' => 316.7, 'cost_price' => 287.83, 'has_options' => 'no'),
        "438" => array('sale_price' => 447.3, 'cost_price' => 406.6, 'has_options' => 'no'),
        "439" => array('sale_price' => 1294.7, 'cost_price' => 1177, 'has_options' => 'no'),
        "440" => array('sale_price' => 4566.8, 'cost_price' => 4151.6, 'has_options' => 'no'),
        "441" => array('sale_price' => 6226.4, 'cost_price' => 5660.3, 'has_options' => 'no'),


        /*snow leaopard vodka*/
        "399" => array('sale_price' => 96.6, 'cost_price' => 87.74, 'has_options' => 'no'),

        /*london dry gin*/
        "400" => array('sale_price' => 101.3, 'cost_price' => 92.02, 'has_options' => 'no'),


        /*singbev*/
        "401" => array('sale_price' => 361.4, 'cost_price' => 328.49, 'has_options' => 'no'),
        "402" => array('sale_price' => 37.7, 'cost_price' => 34.24, 'has_options' => 'no'),
        "403" => array('sale_price' => 61.2, 'cost_price' => 55.64, 'has_options' => 'no'),
        "404" => array('sale_price' => 61.2, 'cost_price' => 55.64, 'has_options' => 'no'),
        "405" => array('sale_price' => 38.9, 'cost_price' => 35.31, 'has_options' => 'no'),
        "406" => array('sale_price' => 14.2, 'cost_price' => 12.84, 'has_options' => 'no'),
        "407" => array('sale_price' => 55.4, 'cost_price' => 50.29, 'has_options' => 'no'),
        "408" => array('sale_price' => 81.3, 'cost_price' => 73.83, 'has_options' => 'no'),
        "409" => array('sale_price' => 153.1, 'cost_price' => 139.1, 'has_options' => 'no'),
        "410" => array('sale_price' => 90.7, 'cost_price' => 82.39, 'has_options' => 'no'),
        "411" => array('sale_price' => 80.1, 'cost_price' => 72.76, 'has_options' => 'no'),
        "412" => array('sale_price' => 73, 'cost_price' => 66.34, 'has_options' => 'no'),
        "413" => array('sale_price' => 88.3, 'cost_price' => 80.25, 'has_options' => 'no'),
        "414" => array('sale_price' => 100.1, 'cost_price' => 90.95, 'has_options' => 'no'),
        "415" => array('sale_price' => 146, 'cost_price' => 132.68, 'has_options' => 'no'),
        "416" => array('sale_price' => 88.3, 'cost_price' => 80.25, 'has_options' => 'no'),
        "417" => array('sale_price' => 188.4, 'cost_price' => 171.2, 'has_options' => 'no'),
        "418" => array('sale_price' => 96.6, 'cost_price' => 87.74, 'has_options' => 'no'),
        "419" => array('sale_price' => 103.6, 'cost_price' => 94.16, 'has_options' => 'no'),
        "420" => array('sale_price' => 339, 'cost_price' => 308.16, 'has_options' => 'no'),
        "421" => array('sale_price' => 102.4, 'cost_price' => 93.09, 'has_options' => 'no'),
        "422" => array('sale_price' => 17.1, 'cost_price' => 24.61, 'has_options' => 'no'),
        "423" => array('sale_price' => 34.2, 'cost_price' => 31.03, 'has_options' => 'no'),
        
        "425" => array('sale_price' => 188.4, 'cost_price' => 171.2, 'has_options' => 'no'),
        "426" => array('sale_price' => 776.9, 'cost_price' => 706.2, 'has_options' => 'no'),
        "444" => array('sale_price' => 80.7, 'cost_price' => 73.295, 'has_options' => 'no'),
        "445" => array('sale_price' => 115.4, 'cost_price' => 104.86, 'has_options' => 'no'),
        "446" => array('sale_price' => 94.2, 'cost_price' => 85.6, 'has_options' => 'no'),



       /*swirls bakeshop*/
        "427" => array('sale_price' => 21.5, 'cost_price' => 19.55, 'has_options' => 'yes',
            'options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '6',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
        ),
        "428" => array('sale_price' => 42.3, 'cost_price' => 38.45, 'has_options' => 'yes','options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '12',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
            ),
        "429" => array('sale_price' => 28.5, 'cost_price' => 25.91, 'has_options' => 'yes','options' => array(
                    'option_name' => 'Flavors',
                    'option_count' => '12',
                    'option_list' => array(
                            array ( 'name' => 'Very Vanilla',
                                    'desc' => 'Madagascar vanilla cupcake with fluffy vanilla buttercream frosting',
                                ),
                            array ( 'name' => 'Milk chocolate',
                                    'desc' => 'Moist Milk Chocolate Cupcake with Vanilla Buttercream Frosting Topped with Colourful M&M Candy',
                                ),
                            array ( 'name' => 'Oh so cocoa',
                                    'desc' => 'Cocoa berry cupcake dark chocolate buttercream frosting dipped in chocolate sprinkles',
                                ),
                            array ( 'name' => 'What\'s up doc?',
                                    'desc' => 'Aromatic carrot cupcake packed with pineapple & walnuts with smooth cream cheese frosting sprinkled with crushed walnuts',
                                ),
                            array ( 'name' => 'Red velvet',
                                    'desc' => 'Crimson chocolate cupcake delicious southern style with cream cheese frosting',
                                ),
                            array ( 'name' => 'Nuts about you',
                                    'desc' => 'Milk chocolate cupcake with smooth peanut butter frosting topped with peanut m&m',
                                ),
                            array ( 'name' => 'Nutella',
                                    'desc' => 'Chocolate cupcake with an oozy nutella center & hazelnut frosting topped with roasted hazenuts',
                                ),
                            array ( 'name' => 'Strawberry burst',
                                    'desc' => 'Juicy & fresh strawberry white chocolate cupcake frosted with strawberry cream cheese frosting',
                                ),
                            array ( 'name' => 'Vanilla chocolate',
                                    'desc' => 'Moist madagascar vanilla cupcake swirled with chocolate frosting',
                                ),
                            array ( 'name' => 'Bounty',
                                    'desc' => 'Vanilla cupcake with coconut buttercream and snowflake frosting',
                                ),
                            array ( 'name' => 'Cookies & cream',
                                    'desc' => 'Chocolate cupcake with cookies & cream frosting',
                                ),
                            array ( 'name' => 'Mint chocolate bliss',
                                    'desc' => 'Chocolate cupcake with peppermint buttercream frosting',
                                ),
                        )
                )
            ),


        /*daily juice*/
        "430" => array('sale_price' => 249, 'cost_price' => 226.36, 'has_options' => 'no'),
        "431" => array('sale_price' => 422, 'cost_price' => 383.64, 'has_options' => 'no'),
        "432" => array('sale_price' => 167, 'cost_price' => 151.82, 'has_options' => 'no'),
        "433" => array('sale_price' => 280, 'cost_price' => 254.55, 'has_options' => 'no'),


        /*KPO*/
        "442" => array('sale_price' => 85.0, 'cost_price' => 77.27, 'has_options' => 'no'),


        /*NASSIM HILL*/
        "443" => array('sale_price' => 85.0, 'cost_price' => 77.27, 'has_options' => 'no'),

        /*delights heaven*/
        "448" => array('sale_price' => 31.4, 'cost_price' => 28.55, 'has_options' => 'no'),

        /*valrhona*/
        "655" => array('sale_price' => 39.0, 'cost_price' => 36.0, 'has_options' => 'no'),
        "656" => array('sale_price' => 48.3, 'cost_price' => 43.85, 'has_options' => 'no'),
        "657" => array('sale_price' => 48.3, 'cost_price' => 43.85, 'has_options' => 'no'),
        "658" => array('sale_price' => 32.40, 'cost_price' => 29.45, 'has_options' => 'no'),

        "659" => array('sale_price' => 32.40, 'cost_price' => 29.45, 'has_options' => 'no'),
        "660" => array('sale_price' => 71.30, 'cost_price' => 64.80, 'has_options' => 'no'),
        "661" => array('sale_price' => 63.40, 'cost_price' => 57.60, 'has_options' => 'no'),
        "662" => array('sale_price' => 56.20, 'cost_price' => 51.05, 'has_options' => 'no'),

        /*laurent perrier */
        "738" => array('sale_price' => 74.00, 'cost_price' => 67.29, 'has_options' => 'no'),
        "739" => array('sale_price' => 126.50, 'cost_price' => 114.95, 'has_options' => 'no'),

        /*the providore */
        "735" => array('sale_price' => 74.30, 'cost_price' => 67.52, 'has_options' => 'no'),
        "736" => array('sale_price' => 96.20, 'cost_price' => 87.38, 'has_options' => 'no'),
        "737" => array('sale_price' => 122.40, 'cost_price' => 111.21, 'has_options' => 'no'),

        /*Glenfiddich */
        "734" => array('sale_price' => 173.80, 'cost_price' => 158.00, 'has_options' => 'no'),

         /*Moet & Chandon */
        "740" => array('sale_price' => 93.50, 'cost_price' => 85.00, 'has_options' => 'no'),

        /*kusmi tea */
        "731" => array('sale_price' => 175.70, 'cost_price' => 158.13, 'has_options' => 'no'),
        "732" => array('sale_price' => 147.70, 'cost_price' => 132.90, 'has_options' => 'no'),
        "733" => array('sale_price' => 149.60, 'cost_price' => 134.58, 'has_options' => 'no'),

        /*Euraco*/
        "741" => array('sale_price' => 416.00, 'cost_price' => 349.91, 'has_options' => 'no'),
        "742" => array('sale_price' => 267.00, 'cost_price' => 224.58, 'has_options' => 'no'),
        "744" => array('sale_price' => 153.00, 'cost_price' => 128.69, 'has_options' => 'no'),
/**FOR DEV*********************************/ 
/**FOR DEV*********************************/ 
/**FOR DEV*********************************/ 
    );

    if($prod_id>0 && isset($temp[$prod_id]) ) 
    {
        return $temp[$prod_id];
    }
    return false;

}
function is_sold_out($prod_id=0)
{
    $list =  array(655,656,657,658,659,660);
    if(in_array($prod_id, $list))
    {
        return true;
    }
    else
    {
        return false;
    }
}
function get_winestore_prod_data($id = 0) {

	$temp = array(
//            "5" => array('group' => 1, 'base_price' => 47.00, 'a_20' => 10, 'a_30' => 40.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "10" => array('group' => 2, 'base_price' => 47.00, 'a_20' => 20, 'a_30' => 80.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "11" => array('group' => 3, 'base_price' => 47.00, 'a_20' => 57.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "8" => array('group' => 4, 'base_price' => 47.00, 'a_20' => 67.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//	
	
            "349" => array('group' => 1, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
            "350" => array('group' => 1, 'base_price' => 46.00, 'a_20' => 36.80, 'a_30' => 32.20, 'dollar_off_20' => 9.20, 'dollar_off_30' => 13.80, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "352" => array('group' => 1, 'base_price' => 36.80, 'a_20' => 32.20, 'a_30' => 9.20, 'dollar_off_20' => 13.80, 'dollar_off_30' => 31.28, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "354" => array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "355" => array('group' => 1, 'base_price' => 66.00, 'a_20' => 52.80, 'a_30' => 46.20, 'dollar_off_20' => 13.20, 'dollar_off_30' => 19.80, 'offer_20' => 44.88, 'offer_30' => 39.27),
            "356"=> array('group' => 1, 'base_price' => 66.00, 'a_20' => 52.80, 'a_30' => 46.20, 'dollar_off_20' => 13.20, 'dollar_off_30' => 19.80, 'offer_20' => 44.88, 'offer_30' => 39.27),
            "358"=> array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "360"=> array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "361"=> array('group' => 1, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15),
            "362"=> array('group' => 1, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15),
            "365"=> array('group' => 2, 'base_price' => 39.00, 'a_20' => 31.20, 'a_30' => 27.30, 'dollar_off_20' => 7.80, 'dollar_off_30' => 11.70, 'offer_20' => 26.52, 'offer_30' => 23.20),
            "367"=> array('group' => 2, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69),
            "369"=> array('group' => 2, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "371"=> array('group' => 2, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.20, 'offer_30' => 23.80),
            "372"=> array('group' => 2, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69),
            "373"=> array('group' => 2, 'base_price' => 42.50, 'a_20' => 34.00, 'a_30' => 29.75, 'dollar_off_20' => 8.50, 'dollar_off_30' => 12.75, 'offer_20' => 28.90, 'offer_30' => 25.29),
            "375"=> array('group' => 2, 'base_price' => 41.00, 'a_20' => 32.80, 'a_30' => 28.70, 'dollar_off_20' => 8.20, 'dollar_off_30' => 12.30, 'offer_20' => 27.88, 'offer_30' => 24.39),
            "377"=> array('group' => 2, 'base_price' => 44.00, 'a_20' => 35.20, 'a_30' => 30.80, 'dollar_off_20' => 8.80, 'dollar_off_30' => 13.20, 'offer_20' => 29.92, 'offer_30' => 26.18),
            "379"=> array('group' => 3, 'base_price' => 51.00, 'a_20' => 40.80, 'a_30' => 35.70, 'dollar_off_20' => 10.20, 'dollar_off_30' => 15.30, 'offer_20' => 34.68, 'offer_30' => 30.34),
            "381"=> array('group' => 3, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.60, 'offer_30' => 26.77),
            "382"=> array('group' => 3, 'base_price' => 44.00, 'a_20' => 35.20, 'a_30' => 30.80, 'dollar_off_20' => 8.80, 'dollar_off_30' => 13.20, 'offer_20' => 29.92, 'offer_30' => 26.18),
            "384"=> array('group' => 3, 'base_price' => 37.00, 'a_20' => 29.60, 'a_30' => 25.90, 'dollar_off_20' => 7.40, 'dollar_off_30' => 11.10, 'offer_20' => 25.16, 'offer_30' => 22.01),
            "386"=> array('group' => 3, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "351"=> array('group' => 3, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "353"=> array('group' => 3, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "357"=> array('group' => 3, 'base_price' => 40.50, 'a_20' => 32.40, 'a_30' => 28.35, 'dollar_off_20' => 8.10, 'dollar_off_30' => 12.15, 'offer_20' => 27.54, 'offer_30' => 24.1),
            "359"=> array('group' => 3, 'base_price' => 37.50, 'a_20' => 30.00, 'a_30' => 26.25, 'dollar_off_20' => 7.50, 'dollar_off_30' => 11.25, 'offer_20' => 25.5,  'offer_30' => 22.31),
            "363"=> array('group' => 3, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "364"=> array('group' => 3, 'base_price' => 37.00, 'a_20' => 29.60, 'a_30' => 25.90, 'dollar_off_20' => 7.40, 'dollar_off_30' => 11.10, 'offer_20' => 25.16, 'offer_30' => 22.01),
            "366"=> array('group' => 3, 'base_price' => 35.50, 'a_20' => 28.40, 'a_30' => 24.85, 'dollar_off_20' => 7.10, 'dollar_off_30' => 10.65, 'offer_20' => 24.14, 'offer_30' => 21.12),
            "368"=> array('group' => 3, 'base_price' => 34.50, 'a_20' => 27.60, 'a_30' => 24.15, 'dollar_off_20' => 6.90, 'dollar_off_30' => 10.35, 'offer_20' => 23.46, 'offer_30' => 20.53),
            "370"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "374"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "376"=> array('group' => 4, 'base_price' => 40.50, 'a_20' => 32.40, 'a_30' => 28.35, 'dollar_off_20' => 8.10, 'dollar_off_30' => 12.15, 'offer_20' => 27.54, 'offer_30' => 24.1),
            "378"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "380"=> array('group' => 5, 'base_price' => 43.50, 'a_20' => 34.80, 'a_30' => 30.45, 'dollar_off_20' => 8.70, 'dollar_off_30' => 13.05, 'offer_20' => 29.58, 'offer_30' => 25.88),
            "383"=> array('group' => 5, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "385"=> array('group' => 5, 'base_price' => 46.00, 'a_20' => 36.80, 'a_30' => 32.20, 'dollar_off_20' => 9.20, 'dollar_off_30' => 13.80, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "387"=> array('group' => 5, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "388"=> array('group' => 5, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "389"=> array('group' => 5, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
            "390"=> array('group' => 5, 'base_price' => 54.00, 'a_20' => 43.20, 'a_30' => 37.80, 'dollar_off_20' => 10.80, 'dollar_off_30' => 16.20, 'offer_20' => 36.72, 'offer_30' => 32.13),
            "391"=> array('group' => 5, 'base_price' => 38.00, 'a_20' => 30.40, 'a_30' => 26.60, 'dollar_off_20' => 7.60, 'dollar_off_30' => 11.40, 'offer_20' => 25.84, 'offer_30' => 22.61),
            "392"=> array('group' => 5, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69), 
            "393"=> array('group' => 5, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15)
        );
	if($id>0 && isset($temp[$id]) )	
	{
            return $temp[$id];
	}
	return false;
	
}
function get_winestore($type = 'id') {
	if ($type == 'prod_id') {
		return array(
//			5,
//			10,
//			11,
//			8,
			
			
			
            349,
            350,
            352,
            354,
            355,
            356,
            358,
            360,
            361,
            362,
            365,
            367,
            369,
            371,
            372,
            373,
            375,
            377,
            379,
            381,
            382,
            384,
            386,
            351,
            353,
            357,
            359,
            363,
            364,
            366,
            368,
            370,
            374,
            376,
            378,
            380,
            383,
            385,
            387,
            388,
            389,
            390,
            391,
            392,
            393,
        );
	}		
    else if ($type == 'id') {
        return array(
//            "5" => array('group' => 1, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "10" => array('group' => 2, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "11" => array('group' => 3, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
//            "8" => array('group' => 4, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
            
            "349" => array('group' => 1, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
            "350" => array('group' => 1, 'base_price' => 46.00, 'a_20' => 36.80, 'a_30' => 32.20, 'dollar_off_20' => 9.20, 'dollar_off_30' => 13.80, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "352" => array('group' => 1, 'base_price' => 36.80, 'a_20' => 32.20, 'a_30' => 9.20, 'dollar_off_20' => 13.80, 'dollar_off_30' => 31.28, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "354" => array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "355" => array('group' => 1, 'base_price' => 66.00, 'a_20' => 52.80, 'a_30' => 46.20, 'dollar_off_20' => 13.20, 'dollar_off_30' => 19.80, 'offer_20' => 44.88, 'offer_30' => 39.27),
            "356"=> array('group' => 1, 'base_price' => 66.00, 'a_20' => 52.80, 'a_30' => 46.20, 'dollar_off_20' => 13.20, 'dollar_off_30' => 19.80, 'offer_20' => 44.88, 'offer_30' => 39.27),
            "358"=> array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "360"=> array('group' => 1, 'base_price' => 50.00, 'a_20' => 40.00, 'a_30' => 35.00, 'dollar_off_20' => 10.00, 'dollar_off_30' => 15.00, 'offer_20' => 34.00, 'offer_30' => 29.75),
            "361"=> array('group' => 1, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15),
            "362"=> array('group' => 1, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15),
            "365"=> array('group' => 2, 'base_price' => 39.00, 'a_20' => 31.20, 'a_30' => 27.30, 'dollar_off_20' => 7.80, 'dollar_off_30' => 11.70, 'offer_20' => 26.52, 'offer_30' => 23.20),
            "367"=> array('group' => 2, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69),
            "369"=> array('group' => 2, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "371"=> array('group' => 2, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.20, 'offer_30' => 23.80),
            "372"=> array('group' => 2, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69),
            "373"=> array('group' => 2, 'base_price' => 42.50, 'a_20' => 34.00, 'a_30' => 29.75, 'dollar_off_20' => 8.50, 'dollar_off_30' => 12.75, 'offer_20' => 28.90, 'offer_30' => 25.29),
            "375"=> array('group' => 2, 'base_price' => 41.00, 'a_20' => 32.80, 'a_30' => 28.70, 'dollar_off_20' => 8.20, 'dollar_off_30' => 12.30, 'offer_20' => 27.88, 'offer_30' => 24.39),
            "377"=> array('group' => 2, 'base_price' => 44.00, 'a_20' => 35.20, 'a_30' => 30.80, 'dollar_off_20' => 8.80, 'dollar_off_30' => 13.20, 'offer_20' => 29.92, 'offer_30' => 26.18),
            "379"=> array('group' => 3, 'base_price' => 51.00, 'a_20' => 40.80, 'a_30' => 35.70, 'dollar_off_20' => 10.20, 'dollar_off_30' => 15.30, 'offer_20' => 34.68, 'offer_30' => 30.34),
            "381"=> array('group' => 3, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.60, 'offer_30' => 26.77),
            "382"=> array('group' => 3, 'base_price' => 44.00, 'a_20' => 35.20, 'a_30' => 30.80, 'dollar_off_20' => 8.80, 'dollar_off_30' => 13.20, 'offer_20' => 29.92, 'offer_30' => 26.18),
            "384"=> array('group' => 3, 'base_price' => 37.00, 'a_20' => 29.60, 'a_30' => 25.90, 'dollar_off_20' => 7.40, 'dollar_off_30' => 11.10, 'offer_20' => 25.16, 'offer_30' => 22.01),
            "386"=> array('group' => 3, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "351"=> array('group' => 3, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "353"=> array('group' => 3, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "357"=> array('group' => 3, 'base_price' => 40.50, 'a_20' => 32.40, 'a_30' => 28.35, 'dollar_off_20' => 8.10, 'dollar_off_30' => 12.15, 'offer_20' => 27.54, 'offer_30' => 24.1),
            "359"=> array('group' => 3, 'base_price' => 37.50, 'a_20' => 30.00, 'a_30' => 26.25, 'dollar_off_20' => 7.50, 'dollar_off_30' => 11.25, 'offer_20' => 25.5,  'offer_30' => 22.31),
            "363"=> array('group' => 3, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "364"=> array('group' => 3, 'base_price' => 37.00, 'a_20' => 29.60, 'a_30' => 25.90, 'dollar_off_20' => 7.40, 'dollar_off_30' => 11.10, 'offer_20' => 25.16, 'offer_30' => 22.01),
            "366"=> array('group' => 3, 'base_price' => 35.50, 'a_20' => 28.40, 'a_30' => 24.85, 'dollar_off_20' => 7.10, 'dollar_off_30' => 10.65, 'offer_20' => 24.14, 'offer_30' => 21.12),
            "368"=> array('group' => 3, 'base_price' => 34.50, 'a_20' => 27.60, 'a_30' => 24.15, 'dollar_off_20' => 6.90, 'dollar_off_30' => 10.35, 'offer_20' => 23.46, 'offer_30' => 20.53),
            "370"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "374"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "376"=> array('group' => 4, 'base_price' => 40.50, 'a_20' => 32.40, 'a_30' => 28.35, 'dollar_off_20' => 8.10, 'dollar_off_30' => 12.15, 'offer_20' => 27.54, 'offer_30' => 24.1),
            "378"=> array('group' => 4, 'base_price' => 40.00, 'a_20' => 32.00, 'a_30' => 28.00, 'dollar_off_20' => 8.00, 'dollar_off_30' => 12.00, 'offer_20' => 27.2,  'offer_30' => 23.8),
            "380"=> array('group' => 5, 'base_price' => 43.50, 'a_20' => 34.80, 'a_30' => 30.45, 'dollar_off_20' => 8.70, 'dollar_off_30' => 13.05, 'offer_20' => 29.58, 'offer_30' => 25.88),
            "383"=> array('group' => 5, 'base_price' => 43.00, 'a_20' => 34.40, 'a_30' => 30.10, 'dollar_off_20' => 8.60, 'dollar_off_30' => 12.90, 'offer_20' => 29.24, 'offer_30' => 25.58),
            "385"=> array('group' => 5, 'base_price' => 46.00, 'a_20' => 36.80, 'a_30' => 32.20, 'dollar_off_20' => 9.20, 'dollar_off_30' => 13.80, 'offer_20' => 31.28, 'offer_30' => 27.37),
            "387"=> array('group' => 5, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "388"=> array('group' => 5, 'base_price' => 45.00, 'a_20' => 36.00, 'a_30' => 31.50, 'dollar_off_20' => 9.00, 'dollar_off_30' => 13.50, 'offer_20' => 30.6,  'offer_30' => 26.77),
            "389"=> array('group' => 5, 'base_price' => 47.00, 'a_20' => 37.60, 'a_30' => 32.90, 'dollar_off_20' => 9.40, 'dollar_off_30' => 14.10, 'offer_20' => 31.96, 'offer_30' => 27.96),
            "390"=> array('group' => 5, 'base_price' => 54.00, 'a_20' => 43.20, 'a_30' => 37.80, 'dollar_off_20' => 10.80, 'dollar_off_30' => 16.20, 'offer_20' => 36.72, 'offer_30' => 32.13),
            "391"=> array('group' => 5, 'base_price' => 38.00, 'a_20' => 30.40, 'a_30' => 26.60, 'dollar_off_20' => 7.60, 'dollar_off_30' => 11.40, 'offer_20' => 25.84, 'offer_30' => 22.61),
            "392"=> array('group' => 5, 'base_price' => 41.50, 'a_20' => 33.20, 'a_30' => 29.05, 'dollar_off_20' => 8.30, 'dollar_off_30' => 12.45, 'offer_20' => 28.22, 'offer_30' => 24.69), 
            "393"=> array('group' => 5, 'base_price' => 49.00, 'a_20' => 39.20, 'a_30' => 34.30, 'dollar_off_20' => 9.80, 'dollar_off_30' => 14.70, 'offer_20' => 33.32, 'offer_30' => 29.15)
        );
    } else {
        return array(
            "1" => array(
                "name" => "<div id='wrapper' style='position:relative;max-width:100%;margin:0 auto;'>
                        <div style='float:left;max-width:20%;'>
                                <img style='float:left;width:100%;' src='https://glomp.it/custom/uploads/products/3851_d41d8cd98f00b204e9800998ecf8427e_1435927755_s.jpg'/>
                        </div>
                        <div style='margin-top:1%;margin-left:10px;float:left;max-width:75%;font-weight: bold;'>

                                THE SHAKY BRIDGE WINERY<br>
                                New Zealand, Central Otago
                        </div>
                </div>",
                "desc" => "
                <div id='wrapper' style='width:100%;margin:0 auto;'>
                        <div style=' float:left;width:107px;'>
                                <img src='https://glomp.it/custom/uploads/products/3851_d41d8cd98f00b204e9800998ecf8427e_1435927755_s.jpg'/>
                        </div>
                        <div style='margin-top:10px;margin-left:10px;float:left;width:520px;font-weight: bold;'>

                                THE SHAKY BRIDGE WINERY<br>
                                New Zealand, Central Otago<br><br><br><br>
                        </div>
                </div>
                <div>
                    <p>
                        Growing grapes in Central Otago for many years is the difference that defines Shaky Bridge Wines. In 1973, Bill and Gill Grant planted some vines on their 25 acre block on Dunstan Rd just outside the township of Alexandra to 'see what might happen'. Some Muller Thurgau and Chasselas were planted way back then, but Bill and Gill had an inspired moment a few years later when they heard they could get hold of some Pinot Noir and Gewurztraminer vines.
                    </p>
                    <p>	
                        A shipment of whole plant Pinot Noir vines arrived from Akaroa, near Christchurch, in the late 1970s and Bill and Gill went to work setting up one of the first Pinot Noir vineyards in Central Otago. There weren't too many believers in Central Otago wines back then, but the Grant's travels around the world had them convinced they could make something happen. We're most happy they ignored the naysayers and blazed a pioneering trail for Central Otago wine.
                    </p>
                    <p>	
                        Our winemaker, Dave Grant, learned his craft during the 1990s and has been making wines for Shaky Bridge since the first release in 2002. A single Pinot Noir wine was made in that first vintage by coaxing the intensely flavored fruit through the winemaking process and in to the bottle. Crafting wines is the central theme of the Shaky Bridge approach to winemaking; get the raw material in the vineyard as best as it can be and gently extract the best flavors to deliver wines of finesse, elegance and substance. Intervention is minimal.
                    </p>
                    <div style='margin-left:20px;'>
                        <ul>
                                <li>
                                        Shaky Bridge Wines are handcrafted to the highest quality standards in each brand tier.
                                </li>
                                <li>
                                        We have complete control over our grape growing and therefore the quality of fruit used to make our wines.
                                </li>
                                <li>
                                        We do not rush the winemaking process, using time to allow the 'right' flavors to develop in barrel or bottle before we release a wine for sale.
                                </li>
                                <li>
                                        Our flagship Pinot Noir is kept in bottle for a minimum of 12 months to allow bottle maturation to develop flavors, integrate components and to provide an exceptional wine experience. Our wines are made to move you.
                                </li>
                                <li>
                                        We use only small 'barrique' barrels (225 litres) to mature all our Pinot Noir and our Pont Tremblant Chardonnay.
                                </li>
                                <li>
                                        We know the importance of crafting flavorful, memorable, well structured and balanced wines for your enjoyment.
                                </li>
                                <li>
                                        That notion is the core of our philosophy. Everything we do each day, each week and each vintage, is done to achieve that goal.
                                </li>
                        </ul>
                    </div>
                </div>",
                "desc_m" => "
                    <div>
                        <p>
                            Growing grapes in Central Otago for many years is the difference that defines Shaky Bridge Wines. In 1973, Bill and Gill Grant planted some vines on their 25 acre block on Dunstan Rd just outside the township of Alexandra to 'see what might happen'. Some Muller Thurgau and Chasselas were planted way back then, but Bill and Gill had an inspired moment a few years later when they heard they could get hold of some Pinot Noir and Gewurztraminer vines.
                        </p>
                        <p>	
                            A shipment of whole plant Pinot Noir vines arrived from Akaroa, near Christchurch, in the late 1970s and Bill and Gill went to work setting up one of the first Pinot Noir vineyards in Central Otago. There weren't too many believers in Central Otago wines back then, but the Grant's travels around the world had them convinced they could make something happen. We're most happy they ignored the naysayers and blazed a pioneering trail for Central Otago wine.
                        </p>
                        <p>	
                            Our winemaker, Dave Grant, learned his craft during the 1990s and has been making wines for Shaky Bridge since the first release in 2002. A single Pinot Noir wine was made in that first vintage by coaxing the intensely flavored fruit through the winemaking process and in to the bottle. Crafting wines is the central theme of the Shaky Bridge approach to winemaking; get the raw material in the vineyard as best as it can be and gently extract the best flavors to deliver wines of finesse, elegance and substance. Intervention is minimal.
                        </p>
                        <div style='margin-left:20px;'>
                            <ul>
                                    <li>
                                            Shaky Bridge Wines are handcrafted to the highest quality standards in each brand tier.
                                    </li>
                                    <li>
                                            We have complete control over our grape growing and therefore the quality of fruit used to make our wines.
                                    </li>
                                    <li>
                                            We do not rush the winemaking process, using time to allow the 'right' flavors to develop in barrel or bottle before we release a wine for sale.
                                    </li>
                                    <li>
                                            Our flagship Pinot Noir is kept in bottle for a minimum of 12 months to allow bottle maturation to develop flavors, integrate components and to provide an exceptional wine experience. Our wines are made to move you.
                                    </li>
                                    <li>
                                            We use only small 'barrique' barrels (225 litres) to mature all our Pinot Noir and our Pont Tremblant Chardonnay.
                                    </li>
                                    <li>
                                            We know the importance of crafting flavorful, memorable, well structured and balanced wines for your enjoyment.
                                    </li>
                                    <li>
                                            That notion is the core of our philosophy. Everything we do each day, each week and each vintage, is done to achieve that goal.
                                    </li>
                            </ul>
                        </div>
                    </div>"
            ),
            "2" => array(
						"name" => "<div id='wrapper' style='position:relative;max-width:100%;margin:0 auto;'>
							<div style='float:left;max-width:20%;'>
								<img style='float:left;width:100%;' src='https://glomp.it/custom/uploads/products/8807_d41d8cd98f00b204e9800998ecf8427e_1435925950_k.jpg'/>
							</div>
							<div style='margin-top:1%;margin-left:10px;float:left;max-width:75%;font-weight: bold;'>
								
								THE KAHURANGI ESTATE WINERY - TROUT VALLEY<br>
                                New Zealand, Nelson
							</div>
						</div>",
						"desc" => "
						<div id='wrapper' style='width:100%;margin:0 auto;'>
							<div style=' float:left;width:107px;'>
								<img src='https://glomp.it/custom/uploads/products/8807_d41d8cd98f00b204e9800998ecf8427e_1435925950_k.jpg'/>
							</div>
							<div style='margin-top:10px;margin-left:10px;float:left;width:520px;font-weight: bold;'>
								THE KAHURANGI ESTATE WINERY - TROUT VALLEY<br>
                                New Zealand, Nelson<br><br><br><br>
							</div>
						</div>
				<div>
				<p>	
                Kahurangi Estate, the maker of Trout Valley wines, is a family owned boutique vineyard dedicated to producing hand-made premium quality wines in a truly sustainable manner. Our focus is on aromatic grape varieties that respond to the Nelson region's cool climate conditions. The vineyard was planted in 1973 and has the oldest commercial Riesling vines in the South Island. 
				</p><p>	
                Greg and Amanda Day moved back home to New Zealand with their young family in 1998, to purchase the existing Vineyard in Upper Moutere and develop the Kahurangi brand. Today we continue to be involved in all aspects of the vineyard, winemaking and marketing with our daughter, Samantha joining the family business in 2008. We are proud to stand together with our Kahurangi team and present wines to you that reflect the true flavours and beauty of the Nelson region.
				</p><p>		
                Trout Valley Wines are from Nelson, New Zealand. The Nelson region includes three National Parks, the largest being Kahurangi, known for its great beauty and is home to some of the finest Brown Trout fishing water in the world.
				</p><p>	
                Trout Valley Wines are made from selected grapes that are processed with great care to produce premium standard wine. Grapes for Trout Valley are sourced from seven contract grape growers through the Nelson region. These vineyard sites all have their own characteristics and allow the winery a range of blending options to ensure we produce consistent quality from vintage to vintage.
				</p><p>		
                Neil Todd has been with Kahurangi Estate since April 2001, starting as assistant in the Winery under the guidance of Daniel Schwarzenbach. When Daniel left in 2004, Neil stepped up to the role as Chief Winemaker. He kick-started his new career with a Gold Medal and Trophy in the International Wine Challenge for our Kahurangi Unwooded Chardonnay 2005. Since then Neil has gone on to produce many Gold, Silver and Bronze medals for Kahurangi Estate and we are very proud to have him as our Chief Winemaker. Emmanuelle David joined our winemaking team in 2013. Originally from France, Emmanuelle started her apprenticeship in the Burgundy region while studying viticulture and oenology. As any good winemaker student should Emmanuelle travelled and worked harvests in South Africa and Australia before finishing her degree back home in France. Now a qualified oenologist, Emmanuelle and her husband moved here to New Zealand in 2006 and settled here in sunny Nelson in 2008.
				</p>
				</div>
				",
                "desc_m" => "
                    <div>
                        <p>Kahurangi Estate, the maker of Trout Valley wines, is a family owned boutique vineyard dedicated to producing hand-made premium quality wines in a truly sustainable manner. Our focus is on aromatic grape varieties that respond to the Nelson region's cool climate conditions. The vineyard was planted in 1973 and has the oldest commercial Riesling vines in the South Island. </p>
                        <p>Greg and Amanda Day moved back home to New Zealand with their young family in 1998, to purchase the existing Vineyard in Upper Moutere and develop the Kahurangi brand. Today we continue to be involved in all aspects of the vineyard, winemaking and marketing with our daughter, Samantha joining the family business in 2008. We are proud to stand together with our Kahurangi team and present wines to you that reflect the true flavours and beauty of the Nelson region.</p>
                        <p>Trout Valley Wines are from Nelson, New Zealand. The Nelson region includes three National Parks, the largest being Kahurangi, known for its great beauty and is home to some of the finest Brown Trout fishing water in the world.</p>
                        <p>Trout Valley Wines are made from selected grapes that are processed with great care to produce premium standard wine. Grapes for Trout Valley are sourced from seven contract grape growers through the Nelson region. These vineyard sites all have their own characteristics and allow the winery a range of blending options to ensure we produce consistent quality from vintage to vintage.</p>
                        <p>Neil Todd has been with Kahurangi Estate since April 2001, starting as assistant in the Winery under the guidance of Daniel Schwarzenbach. When Daniel left in 2004, Neil stepped up to the role as Chief Winemaker. He kick-started his new career with a Gold Medal and Trophy in the International Wine Challenge for our Kahurangi Unwooded Chardonnay 2005. Since then Neil has gone on to produce many Gold, Silver and Bronze medals for Kahurangi Estate and we are very proud to have him as our Chief Winemaker. Emmanuelle David joined our winemaking team in 2013. Originally from France, Emmanuelle started her apprenticeship in the Burgundy region while studying viticulture and oenology. As any good winemaker student should Emmanuelle travelled and worked harvests in South Africa and Australia before finishing her degree back home in France. Now a qualified oenologist, Emmanuelle and her husband moved here to New Zealand in 2006 and settled here in sunny Nelson in 2008.</p>
                    </div>"
                ),
            "3" => array(
						"name" => "<div id='wrapper' style='position:relative;max-width:100%;margin:0 auto;'>
										<div style='float:left;max-width:20%;'>
											<img style='float:left;width:100%;' src='https://glomp.it/custom/uploads/products/7993_d41d8cd98f00b204e9800998ecf8427e_1435928007_m.jpg'/>
										</div>
										<div style='margin-top:1%;margin-left:10px;float:left;max-width:75%;font-weight: bold;'>
											THE MATAHIWI ESTATE WINERY <br />
											New Zealand, Wairarapa  & Hawkes Bay
										</div>
									</div>",
						"desc" => "
									<div id='wrapper' style='width:100%;margin:0 auto;'>
										<div style=' float:left;width:107px;'>
											<img src='https://glomp.it/custom/uploads/products/7993_d41d8cd98f00b204e9800998ecf8427e_1435928007_m.jpg'/>
										</div>
										<div style='margin-top:10px;margin-left:10px;float:left;width:520px;font-weight: bold;'>
											THE MATAHIWI ESTATE WINERY <br />
											New Zealand, Wairarapa  & Hawkes Bay<br/><br/><br><br>
										</div>
									</div>
                <div>
                    <p>
                        On returning to New Zealand in 1998, Alistair Scott’s vision was to establish a wine company capable of producing world-class wines and to this day that vision has not changed. As a Pinot Noir enthusiast, Alastair has focused his efforts on this notoriously complicated but delicious variety. He planted Matahiwi’s 80-hectare single vineyard with 60% Pinot Noir and the balance with Sauvignon Blanc and Pinot Gris. Alastair and Jane share a vision and a passion to make wines to reflect a purity of fruit and a sense of place by staying true to the characteristics of the local area.
                    </p>
                    <p>
                    We adopted the mythical sacred firebird, the Phoenix, which has long been seen as a symbol of renewal as our logo. The Matahiwi Phoenix symbolises the rebirth of winemaking in the area for the first time since prohibition.
                    </p>
                    Matahiwi Winery produces three ranges of wine under Holly, Matahiwi Estate and Mt Hector brands. Matahiwi is sheltered by the Tararuas mountain range and enjoys a long and dry growing season with balmy warm days and pleasantly cool nights. The climate and the exposed stony sub-soils make the area ideal for growing grapes.
                    <p>
                    At Matahiwi Estate, we understand it takes more than just good soil and an agreeable climate to produce great wine. It takes team work, passion and a shared vision. The united collaboration between Alastair and winemaker Jane Cooper has built a successful business creating wines that have the Matahiwi hallmarks of balanced flavours, mouth-filling texture and a lingering finish.
                    </p>
                    The winery continually tests different viticulture and winemaking methods, including organic regimes on certain Pinot Noir blocks. To a large extent homegrown mulches and products made from seaweeds and humic acids replace hard fertilisers. Sheep wander among the vines in winter.
                    <p>
                    Grapes are predominantly grown in the Wairarapa with small volumes of Chardonnay sourced from selected vineyard sites in Hawke’s Bay. The Wairarapa vineyards lie on the north-west outskirts of Masterton, with Pinot Noir and Sauvignon Blanc the two main varieties plus a small amount of Pinot Gris.
                    </p>
                </div>",
                "desc_m" => "
                    <div>
                        <p>
                            On returning to New Zealand in 1998, Alistair Scott’s vision was to establish a wine company capable of producing world-class wines and to this day that vision has not changed. As a Pinot Noir enthusiast, Alastair has focused his efforts on this notoriously complicated but delicious variety. He planted Matahiwi’s 80-hectare single vineyard with 60% Pinot Noir and the balance with Sauvignon Blanc and Pinot Gris. Alastair and Jane share a vision and a passion to make wines to reflect a purity of fruit and a sense of place by staying true to the characteristics of the local area.
                        </p>
                        <p>
                        We adopted the mythical sacred firebird, the Phoenix, which has long been seen as a symbol of renewal as our logo. The Matahiwi Phoenix symbolises the rebirth of winemaking in the area for the first time since prohibition.
                        </p>
                        Matahiwi Winery produces three ranges of wine under Holly, Matahiwi Estate and Mt Hector brands. Matahiwi is sheltered by the Tararuas mountain range and enjoys a long and dry growing season with balmy warm days and pleasantly cool nights. The climate and the exposed stony sub-soils make the area ideal for growing grapes.
                        <p>
                        At Matahiwi Estate, we understand it takes more than just good soil and an agreeable climate to produce great wine. It takes team work, passion and a shared vision. The united collaboration between Alastair and winemaker Jane Cooper has built a successful business creating wines that have the Matahiwi hallmarks of balanced flavours, mouth-filling texture and a lingering finish.
                        </p>
                        The winery continually tests different viticulture and winemaking methods, including organic regimes on certain Pinot Noir blocks. To a large extent homegrown mulches and products made from seaweeds and humic acids replace hard fertilisers. Sheep wander among the vines in winter.
                        <p>
                        Grapes are predominantly grown in the Wairarapa with small volumes of Chardonnay sourced from selected vineyard sites in Hawke’s Bay. The Wairarapa vineyards lie on the north-west outskirts of Masterton, with Pinot Noir and Sauvignon Blanc the two main varieties plus a small amount of Pinot Gris.
                        </p>
                    </div>"
                ),
            "4" => array(
						"name" => "<div id='wrapper' style='position:relative;max-width:100%;margin:0 auto;'>
										<div style='float:left;max-width:20%;'>
											<img style='float:left;width:100%;' src='https://glomp.it/custom/uploads/products/732_d41d8cd98f00b204e9800998ecf8427e_1435926828_l.jpg'/>
										</div>
										<div style='margin-top:1%;margin-left:10px;float:left;max-width:75%;font-weight: bold;'>
											THE LAKE ROAD WINERY <br />
											New Zealand, Gisborne & Marlborough
										</div>
									</div>",
						"desc" => "
									<div id='wrapper' style='width:100%;margin:0 auto;'>
										<div style=' float:left;width:107px;'>
											<img src='https://glomp.it/custom/uploads/products/732_d41d8cd98f00b204e9800998ecf8427e_1435926828_l.jpg'/>
										</div>
										<div style='margin-top:10px;margin-left:10px;float:left;width:520px;font-weight: bold;'>
											THE LAKE ROAD WINERY <br/>
											New Zealand, Gisborne & Marlborough <br/><br/><br><br>
										</div>
									</div>
                <div>
                    <p>
                        Lake Road wines began as a partnership in the early 70's with the plan of investing in premium vineyards supplying the pioneering wine companies such as Penfolds, Corbans, Giesens and Villa Maria.
                    </p>
                        As a family business, quality was always valued over quantity and through careful viticulture the vines produced many gold medals, cementing a reputation for premium wines.
                    <p>
                        In the mid 2000's after more than 30 years of viticulture, winemaking was begun with the first wines under our Lake Road label released in 2008 and exported to Australia.
                    </p>
                    <p>
                        Our mission is to create contemporary wines with an uncompromising view to quality, presented to our customers at price points that represent great value across our complete range, exceeding expectations and guaranteeing satisfaction with minimal environmental impact.
                    </p>
                    <p>
                        Exports of our wine have since spread to China, UK and the EU. The range of wines from the four main regions offered by Lake Road has broadened to today when all major varieties are exported. Marlborough Sauvignon Blanc is the cornerstone of our range and this is complemented by other white wines such as Chardonnay, Pinot Gris and Gewurztraminer. Gisborne Merlot is our main variety of red wine we offer and showcase the best of this particular region.
                    </p>
                </div>", 
                "desc_m" => "
                    <div>
                        <p>
                            Lake Road wines began as a partnership in the early 70's with the plan of investing in premium vineyards supplying the pioneering wine companies such as Penfolds, Corbans, Giesens and Villa Maria.
                        </p>
                            As a family business, quality was always valued over quantity and through careful viticulture the vines produced many gold medals, cementing a reputation for premium wines.
                        <p>
                            In the mid 2000's after more than 30 years of viticulture, winemaking was begun with the first wines under our Lake Road label released in 2008 and exported to Australia.
                        </p>
                        <p>
                            Our mission is to create contemporary wines with an uncompromising view to quality, presented to our customers at price points that represent great value across our complete range, exceeding expectations and guaranteeing satisfaction with minimal environmental impact.
                        </p>
                        <p>
                            Exports of our wine have since spread to China, UK and the EU. The range of wines from the four main regions offered by Lake Road has broadened to today when all major varieties are exported. Marlborough Sauvignon Blanc is the cornerstone of our range and this is complemented by other white wines such as Chardonnay, Pinot Gris and Gewurztraminer. Gisborne Merlot is our main variety of red wine we offer and showcase the best of this particular region.
                        </p>
                    </div>
                "),
            "5" => array(
						"name" => "<div id='wrapper' style='position:relative;max-width:100%;margin:0 auto;'>
										<div style='float:left;max-width:20%;'>
											<img style='float:left;width:100%;' src='https://glomp.it/custom/uploads/products/6955_d41d8cd98f00b204e9800998ecf8427e_1435927238_w.jpg'/>
										</div>
										<div style='margin-top:1%;margin-left:10px;float:left;max-width:75%;font-weight: bold;'>
											THE WHITEHAVEN WINERY<br />
											New Zealand, Marlborough
										</div>
									</div>",
						"desc" => "
									<div id='wrapper' style='width:100%;margin:0 auto;'>
										<div style=' float:left;width:107px;'>
											<img src='https://glomp.it/custom/uploads/products/6955_d41d8cd98f00b204e9800998ecf8427e_1435927238_w.jpg'/>
										</div>
										<div style='margin-top:10px;margin-left:10px;float:left;width:520px;font-weight: bold;'>
											THE WHITEHAVEN WINERY<br />
											New Zealand, Marlborough<br /><br /><br><br>
										</div>
									</div>
                <div>
                    <p>
                        Every bottle of Whitehaven wine proudly carries the anchor insignia, the distinctive quality marque of one of New Zealand's most respected family wine companies. Established in 1994 in the heart of the internationally renowned Marlborough region, Whitehaven is the realisation of the White family's vision to create a respected and critically acclaimed winery.  Sailing the Pacific, they set a new course for the Marlborough Sounds, dropped anchor and launched a wine label - Whitehaven.
                    <p>
                        Today the company is headed by Sue White, an enthusiastic advocate of the region, who continues to make the dreams she shared with her late husband Greg a reality. Sue leads a small, talented, committed team who together are carving global recognition for the Whitehaven label, showcasing elegant, single varietal Marlborough wines to over a dozen countries.
                    </p>
                        The Whitehaven philosophy centres on the pursuit of 'quality without compromise' a principle underpinning the entire winemaking cycle – from vineyard management to every bottle of pure Marlborough wine bearing the Whitehaven label. In a relatively short history Whitehaven has evolved to become one of Marlborough's leading ambassadors, our award-winning wines bearing the distinctive anchor insignia are enjoyed around the world. It's a marque of quality, consistency and integrity.
                    <p>
                        Marlborough is the heartland of New Zealand winemaking and is widely recognised as one of the world’s iconic grape growing regions.  With long sunshine hours and a relatively cool climate, Marlborough grapes enjoy the advantage of a long, slow ripening period, which intensifies flavours and gives Marlborough wines their unique character.
                    </p>
                    <p>
                        Whitehaven's winemaking philosophy is centred on the pursuit of quality without compromise.  Chief winemaker Sam Smail leads a talented winemaking team totally committed to this philosophy.  Using New World techniques and Old World craftsmanship, Sam and his team are focused on a portfolio of varietal wines that deliver an enviable mix of quality, intensity and consistency, all with an elegant edge.
                    </p>
                </div>
            ",
            "desc_m" => "
                <div>
                    <p>
                        Every bottle of Whitehaven wine proudly carries the anchor insignia, the distinctive quality marque of one of New Zealand's most respected family wine companies. Established in 1994 in the heart of the internationally renowned Marlborough region, Whitehaven is the realisation of the White family's vision to create a respected and critically acclaimed winery.  Sailing the Pacific, they set a new course for the Marlborough Sounds, dropped anchor and launched a wine label - Whitehaven.
                    <p>
                        Today the company is headed by Sue White, an enthusiastic advocate of the region, who continues to make the dreams she shared with her late husband Greg a reality. Sue leads a small, talented, committed team who together are carving global recognition for the Whitehaven label, showcasing elegant, single varietal Marlborough wines to over a dozen countries.
                    </p>
                        The Whitehaven philosophy centres on the pursuit of 'quality without compromise' a principle underpinning the entire winemaking cycle – from vineyard management to every bottle of pure Marlborough wine bearing the Whitehaven label. In a relatively short history Whitehaven has evolved to become one of Marlborough's leading ambassadors, our award-winning wines bearing the distinctive anchor insignia are enjoyed around the world. It's a marque of quality, consistency and integrity.
                    <p>
                        Marlborough is the heartland of New Zealand winemaking and is widely recognised as one of the world’s iconic grape growing regions.  With long sunshine hours and a relatively cool climate, Marlborough grapes enjoy the advantage of a long, slow ripening period, which intensifies flavours and gives Marlborough wines their unique character.
                    </p>
                    <p>
                        Whitehaven's winemaking philosophy is centred on the pursuit of quality without compromise.  Chief winemaker Sam Smail leads a talented winemaking team totally committed to this philosophy.  Using New World techniques and Old World craftsmanship, Sam and his team are focused on a portfolio of varietal wines that deliver an enviable mix of quality, intensity and consistency, all with an elegant edge.
                    </p>
                </div>
            "),
        );
    }
}
/*
 * push_notification
 * Push notification on smart phones
 * 
 * @access public
 * @param array
 * @return none
 */
function push_notification($params) {
    $CI = & get_instance();
    $CI->load->model('push_notifications_m');
    $CI->load->model('users_m');
    
    $friend = $CI->users_m->get_record(array(
        'select' => 'user_id, user_device_token',
        'where' => array('user_email' => $params['user_email']),
    ));
    
    if (empty($friend['user_device_token'])) {
        return;
    }
    
    //Get count sent and new notifs
    $notifs = $CI->db->get_where('gl_push_notification', 'status <> "seen" AND user_id = "'.$friend['user_id'].'"');
    $notifs = $notifs->num_rows();
                    
    $CI->load->library('ios_push', array(
        'device_token' => trim($friend['user_device_token']),
        'body' => array(
            'alert' => $params['message'],
            'sound' => 'default',
            'badge' => $notifs + 1,
            'params' => $params['params']
        )
    ));
    
    $CI->load->library('android_push', array(
        'device_token' => trim($friend['user_device_token']),
        'body' => array(
            'message' => $params['message'],
            'vibrate' => 1,
            'sound' => 1,
            'params' => $params['params']
        )
    ));

    $push_id = $CI->push_notifications_m->_insert(array(
        'user_id' => $friend['user_id'],
        'type' => $params['type'],
        'status' => 'new',
        'created' => date('Y-m-d H:i:s'),
    ));

    $res2 = $CI->android_push->send();
    $res = $CI->ios_push->send();
    
    //TODO: MAKE IN_ARRAY CHECK?
    if ($res || $res2) {
        $CI->push_notifications_m->_update(array('id' => $push_id),
                array(
                    'status' => 'sent',
                    'modified' => date('Y-m-d H:i:s')
        ));
    }
}
    function fb_curl($url, $follow = TRUE, $cookie = FALSE, $post_string = '', $post_count = 7, $scope_id = '')  
    {  
        /* usage
         * 
         * 
            $this->load->library('simple_html_dom');
        
            $url = 'https://login.facebook.com/login.php?login_attempt=1';  

            $email = 'testfour.glomp@gmail.com';  
            $pass = 'GlompTest123_fb';
            $post = 'charset_test='.htmlspecialchars("&euro;,&acute;,â‚¬,Â´,æ°´,Ð”,Ð„").'&lsd=OsC-Z&locale=en_US&email='.$email.'&pass='.$pass.'&persistent=1&default_persistent=0';

            $data = fb_curl($url, TRUE, TRUE, $post, 7, '1550275249');
            $fbid = str_get_html($data)->find('meta[property=al:android:url]', 0);
            $username = str_get_html($data)->find('meta[http-equiv=refresh]', 0);

            $data = array(
                'fbid' => str_replace("fb://profile/", "", $fbid->content),
                'username' => trim(preg_replace(array("/0; URL=\//", "/\?_fb_noscript=1/"), "", $username->content))
            );

            echo json_encode($data);
         * 
         */
        
        $cookie_file_path = FCPATH.'cookies.txt';
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_HEADER, 1);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $follow);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($ch, CURLOPT_FILETIME, 1);  
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  

        if($post_string != '')  
        {  
            curl_setopt($ch, CURLOPT_POST, $post_count);  
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);  
        }  
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; it; rv:2.0b4) Gecko/20100818');  
        curl_setopt($ch, CURLOPT_REFERER, $url);  
        if($cookie)  
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);  

        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);  

        $data = curl_exec($ch);  
        curl_close($ch);
        
        if ( $cookie) {
            return fb_curl('https://www.facebook.com/'. $scope_id, TRUE, FALSE, '', 7, '');
        }

        return $data;  
    }
?>