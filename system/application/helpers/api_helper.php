<?php

/*
  Name: API-helpper
  Author: Allan Bernabe
 */

function check_token($user_token, $x) {
    if (empty($user_token)) {
        return $x->response(array('success' => FALSE), 200);
    }
}

function upload_profile_pic($x, $params) {
    //update profile pic
    /* upload photo start */
    if (isset($_FILES['user_photo']['tmp_name']) && $_FILES['user_photo']['tmp_name'] != "") {
        $config = array(
            'allowed_types' => 'jpeg|jpg|gif|png',
            'upload_path' => 'custom/uploads/users/',
            'maximum_filesize' => 2
        );

        $user_photo = $x->custom_func->custom_upload($config, $_FILES['user_photo']);
        $uploadError = preg_match("/^Error/", $user_photo) ? $user_photo : NULL;
        if (empty($uploadError)) {
            try {
                list($width, $height) = getimagesize('custom/uploads/users/' . $user_photo);
                $newName = mt_rand(1, 10000) . "_" . $user_photo;


                if ($height < 140 || $width < 140) {
                    $uploadError = "Invalid image file size.";
                }

                if ($width > 1024) {
                    $x->resizeImg('custom/uploads/users/' . $user_photo, 'custom/uploads/users/' . $newName, 1024, 720, 3);
                }
                if ($height > 720)
                    $x->resizeImg('custom/uploads/users/' . $user_photo, 'custom/uploads/users/' . $newName, 1024, 720, 3);

                if (file_exists("custom/uploads/users/" . $newName)) {
                    $user_photo = $newName;
                } else {
                    $user_photo = $user_photo;
                }
            } catch (Exception $e) {
                if (($x->config->item('log_threshold')) > 0) {
                    $x->log_message('error', $e->getMessage());
                }
            }


            if (!isset($uploadError) || (isset($uploadError) && $uploadError == "")) {
                $x->resizeImg("custom/uploads/users/" . $user_photo, "custom/uploads/users/thumb/" . $user_photo, USER_PHOTO_THUMB_WIDTH, USER_PHOTO_THUMB_HEIGHT);
                if ($params['register'] != TRUE) {
                    $x->login_m->updatePhoto($user_photo, $params['user_id']);

                    $user_photo_orig = $user_photo;
                    $x->login_m->updatePhotoOrig($user_photo_orig, $params['user_id']);
                } else {
                    return $user_photo;
                }
            } else {
                return array(
                    'success' => FALSE,
                    'message' => $uploadError,
                    'error_code' => '005');
            }
        } else {
            return array(
                'success' => FALSE,
                'message' => $uploadError,
                'error_code' => '005');
        }
    }//user photo temp check
    /*     * ****upload photo end** */
}

?>