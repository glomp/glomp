<?php

/*
 * This class is for managing native app notifications
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @version     1.0
 */

class Ios_push {

    protected $CI;
    protected $vars = array(
        'device_token' => '',
        'local_cert' => 'prod_ck.pem',
        'passphrase' => '', //Gl0mp!tpassword (old password)
        'server' => 'ssl://gateway.push.apple.com:2195',
        'body' => array(
                'alert' => '',
                'sound' => 'default',
        ),
    );

    function __construct($params) {
        $this->CI = & get_instance();
        
        $this->vars['local_cert'] = (isset($params['local_cert'])) ? $params['local_cert'] : $this->vars['local_cert'];
        $this->vars['passphrase'] = (isset($params['passphrase'])) ? $params['passphrase'] : $this->vars['passphrase'];
        $this->vars['server'] = (isset($params['server'])) ? $params['server'] : $this->vars['server'];
        $this->vars['body'] = (isset($params['body'])) ? $params['body'] : $this->vars['body'];
        $this->vars['device_token'] = $params['device_token'];
    }
    
    function send() {
        if (! ctype_xdigit($this->vars['device_token'])) {
            return FALSE;
        }
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->vars['local_cert']);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $this->vars['passphrase']);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                $this->vars['server'], $err,
                $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp) {
            //exit("Failed to connect: $err $errstr" . PHP_EOL);
            return TRUE;
        }
                

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = $this->vars['body'];
        
        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $this->vars['device_token']) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
                return FALSE;
        else
                return TRUE;
        // Close the connection to the server
        fclose($fp);
    }

}