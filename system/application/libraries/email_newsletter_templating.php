<?php
/*
 * This class is for managing campaign email templates
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @model depenencies newsletter_m, email_logs_m, campaign_newsletter_m
 * @version     1.0
 */

class Email_newsletter_templating 
{
    protected $CI;
    protected $vars = array(
        'subject' => 'subject',
        'lang_id' => '1',
        'from_email' => SITE_DEFAULT_SENDER_EMAIL,
        'from_name' => SITE_DEFAULT_SENDER_NAME,
    );
    protected $record;
            
    function __construct(){
        $this->CI = & get_instance();
        $this->CI->load->model('newsletter_m');
        $this->CI->load->model('campaign_newsletter_m');
        $this->CI->load->model('email_logs_m');
    }
    /**
     * config
     * Sets config for the the output that will be sending on the email.
     * 
     * @params string
     * @params array
    */
    function config($vars = array())
    {
        //Set the defaults
        $this->vars = array(
            'subject' => 'subject',
            'lang_id' => '1',
            'from_email' => SITE_DEFAULT_SENDER_EMAIL,
            'from_name' => SITE_DEFAULT_SENDER_NAME,
        );
        
        $this->vars = $this->set_params($this->vars, $vars);
        $vars = $this->vars;
        
        $rec = $this->CI->newsletter_m->get_record(array(
            'where' => array(
                'newsletter_id' => $vars['newsletter_id'],
            )
        ));
        
        //$this->vars['subject'] = $rec['subject'];
        $this->record = $rec;
    }
    /**
     * set_subject
     * 
     * Method sets subject overiding the default subject or the subject from pulled from the database.
     *  - This must be set after the config function and before the send function
     * 
     * @params string
     * @return none
     */    
    function set_subject($subject) {
        
       if ( empty($subject) ) {
           $subject = $this->vars['subject'];
       }
       
       $this->vars['subject'] = $subject;
    }
    /**
     * get_message
     * Method gets the generated message replacing the placeholder(s).
     *  
     * @return string
     */
    function get_message() {
        $vars = $this->vars;
        
        if ( empty($vars['params'])) {
            return $this->record['body'];
        }
        
        return str_replace(array_keys($vars['params']), array_values($vars['params']), $this->record['body']);
    }
    /**
     * Method send generated message into an email.
     *  
     * @return bool
     */
    function send ($print_debugger =FALSE) {     
        $url = 'http://sendgrid.com/';
        $user = 'glompzac';
        $pass = '6lomp!DOT1+_send';
        
        //Insert into log then get ID for the view on browser
        $edata = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $data['view_id'] = $this->CI->email_logs_m->_insert($edata);
        
        $camp_newsletter = $this->CI->campaign_newsletter_m->get_record(array(
            'where' => array('campaign_newsletter_id' => $this->vars['campaign_newsletter_id'])
        ));
        $template_data['campaign_newsletter_id']=$this->vars['campaign_newsletter_id'];
        $template_data['attributes'] = json_decode($camp_newsletter['newsletter_template_value'], TRUE);
        
        //layout
        $body = $this->CI->load->view(ADMIN_FOLDER . '/newsletter/_layouts/' . $this->record['name'], $template_data, TRUE, FALSE, FALSE);
        
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $this->vars['to'],
            'subject'   => $this->vars['subject'],
            'html'      => $body,
            'text'      => $body,
            'from'      => $this->vars['from_email'],
            'fromname'  => $this->vars['from_name'],
        );
        //'subject'   => $this->vars['subject'],

        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);

        //print everything out
        $response = json_decode($response);
        //$print_debugger = TRUE;
        if ($print_debugger) {
            echo '<pre>';
            print_r($response);
            exit;
        }
        
        $where = array(
            'email_log_id' => $data['view_id']
        );
        
        if (isset($response->message)) {
            $data = array(
                'subject' =>  $this->vars['subject'],
                'from_name' =>  $this->vars['from_name'],
                'from_email' => $this->vars['from_email'],
                'to_email' => $this->vars['to'],
                'to_name' => '',
                'email_body' => $body,
                'response' => $response->message,
                'response_message' => (isset($response->errors)) ? json_encode($response->errors) : '',
                'date_created' => date('Y-m-d H:i:s'),
            );
            
            $insert_id = $this->CI->email_logs_m->_update($where, $data);
            
            if ($response->message != 'success') {
                return FALSE;
            }
            else {
                return $where['email_log_id'];
            }
        }
        
        $data = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'email_body' => $body,
            'response' => 'Unexpected error',
            'response_message' => 'Failed: No connection established.',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $update_id = $this->CI->email_logs_m->_update($where,$data);
        
        return FALSE;
    }
    /**
     * set_params
     * 
     * Set parameters values
     * 
     * @access public
     * @param array
     * @param array
     * @return array
     */
    function set_params($params, $args)
    {   
            foreach ($args as $i => $value)
            {
                    if (isset($args[$i]))
                    {
                            $params[$i] = $value;
                    }
            }

            return $params;
    }
}