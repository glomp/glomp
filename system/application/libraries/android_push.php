<?php

/*
 * This class is for managing native android app notifications
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @version     1.0
 */

class Android_push {

    protected $CI;
    protected $vars = array(
        'device_token' => '',
        'api_key' => 'AIzaSyDUod5mVUdUAUAvzapdBLBGr8biJ4ita68',
        'server' => 'https://android.googleapis.com/gcm/send',
        'body' => array(
            'vibrate' => 1,
            'sound' => 1,
        ),
    );

    function __construct($params) {
        $this->CI = & get_instance();

        $this->vars['api_key'] = (isset($params['api_key'])) ? $params['api_key'] : $this->vars['api_key'];
        $this->vars['server'] = (isset($params['server'])) ? $params['server'] : $this->vars['server'];
        $this->vars['body'] = (isset($params['body'])) ? $params['body'] : $this->vars['body'];
        $this->vars['device_token'] = $params['device_token'];
    }

    function send() {

        $fields = array
            (
            'registration_ids' => array($this->vars['device_token']),
            'data' => $this->vars['body']
        );

        $headers = array
            (
            'Authorization: key=' . $this->vars['api_key'],
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result);
        
        if (! isset($data->success)) {
            return FALSE;
        }
        
        if ($data->success == 1) {
            return TRUE;
        }
        
        return FALSE;
    }

}