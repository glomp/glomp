<?php
/*
Author : Shree 
Date : July-25-2013
This library will be included into the all pages in header. It will check the user is logged in or not if not logged in it will redirect to login page
*/
class Admin_login_check 
{
	function Admin_login_check()
		{
			
			$CI = & get_instance();
			if($CI->session->userdata('admin_id') && $CI->session->userdata('is_logged_in')==true)
			{
					$data=array(
					'admin_id'=>$CI->session->userdata('admin_id'),
					'admin_status' =>'Active' 
					);
					
					$result=$CI->db->get_where('gl_admin',$data);
					if($result->num_rows()==1)
					{
						
						
						$inactive = 4800; //1200
						$session_life = time()-$CI->session->userdata('since_logged_in');
						if($session_life > $inactive)
						{
							$msg="Session has been expired";
							redirect(base_url(ADMIN_FOLDER.'/login/logout/'.$msg));
							exit();
						}
						else
						{
							$CI->session->set_userdata('since_logged_in',time());
						}
					}
					else
					{ 
					  $CI->session->set_flashdata("err", "Session has been expired.");
					  $msg="Session has been expired:1";
					  redirect(base_url(ADMIN_FOLDER.'/login/logout/'.$msg));
					  exit();
					}
				
			}//if close of checking session set
			else
			{
				
				redirect(base_url(ADMIN_FOLDER.'/login/logout/1'));
				exit();
			}


	}//function close

}///EOC
?>