<?php
/*
Author : Shree 
Date : July-25-2013
This library will be included into the all pages in header. It wil check the user is logged in or not if not logged in it will redirect to login page
*/
class Merchant_admin_login_check 
{
	function Merchant_admin_login_check()
		{
			
			$CI = & get_instance();
			if($CI->session->userdata('merchant_id') && $CI->session->userdata('is_logged_in')==true)
			{
					$data=array(
					'merchant_id'=>$CI->session->userdata('merchant_id'),
					);
					
					$result=$CI->db->get_where('gl_merchant',$data);
					if($result->num_rows()==1)
					{
						
						
						$inactive = 4800; //1200
						$session_life = time()-$CI->session->userdata('since_logged_in');
						if($session_life > $inactive)
						{
							$msg="Session has been expired";
							redirect(base_url(MERCHANT_FOLDER.'/login/logout/'.$msg));
							exit();
						}
						else
						{
							$CI->session->set_userdata('since_logged_in',time());
						}
					}
					else
					{ 
					  $CI->session->set_flashdata("err", "Session has been expired.");
					  $msg="Session has been expired:1";
					  redirect(base_url(MERCHANT_FOLDER.'/login/logout/'.$msg));
					  exit();
					}
				
			}//if close of checking session set
			else
			{
				
				redirect(base_url(MERCHANT_FOLDER.'/login/logout/1'));
				exit();
			}


	}//function clsoe

}///EOC
?>