<?php

/*
 * This class is for managing linkedIN REST API
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @version     1.0
 */

class LinkedIn_API {

    protected $CI;
    protected $vars = array(
        'API_KEY' => '75ocjfra1o2yjt',
        'API_SECRET' => 'eaBn4H91k2aSfSyq',
        'REDIRECT_URI' => '',
        'SCOPE' => '',
    );

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('session');
        $this->CI->load->model('system_log_m');
        $this->CI->load->library('user_agent');
        $this->vars['REDIRECT_URI'] = site_url('ajax_post/linkedIn_callback');
    }

    function getAuthorizationCode() {
        $params = array('response_type' => 'code',
            'client_id' => $this->vars['API_KEY'],
            'scope' => $this->vars['SCOPE'],
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => $this->vars['REDIRECT_URI'],
        );

        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);

        // Needed to identify request when it returns to us
        //$_SESSION['state_li'] = $params['state'];
        $this->CI->session->set_userdata('state_li', $params['state']);

        // Redirect user to authenticate
        return $url;
        //header("Location: $url");
        //exit;
    }

    function getAccessToken() {
        $params = array('grant_type' => 'authorization_code',
            'client_id' => $this->vars['API_KEY'],
            'client_secret' => $this->vars['API_SECRET'],
            'code' => $_GET['code'],
            'redirect_uri' => $this->vars['REDIRECT_URI'],
        );
        //print_r($params);
        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);

        // Tell streams to make a POST request
        $context = stream_context_create(
                array('http' =>
                    array('method' => 'POST',
                    )
                )
        );

        // Retrieve access token information
        $response = @file_get_contents($url, false, $context);

        // Native PHP object, please
        $token = json_decode($response);

        // Store access token and expiration time
        //$_SESSION['access_token'] = $token->access_token; // guard this! 
        //$_SESSION['expires_in'] = $token->expires_in; // relative time (in seconds)
        //$_SESSION['expires_at'] = time() + $_SESSION['expires_in']; // absolute time

        $this->CI->session->set_userdata('access_token_li', $token->access_token);
        $this->CI->session->set_userdata('expires_in_li', $token->expires_in);
        $this->CI->session->set_userdata('expires_at_li', time() + $token->expires_in);

        return true;
    }

    function fetch($method, $resource, $body = '') {
        $params = array('oauth2_access_token' => $this->CI->session->userdata('access_token_li'), //$_SESSION['access_token'],
            'format' => 'json',
        );

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request
        $context = stream_context_create(
                array('http' =>
                    array('method' => $method)
                )
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = json_encode(curl_exec($ch));
        // convert response
        $response = json_decode($output);

        // handle error; error output
        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            $data = array(
                'log_type' => 'linkedin_api',
                'log_event' => 'Processing',
                'log_message' => 'Backend error handler',
                'browser_info' => $this->CI->agent->agent_string(),
                'log_url' => site_url(),
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'log_details' => $response,
                'log_timestamp' => date('Y-m-d H:i:s')     
            );

            $this->CI->system_log_m->_insert($data);
            curl_close($ch);
            $this->clear_session();
            return json_decode(json_encode(array('error'))); // for testing
        }
        
        // Hocus Pocus
        //$response = @file_get_contents($url, false, $context);
        
        if (empty($response)) {
          return json_decode(json_encode(array('error'))); // for testing  
        }

        // Native PHP object, please
        return json_decode($response);
    }

    function send($method, $resource, $body = '', $type = 'json') {
        $params = array('oauth2_access_token' => $this->CI->session->userdata('access_token_li'), //$_SESSION['access_token'],
            'format' => 'json',
        );

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request

        $context = stream_context_create(
                array('http' =>
                    array(
                        'method' => $method,
                        'header' => "Content-Type:application/json\r\n" . "Content-Length: " . strlen($body) . "\r\n",
                        'content' => $body
                    )
                )
        );

        // Hocus Pocus
        $response = @file_get_contents($url, false, $context);
        
        if (empty($response)) {
          return json_decode(json_encode(array('error'))); // for testing  
        }

        // Native PHP object, please
        return json_decode($response);
    }

    function clear_session() {
        $this->CI->session->unset_userdata('access_token_li');
        $this->CI->session->unset_userdata('expires_in_li');
        $this->CI->session->unset_userdata('expires_at_li');
        $this->CI->session->unset_userdata('state_li');
    }

}