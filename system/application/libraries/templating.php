<?php

/*
 * This class is for managing campaign email templates
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @model       section_m, widget_m
 * @version     1.0
 */

class Templating {

    protected $CI;
    protected $vars = array(
        'actions' => array(
            'section_type_save' => '',
            'section_delete_save' => '',
            'section_sort_save' => '',
            'widget_add_save' => '',
            'widget_load_options' => '',
            'widget_delete_save' => '',
        ),
        'theme' => 'assets/backend/css/cupertino/jquery-ui-1.10.4.custom.min.css'
    );
    protected $record;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('section_m');
        $this->CI->load->model('widget_m');
    }

    /**
     * config
     * Sets config.
     * 
     * @params string
     * @params array
     */
    function config($vars = array()) {
        //Set the defaults
        $this->vars = array(
            'actions' => array(
                'section_type_save' => '',
                'section_delete_save' => '',
                'section_sort_save' => '',
                'widget_add_save' => '',
                'widget_load_options' => '',
                'widget_delete_save' => '',
            ),
            'theme' => 'assets/backend/css/cupertino/jquery-ui-1.10.4.custom.min.css'
        );

        $this->vars = $this->set_params($this->vars, $vars);
        $vars = $this->vars;
    }

    /**
     * load_template
     * 
     * Loads the templating view
     *  - This must be set after the config function and before the send function
     * 
     * @params none
     * @return none
     */
    function load_template($template_id = 0) {
        $components = '';
        $section_type_1col = $this->CI->load->view('template/section_types/_section_type_1col_v', array(
            'section_id' => '',
            'display_add' => '',
            'widget' => ''
                ), TRUE, FALSE, FALSE);

        $section_type_2col = $this->CI->load->view('template/section_types/_section_type_2col_v', array(
            'section_id' => '',
            'display_add_1' => '',
            'display_add_2' => '',
            'widget_1' => '',
            'widget_2' => ''
                ), TRUE, FALSE, FALSE);

        $section_type_3col = $this->CI->load->view('template/section_types/_section_type_3col_v', array(
            'section_id' => '',
            'display_add_1' => '',
            'display_add_2' => '',
            'display_add_3' => '',
            'widget_1' => '',
            'widget_2' => '',
            'widget_3' => ''
                ), TRUE, FALSE, FALSE);

        $section_type_2_1col = $this->CI->load->view('template/section_types/_section_type_2_1col_v', array(
            'section_id' => '',
            'display_add_1' => '',
            'display_add_2' => '',
            'widget_1' => '',
            'widget_2' => ''
                ), TRUE, FALSE, FALSE);
        
        $widget_type_image = $this->CI->load->view('template/widget_types/_widget_type_image_v', array('widget_id' => '', 'widget_type' => ''), TRUE, FALSE, FALSE);
        $widget_type_text = $this->CI->load->view('template/widget_types/_widget_type_text_v', array('widget_id' => '', 'widget_type' => ''), TRUE, FALSE, FALSE);
        $widget_type_title = $this->CI->load->view('template/widget_types/_widget_type_title_v', array('widget_id' => '', 'widget_type' => ''), TRUE, FALSE, FALSE);
        $widget_type_photostory = $this->CI->load->view('template/widget_types/_widget_type_photostory_v', array('widget_id' => '', 'widget_type' => ''), TRUE, FALSE, FALSE);

        $data['theme'] = $this->vars['theme'];
        $data['actions'] = $this->vars['actions'];
        $data['section_types'] = array(
            array('id' => 'One', 'name' => '1 Column', 'html' => $section_type_1col),
            array('id' => 'Two', 'name' => '2 Column', 'html' => $section_type_2col),
            array('id' => 'Three', 'name' => '3 Column', 'html' => $section_type_3col),
            array('id' => 'Two_One', 'name' => '2 and 1 Column', 'html' => $section_type_2_1col),
        );

        $data['widget_types'] = array(
            array('id' => 'image', 'name' => 'Image', 'html' => $widget_type_image),
            array('id' => 'text', 'name' => 'Text', 'html' => $widget_type_text),
            array('id' => 'title', 'name' => 'Title', 'html' => $widget_type_title),
            array('id' => 'photostory', 'name' => 'Photo Story', 'html' => $widget_type_photostory)
        );

        /* TODO: I don't know if this is suppose to be in here */
        $sections = $this->CI->section_m->get_list(array(
            'where' => array('template_id' => $template_id),
            'order_by' => 'sort_order ASC',
            'resource_id' => TRUE
        ));

        if ($sections->num_rows() > 0) {
            $components = $this->_load_components($sections->result());
        }

        $data['components'] = $components;
        $data['template_id'] = $template_id;

        return $this->CI->load->view('template/_template_v', $data, TRUE, FALSE, FALSE);
    }

    function _load_components($sections) {
        $section = '';
        foreach ($sections as $row) {
            switch ($row->type) {
                case 'One':
                    $data['section_id'] = 'data-section_id="' . $row->id . '"';
                    $data['display_add'] = 'display: block';
                    $data['widget'] = '';

                    $widgets = $this->CI->widget_m->get_list(array(
                        'where' => array('section_id' => $row->id),
                        'resource_id' => TRUE,
                    ));
                    if ($widgets->num_rows() > 0) {
                        $widget = $widgets->result();
                        $data['display_add'] = 'display:none';
                        $data['widget'] = $this->_load_widgets($widget[0]);
                    }

                    $section .= $this->CI->load->view('template/section_types/_section_type_1col_v', $data, TRUE, FALSE, FALSE);
                    break;
                case 'Two':
                    $data['section_id'] = 'data-section_id="' . $row->id . '"';
                    $data['display_add_1'] = 'display: block';
                    $data['display_add_2'] = 'display: block';
                    $data['widget_1'] = '';
                    $data['widget_2'] = '';

                    $widgets = $this->CI->widget_m->get_list(array(
                        'where' => array('section_id' => $row->id),
                        'order_by' => 'position ASC',
                        'resource_id' => TRUE,
                    ));

                    if ($widgets->num_rows() > 0) {
                        $widget = $widgets->result();

                        foreach ($widget as $w) {
                            for ($i = 1; $i <= 2; $i++) {
                                if ($w->position == $i) {
                                    $data['display_add_' . $i] = 'display:none';
                                    $data['widget_' . $i] = $this->_load_widgets($w);
                                    break;
                                }
                            }
                        }
                    }

                    $section .= $this->CI->load->view('template/section_types/_section_type_2col_v', $data, TRUE, FALSE, FALSE);
                    break;
                case 'Two_One':
                    $data['section_id'] = 'data-section_id="' . $row->id . '"';
                    $data['display_add_1'] = 'display: block';
                    $data['display_add_2'] = 'display: block';
                    $data['widget_1'] = '';
                    $data['widget_2'] = '';

                    $widgets = $this->CI->widget_m->get_list(array(
                        'where' => array('section_id' => $row->id),
                        'order_by' => 'position ASC',
                        'resource_id' => TRUE,
                    ));

                    if ($widgets->num_rows() > 0) {
                        $widget = $widgets->result();

                        foreach ($widget as $w) {
                            for ($i = 1; $i <= 2; $i++) {
                                if ($w->position == $i) {
                                    $data['display_add_' . $i] = 'display:none';
                                    $data['widget_' . $i] = $this->_load_widgets($w);
                                    break;
                                }
                            }
                        }
                    }

                    $section .= $this->CI->load->view('template/section_types/_section_type_2_1col_v', $data, TRUE, FALSE, FALSE);
                    break;
                case 'Three':
                    $data['section_id'] = 'data-section_id="' . $row->id . '"';
                    $data['display_add_1'] = 'display: block';
                    $data['display_add_2'] = 'display: block';
                    $data['display_add_3'] = 'display: block';
                    $data['widget_1'] = '';
                    $data['widget_2'] = '';
                    $data['widget_3'] = '';

                    $widgets = $this->CI->widget_m->get_list(array(
                        'where' => array('section_id' => $row->id),
                        'order_by' => 'position ASC',
                        'resource_id' => TRUE,
                    ));

                    if ($widgets->num_rows() > 0) {
                        $widget = $widgets->result();
                        foreach ($widget as $w) {
                            for ($i = 1; $i <= 3; $i++) {
                                if ($w->position == $i) {
                                    $data['display_add_' . $i] = 'display:none';
                                    $data['widget_' . $i] = $this->_load_widgets($w);
                                    break;
                                }
                            }
                        }
                    }

                    $section .= $this->CI->load->view('template/section_types/_section_type_3col_v', $data, TRUE, FALSE, FALSE);
                    break;
            }
        }

        return $section;
    }

    function _load_widgets($widget) {
        $w = '';
        $types = array(
            'title' => array('template' => 'template/widget_types/_widget_type_title_v', 'table_name' => 'gl_widget_title'),
            'image' => array('template' => 'template/widget_types/_widget_type_image_v', 'table_name' => 'gl_widget_image'),
            'text' => array('template' => 'template/widget_types/_widget_type_text_v', 'table_name' => 'gl_widget_text'),
            'photostory' => array('template' => 'template/widget_types/_widget_type_photostory_v', 'table_name' => 'gl_widget_photostory'),
        );

        foreach ($types as $key => $value) {
            if ($key == $widget->widget_type) {
                $sub_widget = $this->CI->db->get_where($value['table_name'], array('widget_id' => $widget->id));
                $sub_widget = $sub_widget->result();
                
                $widget->data = $sub_widget[0];
                $w = $this->CI->load->view($value['template'], array(
                    'widget_id' => 'data-widget_id ="' . $widget->id . '_' . $sub_widget[0]->id . '"',
                    'widget_type' => 'data-widget_type ="' . $widget->widget_type . '"',
                    'widget_template' => $this->CI->load->view(
                                            'includes/widget_type_'.$widget->widget_type.'.php',
                                            array(
                                                    'widget' => $widget
                                            ),
                                            TRUE,FALSE,FALSE)
                        ), TRUE, FALSE, FALSE);
                break;
            }
        }
        return $w;
    }

    /**
     * _source_loader
     * 
     * Loads other theme settings
     *  - This must be set after the config function and before the send function
     * 
     * @params array
     * @return string
     */
    function _source_loader($src = array()) {
        $sources = '';

        if (isset($src['css'])) {
            foreach ($src['css'] as $css) {
                $sources .= '<link rel="stylesheet" type="text/css" href="' . $css . '">';
            }
        }
        if (isset($src['js'])) {
            foreach ($src['js'] as $js) {
                $sources .= '<script src="' . $js . '" type="text/javascript"></script>';
            }
        }

        return $sources;
    }

    /**
     * set_params
     * 
     * Set parameters values
     * 
     * @access public
     * @param array
     * @param array
     * @return array
     */
    function set_params($params, $args) {
        foreach ($args as $i => $value) {
            if (isset($args[$i])) {
                $params[$i] = $value;
            }
        }

        return $params;
    }

}