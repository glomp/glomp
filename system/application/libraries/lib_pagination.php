<?php
class lib_pagination
{
	function __Construct()
	{
		$CI=& get_instance();
		$CI->load->library('pagination');
	}
	
	function DoPagination($base_url,$total_rows,$per_page,$uri_segment,$suffix="",$query_string="")
	{
		$CI = & get_instance();
		$config['suffix'] = $suffix ;
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
        
        $config['enable_query_strings'] = true;
        
        //added by ryan
        $config['query_string'] = "";
        if($query_string!=""){
            $config['query_string'] = "?search_q=".$query_string;
        }
        //added by ryan
		
		if($per_page>0)
		$config['per_page'] = $per_page;
		else
		$config['per_page'] = PAGINATION_PER_PAGE;
		
		$config['first_url'] = $base_url.$suffix;
		$config['uri_segment']=$uri_segment;
	    $config['first_link'] = 'First';
	    $config['last_link']  = 'Last';
	    $config['next_link']  = 'Next';
	    $config['prev_link']  = 'Prev';
		$config['cur_tag_open']  = '<span class="active">';
		$config['cur_tag_close']='</span>';
		$CI->pagination->initialize($config);
		$links=$CI->pagination->create_links();
		return $links;
	}
}
?>