<?php
/*
Author : Kishan Mainali 
Date : May 5/28/2012

Edited by ashok
*/
class Custom_func
{
	
	/* filter slug(URL) */
	function filterslug($slug)
	{
		$slug = str_replace(" ","-",$slug);
		$slug = str_replace('"','-',$slug);
		$slug = str_replace(",","",$slug);
		$slug = str_replace("'","",$slug);
		$slug = str_replace("(","-",$slug);
		$slug = str_replace(")","-",$slug);
		$slug = str_replace(".","",$slug);
		$slug = str_replace("/","-",$slug);
		$slug = str_replace("$","-",$slug);
		$slug = str_replace("&","and",$slug);
		$slug = str_replace("%","-",$slug);
		$slug = str_replace("[","-",$slug);
		$slug = str_replace("]","-",$slug);
		$slug = str_replace("=","-",$slug);
		$slug = str_replace("+","-",$slug);
		$slug = str_replace("#","-",$slug);
		$slug = str_replace("?","-",$slug);
		$slug = str_replace(":","-",$slug);
		$slug = str_replace(";","-",$slug);
		$slug = str_replace("^","-",$slug);
		$slug = str_replace("*","-",$slug);
		$slug = str_replace("@","-",$slug);
		$slug = str_replace("!","-",$slug);
		$slug = str_replace("`","-",$slug);
		$slug = str_replace("~","-",$slug);
		return strtolower($slug);
	}
	
		
	function iframeResizer($embed, $w, $h){
	//$alt = preg_match_all('/(width|height)=("[^"]*")/i', $embed, $matches);
		$embed = preg_replace('/(width)=("[^"]*")/i', 'width="'.$w.'"', $embed);
		$embed = preg_replace('/(height)=("[^"]*")/i', 'height="'.$h.'"', $embed);
	
		return $embed;
	}
#input mm dd yyyy date ,  separator
#return yyy-mm-dd

	
	/*Select datetime*/
	function datetime()
	{
		return date("Y-m-d H:i:s");
	}
	
	function dateOnly()
	{
		return date("Y-m-d");
	}
	
	function compute_remaining_days($from, $to)
	{
		$tempDate = (strtotime($to) -  strtotime($from) );		
	//	return $tempDate;
		return  $this->dayConvert($tempDate);
	}
	
	function dayConvert($data){
	
		$days    = floor(($data )/ (60*60*24));
		if($days>1)
			$days= $days." days";
		else
			$days= $days." day";
		return $days;
	}
	
	/* Get the visitor's IP */		
	function getUserIP() 
	{

                if(!empty($_SERVER['HTTP_CLIENT_IP'])) {

                $IP=$_SERVER['HTTP_CLIENT_IP']; // share internet

                } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

                $IP=$_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy

                } else {

                $IP=$_SERVER['REMOTE_ADDR'];

                }

                return $IP;

    }
	
	/* Get a user's Get referrer from _SERVER */
	function getReferer() 
	{
		return $findreferer = isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:"";
	}
	#return user used device
	function user_device()
	{
		return 'Computer';
	
	}
	
	/* upload a file */
	function custom_upload($config, $filename) 
	{
		if(is_array($config))
		{
			extract($config, EXTR_PREFIX_SAME, "wddx"); // extract the value from array exactly from its name
				//alternate method : $config = array_values($config);
			if(isset($allowed_types) && isset($upload_path))
				{
				
					if($filename['name'] != "") 
					{
						$maximum_filesize = isset($maximum_filesize)?$maximum_filesize:'5';
						$MAXIMUM_FILESIZE = $maximum_filesize * 1024 * 1024;
						/*$max_height = isset($max_height) && $max_height!=NULL?$max_height:900;
						$max_width = isset($max_width) && $max_width!=NULL?$max_width:1024;
						$min_height = isset($min_height) && $min_height!=NULL?$min_height:23;
						$min_width = isset($min_width) && $min_width!=NULL?$min_width:19;
						$size = getimagesize($filename['tmp_name']);*/
				
						//  Valid file extensions (images)
						$rEFileTypes =
						"/^\.(".$allowed_types."){1}$/i";
				
						$dir_base = "".$upload_path."";
				
						$isFile = is_uploaded_file($filename['tmp_name']);
						if ($isFile)    //  do we have a file?
						{	 //  sanatize file name
								 //     - remove extra spaces/convert to _,
								 //     - remove non 0-9a-Z._- characters,
								 //     - remove leading/trailing spaces
								 //  check if under 2MB,
								 //  check file extension for legal file types
							$safe_filename = preg_replace(
										 array("/\s+/", "/[^-\.\w]+/"),
										 array("_", ""),
										 trim($filename['name']));
							$ext = strrchr($safe_filename, '.');
							$new_photo_name = "".mt_rand(1,10000)."_".md5(session_id())."_".time()."_".$safe_filename[0];
							
							if (preg_match($rEFileTypes, $ext)==false)
							{
								return "Error ".$ext." file is not supported. Please try another file!";
							}
							else if ($filename['size'] > $MAXIMUM_FILESIZE)
							{
								return "Error The size of the image shouldn't be more than 2MB!";
							}
							/*else if($size!=NULL && $size[0] > $max_width || $size!=NULL && $size[1] > $max_height || $size!=NULL && $size[0] < $min_height || $size!=NULL && $size[1] < $min_width)
							 {
								 return "Error The photo was either too tall or too skinny!";
							 }*/
							else if ($filename['size'] <= $MAXIMUM_FILESIZE &&
							preg_match($rEFileTypes, $ext))
							{
									 $isMove = move_uploaded_file (
									 $filename['tmp_name'],
									 $dir_base.$new_photo_name.$ext);
									 return $new_photo_name.$ext;
							}
									  
						}
					}
					else 
					{
						return "Error The filename cannot be empty!";
					}
				}
				else 
				{
					return "Error Invalid array passed. Please contact the system developer!";
				}
		}
		else 
		{
			return "Error Invalid data received. Please contact the system developer!";
		}
		
	}
	
	function msg($status,$txt)
	{
		return '{"status":'.$status.',"txt":"'.$txt.'"}';
	}
	
		/* Random key generator */		
	function radomkey($min=6, $max=10)
	{
		/* Generating Password */
			$pwd=""; // to store generated password

			for($i=0;$i<rand($min,$max);$i++)
			{
			$num=rand(48,122);

			if(($num > 97 && $num < 122))
			{
			$pwd.=chr($num);
			}

			else if(($num > 65 && $num < 90))
			{
			$pwd.=chr($num);
			}

			else if(($num >48 && $num < 57))
			{
			$pwd.=chr($num);
			}

			else if($num==95)
			{
			$pwd.=chr($num);
			}
            else
			{
			$i--;
			}
			}
			return $pwd;
	}
	

function time_stamp($time_stamp) 
   { 
   $total_seconds = strtotime($time_stamp);// gives total seconsa
   $past_seconds = time()-$total_seconds;//gives time differance in seconds
   $time= date("g:i a",$total_seconds);// gives time like 03:45 pm
   $day_time= date("l",$total_seconds);// gives days like sunday, monday 
   $full_date= date("Y M d ",$total_seconds);// gives full date
   $seconds = $past_seconds;/// past second(s) 
   $minutes = round($past_seconds / 60 );/// past minute(s)
   $hours = round($past_seconds / 3600 ); // past hour(s)
   $days = round($past_seconds / 86400 ); /// past day(s) 
   $weeks = round($past_seconds / 604800 );//past week(s) not used it here.
   $months = round($past_seconds / 2419200 ); // past month(s) 
   $years = round($past_seconds / 29030400 ); // past year(s) 
   if($seconds <= 60)
   {
   return "$seconds seconds ago"; 
   }
   else if($minutes <=60)
   {
   if($minutes==1)
   {
   return "One minute ago"; 
   }
   else
   {
   return "$minutes Minutes ago"; 
   }
   }
   else if($hours <=24)
   {
   if($hours==1)
   {
   return "one hour ago";
   }
   else
   {
   return "$hours hours ago";
   }
   }
   else if($days <=7)
   {
   if($days==1)
   {
   return "Yesterday at $time";
   }
   else
   {
   return $day_time." at ".$time;
   } 
   }
   else
   {
   return $full_date." at ".$time;
   }
   }
	

function ago($date)
{
	   $CI = &get_instance();
	   $time = strtotime($date);
	   $periods = array($CI->lang->line('second'), $CI->lang->line('minute'), $CI->lang->line('hour'), $CI->lang->line('day'), $CI->lang->line('week'), $CI->lang->line('month'), $CI->lang->line('year'), $CI->lang->line('decade'));
	   $lengths = array("60","60","24","7","4.35","12","10");
	
	   $now = time();
	
		   $difference     = $now - $time;
		   $tense         = $CI->lang->line('ago');
	
	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		   $difference /= $lengths[$j];
	   }
	
	   $difference = round($difference);
	
	   if($difference != 1) {
		   $periods[$j].= "s";
	   }
	
	   return "$difference $periods[$j] ".$CI->lang->line('ago')." ";
	}
	

function emailTemplate($param)
{
	$base_url  = base_url(); 
	$param_to = $param['to_name'];
	$param_message = $param['message'];
	$message = '<style type="text/css">
.templateWrapper{font-family:Verdana, Geneva, sans-serif;}
.excheader{  height:133px; width:595px;}
.excbody{ padding:30px;}
.excbody a{ color:#0072bc;}
.excbody p{ margin:5px; padding:5px;}
</style>
<div class="templateWrapper" style="width:595px; border:2px solid #eee; min-height:400px;">
<div class="excheader"><img src="'.$base_url.'assets/frontend/img/email_template_header_bg.jpg" alt="glomp" /></div>
<div class="excbody" style="padding:10px;">'.
$param_message
.'</div>
</div>';
	
	return $message;
}

function profile_pic($photo_name,$gender_default='Male')
{
	if($gender_default=='')
		$gender_default = 'Male';
		
	if($photo_name!="" && file_exists("custom/uploads/users/thumb/$photo_name"))
		return "custom/uploads/users/thumb/$photo_name";
	else
		return "custom/uploads/users/thumb/$gender_default".'.jpg';
}
function merchant_logo($merchant_logo)
{
	if($merchant_logo!="" && file_exists("custom/uploads/merchant/thumb/$merchant_logo"))
		return "custom/uploads/merchant/thumb/$merchant_logo";
	else
		return "custom/uploads/merchant/thumb/merchant.jpg";
	
}
function product_logo($product_logo)
{
	if($product_logo!="" && file_exists("custom/uploads/products/thumb/$product_logo"))
		return "custom/uploads/products/thumb/$product_logo";
	else
		return "custom/uploads/products/thumb/product.jpg";
}

function checkIfFriend($profile_id,$thisUserID ){
	 $CI = &get_instance();
	//$sql = "SELECT * FROM  gl_friends WHERE (friend_user_id='".$profile_id."' AND friend_user_friend_id='".$thisUserID."') OR (friend_user_id='".$thisUserID."' AND friend_user_friend_id='".$profile_id."') LIMIT 2";
	$sql = "SELECT * FROM  gl_friends WHERE (friend_user_id='".$profile_id."' AND friend_user_friend_id='".$thisUserID."')  LIMIT 2";
	$result =$CI->db->query($sql);
	return $result->num_rows();
}

function config_value($key)
{
	 $CI = &get_instance();
	$sql = "SELECT config_value FROM gl_config WHERE config_key = '$key'";	
	$res = $CI->db->query($sql);
	$rec = $res->row();
	return isset($rec->config_value)?$rec->config_value:'';
}

function emailValidation($email)
{
	$regexp = "/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/";
	if(preg_match($regexp, $email))
		return "true";
	else
		return "false";
}
function browser_info($agent=NULL) {
  // Declare known browsers to look for
  $known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
    'konqueror', 'gecko');
  // Clean up agent and build regex that matches phrases for known browsers
  // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
  // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
  $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
  $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
  // Find all phrases (or return empty array if none found)
  if (!preg_match_all($pattern, $agent, $matches)) return array();
  // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
  // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
  // in the UA).  That's usually the most correct.
  $i = count($matches['browser'])-1;
  return array('_browser' => $matches['browser'][$i], '_version' => $matches['version'][$i]);
}

}//eoc

?>