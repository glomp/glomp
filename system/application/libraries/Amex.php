<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 *  ======================================= 
 *  Author     : Allan Jed T. Bernabe 
 *  Email      : allan.bernabe@gmail.com
 *   
 *  ======================================= 
 */
require_once APPPATH . "third_party/Amex/VPCPaymentConnection.php";

class Amex extends VPCPaymentConnection {

    var $params;
    var $response = array();

    public function __construct($params = array()) {
        $this->params = $params;
        $this->setSecureSecret($params['secret']);
    }

    function pay() {
        //Required
        $this->addDigitalOrderField('vpc_AccessCode', $this->params['vpc_AccessCode']);
        $this->addDigitalOrderField('vpc_Amount', $this->params['vpc_Amount']);
        $this->addDigitalOrderField('vpc_Command', 'pay');
        $this->addDigitalOrderField('vpc_Locale', 'en');
        $this->addDigitalOrderField('vpc_MerchTxnRef', $this->params['vpc_MerchTxnRef']);
        $this->addDigitalOrderField('vpc_Merchant', $this->params['vpc_Merchant']);
        $this->addDigitalOrderField('vpc_OrderInfo', $this->params['vpc_OrderInfo']);
        $this->addDigitalOrderField('vpc_Version', '1');
        
        if ($this->params['integration_type'] == 2) {
            $this->addDigitalOrderField('vpc_CardNum', $this->params['vpc_CardNum']);
            $this->addDigitalOrderField('vpc_cardExp', $this->params['vpc_cardExp']);            
            $this->addDigitalOrderField('vpc_CardSecurityCode', $this->params['vpc_CardSecurityCode']);
            return $this->sendMOTODigitalOrder('https://vpos.amxvpos.com/vpcdps');
        } else {
            $this->addDigitalOrderField('vpc_ReturnURL', $this->params['vpc_ReturnURL']);
        }

        // Obtain a one-way hash of the Digital Order data and add this to the Digital Order
        $secureHash = $this->hashAllFields();
        $this->addDigitalOrderField("vpc_SecureHash", $secureHash);
    }

    function verify_payment() {
        //Required
        $this->addDigitalOrderField('vpc_AccessCode', $this->params['vpc_AccessCode']);
        $this->addDigitalOrderField('vpc_Amount', $this->params['vpc_Amount']);
        $this->addDigitalOrderField('vpc_Command', 'pay');
        $this->addDigitalOrderField('vpc_Locale', 'en');
        $this->addDigitalOrderField('vpc_MerchTxnRef', $this->params['vpc_MerchTxnRef']);
        $this->addDigitalOrderField('vpc_Merchant', $this->params['vpc_Merchant']);
        $this->addDigitalOrderField('vpc_OrderInfo', $this->params['vpc_OrderInfo']);
        $this->addDigitalOrderField('vpc_ReturnURL', $this->params['vpc_ReturnURL']);
        $this->addDigitalOrderField('vpc_Version', '1');

        $secureHash = $this->hashAllFields();
        $this->params = array();
        return $secureHash;
    }

    function getOrder() {
        //Clear
        $this->params = array();
        return $this->getDigitalOrder('https://vpos.amxvpos.com/vpcpay');
    }
    
    //Method to get response from curl; 2nd integration option only
    function getResponse() {
        return $this->response;
    }

}