<?php
/*
 * This class is for managing result coming from a DB result object
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @version     1.0
 */

class Manage_cache
{
    protected $result_obj;
    protected $num_rows;
    protected $row;
            
    function __construct(){
        //$this->CI = & get_instance();
    }
    /**
     * set_result
     * set result
     * 
     * @params none
     * @return none
    */    
    function set_result($result_obj) {
        $this->result_obj = $result_obj->result();
    }
    /**
     * result
     * get result return as array object
     * 
     * @params none
     * @return array
    */    
    function set_num_rows($num_rows) {
        $this->num_rows = $num_rows;
    }
    function set_row($row) {
        $this->row = $row;
    }
    /**
     * result
     * get result return as array object
     * 
     * @params none
     * @return array
    */    
    function result() {
       $_result_array['result'] = $this->result_obj;
       return $_result_array['result'];
    }
    /**
     * num_rows
     * get num_rows
     * 
     * @params none
     * @return int
    */    
    function num_rows() {
       return $this->num_rows;
    }
    
    function row() {
       return $this->row;
    }
}