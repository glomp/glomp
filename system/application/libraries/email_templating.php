<?php
/*
 * This class is for managing email templates
 * 
 * @author      Allan Bernabe <allan.bernabe@gmail.com>
 * @model depenencies email_templates_m, email_logs_m
 * @version     1.0
 */

class Email_templating 
{
    protected $CI;
    protected $vars = array(
        'subject' => 'Test Subject',
        'lang_id' => '1',
        'from_email' => SITE_DEFAULT_SENDER_EMAIL,
        'from_name' => SITE_DEFAULT_SENDER_NAME,
        'files' => array(),
    );
    protected $record;
            
    function __construct(){
        $this->CI = & get_instance();
        $this->CI->load->model('email_templates_m');
        $this->CI->load->model('email_logs_m');
    }
    /**
     * config
     * Sets config for the the output that will be sending on the email.
     * 
     * @params string
     * @params array
    */
    function config($vars = array())
    {
        //set defaults
        $this->vars = array(
            'subject' => 'Test Subject',
            'lang_id' => 'english',
            'template' => '',
            'from_email' => SITE_DEFAULT_SENDER_EMAIL,
            'from_name' => SITE_DEFAULT_SENDER_NAME,
            'files' => array(),
            'others' => array(),
            'reply_to' => '',
            'templated_body' => '',
        );
        
        $this->vars = $this->set_params($this->vars, $vars);
        $vars = $this->vars;



        
        $rec = $this->CI->email_templates_m->get_record(array(
            'where' => array(
                'template_name' => $vars['template_name'],
                'lang_id' => $vars['lang_id']
            )
        ));
        
        $this->vars['subject'] = $rec['subject'];
        $this->record = $rec;
    }
    /**
     * set_subject
     * 
     * Method sets subject overiding the default subject or the subject from pulled from the database.
     *  - This must be set after the config function and before the send function
     * 
     * @params string
     * @return none
     */    
    function set_subject($subject) {
        
       if ( empty($subject) ) {
           $subject = $this->vars['subject'];
       }
       
       $this->vars['subject'] = $subject;
    }
    /**
     * get_message
     * Method gets the generated message replacing the placeholder(s).
     *  
     * @return string
     */
    function get_message() {
        $vars = $this->vars;
        
        if ( empty($vars['params'])) {
            return $this->record['body'];
        }
        
        return str_replace(array_keys($vars['params']), array_values($vars['params']), $this->record['body']);
    }
    /**
     * Method send generated message into an email.
     *  
     * @return bool
     */
    function send ($print_debugger =FALSE,$templated_body=FALSE) {     
        $url = 'https://api.sendgrid.com/';
        $user = 'glompzac';
        $pass = '6lomp!DOT1+_send'; 
        //$pass = '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'; 
        

        $data['content'] = $this->get_message();
        
        //Insert into log then get ID for the view on browser
        $edata = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'reply_to' => $this->vars['reply_to'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $data['view_id'] = $this->CI->email_logs_m->_insert($edata);
        
        $template = ADMIN_FOLDER . '/email/_layouts/standard_responsive';
        
        if (! empty($this->vars['template']))
        {
            $template = $this->vars['template'];
        }
        
        if (! empty($this->vars['others']))
        {
            $data['others'] = $this->vars['others'];
        }

        if($templated_body)
        {
            $body = $this->vars['templated_body'];
        }
        else
        {
            $body = $this->CI->load->view($template, $data, TRUE, false, false);    
        }
        
         $this->CI->load->library('email');

        $this->CI->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'smtp.sendgrid.net',
          'smtp_user' => 'glompzac',
          'smtp_pass' => '6lomp!DOT1+_send',
          'mailtype' => 'html',
          'smtp_port' => 587,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));

        $this->CI->email->from($this->vars['from_email'], $this->vars['from_name']);
        $this->CI->email->to($this->vars['to']);
        

        if(isset($this->vars['reply_to']) && $this->vars['reply_to'] !='')
        {
            $this->CI->email->reply_to($this->vars['reply_to'],$this->vars['from_name']);
        }
        
        $this->CI->email->subject($this->vars['subject']);
        $this->CI->email->message($body);
        $response = $this->CI->email->send();

        
        
        if ($print_debugger) {
            echo '<pre>';
            echo $this->CI->email->print_debugger();;
            exit;
        }
        
        $where = array(
            'email_log_id' => $data['view_id']
        );
        
        if ($response == true) {
            $data = array(
                'subject' =>  $this->vars['subject'],
                'from_name' =>  $this->vars['from_name'],
                'from_email' => $this->vars['from_email'],
                'reply_to' => $this->vars['reply_to'],
                'to_email' => $this->vars['to'],
                'to_name' => '',
                'email_body' => $body,
                'response' => $response,
                'response_message' => (isset($response)) ? json_encode($response) : '',
                'date_created' => date('Y-m-d H:i:s'),
            );
            
            $insert_id = $this->CI->email_logs_m->_update($where, $data);
            
            if ($response != true) {
                return FALSE;
            }
            else {
                return $where['email_log_id'];
            }
        }
        
        $data = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'reply_to' => $this->vars['reply_to'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'email_body' => $body,
            'response' => 'Unexpected error',
            'response_message' => 'Failed: No connection established.',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $update_id = $this->CI->email_logs_m->_update($where,$data);
        
        return FALSE;
    }
    function send_old ($print_debugger =FALSE) {     
        $url = 'https://api.sendgrid.com/';
        $user = 'glompzac';
        $pass = '6lomp!DOT1+_send'; 
        //$pass = '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'; 
        

        $data['content'] = $this->get_message();
        
        //Insert into log then get ID for the view on browser
        $edata = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $data['view_id'] = $this->CI->email_logs_m->_insert($edata);
        
        $template = ADMIN_FOLDER . '/email/_layouts/standard_responsive';
        
        if (! empty($this->vars['template']))
        {
            $template = $this->vars['template'];
        }
        
        if (! empty($this->vars['others']))
        {
            $data['others'] = $this->vars['others'];
        }
        
        $body = $this->CI->load->view($template, $data, TRUE, false, false);
        
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $this->vars['to'],
            'subject'   => $this->vars['subject'],
            'html'      => $body,
            'text'      => $body,
            'from'      => $this->vars['from_email'],
            'fromname'  => $this->vars['from_name'],
        );
        
        //Adding attachment
        if (! empty($this->vars['files'])) {
            foreach ($this->vars['files'] as $file) {
                $params['files['.$file['filename'].']'] = '@'.$file['path'];
            }
        }
        
        $request =  $url.'api/mail.send.json';
        print_r($request);
        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);

        //print everything out
        print_r($response);
        $response = json_decode($response);
        $print_debugger = TRUE;
        if ($print_debugger) {
            echo '<pre>';
            print_r($response);
            exit;
        }
        
        $where = array(
            'email_log_id' => $data['view_id']
        );
        
        if (isset($response->message)) {
            $data = array(
                'subject' =>  $this->vars['subject'],
                'from_name' =>  $this->vars['from_name'],
                'from_email' => $this->vars['from_email'],
                'to_email' => $this->vars['to'],
                'to_name' => '',
                'email_body' => $body,
                'response' => $response->message,
                'response_message' => (isset($response->errors)) ? json_encode($response->errors) : '',
                'date_created' => date('Y-m-d H:i:s'),
            );
            
            $insert_id = $this->CI->email_logs_m->_update($where, $data);
            
            if ($response->message != 'success') {
                return FALSE;
            }
            else {
                return $where['email_log_id'];
            }
        }
        
        $data = array(
            'subject' =>  $this->vars['subject'],
            'from_name' =>  $this->vars['from_name'],
            'from_email' => $this->vars['from_email'],
            'to_email' => $this->vars['to'],
            'to_name' => '',
            'email_body' => $body,
            'response' => 'Unexpected error',
            'response_message' => 'Failed: No connection established.',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $update_id = $this->CI->email_logs_m->_update($where,$data);
        
        return FALSE;
    }
    /**
     * set_params
     * 
     * Set parameters values
     * 
     * @access public
     * @param array
     * @param array
     * @return array
     */
    function set_params($params, $args)
    {
            foreach ($args as $i => $value)
            {
                    if (isset($args[$i]))
                    {
                            $params[$i] = $value;
                    }
            }

            return $params;
    }
}