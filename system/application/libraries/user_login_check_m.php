<?php
/*
Author : Shree 
Date : July-30-2013
This library will be included into the all pages in header. It wil check the user is logged in or not if not logged in it will redirect to login page
*/
class User_login_check_m
{
	function User_login_check_m()
		{
			
			$CI = & get_instance();
            $public_links =array(   "profile/view",
                                    "profile/",
                                    "profile/menu",
                                    "profile/getMoreStories",
                                    "profile/merchantProduct",
                                    "merchant/about"
                                );            
            if (!in_array($CI->uri->segment(2)."/".$CI->uri->segment(3), $public_links) )
            {            
                // automated test
                if( $CI->input->post( 'automation' ) && $CI->input->post( 'user_id' ) ) {
                    $CI->session->set_userdata( 'user_id', $CI->input->post( 'user_id' ) );
                    $CI->session->set_userdata( 'is_user_logged_in', true );
                }

                if($CI->session->userdata('user_id') && $CI->session->userdata('is_user_logged_in')==true)
                {
                    
                        $data=array(
                        'user_id'=>$CI->session->userdata('user_id'),
                        'user_status' =>'Active' 
                        );
                        
                        $result=$CI->db->get_where('gl_user',$data);
                        if($result->num_rows()==1)
                        {
                            
                            

                            $inactive = 2400; //1200
                            $session_life = time()-$CI->session->userdata('since_user_logged_in');
                            if($session_life > $inactive)
                            {
                                $msg="Session has been expired";
                                //redirect(base_url('landing/logout/'.$msg));
                                //exit();
                            }
                            else
                            {
                                $CI->session->set_userdata('since_logged_in',time());
                            }
                        }
                        else
                        { 
                          $CI->session->set_flashdata("err", "Session has been expired.");
                          $msg="Session has been expired:1";
                          redirect(base_url(MOBILE_M.'/landing/logout/'.$msg));
                          exit();
                        }
                    
                }//if close of checking session set
                else
                {
                    
                    //redirect(base_url(MOBILE_M.'/landing/logout/'));
                    exit();
                }
            }


	}//function clsoe

    public function underage() {
        $CI = & get_instance();
        
        if( $CI->input->post( 'automation' ) ) return false;
        
        $CI->load->model('underage_m');
        $CI->load->model('users_m');
        $user = $CI->users_m->user_info_by_id( $CI->session->userdata('user_id') )->row();
        return $CI->underage_m->is_underage( $user->user_dob, $user->user_city_id );
    }
}/* eoc */