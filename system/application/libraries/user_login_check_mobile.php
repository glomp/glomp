<?php
/*
Author : Shree 
Date : July-30-2013
This library will be included into the all pages in header. It wil check the user is logged in or not if not logged in it will redirect to login page
*/
class User_login_check_mobile 
{
	function User_login_check_mobile()
		{
			
			$CI = & get_instance();
			if($CI->session->userdata('user_id') && $CI->session->userdata('is_user_logged_in')==true)
			{
				
					$data=array(
					'user_id'=>$CI->session->userdata('user_id'),
					'user_status' =>'Active' 
					);
					
					$result=$CI->db->get_where('gl_user',$data);
					if($result->num_rows()==1)
					{
						
						

						$inactive = 2400; //1200
						$session_life = time()-$CI->session->userdata('since_user_logged_in');
						if($session_life > $inactive)
						{
							$msg="Session has been expired";
							//redirect(base_url('landing/logout/'.$msg));
							//exit();
						}
						else
						{
							$CI->session->set_userdata('since_logged_in',time());
						}
					}
					else
					{ 
					  $CI->session->set_flashdata("err", "Session has been expired.");
					  $msg="Session has been expired:1";
					  redirect(base_url('mobile/landing/logout/'.$msg));
					  exit();
					}
				
			}//if close of checking session set
			else
			{
				
				redirect(base_url('mobile/landing/logout/'));
				exit();
			}


	}//function clsoe

}///EOC
?>