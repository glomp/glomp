<?php
/**
 * extends CI_Lang
 */
class MY_Lang extends CI_Lang {

    function __construct() {
        parent::__construct();

    }
    
	/**
	 * Fetch a single line of text from the language array
	 *
	 * @access	public
	 * @param	string	$line	the language line
     * @param   string  $default the default text
	 * @return	string
	 */
	function line($line = '', $default = '' ) {
		$value = ($line == '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];

		if ($value === FALSE) {
            if( $default ) {
                return $default;
            }
			log_message('error', 'Could not find the language line "'.$line.'"');
            return FALSE;
		}

		return $value;
	}
    
    function compound( $line, $data, $text = "" ) {
        $CI =& get_instance();
        $CI->load->library('parser');

        if( $line && isset( $this->language[ $line ] ) ) {
            $text = $this->language[ $line ];
        }
        if( !$text ) {
            return $line;
        }

        return $CI->parser->parse_string( $text, $data, true );
    }
}
