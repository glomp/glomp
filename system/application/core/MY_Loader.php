<?php
class MY_Loader extends CI_Loader
{	
	function __construct()
    {
		//Get raw post variable and save it into the "post_vars" variable. 
		// - This is Workaround to get by-pass XSS global filtering		
        parent::__construct();
    }
    public function view($view, $vars = array(), $return = TRUE, $attachedLinkedIn=FALSE, $sanitize=TRUE)
	{
        $temp =$this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_object_to_array($vars), '_ci_return' => $return));
        if($sanitize)
        {        
            if(!$attachedLinkedIn)
                echo $this->sanitize_output($temp);
            else
            {
                $temp = $this->sanitize_output($temp);
                $head='
                <script type="text/javascript" src="'.($_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http').'://platform.linkedin.com/in.js">
                    api_key: 75ocjfra1o2yjt
                    authorize: true
                    onLoad: liInitOnload
                </script>
                </head>';
                $temp = str_replace("</head>",$head,$temp);                
                echo $temp;
                
            }
            return;
        }
        return $temp;         
	}
    function compress_page($buffer) {
        $search = array(
        "/\/\*(.*?)\*\/|[\t\r\n]/s" => "",
        "/ +\{ +|\{ +| +\{/" => "{",
        "/ +\} +|\} +| +\}/" => "}",
        "/ +: +|: +| +:/" => ":",
        "/ +; +|; +| +;/" => ";",
        "/ +, +|, +| +,/" => ","
        );
        $buffer = preg_replace(array_keys($search), array_values($search), $buffer);
        return $buffer;
    }
    function sanitize_output($buffer) {

        return $buffer;
        /*
        $search = array(
            '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
            '/[^\S ]+\</s',  // strip whitespaces before tags, except space
            '/(\s)+/s'       // shorten multiple whitespace sequences
        );

        $replace = array(
            '>',
            '<',
            '\\1'
        );
        $buffer = preg_replace($search, $replace, $buffer);
        $output = str_replace(array("\r\n", "\r"), "\n", $buffer);
        return $buffer;
        
        $lines = explode("\n", $output);
        /**/
        $lines = explode("\n", $buffer);
        $new_lines = array();

        foreach ($lines as $i => $line) {
            if(!empty($line))
                $new_lines[] = trim($line);
        }
        
        $new_lines= implode($new_lines);
        $new_lines.='<!---->';
        return $new_lines;
        //return $buffer;        
    }
}
