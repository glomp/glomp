<?php
class MY_Input extends CI_Input {
	var $post_vars;
	
	function __construct()
    {
		//Get raw post variable and save it into the "post_vars" variable. 
		// - This is Workaround to get by-pass XSS global filtering
		$this->post_vars = $_POST;
        parent::__construct();
    }
}