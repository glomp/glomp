<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
/*****Other Constant defined by shree*/
define('ADMIN_FOLDER','adminControl');
define('MERCHANT_FOLDER','merchantControl');
define('MOBILE_FOLDER','mobile');
define('MOBILE_FOLDER2','mobile2');
define('MOBILE_M','m');
define('DEFAULT_LANG_ID',1);
define('SITE_DEFAULT_SENDER_EMAIL','no-reply@glomp.it');
define('SITE_DEFAULT_SENDER_NAME','glomp');


define('SITE_DEFAULT_SUPPORT_EMAIL','support@glomp.it');
define('SITE_DEFAULT_SUPPORT_NAME','glomp');


$currentDir = dirname(__FILE__);
$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/glomp/project/';
/* Directories */
define('_ROOT_DIR_', realpath($currentDir.'/..'));
define('_CONTROLLER_DIR_',  _ROOT_DIR_.'/controllers/');
define('_VIEW_DIR_',  _ROOT_DIR_.'/views/');
define('_VIEW_INCLUDES_DIR_',  _ROOT_DIR_.'/views/includes/');
define('_VIEW_INCLUDES_DIR_ADMIN_',  _ROOT_DIR_.'/views/adminControl/includes/');
define('_LIB_DIR_',  _ROOT_DIR_.'/libraries/');

#mobile directory
define('_CONTROLLER_DIR__MOBILE_',  _ROOT_DIR_.'/controllers/mobile/');
define('_VIEW_DIR_MOBILE_',  _ROOT_DIR_.'/views/mobile/');
define('_VIEW_INCLUDES_DIR_MOBILE_',  _ROOT_DIR_.'/views/mobile/includes/');
define('DOCUMENTROOT',$documentRoot);

define('_EN_LANG_FILE_',  _ROOT_DIR_.'/language/english/en_label_lang.php');
define('_CN_LANG_FILE_',  _ROOT_DIR_.'/language/chinese/cn_label_lang.php');

#user photo
define('USER_PHOTO_THUMB_WIDTH',140);
define('USER_PHOTO_THUMB_HEIGHT',140);

#global config
define('VOCHER_EXPIRATION_DAY',30);
#Turn ON global caching
define('CACHE', 0);


#global Config Facebook
define('FACEBOOK_API_STATUS','DEV');

# paypal action
define('PAYPAL_EMAIL_ADDRESS','z@zzo-creative.com');//
define('PAYPAL_FORM_ACTION','https://www.paypal.com/cgi-bin/webscr');
define('PAYPAL_NOTIFY_URL','http://glomp.it/paypalIpnCurl.php');


# amex brand forex
# amex brand forex
define('BRAND_FOREX_AMEX',1);//

/*AMEX credit card - for dev*/
/*AMEX credit card - for dev*/
define('MACALLAN_EMAIL_1','magbanua.ryan@gmail.com');//
define('MACALLAN_EMAIL_2','magbanua.ryan@gmail.com');//

define('WINESTORE_EMAIL_1','magbanua.ryan@gmail.com');//
define('WINESTORE_EMAIL_2','magbanua.ryan@gmail.com');//

define('MACALLAN_BRAND_PRODUCT_ID',25);//25
define('WINESTORE_BRAND_PRODUCT_ID',26);//26

define('AMEX_CARD_BIN',3456);// live bin is 3762 //dev is 3456
define('AMEX_vpc_AccessCode','24A5C684');
define('AMEX_vpc_Merchant','TEST9800080608');
define('AMEX_vpc_OrderInfo','Total Order');
define('AMEX_secret','3FEFDC93E02547BBA1F29DEEFFF6A911');
/*AMEX credit card - for dev*/


/*AMEX credit card - for LIVE*/
/*AMEX credit card - for LIVE*
define('MACALLAN_EMAIL_1','sulina@magnum.com.sg');//
define('MACALLAN_EMAIL_2','cynthia.tan@edrington.sg');//

define('WINESTORE_EMAIL_1','fdv@winestore.sg');//
define('WINESTORE_EMAIL_2','orders@winestore.sg');//

define('MACALLAN_BRAND_PRODUCT_ID',25);//25
define('WINESTORE_BRAND_PRODUCT_ID',26);//26


define('AMEX_CARD_BIN',3762);// live bin  SGD
define('AMEX_vpc_AccessCode','BA234C8F');
define('AMEX_vpc_Merchant','9800080608');
define('AMEX_vpc_OrderInfo','Total Order');
define('AMEX_secret','4229EFDB8A747835C825B16E5171D70B');
/*AMEX credit card - for LIVE*/


define('PAYPAL_URL', 'https://www.paypal.com/cgi-bin/webscr');







/**CONSTANTS FOR HSBC BRANDS*/
define('HSBC_BRAND_PROD_ID_MACALLAN',27);//27
define('HSBC_BRAND_PROD_ID_SNOW_LEOPARD',28);//28
define('HSBC_BRAND_PROD_ID_LONDON_DRY',29);//29
define('HSBC_BRAND_PROD_ID_SINGBEV',30);//30

define('HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP',31);//31
define('HSBC_BRAND_PROD_ID_DAILY_JUICE',32);//32
define('HSBC_BRAND_PROD_ID_KPO',33);//33
define('HSBC_BRAND_PROD_ID_NASSIM_HILL',34);//34
define('HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN',35);//35




/**CONSTANTS FOR uob BRANDS*/
define('UOB_BRAND_PROD_ID_MACALLAN',36);//36
define('UOB_BRAND_PROD_ID_SNOW_LEOPARD',37);//37
define('UOB_BRAND_PROD_ID_LONDON_DRY',38);//38
define('UOB_BRAND_PROD_ID_SINGBEV',39);//39

define('UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP',40);//40
define('UOB_BRAND_PROD_ID_DAILY_JUICE',41);//41
define('UOB_BRAND_PROD_ID_KPO',42);//42
define('UOB_BRAND_PROD_ID_NASSIM_HILL',43);//43
define('UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN',44);
define('UOB_BRAND_PROD_ID_VALRHONA',55);//55

//44/**CONSTANTS FOR dbs BRANDS*/
define('DBS_BRAND_PROD_ID_MACALLAN',45);//45
define('DBS_BRAND_PROD_ID_SNOW_LEOPARD',46);//46
define('DBS_BRAND_PROD_ID_LONDON_DRY',47);//47
define('DBS_BRAND_PROD_ID_SINGBEV',48);//48

define('DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP',49);//49
define('DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN',50);//50
define('DBS_BRAND_PROD_ID_DAILY_JUICE',51);//51
define('DBS_BRAND_PROD_ID_KPO',52);//52
define('DBS_BRAND_PROD_ID_NASSIM_HILL',53);//53
define('DBS_BRAND_PROD_ID_VALRHONA',54);//53
//new xmas 2016
define('DBS_BRAND_PROD_ID_LAURENT_PERRIER',56);//
define('DBS_BRAND_PROD_ID_PROVIDORE',57);//
define('DBS_BRAND_PROD_ID_GLENFIDDICH',58);//
define('DBS_BRAND_PROD_ID_MOET_CHANDON',59);//
define('DBS_BRAND_PROD_ID_KUSMI_TEA',60);//
define('DBS_BRAND_PROD_ID_EURACO',61);//


/**CONSTANTS FOR new AMEX BRANDS*/
define('AMEX_BRAND_PROD_ID_MACALLAN',56);//56
define('AMEX_BRAND_PROD_ID_SNOW_LEOPARD',57);//57
define('AMEX_BRAND_PROD_ID_LONDON_DRY',58);//58
define('AMEX_BRAND_PROD_ID_SINGBEV',59);//59

define('AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP',60);//60
define('AMEX_BRAND_PROD_ID_DAILY_JUICE',61);//61
define('AMEX_BRAND_PROD_ID_KPO',62);//62
define('AMEX_BRAND_PROD_ID_NASSIM_HILL',63);//63
define('AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN',64);//64
define('AMEX_BRAND_PROD_ID_VALRHONA',65);//65

?>