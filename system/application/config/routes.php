<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "landing";
$route['404_override'] = '';

$route['page/index/terms'] = "page/index/2";
$route['page/index/faq'] = "page/index/3";
$route['page/index/policy'] = "page/index/4";

$route['landing/register'] = 'register';
$route['landing/register/prefill'] = 'register';
//$route['landing/register/facebook'] = 'register';
$route['landing/register/facebook'] = 'user2/register';
//if(  strpos('glomp-hybrid-app', $_SERVER['HTTP_USER_AGENT']) > -1 ) {
    $route['m/landing/register_form'] = 'register';
    $route['m/landing/register_form/(:any)'] = 'register';
    $route['m/landing/register2'] = 'register';
    $route['m/landing/register_landing'] = 'register/auth_option';
    $route['m/landing/register2_form'] = 'register';
    $route['m/landing/register2_form/(:any)'] = 'register';
//}
$route['user/register'] = 'user2/register';    
$route['user/register?linkedin=1'] = 'user2/register';    
$route['m/user/register'] = 'user2/register';
$route['m/user/register/(:any)/(:any)'] = 'user2/register/$1/$2';
$route['m/user/register/facebook'] = 'user2/register';
$route['m/user/register/linkedin'] = 'user2/register';

    
$route['m/redeem/voucher/(:any)'] = 'm/redeem/voucher2/$1';
$route['m/redeem/validated/(:any)'] = 'm/redeem/validated2/$1';

$route['m/brands/preview/(:any)'] = 'brands/preview/$1';
$route['m/brands/preview/(:any)/(:any)'] = 'brands/preview/$1/$2';

$route['profile/menu/brands/(:any)'] = 'profile/brands/($1)';
$route['profile/menu/brands/(:any)/(:any)'] = 'profile/brands/($1)/($1)';
$route['profile/menu/brands/(:any)/(:any)/(:any)'] = 'profile/brands/($1)/($1)/($1)';
$route['profile/menu/brands/(:any)/(:any)/(:any)/(:any)'] = 'profile/brands/($1)/($1)/($1)/($1)';

$route['m/profile/menu/brands/(:any)'] = 'm/profile/brands/($1)';
$route['m/profile/menu/brands/(:any)/(:any)'] = 'm/profile/brands/($1)/($1)';
$route['m/profile/menu/brands/(:any)/(:any)/(:any)'] = 'm/profile/brands/($1)/($1)/($1)';
$route['m/profile/menu/brands/(:any)/(:any)/(:any)/(:any)'] = 'm/profile/brands/($1)/($1)/($1)/($1)';


$route['amex-sg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['amex-sg'] = 'brands/view/amex';
$route['m/amex-sg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['m/amex-sg'] = 'brands/view/amex/0/mobile';




$route['hsbcsg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['hsbcsg'] = 'brands/view/hsbc';
$route['m/hsbcsg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['m/hsbcsg'] = 'brands/view/hsbc/0/mobile';



$route['uobsg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['uobsg'] = 'brands/view/uob';
$route['m/uobsg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['m/uobsg'] = 'brands/view/uob/0/mobile';


$route['dbssg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['dbssg'] = 'brands/view/dbs';
$route['m/dbssg/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['m/dbssg'] = 'brands/view/dbs/0/mobile';




$route['m/brands/info/(:any)'] = 'brands/brand_product_info/$1';
$route['m/brands/(:any)/(:any)/(:any)'] = 'brands/$1/$2/$3';
$route['m/brands/(:any)/(:any)'] = 'brands/$1/$2';
$route['m/brands/(:any)'] = 'brands/$1';

$route['u/(:any)'] = 'm/landing/index/$1'; //from glomp! thru sms

//$route['hsbcsg'] = 'landing/hsbc';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
