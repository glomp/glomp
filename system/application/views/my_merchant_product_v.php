<script type="text/javascript">
$(document).ready(function(e) {
    $(".displyPopUp").on('mouseenter',function(){
		$(this).find('.popUp').show();
		}).on('mouseleave',function(){$('.popUp').hide();});

$('.addFev').on('click',function(){
		var _this =$(this);
		var product_id = _this.data('product_fev_id');
		_this.parents('.displyPopUp').find(".popUp").hide();
		_this.parent('.thumbnail').prepend("<div class='addingFev'><?php echo $this->lang->line('Adding', 'Adding...');?></div>");
		$.ajax({
					type: "GET",
					url: 'profile/addFevitemToFevList/'+product_id,
					cache: false,
					success: function(html){
						if(html=='success'){
							_this.fadeOut(2000);
							$('.addingFev').fadeOut(2000);
							var search_url=window.location.search;												
							if(search_url==''){
								search_url='?';
							}
							else 
								search_url+='&';
							window.location.href = '<?php echo base_url('profile/index/')?>/'+search_url+'favAdded=true';								
						}
						}
					 });			 
		});
	$('.backToMerchantPannel').click(function(){
	$('#merchantProductDetails').hide();
	$('#tabMerchant').fadeIn();
			
	});
	
});
</script>

      <div class="merchantProduct">
      <div class="row-fluid">
      <div class="span2">
      <div class="left">
      <ul class="thumbnails">
            <li class="span12">
            <div class="thumbnail">
			<a href="<?php echo base_url("merchant/about/".$res_merchant->merchant_id)?>?frm=you" >
             <img src="<?php echo $this->custom_func->merchant_logo($res_merchant->merchant_logo);?>" alt="<?php echo $res_merchant->merchant_name;?>">        
			</a> 
            </div>
            </li>
            
        	</ul>
             <a href="javascript:void(0);" class="btn-custom-gray-big btn-block backToMerchantPannel" style="font-weight:normal;"><?php echo $this->lang->line('merchant_list', 'Merchant List');?></a>
      </div>
      </div>
      <div class="span10">
        <div class="tabInnerContent">
      <?php # dirnks tab?>
                <div class="merchantCatProduct">
              		<div class="row-fluid"> 
                		<div class="span12"> 
	 		<?php
		   $num_product = $merchant_product->num_rows();
		   if($num_product>0)
		   {
			  $i=1;
		      $j=1;
			foreach($merchant_product->result() as $row_product)
			{
			$merchantName=$this->merchant_m->merchantName($row_product->prod_merchant_id);
			 if($i==1) {?> <ul class="thumbnails"> <?php } ?>
            <li class="roductImages displyPopUp ">
             <div class="notification">
                 <?php echo $row_product->prod_point;?>
                 <br />
                 <div class="notification_pts"><?php echo $this->lang->line('pts', 'pts'); ?></div>
             </div>
              <div class="popUp">
                  <div class="row-fluid">
                  <div class="span5">
                   <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image);?>" alt="<?php echo stripslashes($row_product->prod_name);?>">
                  </div>
                  <div class="span7">
                  <div class="name1"><?php echo stripslashes($row_product->prod_name);?></div>
                   <p><?php echo stripslashes($row_product->prod_details);?></p>
                  </div>
                  </div>
              
              </div>
           
            <div class="thumbnail">
            <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image);?>" alt="<?php echo stripslashes($row_product->prod_name);?>">
            
            <?php 
			// checking wheteher this product is fev or not if not add will display
			if($this->product_m->checkAddFevitemToFevList($row_product->prod_id)==0){ ?>
        <div class="addFev" data-product_fev_id='<?php echo stripslashes($row_product->prod_id);?>'>
        <img src="assets/frontend/img/Shape_Plus.png" alt="<?php echo $this->lang->line('Add_Favorites_Now', 'Add Favorites Now');?>" /></div>
        <?php }?>
            </div>
            <div class="name1" style="padding:0px 4px; border:solid 0px"><?php echo $merchantName;?></div>
			<div class="name2" style="padding:0px 4px; border:solid 0px"><?php echo stripslashes($row_product->prod_name);?></div>
          <?php /*?>  <div class="name2">KFC Kings Burger</div><?php */?>
          
            </li>
             <?php
		   if($i==5 || $j==$num_product) {?></ul>
		   <?php 
				   }
				$j++;
				if($i==5)
				{
					$i=1;
				}
				else
					$i++;
				} /// end of foreach
		   }//enf of num result check
		   else
		   {
			   ?>
          	 <p class="alert alert-info"><?php echo $this->lang->line('sorry_no_product_available', 'No available products here as yet. Please check out another product category.');?></p>
		   <?php
			   }
		?>
                        
                    </div>
                   </div>
                   
                 </div>
                 </div>
<?php #drinks ta close ?>
            
          
      </div>
      </div>
      </div>
