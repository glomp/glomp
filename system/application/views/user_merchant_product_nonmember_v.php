<style type="text/css">
    .thumbnails li {
        min-height: 200px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(e) {
        $(".displyPopUp").on('mouseenter',function(){
            $(this).find('.popUp').show();
        }).on('mouseleave',function(){$('.popUp').hide();});
		
        $('.backToMerchantPannel').click(function(){
            $('#merchantProductDetails').hide();
            $('#tabMerchant').fadeIn();
			
        });
    });
</script>
<?php //include('includes/glomp_pop_up_js_merchant.php');
$merchant_button=$this->lang->line('merchant_list');
?>
<div class="row-fluid">
    <div class="span12">
        <div class="merchantProduct">
            <div class="row-fluid">
                <div class="span2" style="width:130px">
                    <div class="left" >
                        <ul class="thumbnails">
                            <li class="span12">
                                <a href="<?php echo base_url("merchant/about/" . $res_merchant->merchant_id) . "?frm=" ?>" >
                                    <div class="thumbnail">
                                        <img src="<?php echo $this->custom_func->merchant_logo($res_merchant->merchant_logo); ?>" alt="<?php echo $res_merchant->merchant_name; ?>">
                                    </div>
                                </a>
                            </li>

                        </ul>
                        <a href="javascript:void(0);" class="btn-custom-gray-big btn-block backToMerchantPannel" style="font-weight:normal;"><?php echo str_replace(" ","&nbsp;",$merchant_button); ?></a>
                    </div>
                </div>
                <div class="span10"  style="width:480px; margin-top:-10px;">
                    <div class="merchantCatProduct">
                        <div class="tabInnerContent">
                            <?php
                            $num_product = $merchant_product->num_rows();
                            if ($num_product > 0) {
                                $i = 1;
                                $j = 1;
                                foreach ($merchant_product->result() as $row_product) {
								$merchantName=$this->merchant_m->merchantName($row_product->prod_merchant_id);
                                    if ($i == 1) {
 ?> <ul class="thumbnails"> <?php } ?>
                                        <li class="roductImages displyPopUp glompItemMerchant" style="cursor:pointer;" data-image="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" data-point="<?php echo $row_product->prod_point; ?>" data-productname="<?php echo stripslashes($row_product->prod_name); ?>" data-id="<?php echo $row_product->prod_id; ?>"  data-merchantname="<?php echo stripslashes($this->merchant_m->merchantName($row_product->prod_merchant_id)); ?>">

                                            <div class="notification"><?php echo $row_product->prod_point; ?></div>
                                            <div class="popUp">
                                                <div class="row-fluid">
                                                    <div class="span5">
                                                        <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" alt="<?php echo stripslashes($row_product->prod_name); ?>">
                                                    </div>
                                                    <div class="span7">
                                                        <div class="name1"><?php echo $merchantName;?></div>
												<div class="name2"><?php echo stripslashes($row_product->prod_name);?></div>
<?php /* ?><div class="name2">KFC Kings Burger</div><?php */ ?>
                                                        <p><?php echo stripslashes($row_product->prod_details); ?></p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="thumbnail">
                                        <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" alt="<?php echo stripslashes($row_product->prod_name); ?>">

                                    </div>

                                    <div class="name1" style="padding:0px 4px; border:solid 0px"><?php echo $merchantName;?></div>
			<div class="name2" style="padding:0px 4px; border:solid 0px"><?php echo stripslashes($row_product->prod_name);?></div>
<?php /* ?>  <div class="name2">KFC Kings Burger</div><?php */ ?>

                                </li>
<?php if ($i == 3 || $j == $num_product) { ?></ul>
                                <?php
                                                }
                                                $j++;
                                                if ($i == 3) {
                                                    $i = 1;
                                                }
                                                else
                                                    $i++;
                                            } /// end of foreach
                                        }//enf of num result check
                                ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
/*/Unbing first*/
$('.glompItemMerchant').unbind('click');
/*/Trigger click*/
$('.glompItemMerchant').click(function() {
		if (typeof(doGlomp) === 'function' && linkedIn == false) { 
			doGlomp($(this));
		}
                else if (linkedIn == true) {
                    doGlompLi($(this));
                }
		else{
			$("#showError").hide();
			$("#showSuccess").hide();
			var _this = $(this);
			$('.glompItemPopUp').fadeIn();
			var _image = _this.data('image');
			var _point = _this.data('point');
			var _productName = _this.data('productname');
			var _merchantName = _this.data('merchantname');
			var _id = _this.data('id');
			$("#pipup_image").attr('src',_image);
			$("#popUpItem").text(_productName);
			$("#popUpPoints").text(_point);
			$("#popUpMerchant").text(_merchantName);		
			$("#product_id").val(_id);	
		}
});
</script>