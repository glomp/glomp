<?php
switch ($public_alias) {
	    case 'hsbc':
	        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
	        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  HSBC_BRAND_PROD_ID_KPO;
	        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona = '0';
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
	        break;
	    case 'uob':
	        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
	        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  UOB_BRAND_PROD_ID_KPO;
	        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
	        
	        break;
        case 'dbs':
	        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
	        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  DBS_BRAND_PROD_ID_KPO;
	        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
	        $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
	        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
	        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
	        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
	        $euraco = DBS_BRAND_PROD_ID_EURACO;
	        break;

        case 'amex':
	        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
	        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  AMEX_BRAND_PROD_ID_KPO;
	        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
	        break;
	}


	if(!defined('BRAND_PROD_ID_MACALLAN'))     define('BRAND_PROD_ID_MACALLAN',$macallan);
	if(!defined('BRAND_PROD_ID_SNOW_LEOPARD')) define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
	if(!defined('BRAND_PROD_ID_LONDON_DRY'))   define('BRAND_PROD_ID_LONDON_DRY',$london);

	define('BRAND_PROD_ID_SINGBEV',$singbev);
	define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
	define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

	define('BRAND_PROD_ID_KPO',$kpo);
	define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
	define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
	define('BRAND_PROD_ID_VALRHONA',$valrhona);
	define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
	define('BRAND_PROD_ID_PROVIDORE',$providore);
	define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
	define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
	define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
	define('BRAND_PROD_ID_EURACO',$euraco);
?>
<script>
    var selectedTab;
    var merID;
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
			
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var items = [], items_options = [];
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".displayPopUp").on('mouseenter',function(){
                    $(this).find('.popUp').show();
                }).on('mouseleave',function(){
                    $('.popUp').hide();
                });
				alert_winer_welcome_pop();
            });
			function alert_winer_welcome_pop(brand_product_id)
			{
				var text="";
				<?php
					switch ($brand_product_id) {
						case WINESTORE_BRAND_PRODUCT_ID:
							echo '
								text ="Welcome to this special offer for great NZ wines. Choose either a mix of wines here or select a multiple of your favourite choice. Minimum order of 2 bottles. Enjoy 20% off for 2-3 bottles and 30% off for 4 or more bottles. Delivery in Singapore only is complementary.";
							';
							break;
						case MACALLAN_BRAND_PRODUCT_ID:
							echo '
								text ="Welcome to this special offer for The Macallan Highland Single Malt Scotch Whisky. Enjoy 15% off Recommended Retail Prices. Delivery in Singapore only is complementary.";
							';
							break;
						case BRAND_PROD_ID_MACALLAN:
						case BRAND_PROD_ID_SNOW_LEOPARD:
						case BRAND_PROD_ID_LONDON_DRY:
							echo '
								text ="Welcome to this very special offer for The Macallan Highland Single Malt Scotch Whisky. Enjoy up to 25% off Recommended Retail Prices. Delivery in Singapore is complementary for combined orders of $300 and above of The Macallan whiskies, Snow Leopold Vodka and No.3 London Dry Gin. Orders below $300 can be collected during office hours at Asia Square.";
							';
							break;
						case BRAND_PROD_ID_SINGBEV:
							echo '
								text ="Welcome to this very special offer for a wide variety of renowned alcoholic beverages. Enjoy up to 30% off Recommended Retail Prices. Delivery in Singapore is complementary for combined orders of $350 and above from SingBev. A $35 delivery fee applies for smaller orders.";
							';
							break;
						case BRAND_PROD_ID_SWIRLS_BAKESHOP:
							echo '
								text ="Welcome to this very special offer for delicious gourmet cupcakes from Swirls Bakeshop. Enjoy 10% off a wide variety of fun and delicious flavours. Pick up at 2 shop locations or have them delivered to you for $16.";
							';
							break;
						case BRAND_PROD_ID_DAILY_JUICE:
							echo '
								text ="Welcome to this very special offer for healthy, revitalising juice packages from Daily Juice. Enjoy 10% off the retail price. Complimentary delivery in Singapore.";
							';
							break;
						case BRAND_PROD_ID_DELIGHTS_HEAVEN:
							echo '
								text ="Welcome to this very special offer from The Delights Heaven. Enjoy 15% off a special Premium Gift box of macaroons. Pick up at our shop at Bugis Cube or have them delivered to you for $25.";
							';
							break;
						case BRAND_PROD_ID_KPO:
							echo '
								text ="";
							';
							break;
						case BRAND_PROD_ID_NASSIM_HILL:
							echo '
								text ="";
							';
							break;
						case BRAND_PROD_ID_VALRHONA:
							echo '
								text ="Welcome to this very special offer from Valrhona Gourmet chocolates. Enjoy 28% off Recommended Retail Prices for a selection of Gift boxes. Delivery in Singapore is complementary for orders of $300 and above. Orders below $300 can be delivered for $35 or collected during office hours at Euraco Finefood in Henderson Road.";
							';
							break;

						case BRAND_PROD_ID_LAURENT_PERRIER:
							echo '
								text ="Welcome to this very special offer for Laurent Perrier champagne with special gift box. Enjoy over to 20% off Recommended Retail Prices. Delivery in Singapore is complimentary for orders of 3 bottles or above otherwise a delivery charge of $20 applies. Whilst stocks last.";
							';
							break;
						case BRAND_PROD_ID_PROVIDORE:
							echo '
								text ="Welcome to this very special offer for The Providore hampers. Enjoy 13% off these quality hampers from The Providore. Delivery in Singapore is free on all orders above $200 otherwise a delivery fee of $20 applies. Otherwise, please pick up your purchases at any of The Providore stores. Please go to www.theprovidore.com/store/ for address and opening hours.";
							';
							break;
						case BRAND_PROD_ID_GLENFIDDICH:
							echo '
								text ="Welcome to this exclusive offer for this Glenfiddich 18 Year Old Scotch Whisky Limited Edition Gift Set. Delivery in Singapore is complementary. Whilst stocks last.";
							';
							break;
						case BRAND_PROD_ID_MOET_CHANDON:
							echo '
								text ="Welcome to this exclusive for this Moet & Chandon Ice Imperial, Customized Bottle with Swarovski components. Delivery in Singapore is complementary. Whilst stocks last.";
							';
							break;
						case BRAND_PROD_ID_KUSMI_TEA:
							echo '
								text ="Welcome to this special offer for Kusmi Tea Paris. These fantastic gourmet tea sets are newly launched in Singapore. Delivery in Singapore is complementary.";
							';
							break;
						case BRAND_PROD_ID_EURACO:
							echo '
								text ="Welcome to this special offer for Euraco Finefood hampers. Enjoy these fantastic gourmet hampersat 15% off recommended retail prices. Delivery in Singapore is complementar for orders of $300 or over. A $30 delivery fee is applicable otherwise.";
							';
							break;

							
					}
				?>
				if(text!='')
				{
					var NewDialog = $('<div id="" align="center" style="padding-top:15px !important; font-size:14px">\
						<p align="justify">'+text+'</p>\
										</div>');
						NewDialog.dialog({                    
							dialogClass:'noTitleStuff',
							title: "",
							autoOpen: false,
							resizable: false,
							modal: true,
							width: 500,
							show: '',
							hide: '',
							buttons: [
									{text: "OK",
									"class": 'btn-custom-darkblue width_80_per',
									click: function() {
										$(this).dialog("close");
										
										
									}}
							]
						});
						NewDialog.dialog('open');
				}
			}
        </script>
        <style>
            .product-brand-desc {
              background-color: #DEE5ED;
              padding: 15px;
              border-radius: 10px;
              margin: 10px 30px 10px 0px;
              color: #646B78;
            }
            .product-brand-desc p {
                color: #646B78 !important;
                font-weight: normal;
                line-height: 1.5;
                margin-top: 0px;
            }
             
            .merchantCatProduct div div p {line-height: 17px !important} 
        </style>
        <?php include('includes/glomp_pop_up_my_brand_js.php'); ?>
        
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
									
                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:10px;">
										
                                                <div class="profileTab">
                                                    <ul>
                                                       
                                                    </ul>

                                                    <div class="clearfix"></div>
                                                </div>
												
												<div id="tabBrands" class="tabs" style="background:#343239">
														<a style="float:left;margin:10px 0px 0px 15px;" href="<?php echo site_url();?>brands/view/<?php echo $brand->public_alias;?>" class="btn-custom-transparent_white" >
															<?php echo $this->lang->line('back','Back');?>
														</a>
														<a href="<?php echo site_url('brands/cart/'.$brand->public_alias);?>" >
															<div style="float:right;margin:10px 15px 0px 0px;" class="btn-custom-gray-light" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty_'.$brand->public_alias)=='' ? 'class="gray">0': 'class="red">'.$this->session->userdata('session_cart_qty_'.$brand->public_alias);?></span>)</div>
														</a>
														<div style="clear:both"></div>
													<div id="brand-layer" class="row-fluid" style="display: block;padding-bottom:10px;margin-top:-15px;">
														<div class="tabContent" style="background:#343239">
														
															<div class="span2 thumbnails">
																<div id="brand-info" style="padding: 14px; border-bottom-left-radius: 5px; border-top-right-radius: 5px; background: rgb(210, 215, 225);">
																	<div class="thumbnail">
																		<?php
																			$src = "http://placehold.it/190x102";
																			if($brandproducts[0]->image_logo !="")// && file_exists(site_url('custom/uploads/brands/products/'.$brandproducts[0]->image_logo)))
																				$src = site_url('custom/uploads/brands/products/'.$brandproducts[0]->image_logo);
																		?>
																		<img  src="<?php echo $src; ?>">
																	</div>
																</div>
																<br>
																
															</div> 
															
															<div id="" class="span10 tabInnerContent" comment="Will show after selecting a BrandProduct" style="display: block;padding-top:1px;">
																<div id="brandproduct-info" class="span3" comment="This will propagate after selecting a brandproduct" style="padding: 14px; border-bottom-left-radius: 5px; border-top-right-radius: 5px; display: none; background: rgb(210, 215, 225);">
																	
																</div>
																<div id="brand_merchant_layer" class="span12 merchantCatProduct" comment="List of Products from Merchants">
																	
																		<?php
																		if($brand_product_id == WINESTORE_BRAND_PRODUCT_ID)
																		{
																			$list = get_winestore('list');
																			$x = 0;
																			foreach($list as $key=>$l):
																				$x++;
																				?>
																					<div class="product-brand-desc">
																						<?php echo $l['desc'];?>
																					</div>
																					<ul class="thumbnails" >
																					<?php foreach($merchant_products as $product):
																					
																						$d = get_winestore_prod_data($product->product_id);
																						if($d['group']==$x)
																						{
																					?>
																							<script type="text/javascript">
																								items[<?php echo $product->product_id;?>] =<?php echo json_encode($product);?>;
																							</script>
																							<li class="roductImages displayPopUp " data-image="<?php echo site_url();?>/custom/uploads/products/thumb/<?php echo $product->prod_image;?>" data-point="<?php echo $product->prod_point;?>" data-productname="<?php echo $product->prod_name;?>" data-id="<?php echo $product->product_id;?>" data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" data-merchantname="<?php echo $product->merchant_name;?>" style="min-height:180px;cursor:pointer;margin:10px 30px 10px 0px;border:solid 1px #ddd;border-radius:6px;padding:5px; width: 190px !important">
																								<div class="notification" style="font-size:14px;margin: 1px 0px 0px 110px !important;">
																									<!--<b>$ <?php echo number_format(($d['base_price']  ),2);?></b>
																									-->
																									<span style="font-size:12px;">2&minus;3 bottles S$ <?php echo number_format(($d['a_20']  ),2);?></span>
																									<br>
																									<span style="font-size:12px;">4+ bottles S$ <?php echo number_format(($d['a_30']  ),2);?></span>
																								</div>
																								<div class="thumbnail" style="" align="center"><img src="<?php echo site_url();?>/custom/uploads/products/thumb/<?php echo $product->prod_image;?>" alt="<?php echo $product->prod_name;?>" style="width:102px;height:102px" /></div>
																								<div class="name1" style="padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></div>
																								<div class="name2" style="padding:0px 4px; border:solid 0px; width:183px !important; height: 60px; border:solid 0px #333;"><?php echo $product->prod_name;?></div>
																								
																								<div align="center">
																									<span style="color:#000">Qty: <select name="quantity" autocomplete="off" id="quantity_<?php echo $product->product_id;?>" class="" style="width:55px;">
																										<option value="1" selected="">1
																										</option>
																										<option value="2">2
																										</option>
																										<option value="3">3
																										</option>
																										<option value="4">4
																										</option>
																										<option value="5">5
																										</option>
																										<option value="6">6
																										</option>
																										</option>
																									</select>
																									<button id="cart_<?php echo $product->product_id;?>" data-public_alias="<?php echo $brand->public_alias;?>" data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" data-id="<?php echo $product->product_id;?>" class="btn-custom-green glompItemMerchant" style="font-size:12px !important;margin: 0 50px; ">Add to Cart</button>
																								</div>
																								<div class="popUp" style="margin: 5px 0px 0px; display: none;">
																									<div class="merchant-name name1"><?php echo $product->merchant_name;?></div>
																									<div class="product-name name2"><?php echo $product->prod_name;?></div>
																									<div class="product-desc" style="  line-height:13px !important;margin-top:5px !important"><?php echo $product->prod_details;?></div>
																								</div>
																							</li>
																					<?php } endforeach; ?>
																					</ul>
																					<br>
																					<br>
																				<?php
																			endforeach;
																		}
																		else
																		{
																		?>
																			<ul class="thumbnails" >
																			<?php foreach($merchant_products as $product):
																			?>
																			<script type="text/javascript">
																				items[<?php echo $product->product_id;?>] =<?php echo json_encode($product);?>;
																			</script>
																				<?php
																					$src = "http://placehold.it/190x102";

																					if($product->prod_image !="" )//&& file_exists(site_url('custom/uploads/products/thumb/'.$product->prod_image)))
																						$src = site_url('custom/uploads/products/thumb/'.$product->prod_image);

																					$prod_cost=$product->prod_merchant_cost;
																					$has_option = "no";
																					$prod_data="";
																					if($brand_product_id !=MACALLAN_BRAND_PRODUCT_ID)
																					{
																						$prod_data = get_hsbc_prod_data($product->product_id);
																						$prod_cost = $prod_data["sale_price"];
																						$has_option= $prod_data["has_options"];
																					}
																				?>
																				<script type="text/javascript">
																					items_options[<?php echo $product->product_id;?>] =<?php echo json_encode($prod_data);?>;
																				</script>
																				<li class="roductImages displayPopUp " data-image="<?php echo $src;?>" data-point="<?php echo $product->prod_point;?>" data-productname="<?php echo $product->prod_name;?>" data-id="<?php echo $product->product_id;?>" data-prod_merchant_cost="<?php echo $prod_cost;?>" data-merchantname="<?php echo $product->merchant_name;?>" style="min-height:180px;cursor:pointer;margin:10px 30px 10px 0px;border:solid 1px #ddd;border-radius:6px;padding:5px; width: 190px !important">
																								<div class="notification" style="font-size:14px; margin: 1px 0px 0px 150px !important;">
																							<b>S$ <?php echo number_format(($prod_cost ),2);?></b>
																					</div>
																					<?php
																						$sold_out = is_sold_out($product->product_id);
																					?>

																					<div class="thumbnail" style="width:100%;height:102px">
																						<img src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>" style="height: 100% !important"/>
																						<?php
																						if($sold_out)
																						{
																							?>
																							<img src="<?php echo site_url();?>assets/images/sold-out.PNG" alt="SOLD OUT" style=" height: 100%;margin-top:-102px;background-color:rgba(255,255,255,0.4)" />
																							<?php
																						}
																						?>
																					</div>
																					<div class="name1" style="padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></div>
																					<div class="name2" style="padding:0px 4px; border:solid 0px;overflow: hidden;width: 183px !important; height: 60px;border:solid 0px #333;"><?php echo $product->prod_name;?></div>
																					
																					<div align="center">
																						<span style="color:#000">Qty: <select name="quantity" autocomplete="off" id="quantity_<?php echo $product->product_id;?>" class="" style="width:55px;"
																						<?php echo ($sold_out) ? 'disabled':'';?>
																							>
																							<option value="1" selected="">1
																							</option>
																							<option value="2">2
																							</option>
																							<option value="3">3
																							</option>
																							<option value="4">4
																							</option>
																							<option value="5">5
																							</option>
																							<option value="6">6
																							</option>
																							<option value="7">7
																							</option>
																							<option value="8">8
																							</option>
																							<option value="9">9
																							</option>
																							<option value="10">10
																							</option>
																						</select>
																						<button id="cart_<?php 
																								echo $product->product_id;?>" 
																								data-public_alias="<?php echo $brand->public_alias;?>" 
																								data-prod_merchant_cost="<?php echo $prod_cost;?>" 
																								data-has_option="<?php echo $has_option;?>" 
																								data-id="<?php echo $product->product_id;?>" 																								
																								class="btn-custom-green glompItemMerchant" 
																								<?php echo ($sold_out) ? 'disabled':'';?>
																								style="font-size:12px !important;font-size:12px !important; margin: 0 50px;<?php echo ($sold_out) ? 'background-color:#ddd':'';?>"
																								>Add to Cart</button>
																					</div>
																					<div class="popUp" style="margin: 5px 0px 0px; display: none;">
																						<div class="merchant-name name1"><?php echo $product->merchant_name;?></div>
																						<div class="product-name name2"><?php echo $product->prod_name;?></div>
																						<div class="product-desc" style="  line-height:13px !important;margin-top:5px !important"><?php echo $product->prod_details;?></div>
																					</div>
																				</li>
																			<?php endforeach;
																			?>
																			</ul>
																			<?php
																		}
																		?>
																	
																</div>
															</div>
														</div>
													</div>
												</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>