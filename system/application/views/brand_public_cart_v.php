<script>
    var selectedTab;
    var merID;
	var winery_count = <?php echo $winery_count;?>
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<script src="<?php echo minify('assets/frontend/js/jquery.mask.min.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
			
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		
        <script type="text/javascript">
			$(document).ready(function(e){
			
				<?php if ($winery_count==1) echo "alert_winery();";?>
			
    $(".displayPopUp").on('mouseenter',function(){
		$(this).find('.popUp').show();
		}).on('mouseleave',function(){$('.popUp').hide();})
		
		
		
});
	function alert_winery()
	{
		var NewDialog = $('<div id="" align="center" style="padding-top:15px !important; font-size:14px">\
				<p align="justify">Note: You need atleast two(2) bottles of winery products before proceeding to checkout.</p>\
								</div>');
				NewDialog.dialog({                    
					dialogClass:'noTitleStuff',
					title: "",
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 500,
					height:120,
					show: '',
					hide: '',
					buttons: [
							{text: "OK",
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
								
							}}
					]
				});
				NewDialog.dialog('open');
	}
		
        </script>
		<?php include('includes/glomp_pop_up_my_brand_js.php'); ?>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ;
			$grand_total=0;
		?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
									
                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:10px;">
                                                
												<div id="tabBrands" class="tabs" style="background:#343239">
														<?php if($public_alias=='amex')
														{
														?>
															<a href="<?php echo site_url();?>brands/view/amex/<?php echo MACALLAN_BRAND_PRODUCT_ID;?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< The Macallan
																</div>
															</a>
															<a href="<?php echo site_url();?>brands/view/amex/<?php echo WINESTORE_BRAND_PRODUCT_ID;?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Winestore
																</div>
															</a>
														<?php
														}
														?>
															
														
														<a href="<?php echo site_url('brands/cart/'.$public_alias);?>" >
															<div style="float:right;margin:10px 15px 0px 0px;" class="btn-custom-gray-light" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty_'.$public_alias)=='' ? 'class="gray">0': 'class="red">'.$this->session->userdata('session_cart_qty_'.$public_alias);?></span>)</div>
														</a>

														<div style="clear:both"></div>


														<h3 style="color:#fff;padding:10px 0px 0px 15px;margin:0px;">Cart Items</h3>
													<div id="brand-cart-layer" class="row-fluid" style="display: block;padding-bottom:10px;margin-top:-15px;">
														<div class="tabContent" style="background:#343239">
															<div id="" class="span12 tabInnerContent" comment="" style="display: block;">
																<?php 
																if($cart_data!="")
																{
																
																	$grand_total = 0;
				
																	foreach($cart_data as $row):
																	$product = json_decode($row->prod_details);
																	//print_r($product);
																		$grand_total += (($row->prod_cost ) * $row->prod_qty);
																?>
																<div class="body_bottom_1px" id="item_wrapper_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																
																	<div class="row-fluid" >
																		<div class="span2" style="margin:0px;border:solid 1px #333">
																			<div class="thumbnail">
																			<?php
																			$src = "http://placehold.it/190x102";
																			if($product->prod_image !="")
																				$src = site_url('custom/uploads/products/'.$product->prod_image);
																			?>

																				<img src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>">
																			</div>
																		</div>
																		<div class="span4">
																			<b class="name1" style="font-size:15px;padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></b>
																			<div class="name2" style="font-size:14px;padding:0px 4px; border:solid 0px"><?php echo $product->prod_name;?></div>
																		</div>
																		<div class="span3" align="right">
																			<div class="notification">
																				S$ <span id="prod_cost_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>"><?php echo number_format(($row->prod_cost),2);?></span>
																			</div>
																		</div>
																		<div class="span1" >
																			<select name="quantity" autocomplete="off" 
																				id="quantity_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>" 
																				data-prod_group="<?php echo $row->prod_group;?>" 
																				data-public_alias="<?php echo $public_alias;?>" 
																				data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" 
																				data-id="<?php echo $product->product_id;?>" 
																				data-brand_id="<?php echo $product->brandproduct_id;?>" 
																				class="cart_qty" style="width:55px;">
																				<?php 
																					for($x=0;$x<=10;$x++)
																					{
																						?>
																						<option value="<?php echo $x;?>" 
																						<?php
																							if($x == $row->prod_qty)
																							{
																								?>
																								selected="selected"
																								<?php
																							}
																						?>
																						>
																						<?php echo $x;?>
																						</option>
																						<?php
																					}
																				?>
																			</select>
																		</div>
																		<div class="span2" align="right" style="font-weight:bold;font-size:15px;" id="total_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																			S$ <?php echo number_format(( ($row->prod_cost) ) * ($row->prod_qty),2);?>
																		</div>
																	</div>
																</div>
																<?php endforeach;
																}
																else
																{
																	echo "No items on your cart yet.";
																}
																?>
																
																<hr style="padding:0px; margin:0px;">
																<?php
																if(isset( $grand_total) && $grand_total > 0)
																{
																?>
																<div class="row-fluid" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Total: S$ <span id="grand_total"><?php echo number_format($grand_total,2);?></span>
																	</div>
																</div>
																<div class="row-fluid" >
																	<div class="span8">
																	</div>
																	<div class="span4 hide" align="right" style="font-size:16px;font-weight:bold">
																		USD$ <span id="grand_total_usd"><?php echo number_format(($grand_total*BRAND_FOREX_AMEX),2);?></span>
																	</div>
																</div>
																<!--
																<div class="row-fluid" >
																	<div class="span7">
																	</div>
																	<div class="span5" align="right" style="font-size:12px;">
																		<i>Note: Your card will be billed in USD.</i>
																	</div>
																</div>
																-->
																<div class="row-fluid" >
																	<div class="span9">
                                                                       <label for="chk_confirm"><input type ="checkbox"  style="margin-top:-1px;" id="chk_confirm" />&nbsp;I understand that card purchases processed are not refundable nor returnable.</label>
                                                                    </div>
																	<div class="span3" align="right" style="font-size:16px;font-weight:bold">
																		<button id="checkout" class="btn-custom-gray-light white" style="font-size:20px !important">Checkout</button>
																	</div>
																</div>
																<?php
																}
																?>
																	
															
															</div>
														</div>
														
														
													</div>
												</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>