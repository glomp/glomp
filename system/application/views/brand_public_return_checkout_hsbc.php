<script>
    var selectedTab;
    var merID;
	var winery_count = <?php echo $winery_count;?>
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<script src="<?php echo minify('assets/frontend/js/jquery.mask.min.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<?php include('includes/glomp_pop_up_my_brand_js.php'); ?>
        <script type="text/javascript">
			$(document).ready(function(e) {
				<?php
					if (! empty($data['group_voucher_id'])) {
						if($data['group_voucher_data']->transaction_type =='glomp_to_fb')
						{
							$temp = explode("@",$data['group_voucher_data']->email); ?>
							fb_id ="<?php echo $temp[0];?>";
							do_send_fb_confirm();	
						<?php } 
					} 
				?>
			});
			function do_send_fb_confirm()
			{
				var NewDialog = $('<div id="" align="center" style="padding-top:25px !important; font-size:14px">\
					<p align="justify">If your friend is new to glomp!, we recommend that you send a personal message to inform them that you have glomp!ed (treated) them in order to ensure that they accept the treat.</p>\
				</div>');
				NewDialog.dialog({                    
					dialogClass:'noTitleStuff',
					title: "",
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 500,
					show: '',
					hide: '',
					buttons: [
							{text: "OK",
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
								FB.ui({
								  to: fb_id,
								  method: 'send',
								  link: "<?php echo site_url('m/brands/acceptance/'.$public_alias.'/'.$data['group_voucher_id']);?>"
								  
								},
								  function(response) {
									var msg ='Message successfully sent.';
									if (response && !response.error_code)
									{
									  
										
									} else {
									  msg = 'Message not sent. Refresh the page to retry.'
									}
									
									var NewDialog = $('<div id="" align="center" style="padding-top:20px !important; font-size:14px">\
										<p align="center">'+msg+'</p>\
														</div>');
										NewDialog.dialog({                    
											dialogClass:'noTitleStuff',
											title: "",
											autoOpen: false,
											resizable: false,
											modal: true,
											width: 320,
											height:100,
											show: '',
											hide: '',
											buttons: [
													{text: "OK",
													"class": 'btn-custom-darkblue width_80_per',
													click: function() {
														$(this).dialog("close");
													}}
											]
										});
										NewDialog.dialog('open');
								  }
								);
							}}
					]
				});
				NewDialog.dialog('open');
				
			}
        </script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php"); ?>
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
									
                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:10px;">
                                                
												<div id="tabBrands" class="tabs" style="background:#343239">
														<?php if ($data['success_payment']) { ?>
														<h3 style="color:#fff;padding:10px 0px 0px 15px;margin:0px;">Congratulations!</h3>
														<?php } ?>
														<div style="clear:both"></div>
														<h3 style="color:#fff;padding:10px 0px 0px 15px;margin:0px;"></h3>
													<div id="brand-cart-layer" class="row-fluid" style="display: block;padding-bottom:10px;margin-top:-15px;">
														<div class="tabContent" style="background:#343239">
															<?php if ( ! $data['success_payment']) { ?> 
															<div id="" class="span12 tabInnerContent" comment="" style="display: block;">
																<div id = "wait_message" class="alert alert-success"><?php echo $this->lang->line('payment_wait', 'Please wait while we verify your payment.'); ?><img src="<?php echo base_url('assets/images/ajax-loader-1.gif'); ?>" /></div>
															</div>
															<script>
																/* Check payment record if completed */
																setInterval(function() {
										                            $.ajax({
										                                type: "POST",
										                                dataType: 'json',
										                                url: GLOMP_BASE_URL + 'brands/get_checkout/<?php echo $public_alias; ?>',
										                                data: {
									                                		token: '<?php echo $data['token']; ?>'
										                                },
										                                success: function(r){ 
										                                    if (r.success) {
										                                    	return window.location = GLOMP_BASE_URL + 'brands/get_checkout/<?php echo $public_alias; ?>/' + r.group_voucher_id
										                                    }
										                                }
										                            });
										                        }, 3000);
															</script>
															<?php } ?>

															<?php if ( $data['success_payment']) { ?>
															<div id="" class="span12 tabInnerContent" comment="" style="display: block;">
																<h4 style="padding:0px ;margin:0px;">Order Summary for <?php echo $data['group_voucher_id']; ?></h4>
																<br />
															<?php 
																if($cart_data!="")
																{
																	$grand_total = 0;													
																	$delivery_total = 0;
																	$color="#ddd";
																	foreach($cart_data as $brand_row)
																	{
																		if($color=="#eee") 
																		{
																			$color="#ddd";
																		}
																		else 
																		{
																			$color="#eee";
																		}
																		?>
																		<div style="background-color:<?php echo $color?>;padding:10px 10px;">
																			<?php
																			$per_brand_total = 0;
																			foreach($brand_row['prod_list'] as $row)
																			{
																				$options = $row['option'];
																				$row = $row['product'];																				
																				$product = json_decode($row->prod_details);
																				$grand_total += (($row->prod_cost ) * $row->prod_qty);
																				$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
																				?>
																					<div class="body_bottom_1px" id="item_wrapper_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																						<div class="row-fluid" >
																							<div class="span2" style="margin:0px;border:solid 1px #333">
																								<div class="thumbnail">
																								<?php
																								$src = "http://placehold.it/190x102";
																								if($product->prod_image !="") {
																									$src = site_url('custom/uploads/products/'.$product->prod_image);
																								}
																								?>

																									<img src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>">
																								</div>
																							</div>
																							<div class="span4">
																								<b class="name1" style="font-size:15px;padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></b>
																								<div class="name2" style="font-size:14px;padding:0px 4px; border:solid 0px"><?php echo $product->prod_name;?></div>
																								<?php
																								if(count($options) > 0)
																								{
																									?>
																									<div class="name2" style="font-size:12px;padding:0px 4px; border:solid 0px; padding-left:10px;">
																										<b>Flavor(s):</b>
																										<div style="padding-left: 20px;">	
																											<?php																									
																											foreach ($options as $p)
																											{
																												?>																										
																												<div>
																													<?php echo $p->option_qty;?> - <?php echo $p->option_value;?>
																												</div>
																												<?php
																											}																										
																											?>																																																				
																										</div>
																									</div>
																									<?php
																								}
																								?>																								
																							</div>
																							<div class="span3" align="right">
																								<div class="notification">
																									S$ <span id="prod_cost_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>"><?php echo number_format(($row->prod_cost),2);?></span>
																								</div>
																							</div>
																							<div class="span1" >
																								QTY : <?php echo $row->prod_qty; ?>
																							</div>
																							<div class="span2" align="right" style="font-weight:bold;font-size:14px;" id="total_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																								S$ <?php echo number_format(( ($row->prod_cost) ) * ($row->prod_qty),2);?>
																							</div>
																						</div>
																					</div>
																				<?php
																			}


																			$has_delivery = false;
																			$brand_eror='';
																			switch ($brand_row['brand_id']) {
																				case HSBC_BRAND_PROD_ID_MACALLAN:
																				case HSBC_BRAND_PROD_ID_SNOW_LEOPARD:
																				case HSBC_BRAND_PROD_ID_LONDON_DRY:
																						if($per_brand_total<300)
																						{
																							$brand_eror = "";
																						}
																					break;
																				case HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP:
																					$has_delivery = 16;
																					break;
																				case HSBC_BRAND_PROD_ID_SINGBEV:
																					$has_delivery = 35;
																					break;
																				case HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN:
																					$has_delivery = 25;
																					break;
																				case DBS_BRAND_PROD_ID_EURACO:
																					if ($per_brand_total < 300) {
																						$has_delivery = 30;
																					}
																					break;
																			}
																			$brand_display_error=($brand_eror=='') ? 'none' :'block';
																			?>																			
																			<div 	data-brand_id="<?php echo $brand_row['brand_id'];?>" 
																					data-brand_name="<?php echo $brand_row['brand_name'];?>" 
																					class="brand_error alert alert-error" 
																					id="error_<?php echo $brand_row['brand_id'];?>"
																					style="display:<?php echo $brand_display_error;?>;";
																					align="center"
																			><?php echo $brand_eror;?></div>
																			<?php
																				$has_delivery_display = ($has_delivery===false) ? 'none':'';
																				$delivery_total += $has_delivery;

																				$no_forpickup='';
																				$delivery_only='';
																				if($brand_row['brand_id'] == HSBC_BRAND_PROD_ID_SINGBEV || $brand_row['brand_id'] == HSBC_BRAND_PROD_ID_DAILY_JUICE || $brand_row['brand_id'] == DBS_BRAND_PROD_ID_EURACO )
																				{
																					$no_forpickup='none';
																					$delivery_only=' Only';
																				}
																				?>
																				<!-- delivery charge and subtotal -->
																				<div class="row-fluid"  >
																					<div 	class="span4 delivery_charge" 
																							data-delivery_charge="<?php echo $has_delivery;?>"
																							data-delivery_type="delivery"
																							data-id="<?php echo $brand_row['brand_id'];?>" 
																						 	align="right" style="font-size:14px;font-weight:bold;display:<?php echo $has_delivery_display;?>" id="delivery_charge_<?php echo $brand_row['brand_id'];?>">
																						Delivery Charge: S$ <span id=""><?php echo number_format($has_delivery,2);?></span>
																					</div>
																				</div>
																				<!-- delivery charge and subtotal -->
																		</div>
																		<?php
																	}
																}
																else
																{
																	echo "No items on your cart yet.";
																}
																?>
																
																<hr style="padding:0px; margin:0px;">
																<?php
																if(isset( $grand_total) && $grand_total > 0)
																{
																?>
																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Sub Total: S$ <span id="sub_total" ><?php echo number_format(($grand_total),2);?></span>
																	</div>
																</div>

																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		GST: S$ <span id="gst" ><?php echo number_format($grand_total * 0.07, 2);?></span>
																	</div>
																</div>

																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Total: S$ <span id="grand_total" ><?php echo number_format(($grand_total * 0.07) + $grand_total + $delivery_total,2);?></span>
																	</div>
																</div>

																<div class="row-fluid" >
																	<div class="span8">
																	</div>
																	<div class="span4 hide" align="right" style="font-size:16px;font-weight:bold">
																		USD$ <span id="grand_total_usd"><?php echo number_format(( ($grand_total + $delivery_total) * BRAND_FOREX_AMEX),2);?></span>
																	</div>
																</div>


																<div class="row-fluid" >
																	<div class="span9">
																	</div>
																	<div class="span3" align="right" style="font-size:16px;font-weight:bold">
																		<button id="order_confirm_ok" class="btn-custom-green" style="font-size:20px !important">OK</button>
																	</div>
																</div>

																<script>
                                                                    $('#order_confirm_ok').click(function(e){
                                                                       e.preventDefault();
                                                                       return window.location = '<?php echo site_url('brands/view/'.$public_alias);?>';
                                                                    });
                                                                </script>


																<?php } ?>
															<?php } ?>
														</div>
													</div>
												</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>