<script type="text/javascript">
    $(document).ready(function(e) {
        $(".displyPopUp").on('mouseenter', function() {
            $(this).find('.popUp').show();
        }).on('mouseleave', function() {
            $('.popUp').hide();
        })
    });
</script>

<?php # dirnks tab?>
<div class="merchantCatProduct">
    <div class="row-fluid"> 
        <div class="span12"> 
            <?php
            $users_merchant_id_list = $this->merchant_m->regionwise_merchant_list($profile_region_id);
            $user_specfic_product = $this->product_m->users_cat_product($users_merchant_id_list, $cat_id);
            $num_product = $user_specfic_product->num_rows();
            if ($num_product > 0) {
                $i = 1;
                $j = 1;
                foreach ($user_specfic_product->result() as $row_product) {
				$merchantName=$this->merchant_m->merchantName($row_product->prod_merchant_id);
                    if ($i == 1) {
            ?> <ul class="thumbnails"> <?php } ?>
                        <li class="span2 displyPopUp glompItem" style="cursor:pointer;" data-image="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" data-point="<?php echo $row_product->prod_point; ?>" data-productname="<?php echo stripslashes($row_product->prod_name); ?>" data-id="<?php echo $row_product->prod_id; ?>" data-merchantname="<?php echo stripslashes($this->merchant_m->merchantName($row_product->prod_merchant_id)); ?>" >
                            <div class="notification1" style="margin-left: 66px;"><?php echo $row_product->prod_point; ?> pts</div>
                            <div class="popUp" style="margin-top:124px;">
                                <div class="row-fluid">
                                    <div class="span5">
                                        <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" alt="<?php echo stripslashes($row_product->prod_name); ?>">
                                    </div>
                                    <div class="span7">
                                       <div class="name1"><?php echo $merchantName;?></div>
										<div class="name2"><?php echo stripslashes($row_product->prod_name);?></div>
                                        <p><?php echo stripslashes($row_product->prod_details); ?></p>
                                    </div>
                                </div>

                            </div>
                            <div class="thumbnail">
                                <img src="<?php echo $this->custom_func->product_logo($row_product->prod_image); ?>" alt="<?php echo stripslashes($row_product->prod_name); ?>">
                            </div>                            
							<div class="name1"><?php echo $merchantName;?></div>
							<div class="name2"><?php echo stripslashes($row_product->prod_name);?></div>

                        </li>
                <?php if ($i == 6 || $j == $num_product) {
 ?></ul>
            <?php
                    }
                    $j++;
                    if ($i == 6) {
                        $i = 1;
                    }
                    else
                        $i++;
                } /// end of foreach
            }//enf of num result check
            else {
            ?>
                <p class="alert alert-info"><?php echo $this->lang->line('sorry_no_product_available', 'No available products here as yet. Please check out another product category.'); ?></p>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<?php #drinks ta close  ?>