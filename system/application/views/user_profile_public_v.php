<script>
    var selectedTab;
    var merID;
<?php
if (isset($_GET['tab'])) {
    echo 'selectedTab="' . $_GET['tab'] . '";';
}
if (isset($_GET['merID']) && is_numeric($_GET['merID'])) {
    echo 'merID="' . $_GET['merID'] . '";';
}
?>
</script>
<?php
$profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic, $user_record->user_gender);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $user_record->user_fname; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="description" content="<?php echo $user_record->user_fname.' '.$user_record->user_lname." is on glomp!."; ?>" >
        <meta name="keywords" content="<?php echo meta_keywords('profile');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/nanoscroller.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->

        <link rel="stylesheet" type="text/css" media="screen" href="assets/frontend/css/als_demo.css" />        
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery.als-1.2.min.js"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script type="text/javascript">
        var GLOMP_BASE_URL  ="<?php echo base_url('');?>";
        var FB_APP_ID       ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
        var loadingMoreStories=false;
	
            $(document).ready(function(e) {
                

                $(".buzzScroll").nanoScroller();
                $("#tabBuz_wrapper").bind("scrollend", function(e){                    
                    if(!loadingMoreStories)
                    {
                    
                        loadingMoreStories=true;            
                        start_count=$('#glomp_buzz_content').children().last().data().id;
                        if(start_count>0)
                        {                        
                            $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">Loading more stories...<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                            $('#glomp_buzz_content').append($loader);
                            data ='start_count='+start_count+'&profile_id='+<?php echo $buzz_user_id;?>;
                            setTimeout(function() {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'html',
                                    url: 'profile/getMoreStories',
                                    data: data,
                                    success: function(response){ 
                                        if(response!='' &&  response!="<!---->")
                                        {
                                             $('#id_loader').remove();
                                            $('#glomp_buzz_content').append(response);
                                            $(".buzzScroll").nanoScroller();            
                                            loadingMoreStories=false;
                                            
                                        }
                                        else
                                        {                                
                                            $('#id_loader').remove();
                                            $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">No more stories to load.</div>');
                                            $('#glomp_buzz_content').append($no_more);
                                        }
                                    }
                                });
                            }, 1);
                        }
                        
                        
                        /*console.log($('#glomp_buzz_content').children().last().data().id);*/
                        /*xx=1;*/
                    }                       
                });
                $(".displyFevPopUp").on('mouseenter', function() {
                    var read_popup = $(this).find('.getPopupDescription').html();
                    var position = $(this).position();
                    /*console.log(position.left);*/
                    if (position.left > 280)
                        position = 300;
                    else
                        position = position.left;
                    $('#lista1').find('.popUp').show().html(read_popup);
                    $('#lista1').find('.popUp').css({'margin-left': position + 'px'});
                }).on('mouseleave', function() {
                    $('.popUp').hide();
                    /*$('.remove').hide();*/
                });

                $("#ask_remove_frn").on('click', function(e) {
                    e.preventDefault();
                    $("#removeFrenPopup").show();
                    e.stopPropagation();
                    return false;
                });

                $('._hide').click(function() {
                    $('#removeFrenPopup').hide(100);
                });
            });
        </script>
    <script src="<?php echo minify('assets/frontend/js/custom.glomp_share.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            var notbackState=true;
            $(document).ready(function() {

                $("#lista1").als({
                    visible_items: 6,
                    scrolling_items: 1,
                    orientation: "horizontal",
                    circular: "yes",
                    autoscroll: "no",
                    interval: 5000,
                    direction: "right",
                    start_from: 0
                });


                $(".tabs").hide();
                
                $("#glomped_inactive").hide();

                $(".tabs:first").show();
                $(".profileTab a").click(function(e) {
                    var activeTab = $(this).data().href;
                    var activeTab_id = "#"+$(this).data().id;
                    
                    if (activeTab == "#tabBuz")
                    {
                        $("#glomped_active").show();
                        $("#glomped_inactive").hide();
                    }
                    else
                    {
                        $("#glomped_inactive").show();
                        $("#glomped_active").hide();
                    }

                    e.preventDefault();
                    $(".profileTab a").removeClass("activeTab");
                    $(this).addClass("activeTab");
                    $(".tabs").hide();
                    $(activeTab).fadeIn();
                    
                    if(notbackState)
                    {
                        history.pushState({id:1, module:"leave"}, document.title,document.location.href.match(/(^[^#]*)/)[0]+activeTab_id);
                    }
                    else
                    {
                        notbackState=true;
                    }
                        
                    
                });

                $('.nameOfMerchant').on('click', function(e) {
                    e.preventDefault();
                    var _anchor = $(this).attr('href');
                    $('#merchantProductDetails').load(_anchor, function() {
                        $('#tabMerchant').hide();
                        $('#merchantProductDetails').fadeIn();
                    });

                });

                


            });
            
            window.addEventListener("popstate", function(e) {
                var state = e.state;
                //console.log(location.hash);
                if (state && state.module == "leave") {
                    notbackState=false;                    
                    selectedTab =location.hash;/*(location.href).split('#')[1];*/
                    $(selectedTab).click();                     
                    
                }   
                else
                {
                    /*location.reload();*/
                }
            });
             
           

            $("document").ready(function()
            {


                if (selectedTab != '') {
                    $("#" + selectedTab).trigger('click');
                }
                if (merID != '') {
                    $("#merID_" + merID).trigger('click');
                }
            });
        </script>
        
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
                                    <div class="row-fluid">
                                        <div class="userProfileLeft">
                                            <div class="profilePicture">
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div style=" height:150px; overflow:hidden;">
                                                            <img src="<?php echo $profile_pic; ?>"  alt="<?php echo $user_record->user_name; ?>"  style="height:150px;width:150px">
                                                        </div>
                                                    </div>
                                                    <div class="span6" style="padding-left:5px;">
                                                        <div style="height:88px;">
                                                            <div class="name"><h1 style="margin:0px;padding:0px;line-height: 14px;font-size:14px;"><?php echo $user_record->user_name; ?></h1></div>
                                                            <?php
                                                            $dob = explode('-', $user_record->user_dob);
                                                            if (count($dob) !== 3) {
                                                                $dob[0] = 0;
                                                                $dob[1] = 0;
                                                                $dob[2] = 0;
                                                            }
                                                            $dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
                                                            if ($user_record->user_dob_display == 'dob_display_wo_year') {
                                                                $dob = date('M d', strtotime($dob));
                                                            } else {
                                                                $dob = date('M d, Y', strtotime($dob));
                                                            }
                                                            
                                                            if($user_record->user_dob!="")
                                                            {
                                                            ?>
                                                                <div class="dob"><?php echo $this->lang->line('DOB'); ?>: <?php echo $dob; ?>
                                                                </div>
                                                            <?php }?>                                                            
                                                            <div class="address"><?php
                                                            echo $this->regions_m->region_name($user_record->user_city_id);
                                                            ?></div>
                                                        </div>   
                                                        <?php
                                                            $profile_id = $user_record->user_id;                                                        
                                                          ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="userProfileRight">
                                            <?php
                                            $resFev = $this->product_m->userFevProduct($profile_id);
                                            $febCount = $resFev->num_rows();
                                            ?>
                                            <div class="fev">
                                                <div class="title"><?php if (($user_record->user_status != 'Pending'))
                                                echo $user_record->user_fname . ' ' . $user_record->user_lname . '\'s ' . $this->lang->line('favourite') . 's';
                                            ?></div>
<?php if ($febCount > 0 && ($user_record->user_status != 'Pending')) { ?>
                                                    <div>


                                                        <div id="lista1" class="als-container">

                                                            <div class="popUp"><!--do no delete this div--></div>

                                                                    <?php if ($febCount > 6) { ?><span class="als-prev"><img src="assets/frontend/img/thin_left_arrow_333.png" alt="" title="" /></span><?php } ?>
                                                            <div class="als-viewport">
                                                                <ul class="als-wrapper">
                                                                    <?php
                                                                    foreach ($resFev->result() as $recFev) {
                                                                        $merchantName = $this->merchant_m->merchantName($recFev->prod_merchant_id);
                                                                        ?>
                                                                        <li class="als-item displyFevPopUp glompItemFromFev" style="cursor:pointer;" data-image="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" data-point="<?php echo $recFev->prod_point; ?>" data-productname="<?php echo stripslashes($recFev->prod_name); ?>" data-id="<?php echo $recFev->prod_id; ?>"  data-merchantname="<?php echo stripslashes($this->merchant_m->merchantName($recFev->prod_merchant_id)); ?>" >
                                                                            <div class="getPopupDescription">
                                                                                <div class="row-fluid">
                                                                                    <div class="span5">
                                                                                        <img src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>">
                                                                                    </div>
                                                                                    <div class="span7">
                                                                                        <div class="name1"><?php echo $merchantName; ?></div>
                                                                                        <div class="name2"><?php echo stripslashes($recFev->prod_name); ?></div>
                                                                                        <p><?php echo stripslashes($recFev->prod_details); ?></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <img src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>"  />                         <p class="productName"><b><?php echo $merchantName . "<br>" . stripslashes($recFev->prod_name); ?></b></p>
                                                                            </p>
                                                                        </li>
                                                            <?php } ?>

                                                                </ul>
                                                            </div>
                                                    <?php if ($febCount > 6) { ?><span class="als-next"><img src="assets/frontend/img/thin_right_arrow_333.png" alt="" title="" /></span> <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    if (($user_record->user_status != 'Pending')) {
                                                        ?>
                                                        <div class="alert">
                                                            <?php /* ?> <button type="button" class="close" data-dismiss="alert">&times;</button><?php */ ?>
                                                            <strong><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?> <?php echo $this->lang->line('has_not_added_favourite'); ?></strong>
                                                        </div>
                                                        <?php
                                                    } else if (($user_record->user_status == 'Pending')) {
                                                        ?>
                                                        <div class="alert" style="color:#888;font-size:14px;">															
                                                            We are awaiting <b><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?></b> to verify their account. Go ahead and glomp! <b><?php echo $user_record->user_fname ?></b> more!																
                                                        </div>
                                                        <button value="glomp" type="button" style="float:right;background-color:#d2d7e1; border:0px;width: 87px; padding:0px; border:solid 0px; margin-top:48px;margin-right:0px" name="glomp_non_member_from_profile" id="glomp_non_member_from_profile"><img src="assets/frontend/img/glompSubmitButton.png" style="border:0px; width:87px" width="87" /></button>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:-3px;">
                                            <?php if (($user_record->user_status == 'Pending')) {
                                                ?>											

                                                <div class="row-fluid">
                                                    <div class="span12" style="border:solid 0px;background-color:#DEE6EB; border-radius: 5px 5px 0px 5px;height:400px">                                                        
                                                        <?php include('includes/glomp_non_member_from_profile.php'); ?>
                                                    </div>												
                                                </div>											
                                                <?php
                                            } else if (($user_record->user_status != 'Pending')) {
                                                ?>
                                                <div class="profileTab">
                                                    <ul>
                                                        <li ><a href="javascript:void(0);" data-href="#tabBuz" id="tbBuz" data-id="tbBuz" class="activeTab" style="width:121px;">
                                                                <span id="glomped_inactive"><img src="assets/frontend/img/glomp_buzz.png" alt="Glomped"> </span>
                                                                <span id="glomped_active"> <img src="assets/frontend/img/buzz_active.png" alt="Glomped"> </span> </a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbMerchant"  data-id="tbMerchant" data-href="#tabMerchant"><?php echo ucfirst($this->lang->line('merchants')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbDrink"     data-id="tbDrink" data-href="#tabDrink"><?php echo ucfirst($this->lang->line('drinks')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbSnacks"    data-id="tbSnacks" data-href="#tabSnaks"><?php echo ucfirst($this->lang->line('snacks')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbCocktails" data-id="tbCocktails" data-href="#tabCocktails"><?php echo ucfirst($this->lang->line('cocktails')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbSweets"    data-id="tbSweets" data-href="#tabSweets"><?php echo ucfirst($this->lang->line('sweets')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" id="tbOthers"    data-id="tbOthers" data-href="#tabOthers"><?php echo ucfirst($this->lang->line('others')); ?></a></li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="tabContent tabs" id="tabBuz">
                                                    <div class="row-fluid">
                                                        <div class="span8">
                                                            <div class="buzPan tabInnerContent">

                                                                <div class="buzzScroll nano" id="tabBuz_wrapper">
                                                                    <div class="content" id="glomp_buzz_content">
                                                                        <?php
                                                                        if ($glom_buzz_count > 0) {
                                                                            $base_url = base_url();
                                                                            $to_a = $this->lang->line('to_a');
                                                                            foreach ($glomp_buzz->result() as $glomp) {
                                                                                $from_id = $glomp->voucher_purchaser_user_id;
                                                                                $to_id = $glomp->voucher_belongs_usser_id;
                                                                                $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                                                                                $ago_date = $glomp->voucher_purchased_date;

                                                                                $form_name = $frn_info->friends->$from_id->user_name;
                                                                                $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

                                                                                $to_name = $frn_info->friends->$to_id->user_name;
                                                                                $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
                                                                                
                                                                                $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                                                                                $to_fb_id = $frn_info->friends->$to_id->user_fb_id;    
                                                                                
                                                                                
                                                                                /*product and merchant info*/
                                                                                $prod_id = $glomp->voucher_product_id;
                                                                                $prod_info = json_decode($this->product_m->productInfo($prod_id));
                                                                                $prod_image= $prod_info->product->$prod_id->prod_image;        
                                                                                $product_logo = $this->custom_func->product_logo($prod_image);
                                                                                
                                                                                $prod_name = $prod_info->product->$prod_id->prod_name;
                                                                                $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
                                                                                $merchant_name = $prod_info->product->$prod_id->merchant_name;
                                                                                
                                                                                
                                                                                 $story_type="1";
                                                                                if($from_id!=$this->session->userdata('user_id'))
                                                                                {
                                                                                    $story_type="2";
                                                                                }
                                                                                ?>
                                                                                <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">        
                                                                                    <div class="span7">
                                                                                        <div class="row-fluid">
                                                                                            <div class="span10">
                                                                                                <?php if ($from_id == 1) {
                                                                                            ?>
                                                                                            <a href="javascript:void(0);"><?php echo $form_name; ?></a>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a>
                                                                                        <?php } ?>
                                                                                        glomp!ed <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><?php echo $to_name; ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?><div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
                                                                                            </div>
                                                                                            
                                                                                        </div> 
                                                                                        
                                                                                    </div>
                                                                                    <div class="span5">
                                                                                        <?php if ($from_id == 1) { ?>
                                                                                            <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"> <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>
                                                                                        <?php } ?>
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg" alt="red arrow" style="margin:0px 5px" />
                                                                                        <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px"  alt="red arrow" />
                                                                                        <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
                                                                                    </div>
                                                                                </div> 
                                                                                <?php
                                                                                $start_count++;
                                                                            }/*foeach close*/
                                                                        }/*numb count close*/
                                                                        else {
                                                                            ?>

                                                                            <div class="alert alert-info">
                                                                                <p><?php echo $this->lang->line('coming_soon') ?></p>

                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="tabContent tabs" id="tabMerchant"> 
                                                    <div class="tabInnerContent">                                                        
                                                        <?php
                                                        $num_merchant = $region_wise_merchant->num_rows();
                                                        if ($num_merchant > 0) {
                                                            $i = 1;
                                                            $j = 1;
                                                            foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                                                if ($i == 1) {
                                                                    ?> <ul class="thumbnails"> <?php } ?>
                                                                    <li class="span2 merchantShadow nameOfMerchant">
                                                                        <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>"   href="<?php echo base_url("profile/merchantProduct/" . $profile_id . '/' . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                                                            <div class="thumbnail">
                                                                                <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>" style="width:90px !important;">

                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <?php if ($i == 6 || $j == $num_merchant) { ?></ul>
                                                                        <?php
                                                                }
                                                                $j++;
                                                                if ($i == 6) {
                                                                    $i = 1;
                                                                }
                                                                else
                                                                    $i++;
                                                            } /*/ end of foreach*/
                                                        }/*enf of num result check*/                                                        
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="tabContent tabs" id="tabDrink">
                                                    <div class="tabInnerContent">                                                        
                                                                <?php
                                                                $data_v['profile_region_id'] = $user_record->user_city_id;
                                                                $data_v['cat_id'] = 1;
                                                                $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                                ?>                                                            
                                                    </div>
                                                </div>


                                                <div class="tabContent tabs" id="tabSnaks">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 2;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>



                                                <div class="tabContent tabs" id="tabCocktails">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 3;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="tabContent tabs" id="tabSweets">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 4;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="tabContent tabs" id="tabOthers">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 5;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="tabContent tabs" id="merchantProductDetails">
                                                    <?php /* to load merchant*/ ?>
                                                </div> 

                                            <?php } /*if(($user_record->user_status != 'Pending'))*/  ?>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>