<?php

	switch ($public_alias) {
	    case 'hsbc':
	        $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
	        $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  HSBC_BRAND_PROD_ID_KPO;
	        $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona = '0';
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';

	        break;
	    case 'uob':
	        $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
	        $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  UOB_BRAND_PROD_ID_KPO;
	        $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
	        $euraco = '0';

	        break;
        case 'dbs':
	        $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
	        $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  DBS_BRAND_PROD_ID_KPO;
	        $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
	        $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
	        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
	        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
	        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
			$euraco = DBS_BRAND_PROD_ID_EURACO;

	        break;
        case 'amex':
	        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
	        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  AMEX_BRAND_PROD_ID_KPO;
	        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';

	        break;
	}


	if(!defined('BRAND_PROD_ID_MACALLAN'))     define('BRAND_PROD_ID_MACALLAN',$macallan);
	if(!defined('BRAND_PROD_ID_SNOW_LEOPARD')) define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
	if(!defined('BRAND_PROD_ID_LONDON_DRY'))   define('BRAND_PROD_ID_LONDON_DRY',$london);

	define('BRAND_PROD_ID_SINGBEV',$singbev);
	define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
	define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

	define('BRAND_PROD_ID_KPO',$kpo);
	define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
	define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
	define('BRAND_PROD_ID_VALRHONA',$valrhona);
	define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
	define('BRAND_PROD_ID_PROVIDORE',$providore);
	define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
	define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
	define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
	define('BRAND_PROD_ID_EURACO',$euraco);

?>
<script>
    var selectedTab;
    var merID;
	var winery_count = <?php echo $winery_count;?>
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<script src="<?php echo minify('assets/frontend/js/jquery.mask.min.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
			
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var accumulated_brand_list = [<?php echo BRAND_PROD_ID_MACALLAN;?>,<?php echo BRAND_PROD_ID_SNOW_LEOPARD;?>,<?php echo BRAND_PROD_ID_LONDON_DRY;?>]
			
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		
        <script type="text/javascript">
			$(document).ready(function(e){
			
				<?php if ($winery_count==1) echo "alert_winery();";?>
			
    $(".displayPopUp").on('mouseenter',function(){
		$(this).find('.popUp').show();
		}).on('mouseleave',function(){$('.popUp').hide();})
		
		
		
});
	function alert_winery()
	{
		var NewDialog = $('<div id="" align="center" style="padding-top:15px !important; font-size:14px">\
				<p align="justify">Note: You need atleast two(2) bottles of winery products before proceeding to checkout.</p>\
								</div>');
				NewDialog.dialog({                    
					dialogClass:'noTitleStuff',
					title: "",
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 500,
					height:120,
					show: '',
					hide: '',
					buttons: [
							{text: "OK",
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
								
							}}
					]
				});
				NewDialog.dialog('open');
	}
		
        </script>
		<?php include('includes/glomp_pop_up_my_brand_js.php'); ?>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ;
			$grand_total=0;
		?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
									
                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:10px;">
                                                
												<div id="tabBrands" class="tabs" style="background:#343239">
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_MACALLAN);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< The Macallan
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SNOW_LEOPARD);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Snow Leopard Vodka
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_LONDON_DRY);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< No.3 London Dry Gin
																</div>
															</a>


															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SINGBEV);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< SingBev
																</div>
															</a>

														
														<a href="<?php echo site_url('brands/cart/'.$public_alias);?>" >
															<div style="float:right;margin:10px 15px 0px 0px;" class="btn-custom-gray-light" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty_'.$public_alias)=='' ? 'class="gray">0': 'class="red">'.$this->session->userdata('session_cart_qty_'.$public_alias);?></span>)</div>
														</a>

															<div style="clear:both"></div>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SWIRLS_BAKESHOP);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Swirls Bakeshop
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_DELIGHTS_HEAVEN);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< The Delights Heaven
																</div>
															</a>


															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_DAILY_JUICE);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Daily Juice
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_KPO);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< KPO
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_NASSIM_HILL);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Nassim Hill
																</div>
															</a>
															<div style="clear:both"></div>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_VALRHONA);?>" class="hide" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Valrhona
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_LAURENT_PERRIER);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Laurent Perrier
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_PROVIDORE);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< The Providore
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_GLENFIDDICH);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Glenfiddich
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_MOET_CHANDON);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Moet & Chadon
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_KUSMI_TEA);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Kusmi Tea
																</div>
															</a>
															<a href="<?php echo site_url('brands/view/'.$public_alias.'/'.BRAND_PROD_ID_EURACO);?>" class="" >
																<div style="float:left;margin:10px 0px 0px 15px;" class="btn-custom-transparent_white" >
																	<< Euraco
																</div>
															</a>


														<div style="clear:both"></div>


														<h3 style="color:#fff;padding:10px 0px 0px 15px;margin:0px;">Cart Items</h3>
													<div id="brand-cart-layer" class="row-fluid" style="display: block;padding-bottom:10px;margin-top:-15px;">
														<div class="tabContent" style="background:#343239">
															<div id="" class="span12 tabInnerContent" comment="" style="display: block;">
																<?php 
																if($cart_data!="")
																{
																	$accumulated_300 = false;
																	$accumulated =0;

																	foreach($cart_data as $brand_row)
																	{
																		$per_brand_total = 0;
																		$per_brand_qty = 0;

																		foreach($brand_row['prod_list'] as $row)
																		{					
																			$row = $row['product'];																																		
																			$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
																			$per_brand_qty += $row->prod_qty;
																		}
																		switch ($brand_row['brand_id'])
																		{
																			case BRAND_PROD_ID_MACALLAN:																			
																			case BRAND_PROD_ID_SNOW_LEOPARD:
																			case BRAND_PROD_ID_LONDON_DRY:
																				$accumulated += $per_brand_total;
																		}
																	}

																	if($accumulated>=300)
																	{
																		$accumulated_300 = true;
																	}


																	$grand_total = 0;													
																	$delivery_total = 0;
																	$color="#ddd";
																	foreach($cart_data as $brand_row)
																	{
																		if($color=="#eee")
																			$color="#ddd";
																		else
																			$color="#eee";
																		?>
																		<div id="brand_wrapper_<?php echo $brand_row['brand_id'];?>" style="background-color:<?php echo $color?>;padding:10px 10px;">
																			<?php
																			$per_brand_total = 0;
																			$per_brand_qty = 0;
																			foreach($brand_row['prod_list'] as $row)
																			{
																				$options = $row['option'];
																				$row = $row['product'];																				
																				$product = json_decode($row->prod_details);
																				$grand_total += (($row->prod_cost ) * $row->prod_qty);
																				$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
																				$per_brand_qty += $row->prod_qty;
																				?>
																					<div class="body_bottom_1px" id="item_wrapper_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																						<div class="row-fluid" >
																							<div class="span2" style="margin:0px;border:solid 1px #333">
																								<div class="thumbnail">
																								<?php
																								$src = "http://placehold.it/190x102";
																								if($product->prod_image !="")
																									$src = site_url('custom/uploads/products/'.$product->prod_image);
																								?>

																									<img src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>">
																								</div>
																							</div>
																							<div class="span4">
																								<b class="name1" style="font-size:15px;padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></b>
																								<div class="name2" style="font-size:14px;padding:0px 4px; border:solid 0px"><?php echo $product->prod_name;?></div>
																								<?php
																								if(count($options) > 0)
																								{
																									?>
																									<div class="name2" style="font-size:12px;padding:0px 4px; border:solid 0px; padding-left:10px;">
																										<b>Flavor(s):</b>
																										<div style="padding-left: 20px;">																											
																											<?php																									
																											foreach ($options as $p)
																											{
																												?>																										
																												<div>
																													<?php echo $p->option_qty;?> - <?php echo $p->option_value;?>
																												</div>
																												<?php
																											}																										
																											?>																																																				
																										</div>
																									</div>
																									<?php
																								}
																								?>																								
																							</div>
																							<div class="span3" align="right">
																								<div class="notification">
																									S$ <span id="prod_cost_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>"><?php echo number_format(($row->prod_cost),2);?></span>
																								</div>
																							</div>
																							<div class="span1" >
																								<select name="quantity" autocomplete="off" 
																									id="quantity_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>" 
																									data-prod_group="<?php echo $row->prod_group;?>" 
																									data-public_alias="<?php echo $public_alias;?>" 
																									data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" 
																									data-id="<?php echo $product->product_id;?>" 
																									data-brand_id="<?php echo $brand_row['brand_id'];?>"
																									class="cart_qty" style="width:55px;">
																									<?php 
																										for($x=0;$x<=10;$x++)
																										{
																											?>
																											<option value="<?php echo $x;?>" 
																											<?php
																												if($x == $row->prod_qty)
																												{
																													?>
																													selected="selected"
																													<?php
																												}
																											?>
																											>
																											<?php echo $x;?>
																											</option>
																											<?php
																										}
																									?>
																								</select>
																							</div>
																							<div class="span2" align="right" style="font-weight:bold;font-size:14px;" id="total_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
																								S$ <?php echo number_format(( ($row->prod_cost) ) * ($row->prod_qty),2);?>
																							</div>
																						</div>
																					</div>
																				<?php
																			}


																			$has_delivery = false;
																			$brand_eror='';
																			switch ($brand_row['brand_id']) {
																				case BRAND_PROD_ID_MACALLAN:
																				case BRAND_PROD_ID_SNOW_LEOPARD:
																				case BRAND_PROD_ID_LONDON_DRY:
																						if($per_brand_total<300)
																						{
																							if($accumulated_300 === false)
																							{
																								$brand_eror = "
																								Orders for combined orders of The Macallan whiskies, Snow Leopold Vodka and No.3 London Dry Gin should be S$300 and above to select Delivery.<br>
																								Please order more items or change your Delivery Type to <b>For Pick Up</b>.
																								";
																							}																							
																						}
																					break;
																				case BRAND_PROD_ID_SWIRLS_BAKESHOP:
																					$has_delivery = 16;
																					break;
																				case BRAND_PROD_ID_SINGBEV:
																					if($per_brand_total<350)
																					{
																						$has_delivery = 35;
																					}	
																					break;
																				case BRAND_PROD_ID_DELIGHTS_HEAVEN:
																					$has_delivery = 25;
																					break;
																				case BRAND_PROD_ID_VALRHONA:
																					if($per_brand_total<300)
																						$has_delivery = 35;
																					break;
																				case BRAND_PROD_ID_LAURENT_PERRIER:
																					if($per_brand_qty<3)
																						$has_delivery = 20;
																					break;
																				case BRAND_PROD_ID_PROVIDORE:
																					if($per_brand_total < 200)
																						$has_delivery = 20;
																					break;
																				case BRAND_PROD_ID_EURACO:
																					if($per_brand_total < 300)
																						$has_delivery = 30;
																					break;
																			}
																			$brand_display_error=($brand_eror=='') ? 'none' :'block';
																			?>																			
																			<div 	data-brand_id="<?php echo $brand_row['brand_id'];?>" 
																					data-brand_name="<?php echo $brand_row['brand_name'];?>" 
																					class="brand_error alert alert-error" 
																					id="error_<?php echo $brand_row['brand_id'];?>"
																					style="display:<?php echo $brand_display_error;?>;";
																					align="center"
																			><?php echo $brand_eror;?></div>
																			<?php
																				$has_delivery_display = ($has_delivery===false) ? 'none':'';
																				$delivery_total += $has_delivery;

																				$no_forpickup='';
																				$delivery_only='';

																				$no_fordelivery='';
																				$pickup_only='';
																				if($brand_row['brand_id'] == BRAND_PROD_ID_SINGBEV || $brand_row['brand_id'] == BRAND_PROD_ID_DAILY_JUICE || $brand_row['brand_id'] == BRAND_PROD_ID_LAURENT_PERRIER || $brand_row['brand_id'] == BRAND_PROD_ID_GLENFIDDICH || $brand_row['brand_id'] == BRAND_PROD_ID_MOET_CHANDON || $brand_row['brand_id'] == BRAND_PROD_ID_KUSMI_TEA || $brand_row['brand_id'] == BRAND_PROD_ID_EURACO)
																				{
																					$no_forpickup='none';
																					$delivery_only=' Only';
																				}																				
																				if($brand_row['brand_id'] == BRAND_PROD_ID_NASSIM_HILL ||
																					$brand_row['brand_id'] == BRAND_PROD_ID_KPO
																					)
																				{
																					$no_fordelivery='none';
																					$pickup_only=' Only';
																				}
																				?>
																				<!-- delivery charge and subtotal -->
																				<div class="row-fluid"  >
																					<div class="span8" align="left" >
																						<label style="font-size:14px;font-weight:bold" class="radio inline">Delivery Type: </label>
																						<label class="radio inline control-label"  for="for_pickup_<?php echo $brand_row['brand_id'];?>" style="display:<?php echo $no_forpickup;?>;">
																							<input 	type="radio" 
																									style="display:<?php echo $no_fordelivery;?>;"
																									class="delivery_type" 
																									value="pickup" 
																									name="for_delivery_<?php echo $brand_row['brand_id'];?>" 
																									id="for_pickup_<?php echo $brand_row['brand_id'];?>" 
																									data-id="<?php echo $brand_row['brand_id'];?>" 
																									/>
																							For Pick Up<?php echo $pickup_only;?>
																						</label>
																						<label class="radio inline control-label"  for="for_delivery_<?php echo $brand_row['brand_id'];?>" style="display:<?php echo $no_fordelivery;?>;">
																							<input 	type="radio" 
																									style="display:<?php echo $no_forpickup;?>;"
																									class="delivery_type" 
																									value="delivery" 
																									name="for_delivery_<?php echo $brand_row['brand_id'];?>" 
																									id="for_delivery_<?php echo $brand_row['brand_id'];?>" 
																									checked="checked"
																									data-id="<?php echo $brand_row['brand_id'];?>" 
																									data-delivery_charge="<?php echo $has_delivery;?>"
																									 />
																							For Delivery<?php echo $delivery_only;?>
																						</label>
																					</div>
																					<div 	class="span4 delivery_charge" 
																							data-delivery_charge="<?php echo $has_delivery;?>"
																							data-delivery_type="delivery"
																							data-id="<?php echo $brand_row['brand_id'];?>" 
																						 	align="right" style="font-size:14px;font-weight:bold;display:<?php echo $has_delivery_display;?>" id="delivery_charge_<?php echo $brand_row['brand_id'];?>">
																						Delivery Charge: S$ <span id="delivery_charge_value_<?php echo $brand_row['brand_id'];?>"><?php echo number_format($has_delivery,2);?></span>
																					</div>
																				</div>
																				<!-- delivery charge and subtotal -->
																		</div>
																		<?php
																	}
																?>
																
																<?php 
																}
																else
																{
																	echo "No items on your cart yet.";
																}
																?>
																
																<hr style="padding:0px; margin:0px;">
																<?php
																if(isset( $grand_total) && $grand_total > 0)
																{
																?>
																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Sub Total: S$ <span id="sub_total" ><?php echo number_format($grand_total + $delivery_total, 2);?></span>
																	</div>
																</div>
																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		GST: S$ <span id="gst" ><?php echo number_format(($grand_total * 0.07) ,2);?></span>
																	</div>
																</div>
																<div class="row-fluid" style="padding:10px 0px;" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Total: S$ <span id="grand_total" ><?php echo number_format(($grand_total * 0.07) + $grand_total + $delivery_total ,2);?></span>
																	</div>
																</div>
																<div class="row-fluid" >
																	<div class="span8">
																	</div>
																	<div class="span4 hide" align="right" style="font-size:16px;font-weight:bold">
																		USD$ <span id="grand_total_usd" 																				
																		><?php echo number_format(( ($grand_total + $delivery_total) *BRAND_FOREX_AMEX),2);?></span>
																	</div>
																</div>



																

																<!--
																<div class="row-fluid" >
																	<div class="span7">
																	</div>
																	<div class="span5" align="right" style="font-size:12px;">
																		<i>Note: Your card will be billed in USD.</i>
																	</div>
																</div>
																-->
																<div class="row-fluid" >
																	<div class="span9">
                                                                       <label for="chk_confirm"><input type ="checkbox"  style="margin-top:-1px;" id="chk_confirm" />&nbsp;I understand that card purchases processed are not refundable nor returnable.</label>
                                                                    </div>
																	<div class="span3" align="right" style="font-size:16px;font-weight:bold">
																		<button id="checkout" class="btn-custom-gray-light white" style="font-size:20px !important">Checkout</button>
																	</div>
																</div>
																<?php
																}
																?>
																	
															
															</div>
														</div>
														
														
													</div>
												</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>