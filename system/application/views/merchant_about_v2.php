<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <style type="text/css">
            .brand-page {
                padding:0 0 0 5px;
                background:#5a616c;
                font-family: Arial, sans-serif;
            }
            .brand-title h1 {
                color:#fff;
                margin:0;
                padding:0;
                font-size:1em;
                font-weight:normal;
            }
            .brand-template {
                background:#fff;
                margin-bottom:5px;
                width:720px;
                -webkit-border-radius: 0 5px 0 4px;
                border-radius: 0 5px 0 4px;
                padding:20px 40px;
            }
            .brand-header {
                margin: 20px 0;
            }
            .brand-header:after {
                content:" ";
                display:block;
                clear:both;
            }
            .brand-logo{
                float:left;
                width:220px;
            }
            .brand-description {
                float:right;
                width:500px;
                text-align: justify;
            }
            .brand-template:after {
                content:" ";
                display:block;
                clear:both;
            }
            .brand-content {
                float:left;
                width:100%;
            }
            .products {
                float:right;
                width:120px;
            }
            .products a:hover img {
                opacity: 0.7;
            }
            .brand-section {
                margin-bottom:15px;
            }
            .brand-section:last-child {
                margin-bottom:0px;
            }
            .brand-section .section-type-one .section-col {
                width: 100%;
            }
            .brand-section .section-type-two .section-col {
                width: 275px;
            }
            .brand-section .section-type-three .section-col {
                width: 190px;
            }
            .brand-section .section-type-two_one .section-col:first-child {
                width: 370px;
            }
            .brand-section .section-type-two_one .section-col:last-child {
                width: 180px;
            }
            .section-row:after {
                content:" ";
                display:block;
                clear:both;
            }
            .section-col {
                float:left;
                margin-left:5px;
            }
            .section-col:first-child {
                margin-left:0px;
            }
            .widget-title-head {
                margin:0 0 3px;
                font-size:1.4em;
                line-height:1em;
                font-weight:normal;
            }
            .widget-title-sub {
                margin:0px;
                font-size:1em;
                font-weight:bold;
            }
            .widget-photostory {
            
            }
            .widget-photostory .widget-title {
                padding:11px 8px 7px;
            }
            .widget-photostory-body {
                margin-top:5px;
            }
            .widget-photostory-body:after {
                margin-top:10px;
                content:" ";
                display:block;
                clear:both;
            }
            .widget-photostory-image {
                width:515px;
            }
            .widget-photostory-context {
                width:200px;
                text-align: justify;
            }
            .widget-photostory-context div {
                padding:16px 8px;
            }
            .widget-photostory.right .widget-photostory-image {
                float:right;
            }
            .widget-photostory.right .widget-photostory-context {
                float:left;
            }
            .widget-photostory.left .widget-photostory-image {
                float:left;
            }
            .widget-photostory.left .widget-photostory-context {
                float:right;
            }
            .widget-photostory.grey .widget-title,
            .widget-photostory.grey .widget-photostory-context {
                background:#666;
                color:#fff;
            }
            .widget-photostory.lightgrey .widget-title,
            .widget-photostory.lightgrey .widget-photostory-context {
                background:#ddd;
                color:#333;
            }
            .widget-photostory.white .widget-title,
            .widget-photostory.white .widget-photostory-context {
                background:#f0f0f0;
                color:#888;
            }
            .back-to-menu {
                float:right;
                margin-right: 131px;
                margin-top: 8px;
                border-radius: 5px;
                background: #8bc541;
                padding:4px 20px 4px 20px;
                color:#fff;
            }
            .back-to-menu:hover {
                background: #9bdd48;
                -webkit-box-shadow: 0px 0px 4px 1px rgba(0,0,0,0.4);
                -moz-box-shadow: 0px 0px 4px 1px rgba(0,0,0,0.4);
                box-shadow: 0px 0px 4px 1px rgba(0, 0, 0, 0.4);
            }
            .back-to-menu:hover, .back-to-menu:active {
                color:#fff;
                text-decoration:none;
            }
            .tri {
                width : 0;
                height : 0;
                border-top : 6px solid transparent;
                border-bottom : 6px solid transparent;
                border-right : 15px solid #fff;
                position : relative;
                top:2px;
                left:-5px;
                display: inline-block;
            }
        </style>
    </head>
    <body data-tpl="merchant_about">
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>
        <?php $this->load->view('includes/header.php') ?>

        <div class="container">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="outerBorder">
                        <div class="innerBorder brand-page">
                            <div class="brand-title" style="overflow:auto">
								<h1 style="float:left"><?php echo $merchant->merchant_name;?></h1>
								<a href="/profile/view/<?php echo $this->input->get('frm');?>?tab=tbMerchant&merID=<?php echo $merchant->merchant_id;?>" class="back-to-menu">
                                    
                                    <?php echo $this->lang->line('products_list','Products');?>
                                </a>
							</div>
                            <div class="brand-template">
                                <div class="brand-banner">
                                    <?php if(isset($merchant->banner) and $merchant->banner != ''):?>
                                    <img src="/custom/uploads/merchant/<?php echo $merchant->banner;?>" width="720" />
                                    <?php else:?>
                                    <div></div>
                                    <?php endif;?>
                                </div>
                                <div class="brand-header">
                                    <div class="brand-logo"><img src="<?php echo $this->custom_func->merchant_logo($merchant->merchant_logo) ?>" alt="<?php echo $merchant->merchant_name; ?>"/></div>
                                    <div class="brand-description"><?php echo $merchant->merchant_about?></div>
                                </div>
                                <div class="tabs-menu">
                                    <ul>
                                        <li class="selected"><a href="#tab-1"><?php echo $this->lang->line('news','News');?></a></li>
                                        <li><a href="#tab-2"><?php echo $this->lang->line('merchant_locations','Locations');?></a></li>
                                        <li><a href="#tab-3"><?php echo $this->lang->line('termNconditi0n','Terms &amp; Contditions');?></a></li>
                                    </ul>
                                </div>
                                <div class="tabs-content">
                                    <div id="tab-1" class="news brand-content">
                                        <?php if(isset($sections) and is_array($sections)):?>
                                        <?php foreach($sections as $section):?>
                                        <div class="brand-section">
                                            <div class="section-type-<?php echo strtolower($section->type);?> section-row">
                                                <?php if($widgets && array_key_exists($section->id,$widgets)):?>
                                                <?php foreach($widgets[$section->id] as $widget):?>
                                                <div class="section-col">
                                                    <?php include('includes/widget_type_'.$widget->widget_type.'.php'); ?>
                                                </div>
                                                <?php endforeach;?>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </div>
                                    <div id="tab-2" class="locations" style="display:none;">
                                        <div style="padding:20px;">
                                            <?php foreach ($merchant_outlet->result() as $row):?>
                                            <div>
                                                <strong><?php echo $row->outlet_name;?></strong><br />
                                                <?php echo $formatted=$this->address_m->address_format($row->address_1, $row->address_2, $row->address_street, $row->address_locality, $row->address_city_town, $row->address_region, $row->address_zip_code,  $row->region_name)->address_formatted;?>
                                                <br>
                                                <em><?php echo $row->outlet_phone_number?></em>
                                                <br>
                                                <br>
                                            </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                    <div id="tab-3" class="tab-3 conditions" style="display:none;">
                                        <div style="padding:20px;">
                                            <?php echo $merchant->merchant_terms; ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            .tabs-menu ul {
                padding:0;
                margin:0 0 3px;
                overflow:auto;
            }
            .tabs-menu ul li {
                list-style-type:none;
                float:left;
                width:240px;
                
            }
            .tabs-menu ul li a {
                display:block;
                padding:10px;
                text-align:center;
                font-size:1.2em;
                text-decoration:none;
                background:#666/*#5A606C*/;
                color:#ABB2B8;
                border-top-left-radius: 8px;
                border-top-right-radius: 8px;
            }
            .tabs-menu ul li.selected a {
                background:#E2EAED;
                color:#575D6B;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery('.tabs-menu a').on('click',function(e){
                    e.preventDefault();
                    jQuery('.tabs-content > div').hide();
                    jQuery('.tabs-menu .selected').removeClass('selected');
                    jQuery('.tabs-content '+jQuery(this).attr('href')).show();
                    jQuery(this).parent().addClass('selected');
                });
            });
        </script>
    </body>
</html>
