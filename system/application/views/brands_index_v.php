<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <style type="text/css">
            div.page {
                padding:0 0 0 5px;
                background:#5a616c;
            }
            div.page-title h1 {
                color:#fff;
                margin:0;
                padding:0;
                font-size:1em;
                font-weight:normal;
            }
            div.page-body {
                background:#fff;
                margin-bottom:5px;
                width:720px;
                -webkit-border-radius: 0 5px 0 4px;
                border-radius: 0 5px 0 4px;
                padding:20px 40px;
            }
            div.thumbnails {
                /*background:red;*/
                min-height:10px;
            }
            div.thumbnail {
                float:left;
                margin-left: 20px;
            }
            div.thumbnail a {
                display:block;
            }
            div.thumbnail a img {
                width:200px;
                min-height:200px;
            }
        </style>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>
        <?php $this->load->view('includes/header.php') ?>

        <div class="container">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="outerBorder">
                        <div class="innerBorder page">

                            <div class="page-title"><h1>Brands</h1></div>

                            <div class="page-body">
                                <div class="thumbnails">
                                <?php if ($brands):?>
                                <?php foreach($brands as $b):?>
                                    <div class="thumbnail" data-id="<?php echo $b->id?>" data-name="<?php echo $b->name?>">
                                        <a href="/brands/page/<?php echo $b->id?>" title="<?php echo $b->name?>">
                                            <img src="/custom/uploads/brands/thumbnail/<?php echo $b->thumbnail?>" title="<?php echo $b->name?>" />
                                        </a>
                                    </div>
                                <?php endforeach;?>
                                <?php endif;?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
