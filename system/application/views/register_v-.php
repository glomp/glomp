<?php
/// to check user agent
$ua = $this->custom_func->browser_info();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Register | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script src="assets/frontend/js/validate.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(e) {
    /* Popover */
$('#frmRegister input').hover(function()
{
$(this).popover('show')
},function(){$(this).popover('hide')});

$('#uploadFile').click(function(e) {
/*
var evObj = document.createEvent('MouseEvents');
evObj.initMouseEvent('click', true, true, window);
document.getElementById('fileUploadField').click();*/

			  $('#fileUploadField').click();
				/*$('#upload_loader').html('Please wait...<i class="icon-spinner"></i>');  */
                e.preventDefault();
            });	

 $('#day').keyup(function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('#months').focus();
     }
 });
 
 $('#months').keyup(function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('#year').focus();
     }
 });
  $('#year').keyup(function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('#gender-male').focus();
     }
 });
 	
});
  </script>
  <script type="text/javascript">
	$().ready(function() {
		/* validate signup form on keyup and submit*/
		$("#frmRegister").validate({
			rules: {
				
				fname	:"required",
				lname	:"required",
				day	:{
					 required: true,
						minlength: 2,
						maxlength: 2,
						number: true,
						range: [01, 31]
					},
				months	:{
					 required: true,
						minlength: 2,
						maxlength: 2,
						number: true,
						range: [01, 12]
					},
				year	:{
					 required: true,
						minlength: 4,
						maxlength: 4,
						number: true,
						range: [1900, 3000]
					},
					gender:{ required:true },
				location	:"required",
				email	:{
				 required: true,
				 email: true
				},
				pword:"required",
				cpword:"required",
				agree:"required"
				}
				,
			messages: {
				fname:"",
				lname:"",
				day	:"",
				months	:"",
				year	:"",
				gender:"",
				location:"",
				email	:"",
				pword:"",
				cpword:"",
				agree:""
			}
	});
	});


/* for profile picture	*/
	$(function(){
    profilePic = {
        UpdatePreview: function(obj){
          /* if IE < 10 doesn't support FileReader*/
          if(!window.FileReader){
             /* don't know how to proceed to assign src to image tag*/
          } else {
             var reader = new FileReader();
             var target = null;
             
             reader.onload = function(e) {
              target =  e.target || e.srcElement;
               /*$("#profile_pic").prop("src", target.result);*/
             	var  upload_string = target.result;
				
				 var n=upload_string.search(':image/'); 
				 /*find for :image*/
				 if(n>=0)
				 {
				   $("#profile_pic").prop("src", target.result);
				 }else
				 {
					alert("<?php echo $this->lang->line('invalid_image_extension');?>");
				}
			   $('#upload_loader').html('');
			 };
              reader.readAsDataURL(obj.files[0]);
          }
        }
    };
});
	</script>
</head>
<body>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      <div class="innerBorder">
      	<div class="register">
                    <div class="innerPageTitleRed glompFont"><?php echo $this->lang->line('Register');?></div>
                    <div class="content">
                      
						<?php echo form_open_multipart('landing/register','name="frmCms" class="form-horizontal" id="frmRegister"')?>
                        <div class="row-fluid">
                        <div class="span12">
                        <h1><?php echo $this->lang->line('Personal_Details');?></h1>
                        </div>
                        </div>
                       <?php
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if(isset($error_msg))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
					   ?>
                        <div class="row-fluid">
                        <div class="span6">
                        
                        <div class="control-group">
    <label class="control-label" for="name"><?php echo $this->lang->line('Fname');?><span class="required">*</span></label>
    <div class="controls">
    <?php 	
	if(($this->session->userdata('temp_user_fb_id'))!=""){
		$user_fb_id	= ($this->session->userdata('temp_user_fb_id'));
		$fname = ($this->session->userdata('temp_fname'));
		$mname = ($this->session->userdata('temp_mname'));
		$lname = ($this->session->userdata('temp_lname'));
		$email = ($this->session->userdata('temp_email'));
		$gender = ($this->session->userdata('temp_gender'));
		$day 	= ($this->session->userdata('temp_day'));
		$months = ($this->session->userdata('temp_months'));
		$year 	= ($this->session->userdata('temp_year'));
		
		$array_items = array(
			'temp_user_fb_id' => '',
			'temp_fname' => '',
			'temp_lname' => '',
			'temp_mname' => '',
			'temp_email' => '',
			'temp_gender' => '',
			'temp_day' => '',
			'temp_months' => '',
			'temp_year' => ''
		);
		//$this->session->unset_userdata($array_items);
	}
	else{
		$fname = ($this->session->userdata('fname'))?$this->session->userdata('fname'):set_value('fname');
		$lname = ($this->session->userdata('lname'))?$this->session->userdata('lname'):set_value('lname');
		$email = ($this->session->userdata('email'))?$this->session->userdata('email'):set_value('email');
		$gender = ($this->session->userdata('gender'))?$this->session->userdata('gender'):set_value('gender');
		$day 	= ($this->session->userdata('day'))?$this->session->userdata('day'):set_value('day');
		$months = ($this->session->userdata('months'))?$this->session->userdata('months'):set_value('months');
		$year 	= ($this->session->userdata('year'))?$this->session->userdata('year'):set_value('year');
	}

		if(isset($user_fb_id) && $user_fb_id!=""){
			echo '
			<input type="hidden"  id="fb_user_id" name="fb_user_id" value="'.$user_fb_id.'">
			<input type="hidden"  id="fb_fname" name="fb_fname" value="'.$fname.'">
			<input type="hidden"  id="fb_mname" name="fb_mname" value="'.$mname.'">
			<input type="hidden"  id="fb_lname" name="fb_lname" value="'.$lname.'">
			<input type="hidden"  id="fb_email" name="fb_email" value="'.$email.'">
			<input type="hidden"  id="fb_gender" name="fb_gender" value="'.$gender.'">
			<input type="hidden"  id="fb_day" name="fb_day" value="'.$day.'">
			<input type="hidden"  id="fb_months" name="fb_months" value="'.$months.'">
			<input type="hidden"  id="fb_year" name="fb_year" value="'.$year.'">
			';
		}
	?>	
    <input type="text"  id="fname" name="fname" value="<?php echo $fname;?>" class="span11 textBoxGray" >
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="lname"><?php echo $this->lang->line('Lname');?><span class="required">*</span></label>
    <div class="controls">
    <input type="text"  id="lname" name="lname" value="<?php echo $lname;?>" class="span11 textBoxGray" >
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth');?><span class="required">*</span></label>
    <div class="controls">
    <input type="text" id="day" name="day" maxlength="2" value="<?php echo $day;?>"  class="span2 textBoxGray" placeholder="DD"> /
     <input type="text" id="months" name="months" value="<?php echo $months;?>" maxlength="2"  class="span2 textBoxGray" placeholder="MM"> /
      <input type="text" id="year" name="year" maxlength="4" value="<?php echo $year;?>"  class="span3 textBoxGray" placeholder="YYYY">
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender');?><span class="required">*</span></label>
    <div class="controls">
    <label class="radio inline">
<input type="radio" name="gender" id="gender-male" value="Male" <?php echo $checked=($gender!='Female')?'Checked="checked"':"";?> /> <?php echo $this->lang->line('Male');?>
</label>
<label class="radio inline">
<input type="radio" name="gender" id="gender-female" <?php echo $checked=($gender=='Female')?'Checked="checked"':"";?> value="Female"> <?php echo $this->lang->line('Female');?>
</label>
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="location"><?php echo $this->lang->line('Location');?><span class="required">*</span></label>
    <div class="controls">
    
      <select name="location" class="textBoxGray" id="location">
      <option value=""><?php echo $this->lang->line('Select_Location');?></option>
      <?php 
	  echo $this->regions_m->location_dropdown(0,set_value('location'));
	  ?>
      </select>
    </div>
    </div>
                        </div>
                        
                        <div class="span5">
                      		<?php
							
							 $browser = $this->agent->browser();
							if($browser == 'Internet Explorer' || $browser == 'Safari')
							{
							 ?>
                            
<?php echo $this->lang->line('upload_profile_photo');?>
    <input type="file" name="user_photo" value="" accept="image/*"  id="fileUploadField" />
  
  
                     	`<?php }
					 
								else
								{?>
                                
                                 <div class="uploadImgae">&nbsp;<img src="assets/frontend/img/default_profile_pic_register.jpg" alt="" id="profile_pic" width="150" height="160" /></div>
                         <div class="upload">
                          
                     <a href="javascript:;" id="uploadFile" ><?php echo $this->lang->line('upload_profile_photo');?></a>
                       <span id="upload_loader"></span>
                      <p><?php echo $this->lang->line('Note');?>: <?php  
					   echo $this->lang->line('profile_image_upload_note'); ?></p>
                     </div>
                     
                     <input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden;" onchange='profilePic.UpdatePreview(this)' id="fileUploadField" />
                                
                                <?php
								}
						?>
                        	
                        </div>
                        </div>
                         <div class="row-fluid">
   						 <div class="span12 border">
                       
                        </div>
                        </div>
                        
                    <div class="row-fluid">
					<?php	
					if(isset($user_fb_id) && $user_fb_id!=""){
						echo'
						<div class="span12">
                            <label class="checkbox">
								<input type="checkbox" name="chk_notif_via_fb" id="chk_notif_via_fb" value="Y" />
								<strong>'.$this->lang->line('Notif_via_fb').'</strong>
							</label>
						</div>';
					}
					?>
						<div class="span6">
						<div class="control-group">
						<label class="control-label" for="email"><?php echo $this->lang->line('email');?><span class="required">*</span></label>
						<div class="controls">
						<input type="text"  id="email" value="<?php echo $email;?>" name="email" class="span11 textBoxGray" rel="popover" data-content="<?php echo $this->lang->line('emailTips');?>" <?php /*?>data-original-title="<?php echo $this->lang->line('E-mail');?>" <?php */?>>
							</div>
							</div>
                        </div>
                    </div>
                      
                        
                         <div class="row-fluid">
   						 <div class="span12 border">
                       
                        </div>
                        </div>
                        
                      <div class="row-fluid">
                       
   					 <div class="span5">
                     <div class="createPasswordTable" >
                     <table width="100%" border="0" >
                           <tr>
                             <td><label class="control-label" for="pword"><?php echo $this->lang->line('Create_Password');?><span class="required">*</span></label></td>
                             <td>
                             <input type="password"  id="pword" name="pword" class="span11 textBoxGray" > </td>
                           </tr>
                           <tr>
                             <td>
                             <label class="control-label" for="cpword"><?php echo $this->lang->line('Confirm_Password');?><span class="required">*</span></label></td>
                             <td><input type="password" id="cpword" name="cpword" class="span11 textBoxGray" ></td>
                           </tr>
                         </table>
                    
                    </div>
                        </div>
                        <div class="span5">
                        <div class="createPasswordTips">
                        
                        <?php echo $this->lang->line('passwors_tips');?>
                        </div>
                        </div>
                      </div>
                        
                        
                        
                        
                        <div class="row-fluid">
   						 <div class="span12 border">
                       
                        </div>
                        </div>
                        
                        <div class="row-fluid">
   						 <div class="span12">
                                        <label class="checkbox">
                <input type="checkbox" name="agree">
				<?php echo $this->lang->line('I_Agree_to_the');?><strong> 
				<?php echo $this->lang->line('termNconditi0n');?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('index.php/page/index/2');?>" target="_blank"><strong><?php echo $this->lang->line('Read');?></strong></a> <br/><br/>
                </label>
              
<br/><br/>

                        </div>
                        </div>
                        
                        <div class="row-fluid">
   						 <div class="span8">
                        
<div class="control-group">
<input type="hidden" name="token" value="<?php echo $token;?>" />
    <button type="submit" class=" btn-custom-reg" style=" padding:10px 40px; font-size:14px;" name="Register" ><?php echo $this->lang->line('Submit');?></button>
    <div class="pull-right"><button type="reset" style=" padding:10px 19px; font-size:14px;" class="btn-custom-reset" name="Reset" ><?php echo $this->lang->line('Reset_All_Fields');?></button></div>
    <div class="clearfix"></div>
    </div>
                        </div>
                        </div>
                       
    
    </form>
    
    
                    </div>
                </div>
                </div>
       </div>
      
      </div>
      </div>
     
</div>
</body>
</html>