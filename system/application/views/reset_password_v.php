<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $page_title;?> </title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>

</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      	<div class="register">
                    <div class="innerPageTitleRed"><?php echo $this->lang->line('reset_password');?></div>
                         <div class="content">
               <?php echo form_open("landing/reset_password",'name="login_form" class="form-horizontal" id="frmLogin"')?>
     <?php
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if(isset($error_msg))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
					  
					   ?>
     <span id="display_msg"></span>
    <div class="control-group">
    <label class="control-label" for="inputEmail"><?php echo $this->lang->line('Email');?></label>
    <div class="controls">
    <input type="email"  name="user_email" id="inputEmail"  class="span5 textBoxGray" >
   <span id="loader"></span>
    </div>
    </div>
    <div class="control-group">
    <button type="submit" name="reset_password" class="btn-custom-reg"><?php echo $this->lang->line('reset_password');?></button>
    <button type="button" name="cancel" onClick="javascript:location.href='<?php echo base_url();?>'" class="btn-custom-reset"><?php echo $this->lang->line('Cancel');?></button>
     <div class="clearfix"></div>
    </div>
    </form>
            </div>
                    </div>
       </div>
      
      </div>
      </div>
</div>
<?php
if(isset($reset_confirm_popup))
{
?>
<script language="javascript" type="text/javascript">
    if(confirm('<?php echo $this->lang->line('reset_password_confirm_msg')?>'))
	{    
		 $('#loader').html('<i class="icon-spinner icon-spin icon-large"></i>  Please wait..');
		var base_url = '<?php echo base_url('landing/update_password/');?>';
			$.ajax({
			type:'POST',
			data:"email=<?php echo $user_id;?>",
			url:base_url,
			success:function(html){
				 $('#display_msg').html(html);		
					$('#loader').html('');
				}
			});
		
	}else
	{
		location.href='<?php echo base_url();?>'+'landing/reset_password';		
	}
    
	
		
	
</script>
<?php
}
?>
</body>
</html>
