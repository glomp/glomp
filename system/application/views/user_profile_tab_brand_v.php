<?php
$is_18 = $this->users_m->check_if_18($this->session->userdata('user_id'));        
?>
<div id="brands-gallery" style="background:#343239">
    
    <ul class="menu-gallery">
    <?php if($user_record->user_city_id==2)
    {

        //get user dob
        
    ?>
        <li style="background-image:url('https://glomp.it/custom/uploads/brands/american-express.jpg');background-size:contain;background-position:center center;background-repeat:no-repeat">
            <a href="<?php echo ($is_18) ? site_url("amex-sg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                <span>Amex</span>
            </a>
        </li>

        <li style="background-image:url('https://glomp.it/assets/frontend/img/UOB_Cards.jpg');background-size:contain;background-position:center center;background-repeat:no-repeat">
            <a href="<?php echo ($is_18) ? site_url("uobsg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                <span>UOB</span>
            </a>
        </li>

        <li style="background-image:url('https://glomp.it/custom/uploads/brands/dbs_front.jpg');background-size:contain;background-position:center center;background-repeat:no-repeat">
            <a href="<?php echo ($is_18) ? site_url("dbssg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                <span>UOB</span>
            </a>
        </li>
    <?php
    }
    ?>
    <?php if($brands):?>
    
        <?php foreach($brands as $b):?>
        <li data-id="<?php echo $b->id?>" data-name="<?php echo $b->name?>" data-logo="<?php echo $b->logo?>" style="background-image:url('/custom/uploads/brands/<?php echo $b->logo?>');background-size:100% 100%">
            <a 
            href="<?php echo ($is_18) ? site_url("/profile/menu/brands/".$profile_id."/".$b->id): 'javascript:void(0);' ; ?> "
            class="btn-brand <?php echo ($is_18) ? '': 'brand_not_18';?>" 
            data-id="<?php echo $b->id?>">
                <span><?php echo $b->name;?></span>
            </a>
        </li>
        <?php endforeach;?>
    
    <?php else:
        if($user_record->user_city_id!=2)
        {
    ?>
        <div class="menu-gallery" style="padding:10px;color:#eee"><?php echo $this->lang->line('no_brands_found','No brands featured as yet.');?></div>
        
    <?php
        }
        endif;?>
    </ul>
</div>

<div id="brand-layer" class="row-fluid">
    <div class="tabContent" style="background:#343239">
        <div class="span2 thumbnails">
            <div id="brand-info">
                <div class="thumbnail">
                    <a href="#" title=""><img src="" alt="" /></a>
                    <a href="#" class="btn-custom-gray-big btn-block btn-return" style="margin-top:30px;font-weight:normal"><?php echo $this->lang->line('brands_list','Brands List')?></a>
                </div>
            </div>
        </div>
        <div id="brandproducts-list" class="span10 tabInnerContent" comment="List of Products for Brands"></div>

        <div id="merchant-layer" class="span10 tabInnerContent" comment="Will show after selecting a BrandProduct">
            <div id="brandproduct-info" class="span3" comment="This will propagate after selecting a brandproduct">
                <div class="thumbnails">
                    <img src="#" alt="#" />
                    &nbsp;<br />
                    <div class="name"></div>
                    <div class="description"></div>
                    <a href="#" class="btn-custom-gray-big btn-block btn-return2" style="margin-top:20px;font-weight:normal"><?php echo $this->lang->line('products_list','Products List')?></a>
                </div>
            </div>
            <div id="merchantproducts-list" class="span9 merchantCatProduct" comment="List of Products from Merchants">asd</div>
        </div>
    </div>
</div>


<style>
    .menu-gallery { margin:0;padding:8px 0 20px;list-style-type:none; overflow:auto;}
    .menu-gallery li {float:left;margin:14px 0 0 14px;background:#fff;border-radius:15px;box-shadow:none;overflow:hidden;width:287px;margin-left:14px;min-height:200px}
    .menu-gallery li a {display:block;height:200px}
    .menu-gallery li a span {display:none;}
    .menu-gallery li img {width:90px !important;height:90px !important;}
        padding: 10px;
    #brandproduct-info .thumbnail {background: #fff;}
</style>
<script type="text/javascript">
    
    /* 
        this fixes the active tab for brand 
        because makig a different tab background for
        one tab is not very good
    */
    jQuery(document).find('#tbBrands.activeTab').removeClass('activeTab').css({'background':'#343239','color':'#fff'});
    /* end of fix */
    var BrandsGallery = jQuery('#brands-gallery');
    var BrandLayer = jQuery('#brand-layer');
    var BrandInfo = jQuery('#brand-info');
    var BrandProductInfo = jQuery('#brandproduct-info');
    var BrandProductsList = jQuery('#brandproducts-list');
    var MerchantLayer = jQuery('#merchant-layer');
    var MerchantProductsList = jQuery('#merchantproducts-list');

    jQuery(document).on('click','#tbBrands',function(){
        BrandLayer.hide();
        MerchantLayer.hide();
        BrandsGallery.fadeIn(300);
    });
    jQuery(document).on('click','.btn-return',function(e){
        e.preventDefault();
        BrandsGallery.fadeIn(300);
        jQuery('#tbBrands').trigger('click');
    });
    jQuery(document).on('click','.btn-return2',function(e){
        e.preventDefault();
        MerchantLayer.fadeOut(100);
        BrandProductsList.fadeIn(300);
    });
    
    /*TODO CHANGE PLEASE */
    function show_brand_products(x) {
        var x;
        jQuery.getJSON(
            '/brands/json_brand_products',
            {brand_id:jQuery(x).attr('data-id')},
            function( response ) {
                var htmlList = '';
                BrandInfo
                    .css({'padding':'14px','background':'#d2d7e1','border-bottom-left-radius':'5px','border-top-right-radius':'5px'})
                    .find('img')
                    .attr("src",'/custom/uploads/brands/'+response.brand.thumbnail)
                    .attr("alt",response.brand.name);
                BrandInfo.find('.btn-return')
                    .attr("href","/profile/menu/brands/<?php echo $profile_id?>/");
                BrandInfo.find('.thumbnail a:first-child')
                    .attr('href',"/profile/menu/brands/<?php echo $profile_id?>/"+response.brand.id)
                    .attr('title',response.brand.name);
                if(response.products.length > 0) {
                    htmlList = '<ul class="row-fluid merchantCatProduct">';
                    jQuery.each(response.products,function(i,o){
                        htmlList += '<li class="fl product-card displayPopUp">';
                        htmlList += '<a href="javascript:void();" style="overflow:auto;text-align:center" class="show-merchant-products" data-id="'+o.id+'" data-name="'+o.name+'" data-description="'+o.description+'" data-image_logo="'+o.image_logo+'">';
                        htmlList += '<div class="thumbnail"><img src="/custom/uploads/brands/products/'+o.image_logo+'" /></div>';
                        htmlList += '<div class="context"><strong>'+o.name+'</strong></div>';
                        htmlList += '</a>';
                        htmlList += '<div class="popUp" style="margin:0">';
                        htmlList += '<div class="name">'+o.name+'</div>';
                        htmlList += '<div class="product-desc">'+o.description+'</div>';
                        htmlList += '</div>';
                        htmlList += '</li>';
                    });
                    htmlList += '</ul>';
                    BrandProductsList.html(htmlList)
                        .find('ul').css({'margin':'0','padding':'0'})
                        .find('li').css({'margin':'0 14px 20px','width':'110px','min-height':'140px'})
                        .find('.thumbnail img').css({'height':'auto'})
                        .parents('li')
                        .find('.context').css({'display':'block','line-height':'1.2em','margin-top':'1em','color':'#8bc541'});
                }
                BrandsGallery.fadeOut(100);
                BrandProductsList.fadeIn(300);
                BrandLayer.fadeIn(300);
                
                jQuery(document).trigger('show-merchant-products');
            }
        );
    };

    jQuery(document).on('click','.btn-brand',function(e){
        /*e.preventDefault();
        show_brand_products(this);*/
         BrandProductsList.html('');
    });

    jQuery(document).on('click','.show-merchant-products',function(e){
        e.preventDefault();
        var $this = jQuery(this);
        MerchantProductsList.html('');
        jQuery.getJSON(
            '/brands/json_merchant_products',
            {'brandproduct_id':$this.attr('data-id')},
            function(response){
                var html = '';
                
                BrandProductInfo
                    .css({'padding':'14px','background':'#d2d7e1','border-bottom-left-radius':'5px','border-top-right-radius':'5px'})
                    .find('img')
                    .attr('src','/custom/uploads/brands/products/'+$this.attr('data-image_logo'))
                    .attr('alt',$this.attr('data-name'));
                BrandProductInfo.find('.name').html($this.attr('data-name'));
                BrandProductInfo.find('.description').html($this.attr('data-description'));
                BrandProductInfo.hide(); /* zac says we don't need this */

                if(response.products.length > 0) {

                    html += '<ul class="thumbnails">';
                    jQuery.each(response.products,function(i,o){
                        var img = 'product.jpg';
                        if(o.prod_image) img = o.prod_image;
                        html += '<li class="roductImages displayPopUp glompItemMerchant"';
                        html += ' data-image="/custom/uploads/products/thumb/'+img+'"';
                        html += ' data-point="'+o.prod_point+'"';
                        html += ' data-productname="'+o.prod_name+'"';
                        html += ' data-id="'+o.prod_id+'"';
                        html += ' data-merchantname="'+o.merchant_name+'"';
                        html += ' style="min-height:180px;cursor:pointer;margin:10px 20px;">';
                        html += '<div class="notification">'+o.prod_point+'<div class="notification_pts">pts</div></div>';
                        html += '<div class="thumbnail"><img src="/custom/uploads/products/thumb/'+img+'" alt="'+o.prod_name+'" /></div>';
                        html += '<div class="name1" style="padding:0px 4px; border:solid 0px">'+o.merchant_name+'</div>';
                        html += '<div class="name2" style="padding:0px 4px; border:solid 0px">'+o.prod_name+'</div>';
                        html += '<div class="popUp" style="margin:0">';
                        html += '<div class="merchant-name name1">'+o.merchant_name+'</div>';
                        html += '<div class="product-name name2">'+o.prod_name+'</div>';
                        html += '<div class="product-desc">'+o.prod_details+'</div>';
                        html += '</div>';
                        html += '</li>';
                    });
                    html += '</ul>';
                    MerchantProductsList.html(html);

                } else {
                    html = '<?php echo $this->lang->line('no_brandproducts', '<span class="alert alert-info">Product is not available at present.</span>');?>';
                    MerchantProductsList.html(html);
                }
                

                BrandProductsList.fadeOut(100);
                MerchantProductsList.fadeIn(300);
                MerchantLayer.fadeIn(300);
            }
        );
    });
    
    
    jQuery(document).on('mouseenter','.displayPopUp > *', function(e){
        jQuery(e.target).parents('li').find('.popUp').show().css({'margin':'-35px 0 0 0px'});
    }).on('mouseleave','.displayPopUp', function(e){
        jQuery('.popUp').hide();
    });
    
    /* for when coming from brand page */
    var qs = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=', 2);
            if (p.length == 1) {
                b[p[0]] = "";
            } else {
                b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
        }
        return b;
    })(window.location.search.substr(1).split('&'));
    
    jQuery(document).ready(function(){
        
        if(typeof qs.showbrandproducts == 'string') {
            jQuery('#tbBrands').trigger('click');
            var data = qs.showbrandproducts.split('/');
            
            show_brand_products(jQuery('.btn-brand[data-id='+data[0]+']'));
            /*jQuery('.btn-brand[data-id='+data[0]+']').trigger('click');*/
            
            jQuery(document).on('show-merchant-products',function(e){
                jQuery(document).find('.show-merchant-products[data-id='+data[1]+']').trigger('click');
            });
        }
        if(typeof qs.showbrands == 'string') {
            jQuery('#tbBrands').trigger('click');
        }
		if(typeof qs.showmerchants == 'string') {
            jQuery('#tbMerchant').trigger('click');
        }
    });
</script>

 <?php

        if(!$is_18)        
        {
            ?>
            <script language="javascript" type="application/javascript">

                 $(document).ready(function() {

                    $('.brand_not_18').on({
                        click: function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            var NewDialog = $(' <div id="hsbc_dialog" align="center" style="padding-top:25px !important; ">\
                                                <div class="alert alert-error"  id="showError_brand" style="font-size:14px" align="left">\
                                                    <span id="showErrorMessage_brand">\
                                                    You must be at least 18 years of age to enter this page.\
                                                    </span>\
                                                    </div>\
                                                </div>');
                            NewDialog.dialog({                    
                                dialogClass:'noTitleStuff',
                                title: "",
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 450,
                                height:140,
                                show: '',
                                hide: '',
                                buttons: [{text: "OK",
                                        "class": 'btn-custom-blue width_147_px cancel_glomp_option',
                                        click: function() {
                                            $("#hsbc_dialog").dialog("close").dialog('destroy').remove();
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    });

                });
             </script>
            <?php
        }
        ?>