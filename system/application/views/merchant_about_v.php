<?php
$fromLink = '';
if (isset($_GET['frm'])) {
    if ($_GET['frm'] == 'you')
        $fromLink = '?tab=tbMerchant&merID=' . $merchant_record->merchant_id;
    else if (is_numeric($_GET['frm'])) {
        $fromLink = 'view/' . $_GET['frm'] . '?tab=tbMerchant&merID=' . $merchant_record->merchant_id;
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $merchant_record->merchant_name; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
              "stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="assets/frontend/css/als_demo.css" />  
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery.als-1.2.min.js"></script>
    </head>
    <body>
		<?php include_once("includes/analyticstracking.php") ?>
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="outerBorder" style="float:left;">
                        <div class="innerBorder3" style="float:left;width:896px">
                            <div class="tabContent" style="float:left;width:860px">
                                <div style="border:0px solid #333; padding:5px;width:415px;float:left">
                                    <div class="thumbnail merchantShadow" style="float:left">
                                        <img style="height:80px" src="<?php echo $this->custom_func->merchant_logo($merchant_record->merchant_logo) ?>" alt="<?php echo $merchant_record->merchant_name; ?>" />
                                    </div>
                                    <div class="title_type1" style="float:left;margin-top:60px; ">									
                                        <?php echo $merchant_record->merchant_name; ?>
                                    </div>
                                    <div style="clear:both;padding:5px 10px;">
                                        <?php echo $merchant_record->merchant_about; ?>
                                    </div>
                                    <div class="row-fluid" style="padding:10px;">
                                        <a href="<?php echo base_url("profile/" . $fromLink) ?>" class="btn-custom-gray-big" style="font-weight:normal;"><?php echo $this->lang->line('merchant_menu'); ?></a>
                                    </div>
                                </div>
                                <div style="border:0px solid #333; padding:5px;width:415px;float:right">
                                    <div class="merchant_about_right_white" style="">
                                        <div>
                                            <div class="title_type1" style="float:left;margin-top:10px; ">
                                                <?php echo $this->lang->line('merchant_locations'); ?>
                                            </div>
                                        </div>
                                        <div style="clear:both;padding-top:5px;">										
                                            <?php
//                                            echo '<pre>';
//                                            print_r($merchant_outlet->result());
//                                            exit;
                                            foreach ($merchant_outlet->result() as $row) {
                                                /*$outlet_name = $row->outlet_name;
                                                $address1 = $row->address_1;
                                                $region = $row->outlet_region;  
                                                $city = $row->address_city_town;  
                                                $zipcode = $row->address_zip_code;
                                                
												print_r($row).'<br><br><br>';	
                                                $address = $outlet_name.' '.$address1. ' '. $region. ' '.$city. ', '.$zipcode; 
                                                */												

												$formatted=$this->address_m->address_format($row->address_1, $row->address_2, $row->address_street, $row->address_locality, $row->address_city_town, $row->address_region, $row->address_zip_code,  $row->region_name);                                                
												$outlet_name=$row->outlet_name;
                                                echo '
                                                        <div class="merchant_about_location_wrapper">
																<strong>'.$outlet_name.'</strong><br>
                                                                '.$formatted->address_formatted.'
                                                        </div>
                                                ';
                                            }//foreach
                                            ?>
                                            <!--<div class="merchant_about_location_wrapper">
                                                    80, Airport Boulevard<br>
                                                    Unit No: #01-06<br>
                                                    Terminal 1, Arrival hall Central<br>
                                            </div>-->

                                        </div>
                                    </div>
                                    <div class="merchant_about_right_white" style="margin-top:20px;">
                                        <div class="title_type1" style="">
<?php echo $this->lang->line('termNconditi0n'); ?>
                                        </div>
                                        <div style="clear:both;padding:5px 10px;">
<?php echo $merchant_record->merchant_terms; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                    <div style="clear:both" ></div>
                </div>
            </div>      
        </div>
    </body>
</html>