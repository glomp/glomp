<?php
    header('Content-type: application/xml');
    
    echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

  
    <url>
        <loc><?php echo base_url('');?></loc>      
    </url> 
    <url>
        <loc><?php echo base_url('page/faq');?></loc>      
    </url> 

    <?php    
    foreach($users->result() as $rec)
    {        
    ?>
    <url>
        <loc><?php echo base_url('profile/view/'.$rec->user_id);?></loc>      
    </url>        
    <?php
    }    
    ?>
    
    
    
    
    
    
    <?php    
    // mobile 
    foreach($users->result() as $rec)
    {        
    ?>
    <url>
        <loc><?php echo base_url(MOBILE_M.'/profile/view/'.$rec->user_id);?></loc>
    </url>        
    <?php
    }    
    ?>
    
    
    <?php    
    
    // merhcant
    foreach($merchants->result() as $rec)
    {   
    ?>
    <url>
        <loc><?php echo base_url('merchant/about/'.$rec->merchant_id);?></loc>      
    </url>        
    <?php
    }    
    ?>
    
   
</urlset> 