<?php 
    $data['page_title'] = 'Register | glomp!';
    $data['css'] = array();
    $data['js'] = array(
        '<script src="//connect.facebook.net/en_US/all.js"></script>',
        '<script type="text/javascript" src="'.get_protocol().'://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>',
        '<script type="text/javascript">
            var FB_APP_ID = "'.$this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS).'";
            var LOCATION_REGISTER = "'.base_url('index.php/landing/register').'";
            var SIGNUP_POPUP_TEXT = "'.$this->lang->line('signup_popup_text').'";
            var SIGNUP_WITH_FB_BUT_REG = "'.$this->lang->line('signup_with_fb_but_already_registered').'";
        </script>'
    );
?>

<?php $this->load->view('includes/header2.php', $data, FALSE, FALSE, FALSE) ?>
    <!-- Facebook SDK init -->
    <script type="text/javascript">
        window.fbAsyncInit = function() {
            FB.init({
                appId      : FB_APP_ID,
                xfbml      : true,
                version    : 'v2.0',
                status     : true,
                cookie     : true
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div class="container">
        <div class="inner-content">
            <div class="row-fluid">
                <div class="outerBorder">
                    <div class="innerBorder">
                        <div class="register">
                            <div class="innerPageTitleRed glompFont"><?php echo $this->lang->line('Register'); ?></div>
                            <div class="content">

                                <!-- FORM STARTS HERE -->
                                <form action ="<?php echo site_url('user2/register'); ?>" method="post" name="frmCms" class="form-horizontal" id="frmRegister" enctype="multipart/form-data">
                                    <input type="hidden" id="social_id" name="social_id" />
                                    <input type="hidden" id="registration_type" name="registration_type" />
                                    <input type="hidden" id="profile_pic" name="profile_pic" />

                                    <!-- TITLE -->
                                    <div class="row-fluid"><div class="span12">
                                        <h1><?php echo $this->lang->line('Personal_Details'); ?></h1>
                                    </div></div>

                                    <!-- MESSAGE PROMPT -->
                                    <div id="register_error"></div>
                                    <div id="validation-errors" style="display:none">
                                        <div id="errors-inner" class="alert alert-error" style="font-size:12px !important; margin: 0px !important;"></div>
                                    </div>

                                    <!-- FIELDS -->
                                    <div class="row-fluid">

                                        <!-- LEFT Fields -->
                                        <div class="span6">

                                            <!-- fname -->
                                            <div class="control-group">
                                                <label class="control-label" for="name">
                                                    <?php echo $this->lang->line('Fname'); ?><span class="required">*</span>
                                                </label>
                                                <div class="controls">
                                                    <input type="text"  id="fname" name="fname" value="<?php echo $this->input->post( 'fname' ); ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>

                                            <!-- lname -->
                                            <div class="control-group">
                                                <label class="control-label" for="lname"><?php echo $this->lang->line('Lname'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text"  id="lname" name="lname" value="<?php echo $this->input->post( 'lname' ); ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>
                                            
                                            <!-- gender -->
                                            <div class="control-group">
                                                <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo ($this->input->post('gender') != 'Female') ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-female" <?php echo ($this->input->post('gender') == 'Female') ? 'Checked="checked"' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <!-- location -->
                                            <div class="control-group">
                                                <label class="control-label" for="location"><?php echo $this->lang->line('Location'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input class="textBoxGray" type="text" name="location_text" id="location" value="<?php echo $this->input->post('location_text'); ?>" style="width:250px"  autocomplete="off" placeholder="">
                                                    <input type="hidden" name="location" id="location_id" value="<?php echo $this->input->post('location'); ?>" autocomplete="off" class="" >
                                                    <div class="sutoSugSearch" id="autoSugSearch" style="color:#59606B !important;margin-left:20px;width:244px">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <!-- seperator --><div class="fb_separator row-fluid"><div class="span12 border"></div></div>

                                    <!-- FACEBOOK LINK-->
                                    <div class="fb_separator row-fluid">
                                        <div class="span6" >
                                            <button type="button" class="btn-custom-blue glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_Facebook" id="Link_Facebook" >Link Facebook account</button>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="li_separator row-fluid"><div class="span12 border"></div></div>
                                    
                                    <!-- LINKEDIN LINK -->
                                    <div class="li_separator row-fluid">
                                        <div class="span6" >
                                            <button type="button" class="btn-custom-007BB6 glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_LinkedIn" id="Link_LinkedIn" onclick ="liAuth()" >Link LinkedIn Account</button>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid "><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">						
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="email"><?php echo $this->lang->line('email'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="email"  id="email" value="" name="email" class="span11 textBoxGray" rel="popover" data-content="<?php echo $this->lang->line('emailTips'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="pword"><?php $temp = $this->lang->line('Create_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right">
                                                            <input type="password" maxlength="9"  id="pword" name="pword" class="span11 textBoxGray" value="<?php echo $this->input->post('pword'); ?>" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" > </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="cpword"><?php $temp = $this->lang->line('Confirm_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right"><input maxlength="9" type="password" id="cpword" name="cpword" class="span11 textBoxGray" value="<?php echo $this->input->post('cpword'); ?>" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" ></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <div class="createPasswordTips">
                                                <?php echo $this->lang->line('passwors_tips'); ?><br /><br />
                                                <?php echo $this->lang->line('passwors_tips_length','(6-9 characters)'); ?>
                                            </div>&nbsp;
                                        </div>
                                    </div>

                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="promo_code"><?php $temp = $this->lang->line('Promotional_Code'); echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                        <td align="right">
                                                            <input type="text" maxlength="45"  id="promo_code" name="promo_code" placeholder="(optional)" class="span11 textBoxGray" value="<?php echo $this->input->post('promo_code'); ?>" >
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </div>
                                        </div>                                        
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <label class="checkbox">
                                                <input type="checkbox" name="agree">
                                                    <?php echo $this->lang->line('I_Agree_to_the'); ?><strong> 
                                                    <?php echo $this->lang->line('termNconditi0n'); ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('/page/index/terms'); ?>" target="_blank"><strong><?php echo $this->lang->line('Read'); ?></strong></a> <br/><br/>
                                            </label>

                                            <br/><br/>

                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="control-group">
                                                <button type="submit" id="register_btn" class=" btn-custom-reg" style=" padding:10px 40px; font-size:14px;" name="Register" ><?php echo $this->lang->line('Submit'); ?></button>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <button type="reset" style="margin-left:3px; padding:10px 19px; font-size:14px;" class="btn-custom-reset" name="Reset" id="reset" ><?php echo $this->lang->line('Reset_All_Fields'); ?></button>
                                        </div>
                                    </div>

                                
                                </form>
                                <!-- FORM ENDS HERE -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- generic dialog box -->
    <div id="dialog" title="">
        <div id="dialog-content" style="line-height:120%; font-size:16px" align="center"></div>
    </div>
    
    <script type="text/javascript">

        var social_response;


        function loginToFB(params) {
            var method = method || "post"; /* Set method to post by default if not specified.
            The rest of this code assumes you are not using a library.
            It can be made less wordy if you use one.*/
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", GLOMP_BASE_URL + '/landing');   

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);
                    form.appendChild(hiddenField);
                }
            }

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "fb_login");
            hiddenField.setAttribute("value", "fb_login");
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            return form.submit();
        }
        
        $('#validation-errors').dialog({
            autoOpen: false,
            closeOnEscape: false ,
            resizable: false,					
            dialogClass:'dialog_style_error_alerts noTitleStuff ',
            modal: true,
            title:'Please wait...',
            position: 'center',
            width:400,
            buttons:[
                {text: "Close",
                "class": 'btn-custom-white2',
                click: function() {
                    $(this).dialog("close");
                }}
            ]
        });
        
        /* Generic Dialog Box */
        function prompt( msg, titleYes, titleNo, callbackYes, callbackNo ){
            jQuery('#dialog').dialog({
                dialogClass: 'noTitleStuff',
                resizable: false,
                modal: true,
                buttons: [
                    {
                        text: titleYes,
                        "class": 'btn-custom-darkblue',
                        click: callbackYes
                    },
                    {
                        text: titleNo,
                        "class": 'btn-custom-white2',
                        click: callbackNo
                    }
                ]
            }).find('div').html(msg);
        }
        

        /* TODO: REMOVE THIS QUICK FIX */
        <?php if (isset($_GET['fname'])) { ?>
            $('#email').val('<?php echo $_GET['email']; ?>');
            $('#fname').val('<?php echo $_GET['fname']; ?>');
            $('#lname').val('<?php echo $_GET['lname']; ?>');
        <?php } ?>
            
        $('#register_btn').click(function(e) {
           e.preventDefault();
           $.ajax({
                type: "POST",
                dataType: 'json',
                url: $('#frmRegister').attr('action'),
                data: $('#frmRegister').serialize(),
                success: function(r){ 
                    if (r.error != '') {
                        $('#errors-inner').html(r.error);
                        $('#validation-errors').dialog('open');
                        return;
                    } else if (r.status == 'login') {
                        if ($('#registration_type').val() == 'FB' ) {
                            return loginToFB(social_response);
                        } 

                        if ($('#registration_type').val() == 'LI' ) {
                            //redundant code???
                            var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
                                    Connecting to LinkedIn...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
                            </div></div>');

                            connectDialog.dialog({
                                dialogClass: 'noTitleStuff',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                closeOnEscape: false,
                                modal: true,
                                width: 300,
                                height: 120,
                                show: '',
                                hide: ''
                            });

                            return login_linkedIN(connectDialog, social_response);
                        }
                    } else {
                        return window.location = r.location;
                    }
                }
            });
        });
        
        /* FACEBOOK ACCOUNT */
        jQuery('#Link_Facebook').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var facebookDialog = jQuery('<div id="facebookConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">Connecting to Facebook...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
            facebookDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    title: '',
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 300,
                    height: 110,
                    show: '',
                    hide: ''
                });
            
            function propagateFacebook() {
                FB.api('/me', function(response){
                    FB.api('/me/picture?width=140&height=140', function(r){
                        response.profile_pic = r.data.url
                        checkRecords(response);
                    });
                });
            }
            
            
            function checkRecords(r) {
                jQuery.ajax({
                    type: "POST",
                    url: 'ajax_post/checkIfUserHasAnExistingAcct/?id=' + r.id,
                    data: 'fbID=' + r.id,
                    dataType: 'json'
                }).done(function(response){
                    
                    if(response.data == 1 && response.data_status == 'Active') {
                        facebookDialog.dialog('close');
                        jQuery("#dialog-content").html(SIGNUP_WITH_FB_BUT_REG);
                        jQuery('#dialog').dialog({
                            dialogClass: 'noTitleStuff',
                            resizable: false,
                            modal: true,
                            buttons: [
                                {
                                    text: "Login with facebook",
                                    "class": 'btn-custom-darkblue',
                                    click: function() {
                                        loginToFB(r);
                                    }
                                },
                                {
                                    text: "Cancel",
                                    "class": 'btn-custom-white2',
                                    click: function() {
                                        jQuery(this).dialog('close');
                                    }
                                }
                            ]
                        });
                    } else {
                        facebookDialog.dialog('close');
                        //save data to global variable
                        social_response = r;

                        $('.fb_separator').addClass('hide');
                        $('.li_separator').removeClass('hide');

                        $('#registration_type').val('FB');
                        $('#lname').val(r.last_name);
                        $('#fname').val(r.first_name);
                        $('#email').val(r.email);
                        $('#social_id').val(r.id);
                        $('#profile_pic').val(r.profile_pic);
                        return;




                        /*r.registration_type = 'FB';
                        r.promo_code = $('#promo_code').val();
                        var preferred_email = jQuery('<div id="preferred_dialog">\n\
                            <div align="center" style="">\n\
                                <label>Preferred email address: </label>\n\
                                <input class ="span11 textBoxGray" id="preferred_email" type = "text" value = "'+ r.email +'" />\n\
                            </div>\n\
                        </div>');

                        preferred_email.dialog({
                            dialogClass: 'noTitleStuff',
                            resizable: false,
                            autoOpen: true,
                            modal: true,
                            open: function( event, ui ) {
                                $(event.target).next().css('margin-top', '-20px');
                            },
                            buttons: [
                                {
                                    text: "Submit",
                                    "class": 'btn-custom-darkblue',
                                    click: function() {
                                        r.email = $('#preferred_email').val(),
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            url: $('#frmRegister').attr('action'),
                                            data: r,
                                            success: function(re){ 
                                                if (re.error != '') {
                                                    $('#errors-inner').html(re.error);
                                                    $('#validation-errors').dialog('open');
                                                    return;
                                                } else {
                                                    if (re.status == 'login') {
                                                        return loginToFB(r);
                                                    }

                                                    facebookDialog.dialog('close');
                                                    return window.location = re.location;
                                                }
                                            }
                                        });
                                    }
                                }
                            ]
                        });*/
                    }
                });
            }
            
            
            FB.getLoginStatus(function(response){
                if(response.status != 'connected') {
                    FB.login(function(response){
                        if(response.status == 'connected') {
                            propagateFacebook();
                        } else {
                            facebookDialog.dialog('close');
                        }
                    },{scope: 'publish_actions,email,user_birthday,publish_stream,read_stream,user_location,read_friendlists,friends_location,friends_birthday'});
                } else {
                    propagateFacebook();
                }
            });
        });
        
        <?php if (isset($_GET['fb_user_id'])) { ?>
            setTimeout(function(){ $('#Link_Facebook').trigger('click'); }, 1000);
        <?php } ?>

        <?php if (isset($_GET['linkedin'])) { ?>
            var linkedinConnect = function() {
                if (typeof(IN.User) == 'undefined') { 
                    setTimeout(function(){ return linkedinConnect(); }, 2000);
                } else {
                    liAuth();
                }         
            };

            linkedinConnect();
        <?php } ?>

        /* LINKEDIN ACCOUNT */
        function liInitOnload() {
            if (IN.User.isAuthorized()) {
                //NO NEED TO DO ANYTHING        
            }
        }
        function liAuth() {
            IN.User.authorize(function() {
                //If authorized and logged in
                var params = {
                    type: 'linked',
                    type_string: 'LinkedIn',
                    func: function(obj) {
                        return liCheckIfConnected(obj);
                    }
                };

                
                IN.API.Profile("me").fields().result(function(data) { });//bug??? do not remove; when remove it cannot be call again.
                processIntegratedApp(params);
            });
        }
        function processIntegratedApp(params) {
            var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
                    Connecting to ' + params.type_string + '...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
            </div></div>');

            connectDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 300,
                height: 120,
                show: '',
                hide: ''
            });

            connectDialog.dialog("open");
            params.func(connectDialog);
        }
        /* @todo remove ajax_post/processIntegratedApp controller function */
        function liCheckIfConnected(obj) {
            IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth", "location:(country:(code))").result(function(data) { 
                var hasActiveRecord = checkIfIntegrated({
                    id: data.values[0].id,
                    field: 'user_linkedin_id'
                });
                
                
                if( hasActiveRecord ) {
                    prompt(SIGNUP_WITH_FB_BUT_REG,'Login with LinkedIn','Cancel',
                        function(){
                            jQuery('#dialog').dialog('close').dialog('destroy');
                            obj.dialog('close');
                            IN.API.Raw("/people/~/picture-urls::(original)").result(function(r){
                                data.profile_pic = r.values[0];
                                return login_linkedIN(obj, data);
                            });
                        },
                        function(){
                            jQuery('#dialog').dialog('close').dialog('destroy');
                            obj.dialog('close');
                        }
                    );
                } else {
                    IN.API.Raw("/people/~/picture-urls::(original)").result(function(r){
                        obj.dialog('close');
                        var _data = data.values[0];
                        data.profile_pic = r.values[0];
                        //save data to global variable
                        social_response = data;
                        
                        $('.li_separator').addClass('hide');
                        $('.fb_separator').removeClass('hide');

                        $('#registration_type').val('LI');
                        $('#lname').val(_data.lastName);
                        $('#fname').val(_data.firstName);
                        $('#email').val(_data.emailAddress);
                        $('#social_id').val(_data.id);
                        $('#profile_pic').val(data.profile_pic);
                        return;
                    });
                }
            });
        }
        
        
        function login_linkedIN(obj, data) {
            obj.dialog('open');
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: GLOMP_BASE_URL + 'ajax_post/processIntegratedApp',
                data: {
                    id: data.values[0].id,
                    email: data.values[0].emailAddress,
                    field: 'user_linkedin_id',
                    mode: 'linkedIN',
                    device: 'desktop',
                    data: data.values[0]
                },
                success: function(res) {
                    if (res != false) {
                        if (res.status == 'register') {
                            obj.dialog('close');
                            var _data = data.values[0];
                            //save data to global variable
                            //social_response = r;
                            $('.li_separator').addClass('hide');
                            $('.fb_separator').removeClass('hide');

                            $('#registration_type').val('LI');
                            $('#lname').val(_data.lastName);
                            $('#fname').val(_data.firstName);
                            $('#email').val(_data.emailAddress);
                            $('#social_id').val(_data.id);
                            $('#profile_pic').val(data.profile_pic);
                            return;

                        } else {
                            if (res.post_to_url) {
                                /* The rest of this code assumes you are not using a library.*/
                                /* It can be made less wordy if you use one.*/
                                var form = document.createElement("form");
                                form.setAttribute("method", 'post');
                                form.setAttribute("action", res.redirect_url);

                                for (var key in res.data) {
                                    if (res.data.hasOwnProperty(key)) {
                                        var hiddenField = document.createElement("input");
                                        hiddenField.setAttribute("type", "hidden");
                                        hiddenField.setAttribute("name", key);
                                        hiddenField.setAttribute("value", res.data[key]);
                                        form.appendChild(hiddenField);
                                    }
                                }

                                document.body.appendChild(form);
                                obj.dialog('close');
                                return form.submit();
                            }
                            /* Email verified; Go to user dashboard */
                            obj.dialog('close');
                            return window.location = res.redirect_url;
                        }
                    }
                }
            });
                
        }
        
        function checkIfIntegrated(param) {
            var ret = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: 'ajax_post/checkIfIntegrated',
                data: {
                    id: param.id,
                    email: param.email,
                    field: param.field,
                },
                success: function(res) {
                    if (res) {
                        ret = true;
                    } else {
                        ret = false;
                    }
                }
            });

            return ret;
        }
        
    jQuery(window).load(function(){
        
        var prompt = jQuery('<div />');
        prompt.dialog({
            autoOpen:false,
            resizable:false,
            dialogClass:'dialog_style_error_alerts noTitleStuff ',
            modal:true,
            width:300,
            buttons:[
                {
                    text: 'Close',
                    class: 'btn-custom-white2',
                    click: function(){
                        prompt.html('');
                        prompt.dialog('close');
                    }
                }
            ]
        });
        
    });
        
    </script>
</body>
</html>