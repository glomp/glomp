<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $user_record->user_name; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
              "stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="screen" href="assets/frontend/css/als_demo.css" />  
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script language="javascript" type="text/javascript">
            var selectedTab;
            var merID;
<?php
if (isset($_GET['tab'])) {
    echo 'selectedTab="' . $_GET['tab'] . '";';
}
if (isset($_GET['merID']) && is_numeric($_GET['merID'])) {
    echo 'merID="' . $_GET['merID'] . '";';
}
?>
        </script>
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/custom.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery.als-1.2.min.js"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript">
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var inTour = "<?php echo $inTour; ?>";
            var favAdded = "<?php echo $favAdded; ?>";
            var loadingMoreStories=false;
            $(document).ready(function(e) {
<?php
$tweet_status = $this->session->flashdata('tweet_status');
$tweet_message = $this->session->flashdata('tweet_message');
if (!empty($tweet_status)) {
    echo "doTweetSucces('" . $tweet_status . "','" . $tweet_message . "');";
}
?>

                $('.remove').on({
                    mouseenter: function(e) {
                        e.stopPropagation();
                        var id = this.id;
                        $(".remove_" + id).show();

                        
                    }});
                $('.fav_item').on({
                    mouseenter: function(e) {
                        e.stopPropagation();
                        var id = this.id;
                        $(".remove_" + id).show();

                        
                    },
                    mouseleave: function(e) {
                        e.stopPropagation();
                        var id = this.id;
                        $(".remove_" + id).hide();
                        
                    }
                });





                if (inTour == 'yes' && favAdded == 'true') {
                    var NewDialog = $('<div id="tourDialog" align="center"> \
                                                                                        <p style="padding:15px 0px;font-weight:bold !important;">'+ $('#add_more_favourites').text() +'?</p> \
                                                                                </div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        resizable: false,
                        dialogClass: 'noTitleStuff',
                        title: '',
                        modal: true,
                        width: 320,
                        height: 140,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: $('#add_more').text() + "!",
                                "class": 'btn-custom-darkblue ',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#tourDialog').dialog('destroy').remove();
                                    }, 500);
                                }},
                            {text: $('#Return_to_Tour').text(),
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#tourDialog').dialog('destroy').remove();
                                        window.location.href = GLOMP_BASE_URL + 'user/dashboard/?inTour=3';
                                    }, 500);


                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                }
                else if (inTour == 'yes') {
                    var NewDialog = $('<div id="tourDialog_1" class="tourDialog" align="left"> \
                                        <div class="tour_arrow_up_outer" style=""><div class="tour_arrow_up_inner" style=""></div></div> \
                                                <div id="tourDialog_1_content" style="" class="tourDialog_1_content" align="center">' + $('#select_merchant_add_fav_item').text() + ' \
                                                <div style="padding:20px 0px;" id="tour_button_wrapper_1" align="center"> \
                                                </div> \
                                        </div>');
                    
                    NewDialog.dialog({
                        position: {
                            my: 'left+100 top+150',
                            at: 'left top',
                            of: $('#tabBuz')
                        },
                        dialogClass: 'noButtonAndTitleStuff',
                        autoOpen: false,
                        resizable: false,
                        modal: false,
                        width: 300,
                        height: 140,
                        show: 'clip',
                        hide: 'clip',
                    });
                    $elem = $('<button type="button" class="btn-custom-darkblue " style="width:150px !important;"  id="" >'+ $('#Ok').text() +'</button>');
                    $elem.on('click', function(e) {
                        $('#tourDialog_1').dialog("close");
                        setTimeout(function() {
                            $('#tourDialog_1').dialog('destroy').remove();
                        }, 500);
                    });
                    $("#tour_button_wrapper_1").append($elem);
                    NewDialog.dialog('open');
                }
                $("#lista1").als({
                    visible_items: 6,
                    scrolling_items: 1,
                    orientation: "horizontal",
                    circular: "yes",
                    autoscroll: "no",
                    interval: 5000,
                    direction: "right",
                    start_from: 0
                });



                $(".buzzScroll").nanoScroller();
                $(".buzzScroll").bind("scrollend", function(e){                    
                    if(!loadingMoreStories)
                    {
                    
                        loadingMoreStories=true;            
                        start_count=$('#glomp_buzz_content').children().last().data().id;
                        if(start_count>0)
                        {                        
                            $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">'+ $('#Loading_more_stories').text() +'<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                            $('#glomp_buzz_content').append($loader);
                            data ='start_count='+start_count+'&profile_id='+<?php echo $buzz_user_id;?>;
                            setTimeout(function() {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'html',
                                    url: 'profile/getMoreStories',
                                    data: data,
                                    success: function(response){ 
                                        if(response!='' &&  response!="<!---->")
                                        {
                                             $('#id_loader').remove();
                                            $('#glomp_buzz_content').append(response);
                                            $(".buzzScroll").nanoScroller();            
                                            loadingMoreStories=false;
                                            $('.glomp_fb_share').unbind('hover');
                                            $('.glomp_fb_share').hover(        
                                                function(e){
                                                    $this=$(this);
                                                    $data=$this.data();                          
                                                    
                                                    if($data.voucher_id in share_wrapper)
                                                    {
                                                     
                                                        clearTimeout( share_wrapper[$data.voucher_id] );                
                                                        delete share_wrapper[$data.voucher_id];
                                                        
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                    
                                                    
                                                    $('.glomp_fb_share').each(function(){
                                                        $this_temp=$(this);
                                                        $data_temp=$this_temp.data();                 
                                                        if($data_temp.voucher_id!=$data.voucher_id){                    
                                                            $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                                                            clearTimeout( share_wrapper[$data.voucher_id] );                
                                                            delete share_wrapper[$data.voucher_id];
                                                        }
                                                        else
                                                        {
                                                            /*$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); */
                                                        }
                                                    });
                                                    $('#share_wrapper_box_'+$data.voucher_id).fadeIn();
                                                     e.stopPropagation();
                                                },
                                                function(){                
                                                    $this=$(this);
                                                    $data=$this.data();   
                                                    if($data.voucher_id in share_wrapper)
                                                    {            
                                                        share_wrapper[$data.voucher_id]= setTimeout(function() {
                                                                            console.log($data.voucher_id);
                                                                            $('#share_wrapper_box_'+$data.voucher_id).fadeOut();
                                                                            delete share_wrapper[$data.voucher_id];                                    
                                                                        }, 500);								
                                                    }            
                                                }
                                            );
                                        }
                                        else
                                        {                                
                                            $('#id_loader').remove();
                                            $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">'+ $('#no_more_stories_to_load').text() +'.</div>');
                                            $('#glomp_buzz_content').append($no_more);
                                        }
                                    }
                                });
                            }, 1);
                        }
                        
                        
                        /*console.log($('#glomp_buzz_content').children().last().data().id);*/
                        /*xx=1;*/
                    }                       
                });
                $("body").click(function(e) {
                    $('.profile .fev .popUp').hide();
                    /*
                    this block of code affects the events binded to 'document'
                    some elements must be anchored on the document to bind events
                    because they are custom generated.
                    # i.e: jQuery(document).on('click','.element',function(){});
                    e.stopPropagation();
                    */
                });


                $(".displyFevPopUp").on('click', function(e) {
                    var read_popup = $(this).find('.getPopupDescription').html();
                    var position = $(this).position();
                    
                    if (position.left > 280)
                        position = 300;
                    else
                        position = position.left;
                    $('.profile .fev .popUp').show().html(read_popup);
                    $('.profile .fev .popUp').css({'margin-left': position + 'px'});
                    e.stopPropagation();
                }).on('mouseleave', function() {
                    $('.profile .fev .popUp').hide();
                });

                $('.remove').click(function() {
                    if (confirm('<?php echo $this->lang->line('Fev_item_del_conformation', 'Are you sure you want to delete this favourite Item?') ?>'))
                    {
                        $(this).parent('li').append("<div class='removeing'><?php echo $this->lang->line('Removing', 'Removing...'); ?></div>");
                        var _this = $(this);
                        $.ajax({
                            type: "GET",
                            url: 'profile/removeFevItem/' + $(this).attr('id'),
                            cache: false,
                            success: function(html) {
                                if (html == 'success') {
                                    _this.parent('li').fadeOut(600);
                                    window.location = '<?php echo base_url('profile') ?>';
                                }
                            }
                        });
                    }
                });
                $('.addFev1').on('click', function() {
                    var _this = $(this);
                    var product_id = _this.data('product_fev_id');
                    _this.parents('.displyPopUp').find(".popUp").hide();
                    _this.parent('.thumbnail').prepend("<div class='addingFev'><?php echo $this->lang->line('Adding', 'Adding...'); ?></div>");

                    var activeTab = $('.profileTab .activeTab').attr('href');
                    activeTab = activeTab.replace('#', '');
                    $.ajax({
                        type: "GET",
                        url: 'profile/addFevitemToFevList/' + product_id,
                        cache: false,
                        success: function(html) {
                            if (html == 'success') {
                                var search_url = window.location.search;
                                if (search_url == '') {
                                    search_url = '?';
                                }
                                else{
                                    search_url += '&';
                                }
                                window.location.href = '<?php echo base_url('profile/index/') ?>/' + search_url + 'favAdded=true';
                                _this.fadeOut(2000);
                                $('.addingFev').fadeOut(2000);

                            }
                        }
                    });
                })
            });
        </script>
        <script src="assets/frontend/js/custom.glomp_share.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".tabs").hide();
                $(".tabs:first").show();
                $("#glomped_inactive").hide();
                $(".profileTab a").click(function(e) {
                    var activeTab = $(this).data().href;
                    if (activeTab == "#tabBuz")
                    {
                        $("#glomped_active").show();
                        $("#glomped_inactive").hide();
                    }
                    else
                    {
                        $("#glomped_inactive").show();
                        $("#glomped_active").hide();
                    }

                    e.preventDefault();
                    $(".profileTab a").removeClass("activeTab");
                    $(this).addClass("activeTab");
                    $(".tabs").hide();
                    $(activeTab).fadeIn();
                });

                $('.nameOfMerchant').click(function(e) {
                    e.preventDefault();
                    var _anchor = $(this).attr('href');
                    $('#merchantProductDetails').load(_anchor, function() {
                        $('#tabMerchant').hide();
                        $('#merchantProductDetails').fadeIn();
                    });



                });
            });
            $("document").ready(function()
            {

                
                if (selectedTab != '') {
                    $("#" + selectedTab).trigger('click');
                }
                if (merID != '') {
                    $("#merID_" + merID).trigger('click');
                }
            });
        </script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
                                    <div class="row-fluid">
                                        <div class="userProfileLeft">
                                            <div class="profilePicture" >
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div style=" height:150px;width:150px; overflow:hidden; border:0px solid;position:relative">
                                                            <?php
                                                            $profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic, $user_record->user_gender);
                                                            ?>
                                                            <img src="<?php echo $profile_pic; ?>"     alt="<?php echo $user_record->user_name; ?>" style="height:150px;width:150px">
                                                        </div>
                                                    </div>
                                                    <div class="span6" style="padding-left:5px;">
                                                        <div style="height:138px;">
                                                            <div class="name"><?php echo $user_record->user_name; ?></div>
                                                            <?php
                                                            $dob = explode('-', $user_record->user_dob);
                                                            if (count($dob) !== 3) {
                                                                $dob[0] = 0;
                                                                $dob[1] = 0;
                                                                $dob[2] = 0;
                                                            }

                                                            $dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
                                                            if ($user_record->user_dob_display == 'dob_display_wo_year') {
                                                                $dob = date('M d', strtotime($dob));
                                                            } else {
                                                                $dob = date('M d, Y', strtotime($dob));
                                                            }
                                                            if($user_record->user_dob!="")
                                                            {
                                                            ?>
                                                                <div class="dob"><?php echo $this->lang->line('DOB', 'DOB'); ?>: <?php echo $dob; ?>
                                                                </div>
                                                            <?php }?> 
                                                            <div class="address"><?php
                                                                echo $this->regions_m->region_name($user_record->user_city_id);
                                                                ?></div>
                                                        </div>
                                                        <div class="addAsFriens">
                                                            <?php
                                                            $profile_id = $user_record->user_id;
                                                            //echo anchor(base_url()."user/update/",ucwords($this->lang->line('Update_Details')),'class="btn-custom-green-big"');
                                                            ?>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="userProfileRight">
                                            <?php
                                            $resFev = $this->product_m->userFevProduct($profile_id);
                                            $febCount = $resFev->num_rows();
                                            ?>
                                            <div class="fev">
                                                <div class="title">
                                                    <div class="fl"><?php echo $this->lang->line('My_Favourite', 'My favourites'); ?></div>
                                                    <div style="margin-left: 3px;margin-top: -4px;"class="info_tooltip_profile fl" rel="popover" data-content="<?php echo $this->lang->line('help_myprofile_favourite', 'Let your friends know which products you enjoy most by selecting from your menu below!'); ?>" >
                                                        <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <?php if ($febCount > 0) { ?>
                                                    <div id="lista1" class="als-container">
                                                        <div class="popUp">do no delete this div</div>
                                                        <?php if ($febCount > 6) { ?><span class="als-prev"><img src="assets/frontend/img/thin_left_arrow_333.png" alt="" title="" /></span><?php } ?>

                                                        <div class="als-viewport">
                                                            <ul class="als-wrapper">
                                                                <?php
                                                                foreach ($resFev->result() as $recFev) {
                                                                    if( in_array( $recFev->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
                                                                    $merchantName = $this->merchant_m->merchantName($recFev->prod_merchant_id);
                                                                    ?>
                                                                    <li class="als-item displyFevPopUp">
                                                                        <div class="getPopupDescription">
                                                                            <div class="row-fluid">
                                                                                <div class="span5">
                                                                                    <img src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>">
                                                                                </div>
                                                                                <div class="span7">
                                                                                    <div class="name1"><?php echo $merchantName; ?></div>
                                                                                    <div class="name2"><?php echo stripslashes($recFev->prod_name); ?></div>
                                                                                    <?php /* ?><div class="name2">KFC Kings Burger</div><?php */ ?>
                                                                                    <p><?php echo stripslashes($recFev->prod_details); ?></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>                        
                                                                        <div style="display:none" class="remove remove_<?php echo $recFev->prod_id; ?>" id="<?php echo $recFev->prod_id; ?>"><img src="assets/frontend/img/glomp_minus.png" alt="" title="" /></div>

                                                                        <img id="<?php echo $recFev->prod_id; ?>" class="fav_item" src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>" >
                                                                        <p class="productName"><b><?php echo $merchantName . "<br>" . stripslashes($recFev->prod_name); ?></b></p>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>



                                                        <?php if ($febCount > 6) { ?><span class="als-next"><img src="assets/frontend/img/thin_right_arrow_333.png" alt="" title="" /></span> <?php } ?>

                                                    </div>
                                                <?php
                                                } else {
                                                    ?>
                                                    <div class="alert">
                                                    <?php /* ?> <button type="button" class="close" data-dismiss="alert">&times;</button><?php */ ?>
                                                        <strong><?php echo $user_record->user_name; ?> <?php echo $this->lang->line('has_not_added_favourite', 'has not added any favourite items.'); ?>
                                                    </div>
<?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12" style="margin-top:-3px;">

                                            <div class="profileTab">
                                                <ul>
                                                    <li ><a href="javascript:void(0);" data-href="#tabBuz" id="tabBuz_" class="activeTab"  style="width:121px;">  <span id="glomped_inactive"><img src="assets/frontend/img/glomp_buzz.png" alt="Glomped"> </span>
                                                            <span id="glomped_active"> <img src="assets/frontend/img/buzz_active.png" alt="Glomped"> </span> </a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);"  id="tbBrands"  data-href="#tabBrands"><?php echo ucfirst($this->lang->line('brands','Brands')); ?></a></li>
                                                    <li><a href="javascript:void(0);" id="tbMerchant" data-href="#tabMerchant"  class="perTabClass" ><?php echo ucfirst($this->lang->line('merchants', 'merchants')); ?></a></li>
                                                    <li><a href="javascript:void(0);" data-href="#tabDrink" id="tabDrink_" class="perTabClass" ><?php echo ucfirst($this->lang->line('drinks', 'drinks')); ?></a></li>
                                                    <li><a href="javascript:void(0);" data-href="#tabSnaks" id="tabSnaks_" class="perTabClass" ><?php echo ucfirst($this->lang->line('snacks', 'snacks')); ?></a></li>
                                                    <?php if(!$this->user_login_check->underage()):?>
                                                    <li><a href="javascript:void(0);" data-href="#tabCocktails" id="tabCocktails_" class="perTabClass" ><?php echo ucfirst($this->lang->line('cocktails', 'cocktails')); ?></a></li>
                                                    <?php endif;?>
                                                    <li><a href="javascript:void(0);" data-href="#tabSweets" id="tabSweets_" class="perTabClass" ><?php echo ucfirst($this->lang->line('sweets', 'sweets')); ?></a></li>
                                                    <li><a href="javascript:void(0);" data-href="#tabOthers" id="tabOthers_" class="perTabClass" ><?php echo ucfirst($this->lang->line('others', 'others')); ?></a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>





<?php
/* user glomp buzz */
?>
                                            <div class="tabContent tabs" id="tabBuz">
                                                <div class="row-fluid">
                                                    <div class="span8">
                                                        <div class="buzPan tabInnerContent">
                                                            <div class="buzzScroll nano">
                                                                <div class="content"  id="glomp_buzz_content" >
<?php
if ($glom_buzz_count > 0) {
    $base_url = base_url();
    $to_a = $this->lang->line('to_a');
    foreach ($glomp_buzz->result() as $glomp) {

        //product and merchant info
        $prod_id = $glomp->voucher_product_id;
        $prod_info = json_decode($this->product_m->productInfo($prod_id));
        if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
        $prod_image = $prod_info->product->$prod_id->prod_image;
        $product_logo = $this->custom_func->product_logo($prod_image);
        $prod_name = $prod_info->product->$prod_id->prod_name;
        $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
        $merchant_name = $prod_info->product->$prod_id->merchant_name;
        $merchant_id = $prod_info->product->$prod_id->merchant_id;
        
        $from_id = $glomp->voucher_purchaser_user_id;
        $to_id = $glomp->voucher_belongs_usser_id;
        $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
        $ago_date = $glomp->voucher_purchased_date;

        $form_name = $frn_info->friends->$from_id->user_name;
        $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

        $to_name = $frn_info->friends->$to_id->user_name;
        $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);


        $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
        $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
        
        $from_li_id = $frn_info->friends->$from_id->user_linkedin_id;
        $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;

        $story_type = "1";
        if ($from_id != $this->session->userdata('user_id')) {
            $story_type = "2";
        }
        ?>

                                                                            <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">
                                                                                <div class="span7">
                                                                                    <div class="row-fluid">
                                                                                        <div class="span10">
                                                                                            <?php
                                                                                                $sender = '<a href="'.site_url().'profile/view/'.$from_id.'">'.$form_name.'</a>';
                                                                                                if( $from_id == 1 ) {
                                                                                                    $sender = '<a href="javascript:void(0);">'.$form_name.'</a>';
                                                                                                }

                                                                                                echo $this->lang->compound(
                                                                                                    'glomp_buzz',
                                                                                                    array(
                                                                                                        'sender'   => $sender,
                                                                                                        'reciever' => '<a href="'.base_url().'profile/view/'.$to_id.'">'.$to_name.'</a>',
                                                                                                        'merchant' => $merchant_name,
                                                                                                        'product'  => $prod_name
                                                                                                    ),
                                                                                                    "{sender} <strong>glomp!ed</strong> {reciever} to a {merchant} {product}" );
                                                                                            ?>
                                                                                            <div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
                                                                                        </div>
                                                                                        <div class="span2" align="right">                    
                                                                                            <div style="margin-top:28px;" >
                                                                                                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs" data-voucher_id="<?php echo $glomp->voucher_id; ?>" ><?php echo $this->lang->line('glomp_share', "share"); ?></button>
                                                                                                <div style="height:0px;">
                                                                                                    <div id="share_wrapper_box_<?php echo $glomp->voucher_id; ?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id; ?>" style="" align="center">
                                                                                                        <button data-merchant="<?php echo $merchant_name; ?>"  data-belongs="<?php echo $to_name; ?>" data-purchaser="<?php echo $form_name; ?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg'); ?>'); background-size: 21px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id; ?>" data-to_fb_id="<?php echo $to_fb_id; ?>" data-story_type="<?php echo $story_type; ?>" data-prod_name="<?php echo $prod_name; ?>" data-prod_img="<?php echo $product_logo; ?>"  data-voucher_id="<?php echo $glomp->voucher_id; ?>"  ></button>
                                                                                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                                                                        <button title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg'); ?>'); background-size: 21px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="profile" data-voucher_id="<?php echo $glomp->voucher_id; ?>"  ></button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> 


                                                                                </div>
                                                                                <div class="span5">
        <?php if ($from_id == 1) { ?>
                                                                                        <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
                                                                                    <?php
                                                                                    } else {
                                                                                        ?>
                                                                                        <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"> <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>
                                                                                    <?php } ?>
                                                                                    <img src="assets/frontend/img/glomp_arrow_red.jpg" />
                                                                                    <a href="<?php echo base_url('profile/view/'.$to_id.'?tab=tbMerchant&merID='.$merchant_id);?>">
                                                                                        <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                                                                                    </a>
                                                                                    <img src="assets/frontend/img/glomp_arrow_red.jpg" />
                                                                                    <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
                                                                                </div>
                                                                            </div> 

        <?php
        $start_count++;
    }//foeach close
}//numb count close
else {
    ?>

                                                                        <div class="alert alert-info">
                                                                            <p><?php echo $this->lang->line('coming_soon') ?></p>
                                                                        </div>

    <?php }
?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<?php
/* user glomp buzz end */
?>




                                            <?php # mechant tab?>
                                            <div class="tabContent tabs" id="tabMerchant"> 
                                                <div class="tabInnerContent">

                                            <?php
                                            $num_merchant = $region_wise_merchant->num_rows();
                                            if ($num_merchant > 0) {
                                                $i = 1;
                                                $j = 1;
                                                foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                                    if ($i == 1) {
                                                        ?> <ul class="thumbnails"> <?php } ?>
                                                                <li class="span2 merchantShadow">
                                                                    <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>" href="<?php echo base_url("profile/merchantProductlist/" . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                                                        <div class="thumbnail">			
                                                                            <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>" style="width:90px !important;">
                                                                        </div>
                                                                    </a>
                                                                </li>
        <?php if ($i == 6 || $j == $num_merchant) { ?></ul>
            <?php
        }
        $j++;
        if ($i == 6) {
            $i = 1;
        }
        else
            $i++;
    } /// end of foreach
}//enf of num result check
?>

                                                </div>
                                            </div>
                                                    <?php # merchant tab close ?>

                                            <div id="tabBrands" class="tabs" style="background:#343239">
                                                <?php include('user_profile_tab_brand_v.php')?>
                                            </div>
                                            
                                            <div class="tabContent tabs" id="tabDrink">
                                                <div class="tabInnerContent">
                                                    <?php
                                                    $data_v['profile_region_id'] = $user_record->user_city_id;
                                                    $data_v['cat_id'] = 1;
                                                    $this->load->view('profile_tab/own_profile_cat_tab_v', $data_v)
                                                    ?>
                                                </div>  
                                            </div>   
                                            <div class="tabContent tabs" id="tabSnaks">
                                                <div class="tabInnerContent">
                                                    <?php
                                                    $data_v['cat_id'] = 2;
                                                    $this->load->view('profile_tab/own_profile_cat_tab_v', $data_v)
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="tabContent tabs" id="tabCocktails">
                                                <div class="tabInnerContent">
                                                    <?php
                                                    $data_v['cat_id'] = 3;
                                                    $this->load->view('profile_tab/own_profile_cat_tab_v', $data_v)
                                                    ?>
                                                </div> 
                                            </div> 
                                            <div class="tabContent tabs" id="tabSweets">
                                                <div class="tabInnerContent">
                                                    <?php
                                                    $data_v['cat_id'] = 4;
                                                    $this->load->view('profile_tab/own_profile_cat_tab_v', $data_v)
                                                    ?>
                                                </div>  
                                            </div>

                                            <div class="tabContent tabs" id="tabOthers">
                                                <div class="tabInnerContent">
<?php
$data_v['cat_id'] = 5;
$this->load->view('profile_tab/own_profile_cat_tab_v', $data_v)
?>
                                                </div>   
                                            </div>
                                            <div class="tabContent tabs" id="merchantProductDetails">
<?php // to load merchant ?>
                                            </div>                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id ="gl_languages" style="display:none;">
            <span id="add_more_favourites"><?php echo $this->lang->line('add_more_favourites', "Add more Favourites"); ?></span>
            <span id="add_more"><?php echo $this->lang->line('add_more', "Add more"); ?></span>
            <span id="Return_to_Tour"><?php echo $this->lang->line('Return_to_Tour', "Return to Tour"); ?></span>
            <span id="select_merchant_add_fav_item"><?php echo $this->lang->line('select_merchant_add_fav_item', "Select a merchant and add your favourite item"); ?></span>
            <span id="Ok"><?php echo $this->lang->line('Ok', "Ok"); ?></span>
            <span id="Loading_more_stories"><?php echo $this->lang->line('Loading_more_stories', "Loading more stories..."); ?></span>
            <span id="no_more_stories_to_load"><?php echo $this->lang->line('no_more_stories_to_load', "No more stories to load"); ?></span>
        </div>
    </div>
</body>
</html>