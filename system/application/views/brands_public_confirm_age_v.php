<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
          <title>Brands - glomp.it</title>
          <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="description" content=" glomp!." >
        <meta name="keywords" content="<?php echo meta_keywords('profile');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <base href="<?php echo base_url(); ?>" />
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->

        <link rel="stylesheet" type="text/css" media="screen" href="assets/frontend/css/als_demo.css" />        
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet">
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery.als-1.2.min.js"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
		<script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>	
        <script src="assets/frontend/js/custom.glomp_share.js" type="text/javascript"></script>
		
		<script src="assets/frontend/js/validate.js" type="text/javascript"></script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
				
				
				<div class="innerBorder">
                                <div class="register">
                                    <div class="innerPageTitleRed glompFont"></div>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="content">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<h1 style=""><?php echo $this->lang->line('Please_fil_up_the_form_below', 'Please fill-up the form below to continue accessing this page...'); ?></h1>						
                                                        <div style="float:right;margin-top:-35px;font-size:14px;font-weight:bold;">
                                                                <!--<input id="take_start_tour" type="checkbox" style="margin-top:-2px;"  />&nbsp;Take Start Tour-->
                                                        </div>
                                                    </div>
                                                </div>               
                                                <form action="" name="frmRegister" class="form-horizontal" id="frmRegister" enctype="multipart/form-data" method="post" accept-charset="utf-8">
												
												<?php
												echo (validation_errors())? '
													<div class="alert alert-danger" role="alert">
													  '.validation_errors().'
													</div>':''; 
												?>
												
                                                <div class="row-fluid">
                                                    <div class="span6">

                                                         <div class="control-group">
                                                            <label class="control-label" for="fname"><?php echo $this->lang->line('fname', 'First Name'); ?> <span class="required">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text"  id="fname" name="fname" value="<?php echo $this->input->post('fname');?>" class="span11 textBoxGray" >
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="lname"><?php echo $this->lang->line('lname', 'Last Name'); ?> <span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input type="text"  id="lname" name="lname" value="<?php echo $this->input->post('lname');?>" class="span11 textBoxGray" >
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth', 'Date of Birth'); ?> <span class="required">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" id="day" name="day" maxlength="2" value="<?php echo $this->input->post('day');?>"  class="span2 textBoxGray" placeholder="<?php echo $this->lang->line('profile_day', 'DD'); ?>"> /
                                                                <input type="text" id="months" name="months" value="<?php echo $this->input->post('months');?>" maxlength="2"  class="span2 textBoxGray" placeholder="<?php echo $this->lang->line('profile_month', 'MM'); ?>"> /
                                                                <input type="text" id="year" name="year" maxlength="4" value="<?php echo $this->input->post('year');?>"  class="span3 textBoxGray" placeholder="<?php echo $this->lang->line('profile_year', 'YYYY'); ?>">
                                                            </div>
                                                        </div>
                                                        

                                                    </div>

                                                  
                                                </div>

													<div class="row-fluid">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label class="control-label" for="email"><?php echo $this->lang->line('E-mail', 'Email'); ?> <span class="required">*</span></label>
                                                                <div class="controls">
                                                                    <input type="text"  id="email" value="<?php echo $this->input->post('email');?>" name="email" class="span11 textBoxGray" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                  


                                                    <div class="row-fluid">
                                                        <div class="span12 border">

                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <button type="submit" class="btn-custom-reg" style="padding:6px 40px !important" name="update_profile">Submit</button>
                                                        </div>


                                                        <div class="span5">
                                                            <button type="reset" onclick="redirect()" class="btn-custom-reset" style="padding:6px 40px !important" name="Reset">Cancel</button>							   
                                                        </div>
                                                    </div>



                                                    


                                                </div></form>
                                            </div>



                                        </div>



                                    </div>
                                </div>
                            </div>
							
							
               
            </div>
        </div>
    </body>
</html>