<?php
$resReg = $this->regions_m->region_name_only();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Search Friends | Glomp</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/nanoscroller.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            var GLOMP_SITE_URL = '<?php echo site_url(); ?>';
            var linkedIn = false;
<?php
if ($resReg->num_rows() > 0) {
    $reg_list = "";
    foreach ($resReg->result() as $recReg) {
        $reg_list .= '"' . $recReg->region_name . '"' . ",";
    }
    $reg_list = rtrim($reg_list, ",")
    ?>
                var all_location_array = [<?php echo strtolower($reg_list); ?>];

    <?php
}
?>
        </script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".searchUser").nanoScroller();

                $(".addAsFriend").on('click', function(e) {
                    e.preventDefault();

                    var width = $(this).find(".addAsFriendPopUp").width();

                    var _position = $(this).data("position");


                    if (_position == 1)
                        _x = 100;
                    else if (_position == 2)
                        _x = -20;
                    else if (_position == 3)
                        _x = -140;
                    else if (_position == 4)
                        _x = -260;

                    $(this).find(".addAsFriendPopUp").css({"margin-left": _x});
                    $('.addAsFriendPopUp').hide(100);
                    $(this).find(".addAsFriendPopUp").show();

                    e.stopPropagation();
                    return false;
                });
                /*/$('body').click(function(){ $('.popUp').hide(100);});*/
                $('._hide').click(function() {
                    $('.popUp').hide(100);
                });

                $('.addFren').on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).html("<i class=\"icon-ok\"></i> Friend");
                    $.ajax({
                        type: "GET",
                        url: 'profile/add_friend2/' + $(this).attr('id'),
                        cache: false,
                        success: function(html) {
                            if (html == 'success') {
                            }
                        }

                    });


                })

                /*/add_friend2*/
            });
        </script>
    </head>
    <body>
		<?php include_once("includes/analyticstracking.php") ?>
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="middleBorder topPaddingZero">
                        <div class="row-fluid searchPageHeader" >
                            <h1 class="glompFont"><?php echo $this->lang->line('Potential_Matches'); ?></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span7">

                                <div class="searchFriends">


                                    <div class="searchUser nano searchForFriendPanel">
                                        <div class="content">
                                            <?php
                                            $num_found_result=0;
                                            if($resFriendList!="")
                                                $num_found_result = $resFriendList->num_rows();
                                            if ($num_found_result > 0) {
                                                ?>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="alert alert-success">
                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong> <?php echo $this->lang->line('are_you_looking_for_these_persons'); ?></strong>
                                                        </div>
                                                        <?php
                                                        $i = 1;
                                                        $j = 1;
                                                        foreach ($resFriendList->result() as $recFriendList) {
                                                            if ($i == 1) {
                                                                ?> <ul class="thumbnails"> <?php } ?>

                                                                <li class="span4 addAsFriend" data-position="<?php echo $i; ?>">
                                                                    <div class="addAsFriendPopUp"><h1><?php echo $this->lang->line('add_friend'); ?></h1>

                                                                        <div class="thumbHolder"><img src="<?php echo $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_fname; ?>" ></div>
                                                                        <div class="nameHolder">
                                                                            <div class="nameHolderInner">
                                                                                <?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?> <br /> <?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></div>
                                                                            <div class="userInfo">
                                                                                Add <?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?>  to your glomp! network.?
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="btns">

                                                                            <form name="form<?php echo $recFriendList->user_id ?>" method="post" action="">
                                                                                <button type="button" class="btn-custom-gray" name="Invite" onClick="javascript:location.href = '<?php echo base_url("profile/view/" . $recFriendList->user_id); ?>'" ><?php
                                                                                echo
                                                                                ucfirst($this->lang->line('View_Profile'))
                                                                                ?></button>



                                                                                <button type="button" class="btn-custom-gray addFren" id="<?php echo $recFriendList->user_id ?>"><?php
                                                                                    echo
                                                                                    ucfirst($this->lang->line('Yes'))
                                                                                    ?></button>


                                                                                <button type="button" class="btn-custom-white _hide" name="Invite" ><?php
                                                                                    echo
                                                                                    ucfirst($this->lang->line('Close'))
                                                                                    ?></button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div class="friendThumb">
                                                                        <a href="<?php echo base_url("profile/view/" . $recFriendList->user_id); ?>">
                                                                            <div class="thumbnail">
                                                                                <img src="<?php echo $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_fname; ?>" >
                                                                                <div class="thumbUserName"><?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?> <br /> <?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </li>

                                                            <?php if ($i == 3 || $j == $resFriendList->num_rows()) { ?></ul>
                                                                <?php
                                                            }
                                                            $j++;
                                                            if ($i == 3) {
                                                                $i = 1;
                                                            }
                                                            else
                                                                $i++;
                                                        } /// end of foreach($r->result() as $p)
                                                        ?>														
                                                    </div>
													<div style="margin:30px;">&nbsp;</div>
                                                </div>
                                                <?php
                                            }
                                            else {
                                                ?>

                                                <div class="alert alert-error">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <?php echo $this->lang->line('No_result_found'); ?>
                                                </div>

<?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span5">                      
                                <?php include('includes/invite_friends_2.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>