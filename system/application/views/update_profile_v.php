<?php
/* to check user agent*/
$ua = $this->custom_func->browser_info();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $user_record->user_name; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/nanoscroller.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/css/jcrop/jquery.Jcrop.css" rel="stylesheet">        		

        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <style type="text/css">
            .profile_error {
                border: 1px solid red;
            }
        </style>
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/jquery.Jcrop.min.js"></script>		
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/validate.register.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>		
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>

        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered', 'Oops! Looks like you have already registered an account here.  Please login instead.'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var importOnly = "";
            
            function liInitOnload() {
                if (IN.User.isAuthorized()) {
                    /*NO NEED TO DO ANYTHING*/
                }
            }
            function redirect()
            {
                location.href = '<?php echo base_url('user/dashboard'); ?>';
            }
            function location_textKeyUp()
            {
                $('#location_id').val("");
            }
            function importFromFacebook() {
                importOnly = "yes";
                if (fbApiInit)
                {
                }
                else {
                    FB.init({
                        appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                        status: true, /* check login status*/
                        cookie: true, /* enable cookies to allow the server to access the session*/
                        xfbml: true  /* parse XFBML	  		*/
                    });
                    fbApiInit = true;
                }
                FB.getLoginStatus(function(response) {
                    if (response.status == "connected")
                    {
                        getUserDetailsAndConfirm();
                    }
                    else
                    {
                        FB.login
                                (
                                        function(response)
                                        {
                                            if (response.status === 'connected') {
                                                getUserDetailsAndConfirm();
                                            }
                                        }, {scope: 'publish_actions,email'}
                                );
                    }
                });
            }



            $(document).ready(function(e) {
                <?php
                    if (validation_errors() != "" || isset($error_msg) || isset($uploadEerror)) 
                    {
                        echo 'var NewDialog =$("#register_error_from_load");
                        NewDialog.dialog({						
                            autoOpen: false,
                            closeOnEscape: false ,
                            resizable: false,					
                            dialogClass:"dialog_style_error_alerts ",
                            modal: true,
                            title:"",
                            position: "center",
                            width:400,                                
                            buttons:[
                                {text: "'. $this->lang->line('Close', "Close") .'",
                                "class": "btn-custom-white2",
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $("#waitPopup").dialog("destroy").remove();
                                    }, 500);
                                }}
                            ]

                        });
                        NewDialog.dialog("open");';
                    }
                ?>

                /* highlight incomplete */
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    url: 'user2/welcome_incomplete_profile',
                    success: function(r) {
                        if (! r.data.user_dob)  $('#day, #months, #year').addClass('_error');
                        if (! r.data.user_password)  $('#current_pword').addClass('_error');
                        if (! r.data.location)  $('#location').addClass('_error');
                        if (! r.data.profile_pic)  $('#profile_pic').addClass('profile_error');
                        return false;
                    }
                });
              

                $('body').click(function() {
                    $('.sutoSugSearch').hide();
                });
                $('#profilePhotoUpload').on('change', function(){ UpdatePreviewSample(); });
                $('#uploadFilePic').click(function(e) {

                    /*$('#profilePhotoUpload').click();*/
                    /*$('#upload_loader').html('Please wait...<i class="icon-spinner"></i>');*/
                    e.preventDefault();
                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        resizable: false,
                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                        title: '',
                        modal: true,
                        width: 270,
                        height: 100,
                        show: '',
                        hide: '',
                        buttons: [
                            {text: "<?php echo $this->lang->line('edit_thumbnail', 'Edit Thumbnail'); ?>",
                                "class": 'btn-custom-darkblue w220px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    editThumbnail();
                                }},
                            {text: "<?php echo $this->lang->line('upload_from_device', 'Upload from device'); ?>",
                                "class": 'btn-custom-darkblue w220px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    $('#profilePhotoUpload').click();
                                }},
                            {text: "<?php echo $this->lang->line('import_from_facebook', 'Import from Facebook'); ?>",
                                "class": 'btn-custom-blue w220px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    
                                    importFromFacebook();
                                }},
                            {text: "<?php echo $this->lang->line('import_from_linkedin', 'Import from LinkedIn'); ?>",
                                "class": 'btn-custom2-007BB6 w220px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    
                                    /*Import pic only*/
                                    linkLinkedInAccount(true);
                                }},
                            {text: "<?php echo $this->lang->line('Cancel', 'Cancel'); ?>",
                                "class": 'btn-custom-white2 w220px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                }}
                        ]
                    });
                    NewDialog.dialog('open');

                });


                $('#day').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#months').focus();
                    }
                });

                $('#months').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#year').focus();
                    }
                });
                $('#year').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#gender-male').focus();
                    }
                });
                $('#take_start_tour').on('change', function(e) {
                    var stat = $(this).prop('checked');
                    if (stat)
                        stat = 'Y';
                    else
                        stat = 'N';
                    data = '&tour_stat=' + stat;
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: 'ajax_post/setTourStat',
                        data: data,
                        success: function(response) {
                        }
                    });
                });
                
                $('#terminate_account').click(function(e)
                {
                    var NewDialog = $('<div id="deactivatePopup" align="left" style="color:">\
                    <h1 style="font-size:16px;margin:0px;padding:0px;;color:#f00">Are you sure you want to deactivate your account?</h1>\
                    <br>\
                    <div class="row-fluid" style="font-size:12px;">\
                        <div class="span3">\
                            <b>Reason for leaving</b><br><small class="red">(required)</small>\
                        </div>\
                        <div class="span9">\
                            <label class="radio inline" for="reason_1"><input id="reason_1" name="reason" type="radio" value="1" />I don\'t understand how to use glomp!</label><br>\
                            <label class="radio inline" for="reason_2"><input id="reason_2" name="reason" type="radio" value="2" />This is temporary. I\'ll be back.</label><br>\
                            <label class="radio inline" for="reason_3"><input id="reason_3" name="reason" type="radio" value="3" />I have a privacy concern.</label><br>\
                            <label class="radio inline" for="reason_4"><input id="reason_4" name="reason" type="radio" value="4" />I don\'t find glomp! useful.</label><br>\
                            <label class="radio inline" for="reason_5"><input id="reason_5" name="reason" type="radio" value="5" />I have another glomp! account.</label><br>\
                            <label class="radio inline" for="reason_6"><input id="reason_6" name="reason" type="radio" value="6" />Other, please explain further:</label><br>\
                        </div>\
                    </div>\
                    <br>\
                    <div class="row-fluid" style="font-size:12px;">\
                        <div class="span3">\
                            <b>Please explain further</b>\
                        </div>\
                        <div class="span9">\
                            <textarea id="comment" id= rows="4"  style="resize:none;width:280px;" ></textarea>\
                        </div>\
                    </div>\
                    </div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                        title: $('#Please_wait').text(),
                        modal: true,
                        position: 'center',
                        width: 500,
                        height: 400,
                        buttons: [
                            {text: "Deactivate",
                                "class": 'btn-custom-blue-grey_normal',
                                click: function() {
                                    
                                    var reason = $('input:radio[name=reason]:checked').val();
                                    if(reason == undefined)
                                    {
                                        var NewDialog =$('<div><div style="font-size:12px;" class="_error  alert alert-error" >Please select a reason.</div></div>');
                                        NewDialog.dialog({						
                                            autoOpen: false,
                                            closeOnEscape: false ,
                                            resizable: false,					
                                            dialogClass:'dialog_style_blue_grey noTitleStuff ',
                                            modal: true,
                                            title:$('#Please_wait').text(),
                                            position: 'center',
                                            width:400,
                                            height:120,
                                            buttons:[
                                                {text: $('#Close').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 500);
                                                }}
                                            ]
                                            
                                        });

                                        NewDialog.dialog('open');
                                        return;
                                    }
                                    
                                    /*deactivate confirm popup*/
                                    var NewDialog = $('<div id="deactivateConfirmPopup" align="center">Are you sure you want to deactivate your account?</div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                        title: '',
                                        modal: true,
                                        position: 'center',
                                        width: 280,
                                        height: 120,
                                        buttons: [
                                            {text: "Deactivate Now",
                                                "class": 'btn-custom-red-normal',
                                                click: function() {
                                                    /**deactivate now */
                                                    var reason = $('input:radio[name=reason]:checked').val();
                                                    var comment = $('#comment').val();
                                                    var data = 'reason='+reason+'&comment='+encodeURIComponent(comment);
                                                    
                                                    $('#deactivateConfirmPopup').dialog("close");
                                                    setTimeout(function() {
                                                        $('#deactivatePopup').dialog("close");
                                                        $('#terminatePopup').dialog('destroy').remove();
                                                        $('#deactivatePopup').dialog('destroy').remove();
                                                    }, 10);
                                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                                       <div align="center" style="margin-top:5px;">Please wait...<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                                    NewDialog.dialog({
                                                        autoOpen: false,
                                                        closeOnEscape: false,
                                                        resizable: false,
                                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                                        title: 'Please wait...',
                                                        modal: true,
                                                        position: 'center',
                                                        width: 200,
                                                        height: 120
                                                    });
                                                    NewDialog.dialog('open');
                                                    /**/
                                                    $.ajax({
                                                        type: "POST",
                                                        dataType: 'json',
                                                        data:'deactivate=deactivate&'+data,
                                                        
                                                        url: GLOMP_BASE_URL + 'ajax_post/deactivateAccount',
                                                        success: function(response) {
                                                            if(response.status=='Deactivated')
                                                            {
                                                                window.location.href =GLOMP_BASE_URL+'landing/logout/deactivated';
                                                            }
                                                        
                                                        }
                                                    });
                                                    /*deactivate now*/
                                                }
                                            },
                                            {text: "Cancel",
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#deactivateConfirmPopup').dialog('destroy').remove();
                                                    }, 10);
                                                }
                                            }
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                    /*deactivate confirm popup*/
                                }
                            },
                            {text: "Cancel",
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#deactivatePopup').dialog('destroy').remove();
                                    }, 10);
                                }
                            }
                        ]
                    });
                    NewDialog.dialog('open'); 
                    
                    /*
                    var NewDialog = $('<div id="terminatePopup" align="center">Are you sure you want to terminate your account?</div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                        title: '',
                        modal: true,
                        position: 'center',
                        width: 280,
                        height: 120,
                        buttons: [
                            {text: "Yes",
                                "class": 'btn-custom-blue-grey_normal',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#terminatePopup').dialog('destroy').remove();
                                    }, 500);
                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                       <div align="center" style="margin-top:5px;">Please wait...<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_error_alerts',
                                        title: 'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width: 200,
                                        height: 120
                                    });
                                    NewDialog.dialog('open');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        data:'deactivate=deactivate',
                                        
                                        url: GLOMP_BASE_URL + 'ajax_post/deactivateAccount',
                                        success: function(response) {
                                            if(response.status=='Deactivated')
                                            {
                                                window.location.href =GLOMP_BASE_URL+'landing/logout/deactivated'
                                            }
                                        
                                        }
                                    });
                                    
                                }
                            },
                            {text: "No",
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#terminatePopup').dialog('destroy').remove();
                                    }, 500);
                                }
                            }
                        ]
                    });
                    NewDialog.dialog('open');
                    
                    /***/
                });

            });

        </script>

        <script type="text/javascript">
            fbApiInit = false;
            window.fbAsyncInit = function() {
                FB.init({
                    appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                    status: true, /* check login status*/
                    cookie: true, /* enable cookies to allow the server to access the session*/
                    xfbml: true  /* parse XFBML*/
                });
                fbApiInit = true;
            };
            /* Load the SDK asynchronously*/
            (function(d) {
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
            $().ready(function() {
                /* validate signup form on keyup and submit*/

                $("#Link_Facebook").click(function(e) {
                    if (fbApiInit)
                    {
                    }
                    else {
                        FB.init({
                            appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                            status: true, /* check login status*/
                            cookie: true, /* enable cookies to allow the server to access the session*/
                            xfbml: true  /* parse XFBML	  		*/
                        });
                        fbApiInit = true;
                    }

                    FB.login
                            (
                                    function(response)
                                    {
                                        if (response.status === 'connected') {
                                            getUserDetailsAndConfirm();
                                        }
                                    }, {scope: 'publish_actions,email'}
                            );
                });
                $("#frmUpdate").validate({
                    rules: {
                        fname: "required",
                        lname: "required",
                        day: {
                            required: true,
                            minlength: 2,
                            maxlength: 2,
                            number: true,
                            range: [01, 31]
                        },
                        months: {
                            required: true,
                            minlength: 2,
                            maxlength: 2,
                            number: true,
                            range: [01, 12]
                        },
                        year: {
                            required: true,
                            minlength: 4,
                            maxlength: 4,
                            number: true,
                            range: [1900, 3000]
                        },
                        gender: {required: false},
                        location_text: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        /*current_pword:"required",*/
                    }
                    ,
                    messages: {
                        fname: "Incorrect or missing entry. Please re-enter the red boxes.",
                        lname: "Incorrect or missing entry. Please re-enter the red boxes.",
                        day: "Incorrect or missing entry. Please re-enter the red boxes.",
                        months: "Incorrect or missing entry. Please re-enter the red boxes.",
                        year: "Incorrect or missing entry. Please re-enter the red boxes.",
                        gender: "Incorrect or missing entry. Please re-enter the red boxes.",
                        location_text: "Incorrect or missing entry. Please re-enter the red boxes.",
                        email: "Incorrect or missing entry. Please re-enter the red boxes.",
                        current_pword: "Incorrect or missing entry. Please re-enter the red boxes."
                   },
                    invalidHandler: function(e, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            var message = errors == 1 ? 'You missed 1 field. It has been highlighted below' : 'You missed ' + errors + ' fields.  They have been highlighted below';
                            /*$("div.error span").html(message);
                            $( "#dialog-form" ).dialog( "open" );
                            */
                            var NewDialog =$('#register_error');
                            NewDialog.dialog({						
                                autoOpen: false,
                                closeOnEscape: false ,
                                resizable: false,					
                                dialogClass:'dialog_style_error_alerts ',
                                modal: true,
                                title:$('#Please_wait').text(),
                                position: 'center',
                                width:400,                                
                                buttons:[
                                    {text: $('#Close').text(),
                                    "class": 'btn-custom-white2',
                                    click: function() {
                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#waitPopup').dialog('destroy').remove();
                                        }, 500);
                                    }}
                                ]
                                
                            });

                            NewDialog.dialog('open');
                        }
                    },
                    onkeyup: false,
                    onfocusout:false
                });
            });



            var jcrop_api;

            /* The function is pretty simple*/
            function UpdatePreviewSample(_this)
            {
                asdasd();
            }
            function initJcrop()/*{{{*/
            {
                jcrop_api = $.Jcrop($('#jCrop_target'), {
                    onSelect: updateCoords,
                    onChange: updateCoords,
                    onRelease: updateCoords
                });
                jcrop_api.setOptions({
                    setSelect: [0, 140, 140, 0],
                    minSize: [140, 140],
                    aspectRatio: 1 / 1
                });
                jcrop_api.animateTo([140, 140, 0, 0]);
            }
            function updateCoords(c)
            {
                $('#x').val(c.x);
                $('#y').val(c.y);
                $('#w').val(c.w);
                $('#h').val(c.h);
            }
            ;
            function resetCoords()
            {
                $('#x').val(0);
                $('#y').val(0);
                $('#w').val(140);
                $('#h').val(140);
            }
            ;
            /* for profile picture	*/
            function asdasd()
            {
                var form = $('#frmUpdate');
                $('#waitPopup').dialog("close");
                $('#waitPopup').dialog('destroy').remove();
                var NewDialog = $('<div id="waitPopup" align="center">\
                                                                                <div align="center" style="margin-top:5px;">'+ $('#Please_wait').text() +'<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title: $('#Please_wait').text(),
                    modal: true,
                    position: 'center',
                    width: 200
                });
                NewDialog.dialog('open');

                $(form).ajaxSubmit({
                    type: "post",
                    url: GLOMP_BASE_URL + 'ajax_post/upload_temp',
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(response) {
                        if (response.status == 'Error') {
                            $('#waitPopup').dialog("close");
                            $('#waitPopup').dialog('destroy').remove();
                            var NewDialog = $('<div id="waitPopup" align="center">\
                                                                                <h4 style="padding:0px;margin:0px;">Error</h4>\
                                                                                <div align="center" style="margin-top:5px;">' + response.message + '</div></div>');
                            NewDialog.dialog({
                                autoOpen: false,
                                closeOnEscape: false,
                                resizable: false,
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                title: $('#Please_wait').text(),
                                modal: true,
                                position: 'center',
                                buttons: [
                                    {text: $('#Cancel').text(),
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $(this).dialog("close");
                                            setTimeout(function() {
                                                $('#waitPopup').dialog('destroy').remove();
                                            }, 500);
                                        }}
                                ]


                            });
                            NewDialog.dialog('open');

                        }
                        else {
                            $("#tempImg").prop("src", GLOMP_BASE_URL + "custom/uploads/users/temp/" + response.img);
                            $('#tempImg').waitForImages(function() {
                                $('#waitPopup').dialog("close");
                                $('#waitPopup').dialog('destroy').remove();
                                UpdatePreview(response);
                            });
                            /*
                             $("#profile_pic").prop("src", "custom/uploads/users/temp/"+response.thumb);
                             $("#profile_pic_latest").val(response.thumb);
                             $("#profile_pic_orig").val(response.orig);
                             $('#waitPopup').dialog("close");
                             setTimeout(function() {
                             $('#waitPopup').dialog('destroy').remove();
                             },100);
                             */
                        }
                    }
                });
            }
            function  UpdatePreview(target) {
                /* if IE < 10 doesn't support FileReader tempImg                      
                alert(target.result);
                $("#profile_pic").prop("src", target.result);
                $("#jCrop_target").prop("src", ($("#profile_pic").prop("src")));																		
                $("#tempImg").prop("src", GLOMP_BASE_URL+"custom/uploads/users/temp/"+target.img);	*/
                var tempW = target.w;
                var tempH = target.h;
                setTimeout(function()
                {
                    if (((parseInt(tempW) < 140))
                            || ((parseInt(tempH) < 140)))
                    {
                        $('#waitPopup').dialog("close");
                        $('#waitPopup').dialog('destroy').remove();
                        var NewDialog = $('<div id="waitPopup" align="center">\
                                        <h4 style="padding:0px;margin:0px;">'+ $('#Error').text() +'</h4>\
                                        <div align="center" style="margin-top:5px;font-size:14px">'+ $('#Image_too_small').text()+ '</div></div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                            title: $('#Please_wait').text(),
                            modal: true,
                            position: 'center',
                            width: 300,
                            height: 140,
                            buttons: [
                                {text: $('#Ok').text(),
                                    "class": 'btn-custom-white2',
                                    click: function() {
                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#waitPopup').dialog('destroy').remove();
                                        }, 500);
                                    }}
                            ]


                        });
                        NewDialog.dialog('open');
                    }
                    else
                    {
                        var popW = 300;
                        var popH = 300;
                        if ((parseInt(tempW + 120) * 1) > parseInt(popW))
                        {
                            popW = tempW + 120;

                        }
                        if ((parseInt(tempH + 80) * 1) > parseInt(popH))
                        {
                            popH = tempH + 80;

                        }
                        if (popW > 1200)
                            popW = 1200;
                        if (popH > 800)
                            popH = 800;

                        /*create a popup for jcrop*/
                        /*create a popup for jcrop*/
                        $('#jCropDialog').dialog('destroy').remove();
                        var NewDialog = $('<div id="jCropDialog" align="center">\
                                                <img style="width:' + tempW + 'px;height' + tempH + ':px" id="jCrop_target" alt="" class="fixed-size" />\
                                            </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                            title: $('#Please_wait').text(),
                            modal: true,
                            position: 'center',
                            width: popW,
                            height: popH,
                            buttons: [
                                {text: $('#Crop').text(),
                                    "class": 'btn-custom-blue',
                                    click: function() {
                                        var form = $('#frmUpdate');
                                        $('#jCropDialog').dialog("close");
                                        var NewDialog = $('<div id="waitPopup" align="center">\
                                                                    <div align="center" style="margin-top:5px;">'+ $('#Please_wait').text() +'<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                        NewDialog.dialog({
                                            autoOpen: false,
                                            closeOnEscape: false,
                                            resizable: false,
                                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                            title: $('#Please_wait').text(),
                                            modal: true,
                                            position: 'center',
                                            width: 200,
                                            height: 120
                                        });
                                        NewDialog.dialog('open');

                                        $(form).ajaxSubmit({
                                            type: "post",
                                            url: GLOMP_BASE_URL + 'ajax_post/crop_register',
                                            dataType: 'json',
                                            data: $(form).serialize(),
                                            success: function(response) {
                                                if (response.status == 'Error') {
                                                    $('#waitPopup').dialog("close");
                                                    $('#waitPopup').dialog('destroy').remove();
                                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                                    <h4 style="padding:0px;margin:0px;">'+$('#Error').text()+'</h4>\
                                                                    <div align="center" style="margin-top:5px;">' + response.message + '</div></div>');
                                                    NewDialog.dialog({
                                                        autoOpen: false,
                                                        closeOnEscape: false,
                                                        resizable: false,
                                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                        title: $('#Please_wait').text(),
                                                        modal: true,
                                                        position: 'center',
                                                        width: 200,
                                                        height: 120,
                                                        buttons: [
                                                            {text: $('#Cancel').text(),
                                                                "class": 'btn-custom-white2',
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    setTimeout(function() {
                                                                        $('#waitPopup').dialog('destroy').remove();
                                                                    }, 500);
                                                                }}
                                                        ]


                                                    });
                                                    NewDialog.dialog('open');
                                                }
                                                else {
                                                    $("#profile_pic").prop("src", "custom/uploads/users/temp/" + response.thumb);
                                                    $("#profile_pic_latest").val(response.thumb);
                                                    $("#profile_pic_orig").val(response.orig);
                                                    $('#waitPopup').dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 100);
                                                }
                                                $('#profilePhotoUpload').remove();                                                
                                                $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden" id="profilePhotoUpload" />');
                                                $('#profilePhotoUpload').on('change', function(){ UpdatePreviewSample(); });
                                            }
                                        });
                                    }},
                                {text: $('#Cancel').text(),
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $(this).dialog("close");                                                            
                                            $("#profilePhotoUpload").val("");
                                            setTimeout(function() {
                                                $('#waitPopup').dialog('destroy').remove();
                                            }, 500);                                
                                            
                                            $('#profilePhotoUpload').remove();                                                
                                            $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden" id="profilePhotoUpload" />');
                                            $('#profilePhotoUpload').on('change', function(){ UpdatePreviewSample(); });
                                        }}
                            ]
                        });
                        NewDialog.dialog('open');
                        $('#jCrop_target').load(function() {
                            initJcrop();
                            resetCoords();
                        });
                        $("#jCrop_target").prop("src", GLOMP_BASE_URL + "custom/uploads/users/temp/" + target.img);
                    }
                }, 100);
            }


            function editThumbnail() {
                var NewDialog = $('<div id="waitPopup_blue" align="center"><div align="center" style="margin-top:5px;">'+ $('#Please_wait').text()+ '<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'dialog_style_blue_grey noTitleStuff',
                    title: $('#Please_wait').text(),
                    modal: true,
                    position: 'center',
                    width: 200,
                    height: 120
                });
                NewDialog.dialog('open');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: GLOMP_BASE_URL + 'ajax_post/getOriginalImage',
                    data: '',
                    success: function(response) {
                        if (response.status == 'Error') {
                            $('#waitPopup_blue').dialog("close");
                            $('#waitPopup_blue').dialog('destroy').remove();
                            var NewDialog = $('<div id="waitPopup" align="center">\
                                                                        <h4 style="padding:0px;margin:0px;">Error</h4>\
                                                                        <div align="center" style="margin-top:5px;">' + response.message + '</div></div>');
                            NewDialog.dialog({
                                autoOpen: false,
                                closeOnEscape: false,
                                resizable: false,
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                title: $('#Please_wait').text(),
                                modal: true,
                                position: 'center',
                                width: 200,
                                height: 120,
                                buttons: [
                                    {text: $('#Cancel').text(),
                                        "class": 'btn-custom-white2',
                                        click: function() {
                                            $(this).dialog("close");
                                            setTimeout(function() {
                                                $('#waitPopup').dialog('destroy').remove();
                                            }, 500);
                                        }}
                                ]


                            });
                            NewDialog.dialog('open');

                        }
                        else {
                            $("#tempImg").prop("src", GLOMP_BASE_URL + "custom/uploads/users/" + response.data);


                            $('#tempImg').waitForImages(function() {
                                $('#waitPopup_blue').dialog("close");
                                $('#waitPopup_blue').dialog('destroy').remove();


                                var tempW = response.w;
                                var tempH = response.h;


                                if (((parseInt(tempW) < 140))
                                        || ((parseInt(tempH) < 140)))
                                {
                                    $('#waitPopup').dialog("close");
                                    $('#waitPopup').dialog('destroy').remove();
                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                <h4 style="padding:0px;margin:0px;">Error</h4>\
                                                <div align="center" style="margin-top:5px;font-size:14px">'+ $('#Image_too_small').text() +'</div></div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                        title: $('#Please_wait').text(),
                                        modal: true,
                                        position: 'center',
                                        width: 300,
                                        height: 140,
                                        buttons: [
                                            {text: $('#Ok').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 500);
                                                }}
                                        ]


                                    });
                                    NewDialog.dialog('open');
                                }
                                else {
                                    var popW = 300;
                                    var popH = 300;
                                    if ((parseInt(tempW + 120) * 1) > parseInt(popW))
                                    {
                                        popW = tempW + 120;

                                    }
                                    if ((parseInt(tempH + 80) * 1) > parseInt(popH))
                                    {
                                        popH = tempH + 80;

                                    }

                                    if (popW > 1200)
                                        popW = 1200;
                                    if (popH > 800)
                                        popH = 800;

                                    /*/create a popup for jcrop*/
                                    /*/create a popup for jcrop*/
                                    $('#jCropDialog').dialog('destroy').remove();
                                    var NewDialog = $('<div id="jCropDialog" align="center">\
                                                                                                        <img style="width:' + tempW + 'px;height' + tempH + ':px" id="jCrop_target" class="fixed-size" src="" alt="" />\
                                                                                                </div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: true,
                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                        title: $('#Please_wait').text(),
                                        modal: true,
                                        position: 'center',
                                        width: popW,
                                        height: popH,
                                        buttons: [
                                            {text: $('#Crop').text(),
                                                "class": 'btn-custom-blue',
                                                click: function() {
                                                    var form = $('#frmUpdate');
                                                    $('#jCropDialog').dialog("close");
                                                    var NewDialog = $('<div id="waitPopup2" align="center">\
                                                                                                                                                <div align="center" style="margin-top:5px;">'+ $('#Please_wait').text() +'<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                                    NewDialog.dialog({
                                                        autoOpen: false,
                                                        closeOnEscape: false,
                                                        resizable: false,
                                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                        title: $('#Please_wait').text(),
                                                        modal: true,
                                                        position: 'center',
                                                        width: 200,
                                                        height: 120
                                                    });
                                                    NewDialog.dialog('open');

                                                    $(form).ajaxSubmit({
                                                        type: "post",
                                                        url: GLOMP_BASE_URL + 'ajax_post/crop_update',
                                                        dataType: 'json',
                                                        data: $(form).serialize(),
                                                        success: function(response) {
                                                            if (response.status == 'Error') {
                                                                $('#waitPopup2').dialog("close");
                                                                $('#waitPopup2').dialog('destroy').remove();
                                                                var NewDialog = $('<div id="waitPopup" align="center">\
                                                                                                                                                <h4 style="padding:0px;margin:0px;">' + $('#Error').text() +'</h4>\
                                                                                                                                                <div align="center" style="margin-top:5px;">' + response.message + '</div></div>');
                                                                NewDialog.dialog({
                                                                    autoOpen: false,
                                                                    closeOnEscape: false,
                                                                    resizable: false,
                                                                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                                    title: $('#Please_wait').text(),
                                                                    modal: true,
                                                                    position: 'center',
                                                                    width: 200,
                                                                    height: 120,
                                                                    buttons: [
                                                                        {text: $('#Cancel').text(),
                                                                            "class": 'btn-custom-white2',
                                                                            click: function() {
                                                                                $(this).dialog("close");
                                                                                setTimeout(function() {
                                                                                    $('#waitPopup').dialog('destroy').remove();
                                                                                }, 500);
                                                                            }}
                                                                    ]


                                                                });
                                                                NewDialog.dialog('open');

                                                            }
                                                            else {
                                                                $("#profile_pic").prop("src", "custom/uploads/users/temp/" + response.thumb);
                                                                $("#profile_pic_latest").val(response.thumb);
                                                                $("#profile_pic_orig").val(response.orig);
                                                                $('#waitPopup2').dialog("close");
                                                                setTimeout(function() {
                                                                    $('#waitPopup2').dialog('destroy').remove();
                                                                }, 100);
                                                            }
                                                        }
                                                    });
                                                }},
                                            {text: $('#Cancel').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 500);
                                                }}
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                    setTimeout(function() {
                                        $('#jCrop_target').load(function() {
                                            initJcrop();
                                            resetCoords();
                                        });
                                        $("#jCrop_target").prop("src", GLOMP_BASE_URL + "custom/uploads/users/" + response.data);
                                        /*/create a popup for jcrop	*/
                                        /*$('#jCrop_target').waitForImages(function() {
                                            
                                        });*/
                                    }, 100);
                                    /*/create a popup for jcrop	*/
                                }
                            });
                        }
                    }
                });

            }
            
            function linkLinkedInAccount(importOnly, auth) {
                var importOnly = false || importOnly;
                var auth = false || auth;
                
                try {
                    IN.User.isAuthorized();
                }
                catch(err) {
                    var NewDialog = $('<div id="messageDialog" align="center"> \
                        <p style="padding:15px 0px;">'+ $('#LinkedIn_error_1').text() + '</p> \
                    </div>');
                                                
                    NewDialog.dialog({
                        autoOpen: false,
                        resizable: false,
                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                        title: '',
                        modal: true,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: $('#Ok').text(),
                                "class": 'btn-custom-white2',
                                click: function() {
                                    window.location = window.location.href;
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                    return;
                }
                
                if ( ! auth) {
                    IN.User.authorize(function() {
                        return linkLinkedInAccount(importOnly, true);
                    });
                    return;
                }
                        
                /*If authorized and logged in*/
                IN.API.Profile("me").fields("id", "email-address", "picture-urls::(original)").result(function(data) {
                    params = {
                        id: data.values[0].id,
                        email: data.values[0].emailAddress,
                        pic: data.values[0].pictureUrls.values[0],
                        importOnly: '',
                    };
                    
                    if (importOnly) {
                        params.importOnly = 'yes';
                    }
                    
                    var NewDialog = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">'+ $('#Please_wait').text() +'<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                        title: $('#Please_wait').text(),
                        modal: true,
                        position: 'center',
                        width: 200,
                        height: 120
                    });
                    NewDialog.dialog('open');
                    
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: GLOMP_BASE_URL + 'ajax_post/linkThisLinkedInAccount',
                        data: params,
                        success: function(response) {
                            $('#waitPopup').dialog("close");
                            $('#waitPopup').dialog('destroy').remove();

                            if (response.status == "Ok")
                            {
                                if (response.data == 'importOnly') {
                                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                        <p style="padding:15px 0px;">' + response.message + '</p> \
                                                    </div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                        title: '',
                                        modal: true,
                                        width: 350,
                                        height: 160,
                                        show: 'clip',
                                        hide: 'clip',
                                        buttons: [
                                            {text: $('#Ok').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = document.URL;
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();

                                                    }, 500);
                                                }}
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                }
                                else
                                {
                                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                        <p style="padding:15px 0px;">' + response.message + '</p> \
                                                    </div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                        title: '',
                                        modal: true,
                                        width: 350,
                                        height: 160,
                                        show: 'clip',
                                        hide: 'clip',
                                        buttons: [
                                            {text: $('#Invite').text(),
                                                "class": 'btn-custom-blue',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getLinkedIn=true';
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();
                                                    }, 500);
                                                }},
                                            {text: $('#Close').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = document.URL;
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();

                                                    }, 500);
                                                }}
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                }

                            }
                            else {
                                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                    <p style="padding:15px 0px;">' + response.message + '</p> \
                                                </div>');
                                NewDialog.dialog({
                                    autoOpen: false,
                                    resizable: false,
                                    dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                    title: '',
                                    modal: true,
                                    width: 350,
                                    height: 160,
                                    show: 'clip',
                                    hide: 'clip',
                                    buttons: [
                                        {text: $('#Close').text(),
                                            "class": 'btn-custom-white2',
                                            click: function() {
                                                $(this).dialog("close");
                                                setTimeout(function() {
                                                    $('#messageDialog').dialog('destroy').remove();
                                                }, 500);
                                            }}
                                    ]
                                });
                                NewDialog.dialog('open');
                            }
                        }
                    });	/*ajax*/                    
                });
            }
            
            
            function getUserDetailsAndConfirm() {
                FB.api('/me', function(response) {
                    var output = '';
                    var data = '&importOnly=' + importOnly;
                    var ii = 0;
                    for (property in response) {
                        output += property + ': ' + response[property] + '; ';

                        data += '&' + property + '=' + (response[property]) + '';

                    }
                    for (property in response.hometown) {
                        output += property.hometown + ': ' + response.hometown[property] + '; ';
                        data += '&' + property.hometown + '=' + response.hometown[property] + '';
                    }
                    console.log(output);
                    console.log(data);
                    /*post_to_url((document.URL),response,'post')*/
                    /*console.log('Good to see you, ' + response.id + '::'+response.email );*/
                    var NewDialog = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">'+ $('#Please_wait').text() +'<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                        title: $('#Please_wait').text(),
                        modal: true,
                        position: 'center',
                        width: 200,
                        height: 120
                    });
                    NewDialog.dialog('open');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: GLOMP_BASE_URL + 'ajax_post/linkThisFacebookAccount',
                        data: data,
                        success: function(response) {
                            $('#waitPopup').dialog("close");
                            $('#waitPopup').dialog('destroy').remove();

                            if (response.status == "Ok")
                            {
                                if (response.data == 'importOnly') {
                                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                                                                                <p style="padding:15px 0px;">' + response.message + '</p> \
                                                                                                        </div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                        title: '',
                                        modal: true,
                                        width: 350,
                                        height: 160,
                                        show: 'clip',
                                        hide: 'clip',
                                        buttons: [
                                            {text: $('#Ok').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = document.URL;
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();

                                                    }, 500);
                                                }}
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                }
                                else
                                {
                                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                                                                                <p style="padding:15px 0px;">' + response.message + '</p> \
                                                                                                        </div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                        title: '',
                                        modal: true,
                                        width: 350,
                                        height: 160,
                                        show: 'clip',
                                        hide: 'clip',
                                        buttons: [
                                            {text: $('#Invite').text(),
                                                "class": 'btn-custom-blue',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = GLOMP_BASE_URL + 'user/searchFriends/?getFb=true';
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();
                                                    }, 500);
                                                }},
                                            {text: $('#Close').text(),
                                                "class": 'btn-custom-white2',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    window.location.href = document.URL;
                                                    setTimeout(function() {
                                                        $('#messageDialog').dialog('destroy').remove();

                                                    }, 500);
                                                }}
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                }

                            }
                            else {
                                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                                                                        <p style="padding:15px 0px;">' + response.message + '</p> \
                                                                                                </div>');
                                NewDialog.dialog({
                                    autoOpen: false,
                                    resizable: false,
                                    dialogClass: 'dialog_style_blue_grey noTitleStuff',
                                    title: '',
                                    modal: true,
                                    width: 350,
                                    height: 160,
                                    show: 'clip',
                                    hide: 'clip',
                                    buttons: [
                                        {text: $('#Close').text(),
                                            "class": 'btn-custom-white2',
                                            click: function() {
                                                $(this).dialog("close");
                                                setTimeout(function() {
                                                    $('#messageDialog').dialog('destroy').remove();
                                                }, 500);
                                            }}
                                    ]
                                });
                                NewDialog.dialog('open');
                            }

                        }
                    });	/*ajax*/
                });
            }
            function linkFbAccount() {

            }
        </script>
    </head>
    <body>
<?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>
<?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="innerBorder">
                                <div class="register">
                                    <div class="innerPageTitleRed glompFont"><?php echo $this->lang->line('setting', 'Settings'); ?></div>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="content">
                                                <div class="row-fluid">
<?php
$take_start_tour = '';
if ($user_record->user_start_tour == 'Y')
    $take_start_tour = ' checked ';
?>
                                                    <div class="span12">						
                                                        <h1 style=""><?php echo $this->lang->line('Personal_Details', 'Personal Details'); ?></h1>						
                                                        <div style="float:right;margin-top:-35px;font-size:14px;font-weight:bold;">
                                                                <!--<input id="take_start_tour" type="checkbox" style="margin-top:-2px;" <?php echo $take_start_tour; ?> />&nbsp;Take Start Tour-->
                                                        </div>
                                                    </div>
                                                </div>               
                                                <?php echo form_open_multipart('user/update/', 'name="frmUpdate" class="form-horizontal" id="frmUpdate"') ?>

                                                <?php
                                                if (isset($success_msg)) {
                                                    echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $success_msg . "</div>";
                                                }                                                
                                                ?>
                                                <div style="display:none"> 
                                                <div class="" id="register_error_from_load">
                                                    <?php
                                                    if (validation_errors() != "" || isset($error_msg) || isset($uploadEerror)) 
                                                    {
                                                    ?>
                                                        <div class="alert alert-error">
                                                        <?php
                                                        if (validation_errors() != "") {
                                                            echo validation_errors();
                                                        }
                                                        if (isset($error_msg)) {
                                                            echo $error_msg;
                                                        }
                                                        if (isset($uploadEerror)) {
                                                            echo $uploadEerror;
                                                        }
                                                        ?>

                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                </div>
                                                <div class="" id="register_error" style=""></div>
                                                <div class="row-fluid">
                                                    <div class="span6">

                                                        <div class="control-group">
                                                            <label class="control-label" for="fname"><?php echo $this->lang->line('fname', 'First Name'); ?> <span class="required">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text"  id="fname" name="fname" value="<?php echo $user_record->user_fname; ?>" class="span11 textBoxGray" >
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="lname"><?php echo $this->lang->line('lname', 'Last Name'); ?> <span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input type="text"  id="lname" name="lname" value="<?php echo $user_record->user_lname; ?>" class="span11 textBoxGray" >
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <?php
                                                            $dob = explode('-', $user_record->user_dob);
                                                            if (count($dob) !== 3) {
                                                                $dob[0] = '';
                                                                $dob[1] = '';
                                                                $dob[2] = '';
                                                            }
                                                            ?>
                                                            <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth', 'Date of Birth'); ?> <span class="required">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" id="day" name="day" maxlength="2" value="<?php echo $dob[2]; ?>"  class="span2 textBoxGray" placeholder="<?php echo $this->lang->line('profile_day', 'DD'); ?>"> /
                                                                <input type="text" id="months" name="months" value="<?php echo $dob[1]; ?>" maxlength="2"  class="span2 textBoxGray" placeholder="<?php echo $this->lang->line('profile_month', 'MM'); ?>"> /
                                                                <input type="text" id="year" name="year" maxlength="4" value="<?php echo $dob[0]; ?>"  class="span3 textBoxGray" placeholder="<?php echo $this->lang->line('profile_year', 'YYYY'); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <?php
                                                            /*TODO: CHANGE THIS QUICK FIX*/
                                                            if (empty($user_record->user_dob_display)) {
                                                                $user_record->user_dob_display = 'dob_display_w_year';
                                                            }
                                                            ?>
                                                            <label class="control-label" for="display">Display</label>
                                                            <div class="controls">
                                                                <label class="radio inline">
                                                                    <input type="radio" name="dob_display" id="dob_display_w_year" value="dob_display_w_year" <?php echo $checked = ($user_record->user_dob_display == 'dob_display_w_year') ? 'Checked="checked"' : ""; ?> /> DD / MM / YYYY
                                                                </label>
                                                                <label class="radio inline">
                                                                    <input type="radio" name="dob_display" id="dob_display_wo_year" <?php echo $checked = ($user_record->user_dob_display == 'dob_display_wo_year') ? 'Checked="checked"' : ""; ?> value="dob_display_wo_year"> DD / MM
                                                                </label>
                                                            </div>
                                                        </div>








                                                        <div class="control-group">
                                                            <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender', 'Gender'); ?> </label>
                                                            <div class="controls">
                                                                <label class="radio inline">
                                                                    <input type="radio" name="gender" id="gender-male" value="Male" <?php echo $checked = ($user_record->user_gender == 'Male') ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Male', 'Male'); ?>
                                                                </label>
                                                                <label class="radio inline">
                                                                    <input type="radio" name="gender" id="gender-female" <?php echo $checked = ($user_record->user_gender == 'Female') ? 'Checked="checked"' : ""; ?> value="Female"> <?php echo $this->lang->line('Female', 'Female'); ?>
                                                                </label>
                                                            </div>
                                                        </div>



                                                        <?php
                                                        if (isset($_POST['location_text']) && isset($error_msg)) {
                                                            $location_text = $_POST['location_text'];
                                                            $location_id = '';
                                                        } else {
                                                            $location_id = $user_record->user_city_id;
                                                            $location_text = $this->regions_m->getCountryName($user_record->user_city_id);
                                                        }
                                                        ?>
                                                        <div class="control-group">
                                                            <label class="control-label" for="location"><?php echo $this->lang->line('Location', 'Location'); ?><span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input class="textBoxGray" onKeyUp="location_textKeyUp()" type="text" name="location_text" id="location" value="<?php echo $location_text; ?>" style="width:250px"  autocomplete="off" placeholder="">
                                                                <input type="hidden" name="location" id="location_id" value="<?php echo $location_id; ?>" autocomplete="off" class="" >
                                                                <div class="sutoSugSearch" id="autoSugSearch" style="color:#59606B !important;margin-left:20px;width:244px">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <?php
                                                        /**
                                                        this is a temporary solution for adding language selection for users
                                                        @todo options must be taken from the database and listed here
                                                        */
                                                        ?>
                                                        <div class="control-group">
                                                            <label class="control-label"><?php echo $this->lang->line('Language_Preference', 'Default Language'); ?> <span class="required">*</span> </label>
                                                            <div class="controls">
                                                                <label class="radio inline" for="lang-en">
                                                                    <input type="radio" name="user_lang_id" id="lang-en" value="1" <?php echo ($user_record->user_lang_id == 1) ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('English', 'English'); ?>
                                                                </label>
                                                                <label class="radio inline" for="lang-ch">
                                                                    <input type="radio" name="user_lang_id" id="lang-ch" value="2" <?php echo ($user_record->user_lang_id == 2) ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Chinese', 'Chinese'); ?>
                                                                </label>
                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div class="span5">




                                                        <?php
                                                        $profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic, $user_record->user_gender);
                                                        ?>
                                                        <img src="<?php echo $profile_pic; ?>" alt="<?php echo $user_record->user_name; ?>" id="profile_pic" style="height:148px;width:148px"    class="img-polaroid"><br />
                                                        <div class="upload"> <a href="javascript:;" id="uploadFilePic" ><?php echo $this->lang->line('upload_profile_photo', 'Upload Profile Photo'); ?></a>
                                                            <span id="upload_loader"></span>
                                                            <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note', 'A maximum 2MB jpg, gif and png images are supported.'); ?></p>
                                                            <div id="fileUploadField_wrapper">
                                                                <input type="file" name="user_photo" id="profilePhotoUpload" accept="image/*" style="visibility:hidden;" />
                                                            </div>
                                                        </div>


                                                        <input type="hidden" id="x" name="x" value="0"  />
                                                        <input type="hidden" id="autoSave" name="autoSave" value="true"  />
                                                        <input type="hidden" id="y" name="y" value="0" />
                                                        <input type="hidden" id="w" name="w" value="140" />
                                                        <input type="hidden" id="h" name="h" value="140" />
                                                        <input type="hidden" id="profile_pic_latest" name="profile_pic_latest" value="" />
                                                        <input type="hidden" id="profile_pic_orig" name="profile_pic_orig" value="" />
                                                    </div>
                                                </div>

                                                <div class="row-fluid">
                                                    <div class="span12 border"></div>
                                                </div>
                                                <?php
                                                if (isset($user_record->user_fb_id) && $user_record->user_fb_id != "") {
                                                } else {
                                                    ?>
                                                    <div class="row-fluid">
                                                        <div class="span6" >
                                                            <button type="button" class="btn-custom-blue glompFont"  style="font-weight:normal !important;border-radius: 0px !important; width:381px !important"  name="Link_Facebook" id="Link_Facebook" ><?php echo $this->lang->line('link_facebook_account', 'Link Facebook Account'); ?></button>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span12 border">

                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                
                                                <?php
                                                if (empty($user_record->user_linkedin_id)) { ?>
                                                    <div class="row-fluid">
                                                        <div class="span6" >
                                                            <button type="button" class="btn-custom-007BB6 glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_LinkedIn" id="Link_LinkedIn" onclick ="linkLinkedInAccount(false, false)" ><?php echo $this->lang->line('link_linkedin_account', 'Link LinkedIn Account'); ?></button>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span12 border">

                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="row-fluid">
<?php
if (isset($user_record->user_fb_id) && $user_record->user_fb_id != "") {
    if ($user_record->user_fb_nofify_via_fb == 'Y') {
        $notif_checked = ' checked ';
    } else {
        $notif_checked = '';
    }
    echo'
						<div class="span12" style="display:none">
                            <label class="checkbox">
								<input type="checkbox" name="chk_notif_via_fb" id="chk_notif_via_fb" value="Y" ' . $notif_checked . ' />
								<strong>' . $this->lang->line('Notif_via_fb') . '</strong>
							</label>
						</div>';
}
?>

                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label class="control-label" for="email"><?php echo $this->lang->line('E-mail', 'Email'); ?> <span class="required">*</span></label>
                                                                <div class="controls">
                                                                    <input type="text"  id="email" value="<?php echo $user_record->user_email; ?>" name="email" class="span11 textBoxGray" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label class="control-label" for="email"><?php echo $this->lang->line('Notifications', 'Notifications'); ?></label>
                                                                <div class="controls" style="margin-top: 7px;">
                                                                    <a class="glompFont" style="color: blue !important" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                        <?php echo $this->lang->line('Show', 'Show'); ?>/<?php echo $this->lang->line('Hide', 'Hide'); ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row-fluid">
                                                        <div class="span12">
                                                            <div class="control-group">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                      <?php 
                                                                        if (isset($_GET['show_notification'])) {
                                                                            $collapse = 'in';
                                                                        }
                                                                        else {
                                                                            $collapse = '';
                                                                        }
                                                                      ?>
                                                                      <div id="collapseOne" class="panel-collapse collapse <?php echo $collapse; ?>">
                                                                        <div class="panel-body" style="margin-top: -4px;">
                                                                            <?php foreach ($notifications as $rec) { ?>
                                                                                <div class="row-fluid row-margin-5px">
                                                                                    <div class="fl" style="width: 121px;">
                                                                                        <?php $checked = ($this->user_not_sett->is_checked($rec['notification_id'], $user_id) ) ? 'checked' : ''; ?>
                                                                                        <input <?php echo $checked; ?> value ="<?php echo $rec['notification_id']; ?>" type="checkbox" id="chk_notification_<?php echo $rec['notification_id']; ?>" name="notfication_settings[]" class="textBoxGray" >
                                                                                    </div>
                                                                                    <div class="fl" style="width: 300px;">
                                                                                        <label style="width: auto;padding:0px;" class="control-label" for=""><?php echo $rec['name']; ?></label>
                                                                                        <span class="fl"><?php echo $rec['description']; ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row-fluid">
                                                        <div class="span12 border">

                                                        </div>
                                                    </div>

                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <div class="createPasswordTable" >
                                                                <table width="100%" border="0"  cellpadding="5" >
                                                                    <tr>
                                                                        <td><label class="control-label" for="current_pword"><?php $temp = $this->lang->line('Current_Password', 'Current Password');
                                                    echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                                        <td align="right">
                                                                            <input type="password"  id="current_pword" maxlength="9" name="current_pword" class="span11 textBoxGray" >
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <label class="control-label" for="new_pword"><?php $temp = $this->lang->line('New_password', 'New password');
                                                    echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                                        <td align="right"><input type="password"  id="new_pword"  maxlength="9" name="new_pword" class="span11 textBoxGray" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <label class="control-label" for="confirm_pword"  maxlength="9"><?php $temp = $this->lang->line('Confirm_Password', 'Confirm Password');
                                                    echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                                        <td align="right"><input type="password" id="confirm_pword" maxlength="9" name="confirm_pword" class="span11 textBoxGray" ></td>
                                                                    </tr>
                                                                </table>
                                                            </div>







                                                        </div>
                                                        
                                                        <div class="span5">
                                                            <div class="createPasswordTips"><?php echo $this->lang->line('passwors_tips'); ?></div>
                                                            <div padding-top="10px;">&nbsp;<a style="color:#59606B; font-size:14px; text-decoration:underline" href="<?php echo base_url('landing/reset_password'); ?>">
                                                                    <?php echo $this->lang->line('Forgot_Password', 'Forgot Password'); ?>?</a></div>
                                                        </div>
                                                        <div class="cl" align="right" style="padding:5px 0px;">
                                                            <a href="javascript:void(0)" id="terminate_account" style="color:#59606B; text-decoration: underline">
                                                                Deactivate account
                                                            </a>
                                                        </div>
                                                    </div>


                                                    <div class="row-fluid">
                                                        <div class="span12 border">

                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <button type="submit" class="btn-custom-reg"  style="padding:6px 40px !important" name="update_profile"><?php echo $this->lang->line('Save'); ?></button>
                                                        </div>


                                                        <div class="span5">
                                                            <button type="reset" onClick="redirect()" class="btn-custom-reset" style="padding:6px 40px !important"  name="Reset" ><?php echo $this->lang->line('Cancel'); ?></button>							   
                                                        </div>
                                                    </div>



                                                    </form>


                                                </div>
                                            </div>



                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <img src="" id="tempImg" style="display:none" />
            <div id ="gl_languages" style="display:none;">
                <span id="Close"><?php echo $this->lang->line('Close', "Close" ); ?></span>
                <span id="Cancel"><?php echo $this->lang->line('Cancel', "Cancel" ); ?></span>
                <span id="Ok"><?php echo $this->lang->line('Ok', "Ok" ); ?></span>
                <span id="Please_wait"><?php echo $this->lang->line('Please_wait', "Please wait..." ); ?></span>
                <span id="Crop"><?php echo $this->lang->line('Crop', "Crop" ); ?></span>
                <span id="Image_too_small"><?php echo $this->lang->line('Image_too_small', "Sorry this image is too small. Please select a larger one." ); ?></span>
                <span id="Error"><?php echo $this->lang->line('Error', "Error" ); ?></span>
                <span id="LinkedIn_error_1"><?php echo $this->lang->line('LinkedIn_error_1', "An error occur, LinkedIn server is down. Please try again later." ); ?></span>
                <span id="Invite"><?php echo $this->lang->line('Invite', "Invite" ); ?></span>
            </div>
    </body>
</html>
