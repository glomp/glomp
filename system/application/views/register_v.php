<?php
/* to check user agent*/
$ua = $this->custom_func->browser_info();	
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Register | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">        
        <link href="assets/frontend/css/jcrop/jquery.Jcrop.css" rel="stylesheet">        		
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="assets/frontend/js/jquery.Jcrop.min.js"></script>		
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/validate.register.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";

        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.landing.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
        <script type="text/javascript">
            var fbApiInit = false;
            $(document).ready(function(e) {            
        <?php
        if (validation_errors() != "" || isset($error_msg)) 
        {
            echo 'var NewDialog =$("#register_error_from_load");
            NewDialog.dialog({						
                autoOpen: false,
                closeOnEscape: false ,
                resizable: false,					
                dialogClass:"dialog_style_error_alerts ",
                modal: true,
                title:"",
                position: "center",
                width:400,                                
                buttons:[
                    {text: "Close",
                    "class": "btn-custom-white2",
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $("#waitPopup").dialog("destroy").remove();
                        }, 500);
                    }}
                ]
                
            });
            NewDialog.dialog("open");';
        }

if (($this->session->userdata('temp_user_fb_id')) != "") {
    $user_fb_id = ($this->session->userdata('temp_user_fb_id'));
    echo 'getFbPhoto(' . $user_fb_id . ');';
}
?>

<?php
if (($this->session->userdata('temp_user_linkedin_id')) != "") { ?>
	setTimeout(function() {
        IN.API.Raw("/people/~/picture-urls::(original)").result(function(r){ 
        	$("#profile_pic").prop("src", r.values);
        	$("#fb_user_photo").val(r.values);
    	});
    }, 2000);
<?php } ?>

                $('body').click(function() {
                    $('.sutoSugSearch').hide();
                });
                $('#frmRegister input').hover(function()
                {
                    $(this).popover('show')
                }, function() {
                    $(this).popover('hide')
                });

                $('#uploadFile').click(function(e) {

                    $('#fileUploadField').click();
                    e.preventDefault();
                });
                
                $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
                /* Popover*/

                $('#day').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#months').focus();
                    }
                });

                $('#months').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#year').focus();
                    }
                });
                $('#year').keyup(function() {
                    if (this.value.length == $(this).attr('maxlength')) {
                        $('#gender-male').focus();
                    }
                });

            });
            function getFbPhoto(fb_id) {
                facebookDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    title: '',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 300,
                    height: 120,
                    show: '',
                    hide: ''
                });
                facebookDialog.dialog("open");
                doFacebookDelay('facebookLoadingDialog', 'friendsList_loading', 5000, 'popup', 'facebookConnectDialog');

                var url = "http://graph.facebook.com/" + fb_id + "/picture?width=140&height=140&redirect=false";
                $.get(url, function(resp) {
                    var tempURL = resp.data.url;
                    $("#profile_pic").prop("src", tempURL);
                    $("#fb_user_photo").val(tempURL);
                });
                
				if(fbApiInit)
				{}
				else{
					FB.init({
						appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
						status     : true, /* check login status*/
						cookie     : true, /* enable cookies to allow the server to access the session*/
						xfbml      : true  /* parse XFBML*/
					});
					fbApiInit=true;
				}
				FB.getLoginStatus(function (response)
                {
					if(response.status=="connected"){
						FB.api({
								method: 'fql.query',
								query: 'SELECT first_name, last_name, current_location  FROM user WHERE uid = '+fb_id
							},
							function(response) {					
								/*console.log(response);*/
								var country="";
								var first_name=response[0]['first_name'];
								var last_name=response[0]['last_name'];
								if(response[0]['current_location']!=null)
									country=response[0]['current_location']['country'];
								/*console.log(country);*/
								if(country!=""){
									/* $("#location option").filter(function() {
										return this.text == country; 
									}).attr('selected', true);
                                    */
                                    $("#location").val(country);
								}
                                facebookDialog.dialog("close");
                                clearFacebookDelay();
							});						
					}
					else{
						FB.login
						(
							function( response )
							{
								if (response.status === 'connected') {												
									FB.api({
											method: 'fql.query',
											query: 'SELECT first_name, last_name, current_location  FROM user WHERE uid = '+fb_id
										},
										function(response) {					
											/*console.log(response);*/
											var country="";
											var first_name=response[0]['first_name'];
											var last_name=response[0]['last_name'];
											if(response[0]['current_location']!=null)
												country=response[0]['current_location']['country'];
											/*console.log(country);*/
											if(country!=""){
												 $("#location option").filter(function() {
													return this.text == country; 
												}).attr('selected', true);										 
											}	
                                            facebookDialog.dialog("close");
                                            clearFacebookDelay();
										});
								}
							}, {scope: 'publish_actions,email'}
						);	
					}
					/*console.log(response);*/
				});
				
			}	
            
        </script>
        <script type="text/javascript">			
			/* Load the SDK asynchronously*/
			(function(d){
				var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement('script'); js.id = id; js.async = true;
				js.src = "//connect.facebook.net/en_US/all.js";
				ref.parentNode.insertBefore(js, ref);
			}(document));
            $().ready(function() {
				
				
				$("#reset").click(function(ev)
				{
					$.ajax({
					type: "POST",																
					url: 'ajax_post/resetRegister',												
					success: function(res){
						window.location=LOCATION_REGISTER;			
					}
				});
					ev.preventDefault();
				});
				$('#register_btn').click(function(e) {
					var NewDialog = $('<div id="waitPopup" align="center">\
											<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" style="width:40px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
					NewDialog.dialog({						
						autoOpen: false,
						closeOnEscape: false ,
						resizable: false,					
						dialogClass:'dialog_style_glomp_wait noTitleStuff',
						title:'Please wait...',
						modal: true,
						position: 'center',
						width:200,
						height:120
					});
					NewDialog.dialog('open');
					
				});
				
                /* validate signup form on keyup and submit*/
                $("#frmRegister").validate({
                    rules: {
                        fname: "required",
                        lname: "required",
                        day: {
                            required: true,
                            minlength: 2,
                            maxlength: 2,
                            number: true,
                            range: [01, 31]
                        },
                        months: {
                            required: true,
                            minlength: 2,
                            maxlength: 2,
                            number: true,
                            range: [01, 12]
                        },
                        year: {
                            required: true,
                            minlength: 4,
                            maxlength: 4,
                            number: true,
                            range: [1900, 3000]
                        },
                        gender: {required: true},
                        location_text: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        pword: "required",
                        cpword: "required",
                        agree: "required"
                    }
                    ,
                    messages: {
                        fname: "Incorrect or missing entry. Please re-enter the red boxes.",
                        lname: "Incorrect or missing entry. Please re-enter the red boxes.",
                        day: "Incorrect or missing entry. Please re-enter the red boxes.",
                        months: "Incorrect or missing entry. Please re-enter the red boxes.",
                        year: "Incorrect or missing entry. Please re-enter the red boxes.",
                        gender: "Incorrect or missing entry. Please re-enter the red boxes.",
                        location_text: "Incorrect or missing entry. Please re-enter the red boxes.",
                        email: "Incorrect or missing entry. Please re-enter the red boxes.",
                        pword: "Incorrect or missing entry. Please re-enter the red boxes.",
                        cpword: "Incorrect or missing entry. Please re-enter the red boxes.",
                        agree: "Incorrect or missing entry. Please re-enter the red boxes."
                    },
                    invalidHandler: function(e, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            var message = errors == 1 ? 'You missed 1 field. It has been highlighted below' : 'You missed ' + errors + ' fields.  They have been highlighted below';
                            /*$("div.error span").html(message);
                            $( "#dialog-form" ).dialog( "open" );
                            */
                            var NewDialog =$('#register_error');
                            NewDialog.dialog({						
                                autoOpen: false,
                                closeOnEscape: false ,
                                resizable: false,					
                                dialogClass:'dialog_style_error_alerts ',
                                modal: true,
                                title:'Please wait...',
                                position: 'center',
                                width:400,                                
                                buttons:[
                                    {text: "Close",
                                    "class": 'btn-custom-white2',
                                    click: function() {
                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#waitPopup').dialog('destroy').remove();
                                        }, 500);
                                    }}
                                ]
                                
                            });
                            NewDialog.dialog('open');
                        }

                    },
                    onkeyup: false,
                    onfocusout:false
                 });
				/* In this example, since Jcrop may be attached or detached*/
				/* at the whim of the user, I've wrapped the call into a function*/
				
            });

			
			var jcrop_api;
    
			/* The function is pretty simple*/
			function UpdatePreviewSample(_this)
			{                
				asdasd();
			}
            var JcropW=0;
            var JcropH=0;
			function initJcrop()/*{{{*/
			{                
                
				jcrop_api = $.Jcrop($('#jCrop_target'), {                    
                    onSelect: updateCoords,                   
                    onChange: updateCoords,
                    onRelease: updateCoords
                });			  
				jcrop_api.setOptions({
					minSize: [ 140, 140 ],					
					aspectRatio: 1/1 
				});
				jcrop_api.animateTo([140,140,0,0]);
			}
			  function updateCoords(c)
			  {
				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
			  };

			 function resetCoords()
			  {
				$('#x').val(0);
				$('#y').val(0);
				$('#w').val(140);
				$('#h').val(140);
			  };	
      /*for profile picture	*/
	  function asdasd()
	{
		var form = $('#frmRegister');
		$('#waitPopup').dialog("close");
		$('#waitPopup').dialog('destroy').remove();																	
		var NewDialog = $('<div id="waitPopup" align="center">\
									<div align="center" style="margin-top:5px;">Please wait...<br><img width="40"  style="width:40px"  src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');																
		NewDialog.dialog({						
			autoOpen: false,
			closeOnEscape: false ,
			resizable: false,					
			dialogClass:'dialog_style_glomp_wait noTitleStuff',
			title:'Please wait...',
			modal: true,
			position: 'center',
			width:200,
			height:120
		});
		NewDialog.dialog('open');
		
		$(form).ajaxSubmit({
			type: "post",
			url: GLOMP_BASE_URL+'ajax_post/upload_temp',
			dataType: 'json',
			data: $(form).serialize(),
			success: function(response) {
				if(response.status=='Error'){
					$('#waitPopup').dialog("close");																	
					$('#waitPopup').dialog('destroy').remove();																	
					var NewDialog = $('<div id="waitPopup" align="center">\
									<h4 style="padding:0px;margin:0px;">Error</h4>\
									<div align="center" style="margin-top:5px;">'+response.message+'</div></div>');																
					NewDialog.dialog({						
						autoOpen: false,
						closeOnEscape: false ,
						resizable: false,					
						dialogClass:'dialog_style_glomp_wait noTitleStuff',
						title:'Please wait...',
						modal: true,
						position: 'center',
						width:200,
						buttons:[
							{text: "Cancel",
							"class": 'btn-custom-white2',
							click: function() {
								$(this).dialog("close");
								setTimeout(function() {
									$('#waitPopup').dialog('destroy').remove();
								}, 500);
							}}
						]
						
						
					});
					NewDialog.dialog('open');
				
				}
				else{					
					$("#tempImg").prop("src", GLOMP_BASE_URL+"custom/uploads/users/temp/"+response.img);					
					$('#tempImg').waitForImages(function() {
						$('#waitPopup').dialog("close");
						$('#waitPopup').dialog('destroy').remove();					
						UpdatePreview(response);
					});					
				/*
					$("#profile_pic").prop("src", "custom/uploads/users/temp/"+response.thumb);
					$("#profile_pic_latest").val(response.thumb);
					$("#profile_pic_orig").val(response.orig);
					$('#waitPopup').dialog("close");
					setTimeout(function() {
						$('#waitPopup').dialog('destroy').remove();
					},100);
				*/
				}													
			}
		});
	}   
        
function  UpdatePreview(target)
        {
            /*alert(target.result);*/
            /*$("#profile_pic").prop("src", target.result);*/
            /*$("#jCrop_target").prop("src", ($("#profile_pic").prop("src")));*/
            var tempW=target.w;
            var tempH=target.h;
            JcropW=tempW;
            JcropH=tempH;
            setTimeout(function()
            {
                if(( (parseInt(tempW) <140 ) ) 
                || ( (parseInt(tempH) <140 ) ))
                {
                    $('#waitPopup').dialog("close");																	
                    $('#waitPopup').dialog('destroy').remove();																	
                    var NewDialog = $('<div id="waitPopup" align="center">\
                                    <h4 style="padding:0px;margin:0px;">Error</h4>\
                                    <div align="center" style="margin-top:5px;font-size:14px">Sorry this image is too small. Please select a larger one.</div></div>');																
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                        title:'Please wait...',
                        modal: true,
                        position: 'center',
                        width:300,
                        height:140,
                        buttons:[
                            {text: "Ok",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#waitPopup').dialog('destroy').remove();
                                }, 500);
                            }}
                        ]
                        
                        
                    });
                    NewDialog.dialog('open');
                }
                else
                {
                    var popW=300;
                    var popH=300;
                    if( (parseInt(tempW+120)*1) > parseInt(popW) )
                    {												
                        popW=tempW+120;
                        
                    }
                    if( (parseInt(tempH+80)*1) > parseInt(popH) )
                    {												
                        popH=tempH+80;
                        
                    }
                    
                    if (popW>1200)
                        popW=1200;											
                    if(popH>800)
                        popH=800;	
                    
                    /*create a popup for jcrop*/
                    /*create a popup for jcrop*/
                    $('#jCropDialog').dialog('destroy').remove();
                    var NewDialog = $('<div id="jCropDialog" align="center">\
                                            <img style="width:'+tempW+'px;height'+tempH+':px" id="jCrop_target" alt="" class="fixed-size" />\
                                        </div>');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff contentOverflowAuto',
                        title:'Please wait...',
                        modal: true,
                        position: 'center',
                        width:popW,
                        height:popH,
                        buttons: [
                            {text: "Crop",
                                "class": 'btn-custom-blue',
                                click: function() {											
                                    var form = $('#frmRegister');							
                                    $('#jCropDialog').dialog("close");
                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                                <div align="center" style="margin-top:5px;">Please wait...<br><img  style="width:40px"  width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');																
                                    NewDialog.dialog({						
                                        autoOpen: false,
                                        closeOnEscape: false ,
                                        resizable: false,					
                                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                        title:'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width:200,
                                        height:120
                                    });
                                    NewDialog.dialog('open');
                                    
                                    $(form).ajaxSubmit({
                                        type: "post",
                                        url: GLOMP_BASE_URL+'ajax_post/crop_register',
                                        dataType: 'json',
                                        data: $(form).serialize(),
                                        success: function(response) {
                                            if(response.status=='Error'){
                                                $('#waitPopup').dialog("close");																	
                                                $('#waitPopup').dialog('destroy').remove();																	
                                                var NewDialog = $('<div id="waitPopup" align="center">\
                                                                <h4 style="padding:0px;margin:0px;">Error</h4>\
                                                                <div align="center" style="margin-top:5px;">'+response.message+'</div></div>');																
                                                NewDialog.dialog({						
                                                    autoOpen: false,
                                                    closeOnEscape: false ,
                                                    resizable: false,					
                                                    dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                                    title:'Please wait...',
                                                    modal: true,
                                                    position: 'center',
                                                    width:200,
                                                    height:120,
                                                    buttons:[
                                                        {text: "Cancel",
                                                        "class": 'btn-custom-white2',
                                                        click: function() {
                                                            $(this).dialog("close");
                                                            setTimeout(function() {
                                                                $('#waitPopup').dialog('destroy').remove();
                                                            }, 500);
                                                        }}
                                                    ]
                                                    
                                                    
                                                });
                                                NewDialog.dialog('open');
                                            
                                            }
                                            else{
                                                $("#profile_pic").prop("src", "custom/uploads/users/temp/"+response.thumb);
                                                $("#profile_pic_latest").val(response.thumb);
                                                $("#profile_pic_orig").val(response.orig);
                                                $('#waitPopup').dialog("close");
                                                setTimeout(function() {
                                                    $('#waitPopup').dialog('destroy').remove();
                                                },100);
                                            }
                                            $('#fileUploadField').remove();                                                
                                            $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden" id="fileUploadField" />');
                                            $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
                                        }
                                    });
                                }},
                            {text: "Cancel",
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $('#fileUploadField').remove();                                                
                                    $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden" id="fileUploadField" />');
                                    $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
                                    $(this).dialog("close");
                                    setTimeout(function() {                                        
                                        $('#waitPopup').dialog('destroy').remove();
                                    }, 500);
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                    setTimeout(function() {
                        $("#jCrop_target").prop("src", GLOMP_BASE_URL+"custom/uploads/users/temp/"+target.img);
                        /*create a popup for jcrop	*/
                        $('#jCrop_target').waitForImages(function() {
                            setTimeout(function() {
                                initJcrop();
                                resetCoords();
                            }, 500);
                        });
                    }, 50);                    
                    /*Popover*/
                }
            }, 100);
            
            
            
        }
    </script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>
<?php include('includes/header.php') ?>
        <div class="container">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="outerBorder">
                        <div class="innerBorder">
                            <div class="register">
                                <div class="innerPageTitleRed glompFont"><?php echo $this->lang->line('Register'); ?></div>
                                <div class="content">

<?php echo form_open_multipart('landing/register', 'name="frmCms" class="form-horizontal" id="frmRegister"') ?>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <h1><?php echo $this->lang->line('Personal_Details'); ?></h1>
                                        </div>
                                    </div>
                                    
                                    <div class="" id="register_error_from_load">
                                        <?php
                                        if (validation_errors() != "" || isset($error_msg))                                        
                                        {
                                        ?>
                                            <div class="alert alert-error">
                                            <?php
                                           if (validation_errors() != "") {
                                                echo validation_errors();
                                            }
                                            if (isset($error_msg)) {
                                                echo $error_msg;
                                            }                                            
                                            ?>
                                            </div>
                                        <?php
                                        }
                                        ?>

                                    </div>
                                    <div class="" id="register_error" style="">
                                    </div>                                    
                                    <div class="row-fluid">
                                        <div class="span6">

                                            <div class="control-group">
                                                <label class="control-label" for="name"><?php echo $this->lang->line('Fname'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <?php
                                                    $location_text = "";
                                                    $user_linkedin_id = "";
                                                    $array_items = array(
                                                        'temp_fname' => '',
                                                        'temp_lname' => '',
                                                        'temp_mname' => '',
                                                        'temp_email' => '',
                                                        'temp_gender' => '',
                                                        'temp_day' => '',
                                                        'temp_months' => '',
                                                        'temp_year' => ''
                                                    );
                                                    if (($this->session->userdata('temp_user_fb_id')) != "") {
                                                        $user_fb_id = ($this->session->userdata('temp_user_fb_id'));
                                                        $fname = ($this->session->userdata('temp_fname'));
                                                        $mname = ($this->session->userdata('temp_mname'));
                                                        $lname = ($this->session->userdata('temp_lname'));
                                                        $email = ($this->session->userdata('temp_email'));
                                                        $gender = ($this->session->userdata('temp_gender'));
                                                        $day = ($this->session->userdata('temp_day'));
                                                        $months = ($this->session->userdata('temp_months'));
                                                        $year = ($this->session->userdata('temp_year'));


                                                        
                                                        $array_items['temp_user_fb_id'] = '';
                                                        
                                                        $this->session->unset_userdata($array_items);
                                                    } else if(($this->session->userdata('temp_user_linkedin_id')) != "") {
                                                        /*LinkedIN*/
                                                        /*TODO: Please clean code block. All the social app thing.*/
                                                        $user_linkedin_id = ($this->session->userdata('temp_user_linkedin_id'));
                                                        $fname = ($this->session->userdata('temp_fname'));
                                                        $mname = ($this->session->userdata('temp_mname'));
                                                        $lname = ($this->session->userdata('temp_lname'));
                                                        $email = ($this->session->userdata('temp_email'));
                                                        $gender = ($this->session->userdata('temp_gender'));
                                                        $day = ($this->session->userdata('temp_day'));
                                                        $months = ($this->session->userdata('temp_months'));
                                                        $year = ($this->session->userdata('temp_year'));
                                                        
                                                        $array_items['temp_user_linkedin_id'] = '';
                                                        $this->session->unset_userdata($array_items);
                                                    } else {
                                                        $fname = ($this->session->userdata('fname')) ? $this->session->userdata('fname') : set_value('fname');
                                                        $lname = ($this->session->userdata('lname')) ? $this->session->userdata('lname') : set_value('lname');
                                                        $email = ($this->session->userdata('email')) ? $this->session->userdata('email') : set_value('email');
                                                        $gender = ($this->session->userdata('gender')) ? $this->session->userdata('gender') : set_value('gender');
                                                        $day = ($this->session->userdata('day')) ? $this->session->userdata('day') : set_value('day');
                                                        $months = ($this->session->userdata('months')) ? $this->session->userdata('months') : set_value('months');
                                                        $year = ($this->session->userdata('year')) ? $this->session->userdata('year') : set_value('year');
                                                        $location_text = ($this->session->userdata('location')) ? $this->session->userdata('location') : $location_text;
                                                    }

                                                    $promo_code = "";
                                                    if (isset($_POST['location_text']))
                                                        $location_text = $_POST['location_text'];

                                                    if (isset($_POST['promo_code']))
                                                        $promo_code = $_POST['promo_code'];
                                                    $pword = "";
                                                    if (isset($_POST['pword']))
                                                        $pword = $_POST['pword'];
                                                    $cpword = "";
                                                    if (isset($_POST['cpword']))
                                                        $cpword = $_POST['cpword'];

                                                    if (isset($user_fb_id) && $user_fb_id != "") {
                                                        echo '			
                                                        <input type="hidden"  id="fb_user_photo" name="fb_user_photo" value="">
                                                        <input type="hidden"  id="fb_user_id" name="fb_user_id" value="' . $user_fb_id . '">
                                                        <input type="hidden"  id="fb_fname" name="fb_fname" value="' . $fname . '">
                                                        <input type="hidden"  id="fb_mname" name="fb_mname" value="' . $mname . '">
                                                        <input type="hidden"  id="fb_lname" name="fb_lname" value="' . $lname . '">
                                                        <input type="hidden"  id="fb_email" name="fb_email" value="' . $email . '">
                                                        <input type="hidden"  id="fb_gender" name="fb_gender" value="' . $gender . '">
                                                        <input type="hidden"  id="fb_day" name="fb_day" value="' . $day . '">
                                                        <input type="hidden"  id="fb_months" name="fb_months" value="' . $months . '">
                                                        <input type="hidden"  id="fb_year" name="fb_year" value="' . $year . '">
                                                        ';
                                                    }
                                                    if (isset($user_linkedin_id) && $user_linkedin_id != "") {
                                                        echo '
                                                            <input type="hidden"  id="fb_user_photo" name="fb_user_photo" value="">
                                                            <input type="hidden"  id="user_linkedin_id" name="user_linkedin_id" value="' . $user_linkedin_id . '">
                                			';
                                                    }
                                                    ?>
                                                    <input type="text"  id="fname" name="fname" value="<?php echo $fname; ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="lname"><?php echo $this->lang->line('Lname'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text"  id="lname" name="lname" value="<?php echo $lname; ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="day" name="day" maxlength="2" value="<?php echo $day; ?>"  class="span2 textBoxGray" placeholder="DD"> /
                                                    <input type="text" id="months" name="months" value="<?php echo $months; ?>" maxlength="2"  class="span2 textBoxGray" placeholder="MM"> /
                                                    <input type="text" id="year" name="year" maxlength="4" value="<?php echo $year; ?>"  class="span3 textBoxGray" placeholder="YYYY">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="display">Display</label>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" name="dob_display" id="dob_display_w_year" value="dob_display_w_year" <?php echo $checked = ($dob_display == 'dob_display_w_year') ? 'Checked="checked"' : ""; ?> /> DD / MM / YYYY
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" name="dob_display" id="dob_display_wo_year" <?php echo $checked = ($dob_display == 'dob_display_wo_year') ? 'Checked="checked"' : ""; ?> value="dob_display_wo_year"> DD / MM
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo $checked = ($gender != 'Female') ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-female" <?php echo $checked = ($gender == 'Female') ? 'Checked="checked"' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <!--<div class="control-group">
                                                <label class="control-label" for="location"><?php echo $this->lang->line('Location'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                                                        

                                                    <select name="location" class="textBoxGray" id="location1" style="width:263px">
                                                        <option value=""><?php echo $this->lang->line('Select_Location'); ?></option><?php
                                            echo $this->regions_m->location_dropdown(0, set_value('location'));
                                            ?>
                                                    </select>
                                                </div>
                                            </div>-->


                                            <div class="control-group">
                                                <label class="control-label" for="location"><?php echo $this->lang->line('Location'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input class="textBoxGray" type="text" name="location_text" id="location" value="<?php echo $location_text; ?>" style="width:250px"  autocomplete="off" placeholder="">
                                                    <input type="hidden" name="location" id="location_id" value="" autocomplete="off" class="" >
                                                    <div class="sutoSugSearch" id="autoSugSearch" style="color:#59606B !important;margin-left:20px;width:244px">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="span5">


                                            <div class="uploadImage" style="height:140px;width:140px;overflow:visible; border:0px solid"><img src="assets/frontend/img/default_profile_pic_register.jpg" alt="" id="profile_pic" style="height:100%;"  /></div>

                                            <div class="upload">

                                                <span id="uploadFile" style="cursor:pointer" ><?php echo $this->lang->line('upload_profile_photo'); ?></span><span class="required">*</span>
                                                <span id="upload_loader"></span>
                                                <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note'); ?></p>
                                            </div>
                                            <div id="fileUploadField_wrapper">
                                                <input type="file" name="user_photo" value="" accept="image/*" style="visibility:hidden" id="fileUploadField" />
                                            </div>
                                            <input type="hidden" id="x" name="x" value="0"  />
                                            <input type="hidden" id="y" name="y" value="0" />
                                            <input type="hidden" id="w" name="w" value="100" />
                                            <input type="hidden" id="h" name="h" value="100" />
                                            <input type="hidden" id="profile_pic_latest" name="profile_pic_latest" value="" />
                                            <input type="hidden" id="profile_pic_orig" name="profile_pic_orig" value="" />
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 border" style="margin-bottom: 4px;"></div>
                                    </div>
                                    <?php
                                    if (isset($user_fb_id) && $user_fb_id != "") {
                                        
                                    } else {
                                        ?>
                                        <div class="row-fluid">
                                            <div class="span6" >
                                                <button type="button" class="btn-custom-blue glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_Facebook" id="Link_Facebook" >Link Facebook account</button>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12 border">

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if ($user_linkedin_id == "") {
                                        ?>
                                        <div class="row-fluid">
                                            <div class="span6" >
                                                <button type="button" class="btn-custom-007BB6 glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_LinkedIn" id="Link_LinkedIn" onclick ="liAuth()" >Link LinkedIn Account</button>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12 border">

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                    <div class="row-fluid">						
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="email"><?php echo $this->lang->line('email'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text"  id="email" value="<?php echo $email; ?>" name="email" class="span11 textBoxGray" rel="popover" data-content="<?php echo $this->lang->line('emailTips'); ?>" <?php /* ?>data-original-title="<?php echo $this->lang->line('E-mail');?>" <?php */ ?>>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 border">

                                        </div>
                                    </div>

                                    <div class="row-fluid">

                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="pword"><?php $temp = $this->lang->line('Create_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right">
                                                            <input type="password" maxlength="9"  id="pword" name="pword" class="span11 textBoxGray" value="<?php echo $pword; ?>" > </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="cpword"><?php $temp = $this->lang->line('Confirm_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right"><input maxlength="9" type="password" id="cpword" name="cpword" class="span11 textBoxGray" value="<?php echo $cpword; ?>" ></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <div class="createPasswordTips"><?php echo $this->lang->line('passwors_tips'); ?></div>
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 border">

                                        </div>
                                    </div>


                                    <div class="row-fluid">

                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="promo_code"><?php $temp = $this->lang->line('Promotional_Code');
                                    echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                        <td align="right">
                                                            <input type="text" maxlength="45"  id="promo_code" name="promo_code" placeholder="(optional)" class="span11 textBoxGray" value="<?php echo $promo_code; ?>" >
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 border">

                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12">
                                            <label class="checkbox">
                                                <input type="checkbox" name="agree">
<?php echo $this->lang->line('I_Agree_to_the'); ?><strong> 
<?php echo $this->lang->line('termNconditi0n'); ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('index.php/page/index/terms'); ?>" target="_blank"><strong><?php echo $this->lang->line('Read'); ?></strong></a> <br/><br/>
                                            </label>

                                            <br/><br/>

                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6">

                                            <div class="control-group">
                                                <input type="hidden" name="token" value="<?php echo $token; ?>" />
                                                <button type="submit" id="register_btn" class=" btn-custom-reg" style=" padding:10px 40px; font-size:14px;" name="Register" ><?php echo $this->lang->line('Submit'); ?></button>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <button type="reset" style="margin-left:3px; padding:10px 19px; font-size:14px;" class="btn-custom-reset" name="Reset" id="reset" ><?php echo $this->lang->line('Reset_All_Fields'); ?></button>
                                        </div>
                                    </div>


                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- ui-dialog -->
        <div id="dialog" title="">
            <div id="dialog-content" style="line-height:120%; font-size:16px" align="center"></div>
        </div>
        <img src="" id="tempImg" style="display:none" />
    </body>	
</html>
