<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $this->lang->line('glomp_page'); ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
              "stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e) {
                /*open glomp box*/
                $('.reAssign').click(function() {
                    $("#showError").hide();
                    $("#showSuccess").hide();
                    var _this = $(this);
                    $('.glompItemPopUp').fadeIn();
                    var _voucher_id = _this.data('voucherid');
                    $("#_voucher_id").val(_voucher_id);
                });
                /* send glomp to friend*/
                $('._send_glomp').click(function() {
                    var msg = $('#glomp_user_message').val();
                    if (msg == "")
                    {
                        if (!confirm("<?php echo $this->lang->line('glomp_confirmation_message', 'Are you sure you dont want to include a message?'); ?>"))
                        {
                            $('#glomp_user_message').focus();
                            return false;
                        }
                    }
                    $('#cancel_glomp_option').addClass('disabled');
                    $('#cancel_glomp_option').removeClass('_close_popup');
                    $(".Loader").show();
                    $("#showError").hide();
                    $("#showSuccess").hide();
                    $.ajax({
                        type: "POST",
                        url: "profile/reassignVoucher/",
                        data: $('#frmSendVoucher').serialize(),
                        success: function(data) {
                            var d = eval("(" + data + ")");
                            if (d.status == 'success')
                            {
                                $("#showSuccess").show();
                                $("#showSuccessMessage").text(d.msg);
                                $('.glompItemPopUp').fadeOut(5500);
                                location.href = "<?php echo base_url(); ?>";
                            }
                            else if (d.status == 'error')
                            {
                                $(".Loader").hide();
                                $("#showError").show();
                                $("#showErrorMessage").text(d.msg);
                            }
                        }
                    });
                });
                $('body').click(function() {
                    $('.popUp').hide(100);
                });


                var _y = ($(window).height() / 2) - (parseInt($(".glompItemPopUp").css("height")) / 2);
                var _x = ($(window).width() / 2) - (parseInt($(".glompItemPopUp").css("width")) / 2);
                $(".glompItemPopUp").css({"margin-top": _y, "margin-left": _x});


                /*close glomp popup box*/
                $('._close_popup').click(function() {
                    $('.glompItemPopUp').fadeOut();

                });
                /* end of glomp item popup */
            });

        </script>
    <div class="glompItemPopUp">

        <div class="row-fluid">
            <div class="span12">

                <div class="row-fluid">

                    <div class="span12" >
                        <div class="alert alert-success"  id="showSuccess" style="display:none; margin-top:10px; margin-bottom:-5px;">

                            <span id="showSuccessMessage"></span>
                        </div>
                        <div class="alert alert-error"  id="showError" style="display:none;margin-top:10px; margin-bottom:-5px;">

                            <span id="showErrorMessage"></span>
                        </div>

                        <h2 class="text-center"><?php echo $this->lang->line('Reassign_Voucher', 'Reassign Voucher'); ?></h2>
                    </div>

                </div>

                <form name="frmSendVoucher" id="frmSendVoucher" method="post" action="">

                    <textarea name="message" id="glomp_user_message" placeholder="<?php echo $this->lang->line('Message', 'Message'); ?>" class="span12"></textarea>
                    <br /> <br />
                    <input type="password" name="password"  id="password" placeholder=" <?php echo $this->lang->line('Your_Password', 'Your Password'); ?>" class="span12">
                    <div class="forgotPassword"><a href="<?php echo base_url('landing/reset_password'); ?>"><?php echo $this->lang->line('Forgot_Password', 'Forgot Password'); ?> ?</a></div>

                    <input type="text" name="fname"  id="fname" class="span12" placeholder=" <?php echo $this->lang->line('Friend_First_Name', 'Friend First Name'); ?> ">

                    <input type="text" name="lname"  id="lname" class="span12" placeholder="<?php echo $this->lang->line('Friend_Last_Name', 'Friend Last Name'); ?> ">

                    <input type="text" name="email"  id="email" class="span12" placeholder="<?php echo $this->lang->line('Friend_Email', 'Friend Email'); ?> ">
                    <input type="hidden" name="voucger_id"  id="_voucher_id" class="span12">
                    <div class="text-center" style="padding-top:5px;">

                        <span class="Loader" style="display:none;"><i class="icon-spinner icon-spin icon-large"></i></span>  <button type="button" class="btn-custom-gray _send_glomp" data-loading-text="loading stuff..." name="Confirm" ><?php echo $this->lang->line('Confirm', 'Confirm'); ?></button>
                        <button type="button" id="cancel_glomp_option" class="btn-custom-white  _close_popup" name="Cancel" style="margin-left:100px;" ><?php echo $this->lang->line('Cancel', 'Cancel'); ?></button>  

                    </div>
                </form>
            </div>
        </div>
    </div>

</head>
<body>
    <?php include_once("includes/analyticstracking.php") ?>
    <?php include('includes/header.php') ?>
    <div class="container">
        <div class="inner-content">
            <div class="row-fluid">			
                <div class="outerBorder" style="float:left;">				
                    <div class="innerBorder3 fl" style="float:left;width:896px">
                        <div style="padding:5px 0px;">							
                            <img src="assets/frontend/img/glomped.png" alt="Glomped"></a>
                        </div>
                        <div class="tabContent fl" style="float:left;width:860px">
                            <div class="glomp_content_wrapper fl" >
                                <?php
                                $ctr_show = 0;
                                if ($my_glomp_details->num_rows() > 0) {
                                    foreach ($my_glomp_details->result() as $rec_glomped_me) {
                                        $prod_id = $rec_glomped_me->voucher_product_id;
                                        $glomped_message = $rec_glomped_me->voucher_sender_glomp_message;
                                        $glomped_by_name = $rec_glomped_me->user_name;
                                        $glomped_by_photo = $this->custom_func->profile_pic($rec_glomped_me->user_profile_pic, $rec_glomped_me->user_gender);
                                        $glomped_by_name_link = base_url(('profile/view/' . $rec_glomped_me->voucher_purchaser_user_id));

                                        $glomped_me_prod = json_decode($this->product_m->productInfo($prod_id));
                                        $prod_name = $glomped_me_prod->product->$prod_id->prod_name;
                                        $product_logo = $this->custom_func->product_logo($glomped_me_prod->product->$prod_id->prod_image);
                                        $merchant_name = $glomped_me_prod->product->$prod_id->merchant_name;
                                        $merchant_logo = $this->custom_func->merchant_logo($glomped_me_prod->product->$prod_id->merchant_logo);
                                        $merchant_id = $rec_glomped_me->voucher_merchant_id;
                                        $remaining_days = $this->custom_func->compute_remaining_days(date('Y-m-d'), ($rec_glomped_me->voucher_expiry_date));

                                        if ($rec_glomped_me->voucher_orginated_by > 0) { /// given by glomp
                                            //Pending glomp rewards
                                            if ($rec_glomped_me->voucher_type == 'Pending' && $rec_glomped_me->voucher_status != 'Expired') {
                                                if ($rec_glomped_me->voucher_status == 'Consumable' && $rec_glomped_me->voucher_purchaser_user_id == 1) { ?>
                                                    <div class="glomp_gray_wrapper fl" >
                                                        <form action="<?php echo site_url('glomp/acceptVoucher'); ?>" method="POST">
                                                        <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
                                                        <div class="fl">																			
                                                            <div class="glomp_thumb_voucher" style="background-image:url('<?php echo base_url(); ?>assets/frontend/img/glomp_reward_voucher.png');"></div>
                                                            <div class="glomp_thumb_voucher_details"><?php echo $this->lang->line('glomp_reward_voucher_details_great_glompler', 'Thank you for being a great glomp!er'); ?></div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                        <div class="glomp_details_2 fl">
                                                            <div class="glomp_thumb_voucher_details2"><?php echo $this->lang->line('glomp_reward_voucher_details_appreciate_support', 'We appreciate your support. Here\'s'); ?></div>
                                                            <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                            <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                            <div class="glomp_remaining_time">
                                                                <button type="submit" class="btn-custom-blue2" name="accept" ><?php echo $this->lang->line('glomp_accept', 'accept'); ?></button>
                                                                    <?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?>
                                                            </div>
                                                        </div>									
                                                        <div class="glomp_thumb_item fr" style="background-image:url('<?php echo $product_logo; ?>');">
                                                            <div class="glomp_thumb_merchant" style="background-image:url('<?php echo $merchant_logo; ?>');">
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                <?php } ?>
                                                
                                                <?php //given by glomp to your friend but re assigned to you. ?>
                                                <?php if ($rec_glomped_me->voucher_status == 'Consumable' && $rec_glomped_me->voucher_purchaser_user_id > 1) { ?>
                                                    <div class="glomp_gray_wrapper fl" >
                                                        <form action="<?php echo site_url('glomp/acceptVoucher'); ?>" method="POST">
                                                        <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
                                                        <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                        </div>
                                                        <div class="glomp_details fl">
                                                            <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                            <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                            <div  class="glomp_friend_name">
                                                                <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                    <?php echo $glomped_by_name; ?>
                                                                </a>
                                                            </div>
                                                            <div class="glomp_remaining_time"><?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?></div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                        <div class="glomp_buttons_wrapper fr">
                                                            <div class="glomp_buttons">					
                                                                <button type="submit" class="btn-custom-blue2" name="accept" ><?php echo $this->lang->line('glomp_accept', 'accept'); ?></button>
                                                            </div>										
                                                            <div class="glomp_buttons">
                                                                <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                <?php } // end given by glomp to your friend but re-assigned to you. ?>                                
                                
                                                <?php if ($rec_glomped_me->voucher_status == 'Assigned') { ?>
                                                    <!-- reward voucher item-->
                                                    <div class="glomp_gray_wrapper fl" >
                                                        <form action="<?php echo site_url('glomp/acceptVoucher'); ?>" method="POST">
                                                        <div class="fl">																			
                                                            <div class="glomp_thumb_voucher" style="background-image:url('<?php echo base_url(); ?>assets/frontend/img/glomp_reward_voucher.png');"></div>
                                                            <div class="glomp_thumb_voucher_details"><?php echo $this->lang->line('glomp_reward_voucher_details', 'Here\'s something from us for you to treat a friend with'); ?></div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                            <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
                                                            <div class="glomp_details_2 fl">
                                                                <div class="glomp_thumb_voucher_details2"><?php echo $this->lang->line('glomp_reward_voucher_details2', 'Invite a friend to glomp! with this treat!'); ?></div>
                                                                <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                                <div class="glomp_item"><?php echo $prod_name; ?></div>																				
                                                                <div class="glomp_remaining_time">
                                                                    <!--<button type="submit" class="btn-custom-green3" data-voucherid="<?php echo $rec_glomped_me->voucher_id; ?>"><img style="border:0px;" src="assets/frontend/img/glomp_white.png"></img></button> -->
                                                                    <button type="submit" class="btn-custom-blue2" name="accept" ><?php echo $this->lang->line('glomp_accept', 'accept'); ?></button>
                                                                        <?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?>
                                                                </div>
                                                            </div>
                                                            <div class="glomp_thumb_item fr" style="background-image:url('<?php echo $product_logo; ?>');">
                                                                <div class="glomp_thumb_merchant" style="background-image:url('<?php echo $merchant_logo; ?>');"></div>
                                                            </div>
                                                            </form>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php
                                                //Accepted reward voucher
                                                if ($rec_glomped_me->voucher_type != 'Pending' && $rec_glomped_me->voucher_status != 'Expired') { ?>
                                                    <?php if ($rec_glomped_me->voucher_type == 'Consumable') { ?>
                                                        <div class="glomp_gray_wrapper fl" >
                                                            <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                            </div>
                                                            <div class="glomp_details fl">
                                                                <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                                <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                                <div  class="glomp_friend_name">
                                                                    <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                        <?php echo $glomped_by_name; ?>
                                                                    </a>
                                                                </div>
                                                                <div class="glomp_remaining_time"><?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?></div>
                                                            </div>
                                                            <div class="glomp_details_line fl"></div>
                                                            <div class="glomp_buttons_wrapper fr">
                                                                <div class="glomp_buttons">					
                                                                    <a href="<?php echo base_url('merchant/about/' . $merchant_id . '?frm=you'); ?>" style="color:#fff !important">
                                                                        <button type="button" class="btn-custom-green btn-block" name="locate" ><?php echo $this->lang->line('glomp_locate', 'locate'); ?></button>											
                                                                    </a>
                                                                </div>										
                                                                <div class="glomp_buttons">
                                                                    <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($rec_glomped_me->voucher_status == 'Assigned') { ?>
                                                        <!-- reward voucher item-->
                                                        <div class="glomp_gray_wrapper fl" >																		
                                                            <div class="fl">																			
                                                                <div class="glomp_thumb_voucher" style="background-image:url('<?php echo base_url(); ?>assets/frontend/img/glomp_reward_voucher.png');"></div>
                                                                <div class="glomp_thumb_voucher_details"><?php echo $this->lang->line('glomp_reward_voucher_details', 'Here\'s something from us for you to treat a friend with'); ?></div>
                                                            </div>
                                                            <div class="glomp_details_line fl"></div>
                                                            <form action="<?php echo site_url('user/giveVoucher'); ?>" method="POST">
                                                                <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
                                                                <input type="hidden" name="type" id="type" value="reassign" />
                                                                <div class="glomp_details_2 fl">
                                                                    <div class="glomp_thumb_voucher_details2"><?php echo $this->lang->line('glomp_reward_voucher_details2', 'Invite a friend to glomp! with this treat!'); ?></div>
                                                                    <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                                    <div class="glomp_item"><?php echo $prod_name; ?></div>																				
                                                                    <div class="glomp_remaining_time">
                                                                        <button type="submit" class="btn-custom-green3" data-voucherid="<?php echo $rec_glomped_me->voucher_id; ?>"><img style="border:0px;" src="assets/frontend/img/glomp_white.png"></img></button>
                                                                            <?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="glomp_thumb_item fr" style="background-image:url('<?php echo $product_logo; ?>');">
                                                                <div class="glomp_thumb_merchant" style="background-image:url('<?php echo $merchant_logo; ?>');">											
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>                                                    
                                                <?php } //end active voucher given by glomp ?>
                                                <?php if ($rec_glomped_me->voucher_status == 'Redeemed') { ?>     
                                                    <div class="glomp_gray_wrapper glomp_gray_background fl" >								
                                                        <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                        </div>
                                                        <div class="glomp_details fl">
                                                            <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                            <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                            <div  class="glomp_friend_name">
                                                                <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                    <?php echo $glomped_by_name; ?>
                                                                </a>
                                                            </div>
                                                            <div class="glomp_remaining_time_expired">											
                                                                <?php echo $this->lang->line('Redeemed', 'Redeemed'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                        <div class="glomp_buttons_wrapper fr">
                                                            <div class="glomp_buttons">
                                                                <a href="<?php echo base_url('merchant/about/' . $merchant_id . '?frm=you'); ?>" style="color:#fff !important">                        	
                                                                    <button type="button" class="btn-custom-green btn-block" name="locate" ><?php echo $this->lang->line('glomp_locate', 'locate'); ?></button>											
                                                                </a>
                                                            </div>										
                                                            <div class="glomp_buttons">
                                                                <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } //end redeemed voucher ?>
                                                        
                                                <?php if ($rec_glomped_me->voucher_status == 'Expired') { ?>
                                                    <!-- reward voucher expired-->
                                                    <div class="glomp_gray_wrapper glomp_gray_background fl" >
                                                        <div class="fl">																			
                                                            <div class="glomp_thumb_voucher" style="background-image:url('<?php echo base_url(); ?>assets/frontend/img/glomp_voucher_rewards-expired.png');"></div>
                                                            <div class="glomp_thumb_voucher_details_expired"><?php echo $this->lang->line('glomp_reward_voucher_details', 'Here\'s something from us for you to treat a friend with'); ?></div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                        <div class="glomp_details_2 fl">
                                                            <div class="glomp_thumb_voucher_details2"><?php echo $this->lang->line('glomp_reward_voucher_details2', 'Invite a friend to glomp! with this treat!'); ?></div>
                                                            <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                            <div class="glomp_item"><?php echo $prod_name; ?></div>																				
                                                            <div class="glomp_remaining_time_expired">											
                                                                <?php echo $this->lang->line('Expired', 'Expired'); ?>
                                                            </div>
                                                        </div>									
                                                        <div class="glomp_thumb_item fr" style="background-image:url('<?php echo $product_logo; ?>');">
                                                            <div class="glomp_thumb_merchant" style="background-image:url('<?php echo $merchant_logo; ?>');">											
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } //end expired reward voucher ?>
                                        <?php } else { //end given by glomp ?>
                                            <?php if ($rec_glomped_me->voucher_type == 'Pending' && $rec_glomped_me->voucher_status != 'Expired') { ?>
                                                <?php if ($rec_glomped_me->voucher_status == 'Consumable') { ?>    
                                                    <div class="glomp_gray_wrapper fl" >
                                                        <form action="<?php echo site_url('glomp/acceptVoucher'); ?>" method="POST">
                                                        <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
                                                        <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                        </div>
                                                        <div class="glomp_details fl">
                                                            <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                            <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                            <div  class="glomp_friend_name">
                                                                <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                    <?php echo $glomped_by_name; ?>
                                                                </a>
                                                            </div>
                                                            <div class="glomp_remaining_time"><?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?></div>
                                                        </div>
                                                        <div class="glomp_details_line fl"></div>
                                                        <div class="glomp_buttons_wrapper fr">
                                                            <div class="glomp_buttons">					
                                                                <button type="submit" class="btn-custom-blue2" name="accept" ><?php echo $this->lang->line('glomp_accept', 'accept'); ?></button>
                                                            </div>										
                                                            <div class="glomp_buttons">
                                                                <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                <?php } ?>
                                            <?php } //end pending consumable given by a friend ?>
                                            <?php if ($rec_glomped_me->voucher_type != 'Pending' && $rec_glomped_me->voucher_status != 'Expired') { ?>
                                                    <?php if ($rec_glomped_me->voucher_status == 'Consumable') { ?>
                                                        <div class="glomp_gray_wrapper fl" >								
                                                            <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                            </div>
                                                            <div class="glomp_details fl">
                                                                <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                                <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                                <div  class="glomp_friend_name">
                                                                    <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                        <?php echo $glomped_by_name; ?>
                                                                    </a>
                                                                </div>
                                                                <div class="glomp_remaining_time"><?php echo $remaining_days; ?> remaining <?php //echo $this->lang->line('days_remaining', '{days} remaining'); ?></div>
                                                            </div>
                                                            <div class="glomp_details_line fl"></div>
                                                            <div class="glomp_buttons_wrapper fr">
                                                                <div class="glomp_buttons">					
                                                                     <a href="<?php echo base_url('merchant/about/' . $merchant_id . '?frm=you'); ?>" style="color:#fff !important">
                                                                        <button type="button" class="btn-custom-green btn-block" name="locate" ><?php echo $this->lang->line('glomp_locate', 'locate'); ?></button>											
                                                                    </a>
                                                                </div>										
                                                                <div class="glomp_buttons">
                                                                    <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                            <?php } //end active voucher given by a friend ?> 
                                            <?php if ($rec_glomped_me->voucher_status == 'Redeemed') { ?>
                                                <div class="glomp_gray_wrapper glomp_gray_background fl" >								
                                                    <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                    </div>
                                                    <div class="glomp_details fl">
                                                        <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                        <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                        <div  class="glomp_friend_name">
                                                            <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                <?php echo $glomped_by_name; ?>
                                                            </a>
                                                        </div>
                                                        <div class="glomp_remaining_time_expired">											
                                                            <?php echo $this->lang->line('Redeemed', 'Redeemed'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="glomp_details_line fl"></div>
                                                    <div class="glomp_buttons_wrapper fr">
                                                        <div class="glomp_buttons">
                                                            <a href="<?php echo base_url('merchant/about/' . $merchant_id . '?frm=you'); ?>" style="color:#fff !important">                        	
                                                                <button type="button" class="btn-custom-green btn-block" name="locate" ><?php echo $this->lang->line('glomp_locate', 'locate'); ?></button>											
                                                            </a>
                                                        </div>										
                                                        <div class="glomp_buttons">
                                                            <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'glomp_share'); ?></button>-->
                                                        </div>
                                                    </div>
                                                </div>                                                    
                                            <?php } //end redeemed voucher given by a friend ?>
                                            <?php if ($rec_glomped_me->voucher_status == 'Expired') { ?>
                                                <div class="glomp_gray_wrapper glomp_gray_background fl" >
                                                    <div class="glomp_thumb_item fl" style="background-image:url('<?php echo $product_logo; ?>');">
                                                    </div>
                                                    <div class="glomp_details fl">
                                                        <div class="glomp_merchant"><?php echo $merchant_name; ?></div>
                                                        <div class="glomp_item"><?php echo $prod_name; ?></div>
                                                        <div  class="glomp_friend_name">
                                                            <a href="<?php echo $glomped_by_name_link; ?>" style="color:#136BB3">
                                                                <?php echo $glomped_by_name; ?>
                                                            </a>
                                                        </div>
                                                        <div class="glomp_remaining_time_expired">											
                                                            <?php echo $this->lang->line('Expired', 'Expired'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="glomp_details_line fl"></div>
                                                    <div class="glomp_buttons_wrapper fr">
                                                        <div class="glomp_buttons">											
                                                            <a href="<?php echo base_url('merchant/about/' . $merchant_id . '?frm=you'); ?>" style="color:#fff !important">                        			                            	<button type="button" class="btn-custom-green btn-block" name="locate" ><?php echo $this->lang->line('glomp_locate', 'locate'); ?></button>
                                                            </a>
                                                        </div>										
                                                        <div class="glomp_buttons">
                                                            <!--<button type="button" class="btn-custom-blue2 btn-block" name="locate" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>-->
                                                        </div>
                                                    </div>
                                                </div>                                          
                                            <?php } //end expired voucher given by a friend ?>
                                        <?php }//end given by a friend ?>
                        <?php }// end for each ?>
                    <?php }if ($ctr_show >= 0) {//if 
                        ?>
                                <div class="glomp_gray_wrapper glomp_gray_background fl" style="width: auto" >																		
                                    <div class="no_glomped" id="no_glomped">
                                        <p> <?php echo $this->lang->line('dashboard_no_glomped_msg', 'Want to be <b>glomp!</b>ed (treated)? Of course you do! Make it easier for your friends to glomp! you by creating your favourites list. Thats a list of your favourite products from your favourite places.') ?> </p>
                                        <br />
                                        <a href="<?php echo base_url('profile'); ?>" class="btn-custom-primary-big btn-block"><?php echo $this->lang->line('Add_Favorites_Now', 'Add Favorites Now'); ?></a>
                                        <br /> <br />
                                        <p> <?php echo $this->lang->line('dashboard_no_glomped_msg_2', 'And of course, the more friends you have, the more chance you will be <b>glomp!</b>!ed! So add more friends now.') ?></p> <br /> <br />
                                        <a href="<?php echo site_url('user/searchFriends?showall=1'); ?>" class="btn-custom-primary-big btn-block"><?php echo $this->lang->line('Add_Friends', 'Add Friends'); ?></a> <br />
                                    </div>
                                </div>
                    <?php } ?>
                            </div>	
                        </div>
                    </div>
                </div>	
            </div>		
        </div>
    </div>
</div>
<script>
    
</script>
</body>
</html>
