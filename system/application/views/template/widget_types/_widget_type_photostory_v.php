<div class="widget_holder" <?php echo $widget_id; ?> <?php echo $widget_type; ?>>
    <div class ="widget_tool_tip" style="position: relative;top: -15px;width: 40px;left: 33px; display:none;">
        <div align="center" style="cursor: pointer; height: 8px; padding: 4px; width: 10px; float: left;" class="btn ui-state-default ui-corner-all ui-icon ui-icon-closethick templating_widget_delete" /></div>
        <div align="center" style="cursor: pointer; height: 8px; float: left; padding: 4px; width: 8px;" class="btn ui-state-default ui-corner-all ui-icon ui-icon-gear templating_widget_edit" /></div>
    </div>
    <div class="caption" style="display:none">Photo Story</div>
    <div class="preview"><?php echo (isset($widget_template))?$widget_template:'';?></div>
</div>
