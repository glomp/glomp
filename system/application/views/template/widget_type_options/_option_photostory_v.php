<form id="widget_form" action="<?php echo $widget_edit_save; ?>" enctype="multipart/form-data" method="post" ci-tpl="_option_photostory">
    <input type="hidden" id="widget_option_id" name ="widget_option_id" value="<?php echo $widget_option_id; ?>"/>
    <input type="hidden" id="widget_type" name="widget_type"  value="photostory" />
    <div class="field-set">
        <label class="field-name">Title:</label>
        <div class="field-tag"><input type="text" name="title" value="<?php echo $title; ?>" /></div>
    </div>
    <div class="field-set">
        <label class="field-name">Sub Title:</label>
        <div class="field-tag"><input type="text" name="subtitle" value="<?php echo $subtitle; ?>" /></div>
    </div>
	
	
	<div class="field-set">
        <label class="field-name">Brand Product ID:</label>
        <div class="field-tag"><input type="text" name="brand_product_button_id" value="<?php echo $brand_product_button_id; ?>" /></div>
    </div>
	
	
    <div class="field-set">
        <label class="field-name">Image:</label>
        <div class="field-tag"><input type="hidden" name="image" value="<?php echo $image; ?>" /></div>
        <div class="field-tag"><input type="file" name="upload" /></div>
    </div>
    <div class="field-set">
        <label class="field-name">Context:</label>
        <div class="field-tag"><textarea name="context" style="display:none"><?php echo $context; ?></textarea>
            <div id="context-preview" style="font-size:0.8em;background:#fff;border:1px solid #bbb;padding:2px;cursor:pointer;max-height:160;overflow:auto" title="Click to Edit"><?php echo $context; ?></div>
        </div>
    </div>
    <div class="field-set">
        <label class="field-name">Theme:</label>
        <div class="field-tag">
            <div class="options">
                <div class="field"><input type="radio" name="bg_color" value="grey" <?php echo ($bg_color=='grey')?'checked':'';?> id="bg_color-grey" /><label for="bg_color-grey">Grey</label></div>
                <div class="field"><input type="radio" name="bg_color" value="lightgrey" <?php echo ($bg_color=='lightgrey')?'checked':'';?> id="bg_color-lightgrey" /><label for="bg_color-lightgrey">Lightgrey</label></div>
                <div class="field"><input type="radio" name="bg_color" value="white" <?php echo ($bg_color=='white')?'checked':'';?> id="bg_color-white" /><label for="bg_color-white">White</label></div>
            </div>
        </div>
    </div>
    <div class="field-set">
        <label class="field-name">Layout:</label>
        <div class="field-tag">
            <div class="options">
                <div class="field"><input type="radio" name="type" value="left" <?php echo ($type=='left')?'checked':'';?> id="type-left" /><label for="type-left">Left</label></div>
                <div class="field"><input type="radio" name="type" value="right" <?php echo ($type=='right')?'checked':'';?> id="type-right" /><label for="type-right">Right</label></div>
            </div>
        </div>
    </div>
    <input type="button" value="Save" id="save" />
</form>

<script src="/assets/backend/lib/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="/assets/backend/lib/ckeditor/adapters/jquery.js" type="text/javascript"></script>

<script>
    jQuery(document).ready(function(){
    
        jQuery('#widget_form').ajaxForm({
            dataType: 'json',
            async: false,
            success: function( response ) {
                if (response.success) {
                    $('#option_message').text(response.message);
                    $('#option_message').fadeIn('5000', function(){
                        $(this).fadeOut('5000');
                    });
                    console.log(response.widget.template);
                    var widgetHolder = jQuery('div.widget_holder[data-widget_id='+response.widget.info.id+'_'+response.widget.data.id+']');
                    var widgetPreview = widgetHolder.find('.preview');
                    var widgetCaption = widgetHolder.find('.caption');
                    
                    widgetCaption.hide();
                    widgetPreview.html(response.widget.template).show();
                }
            }
        });
        
        jQuery('#save').on('click',function(){
            jQuery('#widget_form').submit();
        });
        jQuery('input[type=text], textarea').on('focusout',function() {
            jQuery('#widget_form').submit();
        });
        jQuery('input[type=radio]').on('change',function(){
            jQuery('#widget_form').submit();
        });
        
        jQuery('#context-preview').on('click',function(){
            var Context = jQuery('[name=context]');
            var EditorDialog = jQuery('<div />');
            var EditorArea = jQuery('<textarea />');
            EditorDialog.dialog({
                autoOpen: false,
                resizable: false,
                title: 'Editor',
                modal: true,
                draggable: false,
                width: 240,
                minHeight: 500,
                buttons: {
                    'Save' : function(){
                        Context.html(EditorArea.val());
                        jQuery('#context-preview').html(EditorArea.val());
                        jQuery('#widget_form').submit();
                        EditorDialog.dialog('close');
                    },
                    'Close': function(){
                        EditorDialog.dialog('close');
                    }
                }
            });
            EditorArea.html(Context.val());
            EditorDialog.parents('.ui-dialog').css({'font-size':'0.7em'});
            EditorDialog.parents('.ui-dialog').find('.ui-dialog-titlebar').hide();
            EditorDialog.html(EditorArea);
            EditorArea.ckeditor({
                toolbar: [ { name: 'basicstyles', items: [ 'Bold', 'Italic' ] } ],
                height: '400'
            });
            EditorDialog.dialog('open');
        });
        
    });
</script>
