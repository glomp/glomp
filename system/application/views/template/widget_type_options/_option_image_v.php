<form id="widget_form" action="<?php echo $widget_edit_save; ?>" enctype="multipart/form-data" method="post">
    <input type="hidden" id="widget_option_id" name ="widget_option_id" value="<?php echo $widget_option_id; ?>"/>
    <input type="hidden" id="widget_type" name="widget_type"  value="image" />
    <div class="field-set">
        <label class="field-name">Source:</label>
        <div class="field-tag"><input type="hidden" name="source" value="<?php echo $source; ?>"></div>
        <div class="field-tag"><input type="file" name="upload" /></div>
    </div>
    <input type="button" value="Save" id="save" />
</form>

<script>
    jQuery(document).ready(function(){
    
        jQuery('#widget_form').ajaxForm({
            dataType: 'json',
            async: false,
            success: function( response ) {
                if (response.success) {
                    $('#option_message').text(response.message);
                    $('#option_message').fadeIn('5000', function(){
                        $(this).fadeOut('5000');
                    });
                    console.log(response.widget.template);
                    var widgetHolder = jQuery('div.widget_holder[data-widget_id='+response.widget.info.id+'_'+response.widget.data.id+']');
                    var widgetPreview = widgetHolder.find('.preview');
                    var widgetCaption = widgetHolder.find('.caption');
                    
                    widgetCaption.hide();
                    widgetPreview.html(response.widget.template).show();
                }
            }
        });
        
        jQuery('#save').on('click',function(){
            jQuery('#widget_form').submit();
        });
        jQuery('input[type=text], textarea').on('focusout',function() {
            jQuery('#widget_form').submit();
        });
        jQuery('input[type=radio]').on('change',function(){
            jQuery('#widget_form').submit();
        });
    });

</script>
