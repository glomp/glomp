<form id="widget_form" action="<?php echo $widget_edit_save; ?>" >
    <input type="hidden" id="widget_option_id" name ="widget_option_id" value="<?php echo $widget_option_id; ?>"/>
    <input type="hidden" id="widget_type" name="widget_type"  value="title" />
    <div class="field-set">
        <label class="field-name">Text:</label>
        <div class="field-tag"><textarea name="main_text"><?php echo $main_text; ?></textarea></div>
    </div>
    <div class="field-set">
        <label class="field-name">Sub text:</label>
        <div class="field-tag"><textarea name="sub_text"><?php echo $sub_text; ?></textarea></div>
    </div>
</form>

<script>
    $('input, textarea').focusout(function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: $('#widget_form').serialize(),
            async: false,
            url: $('#widget_form').attr('action'),
            success: function(response) {
                if (response.success) {
                    $('#option_message').text(response.message);
                    $('#option_message').fadeIn('5000', function(){
                        $(this).fadeOut('5000');
                    });
                    
                    var widgetHolder = jQuery('div.widget_holder[data-widget_id='+response.widget.info.id+'_'+response.widget.data.id+']');
                    var widgetPreview = widgetHolder.find('.preview');
                    var widgetCaption = widgetHolder.find('.caption');
                    
                    widgetCaption.hide();
                    widgetPreview.html(response.widget.template).show();
                }
            }
        });
    });
</script>
