<div class="section_holder tpl-section one-col" <?php  echo $section_id; ?>>
    <div class="tpl-section-col">
        <div data-position ="1" align="center" style="<?php echo $display_add; ?>" class="btn ui-state-default ui-corner-all templating_widget_add">+</div>
        <?php echo $widget; ?>
    </div>

    <div style="clear: both"></div>

    <div class ="section_tool_tip">
        <div align="center" class="btn ui-state-default ui-corner-all ui-icon ui-icon-closethick templating_section_delete tpl-section-option"></div>
        <div align="center" class="btn ui-state-default ui-corner-all ui-icon ui-icon-arrow-4 templating_section_move tpl-section-option"></div>
        <div style="clear: both"></div>
    </div>
</div>
