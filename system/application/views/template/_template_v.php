<!-- ci-tpl=_template -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/stylesheets/theme.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/lib/jquery-qtip/jquery.qtip.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url($theme); ?>">
<link rel="stylesheet" href="/assets/backend/css/jquery-ui.min.css">
<script src="<?php echo base_url('assets/backend/lib/jquery-1.10.2.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/frontend/js/jquery-ui-1.10.3.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/lib/jquery-qtip/jquery.qtip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/lib/templating/general.js'); ?>"></script>
<script src="<?php echo base_url('assets/backend/lib/jquery.form.min.js'); ?>"></script>
<style type="text/css">
    .tpl-section {
        margin: 0 0 10px 0;
        min-height: 50px;
        width:560px;
        width:100%; /* extra width is for the move and delete options */
        box-sizing: border-box;
    }
    .tpl-section:hover {
        background:#f5f5f5;
    }
    .tpl-section .tpl-section-option {
        cursor: pointer;
        height: 8px;
        padding: 4px;
        width: 8px;
        float:left;
    }
    .tpl-section .tpl-section-col {
        margin-left: 10px;
        background: #fff;
        box-sizing: border-box;
        float:left;
        min-height:50px;
    }
    .tpl-section .tpl-section-col .btn {
        cursor: pointer;
        width: 16px;
        height: 16px;
    }
    .tpl-section .tpl-section-col:first-child {
        margin-left:0;
    }
    .tpl-section.one-col .tpl-section-col {
        width:560px;
    }
    .tpl-section.two-col .tpl-section-col {
        width:275px;
    }
    .tpl-section.two-one-col .tpl-section-col:first-child {
        width:370px;
    }
    .tpl-section.two-one-col .tpl-section-col:nth-child(2) {
        width:180px;
    }
    .tpl-section.three-col .tpl-section-col {
        width:180px;
    }
    .tpl-section {
        position: relative;
    }
    .section_tool_tip {
        position: absolute;
        width: 40px;
        display: none;
        top:0;
        right:0;
    }
    .section_tool_tip:after {
        content: " ";
        display: block;
        clear: both;
    }
    .templating_section_move {
        margin-left: 4px;
    }
    .widget_holder {
        min-height:30px;
    }
    .widget_holder:hover {
        background:#ddd;
    }
    /* For Widgets */
            .preview {
                font-size: 0.8em;
            }
            .widget-title-head,
            h3.widget-title-head{
                margin:0px;
                font-size:1.3em;
                line-height:1em;
                font-weight:normal;
            }
            .widget-title p {
                line-height:1.8em;
                font-size:0.9em;
            }
            .widget-title-sub {
                margin:0px;
                font-size:0.9em;
                font-weight:bold;
                letter-spacing:0.1em;
            }
            .widget-photostory img{
                width:100%
            }
            .widget-photostory .widget-title {
                padding:6px 8px 2px;
            }
            .widget-photostory-body {
                margin-top:10px;
            }
            .widget-photostory-body:after {
                margin-top:10px;
                content:" ";
                display:block;
                clear:both;
            }
            .widget-photostory-image {
                width:370px;
            }
            .widget-photostory-context {
                width:180px;
            }
            .widget-photostory.right .widget-photostory-image {
                float:right;
            }
            .widget-photostory.right .widget-photostory-context {
                float:left;
            }
            .widget-photostory.left .widget-photostory-image {
                float:left;
            }
            .widget-photostory.left .widget-photostory-context {
                float:right;
            }
            .widget-title {
                border-bottom:0;
            }
            .widget-photostory.grey .widget-title,
            .widget-photostory.grey .widget-title h3,
            .widget-photostory.grey .widget-photostory-context {
                background:#666;
                color:#fff !important;
            }
            .widget-photostory.lightgrey .widget-title,
            .widget-photostory.lightgrey .widget-title h3,
            .widget-photostory.lightgrey .widget-photostory-context {
                background:#ddd;
                color:#333 !important;
            }
            .widget-photostory.white .widget-title,
            .widget-photostory.white .widget-title h3,
            .widget-photostory.white .widget-photostory-context {
                background:#f0f0f0;
                color:#888 !important;
            }
            .widget-image img {
                width:100%;
            }

    /* form fields */
    .options .field label,
    .options .field input {
        float:left;
    }
    .options .field:after {
        content: " ";
        display:block;
        clear:left;
    }
    .options .field label {
        font-size:0.8em;
    }
    .field-set {
        margin-bottom:10px;
    }
    .field-set .field-name {
        font-size:0.8em;
        font-weight:bold;
    }

</style>
<?php

?>

<div id="templating_content" style="margin: 10px;">
    <div style="float: left; width: 73%; overflow-y:scroll; height:545px;padding:25px 0px 0px 2px; border: 10px solid #ddd; border-right:0">
        <div id="sections_div" style="margin-top: 10px;"><?php echo $components; ?></div>
        <div align="center" style="cursor: pointer;width: 15px;height: 15px;padding: 5px;"class="btn ui-state-default ui-corner-all templating_section_add" />+</div>
</div>
<div style="float: right; width: 24%;height: 100%;background-color: #F1F1F1;margin-left: 1%;">
    <div id="options_div" style="background:#ddd;padding: 10px 5px; font-size:0.8em; font-weight:bold">
        Settings: <span id="option_message" style="color:green; font-weight: bold; float:right"></span>
    </div>
    <div id="option_holder" style="padding:5px"></div>
</div>
<div style="clear:both"></div>
<div id="templating_section_dialog" style="display:none;">
    <?php foreach ($section_types as $s) { ?>
        <a class="section_types" data-id ="<?php echo $s['id'] ?>" href="#" alt="<?php echo $s['name']; ?>" ><?php echo $s['name']; ?></a> 
        <div style="display:none;" id="section_type_<?php echo $s['id']; ?>">
            <?php echo $s['html']; ?>
        </div>
        <br />
    <?php } ?>
</div>
<div id="templating_widget_dialog" style="display:none;">
    <?php foreach ($widget_types as $wt) { ?>
        <a class="widget_types" data-id ="<?php echo $wt['id'] ?>" href="#" alt="<?php echo $wt['name']; ?>" ><?php echo $wt['name']; ?></a> 
        <div style="display:none;" id="widget_type_<?php echo $wt['id']; ?>">
            <?php echo $wt['html']; ?>
        </div>
        <br />
    <?php } ?>
</div>
<div id="remove_sections" style="display:none;"></div>
<input type="hidden" id="template_id" name="template_id" value ="<?php echo $template_id; ?>"/>
</div>

<script>
    var selected_section;
    var selected_widget_position;
    
    if ( $('#sections_div').children().length > 0) {
        /* When components loaded */
        templating_config.section_delete_save = '<?php echo $actions['section_delete_save']; ?>';
        templating_config.widget_delete_save = '<?php echo $actions['widget_delete_save']; ?>';
        templating_config.widget_load_options = '<?php echo $actions['widget_load_options']; ?>';
        load_components_binding();
    }

    $('#templating_section_dialog').dialog({
        title: 'Select section type:',
        autoOpen: false,
        closeText: "hide",
        open: function(event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: [{
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                }
            }]
    });

    $('#templating_widget_dialog').dialog({
        title: 'Select widget type:',
        autoOpen: false,
        closeText: "hide",
        open: function(event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: [{
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                }
            }]
    });

    $(".templating_section_add").qtip({
        content: 'Add Section',
        position: {
            my: 'top left',
            at: 'bottom right',
        }
    });

    /* Dialog click events */
    $(".templating_section_add").click(function() {
        $('#templating_section_dialog').dialog('open');
    });

    $("#sections_div").sortable({
        update: function(event, ui) {
            var section_ids = [];
            $.each($('#sections_div > .section_holder'), function(key, value) {
                section_ids.push($(value).data('section_id'));
            });

            section_ids = section_ids.join('|');
            /*Save to DB */
            $.ajax({
                type: "POST",
                dataType: 'json',
                data: {
                    section_ids: section_ids
                },
                async: false,
                url: '<?php echo $actions['section_sort_save']; ?>',
                success: function(response) {

                }
            });
        },
    });

    $(".widget_types").click(function(e) {
        e.preventDefault();
        var widget_position = $("div[data-section_id='" + selected_section + "']").find('div[data-position="' + selected_widget_position + '"]');
        var widget_html = $(this).next().html();
        var widget_id;
        var widget_type;
        var selected_section_id = selected_section;
        var selected_widget_position_id = selected_widget_position;
        

        /*Save to DB */
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {
                widget_type: $(this).data('id'),
                section_id: selected_section_id,
                position: selected_widget_position_id
            },
            async: false,
            url: '<?php echo $actions['widget_add_save']; ?>',
            success: function(response) {
                if (response.success) {
                    widget_id = response.widget_id;
                    widget_type = response.widget_type;
                }
            }
        });
        $(widget_position).hide();
        /*Add new widget ID reference */
        $(widget_position).parent().append(widget_html);

        var widget = $(widget_position).parent().children(':last-child');
        $(widget).attr('data-widget_id', widget_id);
        $(widget).attr('data-widget_type', widget_type);

        var widget_del_btn = $(widget).find('.templating_widget_delete');
        var widget_edit_btn = $(widget).find('.templating_widget_edit');
        /* Add widget del qtip */
        $(widget_del_btn).qtip({
            content: 'Delete Widget',
            position: {
                my: 'top left',
                at: 'bottom right',
            }
        });
        /* Add widget edit qtip */
        $(widget_edit_btn).qtip({
            content: 'Edit Widget',
            position: {
                my: 'top left',
                at: 'bottom right',
            }
        });

        /*Bind tooltip events */
        $(widget).bind("mouseenter mouseleave", function() {
            var widget_tool_tip = $(this).children('.widget_tool_tip');
            if ($(widget_tool_tip).is(':hidden')) {
                return $(widget_tool_tip).show();
            } else {
                return $(widget_tool_tip).hide();
            }
        });

        $(widget_del_btn).click(function() {
            $.ajax({
                type: "POST",
                dataType: 'json',
                data: {
                    widget_id: widget_id,
                    widget_type: widget_type
                },
                async: false,
                url: '<?php echo $actions['widget_delete_save']; ?>',
                success: function(response) {
                    if (response.success) {
                        var qtip_widget_del_id = $(widget_del_btn).qtip('api').get('id');
                        var qtip_widget_edit_id = $(widget_edit_btn).qtip('api').get('id');
                        
                        $("div[data-widget_id='" + widget_id + "']").remove();
                        $('#qtip-' + qtip_widget_del_id).remove();
                        $('#qtip-' + qtip_widget_edit_id).remove();
                        
                        $("div[data-section_id='" + selected_section_id + "']").find("div[data-position='" + selected_widget_position_id + "']").show();
                        
                        $('#option_holder').html('');
                    }
                }
            });
        });
        
        
        $(widget_edit_btn).click(function() {
            $('#option_holder').load('<?php echo $actions['widget_load_options']; ?>', {
                widget_id: widget_id,
                widget_type: widget_type
            });
        });

        $('#templating_widget_dialog').dialog('close');

    });

    $(".section_types").click(function(e) {
        e.preventDefault();
        var sec_type = $(this).data('id');
        var html = $(this).next().html();
        var section_id;

        var order = $('#sections_div .section_holder').length + 1 + $('#remove_sections .section_holder').length;

        /*Save to DB */
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {
                type: sec_type,
                order: order,
                template_id: $('#template_id').val()
            },
            async: false,
            url: '<?php echo $actions['section_type_save']; ?>',
            success: function(response) {
                if (response.success) {
                    section_id = response.section_id;
                }
            }
        });
        /*Add new section ID reference */
        var section = $('#sections_div').append(html).children(':last-child');
        $(section).attr('data-section_id', section_id);
        /* Add widget qtip */
        var widget_btn = $(section).find('.templating_widget_add');
        $(widget_btn).qtip({
            content: 'Add Widget',
            position: {
                my: 'top left',
                at: 'bottom right',
            }
        });

        /* Add widget click */
        $(widget_btn).click(function() {
            selected_section = section_id;
            selected_widget_position = $(this).data('position');
            $('#templating_widget_dialog').dialog('open');
        });

        /* Add section close/move qtip */
        var section_del_btn = $(section).find('.templating_section_delete');
        var section_move_btn = $(section).find('.templating_section_move');
        $(section_del_btn).qtip({
            content: 'Remove Section',
            position: {
                my: 'top left',
                at: 'bottom right',
            }
        });
        $(section_move_btn).qtip({
            content: 'Move Section',
            position: {
                my: 'top left',
                at: 'bottom right',
            }
        });

        /*Bind tooltip events */
        $(section).bind("mouseenter mouseleave", function() {
            var section_tool_tip = $(this).children('.section_tool_tip');
            if ($(section_tool_tip).is(':hidden')) {
                return $(section_tool_tip).show();
            } else {
                return $(section_tool_tip).hide();
            }
        });

        $(section).children('.section_tool_tip').children('.templating_section_delete').click(function() {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?php echo $actions['section_delete_save']; ?>',
                data: {
                    section_id: section_id,
                },
                async: false,
                success: function(response) {
                    //remove to DOM
                    var qtip_section_del_id = $(section_del_btn).qtip('api').get('id');
                    var qtip_section_move_id = $(section_move_btn).qtip('api').get('id');
                    var qtip_widget_add_id = $(widget_btn).qtip('api').get('id');
                    $("div[data-section_id='" + section_id + "']").appendTo("#remove_sections");

                    $('#qtip-' + qtip_section_del_id).remove();
                    $('#qtip-' + qtip_section_move_id).remove();
                    $('#qtip-' + qtip_widget_add_id).remove();
                    
                    $('#option_holder').html('');
                }
            });
        });

        $('#templating_section_dialog').dialog('close');
    });

    jQuery('.tpl-section');
</script>
