<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $page_title;?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
   <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-1.9.1.js" type="text/javascript"></script> 



<link href="assets/frontend/css/table/style.css" rel="stylesheet">
<script src="assets/frontend/js/__jquery.tablesorter.min.js" type="text/javascript"></script>

<link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>

<script>
	$(document).ready(function ($) {		
		$("#transactionTable").tablesorter({
			 headers: {				
				 0:{sorter:false} ,
				 1:{sorter:false},
				 2:{sorter:false},
				 3:{sorter:false},
				 5:{sorter:false},
				 6:{sorter:false},
				 7:{sorter:false},
				 8:{sorter:false},
				 9:{sorter:false},
				 11:{sorter:false}
			}
		});		
		$( "#date_from" ).datepicker({
		  maxDate: 0,
		  onClose: function( selectedDate ) {
			$( "#date_to" ).datepicker( "option", "minDate", selectedDate );
		  }
		})
		.on('change', function (e) {
			getReportDetails();
		});
		$( "#date_to" ).datepicker({
		  maxDate: 0,
		  onClose: function( selectedDate ) {
			$( "#date_from" ).datepicker( "option", "maxDate", selectedDate );
		  }
		})
		.on('change', function (e) {
			getReportDetails();
		});
		$('#outletID')
		.on('change', function (e) {
			getReportDetails();
		});
		
		getReportDetails();
	});	
	function checkDate(o){
		if(isDate(o.val())){						
			return true;
		}
		else{			
			alert("Invalid Date");
			return false;
		}
	}				
	function isDate(txtDate)
	{
		var currVal = txtDate;
		if(currVal == '')
			return false;
		
		var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; /*Declare Regex*/
		var dtArray = currVal.match(rxDatePattern); /* is format OK?*/
		
		if (dtArray == null) 
			return false;
		
		/*Checks for mm/dd/yyyy format.*/
		dtMonth = dtArray[1];
		dtDay= dtArray[3];
		dtYear = dtArray[5];        
		
		if (dtMonth < 1 || dtMonth > 12) 
			return false;
		else if (dtDay < 1 || dtDay> 31) 
			return false;
		else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
			return false;
		else if (dtMonth == 2) 
		{
			var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
			if (dtDay> 29 || (dtDay ==29 && !isleap)) 
					return false;
		}
		return true;
	}
	function getReportDetails(){
		$('#transactionTable-last').html("");
		$('#transactionTable > tbody:last').html("");	
		
		var outletID=	$('#outletID').val();
		var dateFrom=	$('#date_from').val();
		var dateTo=		$('#date_to').val();
		
		var tempData	='outletID='+outletID;
			tempData	+='&dateFrom='+dateFrom;
			tempData	+='&dateTo='+dateTo;			
		
		$.ajax({
				type: "POST",
				url: 'ajax_post/getMerchantReportDetails/',
				data:tempData
			}).done(function(response) {
				if(response!=""){		
					response=jQuery.parseJSON(response);
					if(response.status='Ok'){
						var records=(response.data);
						if(records!=''){
							$.each(records, function(i, row){
								var $elem=$('<tr> \
								<td>'+(i+1)+'</td> \
								<td>'+(row.redemption_date)+'</td> \
								<td>'+(row.redemption_time)+'</td> \
								<td>'+(row.outlet)+'</td> \
								<td>'+(row.voucher_num)+'</td> \
								<td>'+(row.product)+'</td> \
								<td>'+(row.verification_num)+'</td> \
								<td>'+(row.purchase_date)+'</td> \
								<td>'+(row.purchase_time)+'</td> \
								<td>'+(row.amount)+'</td></tr>');
								
								$('#transactionTable > tbody:last').append($elem);
							});
							$("#transactionTable").trigger("update"); 
						}
						else{
							var $elem=$('<div>No transaction data addded yet.</div>');
							$('#transactionTable-last').append($elem);					
						}
					}
					else{
						alert(response.status+'\n'+response.message);
					}					
				}
		});
	
	}
</script>




</head>
<body>
	<?php include('includes/header.php')?>
	<div class="container">
		<div class="outerBorder">
			<div style="padding:10px 0px; float:left; border:0px solid #000; ">
				<h4 style="float:left">Transaction:&nbsp;</h4>
				<div style="margin-top:5px;float:left">
					<span class="input-prepend"><span class="add-on" style="width:80px;">Date From</span></span><input type="text" style="width:80px;" id="date_from" value="<?php echo date("m/d/Y");?>" />
				</div>
				<div style="margin-top:5px;float:left">&nbsp;&nbsp;
					<span class="input-prepend"><span class="add-on" style="width:80px;">Date To</span></span><input type="text" style="width:80px;" id="date_to" value="<?php echo date("m/d/Y");?>" />
				</div>			
			</div>
			<div style="padding:10px 0px; float:right; border:0px solid #000; ">
				<h4 style="float:left">Outlet:&nbsp;</h4>
				<select style="width:300px; margin-top:5px" id="outletID">
					<option value="0">All</option>				
					<?php					
						foreach($rec_merchant_outlets->result() as $row){
							echo '<option value="'.$row->outlet_id.'">'.$row->outlet_name.' ['.$row->outlet_code.']</option>';
							
						}					
					?>
				</select>
			</div>
			<table id="transactionTable" class="tablesorter" style="clear:both"> 
				<thead> 
					<tr>
						<th style="background-image:none" colspan="7" align="center">Redemption</th>
						<th style="background-image:none" colspan="2" align="center">Purchase</th>
						<th rowspan="2" align="center">Amount($)</th>
					</tr>
					<tr> 
						<th width="20">#</th>
						<th style="width: 169px;" class="header">Date</th>
						<th style="width: 124px;" >Time</th>
						<th style="width: 273px;" >Outlet</th>
						<th>Voucher No.</th>
						<th style="width: 442px;">Product</th>
						<th style="width: 90px;" >Verification No.</th>
						<th style="width: 143px;" class="header">Date</th>
						<th style="width: 85px;" >Time</th>										
					</tr> 				
				</thead> 
				<tbody>   				
				</tbody> 
			</table>
			<div id="transactionTable-last"></div>
		</div>
	</div>
</body>
</html>