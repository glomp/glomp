<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="<?php echo base_url(); ?>" />
	
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/lib/html5.js"></script>
    <![endif]-->
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    
    <div class="row-fluid login">
    <div class="dialog">
       <div class=" text-center">
       	<img src="<?php echo base_url(); ?>/assets/images/logo-small.png"><h4>Merchant's Portal</h4></div>
        <div class="block login-head">
            <div class="block-header login-head">
                <h2>Sign In</h2>
            </div>
            <?php 
				echo form_open(MERCHANT_FOLDER.'/login/validate_credentials','login_form');
			?>
                <?php if($msg!="") {?>
                 <div class="alert alert-error">
                     <i class="icon-exclamation-sign"></i> <?php echo urldecode($msg);?>
                 </div>
             <?php }?>
                <label>Username/Email</label>
                <input type="text" name="username" required class="span12">
                <label>Password</label>
                <input type="password" required name="password" class="span12">
               <?php
			 if($login_attempt>5){
			 ?>
              <label>Captcha</label>
                <input type="text" required name="image_token" required class="span12">
            <img src="<?php echo base_url(MERCHANT_FOLDER.'/login/captcha');?>" id="captcha" width="250" /><div>
            <a href="javascript:void(0)" onclick="document.getElementById('captcha').src='<?php echo base_url(MERCHANT_FOLDER.'/login/captcha');?>?'+Math.random();
                        document.getElementById('captcha-form').focus();"
                        id="change-image"><?php echo $this->lang->line('Not_readable_Change_text');?>Not Readable</a></div>
                  <?php 
			 }
				  ?>
                    <?php if($this->session->flashdata('err')) {?>
                 <div class="alert alert-error">
                     <i class="icon-exclamation-sign"></i> <?php echo $this->session->flashdata('err');?>
                 </div>
             <?php }?>
                <div class="form-actions">
                    <button class="btn-WI pull-right">Sign In</button>
                    <div class="clearfix"></div>
                </div>
            </form>
            
        </div>
    </div>
</div>

	<script src="lib/bootstrap/js/bootstrap.js"></script>
   
  </body>
</html>


