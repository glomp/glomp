<div class="header"><div class="container">
	<div class="inner-content">
		<div class="row-fluid">
			<div class="span4">
				<a href="<?php echo base_url(); ?>merchantControl/dashboard">
					<img src="assets/frontend/img/logo-small.png" alt="Glomp Logo" />
				</a>
			</div>      
			<div class="span8 innerTopNav">
				<span class="cusMenuHolder pull-right" style="margin-right:10px; border:0px solid" >
					<span class="user_name ">
						<a href="javascript:void(0);" ><?php echo $this->session->userdata('merchant_username') ?></a>
					</span> 					
					<div class="cusMenu pull-left" style="width:60px;margin-left:0px; border:0px solid; float:right !important" align="center">
						<script language="javascript" type="application/javascript">
							$(document).ready(function() {
								$(".cusMenuHolder").on({
									mouseenter: function (e) {
										e.stopPropagation();
										$(".cusMenu").show();				
									/*stuff to do on mouse enter*/
								},
								mouseleave: function (e) {
									e.stopPropagation();
									/*	$(".cusMenu").hide();				*/
									/*stuff to do on mouse leave*/
								}
								});
								$('#user_menu li').click(function(e) {
									var href = $(this).find('a').attr('href');
									window.location=href;
								});
								$('#user_menu li').on({
									mouseenter: function (e) {
										e.stopPropagation();
										$(".cusMenu").show();				
									/*stuff to do on mouse enter*/
								},
								mouseleave: function (e) {
									e.stopPropagation();
										$(".cusMenu").hide();
									/*stuff to do on mouse leave*/
								}
								});
								$("body").click(function(){
									$(".cusMenu").hide();
								});
							});
					</script>
						<ul id="user_menu"  >
							<li><?php echo anchor(MERCHANT_FOLDER.'/dashboard/account','Account');?> </li>														
							<li><?php echo anchor(MERCHANT_FOLDER.'/login/logOut',$this->lang->line('log_out'));?> </li>
						</ul>
					</div>
				</span>
			</div>
		</div>		
	</div>	
</div>