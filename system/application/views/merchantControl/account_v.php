<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $page_title;?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
   <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-1.9.1.js" type="text/javascript"></script> 



<link href="assets/frontend/css/table/style.css" rel="stylesheet">
<script src="assets/frontend/js/__jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>

<link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>

<script>
	$(document).ready(function ($) {				
	});
</script>


</head>
<body>
	<?php include('includes/header.php')?>
	<div class="container">
		<div class="outerBorder">
			<div class="inner-content">				
				<div class="row-fluid">
					<div class="span5" style="border:0px solid">						
						<h4>Account Settings	</h4>
						<hr>
						<?php
						if(isset($msg) && $for=="")
						  {
							  echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $msg . "</div>";							  
						  }
						  if(isset($error_msg) && $for=="")
						  {
							  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error_msg . "</div>";
						  }
						  if(validation_errors()!="" && $for=="")
						  {
							echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . validation_errors() . "</div>";							  
						  }
						?>
						<form method="post" action="">
							<table id="accountTable" class="tablesorter" style="clear:both" cellspacing="3"> 							
								<tbody>   			
									<tr>
										<th><strong>Merchant Name<strong></th>
										<td style="border:0px"><?php echo $merchant_record->merchant_name;?></td>
									</tr>
									<tr>
										<th><strong>Email/Username<strong></th>
										<td style="border:0px"><?php echo $this->session->userdata('merchant_username');?></td>
									</tr>
									<tr>
										<th><strong>Old Password<strong></th>
										<th><input type="password" name="pword" id="pword" required maxlength="9" style="width:220px;" /></th>
									</tr>
									<tr>
										<th><strong>New Password<strong></th>
										<th><input type="password" name="npword" id="npword" required maxlength="9" style="width:220px;" /></th>
									</tr>
									<tr>
										<th><strong>Confirm New Password<strong></th>
										<th><input type="password" name="cnpword" id="cnpword" required maxlength="9" style="width:220px;" /></th>
									</tr>
									<tr>									
										<th align="right" colspan="2"><button type="submit" style="margin-right:5px;width:100px; !important" class="btn-custom-darkblue " name="update" id="update" >Update</button></th>
									</tr>
								</tbody> 
							</table>
						</form>
					</div>
                    
                    <?php
						if($this->session->userdata('is_merchant_users')==false)
						{
					?>
					<div class="span7" style="border-left:0px solid;padding:0px 8px 8px 8px;background-color:#FEF9F7">
						<!--users-->
						<h4 class="span5" style="">Users</h4>						
						<div class="span7" align="right" style="padding-top:10px">
							<a href="<?php echo base_url(MERCHANT_FOLDER.'/dashboard/account/add');?>"><button type="button" style="margin-right:5px;width:90px; !important" class="btn-custom-green " name="update" id="update" >Add New</button></a>
							<a href="<?php echo base_url(MERCHANT_FOLDER.'/dashboard/account');?>"><button type="button" style="margin-right:5px;width:90px; !important" class="btn-custom-green " name="update" id="update" >View All</button></a>
						</div>
						<hr style="clear:both" />
						<?php
						if(isset($msg) && $for=="users")
						  {
							  echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $msg . "</div>";							  
						  }
						  if(isset($error_msg) && $for=="users")
						  {
							  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error_msg . "</div>";
						  }
						  if(validation_errors()!="" && $for=="users")
						  {
							echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . validation_errors() . "</div>";							  
						  }
						?>
						<?php 
						if($view=="add")
						{ 
							$fname 		= ($this->session->userdata('temp_fname'));														
							$lname 		= ($this->session->userdata('temp_lname'));
							$username 	= ($this->session->userdata('temp_username'));
							
							$array_items = array(        
								'temp_fname' => '',
								'temp_lname' => '',
								'temp_username' => ''
							);
							$this->session->unset_userdata($array_items);
						
						?>
							<h5>Add New User</h5>
							<form method="post" action="">
								<table id="accountTable" class="tablesorter" style="clear:both;background-color:#FEF9F7" cellspacing="3"> 							
									<tbody>   			
										<tr>
											<th><strong>First Name<strong></th>
											<th><input type="text" name="fname" id="" required  style="width:220px;" value="<?php echo $fname;?>" /></th>
										</tr>
										<tr>
											<th><strong>Last Name<strong></th>
											<th><input type="text" name="lname" id="" required  style="width:220px;"value="<?php echo $lname;?>" /></th>
										</tr>
										<tr>
											<th><strong>Username<strong></th>
											<th><input type="text" name="username" id="" required  style="width:220px;"value="<?php echo $username;?>" /></th>
										</tr>
										<tr>
											<th><strong>Password<strong></th>
											<th><input type="password" name="pword" id="pword" required maxlength="9" style="width:220px;" /></th>
										</tr>										
										<tr>
											<th><strong>Confirm Password<strong></th>
											<th><input type="password" name="cpword" id="cpword" required maxlength="9" style="width:220px;" /></th>
										</tr>
										<tr>									
											<th align="right" colspan="2"><button type="submit" style="margin-right:5px;width:100px; !important" class="btn-custom-green " name="user_add" id="user_add" >Add</button></th>
										</tr>
									</tbody> 
								</table>
							</form>
						<?php }
						else if($view=="edit")
						{ 
							$fname 		= ($this->session->userdata('temp_fname'));														
							$lname 		= ($this->session->userdata('temp_lname'));
							$username 	= ($this->session->userdata('temp_username'));
							
							$array_items = array(        
								'temp_fname' => '',
								'temp_lname' => '',
								'temp_username' => ''
							);
							$this->session->unset_userdata($array_items);
						?>
                        	<h5>Update User</h5>
							<form method="post" action="">
								<table id="accountTable" class="tablesorter" style="clear:both;background-color:#FEF9F7" cellspacing="3"> 							
									<tbody>   			
										<tr>
											<th><strong>First Name<strong></th>
											<th><input type="text" name="fname" id="" required  style="width:220px;" value="<?php echo $user_rec->merchant_user_fname;?>" /></th>
										</tr>
										<tr>
											<th><strong>Last Name<strong></th>
											<th><input type="text" name="lname" id="" required  style="width:220px;"value="<?php echo $user_rec->merchant_user_lname;?>" /></th>
										</tr>
										<tr>
											<th><strong>Username<strong></th>
											<td style="border:0px"><?php echo $user_rec->merchant_username;?></td>
										</tr>
										<tr>
											<th><strong>Old Password<strong></th>
											<th><input type="password" name="pword" id="pword"  maxlength="9" style="width:220px;" /></th>
										</tr>										
										<tr>
											<th><strong>New Password<strong></th>
											<th><input type="password" name="npword" id="npword"  maxlength="9" style="width:220px;" /></th>
										</tr>
                                        <tr>
											<th><strong>Confirm New Password<strong></th>
											<th><input type="password" name="cnpword" id="cnpword"  maxlength="9" style="width:220px;" /></th>
										</tr>
										<tr>									
											<th align="right" colspan="2"><button type="submit" style="margin-right:5px;width:100px; !important" class="btn-custom-green " name="user_update" id="user_update" >Update</button></th>
										</tr>
									</tbody> 
								</table>
							</form>
						<?php }
						else if($view=="")
						{ 
							if(isset($merchantUsersLists) && $merchantUsersLists->num_rows()>0)
							{ ?>
								<table id="usersTable" class="tablesorter" style="clear:both"> 
                                    <thead> 						
                                        <tr> 
                                            <th width="20">#</th>
                                            <th width="">Username</th>
                                            <th width="">First Name</th>
                                            <th width="">Last Name</th>									
                                            <th width="50">Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										$i=0;
										foreach($merchantUsersLists->result() as $user)
										{ 
											$i++;
										?>
                                            <tr> 
                                                <td><?php echo $i;?></th>
                                                <td><?php echo $user->merchant_username;?></th>
                                                <td><?php echo $user->merchant_user_fname;?></th>
                                                <td><?php echo $user->merchant_user_lname;?></th>
                                                <td align="center">
                                                	<?php
													echo anchor(MERCHANT_FOLDER."/dashboard/account/edit/".$user->merchant_user_id, "<i class=\"icon-edit\"></i>","title='Edit' ");													
													?>
                                                </th>
                                            </tr>                                        
										<?php }
									?>                                    			
                                    </tbody> 
                                </table>							
							<?php }
							else
							{ ?>
								<div id="transactionTable-last">No users added yet.</div>
							<?php }
						?>												
						<?php } ?>
						<!--users-->
					</div>
                    <?php }?>
				</div>				
			</div>
		</div>
	</div>
	</body>
</html>