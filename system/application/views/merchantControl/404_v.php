<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Not Found Error</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>

</head>
<body>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      	<div class="register">
                    <div class="innerPageTitleRed">404 Not Found Error!</div>
                    <div class="content">
                        
                      <h1><?php
                      if(isset($error_404))
					  		echo $error_404;
					  ?></h1>
    
    
                    </div>
                </div>
       </div>
      
      </div>
      </div>
</div>
</body>
</html>