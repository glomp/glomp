<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $Page_title;?></title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
    <meta name="keywords" content="<?php echo meta_keywords();?>" >
    <meta name="author" content="<?php echo meta_author();?>" >
	<meta property="og:title" content="glomp!" /> 
	<meta property="og:type" content="profile" />        
    <meta property="og:url" content="<?php echo base_url();?>" />
	<meta property="og:image" content="<?php echo base_url();?>assets/images/FB_invite_icon.png"/>    
    <meta property="og:description"  content="Up your &#39;Like&#39;-ability"/>
    <meta http-equiv="pragma" content="no-cache"> 
	<meta http-equiv="expires" content="0">
  <base href="<?php echo base_url();?>" />

   <!-- Le styles -->
  <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 
  <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
  <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street/'); ?>" rel="stylesheet">
  
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <script type="text/javascript"> 
        var FB_APP_ID										="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
        var LOCATION_REGISTER						="<?php echo site_url('user/register'); ?>";
        var SIGNUP_POPUP_TEXT					="<?php echo $this->lang->line('signup_popup_text');?>";		
        var SIGNUP_WITH_FB_BUT_REG			="<?php echo $this->lang->line('signup_with_fb_but_already_registered');?>";
        var GLOMP_BASE_URL							="<?php echo base_url('');?>";
        var SIGNUP_POPUP_TEXT_VOUCHER	="<?php echo $this->lang->line('signup_popup_text_voucher');?>";
        var WHAT_IS_GLOMP							="<?php echo $this->lang->line('what_is_glomp');?>";
        var WHAT_IS_GLOMP_DETAILS			="<?php echo $this->lang->line('what_is_glomp_details');?>";
        var WHAT_IS_GLOMP_LEARN_MORE	="<?php echo $this->lang->line('learn_more');?>";
        
        var login_click=false;
        
    </script>	
    <!--[if !IE]><!-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/validate.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>        
        <script src="<?php echo minify('assets/frontend/js/custom.landing.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
    <!--<![endif]-->
    
    <!--[if gte IE 9]><!-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/validate.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.landing.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>         
    <!--<![endif]-->   
	
	<!-- the javascript inline code  has been moved the below file-->
	   
	<script type="text/javascript"> 
                var linkedIn = false;
		<?php        
                        if(isset($_GET['linkedInID']) && ! empty($_GET['linkedInID']))
			{
				echo 'var linkedIn = true;';
			}
                        
			if(isset($_GET['fb_action_ids']))
			{
				echo '$(document).ready(function() {	
						$( "#signup-link" ).click();					
				});';
			}
			else if($is_voucher_okay)
			{
				echo '$(document).ready(function() {
                                    setTimeout(function() {
                                        voucherPopup("'.$voucher_from.'");
                                    }, 1500);
				});';			
			}
		?>
            $(document).ready(function() {
            $(".close").click(function() {                     
                $('#error_wrapper').hide();
            });
            $("#user_login").click(function() {                     
                var err="";     
                email  =document.getElementById("inputEmail");
                if($('#inputEmail').val()=='')
                {
                    err="Please enter an email.";                
                }
                else if(email.validity.typeMismatch)                
                {
                   err="Please enter an  email address.";                
                }
                if($('#inputPassword').val()=='')
                    err+="<div>Please enter a password.</div>";                
                    
                if(err!="")
                {
                    $("#error_wrapper").html("<div class=\"alert alert-error\" ><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+err+"</div>");
                    $("#error_wrapper").show();
                    $(".close").click(function() {                     
                        $('#error_wrapper').hide();
                    });
                }                
            });
            
        });
        function validateEmail(email) { 
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
        return re.test(email);
        }
	</script>
    <style>
         body, html {
          height: 100%;
          font-size:12px;
        }
    </style>
</head>
<body style=" background:#D2D7E1;" data-tpl="landing_v">

    
<?php include_once("includes/analyticstracking.php") ?>
<div id="fb-root"></div>
<!--login error -->
<?php
    $login_display='none';
    $verified = $this->session->flashdata('verified');
    if (! empty($verified)) {
            $login_display='';
    }
                    
      if(validation_errors()!="")
      {
          $login_display='';
      }
      if(isset($error_msg))
      {
           $login_display='';
          
        }

    $deactivated = $this->session->flashdata('deactivated');
    if (! empty($deactivated)) {
        $login_display='';
    }
                      
                      
                  ?>
<div id="validation-errors" style="display:none">
    <div id="errors-inner" class="alert alert-error" style="font-size:12px !important; margin: 0px !important;"></div>
</div>
<div id="error_wrapper" style="display:<?php echo $login_display;?>;width:460px;height:885px;padding:100px 250px;position:absolute;z-index:100;top:52px;left:50%;margin-left:-480px;background-color:rgba(0,0,0,0.7);">
    
    <?php
                $verified = $this->session->flashdata('verified');
                if (! empty($verified)) {
                    echo "<div class=\"alert alert-error\" ><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$verified."</div>";
                }
                                
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"alert alert-error\" ><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
                  }
                  if(isset($error_msg))
                  {
                       echo "<div class=\"alert alert-error\"  ><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
                      
                    }

                $deactivated = $this->session->flashdata('deactivated');
                if (! empty($deactivated)) {
                    echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$deactivated."</div>";
                    $this->session->sess_destroy();                    
                }
                      
                      
                  ?>
</div>
<!--login error -->
<div style="width:960px;min-height:1000px;background-color:#525661;;margin:0 auto;">        
    <div class="row-fluid" style=""  >
        <div class="span12 " style="color:#fff;padding-bottom:5px;letter-spacing:0px;font-size:12px;">                        
            <div class="fl" style="color:#fff;;padding:20px 0px 5px 0px;">            
                <div class="fl" style="color:#fff;">            
                        <div class="fl" style="width: 35px;padding-left:20px;padding-top:6px;font-size:13px;margin-bottom:0px;"><b>Login</b></div>
                </div>            
                <div class="fl" style=""  >
                    <?php echo form_open("landing",'name="login_form" class="form-horizontal" id="frmLogin"')?>                
                        <!--<div class="fl" style="margin-bottom:10px;width: 38px;padding-left:20px;padding-top:6px"><?php echo $this->lang->line('E-mail'); ?></div>-->
                        <div class="fl" style=";width: 135px;padding-left:20px;"><input type="email" name="user_name" id="inputEmail" value="<?php //echo $user_name;?>" style="font-size:12px;height: 12px;width: 110px;"  class="" placeholder="Email"></div>
                        <!--<div class="fl" style="margin-bottom:10px;width: 62px;padding-top:6px"><?php echo $this->lang->line('Password'); ?></div> -->
                        <div class="fl" style="width: 135px;"><input type="password" name="user_password" id="inputPassword" value="<?php //echo $user_name;?>" style="font-size:12px;height: 12px;width: 110px;"  class="" placeholder="Password"></div>                    
                        <div class="fl" style="width:88px;padding-top:4px"><a style="font-size:11px;color:white; text-decoration: underline;" href="<?php echo base_url()?>landing/reset_password"><?php echo $this->lang->line('Forgot_Password');?></a></div>
                        <div class="fl">                        
                            <button type="submit" id="user_login" name="user_login" class="btn-custom-blue" style="font-size:12px !important;margin:0px 10px 0px 4px;height:22px;padding:0px !important;background-color:#8BC541 !important;width:50px !important;font-weight:normal !important;font-size:12px !important">login</button>
                        <button type="button" id="Login_Facebook" name="Login_Facebook" class="btn-custom-blue" style="margin-right:10px;font-size:12px !important;height:22px;padding:0px !important;background-color:#3D589A !important;width:137px !important;font-weight:normal !important;font-size:12px !important">
                            <img  style="height:18px;margin-top:4px;float:left;border-radius:4px" src="<?php echo base_url('assets/frontend/img/fb-icon.png');?>" />
                            <div style="margin:3px 0px;border-left:solid 1px #bbb;height:17px;float:left"></div>
                            <div style="margin-top:2px;float:left">&nbsp;login with <b>Facebook</b></div>
                            </button>
                        <button type="button" id="LinkedIN" name="LinkedIN" onclick="liAuth()" class="btn-custom-blue" style="font-size:12px !important;;height:22px;padding:0px !important;background-color:#0184AE !important;width:130px !important;font-weight:normal !important;font-size:12px !important">
                            <img  style="height:18px;margin-top:4px;float:left;border-radius:4px" src="<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>" />
                            <div style="margin:3px 0px;border-left:solid 1px #bbb;height:17px;float:left"></div>
                            <div style="margin-top:2px;float:left">&nbsp;login with <b>LinkedIn</b></div>
                            </button>
                        </div>
                    </form>
                    
                </div>                           
                <div class="cl fl" style=""  >                    
                </div>
            </div>
            <!--login errors-->
            <div class="fl" style="display:none;width:5px;margin-left:20px;padding:12px 0px 4px 0px;border:solid 0px #fff">
                
            </div>
            <!--login errors-->
             <div class=" fr signup-link" style="margin-top:5px;width:100px;padding:20px 5px 5px 10px;color:#FE0000;cursor:pointer;border-left:solid 1px #ccc;" align="left">            
                <a id="signup-link" href="<?php echo base_url('index.php/landing/register');?>">
                    <span style="font-size:15px;letter-spacing:0px;color:#FE0000;font-weight:600">Sign&nbsp;Up!</span>
                </a>
            </div> 
        </div>
        
        
         
            </div>
    <div class="row-fluid" style="width:100%;height:1080px;"  >
        <!--left-->
        <div class="fl btn-toggle-glomp-intro" id="what_is_glomp_button" style="width:32.7%;height:16.76%;background-color: #FF4930;color:  white;cursor:pointer">
            <div style="width: 100%;height:100%;display:table;" align="center">
                <div style="display:table-cell;vertical-align:middle;" align="left">                    
                    <div style="padding:2% 8%;">
                    <h2 style="margin: 0px;line-height: normal;font-size: 20px;letter-spacing:1px;font-weight:normal" class="harabara"><?php echo $this->lang->line('what_is_glomp');?></h2>
                    <div class="cl fl" style="width:100%;margin-top:3%" >
                        <div class="fl" style="width:152px">
                            <div class="body" style=";padding:0% 0%;text-align:justify">
                                <p style="text-align:justify;font-size:12px;line-height:13px">
                                        <?php //echo $this->lang->line('what_is_glomp_details');?>                                        
                                        Discover &#8216;real&#8217; expressions of Like-ing.
                                </p>
                            </div>
                            
                        </div>
                        <div class="fr" align="right" style="margin-top:-30px;padding-left:4px;">                                  
                            <div class="" style="width:60px;height:60px; background-color:#fff;border-radius:100%;-webkit-border-radius:100%;-moz-border-radius:100%;" align="center">
                                <div class="" style="width:56px;height:58px; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_PlayButton.png');?>');background-size: cover;background-position: center;background-repeat:no-repeat;"></div>    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!--right-->
        <div class="fr" style="width:67.3%;height:30.28%; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img1_Temp.png');?>');background-size: cover;background-position: center;background-repeat:no-repeat;">        
            <div class="bubble">
                <div style="padding:0 12%;width:76%;height:100%;display:table;" align="center">                
                    <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;line-height:11px" align="left">                    
                        <div style="margin-top:6px;">
                        Like Sam's post?
                        </div>
                        <div style="margin-top:10px;">
                            Send her a  cupcake to show that you really Like it.
                        </div>
                    </div>
                </div>
            </div>
            <div class="down_triangle"></div>
            <style>                
            .bubble 
            {
                position: relative;
                width: 200px;
                height: 86px;
                padding: 0px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#00FC91;
            }
            .down_triangle { 
               width: 0; 
               height: 0;
               border-top: 30px solid rgba(0,0,0,0.6);
               border-left: 100px solid transparent; 
               border-right: 100px solid transparent; 
            }
            </style>
        </div>
        <!--left-->
        <div class="fl " style="width:32.7%;height:27.31%;background-color: #525661;color: white;">            
            <div style="width: 100%;height:100%;display:table;" align="center">
                <div style="display:table-cell;vertical-align:middle;padding-top:5%" align="center">
                    <div  style="padding:0% 5% 0% 5%;">
                        <img style="width:100%;"src="assets/m/img/logo.png" alt="logo">
                    </div>
                    <div class="" style="padding:0% 5% 0% 5%;color:#fff;text-decoration:none" align="left" >
                        <span class="title " style="letter-spacing:0px;font-size:13px;padding-left:10%;font-family:Arial"><b><?php echo $this->lang->line('Glomp');?>:</b></span> 
                        <span><em><?php echo $this->lang->line('Glompv');?></em></span>
                        <br />
                        <div class="body " style="margin-top:10px;padding:0% 10%;text-align:justify;font-size:12px;line-height:12px;"><p class="Delicious"> <?php echo stripslashes($this->lang->line('Glomp_welcome_message'));?></p></div>
                    </div>                    
                </div>
            </div>
            
        </div>
        <!--center-->
        <div class="fl " id="what_glomp_can_do" style="width:21.1%;height: 13.8%;background-color: #FF8200;color: white;cursor:pointer;overflow:visible;">
            
                <div style="width: 100%;height:100%;display:table;z-index:5;" align="center">
                    <div style="display:table-cell;vertical-align:middle;" align="center">                    
                        <div style="padding:1% 13%;margin-top:-5%">
                            <div class="harabara" style="font-size: 20px;letter-spacing:1px;line-height: normal;font-weight: 100 !important;" align="left">
                                What can glomp! do&nbsp;for you?
                            </div>
                            <div class="" style="margin-top: 5%;font-size:12px;" align="left">A Better experience awaits...&nbsp;&nbsp;&gt;&gt;</div>
                        </div>
                    </div>
                </div>        

    <!--what_glomp_can_do_wrapper -->
    <div class="fl" id="what_glomp_can_do_wrapper" style="cursor:default;display:none;z-index:10;position:relative;margin-top:-149px;width:435px;background-color:#FF8200;" align="center" >        
        <div class="fl" style="padding:29px 26px;">
            <div class="harabara fl" style="font-size: 20px;letter-spacing:1px;line-height: normal;font-weight: 100 !important;" align="left">
                What can glomp! do&nbsp;for you?
            </div>
            <div class="fr">
                <div id="what_glomp_can_do_close" style="margin-top:-15px;margin-right:-15px;border:1px solid #fff;width:30px;height:30px;border-radius:100px;cursor:pointer" align="center">
                    <div style="color:#fff;font-size:30px;font-weight:100;margin-top:4px;margin-left:0px;">x</div>
                </div>
            </div>
            <div class="cl fl" align="" style="margin-top:20px;margin-right:30px;font-size:12px;color:#fff;font-weight:normal">
                <p style="text-align:justify">Simply, <b>glomp!</b> takes you to the next level of digital connectivity.</p>
                <p style="text-align:justify">We all send nice gestures to friends by messaging, sharing content and 'Like'-ing posts. Now with <b>glomp!</b> you can do that little bit more by treating friends to a real product that they'll enjoy.</p>
                <p style="text-align:justify">And to let friends treat you something nice too...</p>
                <p style="text-align:justify">So go ahead and start <b>glomp!</b>ing now!</p>
            </div>
        </div>
    </div>
    <!--what_glomp_can_do_wrapper -->
                
            
        </div>
        <!--right-->
        <div class="fr harabara signup-link" style="width:46.2%;height: 6.9%;background-color: #FE0000;color: white;cursor:pointer">
            <div style="width: 100%;height:100%;display:table;font-size: 20px;letter-spacing:1px;font-weight:normal" align="center">
                <div style="display:table-cell;vertical-align:middle;" align="center">                    
                    <div style="margin-top:2%;">
                        Sign Up!&nbsp;&nbsp;&gt;&gt;
                    </div>
                </div>
            </div>
        </div>        
        <!--right-->
        <div class="fr" style="width:22.0%;height:28.9%; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img4-2.png');?>');background-size: cover;background-position: center top;background-repeat:no-repeat;"></div>        
        <!--right-->
        <div class="fr" style="width:24.2%;height:28.9%; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img3.png');?>');background-size: cover;background-position: center top;background-repeat:no-repeat;" align="right">
            <div class="bubble_1">
                <div style="padding:0 6% 0 14%;width:80%;height:100%;display:table;" align="center">                
                    <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;line-height:11px;" align="left">
                        <div style="padding-top:15px">
                             	&ldquo;Thanks for your advice Jane!&rdquo;
                        </div>
                    </div>
                </div>
            </div>
            <div class="down_triangle_1"></div>
            <style>                
            .bubble_1 
            {
                position: relative;
                width: 134px;
                height: 73px;
                padding: 0px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#BFFF00;
            }
            .down_triangle_1 { 
               width: 0; 
               height: 0;
               border-top: 20px solid rgba(0,0,0,0.6);
               border-left: 67px solid transparent; 
               border-right: 67px solid transparent; 
            }
            </style>
        </div>
        <!--left-->
        <div class="fl" style="width:53.75%;height:28.9%;; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img2.png');?>');background-size: cover;background-position: center top;background-repeat:no-repeat;" align="right">
            <div style="width:96%;height:96%;display:table;padding: 2%;" align="center">
                <div style="display:table-row;">
                    <div align="right" style="float:right">
                        <a href="https://itunes.apple.com/us/app/glomp!/id823405266?mt=8" target="_blank">
                        <img style="width: 186px;border-radius: 10px;" src="<?php echo base_url('assets/images/app_store.png');?>"></a><br>
                        <a href="https://play.google.com/store/apps/details?id=it.glomp" target="_blank">
                        <img style="margin-top:8px;width: 186px;border-radius: 10px;" src="<?php echo base_url('assets/images/google_play.png');?>"></a>
                    </div>
                </div>
                <div style="display:table-row;">
                    <div style="display:table-cell;vertical-align:bottom;" align="right">                    
                        <div class="up_triangle_3"></div>
                        <div class="bubble_3">
                            <div style="padding:0 10%;width:80%;height:100%;display:table;" align="center">                
                                <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;line-height:11px;padding-bottom:15px;" align="center">
                                    <div style="">
                                       &ldquo;That smoothie made my day!&rdquo;
                                    </div>                                
                                </div>
                            </div>                
                        </div>            
                    </div>
                </div>
            </div>            
            <style>                
            .bubble_3 
            {
                position: relative;
                bottom:0px;
                width: 220px;
                height: 74px;
                padding: 0px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#FF7CB3;
            }
            .up_triangle_3 { 
               width: 0; 
               height: 0;
               border-bottom: 32px solid rgba(0,0,0,0.6);
               border-left: 110px solid transparent; 
               border-right: 110px solid transparent; 
            }
            </style>
        </div>
        <!--right-->
        <div class="fr" style="width:46.2%;height:33.8%; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img6_Temp.png');?>');background-size: cover;background-position: center top;background-repeat:no-repeat;">
            <div class="bubble_2">
                <div style="padding:0 15%;width:70%;height:100%;display:table;" align="center">                
                    <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;line-height:11px;text-align:justify;" align="left">                    
                        <div style="margin-top:20px;">
                            Wonder how business is with Mr. Lee?
                        </div>
                        <div style="margin-top:10px;">
                            Let me send him a Latte to touch base.
                        </div>
                    </div>
                </div>                
            </div>
            <div class="down_triangle_2"></div>
            <style>                
            .bubble_2 
            {
                position: relative;
                width: 233px;
                height: 75px;
                padding: 0px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#07E6FF;
            }
            .down_triangle_2 { 
               width: 0; 
               height: 0;
               border-top: 30px solid rgba(0,0,0,0.6);
               border-left: 116px solid transparent; 
               border-right: 117px solid transparent; 
            }
            </style>
        </div>
        <!--left-->
        <div class="fl" style="width:53.75%;height:27%; background-image:url('<?php echo base_url('assets/frontend/img/glomp_Web_Landing_Img5_Temp.png');?>');background-size: cover;background-position: center top;background-repeat:no-repeat;">
            <div style="width:100%;height:100%;display:table;" align="center">                
                <div style="display:table-row;">
                    <div style="display:table-cell;vertical-align:top;" align="right">                        
                        <div class="bubble_5">
                            <div style="padding:0 10% 0 10%;width:80%;height:100%;display:table;" align="center">                
                                <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;line-height:11px;" align="center">                    
                                    <p style="margin-top:20px;">
                                        Can't join the boys to celebrate?
                                    </p>                                
                                </div>
                            </div>                
                        </div>            
                        <div class="down_triangle_5"></div>
                    </div>
                </div>
                <div style="display:table-row;">
                    <div style="display:table-cell;vertical-align:bottom;" align="right">
                        <div class="up_triangle_4"></div>
                        <div class="bubble_4">
                            <div style="padding:0 10%;width:80%;height:100%;display:table;" align="center">                
                                <div style="display:table-cell;vertical-align:middle;font-weight:bold;font-size:11px;" align="center">                    
                                    <div style="margin-top:-4px;">
                                        Congratulate them with a beer.
                                    </div>                                
                                </div>
                            </div>                
                        </div>            
                    </div>
                </div>                
            </div>            
            <style>                
            .bubble_4 
            {
                position: relative;
                bottom:0px;
                width: 220px;                
                height: 63px;                
                padding: 0px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#FFBC05;
            }
            .up_triangle_4 {                 
               width: 0; 
               height: 0;
               border-bottom: 27px solid rgba(0,0,0,0.6);
               border-left: 110px solid transparent; 
               border-right: 110px solid transparent; 
            }
            
            .bubble_5          
            {
                position: relative;
                bottom:0px;
                width: 230px;                
                height: 60px;
                padding: 0px;
                margin-right:220px;
                background: rgba(0,0,0,0.6);
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                color:#FFBC05;
            }
            .down_triangle_5
            { 
               margin-right:220px;
               width: 0; 
               height: 0;
               border-top: 20px solid rgba(0,0,0,0.6);
               border-left: 115px solid transparent; 
               border-right: 115px solid transparent; 
            }
            </style>
        </div>    
    </div>
<div>
<!-- ui-dialog -->
<div id="dialog" title="">
	<div id="dialog-content" style="line-height:120%; font-size:16px" align="center"></div>
</div> 
<!-- what is glomp video-->      
<div class="video-glomp-intro" style="display:none;position:fixed;top:0px;left:0px;background-color:rgba(51,51,51,0.7);height:100%;width:100%;color:#fff;" align="center">
    
    <div style="margin:0 auto; max-width:720px; margin-top:50px;">
        <a href="#" class="btn-circle-green btn-toggle-glomp-intro">x</a>
        <?php
            $res_video = $this->cmspages_m->selectStaticPage(1,DEFAULT_LANG_ID);
            if($res_video->num_rows()>0){
                $rec_video = $res_video->row();
                $video_code =  $this->custom_func->iframeResizer($rec_video->content_content,'100%','400');
                echo html_entity_decode($video_code);
            }
        ?>
        <div style="display:block;width:100%;margin-top:20px;">
            <div>
            <a href="<?php echo base_url();?>page/index/faq#from_landing"
               id="btn-goto-faq" class="btn-custom-green"
               style="padding:10px 30px;font-size:20px;display:inline-block;border-radius:5px !important;box-shadow: 0 0 1px 2px rgba(0,0,0,0.1);text-shadow: 0 0 1px #000000;">
                <?php echo $this->lang->line('frequently_asked_questions','Frequently Asked Questions')?></a>
            </div>
            <!--<div style="float:right">
            <button class="btn-custom-blue"
                style="padding:8px 20px !important;font-size:20px !important;box-shadow: 0 0 1px 2px rgba(0,0,0,0.1) !important;text-shadow: 0 0 1px #000000 !important;">
                <?php echo $this->lang->line('close','Close');?></button>
            </div>-->
        </div>
        <style>
            .btn-circle-green {
                display:block;border-radius:100px;
                width:30px;height:30px;
                border:1px solid #A5FF00;
                font-size:2em;line-height:1.2em;
                position:relative;right:-380px;top:-10px;
                color:#A5FF00;
            }
            .btn-circle-green:hover {
                color: #A5FF00;
                text-decoration: none;
                box-shadow: 0 0 1px 2px rgba(165, 255, 0, 0.1);
            }
        </style>
    </div>
</div>
<script type="text/javascript">
    
    
    jQuery(document).on('click','.btn-toggle-glomp-intro',function(e){
        e.preventDefault();
        jQuery('.video-glomp-intro').slideToggle(300);
    });
</script>
<!-- what is glomp video-->    
    
    
    <script type="text/javascript">
        /* TODO: must be generic  */
        Location.prototype.getParam   = function( key ) {
            var result = false,tmp = [];
            var items = location.search.substr(1).split("&");
            for (var index = 0; index < items.length; index++) {
                tmp = items[index].split("=");
                if (tmp[0] === key) {
                    result = decodeURIComponent(tmp[1]);
                }
            }
            return result;
        };

        jQuery(document).ready(function(){
            if( location.getParam('ref') == 'registered' ) {
                var email = location.getParam('email');
                var msg = 'Please check your welcome email to activate your account.';
                var html  = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'+msg+'</div>';
                jQuery('#error_wrapper').html(html);
                jQuery('#error_wrapper').show();
            }
        });
    </script>
</body>
</html>