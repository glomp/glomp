<?php
/*$fb_config = array(
	'appId'  => ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS)),
	'secret' => ($this->custom_func->config_value('FB_API_APP_SECRET_'.FACEBOOK_API_STATUS)),
	'fileUpload' => true,
	'cookie' => true
);
$this->load->library('facebook', $fb_config);
$facebook = new Facebook($fb_config);
// Get User ID

$user = $facebook->getUser();
*/
// We may or may not have this data based 
// on whether the user is logged in.
// If we have a $user id here, it means we know 
// the user is logged into
// Facebook, but we don’t know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
  try {
    // Proceed knowing you have a logged in user who's authenticated.
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}

// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl 	= $facebook->getLogoutUrl();
} else {
  $loginUrl 		=  $facebook->getLoginUrl(
            array(
                'scope'         => 'publish_actions,email',
                'redirect_uri'  => base_url()
            )
    );
}

// This call will always work since we are fetching public data.
$jiofreed = $facebook->api('/jiofreed');

?>
<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <title>php-sdk</title>
    <style>
      body {
        font-family: 'Lucida Grande', Verdana, Arial, sans-serif;
      }
      h1 a {
        text-decoration: none;
        color: #3b5998;
      }
      h1 a:hover {
        text-decoration: underline;
      }
    </style>
  </head>
  <body>
    <h1>php-sdk</h1>

    <?php if ($user): ?>
      <a href="<?php echo $logoutUrl; ?>">Logout</a>
    <?php else: ?>
      <div>
        Login using OAuth 2.0 handled by the PHP SDK:
        <a href="<?php echo $loginUrl; ?>">Login with Facebook</a>
      </div>
    <?php endif ?>

    <h3>PHP Session</h3>
    <pre><?php print_r($_SESSION); ?></pre>

    <?php if ($user): ?>
      <h3>You</h3>
      <img src="https://graph.facebook.com/<?php echo $user; ?>/picture">

      <h3>Your User Object (/me)</h3>
      <pre><?php print_r($user_profile); ?></pre>
    <?php else: ?>
      <strong><em>You are not Connected.</em></strong>
    <?php endif ?>

    <h3>Public profile of jiofreed</h3>
    <img src="https://graph.facebook.com/jiofreed/picture">
    <?php echo $jiofreed['name']; 
		echo $jiofreed['id'];
	?>
	
  </body>
</html>