<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $rec_privacy->content_title;?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="description" content="Up your 'Like'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
  <meta name="keywords" content="<?php echo meta_keywords($rec_privacy->content_id);?>" >
  <meta name="author" content="<?php echo meta_author();?>" >
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script>
	$(document).ready(function(e) {
		$('#start_tour').on('click',function(e){				
				var stat='Y';
				data='&tour_stat='+stat;
				$.ajax({
					type: "POST",	
					dataType:'json',							
					url: 'ajax_post/setTourStat',
					data:data,
					success: function(response){				
						window.location.href='<?php echo base_url();?>user/dashboard';
							
					}
				});
			});		
		 
		});
	
</script>
</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      <div class="innerBorder2">
      	<div class="register">
                    <div class="innerPageTitleWhite glompFont"><?php echo $rec_privacy->content_name;?>
                    <?php
					if($rec_privacy->content_cms_id==3)
					{
					?>
                    <div class="pull-right cmsPage">
                    <?php 
					if($this->session->userdata('user_id'))
					{
					?>
						<a href="javascript:void(0);" id="start_tour" class="btn-custom-gray-suppoetLink"><?php echo $this->lang->line('Tour', 'Tour');?></a>
						&nbsp;
						<a href="<?php echo base_url('page/support');?>" class="btn-custom-gray-term"><?php echo $this->lang->line('Support', 'Support');?></a>
                    &nbsp;
                   <a href="<?php echo base_url('page/index/terms');?>" class="btn-custom-gray-suppoetLink"><?php echo $this->lang->line('termNcondition', 'Terms');?></a>
                   <?php }?>

                      <?php
                        if($rec_privacy->content_id!=4)
                        {
                        ?>
                        &nbsp;&nbsp;<a href="<?php echo base_url('page/index/policy');?>" class="btn-custom-gray-suppoetLink"><?php echo $this->lang->line('privacyPolicy', "Privacy Policy");?></a>
                        <?php }?>

                   </div>
                   <?php }?>
                   </div>
                    <div class="content <?php if($rec_privacy->content_cms_id==3){echo 'faq_class';} ?>" style="">
                       <?php echo stripslashes($rec_privacy->content_content);?>
                    </div>
                </div>
                </div>
       </div>
      
      </div>
      </div>
</div>
</body>
</html>
