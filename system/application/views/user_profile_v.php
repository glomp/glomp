<script>
    var selectedTab;
    var merID;
<?php
if (isset($_GET['tab'])) {
    echo 'selectedTab="' . $_GET['tab'] . '";';
}
if (isset($_GET['merID']) && is_numeric($_GET['merID'])) {
    echo 'merID="' . $_GET['merID'] . '";';
}
?>
</script>
<?php
$profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic, $user_record->user_gender);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $user_record->user_fname; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->

        <link rel="stylesheet" type="text/css" media="screen" href="assets/frontend/css/als_demo.css" />        
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet">
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery.als-1.2.min.js"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript">
        var GLOMP_BASE_URL  ="<?php echo base_url('');?>";
        var linkedIn  = false;
        var FB_APP_ID       ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
        var loadingMoreStories=false;
	
            $(document).ready(function(e) {
                 <?php
                    $tweet_status = $this->session->flashdata('tweet_status');
                    $tweet_message = $this->session->flashdata('tweet_message');        
                      if (! empty($tweet_status)) {
                          echo "doTweetSucces('".$tweet_status."','".$tweet_message."');";
                      }
                
                ?>


                $(".buzzScroll").nanoScroller();
                $("#tabBuz_wrapper").bind("scrollend", function(e){                    
                    if(!loadingMoreStories)
                    {
                    
                        loadingMoreStories=true;            
                        start_count=$('#glomp_buzz_content').children().last().data().id;
                        if(start_count>0)
                        {                        
                            $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">Loading more stories...<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                            $('#glomp_buzz_content').append($loader);
                            data ='start_count='+start_count+'&profile_id='+<?php echo $buzz_user_id;?>;
                            setTimeout(function() {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'html',
                                    url: 'profile/getMoreStories',
                                    data: data,
                                    success: function(response){ 
                                        if(response!='' &&  response!="<!---->")
                                        {
                                             $('#id_loader').remove();
                                            $('#glomp_buzz_content').append(response);
                                            $(".buzzScroll").nanoScroller();            
                                            loadingMoreStories=false;
                                            $('.glomp_fb_share').unbind('hover');
                                            $('.glomp_fb_share').hover(        
                                                function(e){
                                                    $this=$(this);
                                                    $data=$this.data();                          
                                                    
                                                    if($data.voucher_id in share_wrapper)
                                                    {
                                                     
                                                        clearTimeout( share_wrapper[$data.voucher_id] );                
                                                        delete share_wrapper[$data.voucher_id];
                                                        
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                    
                                                    
                                                    $('.glomp_fb_share').each(function(){
                                                        $this_temp=$(this);
                                                        $data_temp=$this_temp.data();                 
                                                        if($data_temp.voucher_id!=$data.voucher_id){                    
                                                            $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                                                            clearTimeout( share_wrapper[$data.voucher_id] );                
                                                            delete share_wrapper[$data.voucher_id];
                                                        }
                                                        else
                                                        {
                                                            /*$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); */
                                                        }
                                                    });
                                                    $('#share_wrapper_box_'+$data.voucher_id).fadeIn();
                                                     e.stopPropagation();
                                                },
                                                function(){                
                                                    $this=$(this);
                                                    $data=$this.data();   
                                                    if($data.voucher_id in share_wrapper)
                                                    {            
                                                        share_wrapper[$data.voucher_id]= setTimeout(function() {
                                                                            console.log($data.voucher_id);
                                                                            $('#share_wrapper_box_'+$data.voucher_id).fadeOut();
                                                                            delete share_wrapper[$data.voucher_id];                                    
                                                                        }, 500);								
                                                    }            
                                                }
                                            );
                                        }
                                        else
                                        {                                
                                            $('#id_loader').remove();
                                            $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding-bottom:30px;">No more stories to load.</div>');
                                            $('#glomp_buzz_content').append($no_more);
                                        }
                                    }
                                });
                            }, 1);
                        }
                        
                        
                        /*console.log($('#glomp_buzz_content').children().last().data().id);*/
                        /*xx=1;*/
                    }                       
                });
                $(".displyFevPopUp").on('mouseenter', function() {
                    var read_popup = $(this).find('.getPopupDescription').html();
                    var position = $(this).position();
                    
                    if (position.left > 280)
                        position = 300;
                    else
                        position = position.left;
                    $('#lista1').find('.popUp').show().html(read_popup);
                    $('#lista1').find('.popUp').css({'margin-left': position + 'px'});
                }).on('mouseleave', function() {
                    $('.popUp').hide();
                    
                });
                $("#ask_remove_frn").on('click', function(e) {
                    e.preventDefault();
                    $("#removeFrenPopup").show();
                    e.stopPropagation();
                    return false;
                });
                $('._hide').click(function() {
                    $('#removeFrenPopup').hide(100);
                });
            });
        </script>
    <script src="assets/frontend/js/custom.glomp_share.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#lista1").als({
                    visible_items: 6,
                    scrolling_items: 1,
                    orientation: "horizontal",
                    circular: "yes",
                    autoscroll: "no",
                    interval: 5000,
                    direction: "right",
                    start_from: 0
                });


                $(".tabs").hide();
                $("#glomped_inactive").hide();

                $(".tabs:first").show();
                $(".profileTab a").click(function(e) {
                    var activeTab = $(this).data().href;
                    if (activeTab == "#tabBuz")
                    {
                        $("#glomped_active").show();
                        $("#glomped_inactive").hide();
                    }
                    else
                    {
                        $("#glomped_inactive").show();
                        $("#glomped_active").hide();
                    }

                    e.preventDefault();
                    $(".profileTab a").removeClass("activeTab");
                    $(this).addClass("activeTab");
                    $(".tabs").hide();
                    $(activeTab).fadeIn();
                });

                $('.nameOfMerchant').on('click', function(e) {
                    e.preventDefault();
                    var _anchor = $(this).attr('href');
                    $('#merchantProductDetails').load(_anchor, function() {
                        $('#tabMerchant').hide();
                        $('#merchantProductDetails').fadeIn();
                    });

                });

                $('.add_as_friend').click(function(e) {
                    e.preventDefault();

                    
                    var _y = ($(window).height() / 2) - (parseInt($(".addAsFriendPopUp").css("height")) / 2);
                    var _x = ($(window).width() / 2) - (parseInt($(".addAsFriendPopUp").css("width")) / 2);
                    $(".addAsFriendPopUp").css({"margin-top": _y, "margin-left": _x});
                    $('.addAsFriendPopUp').show();
                });

                $('.cancel_friend').click(function() {
                    $('.addAsFriendPopUp').hide();
                });



            });

            $("document").ready(function()
            {


                if (selectedTab != '') {
                    $("#" + selectedTab).trigger('click');
                }
                if (merID != '') {
                    $("#merID_" + merID).trigger('click');
                }
            });
        </script>
        <?php include('includes/glomp_pop_up_merchant.php'); ?>
        <?php include('includes/glomp_pop_up.php') ?>
        <?php include('includes/glomp_pop_up_js.php'); ?>
    </head>
    <body data-tpl="user_profile">
        <?php include_once("includes/analyticstracking.php") ?>
        <div class="addAsFriendPopUp"><h1><?php echo $this->lang->line('add_friend'); ?></h1>
            <div class="thumbHolder"><img src="<?php echo $profile_pic; ?>"  alt="<?php echo $user_record->user_name; ?>"></div>
            <div class="nameHolder">
                <div class="nameHolderInner">
                    <?php echo $user_record->user_name; ?> <br/> <?php
                    echo $this->regions_m->region_name($user_record->user_city_id);
                    ?></div>
                <div class="userInfo">
                    Add <?php echo $user_record->user_name; ?> to Your glomp! network.?
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="btns">
                <button name="Confirm" onClick="javascript:location.href = '<?php echo base_url() . "profile/add_friend/" . $profile_id; ?>'" class="btn-custom-gray" type="button"><?php echo $this->lang->line('Confirm'); ?></button> 
                <button name="Cancel" class="btn-custom-white  cancel_friend" id="cancel_friend" type="button"><?php echo $this->lang->line('Cancel'); ?></button>
            </div>
        </div>
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
                                    <div class="row-fluid">
                                        <div class="userProfileLeft">
                                            <div class="profilePicture">
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div style=" height:150px; overflow:hidden;">
                                                            <img src="<?php echo $profile_pic; ?>"  alt="<?php echo $user_record->user_name; ?>"  style="height:150px;width:150px">
                                                        </div>
                                                    </div>
                                                    <div class="span6" style="padding-left:5px;">
                                                        <div style="height:128px;">
                                                            <div class="name"><?php echo $user_record->user_name; ?></div>
                                                            <?php
                                                            $dob = explode('-', $user_record->user_dob);
                                                            if (count($dob) !== 3) {
                                                                $dob[0] = 0;
                                                                $dob[1] = 0;
                                                                $dob[2] = 0;
                                                            }
                                                            $dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
                                                            if ($user_record->user_dob_display == 'dob_display_wo_year') {
                                                                $dob = date('M d', strtotime($dob));
                                                            } else {
                                                                $dob = date('M d, Y', strtotime($dob));
                                                            }
                                                            if($user_record->user_dob!="")
                                                            {
                                                            ?>
                                                                <div class="dob"><?php echo $this->lang->line('DOB'); ?>: <?php echo $dob; ?>
                                                                </div>
                                                            <?php }?> 
                                                            <div class="address"><?php
                                                            echo $this->regions_m->region_name($user_record->user_city_id);
                                                            ?></div>
                                                        </div>
                                                        <div class="addAsFriens">
                                                            <?php
                                                            $profile_id = $user_record->user_id;
                                                            $user_id = $this->session->userdata('user_id');
                                                            $is_friend = $this->users_m->is_friend($user_id, $profile_id);
                                                            if ($is_friend <= 0 && ($user_record->user_status != 'Pending')) {
                                                                echo anchor("profile/add_friend/" . $profile_id, ucwords(strtoupper($this->lang->line('')) . 'ADD&nbsp;AS&nbsp;FRIEND'), 'class="btn-custom-green-big add_as_friend"');
                                                                ?>

                                                            <?php } else if (($user_record->user_status != 'Pending')) { ?>
                                                                <div class="btn-group">
                                                                    <a class="btn-custom-green-big dropdown-toggle " data-toggle="dropdown" href="#">
                                                                        <i class="icon-ok"></i> <?php echo ucfirst($this->lang->line('friend')); ?> <i class="icon-caret-down"></i>

                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li> <a href="javascript:void(0)" id="ask_remove_frn" data-toggle="modal" data-target="#remove_friend_model"><?php echo ucwords($this->lang->line('unfriend')); ?></a></li>

                                                                    </ul>
                                                                </div>
                                                                <div class="popUp" id="removeFrenPopup"> <div class="title"><?php echo $this->lang->line('remove_friend'); ?></div>

                                                                    <div class="row-fluid">
                                                                        <div class="span12">
                                                                            <p><?php echo $this->lang->line('remove_friend_confrim_msg'); ?></p>
                                                                        </div>

                                                                    </div>
                                                                    <div class="formHolder">

                                                                        <?php echo form_open(base_url() . 'profile/view/' . $profile_id) ?>
                                                                        <input type="hidden" value="<?php echo $profile_id; ?>" name="remove_friend_id" />
                                                                        <button type="submit" class="btn-custom-gray addFren" id=""><?php
                                                                    echo
                                                                    ucfirst($this->lang->line('Yes'))
                                                                        ?></button>
                                                                        <button type="button" class="_hide" name="Invite" ><?php
                                                                            echo
                                                                            ucfirst($this->lang->line('Cancel'))
                                                                            ?></button>
                                                                        </form>
                                                                    </div>
                                                                </div>
<?php } ?>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="userProfileRight">
                                            <?php
                                            $resFev = $this->product_m->userFevProduct($profile_id);
                                            $febCount = $resFev->num_rows();
                                            ?>
                                            <div class="fev">
                                                <div class="title"><?php if (($user_record->user_status != 'Pending'))
                                                echo $user_record->user_fname . ' ' . $user_record->user_lname . '\'s ' . $this->lang->line('favourite') . 's';
                                            ?></div>
<?php if ($febCount > 0 && ($user_record->user_status != 'Pending')) { ?>
                                                    <div>


                                                        <div id="lista1" class="als-container">

                                                            <div class="popUp">do no delete this div</div>

                                                                    <?php if ($febCount > 6) { ?><span class="als-prev"><img src="assets/frontend/img/thin_left_arrow_333.png" alt="" title="" /></span><?php } ?>
                                                            <div class="als-viewport">
                                                                <ul class="als-wrapper">
                                                                    <?php
                                                                    if( is_array( $resFev->result() ) ) {
                                                                    foreach ($resFev->result() as $recFev) {
                                                                        if( in_array( $recFev->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
                                                                        $merchantName = $this->merchant_m->merchantName($recFev->prod_merchant_id);
                                                                        ?>
                                                                        <li class="als-item displyFevPopUp glompItemFromFev" style="cursor:pointer;" data-image="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" data-point="<?php echo $recFev->prod_point; ?>" data-productname="<?php echo stripslashes($recFev->prod_name); ?>" data-id="<?php echo $recFev->prod_id; ?>"  data-merchantname="<?php echo stripslashes($this->merchant_m->merchantName($recFev->prod_merchant_id)); ?>" >
                                                                            <div class="getPopupDescription">
                                                                                <div class="row-fluid">
                                                                                    <div class="span5">
                                                                                        <img src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>">
                                                                                    </div>
                                                                                    <div class="span7">
                                                                                        <div class="name1"><?php echo $merchantName; ?></div>
                                                                                        <div class="name2"><?php echo stripslashes($recFev->prod_name); ?></div>
                                                                                        <p><?php echo stripslashes($recFev->prod_details); ?></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <img src="<?php echo $this->custom_func->product_logo($recFev->prod_image); ?>" alt="<?php echo stripslashes($recFev->prod_name); ?>"  />                         <p class="productName"><b><?php echo $merchantName . "<br>" . stripslashes($recFev->prod_name); ?></b></p>
                                                                            </p>
                                                                        </li>
                                                                    <?php }
                                                                     }?>

                                                                </ul>
                                                            </div>
                                                    <?php if ($febCount > 6) { ?><span class="als-next"><img src="assets/frontend/img/thin_right_arrow_333.png" alt="" title="" /></span> <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    if (($user_record->user_status != 'Pending')) {
                                                        ?>
                                                        <div class="alert">
                                                            <?php /* ?> <button type="button" class="close" data-dismiss="alert">&times;</button><?php */ ?>
                                                            <strong><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?> <?php echo $this->lang->line('has_not_added_favourite'); ?></strong>
                                                        </div>
                                                        <?php
                                                    } else if (($user_record->user_status == 'Pending')) {
                                                        ?>
                                                        <div class="alert" style="color:#888;font-size:14px;">															
                                                            We are awaiting <b><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?></b> to verify their account. Go ahead and glomp! <b><?php echo $user_record->user_fname ?></b> more!																
                                                        </div>
                                                        <button value="glomp" type="button" style="float:right;background-color:#d2d7e1; border:0px;width: 87px; padding:0px; border:solid 0px; margin-top:48px;margin-right:0px" name="glomp_non_member_from_profile" id="glomp_non_member_from_profile"><img src="assets/frontend/img/glompSubmitButton.png" style="border:0px; width:87px" width="87" /></button>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:-3px;">
                                            <?php if (($user_record->user_status == 'Pending')) {
                                                ?>											

                                                <div class="row-fluid">
                                                    <div class="span12" style="border:solid 0px;background-color:#DEE6EB; border-radius: 5px 5px 0px 5px;height:400px">                                                        
                                                        <?php include('includes/glomp_non_member_from_profile.php'); ?>
                                                    </div>												
                                                </div>											
                                                <?php
                                            } else if (($user_record->user_status != 'Pending')) {
                                                ?>
                                                <div class="profileTab">
                                                    <ul>
                                                        <li><a href="javascript:void(0);" data-href="#tabBuz" class="activeTab" style="width:121px;">
                                                                <span id="glomped_inactive"><img src="assets/frontend/img/glomp_buzz.png" alt="Glomped"> </span>
                                                                <span id="glomped_active"> <img src="assets/frontend/img/buzz_active.png" alt="Glomped"> </span> </a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);"  id="tbBrands"  data-href="#tabBrands"><?php echo ucfirst($this->lang->line('brands','Brands')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);"  id="tbMerchant"  data-href="#tabMerchant"><?php echo ucfirst($this->lang->line('merchants')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" data-href="#tabDrink"><?php echo ucfirst($this->lang->line('drinks')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" data-href="#tabSnaks"><?php echo ucfirst($this->lang->line('snacks')); ?></a></li>
                                                        <?php if(!$this->user_login_check->underage()):?>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" data-href="#tabCocktails"><?php echo ucfirst($this->lang->line('cocktails')); ?></a></li>
                                                        <?php endif;?>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" data-href="#tabSweets"><?php echo ucfirst($this->lang->line('sweets')); ?></a></li>
                                                        <li><a class="perTabClass"  href="javascript:void(0);" data-href="#tabOthers"><?php echo ucfirst($this->lang->line('others')); ?></a></li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="tabContent tabs" id="tabBuz">
                                                    <div class="row-fluid">
                                                        <div class="span8">
                                                            <div class="buzPan tabInnerContent">
                                                                
                                                                <div class="buzzScroll nano" id="tabBuz_wrapper">
                                                                    <div class="content" id="glomp_buzz_content">
                                                                        <?php
                                                                        if ($glom_buzz_count > 0) {
                                                                            $base_url = base_url();
                                                                            $to_a = $this->lang->line('to_a');
                                                                            foreach ($glomp_buzz->result() as $glomp) {

                                                                                //product and merchant info
                                                                                $prod_id = $glomp->voucher_product_id;
                                                                                $prod_info = json_decode($this->product_m->productInfo($prod_id));
                                                                                if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
                                                                                $prod_image= $prod_info->product->$prod_id->prod_image;        
                                                                                $product_logo = $this->custom_func->product_logo($prod_image);
                                                                                $prod_name = $prod_info->product->$prod_id->prod_name;
                                                                                $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
                                                                                $merchant_name = $prod_info->product->$prod_id->merchant_name;
                                                                                $merchant_id = $prod_info->product->$prod_id->merchant_id;
                                                                                
                                                                                $from_id = $glomp->voucher_purchaser_user_id;
                                                                                $to_id = $glomp->voucher_belongs_usser_id;
                                                                                $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                                                                                $ago_date = $glomp->voucher_purchased_date;

                                                                                $form_name = $frn_info->friends->$from_id->user_name;
                                                                                $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

                                                                                $to_name = $frn_info->friends->$to_id->user_name;
                                                                                $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
                                                                                
                                                                                $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                                                                                $to_fb_id = $frn_info->friends->$to_id->user_fb_id;    
                                                                                
                                                                                $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
                                                                                $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;
                                                                                
                                                                                 $story_type="1";
                                                                                if($from_id!=$this->session->userdata('user_id'))
                                                                                {
                                                                                    $story_type="2";
                                                                                }
                                                                                ?>
                                                                                <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">        
                                                                                    <div class="span7">
                                                                                        <div class="row-fluid">
                                                                                            <div class="span10">
                                                                                                <?php if ($from_id == 1) {
                                                                                            ?>
                                                                                            <a href="javascript:void(0);"><?php echo $form_name; ?></a>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a>
                                                                                        <?php } ?>&nbsp;glomp!ed <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><?php echo $to_name; ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?><div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
                                                                                            </div>
                                                                                            <div class="span2" align="right">                    
                                                                                                <div style="margin-top:28px;" >
                                                                                                    <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line(''); ?>share</button>
                                                                                                    <div style="height:0px;">
                                                                                                        <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                                                                                            <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                                                                            <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                                                                            <button title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="<?php echo $profile_id;?>" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div> 
                                                                                        
                                                                                    </div>
                                                                                    <div class="span5">
                                                                                        <?php if ($from_id == 1) { ?>
                                                                                            <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"> <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>
                                                                                        <?php } ?>
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg"  style="margin:0px 5px" />
                                                                                        <a href="<?php echo base_url('profile/view/'.$to_id.'?tab=tbMerchant&merID='.$merchant_id);?>">
                                                                                            <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                                                                                        </a>
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px"  />
                                                                                        <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
                                                                                    </div>
                                                                                </div> 
                                                                                <?php
                                                                                $start_count++;
                                                                            }//foeach close
                                                                        }//numb count close
                                                                        else {
                                                                            ?>

                                                                            <div class="alert alert-info">
                                                                                <p><?php echo $this->lang->line('coming_soon') ?></p>

                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="tabContent tabs" id="tabMerchant">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $num_merchant = $region_wise_merchant->num_rows();
                                                        if ($num_merchant > 0) {
                                                            $i = 1;
                                                            $j = 1;
                                                            foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                                                if ($i == 1) {
                                                                    ?> <ul class="thumbnails"> <?php } ?>
                                                                    <li class="span2 merchantShadow nameOfMerchant">
                                                                        <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>"   href="<?php echo base_url("profile/merchantProduct/" . $profile_id . '/' . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                                                            <div class="thumbnail">
                                                                                <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>" style="width:90px !important;">

                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <?php if ($i == 6 || $j == $num_merchant) { ?></ul>
                                                                        <?php
                                                                }
                                                                $j++;
                                                                if ($i == 6) {
                                                                    $i = 1;
                                                                }
                                                                else
                                                                    $i++;
                                                            } /// end of foreach
                                                        }//enf of num result check
                                                        ?>
                                                    </div>
                                                </div>
                                                
                                                <div class="tabContent tabs" id="tabDrink">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['profile_region_id'] = $user_record->user_city_id;
                                                        $data_v['cat_id'] = 1;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="tabContent tabs" id="tabSnaks">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 2;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>



                                                <div class="tabContent tabs" id="tabCocktails">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 3;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="tabContent tabs" id="tabSweets">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 4;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="tabContent tabs" id="tabOthers">
                                                    <div class="tabInnerContent">
                                                        <?php
                                                        $data_v['cat_id'] = 5;
                                                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="tabContent tabs" id="merchantProductDetails">
                                                    <?php // to load merchant ?>
                                                </div> 

                                                <div id="tabBrands" class="tabs" style="background:#343239">
                                                    <?php include('user_profile_tab_brand_v.php')?>
                                                </div>
                                            
                                            <?php } //if(($user_record->user_status != 'Pending'))  ?>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
<script src="assets/frontend/js/validate.js" type="text/javascript"></script>
<script type="text/javascript">
	$().ready(function() {
		/*/ validate signup form on keyup and submit*/
		$("#frmGlomp1").validate({
			rules: {
				password:"required"
				},
			messages: {
				password:""
			}
	});
	});
	</script>
    
<script type="text/javascript">
var glomped=false;
$(document).ready(function(e) {

	/* start of glomp item popup */
	 
	/*/ align pop ip box to the center of the window*/
	var _y = 100;/*/($(window).height()/2)-(parseInt($(".glompItemPopUp").css("height"))/2);*/
	var _x = ($(window).width()/2)-(parseInt($(".glompItemPopUp").css("width"))/2);	
	$(".glompItemPopUp1").css({"margin-top":_y,"margin-left":_x}); 
	
			
	$( "#frmGlomp1" ).submit(function( event ) {
		$('._send_glomp1').click();
		event.preventDefault();
	});	
	
	/*/open glomp box*/
	$(document).on('click','.glompItemMerchant',function(){
		
		$("#showError1").hide();
		$("#showSuccess1").hide();
		var _this = $(this);
		$('.glompItemPopUp1').fadeIn();
		var _image = _this.data('image');
		var _point = _this.data('point');
		var _productName = _this.data('productname');
		var _merchantName = _this.data('merchantname');
		var _id = _this.data('id');
		$("#pipup_image1").attr('src',_image);
		$("#popUpItem1").text(_productName);
		$("#popUpMerchant1").text(_merchantName);
		$("#popUpPoints1").text(_point);
		$("#product_id1").val(_id);
		glomped=false;
	});
		
		/*/ send glomp to friend/*/
	$('._send_glomp1').on('click',function(){    
		if(!glomped)
		{
            glomped=true;
			var msg = $('#glomp_user_message1').val();
			if(msg == "")
			{
				var NewDialog = $('<div id="" align="center" style="padding-top:25px !important;">\
                <?php echo addslashes($this->lang->line('glomp_confirmation_message'));?>\
								</div>');
                NewDialog.dialog({                    
                    dialogClass:'noTitleStuff dialog_style_blue_grey',
                    title: "",
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 320,
                    height: 140,
                    show: '',
                    hide: '',
                    buttons: [{text: 'Add Message',
                            "class": 'btn-custom-darkblue',
                            click: function() {
                                $(this).dialog("close");
                                glomped=false;
                                $('#glomp_user_message1').focus();
                                
                            }},
                        {text: "No Thanks",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                doSendGlomp1();                                
                            }}
                    ]
                });
                NewDialog.dialog('open');
                /*if(!confirm("<?php echo $this->lang->line('glomp_confirmation_message');?>?"))
				{
					$('#glomp_user_message1').focus();
					return false;
				}*/
			}
            else
            {
                doSendGlomp1();
            }
			
			
		}
	});
			/*/close glomp popup box*/
		$('._close_popup1').click(function(){ $('.glompItemPopUp1').fadeOut();});
		
	/* end of glomp item popup */	

});

    function doSendGlomp1()
    {
        $('#cancel_glomp_option1').addClass('disabled');
        $('#cancel_glomp_option1').removeClass('_close_popup');
        $(".Loader1").show();
        $("#showError1").hide();
        $("#showSuccess1").hide();
        $.ajax({
            type: "POST",
            url:"profile/glompToUser/<?php echo $profile_id;?>",
            data: $('#frmGlomp1').serialize(),
            success: function(data){
                var d = eval("("+data+")");
                if(d.status=='success')
                {
                
                    var response=d;
                    var details=d;
                    
                    $('#glomp_user_message1').val('');
                    $('#password').val('');
                    $(".Loader1").hide();
                    
                    glomped=false;
                    tryAutoShare(details);
                    
                    $("#showSuccess1").show();
                    $("#showSuccessMessage1").text(d.msg);
                    $('.glompItemPopUp1').fadeOut();
                                                    
                    /* success*/
                    var NewDialog = $('<div id="confirmPopup" align="center">\
                                        <div align="center" style="margin:5px 0px;">'+response.msg+'.<br><span style="font-size:14px;">Would you like to share the news?</span></div>\
                                        <div id="success_buttons_wrapper" class="cl" align="center" style="padding:15px 0px;"></div>\
                                        <div style="height:0px;" class="cl">\
                                            <div id="share_wrapper_box_up" class="cl fl share_wrapper_box_up" style="" align="center">\
                                                <button data-merchant="'+details.merchant_name+'"  data-belongs="'+details.to_name_real+'" data-purchaser="'+details.form_name_real+'" data-from_fb_id="'+details.from_fb_id+'" data-to_fb_id="'+details.to_fb_id+'" data-story_type="'+details.story_type+'" data-prod_name="'+details.prod_name+'" data-prod_img="'+details.product_logo+'"  data-voucher_id="'+details.voucher_id+'" title="Share on Facebook" style="background:url(\'<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>\'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb"   ></button>\
                                                <button data-merchant="' + details.merchant_name + '"  data-belongs="' + details.to_name_real + '" data-purchaser="' + details.form_name_real + '" data-from_fb_id="' + details.from_fb_id + '" data-to_fb_id="' + details.to_fb_id + '" data-story_type="' + details.story_type + '" data-prod_name="' + details.prod_name + '" data-prod_img="' + details.product_logo + '"  data-voucher_id="' + details.voucher_id + '" title="Share on LinkedIn" style="background:url(\'<?php echo base_url('assets/frontend/img/li_icon_mini.jpg'); ?>\'); background-size: 20px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet"   ></button>\
                                                <button  data-from="<?php echo $profile_id;?>" data-voucher_id="'+details.voucher_id+'" title="Share on Twitter"  style="background:url(\'<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>\'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet"  ></button>\
                                            </div>\
                                        </div>\
                                        </div>');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                        title:'',
                        modal: true,
                        width:300,
                        position: 'center',
                        height:140									
                    });
                    NewDialog.dialog('open'); 
                    $elem = $('<button type="button" class=" w100px glomp_fb_share btn-custom-blue-grey_normal fl" style="height:30px;"  >Share</button>');
                    $elem.on('click', function(e) {
                            $('#share_wrapper_box_up').toggle();
                    });
                    $("#success_buttons_wrapper").append($elem);
                    
                    
                    $elem = $('<button type="button" class=" w100px btn-custom-white  fr"   style=""  >Close</button>');
                    $elem.on('click', function(e) {
                        $('#confirmPopup').dialog("close");
                        setTimeout(function() {
                            $('#confirmPopup').dialog('destroy').remove();
                        }, 500 );
                    });
                    $("#success_buttons_wrapper").append($elem);
                    /* success*/
                    
                    /*location.href="<?php echo base_url();?>";*/
                }
                else if(d.status=='error')
                    {
                    glomped=false;
                    $('#password').val('');
                    $(".Loader1").hide();
                    $("#showError1").show();
                    $("#showErrorMessage1").text(d.msg);
                    }
            }
        });
    
    };
</script>
</html>