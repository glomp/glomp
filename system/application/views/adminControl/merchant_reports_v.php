<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?></title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <!--<link rel="stylesheet" href="assets/backend/css/common.css">-->
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">    

        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <script src="assets/backend/lib/jquery-ui.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="assets/backend/lib/easyui/themes/icon.css">
        <link href="assets/backend/css/cupertino/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
        <style>
            input[readonly] {
                cursor: pointer;
                background-color: inherit !important;
            }
        </style>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript" src="assets/backend/lib/easyui/jquery.easyui.min.js"></script>
        <script>
            var BASE_URL = '<?php echo site_url(ADMIN_FOLDER); ?>';
        </script>
    </head>
    <body class="">   
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid  dashboard">
                <br />
                <h2><?php echo $page_title; ?></h2>
                <div class="row-fluid">
                    <div id="tt" class="easyui-tabs" style="">
                        <div title="Weekly" style="padding:20px;padding-right: 30px;" data-options="selected:<?php echo $weekly_sel; ?>">
                            <div class="span2 control-group">
                                <label class="control-label" for="date_year"><span class="required">*</span>Year:</label>
                                <div class="controls">
                                    <input type="text" readonly id="year" value="<?php echo $current_year; ?>" />
                                </div>
                            </div>
                            <div class="span2 control-group">
                                <label class="control-label" for="date_week"><span class="required">*</span>Week:</label>
                                <div class="controls">
                                    <select id="week">
                                        <?php for($i=1; $i<=52; $i++) { ?>
                                            <?php $selected = ($current_week == $i) ? 'selected' :  ''; ?>
                                            <option <?php echo $selected; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="controls">
                                    Monday: <span id="monday_of_week"></span>
                                </div>
                            </div>
                            <div class="span2 control-group">
                                <label class="control-label" for="download_excel">-</label>
                                <div id="date_to_text" class="controls">
                                    <button id="download_weekly_btn">
                                        Download Excel
                                    </button>
                                </div>
                            </div>
                            <div class="row-fluid"></div>
                            <div class="row-fluid">
                                <table id="datagrid_weekly" title="Reports" style="height:250px;"></table>
                            </div>
                        </div>
                        
                        <div title="Monthly" data-options="closable:false,selected:<?php echo $monthly_sel; ?>" style="padding:20px;padding-right: 30px;">
                            <div class="span2 control-group">
                                <label class="control-label" for="date_month_year"><span class="required">*</span>Month Year:</label>
                                <div class="controls">
                                    <input type="text" readonly id="date_month_year" value="<?php echo $current_month_year; ?>" />
                                </div>
                            </div>
                            <div class="span2 control-group">
                                <label class="control-label" for="download_excel">-</label>
                                <div id="date_to_text" class="controls">
                                    <button id="download_monthly_btn">Download Excel</button>
                                </div>
                            </div>
                            <div class="row-fluid"></div>
                            <div class="row-fluid">
                                <table id="datagrid_monthly" title="Reports" style="height:250px;"></table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <script>
            var grid = '#datagrid_weekly';
            var grid2 = '#datagrid_monthly';
            var download_weekly_url;
            var download_monthly_url;
            
            //On ready
            var w2date = function(year, wn, dayNb){
                var j10 = new Date( year,0,10,12,0,0),
                    j4 = new Date( year,0,4,12,0,0),
                    mon1 = j4.getTime() - j10.getDay() * 86400000;
                return new Date(mon1 + ((wn - 1)  * 7  + dayNb) * 86400000);
            };
            
            var converDate = function(inputFormat) {
                function pad(s) { return (s < 10) ? '0' + s : s; }
                var d = new Date(inputFormat);
                return [pad(d.getMonth()+1), pad(d.getDate()), d.getFullYear()].join('/');
            };
            
            var monday_date_format = w2date($('#year').val(), $('#week').val(), 0);
            $('#monday_of_week').text(converDate(monday_date_format));
            
            $('#download_weekly_btn').click(function(e){
               console.log(download_weekly_url);
               window.open(download_weekly_url, '_blank'); 
            });
            
            $('#download_monthly_btn').click(function(e){
               console.log(download_monthly_url);
               window.open(download_monthly_url, '_blank'); 
            });
            
            $(grid).datagrid({
                url: BASE_URL + '/merchant/get_merchant_reports',
                pagination:true,
                rowStyler: function(index, row){
                    
                },
                columns:[[
                    {field:'merchant_id',title:'', sortable: true, hidden:true},
                    {field:'location',title:'Location',width: 50, sortable: true},
                    {field:'merchant_name',title:'Merchant', width: 80, sortable: true},
                    {field:'merchant_contact_name',title:'Key Contact Name', width: 20, sortable: true},
                    {field:'account_contact_name',title:'Accounts Contact Person', width: 30, sortable: true},
                    {field:'amount',title:'Amount',align:'right', width: 10, sortable: true}
                ]],
                fitColumns: true,
                queryParams: {
                    week: $('#week').val(),
                    year: $('#year').val()
                },
                onLoadSuccess: function(r){
                    if(r.total == 0) {
                        return $('#download_weekly_btn').attr('disabled', 'disabled');
                    }
                    download_weekly_url = BASE_URL + '/merchant/download_report?week=' + $('#week').val() + '&year=' + $('#year').val();
                    return $('#download_weekly_btn').removeAttr('disabled');
                }
            });
            
            $(grid2).datagrid({
                url: BASE_URL + '/merchant/get_merchant_reports',
                pagination:false,
                rowStyler: function(index, row){
                    console.log(row.notify_excempt_amount + ' ' + row.merchant_name);
                    if (row.notify_excempt_amount == 0 || row.notify_excempt_amount == null) {
                        //run default
                        if (row.amount <= 100){
                            return 'background-color:red;color:yellow;';
                        }
                    } else if (row.amount <= row.notify_excempt_amount) {
                        return 'background-color:red;color:yellow;';
                    }
                },
                columns:[[
                    {field:'merchant_id',title:'', sortable: true, hidden:true},
                    {field:'location',title:'Location',width: 50, sortable: true},
                    {field:'merchant_name',title:'Merchant', width: 80, sortable: true},
                    {field:'merchant_contact_name',title:'Key Contact Name', width: 20, sortable: true},
                    {field:'accounts_contact_name',title:'Accounts Contact Person', width: 30, sortable: true},
                    {field:'amount',title:'Amount',align:'right', width: 10, sortable: true}
                ]],
                fitColumns: true,
                queryParams: {
                    month_year: $('#date_month_year').val(),
                    report_filter: 'monthly'
                },
                onLoadSuccess: function(r){
                    if(r.total == 0) {
                        return $('#download_monthly_btn').attr('disabled', 'disabled');
                    }
                    download_monthly_url = BASE_URL + '/merchant/download_report?month_year=' + $('#date_month_year').val() + '&report_filter=monthly';
                    return $('#download_monthly_btn').removeAttr('disabled');
                }
            });
            
            $("#date_from").datepicker({
                beforeShowDay: function(date) {
                    var day = date.getDay();
                    if(day === 1) return [true,''];
                    return [false,''];
                },
                onSelect: function(dateText, inst) {
                    selected_date = new Date(dateText);
                    var numberOfDaysToAdd = 6;
                    selected_date.setDate(selected_date.getDate() + numberOfDaysToAdd); 
                    var to_date = (selected_date.getMonth() + 1) + '/' + selected_date.getDate() + '/' +  selected_date.getFullYear();
                    $('#date_to').val(to_date);
                    $('#date_to_text').text(to_date);
                    
                    $(grid).datagrid('load', {
                        week: $('#week').val(),
                        year: $('#year').val()
                    });
                }
            });    
            
            $('#year').datepicker( {
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy',
                onClose: function(dateText, inst) {
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, 1, 1));
                    var strDate ="01/01"+"/"+year;
                    $(this).blur();//remove focus input box
                    
                    var monday_date_format = w2date(year, $('#week').val(), 0);
                    $('#monday_of_week').text(converDate(monday_date_format));
                    
                    $(grid).datagrid('load', {
                        week: $('#week').val(),
                        year: $('#year').val()
                    });
                }
            });
            
            
            $('#date_month_year').datepicker( {
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) { 
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1));
                    
                    $(grid2).datagrid('load', {
                        month_year: $('#date_month_year').val(),
                        report_filter: 'monthly'
                    });
                }
            });
            
            //this code is to hide the calendar part of year calendar
            $("#year").focus(function () {
              $(".ui-datepicker-calendar").hide();
              $(".ui-datepicker-month").hide();
              $("#ui-datepicker-div").position({
                  my: "center top",
                  at: "center bottom",
                  of: $(this)
              });
            });
            
            $("#date_month_year").focus(function () {
              $(".ui-datepicker-calendar").hide();
              $("#ui-datepicker-div").position({
                  my: "center top",
                  at: "center bottom",
                  of: $(this)
              });
            });
            
            //On change
            $('#week').change(function(){
                var monday_date_format = w2date($('#year').val(), $(this).val(), 0);
                $('#monday_of_week').text(converDate(monday_date_format));
                
                $(grid).datagrid('load', {
                    week: $('#week').val(),
                    year: $('#year').val()
                });
            });
           
        </script>
        <?php include("includes/footer.php"); ?>
    </body>
</html>