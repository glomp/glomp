<?php echo $main_header; ?>
<?php echo $header; ?>
<div class="content">
    <?php echo $main_menu; ?>
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="page-heading">Manage Newsletter Templates</div>
                        </div>
                        <div class="span4" style="text-align:right;"></div>
                    </div>
                </div>
                <div class="page-subtitle">
                </div>
                <table class="table table-hover" width="100%">
                    <tr>
                        <td><strong>SN.</strong></td>
                        <td><strong>Date Modified</strong></td>
                        <td><strong>Template Name</strong></td>
                        <td><strong>Language</strong></td>
                        <td style="text-align:center;"><strong>Options</strong></td>
                    </tr>
                    <?php foreach ($newsletters as $rows) { ?>
                    <tr>
                        <td><?php echo $rows['newsletter_id']; ?></td>
                        <td><?php echo $rows['date_modified']; ?></td>
                        <td><?php echo $rows['name']; ?></td>
                        <td><?php echo $rows['lang']; ?></td>
                        <td style="text-align:center;">
                            <?php echo anchor(ADMIN_FOLDER."/newsletter/edit/".$rows['newsletter_id']."/", "<i class=\"icon-edit\"></i>","title='Edit' "); ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
</body>
</html>


