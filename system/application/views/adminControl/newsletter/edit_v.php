<?php echo $main_header; ?>
<?php echo $header; ?>
<div class="content">
    <?php echo $main_menu; ?>
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="page-heading">Edit "<?php echo $newsletter['name']; ?>" Template</div>
                        </div>
                        <div class="span4" style="text-align:right;">
                        </div>
                    </div>
                </div>

                <div class="page-subtitle">
                    Please click onto the pencil icon to edit content
                </div>
                <div class="row-fluid">
                    <div class="content-box">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="control-group" id ="template_holder">
                                    <iframe width="100%" height="500px" src="<?php echo site_url(ADMIN_FOLDER. '/newsletter/preview/'.$newsletter['newsletter_id']) ?>"></iframe>
                                </div>
                            </div>
                        </div>
                        <?php echo anchor(ADMIN_FOLDER."/newsletter", "Cancel", "class='btn btn-large ui-state-default ui-corner-all'" )?>
                        <?php echo anchor("#", "Preview", "id ='preview_full_btn' class='btn btn-large ui-state-default ui-corner-all'" )?>
                        <?php echo anchor(ADMIN_FOLDER."/newsletter/test_send/". $newsletter['newsletter_id'], "Test Send", "class='btn btn-large ui-state-default ui-corner-all'" )?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#preview_full_btn').click(function(e){
        e.preventDefault();
        popupWindow = window.open('<?php echo site_url(ADMIN_FOLDER. '/newsletter/preview_full/'.$newsletter['newsletter_id']) ?>','name');
        popupWindow.focus();
    });
</script>
<?php echo $footer; ?>
</body>
</html>        