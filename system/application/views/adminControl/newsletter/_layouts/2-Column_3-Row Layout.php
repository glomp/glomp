<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>

        <style>/* ------------------------------------- 
                            GLOBAL 
            ------------------------------------- */
            * { 
                margin:0;
                padding:0;
            }
            * { font-family: verdana, Arial, sans-serif; }

            img { 
                max-width: 100%; 
            }
            .collapse {
                margin:0;
                padding:0;
            }
            body {
                -webkit-font-smoothing:antialiased; 
                -webkit-text-size-adjust:none; 
                width: 100%!importa	nt; 
                height: 100%;
            }


            /* ------------------------------------- 
                            ELEMENTS 
            ------------------------------------- */
            a { color: #2BA6CB;}

            .btn {
                text-decoration:none;
                color: #FFF;
                background-color: #666;
                padding:10px 16px;
                font-weight:bold;
                margin-right:10px;
                text-align:center;
                cursor:pointer;
                display: inline-block;
            }

            p.callout {
                padding:15px;
                background-color:#ECF8FF;
                margin-bottom: 15px;
            }
            .callout a {
                font-weight:bold;
                color: #2BA6CB;
            }

            table.social {
                /* 	padding:15px; */
                background-color: #ebebeb;

            }
            .social .soc-btn {
                padding: 3px 7px;
                font-size:12px;
                margin-bottom:10px;
                text-decoration:none;
                color: #FFF;font-weight:bold;
                display:block;
                text-align:center;
            }
            a.fb { background-color: #3B5998!important; }
            a.tw { background-color: #1daced!important; }
            a.gp { background-color: #DB4A39!important; }
            a.ms { background-color: #000!important; }

            .sidebar .soc-btn { 
                display:block;
                width:100%;
            }

            /* ------------------------------------- 
                            HEADER 
            ------------------------------------- */
            table.head-wrap { width: 100%;}

            .header.container table td.logo { padding: 15px; }
            .header.container table td.label { padding: 15px; padding-left:0px;}


            /* ------------------------------------- 
                            BODY 
            ------------------------------------- */
            table.body-wrap { width: 100%;}


            /* ------------------------------------- 
                            FOOTER 
            ------------------------------------- */
            table.footer-wrap { width: 100%;	clear:both!important;
            }
            .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
            .footer-wrap .container td.content p {
                font-size:10px;
                font-weight: bold;

            }


            /* ------------------------------------- 
                            TYPOGRAPHY 
            ------------------------------------- */
            h1,h2,h3,h4,h5,h6 {
                font-family: verdana, Arial, sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
            }
            h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

            h1 { font-weight:200; font-size: 44px;}
            h2 { font-weight:200; font-size: 37px;}
            h3 { font-weight:500; font-size: 27px;}
            h4 { font-weight:500; font-size: 23px;}
            h5 { font-weight:900; font-size: 17px;}
            h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

            .collapse { margin:0!important;}

            p.lead { font-size:17px; }
            p.last { margin-bottom:0px;}

            ul li {
                margin-left:5px;
                list-style-position: inside;
            }

            /* ------------------------------------- 
                            SIDEBAR 
            ------------------------------------- */
            ul.sidebar {
                background:#ebebeb;
                display:block;
                list-style-type: none;
            }
            ul.sidebar li { display: block; margin:0;}
            ul.sidebar li a {
                text-decoration:none;
                color: #666;
                padding:10px 16px;
                /* 	font-weight:bold; */
                margin-right:10px;
                /* 	text-align:center; */
                cursor:pointer;
                border-bottom: 1px solid #777777;
                border-top: 1px solid #FFFFFF;
                display:block;
                margin:0;
            }
            ul.sidebar li a.last { border-bottom-width:0px;}
            ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



            /* --------------------------------------------------- 
                            RESPONSIVENESS
                            Nuke it from orbit. It's the only way to be sure. 
            ------------------------------------------------------ */

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display:block!important;
                max-width:600px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                max-width:600px;
                margin:0 auto;
                display:block; 
            }

            /* Let's make sure tables in the content area are 100% wide */
            .content table { width: 100%; }


            /* Odds and ends */
            .column {
                width: 300px;
                float:left;
            }
            .column tr td {  }
            .column-wrap { 
                padding:0!important; 
                margin:0 auto; 
                max-width:600px!important;
            }
            .column table { width:100%;}
            .social .column {
                width: 296px;
                min-width: 279px;
                float:left;
            }

            .box_content {
                position: relative;
                width: 276px;
                min-height: 100px;
                top: -120px;
                background-color: rgba(0, 0, 0, 0.5);
                padding: 10px;
                color:white;
            }

            .column img {
                width: 246px;
                height: 246px;
            }

            /* Be sure to place a .clear element after each set of columns, just to be safe */
            .clear { display: block; clear: both; }


            /* ------------------------------------------- 
                            PHONE
                            For clients that support media queries.
                            Nothing fancy. 
            -------------------------------------------- */
            @media only screen and (max-width: 600px) {

                a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

                div[class="column"] { width: auto!important; float:none!important;}

                table.social div[class="column"] {
                    width:auto!important;
                }

            }
            table {
                border: 0px black solid;
            }
        </style>
        <?php
        $image1 = (!isset($attributes['img1'])) ? base_url('assets/images/green.jpg') : $attributes['img1'];
        $image2 = (!isset($attributes['img2'])) ? base_url('assets/images/green.jpg') : $attributes['img2'];
        $image3 = (!isset($attributes['img3'])) ? base_url('assets/images/green.jpg') : $attributes['img3'];
        $image4 = (!isset($attributes['img4'])) ? base_url('assets/images/green.jpg') : $attributes['img4'];
        $image5 = (!isset($attributes['img5'])) ? base_url('assets/images/green.jpg') : $attributes['img5'];
        $image6 = (!isset($attributes['img6'])) ? base_url('assets/images/green.jpg') : $attributes['img6'];

        $style = "
            width: 25%;
            height: 200px;
            vertical-align:middle;
            background-repeat: no-repeat;
            background-position: center ;
        ";
        $image1 = "background-image: url('" . $image1 . "')";
        $image2 = "background-image: url('" . $image2 . "')";
        $image3 = "background-image: url('" . $image3 . "')";
        $image4 = "background-image: url('" . $image4 . "')";
        $image5 = "background-image: url('" . $image5 . "')";
        $image6 = "background-image: url('" . $image6 . "')";

        $style_content = "
                display: block !important;
                max-width:600px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            ";
        $style_content_2 = "
                display: block !important;
                max-width:588px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            ";    
        $style_container = "
                display: block !important;
                max-width:600px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            ";
        $style_container_2 = "
                display: block !important;
                max-width:588px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            ";
            
        ?>

    </head>

    <body bgcolor="#FFFFFF">
        <div style="max-width: 600px !important; margin:0 auto;">
            <table  width="100%" bgcolor="#FFFFFF" cellpadding="0"  cellspacing ="">
                <tr>
                    <td>
                    <!-- HEADER -->        
                        <table class="head-wrap" width="100%" bgcolor="#FFFFFF" cellpadding="0"  cellspacing ="0">
                            <tr>
                                <td class="header container" bgcolor ="#F0F0F5" style="<?php echo $style_container; ?>">
                                    <div class="content" style="<?php echo $style_content_2; ?>;padding-top:6px !important;">
                                        <table bgcolor="#636466" cellpadding="0" cellspacing ="0" width="100%">
                                            <tr>
                                                <td align="center"><img src="<?php echo base_url('assets/frontend/img/email_template_header.png'); ?>" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                        </table><!-- /HEADER -->
                        <a href="<?php echo site_url(); ?>" target ="_blank" style="text-decoration: none;" >
                            <table class="head-wrap" bgcolor="#F0F0F5"  style="<?php echo $style_container; ?>" cellpadding="0" cellspacing ="6">
                                 <tr>
                                    <td class="header" align="left" style="background-color:#0153A5; " colspan="2">
                                        <div style="<?php echo $style_content_2; ?>
                                             position:relative;
                                             padding: 10px;
                                             color:#959595;
                                             bottom: 0px;" class="editable" attr_name="content_header_1">
                                             <?php echo (isset($attributes['content_header_1'])) ? $attributes['content_header_1'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header" align="left" style="background-color:#F9931C; " colspan="2">
                                        <div style="<?php echo $style_content_2; ?>
                                             position:relative;
                                             padding: 10px;
                                             color:#959595;
                                             bottom: 0px;" class="editable" attr_name="content_header_2">
                                             <?php echo (isset($attributes['content_header_2'])) ? $attributes['content_header_2'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="<?php echo $style . $image1; ?>;width: 160px;">
                                        <div style="height: 4px;position: relative" class="uploadable" attr_name="img1"></div>
                                        <div style="position: relative;
                                             height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content1">
                                             <?php echo (isset($attributes['content1'])) ? $attributes['content1'] : '&nbsp;'; ?>
                                        </div>                    
                                    </td>
                                    <td align="left" style="<?php echo $style . $image2; ?>">
                                        <div style="position: relative;height: 4px;" class="uploadable" attr_name="img2"></div>
                                        <div style="position: relative;
                                             height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content2">
                                             <?php echo (isset($attributes['content2'])) ? $attributes['content2'] : '&nbsp;'; ?>
                                        </div>                    
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="<?php echo $style . $image3; ?>">
                                        <div style="position: relative;height: 4px;" class="uploadable" attr_name="img3"></div>                    
                                        <div style="position: relative;
                                             height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content3">
                                             <?php echo (isset($attributes['content3'])) ? $attributes['content3'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                    <td align="left" style="<?php echo $style . $image4; ?>">
                                        <div style="position: relative;height: 4px;" class="uploadable" attr_name="img4"></div>
                                        <div style="position: relative;
                                             height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content4">
                                             <?php echo (isset($attributes['content4'])) ? $attributes['content4'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="left" style="<?php echo $style . $image5; ?>">
                                        <div style="position: relative;height: 4px;" class="uploadable" attr_name="img5"></div>
                                        <div style="position: relative;
                                                height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content5">
                                             <?php echo (isset($attributes['content5'])) ? $attributes['content5'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                    <td align="left" style="<?php echo $style . $image6; ?>">
                                        <div style="position: relative;height: 4px;" class="uploadable" attr_name="img6"></div>
                                        <div style="position: relative;
                                             height: 39px;                         
                                             overflow-y: hidden;
                                             background-color: rgba(0, 0, 0, 0.5);
                                             padding: 15px 25px;
                                             top: 64px;
                                             text-decoration:none !important;
                                             color:white !important;" class="editable" attr_name="content6">
                                             <?php echo (isset($attributes['content6'])) ? $attributes['content6'] : '&nbsp;'; ?>
                                        </div>
                                        
                                    </td>
                                </tr>           
                                 <tr>
                                    <td class="header" align="left" style="background-color:#EF1C25; " colspan="2">
                                        <div style="<?php echo $style_content_2; ?>
                                             position:relative;
                                             padding: 10px;
                                             color:#959595;
                                             bottom: 0px;" class="editable" attr_name="content_header_3">
                                             <?php echo (isset($attributes['content_header_3'])) ? $attributes['content_header_3'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="header" align="left" style="background-color:#8EC63F; " colspan="2">
                                        <div style="<?php echo $style_content_2; ?>
                                             position:relative;
                                             padding: 10px;
                                             color:#959595;
                                             bottom: 0px;" class="editable" attr_name="content_header_4">
                                             <?php echo (isset($attributes['content_header_4'])) ? $attributes['content_header_4'] : '&nbsp;'; ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </a>
                        <!-- FOOTER --> 
                        <table class="head-wrap" width="100%" bgcolor="#FFFFFF" cellpadding="0"  cellspacing ="0">
                            <tr>
                                <td class="header container" bgcolor ="#F0F0F5" style="<?php echo $style_container; ?>">
                                    <div class="content" style="<?php echo $style_content_2; ?>;background-color:#fbfcf2 !important;">
                                        <div style="font-size: 8px !important;padding-left: 25px;padding-bottom: 10px;padding-top: 16px;">Having trouble viewing this email? 
                                            <span><a style="font-size: 8px !important;" target="_blank" href="<?php echo site_url('newsletter/view/'.$campaign_newsletter_id); ?>">View it on your browser</a></span>
                                        </div>
                                        <div style="min-height:20px;color:#545456;padding:16px;padding-bottom: 10px;padding-top: 0px;padding-left:25px">
                                            <span style="font-size: 8px !important">
                                                Subscription settings <a href="<?php echo site_url('landing/notificationRedirect'); ?>" style="font-size: 8px !important;">Click here</a>
                                            </span>
                                        </div>
                                        <div style="min-height: 20px;color:#545456;padding: 16px;padding-bottom: 5px;padding-top: 0px;padding-left: 25px;">
                                            <span style="font-size: 8px !important">Copyright &copy; <?php echo date('Y'); ?> glomp! All rights reserved.</span>
                                        </div>
                                    </div>
                                    <div style="padding: 3px !important;"></div>
                                </td>
                            </tr>
                        </table>                    
                        <!-- /FOOTER -->
                    </td>                            
                </tr>
            </table>
        </div>
    </body>
</html>