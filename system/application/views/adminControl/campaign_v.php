<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e){
                /*Starts to two because 1 is pre-created*/
                var master_tr_ctr = 2;
                var invite_tr_ctr = 2;
                $('#add_more_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='master_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'master_tr_' + master_tr_ctr);
                    /*Add counter for unique ID*/
                    master_tr_ctr++;
                    
                    $("tr[id^='master_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });
                
                $('#add_more_user_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='invites_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'invites_tr_' + invite_tr_ctr);
                    /*Add counter for unique ID*/
                    invite_tr_ctr++;
                    
                    $("tr[id^='invites_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });
                
                $("#camp_voucher_table").on('click', '.remove_tr', function() {
                    $(this).parent().parent().remove();
                });
                /*for editing*/
                $('#add_more_tr_edit').click(function(e) {
                    var select_html = $('#product_id_0').html();
                    var tr_html = '<tr id="master_tr"><td><select name="new_product_id[]"' + select_html + '</select></td><td><input type="text" value="" name="new_voucher_qty[]"></td><td>&nbsp;</td><td><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';

                    $('#camp_voucher_table tr:last').after(tr_html);
                });
                $("#camp_voucher_table").on('click', '.delete_tr', function() {
                    if (!confirm('Are you sure you want to delete this record?'))
                    {
                        return false;
                    }
                    $(this).parent().parent().remove();
                    var id = $(this).attr('id');
                     
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/remove_cam_details'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + id,
                        success: function(html) {

                        }
                    });
                });
            });
        </script>
        <style type="text/css">
            .hidden_td{
                visibility:hidden;
            }
        </style>
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign/AddCampaign", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>

                        <div class="page-subtitle">
                            <?php
                            //  echo $page_subtitle;
                            if (isset($msg)) {
                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                            }
                            if (isset($error_msg)) {
                                echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                            }
							else{
								$error_msg = $this->session->flashdata('error_msg');
								if (! empty($error_msg)) {
									echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
								}
							}
                            if (validation_errors() != "") {
                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                            }
                            ?>
                        </div>
                            <?php if ($page_action == "view") { ?>
                            <table width="100%" class="table table-condensed table-hover">
                                <tr>
                                    <td><strong>SN.</strong></td>
                                    <td><strong>Created Date</strong></td>
                                    <td><strong>Campaign Name</strong></td>
                                    <td style="text-align:center"><strong>Campaign Type</strong></td>
                                    <td style="text-align:center"><strong>Total Vouchers/Available</strong></td>
                                    <td style="text-align:center;"><strong>Executed Date</strong></td>
                                    <td style="text-align:center;"><strong>Options</strong></td>
                                </tr>
                                <?php
                                if (isset($res_campaign)) {
                                    $i = 1;
                                    foreach ($res_campaign->result() as $rec_campaign) {

                                        $res_voucher = $this->campaign_m->campaign_voucher_by_camp_id($rec_campaign->campaign_id);
                                        $num_rows = $res_voucher->num_rows();
                                        $total_voucher = $num_rows;

                                        $res_available = $this->campaign_m->campaign_voucher_by_camp_id($rec_campaign->campaign_id, "AND camp_voucher_assign_status = 'UnAssigned'");
                                        $available_voucher = $res_available->num_rows();
                                ?>
                                        <tr>
                                            <td><?php echo $i;$i++; ?></td>
                                            <td><?php echo $this->custom_func->time_stamp($rec_campaign->campaign_created_date); ?></td>
                                            <td><?php echo $rec_campaign->campaign_name; ?></td>
                                            <td style="text-align:center">
                                                <?php echo $rec_campaign->campaign_type; ?>
                                            </td>
                                            <td style="text-align:center;"><?php echo anchor(ADMIN_FOLDER . "/campaign/voucher_details/" . $rec_campaign->campaign_id . "/", $total_voucher . '/' . $available_voucher); ?></td>
                                            <td style="text-align:center;"><?php
                                                if ($rec_campaign->campaign_last_executed_date != "") {
                                                    echo anchor(ADMIN_FOLDER . "/campaign/voucher_details/" . $rec_campaign->campaign_id . "/", $this->custom_func->time_stamp($rec_campaign->campaign_last_executed_date));
                                                }
                                                else
                                                    echo "Never";
                                                ?>
                                            </td>
                                            <td style="text-align:center;">
                                                <?php
                                                    echo anchor(ADMIN_FOLDER . "/campaign/details/" . $rec_campaign->campaign_id . "/", "<i class=\"icon-eye-open\"></i>", "title='View' ");
                                                    if ($rec_campaign->campaign_last_executed_date == "") {
                                                        echo "&nbsp;&nbsp;&nbsp;";
                                                        echo anchor(ADMIN_FOLDER . "/campaign/editCampaign/" . $rec_campaign->campaign_id . "/", "<i class=\"icon-edit\"></i>", "title='Edit' ");
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                            <?php
                                            }
                                                echo '<tr><td colspan="7">' . $links . '</td></tr>';
                                            }
                                            else
                                                echo'<tr><td colspan="7">No records found.</td></tr>';
                                        ?>
                            </table>
                            <?php } elseif ($page_action == "add") { ?>
                            <?php echo form_open(ADMIN_FOLDER . "/campaign/AddCampaign", 'name="frmCms" id="cmspages"') ?>
                            <div class="content-box">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td><span>* Campaign </span>  Name</td>
                                                <td><span>*</span>  Campaign Type</td>
                                                <td><span>*</span>  Voucher Expiry Day</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <?php 
                                                    $voucher_types = array(array(
                                                        'key'=> '',
                                                        'text'=> 'Please Select',
                                                    ),
                                                    array(
                                                        'key'=> 'Consumable',
                                                        'text'=> 'Consumable',
                                                    ),
                                                    array(
                                                        'key'=> 'Assignable',
                                                        'text'=> 'Assignable',
                                                    ),
                                                    array(
                                                        'key'=> 'Both',
                                                        'text'=> 'Both',
                                                    ));
                                                ?>
                                                
                                                <td><input type="text" name="campaign_name" value="<?php echo set_value('campaign_name'); ?>"></td>
                                                <td> <select name="campaign_type">
                                                        <?php foreach ($voucher_types as $vt) { 
                                                            $sel = (set_value('campaign_type') == $vt['key']) ? 'selected' : '';
                                                        ?>
                                                            <option <?php echo $sel; ?> value="<?php echo $vt['key']; ?>"><?php echo $vt['text']; ?></option>
                                                        <?php } ?>
                                                    </select></td>
                                                <td> <input type="text" value="<?php echo set_value('expiry_day'); ?>" name="expiry_day"></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <!--
                                            <tr>
                                                <td><a href="javascript:void(0)" id="add_more_tr" class="btn btn-info">Add more product <i class="icon-plus"></i></a></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>* Product</td>
                                                <td>* Voucher Quantity</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr id="master_tr_1">
                                                <td><select name="product_id[]" id="product_id_">
                                                        <option value="">Please Choose Product</option>
                                                        <?php
                                                            echo $this->product_m->product_by_merchant_dropdown(set_value('product_id[]'));
                                                        ?>
                                                    </select>
                                                </td>
                                                <td><input type="text" value="<?php echo set_value('voucher_qty[]'); ?>" name="voucher_qty[]"></td>
                                                <td>&nbsp;</td>
                                                <td  class="hidden_td"><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td>
                                            </tr>
                                            -->
                                            <tr>
                                                <td><a href="javascript:void(0)" id="add_products" class="btn btn-info">Add product <i class="icon-plus"></i></a></td>
                                                <td colspan="3">
                                                    <!-- This is hidden from view-->
                                                    <ul id="product-list" class="row" style="display:none;">
                                                        <?php if(isset($sponsor)):?>
                                                        <li class="row sponsor" style="float:none;text-align:left;padding:0 5px;margin:0; background:#faf5ce;">
                                                            <div><strong>Sponsor Products</strong></div>
                                                            <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                <?php foreach($sponsor as $p):?>
                                                                <li class="row prod_info"
                                                                    data-region_id="<?php echo $p->merchant_region_id?>"
                                                                    data-merchant_id="<?php echo $p->merchant_id?>"
                                                                    data-keywords="<?php echo strtolower($p->merchant_name.' '.$p->prod_name);?>"
                                                                    data-prod_id="<?php echo $p->prod_id?>"
                                                                    data-prod_name="<?php echo $p->prod_name?>"
                                                                    data-current_qty="<?php echo ($p->current_qty)?$p->current_qty:'0'?>"
                                                                    style="float:none; text-align:left;padding:0;margin:0">
                                                                    <div style="float:left"><?php echo $p->prod_name?> (<?php echo $p->merchant_name?>)</div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo ($p->current_qty)?$p->current_qty:'0'?> left</div>
                                                                </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </li>
                                                        <?php endif;?>
                                                        <?php foreach($merchants as $mid => $m):?>
                                                        <li class="row" data-merchant_id="<?php echo $mid?>" data-region_id="<?php echo $m['region']?>" style="float:none;text-align:left;padding:0 5px;margin:0">
                                                            <div><strong><?php echo $m['name'];?></strong></div>
                                                            <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                <?php foreach( $m['products'] as $p ):?>
                                                                <li class="row prod_info" data-keywords="<?php echo strtolower($m['name'].' '.$p->prod_name);?>" data-prod_id="<?php echo $p->prod_id?>" data-prod_name="<?php echo $p->prod_name?>" style="float:none; text-align:left;padding:0;margin:0">
                                                                    <div style="float:left"><?php echo $p->prod_name?></div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        <?php endforeach;?>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Filter</td>
                                                <td>* Product</td>
                                                <td>* Voucher Qty</td>
                                                <td></td>
                                            </tr>
                                            <tr class="add-product">
                                                <td>
                                                    <select class="filter_by_region" name="region_id">
                                                        <option value="">Filter By Region</option>
                                                        <?php foreach($regions as $r):?>
                                                        <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </td>
                                                <td class="add_products">
                                                    <input type="hidden" class="product_id" name="product_id[]" />
                                                    <input type="text" class="trigger_product_list" placeholder="Select a Product" />
                                                    <ul class="place-holder"></ul>
                                                </td>
                                                <td>
                                                    <input type="text" name="voucher_qty[]" placeholder="Voucher Quantity" />
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="remove_row">Remove <i class="icon-remove"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><a href="javascript:void(0)" id="add_more_user_tr" class="btn btn-info">Add More Invites by Email <i class="icon-plus"></i></a></td>
                                                <td><i>*Note: Upon creation of campaign, email addresses that are not exists in the database will be added.</i></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Sender Email Address</td>
                                                <td style="width: 284px">Sender Name</td>
                                                <td>Recipient Email Address</td>
                                                <td>Recipient First Name</td>
                                                <td>Recipient Last Name</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr id="invites_tr_1">
                                                <td><input type="text" name="invite_sender_email[]" /></td>
                                                <td><input type="text" name="invite_sender_name[]" /></td>
                                                <td><input type="text" name="invite_email[]" /></td>
                                                <td><input type="text" name="invite_fname[]" /></td>
                                                <td><input type="text" name="invite_lname[]" /></td>
                                                <td class="hidden_td"><a href="javascript:void(0)"  class="remove_tr">Remove<i class="icon-remove"></i></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <input class="btn btn-large btn-WI" type="submit" name="create_campaign" value="Create Campaign" >
                                <button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href = '<?php echo site_url() . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>
                            </div>
                            <?php echo form_close(); ?>
                            <?php } elseif ($page_action == "edit") { ?>
                            <?php echo form_open(ADMIN_FOLDER . "/campaign/editCampaign/" . $rec_campaign->campaign_id, 'name="frmCms" id="cmspages"') ?>
                            <div class="content-box">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td><span>* Campaign </span>  Name</td>
                                                <td><span>*</span>  Campaign Type</td>
                                                <td><span>*</span>  Voucher Expiry Day</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="campaign_name" value="<?php echo $rec_campaign->campaign_name ?>"></td>
                                                <td>
                                                    <?php
                                                        $type = $rec_campaign->campaign_type;
                                                    ?>
                                                    <select name="campaign_type">
                                                        <option value="">Please Select</option>
                                                        <option value="Consumable" <?php echo $selected = ($type == 'Consumable') ? 'selected="selected"' : ''; ?>>Consumable</option>
                                                        <option value="Assignable" <?php echo $selected = ($type == 'Assignable') ? 'selected="selected"' : ''; ?>>Assignable</option>
                                                        <option value="Both" <?php echo $selected = ($type == 'Both') ? 'selected="selected"' : ''; ?>>Both</option>

                                                    </select></td>
                                                <td> <input type="text" value="<?php echo $rec_campaign->campaign_voucher_expiry_day; ?>" name="expiry_day"></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><a href="javascript:void(0)" id="add_products" class="btn btn-info">Add product <i class="icon-plus"></i></a></td>
                                                <td colspan="3">
                                                    <!-- This is hidden from view-->
                                                    <ul id="product-list" class="row" style="display:none;">
                                                        <?php if(isset($sponsor)):?>
                                                        <li class="row sponsor" style="float:none;text-align:left;padding:0 5px;margin:0; background:#faf5ce;">
                                                            <div><strong>Sponsor Products</strong></div>
                                                            <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                <?php foreach($sponsor as $p):?>
                                                                <li class="row prod_info" data-region_id="<?php echo $p->merchant_region_id?>" data-merchant_id="<?php echo $p->merchant_id?>" data-keywords="<?php echo strtolower($p->merchant_name.' '.$p->prod_name);?>" data-prod_id="<?php echo $p->prod_id?>" data-prod_name="<?php echo $p->prod_name?>" style="float:none; text-align:left;padding:0;margin:0">
                                                                    <div style="float:left"><?php echo $p->prod_name?> (<?php echo $p->merchant_name?>)</div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo ($p->current_qty)?$p->current_qty:'0'?> left</div>
                                                                </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </li>
                                                        <?php endif;?>
                                                        <?php foreach($merchants as $mid => $m):?>
                                                        <li class="row" data-merchant_id="<?php echo $mid?>" data-region_id="<?php echo $m['region']?>" style="float:none;text-align:left;padding:0 5px;margin:0">
                                                            <div><strong><?php echo $m['name'];?></strong></div>
                                                            <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                <?php foreach( $m['products'] as $p ):?>
                                                                <li class="row prod_info" data-keywords="<?php echo strtolower($m['name'].' '.$p->prod_name);?>" data-prod_id="<?php echo $p->prod_id?>" data-prod_name="<?php echo $p->prod_name?>" style="float:none; text-align:left;padding:0;margin:0">
                                                                    <div style="float:left"><?php echo $p->prod_name?></div>
                                                                    <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        <?php endforeach;?>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Filter</td>
                                                <td>* Product</td>
                                                <td>* Voucher Qty</td>
                                                <td></td>
                                            </tr>
                                            <?php
                                                $res_details = $this->campaign_m->campaign_details_by_id($camp_id);
                                                $num_rows = $res_details->num_rows();
                                            ?>
                                            <?php if($num_rows > 0):?>
                                            <?php foreach($res_details->result() as $row_details):?>
                                            <?php
                                                $prod = $this->db->get_where('gl_product',array('prod_id'=>$row_details->camp_prod_id))->row();
                                            ?>
                                            <tr class="add-product edit-record">
                                                <td>
                                                    <input type="hidden" name="cam_details_id[]" class="cam_details_id" value="<?php echo $row_details->camp_details_id; ?>" />
                                                    <select class="filter_by_region" name="region_id">
                                                        <option value="">Filter By Region</option>
                                                        <?php foreach($regions as $r):?>
                                                        <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </td>
                                                <td class="add_products">
                                                    <input type="hidden" class="product_id" name="product_id[]" value="<?php echo $row_details->camp_prod_id?>" />
                                                    <input type="text" class="trigger_product_list" placeholder="Select a Product" value="<?php echo $prod->prod_name?>" />
                                                    <ul class="place-holder"></ul>
                                                </td>
                                                <td>
                                                    <input type="text" class="voucher_qty" name="voucher_qty[]" placeholder="Voucher Quantity" value="<?php echo $row_details->camp_voucher_qty ?>" />
                                                </td>
                                                <td>
                                                    <a href="/adminControl/campaign/remove_cam_details/<?php echo $row_details->camp_details_id; ?>" class="remove_row" data-id="<?php echo $row_details->camp_details_id; ?>">Remove <i class="icon-remove"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                            <?php else:?>
                                            <tr class="add-product">
                                                <td>
                                                    <select class="filter_by_region" name="region_id">
                                                        <option value="">Filter By Region</option>
                                                        <?php foreach($regions as $r):?>
                                                        <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </td>
                                                <td class="add_products">
                                                    <input type="hidden" class="product_id" name="new_product_id[]" />
                                                    <input type="text" class="trigger_product_list" placeholder="Select a Product" />
                                                    <ul class="place-holder"></ul>
                                                </td>
                                                <td>
                                                    <input type="text" name="new_voucher_qty[]" placeholder="Voucher Quantity" />
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="remove_row">Remove <i class="icon-remove"></i></a>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                        </table>

                                        <input class="btn btn-large btn-WI" type="submit" name="update_campaign" value="Save" >
                                        <button class="btn btn-large" type="button" name="editCat" onclick="Javascript:location.href = '<?php echo site_url() . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>
                                    </div>
                                    <?php echo form_close(); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php include("includes/footer.php"); ?>

    <style type="text/css">
        .place-holder {
            display:none;position:absolute;height:200px;min-width:400px;overflow:auto;background:#fff;font-size:0.9em;padding:0;margin:0;
        }
        .prod_info {
            cursor:pointer;
        }
        .prod_info:hover {
            background:#a5d7eb;
        }
    </style>
    <script src="custom/lib/bootstrap/js/bootstrap.js"></script>
    <script src="/assets/backend/js/campaign.js"></script>
    </body>
</html>


