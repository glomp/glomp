<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <script src="assets/backend/lib/jquery-ui.js" type="text/javascript"></script>     
     <script src="assets/backend/lib/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
      <link rel="stylesheet" type="text/css" href="assets/backend/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
   <script language="javascript" type="text/javascript">
   $(document).ready(function(e) {     
        $('.datepicker').datepicker();
        
        $('.filter').click(function(e)
        {
            val=$(this).val();             
           $('.filter_select').prop("disabled",true);
           $('.filter_select option[value=0]').prop('selected',true);           
            $('#'+val+'_id').prop("disabled",false);
        });
        $('#hide_deleted_chk').click(function(e)
        {
            var stat = $('#hide_deleted_chk').prop('checked');
            if(stat==true)
            {
                $('.hide_deleted_class').hide();
            }
            else
            {
                $('.hide_deleted_class').show();                
            }
        });
        var stat = $('#hide_deleted_chk').prop('checked');
        if(stat==true)
        {
            $('.hide_deleted_class').hide();
        }
        else 
        {
            $('.hide_deleted_class').show();
        }
    });
    var location_id = <?php echo $location_id;?>;
    var merchant_id = <?php echo $merchant_id;?>;
    var category_id = <?php echo $category_id;?>;    
    $(function() {        
        var sortedIDs;
        $( "#gridSave" ).click(function()
        {            
            var data="sortedIDs="+sortedIDs+"&location_id="+location_id+"&merchant_id="+merchant_id+"&category_id="+category_id;
                $.ajax({
                    type: "POST",
                    dataType: 'html',
                    url: 'adminControl/product/updateRanks',
                    data: data,
                    success: function(response){ 
                         alert("Succesfully save.");
                    }
                });
            
        });
        $( "#sortable" ).sortable({            
            update: function( event, ui ) {
                console.log(ui.item);
                sortedIDs = $( "#sortable" ).sortable( "toArray",{attribute:'data-id'});                
                
            }
        });
        $( "#sortable" ).disableSelection();
    });
   </script>
   <style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width:1275px; margin:0 auto;border:solid 0px;}
    #sortable li {background-color:#FFFDFC; margin: 3px 3px 3px 0; padding: 1px; float: left; width: 150px; height: 170px; font-size: 12px; text-align: center; border:solid 2px #ddd;vertical-align:center;display:table-cell }
    </style>
  </head>
  <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
    <?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <?php /*/include("includes/right-sidebar.php");*/?> 
            <div class="span12">
     
     
     <div class="page-header">
                  <div class="row-fluid">
                  <div class="span2">
                  <div class="page-heading">Manage&nbsp;Products</div>
                  </div>
                  <div class="span10" style="text-align:right;">
                    <form name="form1" method="get" action="<?php echo base_url(ADMIN_FOLDER."/product/".$view_as);?>">
                        <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">
                        <?php echo anchor(ADMIN_FOLDER."/product/addProduct", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                          <?php echo anchor(ADMIN_FOLDER."/product/", "List View", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                          <?php echo anchor(ADMIN_FOLDER."/product/grid", "Grid View", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                        </div>
                        <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">
                            <span>Location&nbsp;</span>
                            <select name="location_id" id="location_id" onChange="form.submit();" style="width:150px;margin-top:3px">                                            
                             <?php
                             $location_id =(isset($_GET['location_id']))?intval($_GET['location_id']):0;
                             ?>
                             <option value="0" disabled
                             <?php
                                if($location_id==0)
                                    echo 'selected'
                             ?>
                             >Select Location</option>
                             <?php echo $this->regions_m->getAllCountryDropdown($location_id,'locations')?>
                            </select>
                        </div>
                        <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">
                            Filter By &nbsp;
                            <input type="radio" class="filter" <?php echo ($merchant_id>0)?'checked':''; ?> name="filter" value="merchant" style="margin-top:-3px;">&nbsp;&nbsp;Merchant&nbsp;
                            <select name="merchant_id" class="filter_select" id="merchant_id" <?php echo ($merchant_id>0)?'':'disabled'; ?>  onChange="form.submit();" style="width:120px;">
                            <option value="0">--All--</option>
                             <?php
                             $merchant_id =(isset($_GET['merchant_id']))?intval($_GET['merchant_id']):0;
                             $region_wise_merchant = $this->merchant_m->regionwise_merchant_deep($location_id);
                            foreach ($region_wise_merchant->result() as $row) {                            
                                $selected = ($merchant_id==$row->merchant_id)?'selected="selected"':"";                                
                                 ?> <option value='<?php echo $row->merchant_id;?>."' <?php echo $selected;?> ><?php echo $row->merchant_name;?></option>                              
                            <?php
                            } /*/ end of foreach*/                         
                             ?>
                             
                            </select><br>
                            <input type="radio" class="filter" <?php echo ($merchant_id==0)?'checked':''; ?> name="filter" value="category" style="margin-top:-3px;">&nbsp;&nbsp;Category&nbsp;
                            <select name="category_id" <?php echo ($merchant_id==0)?'':'disabled'; ?>   class="filter_select"id="category_id" onChange="form.submit();" style="width:120px;">
                            <option value="0">--All--</option>
                             <?php
                             $category_id =(isset($_GET['category_id']))?intval($_GET['category_id']):0;
                             echo $this->categories_m->catOption($category_id)?>
                             
                            </select>
                        </div>
                        <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">                          
                          <input type="text" value="<?php echo $search_q;?>" placeholder="Search..." name="search_q" id="search_q" style="width:120px; margin-right:5px; margin-bottom:0px;" class="fr ui-corner-all" />
                          <input class="btn btn-WI" style="margin-right:0px; width:60px; height:30px;" type="submit" name="" value="Search" >                  
                          
                        </div>
                            <?php 
                                echo form_close();
                              ?>
                  
                  </div>                 
                  </div>
                  </div>
                  <?php if($page_action=="grid") { ?>
                  <div class="page-subtitle">
                    <div style="float:left">
                        <?php
                        echo "Grid View";
                        ?>
                    </div>
                    <div style="float:right;font-size:14px;">
                        <input type="checkbox" id="hide_deleted_chk" checked />&nbsp;Hide all deleted items
                    </div>
                    <div style="clear:both">
                    </div>
                  <?php
                  if(isset($breadcum))
                  {
                  echo $breadcum;
                  }
                  if(isset($msg))
                  {
                      echo "<div class=\"success_msg\">".$msg."</div>";
                  }
                  if(isset($error_msg))
                  {
                      echo "<div class=\"custom-error\">".$error_msg."</div>";
                  }
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"custom-error\">".validation_errors()."</div>";
                  }
                  ?>
                  </div>
                 <?php if($location_id>0)
                        {
                            if($res_product)
                            {  
                                $i=1;?>
                                <ul id="sortable" style="">
                                <?php                    
                                foreach($res_product as $rec_product){                  
                                
                                    $tr_class = ($rec_product->prod_status=="Deleted")?" error hide_deleted_class ":"";
                                    if($tr_class=="")
                                        $tr_class = ($rec_product->prod_status=="Pending for Deletion")?" error ":"";
                                    
                                ?>                            
                                    <li class="ui-state-default <?php echo $tr_class;?>" id="product_<?php echo $i;?>" data-id="<?php echo $rec_product->prod_id;?>" >                        
                                        <div align="left" style="float:left;padding:0px 5px;" >
                                            <?php echo $rec_product->prod_status;?>
                                        </div>
                                        <div align="right" style="float:right;padding:0px 5px;" >
                                            <?php
                                                $i++;
                                                echo anchor(ADMIN_FOLDER."/product/editRecord/".$rec_product->prod_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
                                                if ($rec_product->prod_status != "Deleted" && $rec_product->prod_status != "Pending for Deletion") {
                                                    echo "&nbsp;&nbsp;&nbsp;";
                                                    echo anchor(ADMIN_FOLDER . "/product/deleteRecord/" . $rec_product->prod_id . "/", "<i class=\"icon-trash\"></i>", "title='Delete'");
                                                }
                                            ?>                                
                                        </div>
                                        <div style="clear:both"></div>
                                        <div align="center" style="padding:2px;" >          
                                            <img class="thumbnail" src="<?php echo $this->custom_func->product_logo($rec_product->prod_image) ?>" alt="<?php echo $rec_product->prod_name; ?>" style="width:100px !important;">
                                        </div>
                                        <div align="center" style="padding:0px 5px;" >
                                            <b><?php echo $rec_product->prod_name;?></b>
                                        </div>                                        
                                    </li>                                                     
                                <?php $i++; } ?>
                                </ul>  
                                <div style="clear:both;">
                                    <hr>
                                    <div class="control-group">
                                        <div class="controls">
                                            <button type="button" name="addPage" id="gridSave" class="btn btn-WI">Save</button>                                
                                        </div>
                                    </div>
                                </div>
                              <?php      
                            }
                            else {echo'<div>No records found.</div>';}
                        }
                        else
                        {
                            echo '<div class="" align="center" style="font-size:18px;">You must select a location first.</div>';
                        }
                      } elseif($page_action=="view") { ?>
                  
                  <div class="page-subtitle">
                    <div style="float:left">
                        <?php
                        echo "View All ";
                        ?>
                    </div>
                    <div style="float:right;font-size:14px;">
                        <input type="checkbox" id="hide_deleted_chk" checked />&nbsp;Hide all deleted items
                    </div>
                    <div style="clear:both">
                    </div>
                  <?php
                  if(isset($breadcum))
                  {
                  echo $breadcum;
                  }
                  if(isset($msg))
                  {
                      echo "<div class=\"success_msg\">".$msg."</div>";
                  }
                  if(isset($error_msg))
                  {
                      echo "<div class=\"custom-error\">".$error_msg."</div>";
                  }
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"custom-error\">".validation_errors()."</div>";
                  }
                  ?>
                  </div>
                    <?php if($location_id>0)
                    {?>
                          <table width="100%" class="table table-hover">
                          <tr>
                          <td><strong>SN.</strong></td>
                           <td><strong>Added Date</strong></td>
                          <td><strong>Product Name</strong></td>
                          <td><strong>Point</strong></td>
                          <td><strong>Category</strong></td>
                          <td><strong>Merchant</strong></td>
                          <td style="text-align:center;"><strong>Order Position</strong></td>
                          <td style="text-align:center;"><strong>Status</strong></td>
                          <td style="text-align:center;"><strong>Options</strong></td>
                          </tr>
                          <?php if($res_product) {  
                          $i=1;
                          foreach($res_product as $rec_product){                  
                            $tr_class = ($rec_product->prod_status=="Deleted")?"class='error hide_deleted_class '":"";
                            if($tr_class=="")
                                $tr_class = ($rec_product->prod_status=="Pending for Deletion")?"class='error'":"";
                            
                          ?>                                                        
                          <tr <?php echo $tr_class;?> >
                          <td><?php echo $i; $i++; ?></td>
                          <td><?php echo $rec_product->prod_added;?></td>
                          <td><?php echo $rec_product->prod_name;?></td>
                           <td><?php echo $rec_product->prod_point;?></td>
                          <td><?php echo $rec_product->cat_name;?></td>
                           <td><?php echo $rec_product->merchant_name;?></td>                           
                           <td style="text-align:center;"><?php echo $rec_product->prod_rank;?></td>                           
                           <td style="text-align:center;"><?php echo $rec_product->prod_status;?></td>
                          <td style="text-align:center"><?php
                                
                                    echo anchor(ADMIN_FOLDER."/product/editRecord/".$rec_product->prod_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
                                    if ($rec_product->prod_status != "Deleted" && $rec_product->prod_status != "Pending for Deletion") {
                                        echo "&nbsp;&nbsp;&nbsp;";
                                        echo anchor(ADMIN_FOLDER . "/product/deleteRecord/" . $rec_product->prod_id . "/?location_id=".$location_id.'&category_id='.$category_id.'&search_q='.$search_q.'&merchant_id='.$merchant_id, "<i class=\"icon-trash\"></i>", "title='Delete'");
                                    }
                                    if($rec_product->prod_cat_id == 9) {
                                        echo "&nbsp;&nbsp;&nbsp;";
                                        echo anchor(ADMIN_FOLDER . "/product/addStock/" . $rec_product->prod_id . "/", "<i class=\"icon-plus\"></i>", "title='Set Stock'");
                                    }
                            ?></td>
                          </tr>
                          <?php }
                          echo '<tr><td colspan="8">'.$this->pagination->create_links().'</td></tr>';
                          } else echo'<tr><td colspan="8">No records found.</td></tr>'; ?>
                          </table>
                    <?php
                    }
                    else
                    {
                        echo '<div class="" align="center" style="font-size:18px;">You must select a location first.</div>';
                    }
                } elseif($page_action=="delete") { ?>
                  <div class="page-subtitle"><?php
                  echo "Delete Product \"".$rec_product->prod_name."\"";
                  
                  if(isset($msg))
                  {
                      echo "<div class=\"success_msg\">".$msg."</div>";
                  }
                  if(isset($error_msg))
                  {
                      echo "<div class=\"custom-error\">".$error_msg."</div>";
                  }
                  
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"custom-error\">".validation_errors()."</div>";
                  }
                  ?>
                  </div>                    
                    <div class="content-box" >
                        <table cellpadding="5" cellspacing="5" border="0" width="100%">
                            <tr>
                                <th align="left" width="" colspan="2">Conflicts Checks List:</th>
                            </tr>
                            <tr>
                                <td align="left" width="200">&nbsp;&nbsp;Vouchers</td>
                                <td width="">
                                    <?php
                                    $has_conflict=false;
                                    if($conflict_voucher==0)
                                    {?>
                                        <div class="success_msg">
                                        No conflict.
                                        </div>
                                    <?php
                                    }
                                    else
                                    {
                                        $has_conflict=true;
                                    ?>
                                        <div class="custom-error error">
                                                <?php echo $conflict_voucher; ?> voucher(s) are in conflict.
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="">&nbsp;&nbsp;Campaign by Rule</td>
                                <td width="">
                                    <?php
                                    if($conflict_campaign_by_rule==0)
                                    {?>
                                        <div class="success_msg">
                                        No conflict.
                                        </div>
                                    <?php
                                    }
                                    else
                                    {
                                        $has_conflict=true;
                                    ?>
                                        <div class="custom-error error">
                                                <?php echo $conflict_campaign_by_rule; ?> campaign vouchers(s) are in conflict.
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="">&nbsp;&nbsp;Campaign by Promo Code</td>
                                <td width="" >
                                    <?php
                                    if($conflict_campaign_by_promo_code==0)
                                    {?>
                                        <div class="success_msg">
                                        No conflict.
                                        </div>
                                    <?php
                                    }
                                    else
                                    {
                                        $has_conflict=true;
                                    ?>
                                        <div class="custom-error error">
                                                <?php echo $conflict_campaign_by_promo_code; ?> campaign(s) are in conflict.
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th align="left" width="" colspan="2"><hr size="1" style="padding:0px;margin:0px;" /></th>
                            </tr>
                            <?php if(!$has_conflict)
                            { ?>
                            <tr>
                                <td align="left" width="" colspan="2">
                                    Are you sure you want to delete this record?
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <?php echo form_open_multipart(ADMIN_FOLDER."/product/deleteRecord/$contentID/delete_now",'name="frmCms" class="form-horizontal" id="region"')?>
                                        <button type="submit" name="" class="btn btn-WI">Delete this product</button>&nbsp;
                                        <?php echo anchor(ADMIN_FOLDER."/product/", "Cancel", "class='btn-WI btn ui-state-default ui-corner-all'" )?>                                    
                                    <?php 
                                    echo form_close();
                                    ?>
                                </td>
                            </tr>
                            <?php }
                            else
                            { ?>
                            <tr>
                                <td align="left" width="" colspan="2">
                                    <?php echo form_open_multipart(ADMIN_FOLDER."/product/deleteRecord/$contentID?location_id=".$location_id.'&category_id='.$category_id.'&search_q='.$search_q.'&merchant_id='.$merchant_id
                                    ,'name="frmCms" class="form-horizontal" id="region"')?>
                                        <div class="error" style="padding:4px 4px;font-size:14px;">
                                        There will be conflict(s) if this product is deleted. This product cannot be deleted at the moment.
                                        </div>
                                        <div style="padding:5px 20px;">                                        
                                            <input type="radio" value="delete_if_no_conflicts" name="delete_type" checked style="margin-top:-2px;" />&nbsp;Automatically delete if there are no more conflicts.<br>
                                            <input type="radio" value="delete_after_num_days"name="delete_type" style="margin-top:-2px;" />&nbsp;Automatically delete after <input name="num_days" maxlength="2" type="text" style="width:20px" value="45" /> day(s).
                                        </div>
                                        <div >
                                            <button type="submit" class="btn btn-WI">Set this product as "Pending for Deletion"</button>&nbsp;
                                            <?php echo anchor(ADMIN_FOLDER."/product/?location_id=".$location_id.'&category_id='.$category_id.'&search_q='.$search_q.'&merchant_id='.$merchant_id,
                                            "Cancel", "class='btn-WI btn ui-state-default ui-corner-all'" )?>                                    
                                        </div>
                                    <?php 
                                    echo form_close();
                                    ?>
                                </td>
                            </tr>                            
                            <?php }                            
                            ?> 
                        </table>    
                    </div>                    
                  
                  <?php } elseif($page_action=="add") { ?>
                  
                  <div class="page-subtitle"><?php
                  echo "Add New Product";
                  if(isset($msg))
                  {
                      echo "<div class=\"success_msg\">".$msg."</div>";
                  }
                  if(isset($error_msg))
                  {
                      echo "<div class=\"custom-error\">".$error_msg."</div>";
                  }
                  
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"custom-error\">".validation_errors()."</div>";
                  }
                  ?>
                  </div>
                  <?php echo form_open_multipart(ADMIN_FOLDER."/product/addProduct",'name="frmCms" class="form-horizontal" id="region"')?>
                  <div class="content-box" >
                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                        <tr>
                            <td width="30%"><span class="required">*</span>Product Name</td>
                            <td width="70%"><input type="text" name="prod_name" id="prod_name" required   /></td>
                        </tr>
                        <tr>
                            <td><span class="required">*</span>Product Cateogry</td>
                            <td><select name="prod_cat_id">
                            <?php echo $this->categories_m->catOption(set_value('prod_cat_id'))?>
                            </select></td>
                        </tr>
                        
                        <tr>
                            <td><span class="required">*</span>Merchant</td>
                            <td><select name="prod_merchant_id">
                             <?php echo $this->merchant_m->merchantDropDown(set_value('prod_merchant_id'))?>
                            </select></td>
                        </tr>
                        <tr>                         
                            <td><span class="required">*</span>Menu price (points)</td>
                            <td><input type="text" name="prod_point" id="prod_point" required  /></td>
                        </tr>
                        <tr>
                            <td><span class="required">*</span>Merchant Price ($)</td>
                            <td><input type="text" name="prod_merchant_cost" id="prod_merchant_cost" required  /></td>
                        </tr>
                        <tr>
                            <td><span class="required">*</span>Voucher validity (days)</td>
                            <td><input type="text"  name="prod_voucher_expiry_day" value="<?php echo $this->custom_func->config_value('VOUCHER_EXPIRTY');?>"  required  /></td>
                        </tr>
                        <tr>
                            <td><span class="required">*</span>Refund after expiry (points)</td>
                            <td><input type="text" name="prod_reverse_point" id="prod_reverse_point" required  /></td>
                        </tr>
                        
                        <tr>
                            <td>Merchant Order Rank</td>
                            <td><input type="text" name="prod_rank" id="prod_rank" value="0"  /></td>
                        </tr>    
                        <tr>
                            <td>Category Order Rank</td>
                            <td><input type="text" name="prod_rank_cat" id="prod_rank_cat" value="0" /></td>
                        </tr>    
                         <tr>
                            <td>Product Description</td>
                            <td><textarea class="span8" name="prod_details"></textarea></td>
                        </tr>
                         <tr>
                            <td>Product Warning Message</td>
                            <td><textarea class="span8" name="prod_warning"></textarea></td>
                      
                        </tr>
                          <tr>
                            <td valign="top">Product Terms & Conditions</td>
                            <td><!--ckeditor starts-->
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
                  <script type="text/javascript">
                   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
                  </script>
                  <textarea name="prod_terms_condition" style="width:200px!important; height:200px;" class="ckeditor"></textarea>
                  <!--ckeditor ends-->
                            </td>
                      
                        </tr>
                        <tr>
                          <td>Product Status</td>
                          <td>Active
                              <input type="radio" name="prod_status" value="Active" checked />
InActive
<input type="radio" name="prod_status" value="InActive"  />
                          </td>
                        </tr>
                           <tr>
                            <td>Product Image</td>
                            <td><input type="file" name="prod_image" /></td>
                        </tr>  
                         </tr>
                           <tr>
                            <td>&nbsp;</td>
                            <td><button type="submit" name="addPage" class="btn btn-WI">Add Product</button>
      <input type="reset" name="Cancel" value="Reset" class="btn btn-WI"></td>
                        </tr>                 
                    </table>    
              </div>
                  <?php 
                  echo form_close();
                  ?>
                  <?php } elseif($page_action=="edit") { ?>
                  
                  <div class="page-subtitle">
                  <?php
                  echo "Edit Product";
                  if(isset($msg))
                  {
                      echo "<div class=\"success_msg\">".$msg."</div>";
                  }
                  if(isset($error_msg))
                  {
                      echo "<div class=\"custom-error\">".$error_msg."</div>";
                  }
                  if(validation_errors()!="")
                  {
                      echo "<div class=\"custom-error\">".validation_errors()."</div>";
                  }
                  ?>
                  </div>
                   <?php echo form_open_multipart(ADMIN_FOLDER."/product/editRecord/$contentID",'name="frmCms" class="form-horizontal" id="region"')?>
                  <table cellpadding="5" cellspacing="5" border="0" width="100%">
                    <tr>
                      <td width="31%"><span class="required">*</span>Product Name</td>
                      <td width="69%"><input type="text" value="<?php echo $rec_product->prod_name;?>" name="prod_name" id="prod_name2" required   /></td>
                    </tr>
                    <tr>
                      <td><span class="required">*</span>Product Cateogry</td>
                      <td><select name="prod_cat_id">
                        <?php echo $this->categories_m->catOption($rec_product->prod_cat_id)?>
                      </select></td>
                    </tr>
                    <tr>
                      <td><span class="required">*</span>Merchant</td>
                      <td><select name="prod_merchant_id">
                        <?php echo $this->merchant_m->merchantDropDown($rec_product->prod_merchant_id)?>
                      </select></td>
                    </tr>
                    <tr>
                      <td><span class="required">*</span>Menu price (points)</td>
                      <td><input type="text" value="<?php echo $rec_product->prod_point;?>" name="prod_point" id="prod_point" required  /></td>
                    </tr>
                    <tr>
                        <td><span class="required">*</span>Merchant Price ($)</td>
                        <td><input type="text" name="prod_merchant_cost" id="prod_merchant_cost" value="<?php echo $rec_product->prod_merchant_cost;?>"  required  /></td>
                    </tr>
                    <tr>
                        <td><span class="required">*</span>Voucher validity (days)</td>
                        <td><input type="text"  name="prod_voucher_expiry_day" value="<?php echo $rec_product->prod_voucher_expiry_day;?>"  required  /></td>
                    </tr>
                    <tr>
                      <td>Refund after expiry (points)</td>
                      <td><input type="text" value="<?php echo $rec_product->prod_reverse_point;?>" name="prod_reverse_point" id="prod_reverse_point" /></td>
                    </tr>
                                     
                    <tr>
                        <tr>
                            <td>Merchant Order Rank</td>
                            <td><input type="text" name="prod_rank" id="prod_rank" value="<?php echo $rec_product->prod_rank;?>"  /></td>
                        </tr>
                        <tr>
                            <td>Category Order Rank</td>
                            <td><input type="text" name="prod_rank_cat" id="prod_rank_cat" value="<?php echo $rec_product->prod_rank_cat;?>"  /></td>
                        </tr> 
                        
                    
                      <td>Product Description</td>
                      <td><textarea class="span8" name="prod_details"><?php echo $rec_product->prod_details;?></textarea></td>
                    </tr>
                    <tr>
                      <td>Product Warning Message</td>
                      <td><textarea class="span8" name="prod_warning"><?php echo $rec_product->prod_warning;?></textarea></td>
                    </tr>
                     <tr>
                            <td valign="top">Product Terms & Conditions</td>
                            <td>
                            
                             <!--ckeditor starts-->
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
                  <script type="text/javascript">
                   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
                  </script>
                  <textarea name="prod_terms_condition" style="width:200px!important; height:200px;" class="ckeditor">
                  <?php echo $rec_product->prod_terms_condition;?></textarea>
                  <!--ckeditor ends-->
                  
                       </td>
                      
                        </tr>
                     <tr>
                          <td>Product Status</td>
                          <td><input type="radio" name="prod_status" value="Active" <?php echo ($rec_product->prod_status=='Active')?"Checked='checked'":"";?> />&nbsp;Active&nbsp;
                               <input type="radio" name="prod_status" value="InActive" <?php echo ($rec_product->prod_status=='InActive')?"Checked='checked'":"";?> />&nbsp;InActive&nbsp;
                               <?php
                                    if($rec_product->prod_status=='Deleted' || $rec_product->prod_status=='Pending for Deletion')
                                    {
                                         
                               ?>
                                <input type="radio" name="prod_status" value="<?php echo $rec_product->prod_status;?>" disabled checked />&nbsp;
                               <?php 
                                echo $rec_product->prod_status;
                               } ?>
                               
                          </td>
                        </tr>
                    <tr>
                      <td>Product Image</td>
                      <td><input type="file" name="prod_image" />
                      <input type="hidden" name="old_image" value="<?php echo $rec_product->prod_image;?>" />
                      </td>
                    </tr>
  
                    <tr>
                      <td>&nbsp;</td>
                      <td><?php 
                      if($rec_product->prod_image!="" && file_exists('custom/uploads/products/thumb/'.$rec_product->prod_image))
                      {?>
                      <img src="custom/uploads/products/thumb/<?php echo $rec_product->prod_image;?>" />
                      <?php
                          
                          }
                      ?></td>
                    </tr>
                    <tr>
    <td>&nbsp;</td>
    <td><button type="submit" name="editPage" class="btn btn-WI">Update Product</button>
      <input type="reset" name="Cancel2" value="Cancel" class="btn btn-WI"></td>
  </tr>
                  </table>
                  <?php }                   
                  echo form_close();
                   ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="assets/backend/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>

<script>
 $('#prod_point').keyup(function() {
    var points = $('#prod_point').val() * 0.9;
    $('#prod_reverse_point').val(Math.floor(points));
 })
</script>
