<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
	<!--<link rel="stylesheet" href="assets/backend/css/common.css">-->
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">    
    
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    
    
    
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <style>
        .custom-error {
            color: red;
        }
        .success_msg {
            color: green;
        } 
    </style>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
  <script>  
	$(document).ready(function(e) {
		checkIfLocalityIsApplicable( ($('#country_id').val()),false );
		checkIfLocalityIsApplicable( ($('#country_id_outlet').val()),true );
		$('#country_id').change(function() {  
			checkIfLocalityIsApplicable( ($('#country_id').val()),false );
		});
		$('#country_id_outlet').change(function() {  
			checkIfLocalityIsApplicable( ($('#country_id_outlet').val()),true );			
		});
		$('#add_reset').click(function(e) {  
			$('.locality').hide();			
		});
        
       
       $('#outlet_region_id').change(function() {  
			generate_outlet_code();
		});
        
        <?php if($page_action=="edit") { ?>                  
            /*generate an outlet code*/
            function generate_outlet_code()
            {
                var outlet_region_id= $("#outlet_region_id").val();
                var base_url = '<?php echo base_url(ADMIN_FOLDER . '/merchant/generate_outlet_code'); ?>';
                $.ajax({
                    type: 'GET',
                    url: base_url + "/" + outlet_region_id,
                    success: function(html) {                    
                        if(html=='no_prefix')
                        {                        
                            $("#outlet_code_text").html('<div class="custom-error" style="color:#ff0000">No prefix available for this region yet. Please add one first.</div>');
                        }
                        else if(html=='prefix_reached_max')
                        {
                            $("#outlet_code_text").html('<div class="custom-error" style="color:#ff0000">No prefix available for this region yet. Please add one first.</div>');
                        }
                        else
                        {
                            $("#outlet_code").val(html);
                            $("#outlet_code_text").html(html);
                        }
                    }
                });
            }
            <?php if(!isset($outlet_edit)) { ?>                              
                generate_outlet_code();

            <?php }?>                  
            /*generate an outlet code*/        
        <?php }?> 
		
	});	
	function checkIfLocalityIsApplicable(country_id,outlet){
		$.ajax({
			type: "POST",
			url: 'ajax_post/checkIfLocalityIsApplicable/',
			data:'country_id='+country_id				
		}).done(function(response){
			if(response!=""){
				response=jQuery.parseJSON(response);				
				if((response.data)==0){
					if(outlet)
                    {
						$('.locality-outlet').hide();
                    }
					else
                    {
						$('.locality').hide();
                    }
				}
				else{
					if(outlet)
                    {
						$('.locality-outlet').show();
                    }
					else
                    {
						$('.locality').show();
                    }
				}
				$('#merchant_locality_required').val(response.data);
			}
		});	
	}
    var location_id = <?php echo $location_id;?>;
    $(function() {
        var sortedIDs;
        $( "#gridSave" ).click(function()
        {            
            var data="sortedIDs="+sortedIDs+"&location_id="+location_id;
            $.ajax({
                type: "POST",
                dataType: 'html',
                url: 'adminControl/merchant/updateRanks',
                data: data,
                success: function(response){ 
                    alert("Succesfully save.");
                }
            });
            
        });
        $( "#sortable" ).sortable({            
            update: function( event, ui ) {
                console.log(ui.item);
                sortedIDs = $( "#sortable" ).sortable( "toArray",{attribute:'data-id'});                
                
            }
        });
        $( "#sortable" ).disableSelection();
    });
  </script>
    <style>
    #sortable { list-style-type: none; margin: 0; padding: 0; width:1260px; margin:0 auto;border:solid 0px;}
    #sortable li {background-color:#FFFDFC; margin: 3px 3px 3px 0; padding: 1px; float: left; width: 170px; height: 160px; font-size: 12px; text-align: center; border:solid 2px #ddd;vertical-align:center;display:table-cell }
    </style>
  </head>
  <body class="">   
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
            
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
        
        
           
			<?php /*/include("includes/right-sidebar.php");*/?> 
            <div class="span12">
            
            <div class="page-header">
                  <div class="row-fluid">
                  <div class="span2">
                  <div class="page-heading">Manage Merchants</div>
                  </div>
                  
                  <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">                  
                        <?php echo anchor(ADMIN_FOLDER."/merchant/addMerchant", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                        <?php echo anchor(ADMIN_FOLDER."/merchant/", "List View", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                        <?php echo anchor(ADMIN_FOLDER."/merchant/grid", "Grid View", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  </div>
                  
                  
                  <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">
                  <form name="form1" method="get" action="<?php echo base_url(ADMIN_FOLDER."/merchant/".$view_as);?>">                   
                    <span>Select Location &nbsp;</span>
                    <select name="location_id" id="location_id" onChange="form.submit();" style="width:300px;margin-top:3px">                                            
                     <?php
					 $location_id =(isset($_GET['location_id']))?intval($_GET['location_id']):0;
                     ?>
                     <option value="0" disabled
                     <?php
                        if($location_id==0)
                            echo 'selected'
                     ?>
                     >Select Location</option>
                     <?php
					 echo $this->regions_m->getAllCountryDropdown($location_id,'locations')?>
                    </select>                  
                  </div>
                  
                  <div class="" style="border:0px solid;float:right;text-align:right;">
                      <?php 
                      // echo form_open(ADMIN_FOLDER.'/merchant/', "method='get'");?>                    
                      <input type="text" value="<?php echo $search_q;?>" placeholder="Search..." name="search_q" id="search_q" style="width:200px; margin-right:5px; margin-bottom:0px;" class="fr ui-corner-all" />
                      <input class="btn btn-WI" style="margin-right:0px; width:70px; height:30px;" type="submit" name="" value="Search" >                  
                      <?php 
                        echo form_close();
                      ?>
                    </div>
                  
                  </div>
                  </div>
              
                  
                  <?php if($page_action=="grid")
                  { ?>
                  
                      <div class="page-subtitle"><?php
                      echo 'Grid View';
                      if(isset($breadcum))
                      {
                          echo $breadcum;
                      }
                      if(isset($msg))
                      {
                          echo "<div class=\"success_msg\">".$msg."</div>";
                      }
                      if(isset($error_msg))
                      {
                          echo "<div class=\"custom-error\">".$error_msg."</div>";
                      }
                      if(validation_errors()!="")
                      {
                          echo "<div class=\"custom-error\">".validation_errors()."</div>";
                      }
                      ?>
                      </div>
                      <?php if($location_id>0)
                            {
                                if($res_merchant)
                                { 
                                    $i=1;?>                      
                                    <ul id="sortable" style="">
                                        <?php 
                                        foreach($res_merchant as $rec_merchant)
                                        { ?>                            
                                            <li class="ui-state-default" id="merchant_<?php echo $i;?>" data-id="<?php echo $rec_merchant->merchant_id;?>" >
                                                <div align="right" style="padding:0px 5px;" >
                                                    <?php
                                                        $i++;
                                                        echo anchor(ADMIN_FOLDER."/merchant/editRecord/".$rec_merchant->merchant_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
                                                        if($this->session->userdata('admin_email')=='z@glomp.it')
                                                        {                                        
                                                        ?>
                                                        &nbsp;|&nbsp;&nbsp;<a target="_blank" href="<?php echo ADMIN_FOLDER.'/merchant/viewInMerchantPortal/'.$rec_merchant->merchant_id;?>">
                                                            View in Merchant Portal
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div align="center" style="padding:2px;" >			
                                                    <img class="thumbnail" src="<?php echo $this->custom_func->merchant_logo($rec_merchant->merchant_logo) ?>" alt="<?php echo $rec_merchant->merchant_name; ?>" style="width:100px !important;">
                                                </div>
                                                <div align="center" style="padding:0px 5px;" >
                                                    <b><?php echo $rec_merchant->merchant_name;?></b>
                                                </div>                                        
                                            </li>
                                        <?php } ?>                            
                                    </ul> 
                                    <div style="clear:both;">
                                        <hr>
                                        <div class="control-group">
                                            <div class="controls">
                                                <button type="button" name="addPage" id="gridSave" class="btn btn-WI">Save</button>                                
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                else { echo '<div>No records found.</div>';}
                            }
                             else
                            {
                                echo '<div class="page-subtitle" align="center" style="font-size:18px;">You must select a location first.</div>';
                            }
                      } elseif($page_action=="view") { ?>
                      <div class="page-subtitle"><?php
                      echo "View All ";
                      if(isset($breadcum))
                      {
                          echo $breadcum;
                      }
                      if(isset($msg))
                      {
                          echo "<div class=\"success_msg\">".$msg."</div>";
                      }
                      if(isset($error_msg))
                      {
                          echo "<div class=\"custom-error\">".$error_msg."</div>";
                      }
                      if(validation_errors()!="")
                      {
                          echo "<div class=\"custom-error\">".validation_errors()."</div>";
                      }
                      ?>
                      </div>
                      
                      <?php
                        if($location_id>0)
                        {
                      ?>
                          <table class="table table-hover">
                          <tr>
                          <td><strong>SN.</strong></td>
                          <td><strong>Created Date</strong></td>
                          <td><strong>Merchant</strong></td>
                          <td><strong>Location</strong></td>
                          
                          <td style="text-align:center;"><strong>Status</strong></td>
                          <td style="text-align:center;"><strong>Order Position</strong></td>
                          <td style="text-align:center;"><strong>Options</strong></td>
                          </tr>
                          <?php if($res_merchant) { 
                          $i=1;
                          foreach($res_merchant as $rec_merchant) {
                              $tr_class = ($rec_merchant->merchant_status=="Deleted")?"class='error'":"";
                          ?>
                          <tr <?php echo $tr_class;?>>
                          <td><?php echo $i; $i++; ?></td>
                          <td><?php echo $rec_merchant->merchant_created_date;?></td>
                          <td><?php echo $rec_merchant->merchant_name;?></td>
                          <td>
                           <?php
                                 echo $this->regions_m->selectRegionNameByID($rec_merchant->merchant_region_id);
                            ?>
                          </td>
                          <td style="text-align:center;"><?php echo $rec_merchant->merchant_status;?></td>
                          <td style="text-align:center;"> <?php echo $rec_merchant->merchant_rank;?></td>
                          <td style="text-align:center">
                          
                            <?php
                                
                                    echo anchor(ADMIN_FOLDER."/merchant/editRecord/".$rec_merchant->merchant_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
                                    /*
                                    if($rec_merchant->merchant_status!='Deleted'){
                                    echo "&nbsp;&nbsp;&nbsp;";
                                    echo anchor(ADMIN_FOLDER."/merchant/deleteRecord/".$rec_merchant->merchant_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");
                                    }*/
                                    //if($this->session->userdata('admin_email')=='z@glomp.it')
                                    {
                                    
                            ?>
                                &nbsp;|&nbsp;&nbsp;<a target="_blank" href="<?php echo ADMIN_FOLDER.'/merchant/viewInMerchantPortal/'.$rec_merchant->merchant_id;?>">
                                    View in Merchant Portal
                                </a>
                                <?php } 
                                
                                ?>
                                &nbsp;|&nbsp;&nbsp;<a href="/adminControl/merchant/template/<?php echo $rec_merchant->merchant_id?>" class="edit-template">Template</a>
                                &nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url('adminControl/product?location_id='.$rec_merchant->merchant_country_region_id.'&filter=merchant&merchant_id='.$rec_merchant->merchant_id.'.%22&search_q=');?>" class="">View products</a>
                            </td>
                          </tr>
                          <?php }
                          echo '<tr><td colspan="6">'.$links.'</td></tr>';
                          } else echo'<tr><td colspan="6">No records found.</td></tr>'; ?>
                          </table>
                          
                          
                      <?php
                      }
                      else
                      {
                        echo '<div class="page-subtitle" align="center" style="font-size:18px;">You must select a location first.</div>';
                      }
                   } elseif($page_action=="add") { ?>
                  
                  <div class="page-subtitle"><?php
				  echo "Add New Merchant";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <?php echo form_open_multipart(ADMIN_FOLDER."/merchant/addMerchant",'name="frmCms" class="form-horizontal" id="region"')?>
                  <div class="content-box" >
                    
    <div class="control-group">
    	<label class="control-label" for="merchant_name"><span class="required">*</span> Name</label>
    	<div class="controls">
			<input type="text" name="merchant_name" id="merchant_name" required   value="<?php echo set_value('merchant_name');?>" >
    	</div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="merchant_contact_name"><span class="required">*</span>Key Contact Name</label>
        <div class="controls">
            <input type="text" id="merchant_contact_name"  name="merchant_contact_name" required value="<?php echo set_value('merchant_contact_name');?>" >
        </div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="merchant_email"><span class="required">*</span>Key Contact Email</label>
        <div class="controls">
            <input type="email" id="merchant_email"  name="merchant_email" required value="<?php echo set_value('merchant_email');?>" >
        </div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="accounts_contact_name">Accounts Contact Name</label>
        <div class="controls">
            <input type="text" id="accounts_contact_name"  name="accounts_contact_name" value="<?php echo set_value('accounts_contact_name');?>" >
        </div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="accounts_contact_email">Accounts Contact E-mail</label>
        <div class="controls">
            <input type="text" id="accounts_contact_email"  name="accounts_contact_email" value="<?php echo set_value('accounts_contact_email');?>" >
        </div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="notify_excempt_amount">Minimum account settlement</label>
        <div class="controls">
            <input type="text" id="notify_excempt_amount"  name="notify_excempt_amount" value="<?php echo set_value('notify_excempt_amount');?>" >
            <div class="admin-tooltip"><small>Exempt from notification if credit is less than this amount.</small></div>
        </div>
    </div>
                      
    <div class="control-group">
        <label class="control-label" for="notify_top_limit">Credit limit</label>
        <div class="controls">
            <input type="text" id="notify_top_limit"  name="notify_top_limit" value="<?php echo set_value('notify_top_limit');?>" >
            <div class="admin-tooltip"><small>Notify glomp! accounts people when credit reaches this amount.</small></div>
        </div>
    </div>
    <div class="control-group">
   		 <label class="control-label" for="merchant_email"><span class="required"></span>Order Position</label>
   		 <div class="controls">
   				 <input type="text" id="merchant_rank"  name="merchant_rank" value="<?php echo set_value('merchant_rank');?>" >
   			 </div>
    </div>
    
    <hr size="1" />
    <div class="control-group" >
   		 <label class="control-label" for="merchant_username"><span class="required">*</span>Username</label>
   		 <div class="controls">
   				 <input type="text" id="merchant_username" maxlength="45" name="merchant_username" required value="<?php echo set_value('merchant_username');?>" >
   			 </div>
    </div>
	<div class="control-group">
   		 <label class="control-label" for="pword"><span class="required">*</span>Password</label>
   		 <div class="controls">
   				 <input type="password" id="pword" maxlength="9" name="pword" required value="<?php echo set_value('pword');?>" >
   			 </div>
    </div>
	<div class="control-group">
   		 <label class="control-label" for="cpword"><span class="required">*</span>Confirm&nbsp;Password</label>
   		 <div class="controls">
   				 <input type="password" id="cpword" maxlength="9" name="cpword" required value="<?php echo set_value('cpword');?>" >
   			 </div>
    </div>
    <hr size="1" />
	<div class="control-group">
    	<label class="control-label" ><span class="required">*</span>Country</label>
    	<div class="controls">
			<select name="merchant_country" id="country_id">
            	<?php 
					echo $this->regions_m->getAllCountryDropdown(set_value('merchant_country'));
				?>               
            </select>
    	</div>
	</div>
	<div class="control-group">
    	<label class="control-label" ><span class="required">*</span>Address 1</label>
    	<div class="controls">			
			<input type="text" name="merchant_address_1" required value="<?php echo set_value('merchant_address_1');?>" maxlength="50" />
    	</div>
    </div>
	<div class="control-group">
    	<label class="control-label" >Address 2</label>
    	<div class="controls">			
			<input type="text" name="merchant_address_2" value="<?php echo set_value('merchant_address_2');?>" maxlength="50" />
    	</div>
    </div>
	<div class="control-group">
    	<label class="control-label" ><span class="required">*</span>Street</label>
    	<div class="controls">			
			<input type="text" name="merchant_street" value="<?php if(isset($merchant_street))echo $merchant_street;?>" maxlength="50" />
    	</div>
    </div>
	<div class="control-group locality">
    	<label class="control-label" ><span >*</span>Locality Name</label>
    	<div class="controls">			
			<input type="hidden" id="merchant_locality_required" name="merchant_locality_required" value="0" maxlength="1" />
			<select name="merchant_locality_name" >
            	<?php 
					echo $this->regions_m->getLocalityDropdown(set_value('merchant_locality_name'));
				?>               
            </select>
    	</div>
    </div>
	<div class="control-group locality">
    	<label class="control-label" ><span class="required">*</span>Locality</label>
    	<div class="controls">			
			<input type="text" name="merchant_locality" value="<?php echo set_value('merchant_locality');?>" maxlength="50" />
    	</div>
    </div>
	<div class="control-group">
    	<label class="control-label" ><span class="required">*</span>City/Town</label>
    	<div class="controls">			
			<input type="text" name="merchant_city" value="<?php if(isset($merchant_city))echo $merchant_city;?>" maxlength="50" />
    	</div>
    </div>
	<div class="control-group">
    	<label class="control-label" ><span class="required">*</span>Region Name</label>
    	<div class="controls">			
			<select name="merchant_region_name" id="merchant_region_name">
            	<?php 
					echo $this->regions_m->getRegionListDropdown(set_value('merchant_region_name'));
				?>                             
            </select>			
    	</div>
    </div>
	 <div class="control-group">
    	<label class="control-label" ><span class="required">*</span>Region/City</label>
    	<div class="controls">
			<select name="region_id">
            	<?php 
					echo $this->regions_m->getAllRegionDropdown(set_value('region_id'));
				?>               
            </select>
    	</div>
    </div>
	<div class="control-group">
    	<label class="control-label" ><span class="required"></span>ZIP/Postal Code</label>
    	<div class="controls">			
			<input type="text" name="merchant_zip" value="<?php echo set_value('merchant_zip');?>" maxlength="15" />
    	</div>
    </div>
     <div class="control-group">
    	<label class="control-label" for="merchant_logo">Logo</label>
  	 	 <div class="controls">
			<input type="file" id="merchant_logo" name="merchant_logo"/>
    	</div>
    </div>    
     <div class="control-group">
    	<label class="control-label" for="merchant_contact"><span class="required">*</span>Contact</label>
    	<div class="controls">
			<input type="text" name="merchant_contact" value="<?php echo set_value('merchant_contact');?>" />
    	</div>
    </div>   
     
      <div class="control-group">
    	<label class="control-label" for="merchant_fax">Fax</label>
  	 	 <div class="controls">
			<input type="text" id="fax" name="merchant_fax" value="<?php echo set_value('merchant_fax');?>" />
    	</div>
    </div>
         <div class="control-group">
    	<label class="control-label" for="details">Details</label>
  	 	 <div class="controls">
			<textarea id="details" name="merchant_details" rows="3"><?php echo set_value('merchant_details');?></textarea>
    	</div>
    </div>
	
	
	<div class="control-group">
    	<label class="control-label" for="About">*About</label>
  	 	 <div class="controls">
			<!--ckeditor starts-->
		  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
		  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
		  <script type="text/javascript">
		   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
		  </script>		  
          <textarea name="merchant_about" style="width:600px; height:200px;" class="ckeditor"><?php echo set_value('merchant_about');?></textarea>
		  <!--ckeditor ends-->
    	</div>
    </div>
	
	
	<div class="control-group">
    	<label class="control-label" for="Terms and Conditions">Terms and Conditions</label>
  	 	 <div class="controls">
			<!--ckeditor starts-->
		  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
		  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
		  <script type="text/javascript">
		   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
		  </script>
		  <textarea name="merchant_terms" style="width:600px; height:200px;" class="ckeditor"><?php echo set_value('merchant_terms');?></textarea>
		  <!--ckeditor ends-->
    	</div>
    </div>
		

    <div class="control-group">
    <div class="controls">
    <button type="submit" name="addPage" class="btn btn-WI">Create Merchant</button>
      <input type="reset" name="reset" id="add_reset" value="Reset" class="btn btn-WI">
    </div>
    </div>
                  </div>
                  <?php 
				  echo form_close(); 
				  ?>
                  <?php } elseif($page_action=="edit") { ?>                  
                  <div class="page-subtitle">
				  <?php
				  echo "Edit Merchant";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                 
                 
                 <table class="merchant-tb-left" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
                	<td width="49%">
					 <div class="content-box">
					<?php echo form_open_multipart(ADMIN_FOLDER."/merchant/editRecord/".$rec_merchant->merchant_id,'name="frmCms" class="form-horizontal" id="region"')?>
                    	<table width="100%" cellpadding="5" cellspacing="5" border="0">
                        <tr>
                        	<td width="150"><span class="required">*</span> Name</td>
                            <td><input type="text" name="merchant_name" id="merchant_name" value="<?php echo $rec_merchant->merchant_name;?>" required   /></td>
                        </tr>
    <tr>
        <td><label for="merchant_contact_name"><span class="required">*</span>Key Contact Name</label></td>
        <td>
            <input type="text" id="merchant_contact_name"  name="merchant_contact_name" required value="<?php echo $rec_merchant->merchant_contact_name; ?>" >
        </td>
    </tr>
                      
    <tr>
        <td><label for="merchant_email"><span class="required">*</span>Key Contact Email</label></td>
        <td>
            <input type="email" id="merchant_email"  name="merchant_email" required value="<?php echo $rec_merchant->merchant_email;?>" >
        </td>
    </tr>
    
    <tr>
        <td for="accounts_contact_name">Accounts Contact Name</td>
        <td class="controls">
            <input type="text" id="accounts_contact_name"  name="accounts_contact_name" value="<?php echo $rec_merchant->accounts_contact_name;?>" >
        </td>
    </tr>
                      
    <tr>
        <td><label for="accounts_contact_email">Accounts Contact E-mail</label></td>
        <td>
            <input type="email" id="accounts_contact_email"  name="accounts_contact_email" value="<?php echo $rec_merchant->accounts_contact_email;?>" >
        </td>
    </tr>
    
    <tr>
        <td><label for="notify_excempt_amount">Minimum account settlement</label></td>
        <td>
            <input type="text" id="notify_excempt_amount"  name="notify_excempt_amount" value="<?php echo $rec_merchant->notify_excempt_amount; ?>" >
            <div class="admin-tooltip"><small>Exempt from notification if credit is less than this amount.</small></div>
        </td>
    </tr>
                      
    <tr>
        <td><label for="notify_top_limit">Credit limit</label></td>
        <td>
            <input type="text" id="notify_top_limit"  name="notify_top_limit" value="<?php echo $rec_merchant->notify_top_limit; ?>" >
            <div class="admin-tooltip"><small>Notify glomp! accounts people when credit reaches this amount.</small></div>
        </td>
    </tr>
                        <tr>
							<td><span class="required"></span>Order Position</td>
							<td>
								<input type="text" id="merchant_rank" name="merchant_rank"  value="<?php echo $rec_merchant->merchant_rank;?>" >
							</td>
                        </tr>
                        <tr><td colspan="2"><hr style="margin:0px; padding:0px;" size="1" /></td></tr>
                        <tr>
							<td><span class="required"></span>Username</td>
							<td>
								<input type="text" id="merchant_username" maxlength="45" name="merchant_username"  value="<?php echo $rec_merchant->merchant_username;?>" >
                                <input type="hidden" name="current_merchant_id"  value="<?php echo $rec_merchant->merchant_id;?>" >
							</td>
                        </tr>
						<tr>
							<td><span class="required"></span>New Password</td>
							<td>
								<input type="password" id="npword" maxlength="9" name="npword"  value="" >								
							</td>
                        </tr>
						<tr>
							<td><span class="required"></span>Confirm New Pass</td>
							<td>
								<input type="password" id="cnpword" maxlength="9" name="cnpword"  value="" >								
							</td>
                        </tr>
                        <tr><td colspan="2"><hr style="margin:0px; padding:0px;" size="1" /></td></tr>
						<tr>
							<td><span class="required">*</span>Country</td>
							<td>
								<select name="merchant_country" id="country_id">
									<?php 
										echo $this->regions_m->getAllCountryDropdown(($rec_merchant->merchant_country_region_id));
									?>               
								</select>
							</td>
                        </tr>
						<tr>
							<td><span class="required">*</span>Address 1</td>
							<td>
								<input type="hidden" id="merchant_address_id" name="merchant_address_id" value="<?php echo $rec_merchant_address->address_id;?>" maxlength="1" />								
								<input type="text" maxlength="50" id="merchant_address_1" name="merchant_address_1" required value="<?php echo $rec_merchant_address->address_1;?>" >
							</td>
                        </tr>
						<tr>
							<td><span class="required"></span> Address 2</td>
							<td>
								<input type="text" maxlength="50" id="merchant_address_2" name="merchant_address_2" value="<?php echo $rec_merchant_address->address_2;?>" >
							</td>
                        </tr>
						<tr>
							<td><span class="required">*</span>Street</td>
							<td>
								<input type="text" maxlength="50" id="merchant_street" name="merchant_street" value="<?php echo $rec_merchant_address->address_street;?>" >
							</td>
                        </tr>
						<tr class="locality">
							<td><span class="required">*</span>Locality Name</td>
							<td>
								<input type="hidden" id="merchant_locality_required" name="merchant_locality_required" value="0" maxlength="1" />								
								<select name="merchant_locality_name" >
									<?php 
										echo $this->regions_m->getLocalityDropdown($rec_merchant_address->address_locality_name);
									?>               
								</select>	
							</td>
                        </tr>
						<tr class="locality">
							<td><span class="required"></span>Locality</td>
							<td>
								<input type="text" maxlength="50" id="merchant_locality" name="merchant_locality" value="<?php echo $rec_merchant_address->address_2;?>" >
							</td>
                        </tr>						
						<tr>
							<td><span class="required">*</span>City/Town</td>
							<td>
								<input type="text" id="merchant_city" name="merchant_city" value="<?php echo $rec_merchant_address->address_city_town;?>" >
							</td>
                        </tr>
						<tr>
							<td><span class="required">*</span>Region Name</td>
							<td>								
								<select name="merchant_region_name" id="merchant_region_name">
									<?php 
										echo $this->regions_m->getRegionListDropdown( ($rec_merchant_address->address_region_name) );
									?>                             
								</select>
							</td>
                        </tr>
						<tr>
							<td><span class="required">*</span>Region/City</td>
							<td>
								<select name="region_id">
									<?php echo $this->regions_m->getAllRegionDropdown($rec_merchant_address->address_region);?>
								</select>
							</td>
                        </tr>
                        <tr>
                          <td><span class="required"></span>ZIP/Postal Code</td>							
                          <td>
							<input type="text" id="merchant_zip" name="merchant_zip" value="<?php echo $rec_merchant_address->address_zip_code;?>" >			
                         </td>
                        </tr>
                        <tr>
                          <td>Logo</td>
                          <td>
                            <input type="file" id="merchant_logo" name="merchant_logo"/>
                            <input type="hidden" name="old_image" value="<?php echo $rec_merchant->merchant_logo;?>" />
                         </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td> <?php if($rec_merchant->merchant_logo!="" && 
			file_exists("custom/uploads/merchant/thumb/".$rec_merchant->merchant_logo))?>
            <img src="<?php echo "custom/uploads/merchant/thumb/".$rec_merchant->merchant_logo;?>" /> </td>
                        </tr>
                        <tr>
                          <td><span class="required">*</span>Contact</td>
                          <td>
                            <input type="text" name="merchant_contact" value="<?php echo $rec_merchant->merchant_contact;?>" />
                         </td>                                                
                        </tr>
                        <tr>
                          <td>Fax</td>
                          <td>
                            <input type="text" id="fax2" name="merchant_fax" value="<?php echo $rec_merchant->merchant_fax;?>" />
                        </td>
                        </tr>
                        <tr>
                          <td>Details</td>
                          <td><textarea id="merchant_details" name="merchant_details" rows="3"><?php echo $rec_merchant->merchant_details;?></textarea></td>
                        </tr>
						<tr>                          
                          <td colspan="2">*
							About<br>
							<!--ckeditor starts-->
							  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
							  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
							  <script type="text/javascript">
							   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
							  </script>
							  <textarea name="merchant_about" style="width:600px; height:200px;" class="ckeditor"><?php echo $rec_merchant->merchant_about;?></textarea>
							  <!--ckeditor ends-->
						  </td>
                        </tr>
						<tr>                          
                          <td colspan="2">
							Terms and Conditions<br>
							<textarea name="merchant_terms" style="width:600px; height:200px;" class="ckeditor"><?php echo $rec_merchant->merchant_terms;?></textarea>
						  </td>
                        </tr>
                        <tr>
                            <td>Banner</td>
                            <td>
                            <?php
                              $merchant_banner =  (!isset($rec_merchant->banner) || $rec_merchant-> banner == '') ? 'merchant.jpg' : ''.$rec_merchant->banner;
                            ?>
                                <img src="<?php echo base_url('/custom/uploads/merchant/'.$merchant_banner);?>" id="preview-banner" width="100" />
                                <input type="file" name="banner" id="banner" />
                            </td>
                        </tr>
                        <tr>
                          <td>Merchant Status</td>
                          <td>Active
                              <input type="radio" name="merchant_status" value="Active" <?php echo ($rec_merchant->merchant_status=='Active')?"Checked='checked'":"";?> />
InActive
<input type="radio" name="merchant_status" value="InActive" <?php echo ($rec_merchant->merchant_status!='Active')?"Checked='checked'":"";?> />
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td> <input type="hidden" name="merchant_id" value="<?php echo $rec_merchant->merchant_id;?>" />
                            <button type="submit" name="editPage" class="btn btn-WI">Update Merchant</button>
                            <button type="button" name="edit-template" class="edit-template btn btn-WI" href="/adminControl/merchant/template/<?php echo $rec_merchant->merchant_id?>">Template</button>
                            <input type="reset" name="reset" value="Reset" class="btn btn-WI"></td>
                        </tr>
                        </table>
                        <?php echo form_close();?>
                        </div>
                    </td>
                  <td width="49%" valign="top">
                    <div class="content-box" style="margin-left:10px;">
                   	<p>Merchant Outlet:</p>
                    <?php 
					echo form_open(ADMIN_FOLDER."/merchant/editRecord/". $rec_merchant->merchant_id);
					$outlet_name		="";
					$outlet_code		="";
                    $outlet_phone_number="";
					$outlet_id			="";
					
					$outlet_country		="";
					$outlet_address_id	="";
					$outlet_address_1	="";
					$outlet_address_2	="";
					$outlet_street		="";
					$outlet_locality_name="";
					$outlet_locality	="";
					$outlet_city		="";
					$outlet_region_name	="";
					$outlet_zip			="";
					
					$btn_name = "Add New";
					if(isset($outlet_edit))
					{
						$outlet_name = $res_outlet_by_id->outlet_name;
						$outlet_code = $res_outlet_by_id->outlet_code;
                        $outlet_phone_number = $res_outlet_by_id->outlet_phone_number;
						$outlet_id = $res_outlet_by_id->outlet_id;
						
						$outlet_country		=$res_outlet_address->address_country_id;
						$outlet_address_id	=$res_outlet_address->address_id;
						$outlet_address_1	=$res_outlet_address->address_1;
						$outlet_address_2	=$res_outlet_address->address_2;
						$outlet_street		=$res_outlet_address->address_street;
						$outlet_locality_name=$res_outlet_address->address_locality_name;
						$outlet_locality	=$res_outlet_address->address_locality;
						$outlet_city		=$res_outlet_address->address_city_town;
						$outlet_region_name	=$res_outlet_address->address_region_name;
						$outlet_zip			=$res_outlet_address->address_zip_code;
						
						$btn_name = "Update";
					}
					?>
                   <input type="hidden" name="merchant_id" value="<?php echo $rec_merchant->merchant_id;?> " />
                    <input type="hidden" name="outlet_id" value="<?php echo $outlet_id;?> " />
					
					
					<table width="" cellpadding="5" cellspacing="5" border="0">
                        <tr>
                        	<td width="120">
								<span class="required">*</span>Outlet Name
							</td>
                            <td>
								<input type="text" name="outlet_name" value="<?php echo $outlet_name;?>" required placeholder="Outlet Name" />
							</td>                        
                        	<td width="120">
								<span class="required">*</span>Outlet Code
							</td>
                            <td>
								<input type="hidden" id="outlet_code" name="outlet_code" value="<?php echo $outlet_code?>" required placeholder="Outlet Code" />
                                <div id="outlet_code_text" ><?php echo $outlet_code?></div>
							</td>
                        </tr>
						<tr>
							<td colspan="4" cellpadding="0" cellspacing="0"  ><hr style="margin:0px;padding:0px;" /></td>
						</tr>
						<tr>
                            <td width="120">
								<span class="required">*</span>Outlet Phone Number
							</td>
                            <td>
								<input type="text" id="outlet_phone_number" name="outlet_phone_number" value="<?php echo $outlet_phone_number?>" required placeholder="Outlet Phone Number" maxlength="45" />
							</td>
							<td><span class="required">*</span>Country</td>
							<td>
								<select name="outlet_country" id="country_id_outlet">
									<?php 
										echo $this->regions_m->getAllCountryDropdown(($outlet_country));
									?>               
								</select>
							</td>
                        </tr>						
						<tr>
							<td><span class="required">*</span>Address 1</td>
							<td>
								<input type="hidden" id="outlet_address_id" name="outlet_address_id" value="<?php echo $outlet_address_id;?>" maxlength="1" />																
								<input type="text" maxlength="50" id="outlet_address_1" name="outlet_address_1" required value="<?php echo $outlet_address_1;?>" >
							</td>
                        
							<td><span class="required"></span> Address 2</td>
							<td>
								<input type="text" maxlength="50" id="outlet_address_2" name="outlet_address_2" value="<?php echo $outlet_address_2;?>" >
							</td>
                        </tr>						
						<tr>
							<td><span class="required">*</span>Street</td>
							<td colspan="3">
								<input type="text" style="width:97%" maxlength="50" id="outlet_street" name="outlet_street" value="<?php echo $outlet_street;?>" >
							</td>
                        </tr>
						<tr class="locality-outlet">
							<td><span class="required"></span>Locality Name</td>
							<td>
								<input type="hidden" id="outlet_locality_required" name="outlet_locality_required" value="0" maxlength="1" />								
								<select name="outlet_locality_name" >
									<?php 
										echo $this->regions_m->getLocalityDropdown($outlet_locality_name);
									?>               
								</select>	
							</td>                        
							<td><span class="required"></span>Locality</td>
							<td>
								<input type="text" maxlength="50" id="outlet_locality" name="outlet_locality"  value="<?php echo $outlet_locality;?>" >
							</td>
                        </tr>						
						<tr>
							<td><span class="required">*</span>City/Town</td>
							<td>
								<input type="text" id="outlet_city" name="outlet_city" value="<?php echo $outlet_city;?>" >
							</td>                        
							<td><span class="required"></span>Region Name</td>
							<td>								
								<select name="outlet_region_name" id="outlet_region_name">
									<?php 
										echo $this->regions_m->getRegionListDropdown( ($outlet_region_name) );
									?>                             
								</select>
							</td>
                        </tr>
						<tr>
							<td><span class="required">*</span>Region/City</td>
							<td>
								<select name="outlet_region_id" id="outlet_region_id">	
									<?php
                                                                        $outlet_region_id = 0;
                                                                        if(isset($res_outlet_by_id->outlet_region_id)) {
                                                                            $outlet_region_id = $res_outlet_by_id->outlet_region_id;
                                                                        }
									echo $this->regions_m->getAllRegionDropdown($outlet_region_id); ?>
								</select>
							</td>                        
                          <td><span class="required"></span>ZIP/Postal Code</td>							
                          <td>
							<input type="text" id="outlet_zip" name="outlet_zip" value="<?php echo $outlet_zip;?>" >			
                         </td>
                        </tr>
					</table>
                    <input type="submit"  id="add_new_outlet" name="add_outlet" class="btn btn-WI" value="<?php echo $btn_name;?>" />
                    <?php
					echo form_close();
					 ?>
                        <table class="table"  width="100%">
                        	<tr>
                            	<td>SN</td>
                                <td>Name</td>
                                <td>Code</td>
                                 <td>Option</td>
                            </tr>
                            <?php
							$i = 1; 
							foreach($res_outlet->result() as $row){
							?>
                            <tr>
                            	<td><?php echo $i;?></td>
                                <td><?php echo $row->outlet_name;?></td>
                                <td><?php echo $row->outlet_code;?></td>
                                 <td style="text-align:center">
                                 <?php
                                    echo anchor(ADMIN_FOLDER."/merchant/editRecord/".$rec_merchant->merchant_id."/".$row->outlet_id, "<i class=\"icon-edit\"></i>","title='Edit' ");
                                    echo "&nbsp;&nbsp;&nbsp;";
                                    echo anchor(ADMIN_FOLDER."/merchant/deleteRecord/".$rec_merchant->merchant_id."/".$row->outlet_id, "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\""); ?></td>
                            </tr>
                            <?php 
							$i++;
							}
							?>
                        </table>
                    
                  </div></td>
                </tr>                 
                 
                 
                 </table>
                 
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <!--<script src="custom/lib/bootstrap/js/bootstrap.js"></script> -->
        <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">
        <link rel="stylesheet" href="assets/frontend/css/jcrop/jquery.Jcrop.min.css">
        <style type="text/css">
            .ui-dialog.ui-widget {
                font-size: 85%;
                padding:0;
                border-radius:0px;
            }
            .ui-dialog .ui-dialog-titlebar {
                border-radius:0;
                border:0;
            }
            .ui-dialog.ui-widget-content {
                -webkit-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                -moz-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
            }
        </style>
        <script type="text/javascript" src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript" src="assets/backend/lib/jquery.form.min.js"></script>
        <script type="text/javascript" src="/assets/backend/lib/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/assets/backend/lib/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="/assets/frontend/js/jquery.Jcrop.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                var TemplateDialog = jQuery('<div id="template-dialog" />');
                TemplateDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Edit Template',
                    modal: true,
                    draggable: false,
                    width: 864,
                    buttons: []
                });
                jQuery(document).on('click', '.edit-template', function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url : jQuery(this).attr('href'),
                        dataType: 'json'
                    }).done(function(response){
                        var url = '/adminControl/templates/index/'+response.template.id;
                        var buttons = TemplateDialog.dialog('option','buttons');
                        TemplateDialog
                            .html('<iframe frameborder="0" scrolling="no" width="840" height="600" src="'+url+'"></iframe>')
                            .dialog('option','buttons',[{
                                text: "Mobile Preview",
                                click: function() {
                                    window.open('/m/profile/menu/0?tab=merchants&mID=<?php echo $rec_merchant->merchant_id;?>&mDetail=info&preview=1', "", "width=400, height=700");
                                }
                            },{
                                text: "Desktop Preview",
                                click: function() {
                                    window.open('/merchant/about/<?php echo $rec_merchant->merchant_id;?>?frm=0&preview=1');
                                }
                            },{
                                text: "Cancel",
                                click: function() {
                                    TemplateDialog.dialog('close');
                                }
                            }])
                            .dialog('open');

                    });
                });            
            });
        </script>
    </body>
</html>