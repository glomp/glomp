{brands}
<tr id="record-{id}">
    <td>{id}</td>
    <td>{name}</td>
    <td>{description}</td>
    <td>{logo}</td>
    <td>{type}</td>
	<td>{public_alias}</td>
	<td>{status}</td>
	<td>
        <a href="/adminControl/brands/edit/{id}" title="Edit"><i class="icon-edit" data-id="{id}"></i></a>
        &nbsp;&nbsp;
        <a href="/adminControl/brands/delete/{id}" title="Delete" class="delete-record" data-id="{id}"><i class="icon-trash"></i></a>
        &nbsp;&nbsp;
        <a href="/adminControl/brands/template/{id}" class="edit-template" data-id="{id}">Template</a>
    </td>
</tr>
{/brands}
