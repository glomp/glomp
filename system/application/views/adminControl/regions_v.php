<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript">
    	function submit_region(id)
		{
			var link_ = '<?php echo base_url(ADMIN_FOLDER.'/regions/index');?>';
			link_ = link_+"/"+id;
			location.href=link_;
			
		}		
    
   </script>
   <script language="javascript" type="text/javascript">
            $(document).ready(function(e){
            
                
                /*/Starts to two because 1 is pre-created*/
                var master_tr_ctr = 2;
                
                $('#add_more_tr').click(function(e) {
                    /*/Clone last created row*/
                    var clone = $("tr[id^='master_tr_']:last").clone();
                    /*/Change unique ID*/
                    clone = $(clone).attr('id', 'master_tr_' + master_tr_ctr);
                    /*/Add counter for unique ID*/
                    master_tr_ctr++;
                    
                    $("tr[id^='master_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });
                
               
                $("#region_table_prefix").on('click', '.remove_tr', function() {
                    $(this).parent().parent().remove();
                });
                
                $('#add_more_tr_edit').click(function(e) {
                    var select_html = $('#product_id_0').html();
                    var tr_html = '<tr id="master_tr"><td><input type="text" name="new_region_outlet_prefix[]" style="width:50px" value="" maxlength="3"></td><td><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';

                    $('#region_table_prefix tr:last').after(tr_html);
                });
                $("#region_table_prefix").on('click', '.delete_tr', function() {
                    if (!confirm('Are you sure you want to delete this record?'))
                    {
                        return false;
                    }
                    
                    
                    var id = $(this).data().id;
                    var prefix = $(this).data().prefix;
                    
                    <?php if($page_action=="edit") { ?>
                        var base_url = '<?php echo base_url(ADMIN_FOLDER . '/regions/remove_outlet_code_prefix'); ?>';
                        $.ajax({
                            type: 'GET',
                            url: base_url + "/" + id+ "/" + prefix,
                            success: function(html) {
                                if(html=='deleted')
                                {
                                    $('#prefix_'+id).parent().parent().remove();
                                }
                                else if(html=='in_use')
                                {   
                                    alert("Cannot be deleted.This outlet code prefix is currently in use.")
                                }
                            }
                        });
                    <?php } ?>
                });
            });
        </script>
        <style type="text/css">
            .hidden_td{
                visibility:hidden;
            }
        </style>
  </head>
  <body class="">
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
			<?php //include("includes/right-sidebar.php");?> 
            <div class="span12">
     
                  <?php if($page_action=="view") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span3">
                  <div class="page-heading">Manage region</div>
                  	
                  </div>
                  <div class="span9" style="text-align:right;">                    
                    <div class="" style="border:0px solid;float:right;text-align:right;margin-left:20px;">
                        <?php echo anchor(ADMIN_FOLDER."/regions/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                        <?php echo anchor(ADMIN_FOLDER."/regions/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                    </div>
                    <div class="" style="border:0px solid;float:right;text-align:right;">
                        <?php 
                        echo form_open(ADMIN_FOLDER.'/regions/');?>
                            <span>Select Location &nbsp;</span>
                          <select name="parent_id" onChange="submit_region(this.value)">
                            <option value="0">Select Location</option>
                            <?php  $this->regions_m->location_dropdown(0,$parent_id);?>
                          </select>
                          <?php 
                            echo form_close();
                          ?>
                      </div>
                  
                      <div class="" style="border:0px solid;float:right;text-align:right;">
                      <?php 
                       echo form_open(ADMIN_FOLDER.'/regions/', "method='get'");?>                    
                      <input type="text" value="<?php echo $search_q;?>" placeholder="Search..." name="search_q" id="search_q" style="width:200px; margin-right:5px; margin-bottom:0px;" class="fr ui-corner-all" />
                      <input class="btn btn-WI" style="margin-right:40px; width:70px; height:30px;" type="submit" name="" value="Search" >                  
                      <?php 
                        echo form_close();
                      ?>
                       </div>
                   
                  </div>
                  </div>
                  </div>
                 
                  <div class="page-subtitle"><?php
				  echo "View All ";
				 
				  if(isset($breadcum))
				  {
					  echo $breadcum;
				  }
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <table class="table table-hover">
                  <tr>
                  <td><strong>SN.</strong></td>
                  <td><strong>Regions</strong></td>
                  <td style="text-align:center;"><strong>Selectable</strong></td>
                  <td style="text-align:center;"><strong>Prefix Outlet Codes</strong></td>
                  <td style="text-align:center;"><strong>Active</strong></td>
                  <td style="text-align:center;"><strong>Options</strong></td>
                  </tr>
                  <?php if($res_region) { 
				  $i=1;
				  foreach($res_region as $rec_region) {
				  ?>
                  <tr>
                  <td><?php echo $i; $i++; ?></td>
                  <td><?php
				 $cnt = $this->regions_m->TotalSubRegion($rec_region->region_id);
				  if($cnt>0)
				  {
					  echo anchor(ADMIN_FOLDER."/regions/index/".$rec_region->region_id."/",$rec_region->region_name.' ('.$cnt.')');
				  }else
				  	echo $rec_region->region_name;
				  
				  
				  ?></td>
                  <td style="text-align:center;"><?php echo ($rec_region->region_selectable=='Y')?'Yes':'No';?></td>
                  <td><?php                  
                    $region_outlet_prefix=$this->merchant_m->get_prefix_outlet_codes($rec_region->region_id);
                    if(isset($region_outlet_prefix) )
                    {
                        $count=0;
                        foreach ($region_outlet_prefix->result() as $prefix) {                                                                                                                        
                            echo $prefix->prefix_outlet_code.", ";
                        }
                    }                  
                    ?></td>
                  <td style="text-align:center;">
                  <?php if($rec_region->region_status=='Active'){?>
				  <?php echo anchor(ADMIN_FOLDER."/regions/publish/".$rec_region->region_id."/N/", '<img src="assets/images/icons/active.png" alt="Active" border="0" title="Active" />');?>					
                  <?php }else{?>
                  <?php echo anchor(ADMIN_FOLDER."/regions/publish/".$rec_region->region_id."/Y/", '<img src="assets/images/icons/inactive.png" alt="Inactive" border="0" title="Inactive" />');?>
                  <?php }?>
                  </td>
                  <td style="text-align:center;">
                   <?php
				  
					   echo anchor(ADMIN_FOLDER."/regions/index/".$rec_region->region_id."/", "<i class=\"icon-folder-close\"></i>","title='Sub region' ");
					   echo "&nbsp;&nbsp;&nbsp;";
				  
					echo anchor(ADMIN_FOLDER."/regions/editPage/".$rec_region->region_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
					echo "&nbsp;&nbsp;&nbsp;";
					/*echo anchor(ADMIN_FOLDER."/regions/deletePage/".$rec_region->region_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");*/
					?>
                  </td>
                  </tr>
                  <?php 
				  }
				  echo '<tr><td colspan="5">'.$links.'</td></tr>';
				  } else echo'<tr><td colspan="5">No records found.</td></tr>'; ?>
                  </table>
                  <?php } elseif($page_action=="add") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span8">
                  <div class="page-heading">Manage region</div>
                  </div>
                  <div class="span4" style="text-align:right;">
				  <?php echo anchor(ADMIN_FOLDER."/regions/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  <?php echo anchor(ADMIN_FOLDER."/regions/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  </div>
                  </div>
                  </div>
                  <div class="page-subtitle"><?php
				  echo "Add New region";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <?php echo form_open(ADMIN_FOLDER."/regions/addPage",'name="frmCms" id="region"')?>
                  <div class="content-box">                    
                  
                    <table  id="" cellpadding="15" border="0" width="100%">                                          
                        <tr>
                            <td width="100" valign="top">
                                <?php
                                    
                                 if($region_child)
                                 {?>
                                  Parent Region:<br>
                                  <select name="parent_id">
                                  <option value="0">Choose Parent Region</option>
                                  <?php $this->regions_m->location_dropdown(0,set_value('parent_id')); ?>
                                  </select><br>
                                  <?php 
                                 }else echo ' <input type="hidden" name="parent_id" value="0" />';
                                  ?>
                                 
                                  * Region Name:<br>
                                  <input type="text" name="region_name" required value="<?php echo set_value('region_name'); ?>"><br>
                                  * Timezone:<br>
                                  <select name="region_timezone">
                                  <?php
                                    foreach($time_zones as $key => $value)
                                    {?>
                                        <option value="<?php echo $value;?>"><?php echo $key;?></option>
                                    <?php                       
                                    }
                                  ?>
                                  </select>
                                  
                                  <br>
                                  Selectable<br>
                                   <input type="checkbox" name="region_selectable"  checked />                                
                            </td>
                            <td valign="top">
                                <table cellpadding="5" cellspacing="5" id="region_table_prefix"  border="0">                                          
                                    <tr width="10">
                                        <td colspan="2"><a href="javascript:void(0)" id="add_more_tr" class="btn btn-info">Add more outlet codes prefix&nbsp;<i class="icon-plus"></i></a></td>                                        
                                    </tr>
                                    <tr>
                                        <td>* Outlet Codes Prefix</td>                                                
                                        <td>&nbsp;</td>                                        
                                    </tr>
                                    <tr id="master_tr_1">
                                        <?php
                                            $prefix="";
                                            if(isset($region_outlet_prefix) && $region_outlet_prefix[0]!='')
                                            {
                                                $prefix=$region_outlet_prefix[0];
                                            }
                                        ?>
                                        <td>
                                            <input type="text" name="region_outlet_prefix[]" style="width:50px" value="<?php echo $prefix;?>" maxlength="3">
                                        </td>                                                                                        
                                        <td  class="hidden_td"><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td>
                                    </tr> 
                                    <?php
                                        if(isset($region_outlet_prefix) && count($region_outlet_prefix)>1)
                                        {
                                            $prefix=$region_outlet_prefix[0];
                                            $count=0;
                                            foreach ($region_outlet_prefix as $prefix) {                        
                                                if (! empty($prefix) && $count>0){
                                                ?>
                                                    <tr id="master_tr_<?php echo ($count+1);?>">
                                                        <td>
                                                            <input type="text" name="region_outlet_prefix[]" style="width:50px" value="<?php echo $prefix;?>" maxlength="3">
                                                        </td>                                                                                        
                                                        <td  class=""><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td>
                                                    </tr> 
                                                <?php
                                                }
                                                $count++;
                                            }
                                        }
                                    ?>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                   
                    
                   
                    <input class="btn btn-WI" style="height:40px; width:100px;" type="submit" name="addPage" value="Add" >
                    <button class="btn" style="height:40px; width:100px;" type="button" name="addPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>

                  </div>
                  <?php echo form_close(); ?>
                  <?php } elseif($page_action=="edit") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                          <div class="span8">
                            <div class="page-heading">Manage region</div>
                          </div>
                          <div class="span4" style="text-align:right;">
                            <?php echo anchor(ADMIN_FOLDER."/regions/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                            <?php echo anchor(ADMIN_FOLDER."/regions/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                          </div>
                      </div>
                  </div>
                  <div class="page-subtitle"><?php
				  echo "Edit region";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
			  ?>
                 </div>
                 <?php echo form_open(ADMIN_FOLDER."/regions/editPage/".$res_region->region_id,'name="frmCms" id="cmspages"')?>
                 <div class="content-box">
                 <table  id="" cellpadding="15" border="0" width="100%">                                          
                        <tr>
                            <td width="100" valign="top">
                                Parent region:<br>
                                  <select name="parent_id">
                                  <option value="0">Choose Parent region</option>
                                   <?php $this->regions_m->location_dropdown(0,$res_region->region_parent_id); ?>
                                  </select><br>
                                 
                                  * Region Name:<br/>
                                  <input type="text" name="region_name" required value="<?php echo $res_region->region_name; ?>" />                  
                                  <br>                  
                                  * Timezone:<br>
                                  <select name="region_timezone">
                                  <?php
                                    foreach($time_zones as $key => $value)
                                    {?>
                                        <option value="<?php echo $value;?>"
                                            <?php 
                                                if($res_region->region_timezone==$value)
                                                    echo 'selected="selected"';
                                            ?>                            
                                        ><?php echo $key;?></option>
                                    <?php                       
                                    }
                                  ?>
                                  </select><br>
                                   Selectable<br>
                                   <input type="checkbox" name="region_selectable" <?php echo ( $res_region->region_selectable=='Y')?'checked':'';?> /><br>
                            
                            </td>
                            <td valign="top">
                                <table cellpadding="5" cellspacing="5" id="region_table_prefix"  border="0">                                          
                                    <tr width="10">
                                        <td colspan="2"><a href="javascript:void(0)" id="add_more_tr_edit" class="btn btn-info">Add more outlet codes prefix&nbsp;<i class="icon-plus"></i></a></td>                                        
                                    </tr>
                                    <tr>
                                        <td>* Outlet Codes Prefix</td>                                                
                                        <td>&nbsp;</td>                                        
                                    </tr>                                   
                                    <?php
                                        if(isset($region_outlet_prefix) )
                                        {                                            
                                            $count=0;                                            
                                            foreach ($region_outlet_prefix->result() as $prefix) {                        
                                                $hidden_td="";
                                                if ($count==0)
                                                {
                                                    $hidden_td="hidden_td";
                                                }
                                                ?>
                                                    <tr id="master_tr_<?php echo ($count+1);?>">
                                                        <td>
                                                            <?php echo $prefix->prefix_outlet_code;?>
                                                        </td>                                                                                        
                                                        <td  class="<?php echo $hidden_td;?>"><a href="javascript:void(0)" data-prefix="<?php echo $prefix->prefix_outlet_code;?>" data-id="<?php echo $prefix->id;?>" id="prefix_<?php echo $prefix->id;?>"  class="delete_tr">Remove <i class="icon-remove"></i></a></td>
                                                    </tr> 
                                                <?php                                                
                                                $count++;
                                            }
                                        }
                                                                                
                                        if(isset($new_region_outlet_prefix) && count($new_region_outlet_prefix)>0)
                                        {                                            
                                            $count=0;
                                            foreach ($new_region_outlet_prefix as $prefix) {                                                                        
                                                ?>
                                                    <tr id="master_tr_<?php echo ($count+1);?>">
                                                        <td>
                                                            <input type="text" name="new_region_outlet_prefix[]" style="width:50px" value="<?php echo $prefix;?>" maxlength="3">
                                                        </td>                                                                                        
                                                        <td  class=""><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td>
                                                    </tr> 
                                                <?php                                                
                                                $count++;

                                            }
                                        }
                                    ?>                                    
                                </table>
                           </td>
                        </tr>
                    </table>  
                  <input class="btn btn-WI" style="height:40px; width:100px;" type="submit" name="editPage" value="Save" >
                  <button class="btn" style="height:40px; width:100px;" type="button" name="editPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


