<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Create Brand - Glomp! Admin</title>
        <base href="<?php echo base_url(); ?>" />
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">
        <link rel="stylesheet" href="assets/frontend/css/jcrop/jquery.Jcrop.min.css">
        
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <style type="text/css">
            .ui-dialog.ui-widget {
                font-size: 85%;
                padding:0;
                border-radius:0px;
            }
            .ui-dialog .ui-dialog-titlebar {
                border-radius:0;
                border:0;
            }
            .ui-dialog.ui-widget-content {
                -webkit-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                -moz-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
            }
        </style>
    </head>
    <body ci-tpl="brands_create">
        <?php include("includes/header.php"); ?>

        <div class="content">
            <?php include("includes/main-menu.php");?>
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span2">
                            <div class="page-heading">Manage Brands</div>
                        </div>
                        <div class="span10">
                            <div style="float:right;margin-right:10px;">
                                <form id="filter" action="/adminControl/brands/index">
                                    <input type="text" name="key" placeholder="Search..." style="margin:0;" value="<?php echo $this->input->get('key')?>" id="key" />
                                    &nbsp;
                                    <input type="submit" value="Search" class="btn btn-WI" />
                                    <?php if($this->input->get('key')):?>
                                    &nbsp;
                                    <input type="button" value="Clear" class="btn" onclick="document.getElementById('key').value='';document.getElementById('filter').submit()" />
                                    <?php endif;?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-subtitle">Create New Brand</div>
                <div class="content-box">

                    <?php echo ($error)?'<div class="ui-state-error" style="padding:10px 5px 0;">'.$error.'</div>&nbsp;':''; ?>

                    <form method="post" enctype="multipart/form-data" id="brand-create-form">
                        <div class="field row-fluid">
                            <label for="name" class="span2">Name *</label>
                            <div class="span10">
                                <input type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="logo" class="span2">Logo *<br /><small>Max of 1200 x 1200 pixels</small></label>
                            <div class="span10">
                                <img src="" id="preview-logo" width="100" /><br />
                                <input type="file" name="logo" id="logo" value="<?php echo set_value('logo'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="thumbnail" class="span2">Thumbnail *<br /><small>For brand listings<br />Max of 200 x 200 px</small></label>
                            <div class="span10">
                                <img src="" id="preview-thumbnail" width="100" /><br />
                                <input type="file" name="thumbnail" id="thumbnail" value="<?php echo set_value('thumbnail'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="banner" class="span2">Banner<br /><small>Max of 1200 x 1200 pixels<br />Click preview to crop</small></label>
                            <div class="span10">
                                <img src="" id="preview-banner" width="100" /><br />
                                <input type="file" name="banner" id="banner" value="<?php echo set_value('banner'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="location_id" class="span2">Location *</label>
                            <div class="span10">
                                <select name="location_id" id="location_id">	
                                        <?php echo $locations; ?>
                                </select>
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="status" class="span2">Status *</label>
                            <div class="span10">
                                <select name="status" id="status">
                                    <option value="Active" <?php echo set_select('status', 'Active', TRUE); ?>>Active</option>
                                    <option value="Inactive" <?php echo set_select('status', 'Inactive'); ?>>Inactive</option>
                                </select>
                            </div>
                        </div>
						 <div class="field row-fluid">
                            <label for="type" class="span2">Type *</label>
                            <div class="span10">
                                <select name="type" id="type">
                                    <option value="private" <?php echo set_select('status', 'private', TRUE); ?>>Private</option>
                                    <option value="public" <?php echo set_select('status', 'public'); ?>>Public</option>
                                </select>
                            </div>
                        </div>
						 <div class="field row-fluid">
                            <label for="public_alias" class="span2">Public Alias *<small>(required if type is public)</small></label>
                            <div class="span10">
                                <input type="text" name="public_alias" id="public_alias" value="<?php echo set_value('public_alias'); ?>" />
                            </div>
                        </div>
						
						
                        <div class="actions row-fluid">
                            <div class="span2"></div>
                            <div class="span10">
                                <input type="submit" value="Save" class="btn btn-WI" name="save" />&nbsp;
                                <input type="reset" value="Reset" class="btn btn-WI" />&nbsp;
                                <input type="button" value="Cancel" class="btn btn-WI" onclick="window.location='/adminControl/brands'" />
                            </div>
                        </div>
                        <input type="hidden" name="x" class="c" />
                        <input type="hidden" name="y" class="c" />
                        <input type="hidden" name="x2" class="c" />
                        <input type="hidden" name="y2" class="c" />
                        <input type="hidden" name="w" class="c" />
                        <input type="hidden" name="h" class="c" />
                    </form>

                </div>
            </div>

        </div>

        <script type="text/javascript" src="assets/backend/lib/jquery.js"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript" src="/assets/frontend/js/jquery.Jcrop.min.js"></script>
        <script src="//cdn.ckeditor.com/4.4.5/basic/ckeditor.js"></script>
        <script src="//cdn.ckeditor.com/4.4.5/full-all/adapters/jquery.js"></script>
        
        <script type="text/javascript">
            jQuery(document).ready(function(){
                
                var BrandForm = jQuery('#brand-create-form');
                var BannerField = jQuery('input#banner');
                var LogoField = jQuery('input#logo');
                var ThumbnailField = jQuery('input#thumbnail');
                var CropBannerDialog = jQuery('<div id="crop-banner-dialog" />');
                var PreviewBanner = jQuery('#preview-banner');
                var Alert = jQuery('<div class="alert-dialog" />');

                Alert.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Warning!',
                    modal: true,
                    draggable: false,
                    buttons: {
                        'Ok' : function(){
                            Alert.html('').dialog('close');
                        }
                    }
                });
                CropBannerDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Crop Banner',
                    modal: true,
                    draggable: false,
                    width: 800
                });
                
                PreviewBanner.css({'cursor':'pointer'});
                PreviewBanner.on('click',function(e){
                
                    var src = PreviewBanner.attr('src');
                    var image = jQuery('<img src="'+src+'" />');
                    var coor;
                    
                    image.on('load',function(){
                        CropBannerDialog.html(image).dialog('open');
                        var imgTmp = new Image();
                        imgTmp.src = src;
                        image.Jcrop({
                            aspectRatio: 720 / 300,
                            trueSize: [imgTmp.naturalWidth,imgTmp.naturalHeight],
                            onSelect: function(c){
                                coor = c;
                                jQuery('[name=x]').val(c.x);
                                jQuery('[name=y]').val(c.y);
                                jQuery('[name=x2]').val(c.x2);
                                jQuery('[name=y2]').val(c.y2);
                                jQuery('[name=w]').val(c.w);
                                jQuery('[name=h]').val(c.h);
                            }
                        });
                        CropBannerDialog.dialog('option','buttons',[{
                            text : 'Crop',
                            click : function() {
                                
                                if( typeof coor == 'undefined' ) {
                                    Alert.html('Select an area of the image to crop').dialog('open');
                                    return;
                                }
                                
                                var canvas = document.createElement('canvas');
                                var context = canvas.getContext('2d');
                                var type = 'image/jpeg',
                                    quality = 0.92;
                                canvas.width = coor.w;
                                canvas.height = coor.h;
                                
                                context.drawImage(imgTmp,
                                                 coor.x,
                                                 coor.y,
                                                 coor.w,
                                                 coor.h,
                                                 0,
                                                 0,
                                                 coor.w,
                                                 coor.h);
                                
                                PreviewBanner.attr('src',canvas.toDataURL( type, quality ));
                                
                                CropBannerDialog.dialog('close');
                            }
                        },{
                            text : 'Cancel',
                            click : function() {
                                /*jQuery('.c').val('');
                                PreviewBanner.attr('src',src);*/
                                CropBannerDialog.dialog('close');
                            }
                        }]);
                    });
                });
                
                BannerField.on('change',function(e){
                    var file = e.target.files[0];
                    var src = window.URL.createObjectURL(file);
                    PreviewBanner.attr('src',src);
                });
                LogoField.on('change',function(e){
                    var file = e.target.files[0];
                    var src = window.URL.createObjectURL(file);
                    jQuery('#preview-logo').attr('src',src);
                });
                ThumbnailField.on('change',function(e){
                    var file = e.target.files[0];
                    var src = window.URL.createObjectURL(file);
                    jQuery('#preview-thumbnail').attr('src',src);
                });
                
                BrandForm.on('reset', function(e){
                    PreviewBanner.attr('src','');
                });

                
                /*jQuery('.wysiwyg.simple').ckeditor({
                    toolbar: [ { name: 'basicstyles', items: [ 'Bold', 'Italic' ] } ],
                    height: '100'
                });*/
				jQuery('.wysiwyg.simple').ckeditor({
                   	toolbarGroups: [						
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
						{ name: 'links' }
					],
                    height: '100'
                });
            });
        </script>
    </body>
</html>
