<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="glomp">
    <meta name="author" content="Young Mids Creation">
 	<base href="<?php echo base_url(); ?>" />
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/backend/lib/online-jquery.js"></script>
	<link href="assets/backend/lib/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <script type="text/javascript" src="assets/backend/lib/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
	 <script language="javascript" type="text/javascript">
   $(document).ready(function(e) {
    $('#change_password').click(function(e) {
       
	   if($('#change_password').is(':checked'))
	   {
	     $('#new_pword').removeAttr('disabled');
		 $('#retype_new_pword').removeAttr('disabled');
	   }else
	   {
		   	     $('#new_pword').attr('disabled','disabled');
				 $('#retype_new_pword').attr('disabled','disabled');

		   }
    });
});
   
   </script>
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   <!--<![endif]-->
   <!--<![endif]-->
  
  <body class=""> 
  
    
    <?php include("includes/header.php"); ?>


<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
			<?php //include("includes/right-sidebar.php");?> 
            <div class="span12">
     
                  <?php if($page_action=="edit") { 
				  ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span12">
                  <div class="page-heading">My Account</div>
                  <?php
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  </div>
                  </div>
                  <div class="content-box">
                  <div class="row-fluid">
                  <div class="span12">
                  <?php echo form_open_multipart(ADMIN_FOLDER."/myaccount/",'name="frmCms" id="myaccount"')?>
                  <table style="border:none;" width="100%">
                  <tr>
                  	<td width="50%" valign="top"><table class="table table-condensed table-borderless" style="border:none;">
                  <tr>
                  	<td width="50%">Name</td>
                    <td><input type="text" name="admin_name" value="<?php echo $row->admin_name;?>" required></td>
                  </tr>
                   <tr>
                  	<td width="20%">Email</td>
                    <td><input type="text" name="admin_email" readonly value="<?php echo $row->admin_email;?>" required></td>
                  </tr>
                  <tr>
                 	 <td>Current Password</td>
                     <td><input type="password" name="old_pword" value="" required></td>
                  </tr>
                  <tr>
                    <td>Chage Password?</td>
                    <td><input type="checkbox" value="1" name="change_password" id="change_password" /></td>
                  </tr>
                  <tr>
              	    <td>New Password</td>
                    <td><input type="password" disabled id="new_pword" name="new_pword" value="" required></td>
                  </tr>
                  <tr>
              	    <td>Retype New Password</td>
                    <td><input type="password" disabled id="retype_new_pword" name="retype_new_pword" value="" required></td>
                  </tr>
                  <tr>
                  <td>&nbsp;</td>
                  <td> <input type="submit" name="UpdatePword" value="Update" class="btn btn-WI">
                   <input type="reset" name="reset" value="Reset" class="btn"></td>
                  </tr>                   
                  </table></td>
                    <td valign="top"><h2>Other Info</h2>
                    	<p class="text-left">Status: <?php echo $row->admin_status;?></p>
                        <p class="text-left">UserType: <?php echo $row->admin_type;?></p>
                        <p class="text-left">Account Created Date: <?php echo $row->admin_added;?></p>
                        <p class="text-left">Last Login: <?php echo $row->admin_last_login;?></p>
                        <p class="text-left">Last Login IP: <?php echo $row->admin_last_ip;?></p>
                        <p class="text-left">Currnt IP: <?php echo $_SERVER['REMOTE_ADDR'];;?></p>
                    </td>
                  </tr>
                  </table>
	                  
				  <?php echo form_close(); ?>
                  </div>
                  </div>
                  </div>
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


