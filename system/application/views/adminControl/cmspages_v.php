<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Youth Information System">
    <meta name="author" content="Young Mids Creation">
 	<base href="<?php echo base_url(); ?>" />
   <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
  </head>
  <body class=""> 
    
  <?php include("includes/header.php"); ?>


<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
			<?php //include("includes/right-sidebar.php");?> 
            
            
             
            <div class="span12">
     
                  <?php if($page_action=="view") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span8">
                  <div class="page-heading">Manage Pages</div>
                  </div>
                  <div class="span4" style="text-align:right;">
				  <?php //echo anchor(ADMIN_FOLDER."/cmspages/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  <?php echo anchor(ADMIN_FOLDER."/cmspages/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  </div>
                  </div>
                  </div>
                  <div class="page-subtitle"><?php
				  echo "View All";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <table class="table table-condensed table-hover">
                  <tr>
                  <td><strong>SN.</strong></td>
                  <td><strong>Date</strong></td>
                  <td><strong>Page Name</strong></td>
                  <td><strong>Page Type</strong></td>
                  <td style="text-align:center"><strong>Display Order</strong></td>
                  <td style="text-align:center;"><strong>Status</strong></td>
                  <td style="text-align:center;"><strong>Options</strong></td>
                  </tr>
                  <?php if($res_cmspages) { 
				  $i=1;
				  echo form_open(ADMIN_FOLDER."/cmspages/UpdateDisplayOrder",'name="frmCms" id="cmspages"');
				  foreach($res_cmspages as $rec_cmspages) {
				  ?>
                  <tr>
                  <td><?php echo $i; $i++; ?></td>
                  <td><?php echo $rec_cmspages->cms_added; ?></td>
                  <td><?php echo $rec_cmspages->content_name; ?></td>
                  <td><?php echo $this->cmspages_m->cmsMenuNameByID($rec_cmspages->cms_menu_id); ?></td>
                  <td style="text-align:center">
                  <input style="text-align:center;" type="text" class="input-mini" name="disp_order[]" value="<?php echo $rec_cmspages->cms_display_order; ?>">
                  <input name="cms_id[]" type="hidden" id="cms_id[]" value="<?php echo $rec_cmspages->cms_id; ?>" />
                  </td>
                  <td style="text-align:center;">
                  <?php if($rec_cmspages->cms_active=='Y'){?>
				  <?php echo anchor(ADMIN_FOLDER."/cmspages/publish/".$rec_cmspages->cms_id."/N/", '<img src="custom/images/icons/active.png" alt="Active" border="0" title="Active" />');?>					
                  <?php }else{?>
                  <?php echo anchor(ADMIN_FOLDER."/cmspages/publish/".$rec_cmspages->cms_id."/Y/", '<img src="custom/images/icons/inactive.png" alt="Draft" border="0" title="Inactive" />');?>
                  <?php }?>
                  </td>
                  <td style="text-align:center;">
                   <?php
					echo anchor(ADMIN_FOLDER."/cmspages/editPage/".$rec_cmspages->cms_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
					if($rec_cmspages->cms_delete_allow == 'Y'){
					echo "&nbsp;&nbsp;&nbsp;";
					echo anchor(ADMIN_FOLDER."/cmspages/deletePage/".$rec_cmspages->cms_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");
					}
					?>
                  </td>
                  </tr>
                  <?php }
				  echo '<tr><td colspan="7">
				  <input class="btn btn-WI" style="height:40px;" type="submit" name="updateDispOrder" value="Update Display Order" >
				  </td></tr>';
				  echo form_close();
				  echo '<tr><td colspan="7">'.$links.'</td></tr>';
				  } else echo'<tr><td colspan="7">No records found.</td></tr>'; ?>
                  </table>
                  <?php } elseif($page_action=="add") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span8">
                  <div class="page-heading">Manage Pages</div>
                  </div>
                  <div class="span4" style="text-align:right;">
				  <?php echo anchor(ADMIN_FOLDER."/cmspages/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  <?php echo anchor(ADMIN_FOLDER."/cmspages/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  </div>
                  </div>
                  </div>
                  <div class="page-subtitle"><?php
				  echo "Add New Page";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <?php echo form_open(ADMIN_FOLDER."/cmspages/addPage",'name="frmCms" id="cmspages"')?>
                  <div class="content-box">
                  * Page Type(Menu):<br>
                  <select name="menu_id" required>
                  <?php echo $this->cmspages_m->cmsMenu(set_value('menu_id')); ?>
                  </select><br>
                  * Page Name:<br>
                  <input type="text" name="page_name" required value="<?php echo set_value('page_name'); ?>"><br>
                  * Page Title:<br>
                  <input type="text" name="page_title" required value="<?php echo set_value('page_title'); ?>"><br>
                  Keyword:<br>
                  <textarea name="keyword" style="width:400px; height:70px;"><?php echo set_value('keyword'); ?></textarea><br>
                  Description:<br>
                  <textarea name="description" style="width:400px; height:70px;"><?php echo set_value('description'); ?></textarea><br>
                  Contents:<br>
                  <!--ckeditor starts-->
				  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
                  <script type="text/javascript">
                   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
                  </script>
                  <textarea name="contents" style="width:600px; height:200px;" class="ckeditor">
				  <?php echo set_value('contents'); ?></textarea>
                  <!--ckeditor ends-->
                  <br>
                  <input class="btn btn-WI" style="height:40px; width:100px;" type="submit" name="addPage" value="Add" >
                  <button class="btn" style="height:40px; width:100px;" type="button" name="addPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } elseif($page_action=="edit") { ?>
                  <div class="page-header">
                  <div class="row-fluid">
                  <div class="span8">
                  <div class="page-heading">Manage Pages</div>
                  </div>
                  <div class="span4" style="text-align:right;">
				  <?php echo anchor(ADMIN_FOLDER."/cmspages/addPage", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  <?php echo anchor(ADMIN_FOLDER."/cmspages/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  </div>
                  </div>
                  </div>
                  <div class="page-subtitle"><?php
				  echo "Edit Page";
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <?php echo form_open(ADMIN_FOLDER."/cmspages/editPage/".$res_cmspages->cms_id."/".$res_cmspages->lang_id,'name="frmCms" id="cmspages"')?>
                  <div class="content-box">
                  <script type="text/javascript">
					function lang_change(id,cms_id)
					{
						window.location.href="<?php echo base_url(ADMIN_FOLDER."/cmspages/editPage"); ?>/"+cms_id+"/"+id;
					}
					</script>
                  * Language:<br>
                  <select name="lang_id" required onchange="lang_change(this.value,<?php echo $res_cmspages->cms_id; ?>)">
                  <?php echo $this->cmspages_m->cmsLanguage($res_cmspages->lang_id); ?>
                  </select><br>
                  * Page Type(Menu):<br>
                  <select name="menu_id" required>
                  <?php echo $this->cmspages_m->cmsMenu($res_cmspages->cms_menu_id); ?>
                  </select><br>
                  * Page Name:<br>
                  <input type="text" name="page_name" required value="<?php echo $res_cmspages->content_name; ?>"><br>
                  * Page Title:<br>
                  <input type="text" name="page_title" required value="<?php echo $res_cmspages->content_title; ?>"><br>
                  Keyword:<br>
                  <textarea name="keyword" style="width:400px; height:70px;"><?php echo $res_cmspages->content_keywords; ?></textarea><br>
                  Description:<br>
                  <textarea name="description" style="width:400px; height:70px;"><?php echo $res_cmspages->content_desc; ?></textarea><br>
                  Contents:<br>
                   <!--ckeditor starts-->
				  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
                  <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
                  <script type="text/javascript">
                   CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
                  </script>
                  <textarea name="contents" style="width:600px; height:200px;" class="ckeditor"><?php echo $res_cmspages->content_content; ?></textarea>
                  <!--ckeditor ends--><br>
                  <input class="btn btn-WI" style="height:40px; width:100px;" type="submit" name="editPage" value="Save" >
                  <button class="btn" style="height:40px; width:100px;" type="button" name="editPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


