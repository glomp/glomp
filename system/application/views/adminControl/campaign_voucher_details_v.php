<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e) {
		
        $('#add_more_tr').click(function(e) {
           $('#camp_voucher_table tr:last').after($('#master_tr').clone());
		  $( '#camp_voucher_table tr td:last' ).removeClass('hidden_td');
        });
		
		$("#camp_voucher_table").on('click','.remove_tr',function(){
      		  $(this).parent().parent().remove();
        });
		/*for editing*/
		 $('#add_more_tr_edit').click(function(e) {
			 var select_html = $('#product_id_0').html();
			 var tr_html = '<tr id="master_tr"><td><select name="new_product_id[]"'+select_html+'</select></td><td><input type="text" value="" name="new_voucher_qty[]"></td><td>&nbsp;</td><td><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';
        
		  $('#camp_voucher_table tr:last').after(tr_html);
		});
		$("#camp_voucher_table").on('click','.delete_tr',function(){
			if(!confirm('Are you sure you want to delete this record?'))
				{return false;}
      		$(this).parent().parent().remove();
			var id  = $(this).attr('id');
			 
			var base_url = '<?php echo base_url(ADMIN_FOLDER.'/campaign/remove_cam_details');?>';
			$.ajax({
			type:'GET',
			url:base_url+"/"+id,
			success:function(html){
					
				}
			});
        });
    });
    </script>
  <style type="text/css">
  .hidden_td{
	  visibility:hidden;
	  }
  </style>
  </head>
  <body class="">
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid dashboard">
    	<div class="row-fluid">
            <div class="span12">
            <div class="page-header">
            	<div class="row-fluid">
                	<div class="span8">
                  		<div class="page-heading"><?php echo $page_title;?></div>
                  	</div>
                  	<div class="span4" style="text-align:right;">
						<?php echo anchor(ADMIN_FOLDER."/campaign/AddCampaign", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
						<?php echo anchor(ADMIN_FOLDER."/campaign", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  	</div>
                  </div>
             </div>
             
                <div class="page-subtitle">
                    <?php
                      //  echo $page_subtitle;
                        if(isset($msg))
                        {
                             echo "<div class=\"success_msg\">".$msg."</div>";
                        }
                        if(isset($error_msg))
                        {
                             echo "<div class=\"alert alert-error\">".$error_msg."</div>";
                        }
                        if(validation_errors()!="")
                        {
                            echo "<div class=\"custom-error\">".validation_errors()."</div>";
                        }
						
						if($this->session->flashdata('campaign_run_message'))
                        {
							 echo  $this->session->flashdata('campaign_run_message');
                        }
                    ?>
                    
                 </div>
            <?php if($page_action=="view") { ?>
            
            <div class="row-fluid">
                                    <div class="span12 account">
                                     Name:<span class="label label-success"><?php echo $rec_camp_->campaign_name;?> </span> Type:<span class="label label-success"> <?php echo $rec_camp_->campaign_type;?></span> Expiry Day :<span class="label label-success"> <?php echo $rec_camp_->campaign_voucher_expiry_day;?> </span>  Last Execution Date:<span class="label label-success"><?php echo ($rec_camp_->campaign_last_executed_date=='')?'Never':$rec_camp_->campaign_last_executed_date;?></span>
                                     <?php
	$res_available = $this->campaign_m->campaign_voucher($camp_id, "AND camp_voucher_assign_status = 'UnAssigned'");
	$available_voucher = $res_available->num_rows();
									 ?>
      Available Voucher: <span class="label label-success"><?php echo $available_voucher;?> </span>
                                   
                                    
               					  </div>
                			 </div>
                             
            <table width="100%" class="table table-condensed table-hover">
            	<tr>
                	<td><strong>SN.</strong></td>
                  	<td><strong>Voucher Assigned Date</strong></td>
                  	<td><strong>Merchant</strong></td>
                  	<td><strong>Product</strong></td>
                  	<td><strong>Vocuher Type</strong></td>
                  	<td><strong>Asigned</strong></td>
               	</tr>
				<?php 
					if(isset($res_camp)) { 
				  	$i=1;
				  	
					foreach($res_camp->result() as $rec_camp) {
						if($recently_distributed_voucher >= $i)
							$recent_css = 'class="info"';
						else
							$recent_css = '';
				  	
				  ?>
                <tr  <?php echo $recent_css;?>>
                	<td><?php echo $i;?></td>
                    <td><?php echo $rec_camp->camp_voucher_assigned_date;//echo $this->custom_func->time_stamp($rec_camp->camp_voucher_assigned_date); ?></td>
                    <td>
                   <?php  echo $this->merchant_m->merchantName($rec_camp->prod_merchant_id);?>
                    </td>
                    <td><?php echo $rec_camp->prod_name; ?></td>
                    <td><?php echo $rec_camp->camp_voucher_type; ?></td>
                    <td><?php
					if($rec_camp->camp_voucher_assign_status=="Assigned"){?>  
					<?php if($rec_camp->camp_winner_user_id)
					{
						 echo anchor(ADMIN_FOLDER."/user/viewDetails/".$rec_camp->camp_winner_user_id."/","<em>".$this->users_m->userName($rec_camp->camp_winner_user_id)."</em>","target='_blank'");
						 }
					}
					else  if($rec_camp->camp_voucher_assign_status=="UnAssigned")
					{
						echo '--';
					}
					else
					{
					 echo $rec_camp->camp_voucher_assign_status;
					}
					?> </td>
               	  </tr>
                  <?php 
				   $i++; 
				  }
				  } 
				  else 
				  	echo'<tr><td colspan="7">No records found.</td></tr>'; ?>
                  </table>
                  <?php }  ?>
                  
    </div>
</div>
     	<?php include("includes/footer.php");?>
   		<script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


