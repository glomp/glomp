<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit Brand - Glomp! Admin</title>
        <base href="<?php echo base_url(); ?>" />
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">

        <link href="assets/backend/lib/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    </head>
    <body>
        <?php include("includes/header.php"); ?>

        <div class="content">
            <?php include("includes/main-menu.php");?>
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span2">
                            <div class="page-heading">Manage Brands</div>
                        </div>
                        <div class="span10">
                            <div style="float:right">
                                <input type="button" class="btn btn-WI" value="Add New" onclick="window.location='/adminControl/brands/create'" />
                            </div>
                            <div style="float:right;margin-right:10px;">
                                <form id="filter" action="/adminControl/brands/index">
                                    <input type="text" name="key" placeholder="Search..." style="margin:0;" value="<?php echo $this->input->get('key')?>" id="key" />
                                    &nbsp;
                                    <input type="submit" value="Search" class="btn btn-WI" />
                                    <?php if($this->input->get('key')):?>
                                    &nbsp;
                                    <input type="button" value="Clear" class="btn" onclick="document.getElementById('key').value='';document.getElementById('filter').submit()" />
                                    <?php endif;?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-subtitle">Delete Brand</div>
                <div class="content-box"><?php echo $confirm?></div>
            </div>

        </div>

        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/backend/lib/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script></script>
        <script type="text/javascript">
            jQuery(document).on('click','#no',function(e){e.preventDefault();history.go(-1);});
        </script>
    </body>
</html>
