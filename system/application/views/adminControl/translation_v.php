<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <script src="assets/backend/lib/bootstrap/js/bootstrap.js"></script> 
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assests/backend/html5.js"></script>
    <![endif]-->
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
  <?php include("includes/header.php"); ?>


<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
    	<h2><?php echo $page_title;?></h2>
    	<?php
			if($this->session->flashdata('success_message'))
			{
		?>
            	  <div class="alert alert-success">
                       <a class="close" data-dismiss="alert">&times;</a>
                       <strong><?php echo $this->session->flashdata('success_message'); ?></strong>
                   </div>
                <?php } ?>
                
            <?php if(validation_errors()){?>
				<div class="alert alert-error">
                	<a class="close" data-dismiss="alert">&times;</a>
                    <strong><?php echo validation_errors();?></strong>
                </div>
			<?php }?>
            
        <div class="row-fluid">
        	<div class="span12">
            	<div class="well">
                                            <!--box-->
                                            <div class="box corner-all">
                                                <!--box header-->
                                                <div class="box-header grd-white color-silver-dark corner-top">
                                                    <h3><i class="icon-edit"></i> <?php echo $this->session->userdata('translate_lang')?$this->session->userdata('translate_lang'):'';?> Language Translation</h3>
                                                    <div class="pull-right"><div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-inverse dropdown-toggle">Choose language <span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url('index.php/'.ADMIN_FOLDER.'/translation/index/en');?>">English</a></li>
                  <li><a href="<?php echo base_url('index.php/'.ADMIN_FOLDER.'/translation/index/cn');?>">Chinese</a></li>
                </ul>
              </div></div>
                                                </div><!--/box header-->
                                                <!--box body-->
                                                <div class="box-body">
                                                    <form  action="<?php echo base_url('index.php/'.ADMIN_FOLDER."/translation/save/");?>" method="post">  
   					 <?php
					 	 foreach($key as $k){
						 if($k!='')	{
					  ?>
                         
                      <div class="control-group">
                        <label class="control-label" for="<?php echo $k;?>"><b><?php echo $k;?></b></label>
                        <div class="controls">
                       <?php if(strlen(stripslashes($this->lang->line($k)))>=30){?>
                       <textarea name="<?php echo $k;?>" id="<?php echo $k;?>" class="span5" rows="5"><?php echo stripslashes($this->lang->line($k));?></textarea>
<?php } else{ ?>
                          <input type="text" class="input-xxlarge" id="<?php echo $k;?>" name="<?php echo $k;?>" class="input-block" value="<?php echo stripslashes($this->lang->line($k));?>" />
                          
                          <?php } ?>
                        </div>
                      </div>
                      <?php }  }?>
                      <div class="control-group">
                      <div class="controls">
                      <input name="lang" type="hidden" value="<?php echo $this->session->userdata('translate_lang');?>" />
                      <input name="saveTranslation" type="submit" class="btn btn-large" value="Save Translation" />
                      <input name="Cancel2" type="button" class="btn btn-large btn-inverse" id="Cancel2" value="Cancel" onclick="javascript:history.go(-1)" />
                        </div>
                      </div>
                   </form>
                                                </div><!--/box body-->
                                            </div><!--/box-->
                                        </div>
            </div>
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   
  </body>
</html>


