<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="glomp">
    <meta name="author" content="Young Mids Creation">
 	<base href="<?php echo base_url(); ?>" />
   
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/backend/lib/online-jquery.js"></script>
	<link href="assets/backend/lib/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <script type="text/javascript" src="assets/backend/lib/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
	
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   <!--<![endif]-->
  <body> 
 
  <?php include("includes/header.php"); ?>


<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
            	<div class="widget">
                	<div class="widget-header">
                  		<div class="title">
                    		<i class="icon icon-home"></i> Dashboard
                  		</div>
                	</div>
                	<div class="widget-body">
                          <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('categories');?>">
                            <i class="icon icon-suitcase icon-4x"></i>
                            <p class="no-margin">Categories</p>
                          </a>
                         <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('cmspages');?>">
                            <i class="icon icon-file-text icon-4x"></i>
                            <p class="no-margin">Cms Pages</p>
                          </a>
                          <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('regions');?>">
                            <i class="icon icon-flag-alt icon-4x"></i> 
                            <p class="no-margin">Regions</p>
                          </a>
                         
                          <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('merchant');?>">
                            <i class="icon icon-maxcdn icon-4x"></i> 
                            <p class="no-margin">Merchant</p>
                          </a>
                  		<div class="clearfix"></div>
                	</div>
                    <div class="widget-body">
                          <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('user');?>">
                            <i class="icon icon-user icon-4x"></i> 
                            <p class="no-margin">Manage Users</p>
                          </a>
                           <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('admin');?>">
                            <i class="icon icon-user icon-4x"></i> 
                            <p class="no-margin">Manage Admins</p>
                          </a>
                          <a class="quick-action-btn span2 input-bottom-margin" href="<?php echo admin_url('translation');?>">
                            <i class="icon icon-globe icon-4x"></i> 
                            <p class="no-margin">Translation</p>
                          </a>
                  		<div class="clearfix"></div>
                	</div>
              	</div>
        	</div>
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


