<link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/preview.css">

<?php echo $template; ?>

<script src="<?php echo base_url() ?>assets/backend/lib/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
<link href="<?php echo base_url() ?>assets/frontend/css/south-street/jquery-ui-1.10.3.custom.medium.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
<script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
<script type="text/javascript">
    CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
</script>

<div id="dialog">
    <textarea name="body" style="width:600px; height:200px;" id="body"></textarea>
    <input type ="hidden" name="attr_name" id="attr_name" />
    <br />
    <?php echo anchor("#", "Save", "id='save_prev_btn' class='btn btn-large ui-state-default ui-corner-all'") ?>
</div>


<div id ="upload_dialog" >
    <form id="upload_dialog_form" method="POST" enctype='multipart/form-data' action="">
        <input type ="hidden" name="attr_name" id="attr_name_img" />
        <input type ="hidden" name="id" value ="<?php echo $newsletter['campaign_newsletter_id']; ?>" />
        <input name="upload_image" type="file" id="upload_image" style="position:absolute;top: -1000px;"/>
    </form>
</div>

<div id ="loader" align="center">Loading...Please wait</div>

<script>
    var get_url = '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/get_data/'); ?>'
    var save_url = '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/set_data/'); ?>'
    var id = <?php echo $newsletter['campaign_newsletter_id']; ?>;

    $('#body').ckeditor();
    $('.editable').prepend('<div class="pencil"></div>');
    $('.uploadable').prepend('<div class="image_upload"></div>');

    $('#dialog').dialog({
        height: 500,
        width: 918,
        autoOpen: false
    });
    
    $("#loader").dialog({
      modal: true,
      dialogClass: 'noTitleStuff', 
      autoOpen: false
    });

    $('.pencil').click(function() {
        var name = $(this).parent().attr('attr_name');
        $('#attr_name').val(name);

        $.ajax({
            type: "post",
            url: get_url,
            dataType: 'json',
            data: {
                id: id,
                attr_name: name
            },
            success: function(res) {
                $('#body').val(res.value);
                $('#dialog').dialog('open');
            }
        });
    });
    
    $("#upload_image").change(function () {
        var form = $('#upload_dialog_form');
        $(this).val();
        $('#loader').dialog('open');
        $(form).ajaxSubmit({
            type: "post",
            url: save_url,
            dataType: 'json',
            data: $(form).serialize(),
            success: function(response) {
                location.reload();
            }
        });
    });

    $('.image_upload').click(function() {
        var name = $(this).parent().attr('attr_name');
        $('#attr_name_img').val(name);
        $('#upload_image').trigger('click');
    });

    $('#save_prev_btn').click(function(e) {
        e.preventDefault();
        var name = $('#attr_name').val();
        $('#loader').dialog('open');
        $.ajax({
            type: "post",
            url: save_url,
            dataType: 'json',
            data: {
                id: id,
                attr_name: name,
                value: $('#body').val(),
            },
            success: function(res) {
                location.reload();
            }
        });
    });

</script>