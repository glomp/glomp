<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Minds Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <script src="assets/backend/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/backend/lib/loader.js" type="text/javascript"></script>
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <style type="text/css">
            .hidden_td{
                visibility:hidden;
            }
        </style>
    </head>
    <body class="">
        <?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/header.php"); ?>
        <div class="content">
            <?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_newsletter/add", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_newsletter", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>

                        <div class="page-subtitle">
                            <?php
                            //  echo $page_subtitle;
                            if (isset($msg)) {
                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                            }
                            if (isset($error_msg)) {
                                echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                            } else {
                                $error_msg = $this->session->flashdata('error_msg');
                                if (!empty($error_msg)) {
                                    echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                                }
                            }
                            if (validation_errors() != "") {
                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                            }
                            ?>
                        </div>
                        <?php echo form_open(ADMIN_FOLDER . "/campaign_newsletter/edit/".$campaign_newsletter['campaign_newsletter_id'], 'name="frmCms" id="cmspages" onSubmit="return make_all_seletect()"') ?>
                        <div class="content-box">
                            <div class="row-fluid">
                                <div class="rown-fluid">
                                    <div class="span6">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td><span>*</span>Campaign Newsletter  Name</td>
                                                <td><span>*</span>Newsletter Template</td>
                                                <td></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="campaign_newsletter_name" value="<?php echo $newsletter_name; ?>"></td>
                                                <td>
                                                    <select name="campaign_newsletter_template" id="campaign_newsletter_template">
                                                        <option value="">Select</option>
                                                        <?php
                                                        foreach ($newsletter_templates as $nt) {
                                                            $sel = ($newsletter_template == $nt['newsletter_id']) ? 'selected' : '';
                                                            ?>
                                                            <option <?php echo $sel; ?> value="<?php echo $nt['newsletter_id']; ?>"><?php echo $nt['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td> </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="page-header">
                                                    <div class="row-fluid">
                                                    </div>
                                                </div>
                                                <div class="page-subtitle">
                                                    Please click onto the pencil icon to edit content
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="content-box">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                <div class="control-group" id ="template_holder">
                                                                    <iframe id="_preview" width="100%" height="500px" src="<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/preview_template/' . $campaign_newsletter['campaign_newsletter_id']) ?>"></iframe>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Rules <a href="javascript:void(0)" id="toogle_campaign_rule" >Show/Hide</a></h5></div>
                            <div id="cmapign_rule_container">
                                <div class="widget-content">
                                    <div class="row-fluid">
                                        <span class="btn-WI btn ui-state-default ui-corner-all" data-campaign_id="28" id="disable_rule"><i class="icon-ban-circle"></i> Disable Rule</span>
                                        <span class="btn-WI btn ui-state-default ui-corner-all disabled" data-campaign_id="28" id="enable_rule"><i class="icon-check-sign"></i> Enable Rule</span>
                                        <input type ="hidden" id ="disable_rule_val" val ="0"/>
                                        <div class="span12 account">

                                            <?php
                                            
                                            if (isset($_POST['update_campaign_newsletter'])) {
                                                $min_age_checked = isset($_POST['min_age_checked']) ? 'checked' : '';
                                                $max_age_checked = isset($_POST['max_age_checked']) ? 'checked' : '';
                                                $regions_checked = isset($_POST['regions_checked']) ? 'checked' : '';
                                                $loggedover_checked = isset($_POST['loggedover_checked']) ? 'checked' : '';
                                                $noactivity_checked = isset($_POST['noactivity_checked']) ? 'checked' : '';
                                                $previously_rewarded_checked = isset($_POST['previously_rewarded_checked']) ? 'checked' : '';
                                                $notglomp_checked = isset($_POST['notglomp_checked']) ? 'checked' : '';
                                                $glomped_checked = isset($_POST['glomped_checked']) ? 'checked' : '';
                                                $lessfriends_checked = isset($_POST['lessfriends_checked']) ? 'checked' : '';
                                                $member_fb_notconnected = isset($_POST['member_fb_notconnected']) ? 'checked' : '';
                                                //Mandatory or not
                                                $loggedover_set_checked1 = (isset($_POST['loggedover_set']) && $_POST['loggedover_set'] == 'no') ? 'checked' : '';
                                                $loggedover_set_checked2 = (isset($_POST['loggedover_set']) && $_POST['loggedover_set'] == 'yes') ? 'checked' : '';
                                                $noactivity_set_checked1 = (isset($_POST['noactivity_set']) && $_POST['noactivity_set'] == 'no') ? 'checked' : '';
                                                $noactivity_set_checked2 = (isset($_POST['noactivity_set']) && $_POST['noactivity_set'] == 'yes') ? 'checked' : '';
                                                $previously_rewarded_set_checked1 = (isset($_POST['previously_rewarded_set']) && $_POST['previously_rewarded_set'] == 'no') ? 'checked' : '';
                                                $previously_rewarded_set_checked2 = (isset($_POST['previously_rewarded_set']) && $_POST['previously_rewarded_set'] == 'yes') ? 'checked' : '';
                                                $notglomp_set_checked1 = (isset($_POST['notglomp_set']) && $_POST['notglomp_set'] == 'no') ? 'checked' : '';
                                                $notglomp_set_checked2 = (isset($_POST['notglomp_set']) && $_POST['notglomp_set'] == 'yes') ? 'checked' : '';
                                                $glomped_set_checked1 = (isset($_POST['glomped_set']) && $_POST['glomped_set'] == 'no') ? 'checked' : '';
                                                $glomped_set_checked2 = (isset($_POST['glomped_set']) && $_POST['glomped_set'] == 'yes') ? 'checked' : '';
                                                $lessfriends_set_checked1 = (isset($_POST['lessfriends_set']) && $_POST['lessfriends_set'] == 'no') ? 'checked' : '';
                                                $lessfriends_set_checked2 = (isset($_POST['lessfriends_set']) && $_POST['lessfriends_set'] == 'yes') ? 'checked' : '';
                                                $member_fb_notconnected_set_checked1 = (isset($_POST['member_fb_notconnected_set']) && $_POST['member_fb_notconnected_set'] == 'no') ? 'checked' : '';
                                                $member_fb_notconnected_set_checked2 = (isset($_POST['member_fb_notconnected_set']) && $_POST['member_fb_notconnected_set'] == 'yes') ? 'checked' : '';
                                                
                                            } else {
                                                //Get all rule that is checked or not
                                                $min_age_checked = ($this->campaign_newsletter_rule_m->get_ischecked('min_age_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $max_age_checked = ($this->campaign_newsletter_rule_m->get_ischecked('max_age_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $regions_checked = ($this->campaign_newsletter_rule_m->get_ischecked('regions_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $loggedover_checked = ($this->campaign_newsletter_rule_m->get_ischecked('loggedover_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $noactivity_checked = ($this->campaign_newsletter_rule_m->get_ischecked('noactivity_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $previously_rewarded_checked = ($this->campaign_newsletter_rule_m->get_ischecked('previously_rewarded_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $notglomp_checked = ($this->campaign_newsletter_rule_m->get_ischecked('notglomp_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $glomped_checked = ($this->campaign_newsletter_rule_m->get_ischecked('glomped_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $lessfriends_checked = ($this->campaign_newsletter_rule_m->get_ischecked('lessfriends_checked', $campaign_newsletter_id)) ? 'checked' : '';
                                                $member_fb_notconnected = ($this->campaign_newsletter_rule_m->get_ischecked('member_fb_notconnected', $campaign_newsletter_id)) ? 'checked' : '';
                                                
                                                //Manddatory or not
                                                $loggedover_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('loggedover_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $loggedover_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('loggedover_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $noactivity_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('noactivity_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $noactivity_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('noactivity_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $previously_rewarded_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('previously_rewarded_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $previously_rewarded_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('previously_rewarded_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $notglomp_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('notglomp_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $notglomp_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('notglomp_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $glomped_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('glomped_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $glomped_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('glomped_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $lessfriends_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('lessfriends_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $lessfriends_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('lessfriends_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                                $member_fb_notconnected_set_checked1 = ($this->campaign_newsletter_rule_m->get_isset('member_fb_notconnected_set', $campaign_newsletter_id, 'no')) ? 'checked' : '';
                                                $member_fb_notconnected_set_checked2 = ($this->campaign_newsletter_rule_m->get_isset('member_fb_notconnected_set', $campaign_newsletter_id, 'yes')) ? 'checked' : '';
                                            }

                                            //$rec_rule = $res_camp_rule->row();
                                            $rule = $this->campaign_newsletter_rule_m->get_record(array(
                                                'where' => array('campaign_newsletter_id' => $campaign_newsletter_id)
                                            ));
                                            
                                            ?><br/>
                                            <?php //echo form_open(ADMIN_FOLDER . '/campaign/details/' . $camp_id, 'id = "campaign_rule_form" onSubmit="return make_all_seletect()"'); ?>
                                            <div class="row-fluid" id ="campaign_rule_form" >
                                                <div class="other_rules">
                                                    <div class="span3">
                                                        <input class="check_rule" style="margin: 0px;" <?php echo $min_age_checked; ?> type="checkbox" name ="min_age_checked" value="yes"> Min Age : <input type="text" name="min_age" class="span3" value="<?php echo $rule['min_age']; ?>" /> </div>
                                                    <div class="span3">
                                                        <input class="check_rule" style="margin: 0px;" <?php echo $max_age_checked; ?> type="checkbox" name ="max_age_checked" value="yes">  Max Age: <input type="text" name="max_age" class="span3" value="<?php echo $rule['max_age']; ?>" /> </div>
                                                </div>
                                                <div class="span3">
                                                    Gender  : <select name="gender" class="span3">
                                                        <?php
                                                        $gender = $rule['gender'];
                                                        ?>
                                                        <option value="Both" <?php echo ($gender == 'Both') ? 'selected="selected"' : ''; ?>>Both</option>
                                                        <option value="Male" <?php echo ($gender == 'Male') ? 'selected="selected"' : ''; ?>>Male</option>
                                                        <option value="Female" <?php echo ($gender == 'Female') ? 'selected="selected"' : ''; ?>>Female</option>
                                                    </select>  
                                                </div>
                                                <br />
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td><input class="check_rule" style="margin: 0px;" <?php echo $regions_checked; ?> type="checkbox" name ="regions_checked" value="yes"> All Available Regions</td>
                                                        <td>Selected Regions</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="36%"> <select name="regions_id_all" id="regions_id_all" size="15" multiple class="span10">
                                                                <?php echo $this->regions_m->recursive(); ?>
                                                            </select></td>
                                                        <td width="64%">
                                                                <?php
                                                                $list = $rule['region_id'];
                                                                $res_own_region = $this->campaign_m->select_rule_region($list);
                                                                
                                                                ?>
                                                            <select name="regions_id_selected[]" id="regions_id_selected" size="15" multiple class="span5">
                                                            <?php
                                                            if ($res_own_region->num_rows() > 0) {
                                                                foreach ($res_own_region->result() as $row) {
                                                                    echo "<option value='" . $row->region_id . "'>" . $row->region_name . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                            </select></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="btn" id="button_transfer">Add Selected <i class="icon-plus"></i></span></td>
                                                        <td><span class="btn" id="remove_selected">Remove Selected <i class="icon-trash"></i></span></td>
                                                    </tr>
                                                </table>
                                                <br/>
                                                <div class="other_rules">
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" style="margin: 0px; " <?php echo $loggedover_checked; ?> type="checkbox" name ="loggedover_checked" value="yes"> Members who have been logged out for over 
                                                            <input type="text" name="member_logout_days" class="span3" value="<?php echo $rule['member_logout_days']; ?>" /> days
                                                            <input type="radio" name ="loggedover_set" <?php echo $loggedover_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="loggedover_set" <?php echo $loggedover_set_checked2; ?>  value="yes"> (Mandatory)
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $noactivity_checked; ?> name ="noactivity_checked" value="yes"> Members who have had no activity for over 
                                                            <input type="text" name="member_noactivity_days" class="span3" value="<?php echo $rule['member_noactivity_days']; ?>" /> days
                                                            <input type="radio" name ="noactivity_set" <?php echo $noactivity_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="noactivity_set" <?php echo $noactivity_set_checked2; ?>  value="yes"> (Mandatory)
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $previously_rewarded_checked; ?> name ="previously_rewarded_checked" value="yes"> Members who have been previously rewarded within 
                                                            <input type="text" name="member_prev_rewarded_days" class="span3" value="<?php echo $rule['member_prev_rewarded_days']; ?>" /> days
                                                            <input type="radio" name ="previously_rewarded_set" <?php echo $previously_rewarded_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="previously_rewarded_set" <?php echo $previously_rewarded_set_checked2; ?>  value="yes"> (Mandatory)
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $notglomp_checked; ?>  name ="notglomp_checked" value="yes"> Members who have not glomp!ed anyone for over 
                                                            <input type="text" name="member_noglomp_days" class="span3" value="<?php echo $rule['member_noglomp_days']; ?>" /> days
                                                            <input type="radio" name ="notglomp_set" <?php echo $notglomp_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="notglomp_set" <?php echo $notglomp_set_checked2; ?>  value="yes"> (Mandatory)
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span9">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $glomped_checked; ?>  name ="glomped_checked" value="yes"> Members who have glomp!ed less than 
                                                            <input type="text" name="member_less_glomped" id="member_less_glomped"  class="span3" value="<?php echo $rule['member_less_glomped']; ?>" /> times in the last 
                                                            <input type="text" id="member_glomped_days" name="member_glomped_days" class="span3" value="<?php echo $rule['member_glomped_days']; ?>" /> days
                                                            <input type="radio" name ="glomped_set" <?php echo $glomped_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="glomped_set" <?php echo $glomped_set_checked2; ?>  value="yes"> (Mandatory)
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span8">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $lessfriends_checked; ?> name ="lessfriends_checked" value="yes"> Members who have less than no. of 
                                                            <input type="text" name="member_less_nofriends" class="span3" value="<?php echo $rule['member_less_nofriends'] ?>" /> friends
                                                            <input type="radio" name ="lessfriends_set" <?php echo $lessfriends_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="lessfriends_set" <?php echo $lessfriends_set_checked2; ?>  value="yes"> (Mandatory)
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" type="checkbox" name ="member_fb_notconnected" <?php echo $member_fb_notconnected; ?>  value="yes"> Members who have not connected their facebook account
                                                            <input type="radio" name ="member_fb_notconnected_set" <?php echo $member_fb_notconnected_set_checked1; ?>  value="no"> (Optional)
                                                            <input type="radio" name ="member_fb_notconnected_set" <?php echo $member_fb_notconnected_set_checked2; ?>  value="yes"> (Mandatory)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php include_once(_VIEW_INCLUDES_DIR_ADMIN_."while_label_rules.php");?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <br />
                        <div class="row-fluid">
                            <div class="span12">
                                <input class="btn btn-large btn-WI" type="submit" name="update_campaign_newsletter" value="Save Campaign Newsletter" >
                                <input class="btn btn-large btn-WI" type="submit" name="update_campaign_newsletter" value="Save and Run" >
                                <button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href = '<?php echo site_url() . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>
                            </div>
                        </div>
                        <?php echo form_close();  ?>
                    </div>
                </div>
            </div>
        </div>
<?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/footer.php"); ?>
    
<script>
        var camp_newsletter_id = '<?php echo $campaign_newsletter['campaign_newsletter_id']; ?>';
        var previous_id;
        
        
         
        

        $("#campaign_newsletter_template").focus(function() {
            /* Store the current value on focus and on change*/
            previous_id = $(this).val();
        }).change(function() {
            var id = $(this).val();
            if (confirm("Changing template will remove last updated content. Are you sure you want to continue?"))
            {
                var data = {
                    'camp_newsletter_id': camp_newsletter_id,
                    'template_id': id
                };
                _save_campaign(data);
            }
            else {
                $(this).val(previous_id);
                return false;
            }

            if (id != '') {
                var url = '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/preview_template/'); ?>';
                var url = url + '/' + camp_newsletter_id;
                $('#_preview').prop('src', url);
            }
            else {
                $('#_preview').prop('src', '');
            }
        });

        $('#toogle_voucher_details').click(function(e) {

            $('#voucher_details_table').collapse('toggle');
        });
        $('#voucher_details_table').collapse('hide');
        
        $('#toogle_campaign_rule').click(function(e) {

            $('#cmapign_rule_container').collapse('toggle');
        });

        var camp_id = camp_newsletter_id;

        if (<?php echo $campaign_newsletter['newsletter_no_rule']; ?> == '1') {
            disable_rule($('#disable_rule'));
        }

        $('#disable_rule').click(function(e) {
            disable_rule(this);
        });

        $('#enable_rule').click(function(e) {
            enable_rule(this);
        });

        function disable_rule(d) {

            $(d).addClass('disabled');
            $('#enable_rule').removeClass('disabled');
            $('#campaign_rule_form').find('input, select').attr('disabled', 'disabled');
            $('#disable_rule_val').val('1');

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/update_campaign_norule/'); ?>',
                data: {
                    no_rule: 1,
                    campaign_id: camp_id
                },
                dataType: 'json',
                success: function(res) {

                }
            });
        }

        function enable_rule(d) {
            $(d).addClass('disabled');
            $('#disable_rule').removeClass('disabled');
            $('#campaign_rule_form').find('input, select').removeAttr('disabled');
            $('#disable_rule_val').val('0');

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/update_campaign_norule/'); ?>',
                data: {
                    no_rule: 0,
                    campaign_id: camp_id
                },
                dataType: 'json',
                success: function(res) {

                }
            });

        }
        
        
        $('.other_rules :input[type="text"]').change(function(){
            var check_box = $(this).siblings('.check_rule')[0];

            if ($(this).val() == '' || $(this).val() == '0') {
                $(this).val(0);
                $(check_box).prop('checked', false);
                return false;
            }

            if (! $(check_box).is(':checked')) {
                $(check_box).prop('checked', true);
                return false;
            }
        });

        $('.check_rule').change(function(){
            if (! $(this).is(':checked')) {
                var input = $(this).siblings(':input[type="text"]');
                $(input).val(0);
                return false;
            }
        });        

        $('#cmapign_rule_container').collapse('show');
        
        $("#remove_selected").click(function() {
            $("#regions_id_selected > option:selected").each(function() {
                $(this).remove();
            });
        });
        
        $('#button_transfer').click(function(e) {
            $("#regions_id_all > option:selected").each(function() {

                var t = $(this).text();
                var v = $(this).val();
                var str = '<option value="' + v + '" selected="selected">' + t + '</option>';
                /**************/
                var exists = false;
                $('#regions_id_selected option').each(function() {
                    var nt = $(this).val();

                    if (v == this.value)
                        exists = true;
                });
                /***************/
                if (!exists)
                    $('#regions_id_selected').append(str);
            });
        });

        $('#btn_execute_rule').click(function(e) {
            var camp_id = $('#camp_id').val();
            $('#rule_result_display').html('Please wait...');

            
            var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/rule_result'); ?>';
            $.ajax({
                type: 'GET',
                url: base_url + "/" + camp_id + "/" + $('#disable_rule_val').val(),
                success: function(html) {
                    $('#rule_result_display').html('Number of People Found : ' + html);
                }
            });
            
        });
        /*execute rule*/

        $('#btn_execute_rule_confirm').click(function(e) {
            if (!confirm('Are you sure you want to generate voucher?\n Note: You will not be able to change the voucher details once you generate'))
            {
                return false;
            }
            $('#rule_execution_display').html('Please wait...voucher is generating...');
            var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/generate_voucher'); ?>';
            var camp_id = $('#camp_id').val();
            $.ajax({
                type: 'GET',
                url: base_url + "/" + camp_id,
                success: function(html) {
                    $('#rule_execution_display').html(html);
                    location.href = '<?php echo base_url(ADMIN_FOLDER . '/campaign/details/'); ?>' + '/' + camp_id;
                }
            });
        });
        
        $('#run_campaign').click(function(e) {
            if (!confirm('Are you sure you want to run this campaign?'))
            {
                return false;
            }

            $('#display_loading').html('Please wait...');
            $.fn.loadLoader();
            var camp_id = $(this).data('campaign_id');
            var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/run_campaign'); ?>';
            window.location = '<?php echo base_url(ADMIN_FOLDER . '/campaign/run_campaign'); ?>/' + camp_id + '/' + $('#disable_rule_val').val();
        });


        function _save_campaign(data) {
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url(ADMIN_FOLDER . '/campaign_newsletter/update_data/'); ?>',
                async: false,
                data: data,
                dataType: 'json',
                success: function(html) {

                }
            });
        }

        function make_all_seletect()
        {
            $('#regions_id_selected option').prop('selected', 'selected');
            
            $('#whitelabel_id_selected option').prop('selected', 'selected');
            $('#whitelabel_actions_selected option').prop('selected', 'selected');
            $('#whitelabel_merchants_selected option').prop('selected', 'selected');
            $('#whitelabel_products_selected option').prop('selected', 'selected');
        }
    </script>
    </body>
</html>


