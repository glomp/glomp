<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e) {
                /*Starts to two because 1 is pre-created*/
                var master_tr_ctr = 2;
                var invite_tr_ctr = 2;
                $('#add_more_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='master_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'master_tr_' + master_tr_ctr);
                    /*Add counter for unique ID*/
                    master_tr_ctr++;

                    $("tr[id^='master_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });

                $('#add_more_user_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='invites_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'invites_tr_' + invite_tr_ctr);
                    /*Add counter for unique ID*/
                    invite_tr_ctr++;

                    $("tr[id^='invites_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });
                
                $("#camp_voucher_table").on('click', '.remove_tr', function() {
                    $(this).parent().parent().remove();
                });
                /*for editing*/
                $('#add_more_tr_edit').click(function(e) {
                    var select_html = $('#product_id_0').html();
                    var tr_html = '<tr id="master_tr"><td><select name="new_product_id[]"' + select_html + '</select></td><td><input type="text" value="" name="new_voucher_qty[]"></td><td>&nbsp;</td><td><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';

                    $('#camp_voucher_table tr:last').after(tr_html);
                });
                $("#camp_voucher_table").on('click', '.delete_tr', function() {
                    if (!confirm('Are you sure you want to delete this record?'))
                    {
                        return false;
                    }
                    $(this).parent().parent().remove();
                    var id = $(this).attr('id');
                     
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/remove_cam_details'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + id,
                        success: function(html) {

                        }
                    });
                });
            });
        </script>
        <style type="text/css">
            .hidden_td{
                visibility:hidden;
            }
        </style>
    </head>
    <body class="">
        <?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/header.php"); ?>
        <div class="content">
            <?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_newsletter/add", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_newsletter", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>

                        <div class="page-subtitle">
                            <?php
                            //  echo $page_subtitle;
                            if (isset($msg)) {
                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                            }
                            if (isset($error_msg)) {
                                echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                            } else {
                                $error_msg = $this->session->flashdata('error_msg');
                                if (!empty($error_msg)) {
                                    echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                                }
                            }
                            if (validation_errors() != "") {
                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                            }
                            ?>
                        </div>
                        <?php echo form_open(ADMIN_FOLDER . "/campaign_newsletter/add", 'name="frmCms" id="cmspages"') ?>
                        <div class="content-box">
                            <div class="row-fluid">
                                <div class="rown-fluid">
                                    <div class="span6">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td><span>*</span>Campaign Newsletter  Name</td>
                                                <td><span>*</span>Newsletter Template</td>
                                                <td></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="campaign_newsletter_name" value="<?php echo set_value('campaign_newsletter_name'); ?>"></td>
                                                <td>
                                                    <select name="campaign_newsletter_template" id="campaign_newsletter_template">
                                                        <option value="">Select</option>
                                                        <?php
                                                        foreach ($newsletter_templates as $nt) {
                                                            $sel = (set_value('campaign_newsletter_template') == $nt['newsletter_id']) ? 'selected' : '';
                                                        ?>
                                                            <option <?php echo $sel;   ?> value="<?php echo $nt['newsletter_id'];   ?>"><?php echo $nt['name'];   ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td> </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <input class="btn btn-large btn-WI" type="submit" name="create_campaign_newsletter" value="Create Campaign Newsletter" >
                                            <button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href = '<?php echo site_url() . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(APPPATH . "views/" . ADMIN_FOLDER . "/includes/footer.php"); ?>
        <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
        <script>
            
        </script>
    </body>
</html>


