<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Minds Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <script src="assets/backend/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/backend/lib/loader.js" type="text/javascript"></script>
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e) {
                $( "#date_from" ).datepicker({
                  maxDate: 0,
                  onClose: function( selectedDate ) {
                    $( "#date_to" ).datepicker( "option", "minDate", selectedDate );
                  }
                })
                .on('change', function (e) {
                    
                });
                $( "#date_to" ).datepicker({
                  maxDate: 0,
                  onClose: function( selectedDate ) {
                    $( "#date_from" ).datepicker( "option", "maxDate", selectedDate );
                  }
                })
                .on('change', function (e) {
                });
                
                
                $('#toogle_voucher_details').click(function(e) {

                    $('#voucher_details_table').collapse('toggle');
                });
                $('#voucher_details_table').collapse('hide');                
                
                $('#toogle_campaign_rule').click(function(e) {

                    $('#cmapign_rule_container').collapse('toggle');
                });
                
                var camp_id = $('#camp_id').val();
                
                if (<?php echo $no_rule; ?> == '1') {
                    disable_rule($('#disable_rule'));
                }
                
                $('#disable_rule').click(function(e) {
                    disable_rule(this);
                });
                
                $('#enable_rule').click(function(e) {
                    enable_rule(this);
                });
                
                function disable_rule(d) {
                    
                    $(d).addClass('disabled');
                    $('#enable_rule').removeClass('disabled');
                    $('#campaign_rule_form').find('input, select').attr('disabled', 'disabled');
                    $('#disable_rule_val').val('1');
                    
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo site_url(ADMIN_FOLDER . '/campaign/update_campaign_norule/'); ?>',
                        data:{
                            no_rule: 1,
                            campaign_id: camp_id
                        },
                        dataType:'json',
                        success: function(res) {
                            
                        }
                    });
                }
                
                function enable_rule(d) {
                    $(d).addClass('disabled');
                    $('#disable_rule').removeClass('disabled');
                    $('#campaign_rule_form').find('input, select').removeAttr('disabled');
                    $('#disable_rule_val').val('0');
                    
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo site_url(ADMIN_FOLDER . '/campaign/update_campaign_norule/'); ?>',
                        data:{
                            no_rule: 0,
                            campaign_id: camp_id
                        },
                        dataType:'json',
                        success: function(res) {
                            
                        }
                    });
                    
                }
                
                $('#cmapign_rule_container').collapse('show');
             
                
            });
            function make_all_seletect()
            {
                $('#regions_id_selected option').prop('selected', 'selected');
                
                $('#whitelabel_id_selected option').prop('selected', 'selected');
                $('#whitelabel_actions_selected option').prop('selected', 'selected');
                $('#whitelabel_merchants_selected option').prop('selected', 'selected');
                $('#whitelabel_products_selected option').prop('selected', 'selected');
            }
            $(document).ready(function(e) {
                $('#btn_execute_rule').click(function(e) {
                    var camp_id = $('#camp_id').val();
                    $('#rule_result_display').html('Please wait...');

                    
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/rule_result'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + camp_id + "/" +$('#disable_rule_val').val(),
                        success: function(html) {
                            $('#rule_result_display').html('Number of People Found : ' + html);
                        }
                    });
                    
                });
                /*execute rule*/
                $('#btn_execute_rule_confirm').click(function(e) {
                    if (!confirm('Are you sure you want to generate voucher?\n Note: You will not be able to change the voucher details once you generate'))
                    {
                        return false;
                    }
                    $('#rule_execution_display').html('Please wait...voucher is generating...');
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/generate_voucher'); ?>';
                    var camp_id = $('#camp_id').val();
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + camp_id,
                        success: function(html) {
                            $('#rule_execution_display').html(html);
                            location.href = '<?php echo base_url(ADMIN_FOLDER . '/campaign/details/'); ?>' + '/' + camp_id;
                        }
                    });
                });
                
                $('#run_campaign').click(function(e) {
                    if (!confirm('Are you sure you want to run this campaign?'))
                    {
                        return false;
                    }

                    $('#display_loading').html('Please wait...');
                    $.fn.loadLoader();
                    var camp_id = $(this).data('campaign_id');
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign/run_campaign'); ?>';
                    window.location = '<?php echo base_url(ADMIN_FOLDER . '/campaign/run_campaign'); ?>/' + camp_id + '/' + $('#disable_rule_val').val();
                });
            });

        </script>
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign/AddCampaign", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Rules <a href="javascript:void(0)" id="toogle_campaign_rule" >Show/Hide</a></h5></div>
                            <div id="cmapign_rule_container">
                                <div class="widget-content">
                                    <div class="row-fluid">
                                        <span class="btn-WI btn ui-state-default ui-corner-all" data-campaign_id="28" id="disable_rule"><i class="icon-ban-circle"></i> Disable Rule</span>
                                        <span class="btn-WI btn ui-state-default ui-corner-all disabled" data-campaign_id="28" id="enable_rule"><i class="icon-check-sign"></i> Enable Rule</span>
                                        <input type ="hidden" id ="disable_rule_val" val ="0"/>
                                        <div class="span12 account">

                                            <?php
                                            if (isset($msg)) {
                                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                                            }
                                            if (isset($error_msg)) {
                                                echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                                            }
                                            if (validation_errors() != "") {
                                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                                            }

                                            if ($this->session->flashdata('campaign_run_message')) {
                                                echo $this->session->flashdata('campaign_run_message');
                                            }
                                            
                                            if (isset($_POST['submit_rule'])) {
                                                $min_age_checked = isset($_POST['min_age_checked']) ? 'checked' : '';
                                                $max_age_checked = isset($_POST['max_age_checked']) ? 'checked' : '';
                                                $regions_checked = isset($_POST['regions_checked']) ? 'checked' : '';
                                                $loggedover_checked = isset($_POST['loggedover_checked']) ? 'checked' : '';
                                                $noactivity_checked = isset($_POST['noactivity_checked']) ? 'checked' : '';
                                                $previously_rewarded_checked = isset($_POST['previously_rewarded_checked']) ? 'checked' : '';
                                                $notglomp_checked = isset($_POST['notglomp_checked']) ? 'checked' : '';
                                                $glomped_checked = isset($_POST['glomped_checked']) ? 'checked' : '';
                                                $lessfriends_checked = isset($_POST['lessfriends_checked']) ? 'checked' : '';
                                                $member_fb_notconnected = isset($_POST['member_fb_notconnected']) ? 'checked' : '';
                                            }
                                            else {
                                                //Get all rule that is checked or not
                                                $min_age_checked = ($this->campaign_rule_m->get_ischecked('min_age_checked', $camp_id)) ? 'checked' : '';
                                                $max_age_checked = ($this->campaign_rule_m->get_ischecked('max_age_checked', $camp_id)) ? 'checked' : '';
                                                $regions_checked = ($this->campaign_rule_m->get_ischecked('regions_checked', $camp_id)) ? 'checked' : '';
                                                $loggedover_checked = ($this->campaign_rule_m->get_ischecked('loggedover_checked', $camp_id)) ? 'checked' : '';
                                                $noactivity_checked = ($this->campaign_rule_m->get_ischecked('noactivity_checked', $camp_id)) ? 'checked' : '';
                                                $previously_rewarded_checked = ($this->campaign_rule_m->get_ischecked('previously_rewarded_checked', $camp_id)) ? 'checked' : '';
                                                $notglomp_checked = ($this->campaign_rule_m->get_ischecked('notglomp_checked', $camp_id)) ? 'checked' : '';
                                                $glomped_checked = ($this->campaign_rule_m->get_ischecked('glomped_checked', $camp_id)) ? 'checked' : '';
                                                $lessfriends_checked = ($this->campaign_rule_m->get_ischecked('lessfriends_checked', $camp_id)) ? 'checked' : '';
                                                $member_fb_notconnected = ($this->campaign_rule_m->get_ischecked('member_fb_notconnected', $camp_id)) ? 'checked' : '';
                                            }
                                            
                                            $rec_rule = $res_camp_rule->row();
                                            ?><br/>
                                            <?php echo form_open(ADMIN_FOLDER . '/campaign/details/' . $camp_id, 'id = "campaign_rule_form" onSubmit="return make_all_seletect()"'); ?>
                                            <div class="row-fluid">
                                                <div class="other_rules">
                                                    <div class="span3">
                                                        <input class="check_rule" style="margin: 0px;" <?php echo $min_age_checked; ?> type="checkbox" name ="min_age_checked" value="yes"> Min Age : <input type="text" name="min_age" class="span3" value="<?php echo $rec_rule->min_age; ?>" /> </div>
                                                    <div class="span3">
                                                       <input class="check_rule" style="margin: 0px;" <?php echo $max_age_checked; ?> type="checkbox" name ="max_age_checked" value="yes">  Max Age: <input type="text" name="max_age" class="span3" value="<?php echo $rec_rule->max_age; ?>" /> </div>
                                                </div>
                                                <div class="span3">
                                                    Gender  : <select name="gender" class="span3">
                                                        <?php
                                                            $gender = $rec_rule->gender
                                                        ?>
                                                        <option value="Both" <?php echo ($gender == 'Both') ? 'selected="selected"' : ''; ?>>Both</option>
                                                        <option value="Male" <?php echo ($gender == 'Male') ? 'selected="selected"' : ''; ?>>Male</option>
                                                        <option value="Female" <?php echo ($gender == 'Female') ? 'selected="selected"' : ''; ?>>Female</option>
                                                    </select>  
                                                </div>
                                                <br />
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td><input class="check_rule" style="margin: 0px;" <?php echo $regions_checked; ?> type="checkbox" name ="regions_checked" value="yes"> All Available Regions</td>
                                                        <td>Selected Regions</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="36%"> <select name="regions_id_all" id="regions_id_all" size="15" multiple class="span10">
                                                            <?php echo $this->regions_m->recursive(); ?>
                                                            </select></td>
                                                        <td width="64%">
                                                                <?php
                                                                $list = $rec_rule->region_id;
                                                                $res_own_region = $this->campaign_m->select_rule_region($list);
                                                                ?>
                                                            <select name="regions_id_selected[]" id="regions_id_selected" size="15" multiple class="span5">
                                                            <?php
                                                            if ($res_own_region->num_rows() > 0) {
                                                                foreach ($res_own_region->result() as $row) {
                                                                    echo "<option value='" . $row->region_id . "'>" . $row->region_name . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                            </select></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span data-all="regions_id_all" data-selected="regions_id_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                                                        <td><span  data-selected="regions_id_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
                                                    </tr>
                                                </table>
                                                <br/>
                                                <div class="other_rules">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <input class="check_rule" style="margin: 0px; " <?php echo $loggedover_checked; ?> type="checkbox" name ="loggedover_checked" value="yes"> Members who have been logged out for over 
                                                            <input type="text" name="member_logout_days" class="span3" value="<?php echo $rec_rule->member_logout_days; ?>" /> days
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $noactivity_checked; ?> name ="noactivity_checked" value="yes"> Members who have had no activity for over 
                                                            <input type="text" name="member_noactivity_days" class="span3" value="<?php echo $rec_rule->member_noactivity_days; ?>" /> days
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span5">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $previously_rewarded_checked; ?> name ="previously_rewarded_checked" value="yes"> Members who have been previously rewarded within 
                                                            <input type="text" name="member_prev_rewarded_days" class="span3" value="<?php echo $rec_rule->member_prev_rewarded_days; ?>" /> days
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $notglomp_checked; ?>  name ="notglomp_checked" value="yes"> Members who have not glomp!ed anyone for over 
                                                            <input type="text" name="member_noglomp_days" class="span3" value="<?php echo $rec_rule->member_noglomp_days; ?>" /> days
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span7">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $glomped_checked; ?>  name ="glomped_checked" value="yes"> Members who have glomp!ed less than 
                                                            <input type="text" name="member_less_glomped" id="member_less_glomped"  class="span3" value="<?php echo $rec_rule->member_less_glomped; ?>" /> times in the last 
                                                            <input type="text" id="member_glomped_days" name="member_glomped_days" class="span3" value="<?php echo $rec_rule->member_glomped_days; ?>" /> days
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span7">
                                                            <input class="check_rule" style="margin: 0px; "type="checkbox" <?php echo $lessfriends_checked; ?> name ="lessfriends_checked" value="yes"> Members who have less than no. of 
                                                            <input type="text" name="member_less_nofriends" class="span3" value="<?php echo $rec_rule->member_less_nofriends ?>" /> friends
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span6">
                                                            <input class="check_rule" type="checkbox" name ="member_fb_notconnected" <?php echo $member_fb_notconnected; ?>  value="yes"> Members who have not connected their facebook account
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <?php include_once(_VIEW_INCLUDES_DIR_ADMIN_."while_label_rules.php");?>
                                                </div>
                                                <br />
                                                <input type="Submit"  name="submit_rule" class="btn btn-large btn-WI" value="Update Rule" />

                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Information</h5></div>
                            <div class="widget-content">
                                <div class="row-fluid">
                                    <div class="span12 account">
                                        Name:<span class="label label-success"><?php echo $rec_camp->campaign_name; ?> </span> Type:<span class="label label-success"> <?php echo $rec_camp->campaign_type; ?></span> Expiry Day :<span class="label label-success"> <?php echo $rec_camp->campaign_voucher_expiry_day; ?> </span>  Last Execution Date:<span class="label label-success"><?php echo ($rec_camp->campaign_last_executed_date == '') ? 'Never' : $rec_camp->campaign_last_executed_date; ?></span>
<?php
$res_available = $this->campaign_m->campaign_voucher_by_camp_id($camp_id, "AND camp_voucher_assign_status = 'UnAssigned'");
$available_voucher = $res_available->num_rows();
?>
                                        Available Voucher: <span class="label label-success"><?php echo $available_voucher; ?> </span>
                                        <br/><br/>&nbsp;&nbsp;

                                    </div>
                                        <?php echo form_open(ADMIN_FOLDER . '/campagin/rule_result/' . $camp_id); ?>
                                    <input type="hidden" id="camp_id" name="camp_id" value="<?php echo $camp_id; ?>" />

<?php
$res_voucher = $this->campaign_m->campaign_voucher_by_camp_id($camp_id);
$num_rows = $res_voucher->num_rows();
if ($num_rows == 0) {
    ?>
                                        <input type="button" id="btn_execute_rule_confirm"  name="execute_rule" class="btn btn-large btn-WI" value="Create Voucher" /><?php
                                    } else {
                                        ?>	
                                        <span class='btn-WI btn ui-state-default ui-corner-all disable' data-campaign_id="<?php echo $rec_camp->campaign_id ?>" id="run_campaign"><i class="icon-play"></i> Run Campaign</span>
                                    <?php } ?>								

                                    <h5><span id="rule_execution_display"></span> </h5>
                                    &nbsp;&nbsp;&nbsp;&nbsp;<span id="display_loading"></span>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Voucher Details <a href="javascript:void(0)" id="toogle_voucher_details" >Show/Hide</a></h5></div>
                            <div id="voucher_details_table">
                                <div class="widget-content" >
                                    <div class="row-fluid">
                                        <div class="span12 account">

                                            <table width="100%"  class="table table-striped table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>Merchant</td>
                                                        <td>Product</td>
                                                        <td>Image</td>
                                                        <td>Total Voucher</td>
                                                        <td>Remaining Voucher</td>
                                                        <td>Total Point</td>
                                                    </tr>
<?php
$num = $res_camp_details->num_rows();
if ($num > 0) {
    $camp_type = $rec_camp->campaign_type;
    $total_voucher = 0;
    $total_point = 0;
    $remaining = 0;
    foreach ($res_camp_details->result() as $row_deails) {
        $img = $this->custom_func->product_logo($row_deails->prod_image);
        $point = $row_deails->camp_voucher_qty * $row_deails->camp_prod_point;
        $total_point += $point;
        ?>
                                                            <tr>
                                                                <td><?php echo $row_deails->merchant_name; ?></td>
                                                                <td> <?php echo $row_deails->prod_name; ?></td>
                                                                <td><img src="<?php echo $img; ?>" title="<?php echo $row_deails->prod_name; ?>" alt="<?php echo $row_deails->prod_name; ?>" width="100" />
                                                            <?php
                                                            $res_remaining = $this->campaign_m->voucherByCampAndProduct($row_deails->camp_campaign_id, $row_deails->camp_prod_id);
                                                            $remaining_num = $res_remaining->num_rows();
                                                            ?>
                                                                </td>
                                                                <td><?php
                                                            $prod_voucher = $row_deails->camp_voucher_qty;
                                                            if ($camp_type == 'Both') {
                                                                $prod_voucher = $prod_voucher * 2;
                                                            }
                                                            //
                                                            $total_voucher +=$prod_voucher;
                                                            echo $prod_voucher;
                                                            ?></td>
                                                                <td><?php
                                                                    echo $rem = ($prod_voucher - $remaining_num);
                                                                    $remaining +=$rem;
                                                                    ?></td>
                                                                <td><?php echo $point; ?></td>
                                                            </tr>
                                                                    <?php }
                                                                ?>
                                                        <tr>
                                                            <td><strong>Total</strong></td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td><strong><?php echo $total_voucher; ?></strong></td>
                                                            <td><strong><?php echo $remaining; ?></strong></td>
                                                            <td><strong><?php echo $total_point; ?></strong></td>
                                                        </tr>
    <?php
}
?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i>Number of qualified members</h5></div>

                            <div class="widget-content" >
                                <div class="row-fluid">
                                    <div class="span12 account">

                                        <input type="button" id="btn_execute_rule"  name="execute_rule" class="btn btn-large btn-WI" value="Number of Qualified  Members for this campaign" />&nbsp;&nbsp;&nbsp;	



                                        <br/> <br/> <h5><span id="rule_result_display"></span> </h5>

                                    </div>
                                </div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
            <?php include("includes/footer.php"); ?>            
            <script>
                  
                  $('.other_rules :input[type="text"]').change(function(){
                      
                      var check_box = $(this).siblings('.check_rule')[0];
                      
                      if ($(this).val() == '' || $(this).val() == '0') {
                          $(this).val(0);
                          $(check_box).prop('checked', false);
                          return false;
                      }
                      
                      if (! $(check_box).is(':checked')) {
                          $(check_box).prop('checked', true);
                          return false;
                      }
                  });
                  
                  $('.check_rule').change(function(){
                      if (! $(this).is(':checked')) {
                          var input = $(this).siblings(':input[type="text"]');
                          $(input).val(0);
                          return false;
                      }
                  });
/*
                $('.check_rule').each(function(i, elem){
                    if ($(elem).is(':checked')) {
                        return true;
                    }
                    
                    if ($(elem).attr('name') == 'regions_checked') {
                        $('#regions_id_all').attr('disabled', 'disabled')
                        $('#regions_id_selected').attr('disabled', 'disabled')
                    }
                    
                    if ($(elem).attr('name') == 'glomped_checked') {
                        $('#member_less_glomped').attr('disabled', 'disabled');
                        $('#member_glomped_days').attr('disabled', 'disabled');
                    }
                    
                   $(elem).next(':input').attr('disabled', 'disabled');
                });
               
                $('.check_rule').click(function(){
                    if ($(this).is(':checked')) {
                        $(this).next(':input').removeAttr('disabled', 'disabled');
                        
                        if ($(this).attr('name') == 'regions_checked') {
                            $('#regions_id_all').removeAttr('disabled', 'disabled');
                            $('#regions_id_selected').removeAttr('disabled', 'disabled');
                        }
                        if ($(this).attr('name') == 'glomped_checked') {
                            $('#member_less_glomped').removeAttr('disabled', 'disabled');
                            $('#member_glomped_days').removeAttr('disabled', 'disabled');
                        }
                        return true;
                    }
                    
                    $(this).next(':input').attr('disabled', 'disabled');
                        
                    if ($(this).attr('name') == 'regions_checked') {
                        $('#regions_id_all').attr('disabled', 'disabled');
                        $('#regions_id_selected').attr('disabled', 'disabled');
                    }
                    
                    if ($(this).attr('name') == 'glomped_checked') {
                        $('#member_less_glomped').attr('disabled', 'disabled');
                        $('#member_glomped_days').attr('disabled', 'disabled');
                    }
                  
                }).trigger();
*/
            </script>
    </body>
</html>


