<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Minds Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <script src="assets/backend/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/backend/lib/loader.js" type="text/javascript"></script>
        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e){
                
				/*Starts to two because 1 is pre-created*/
			    var nowTemp = new Date();
				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

				var checkin = $('#start_date').datepicker({
				onRender: function(date) {
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
				}
				}).on('changeDate', function(ev) {
				if (ev.date.valueOf() > checkout.date.valueOf()) {
				var newDate = new Date(ev.date);
				newDate.setDate(newDate.getDate() + 1);
				checkout.setValue(newDate);
				}
				checkin.hide();
				$('#expiry_date')[0].focus();
				}).data('datepicker');
				var checkout = $('#expiry_date').datepicker({
				onRender: function(date) {
				return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
				}
				}).on('changeDate', function(ev) {
				checkout.hide();
				}).data('datepicker');
			
				$("#follow_up_campaign").change(function()
				{
					var chk= $(this).prop('checked');
					if(chk){
						$('#follow_up_wrapper').show('normal');						
					}
					else{
						$('#follow_up_wrapper').hide('normal');						
					}
					
				});
				
				$("#enable_time_limit").change(function()
				{
					var chk= !$(this).prop('checked');
					$('#start_date').prop('disabled',chk);
					$('#expiry_date').prop('disabled',chk);
					
				});				
				
			
			
                var master_tr_ctr = 2;
                var f_master_tr_ctr = 2;
				
                $('#add_more_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='master_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'master_tr_' + master_tr_ctr);
                    /*Add counter for unique ID*/
                    master_tr_ctr++;
                    
                    $("tr[id^='master_tr_']:last").after(clone);
                    $(clone).find('.hidden_td').removeClass('hidden_td');
                });
				
				
                
                $('#f_add_more_tr').click(function(e) {
                    /*Clone last created row*/
                    var clone = $("tr[id^='f_master_tr_']:last").clone();
                    /*Change unique ID*/
                    clone = $(clone).attr('id', 'f_master_tr_' + master_tr_ctr);
                    /*Add counter for unique ID*/
                    master_tr_ctr++;
                    
                    $("tr[id^='f_master_tr_']:last").after(clone);
                    $(clone).find('.f_hidden_td').removeClass('f_hidden_td');
                });
				
				
				
                
                $("#camp_voucher_table").on('click', '.remove_tr', function() {
                    $(this).parent().parent().remove();
                });
				
				 $("#camp_follow_up_table").on('click', '.f_remove_tr', function() {
                    $(this).parent().parent().remove();
                });
				
				
				
				
                /*for editing*/
                $('#add_more_tr_edit').click(function(e) {
                    var select_html = $('#product_id_0').html();
                    var tr_html = '<tr id="master_tr"><td><select name="new_product_id[]"' + select_html + '</select></td><td><input type="text" value="" name="new_voucher_qty[]"></td><td>&nbsp;</td><td><a href="javascript:void(0)"  class="remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';

                    $('#camp_voucher_table tr:last').after(tr_html);
                });
				 $('#f_add_more_tr_edit').click(function(e) {
                    var select_html = $('#f_product_id_0').html();
                    var tr_html = '<tr id="f_master_tr"><td><select name="f_new_product_id[]"' + select_html + '</select></td><td><input type="text" value="" name="f_new_voucher_qty[]"></td><td>&nbsp;</td><td><a href="javascript:void(0)"  class="f_remove_tr">Remove <i class="icon-remove"></i></a></td></tr>';

                    $('#camp_follow_up_table tr:last').after(tr_html);
                });
				
				
				
                $("#camp_voucher_table").on('click', '.delete_tr', function() {
                    if (!confirm('Are you sure you want to delete this record?'))
                    {
                        return false;
                    }
                    $(this).parent().parent().remove();
                    var id = $(this).attr('id');
                     
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/remove_cam_details'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + id,
                        success: function(html) {

                        }
                    });
                });
				
				
				$("#camp_follow_up_table").on('click', '.f_delete_tr', function() {
                    if (!confirm('Are you sure you want to delete this record?'))
                    {
                        return false;
                    }
                    $(this).parent().parent().remove();
                    var id = $(this).attr('id');
                     
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/remove_cam_details'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + id,
                        success: function(html) {

                        }
                    });
                });
				
				
				 $('#toogle_campaign_rule').click(function(e) {

                    $('#cmapign_rule_container').collapse('toggle');
                });
					$('#cmapign_rule_container').collapse('show');
            });
			function make_all_select()
			{
				$('#regions_id_selected option').prop('selected', 'selected');
			}
        </script>
        <style type="text/css">
            .hidden_td{
                visibility:hidden;
            }
        </style>
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_by_promo/AddCampaign", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_by_promo", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>

                        <div class="page-subtitle">
                            <?php
                            //  echo $page_subtitle;
                            if (isset($msg)) {
                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                            }
                            if (isset($error_msg)) {
                                echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
                            }
							else{
								$error_msg = $this->session->flashdata('error_msg');
								if (! empty($error_msg)) {
									echo "<div class=\"alert alert-error\">" . $error_msg . "</div>";
								}
							}
                            if (validation_errors() != "") {
                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                            }
                            ?>
                            <div id="error_list" style="display: none;" class="custom-error"></div>
                        </div>
                            <?php if ($page_action == "view") { ?>
                            <table width="100%" class="table table-condensed table-hover">
                                <tr>
                                    <td><strong>SN.</strong></td>
                                    <td><strong>Created Date</strong></td>
                                    <td><strong>Campaign Name</strong></td>
                                    <td style="text-align:center"><strong>Campaign Type</strong></td>
                                    <td style="text-align:center"><strong>Total Vouchers/Available</strong></td>
                                    <td style="text-align:center;"><strong>Status</strong></td>
                                    <td style="text-align:center;"><strong>Options</strong></td>
                                </tr>
                                <?php
                                if (isset($res_campaign)) {
                                    $i = 1;
                                    foreach ($res_campaign->result() as $rec_campaign) {

                                        $res_voucher = $this->campaign_m->campaign_voucher($rec_campaign->campaign_id);
                                        $num_rows = $res_voucher->num_rows();
                                        $used_voucher = $num_rows;//$total_voucher
										
										$total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($rec_campaign->campaign_id);
                                        

                                        $available_voucher = $total_voucher-$used_voucher;
                                ?>
                                        <tr>
                                            <td><?php echo $i;$i++; ?></td>
                                            <td><?php echo $this->custom_func->time_stamp($rec_campaign->campaign_created_date); ?></td>
                                            <td><?php echo $rec_campaign->campaign_name; ?></td>
                                            <td style="text-align:center">
                                                <?php echo $rec_campaign->campaign_type; ?>
                                            </td>
                                            <td style="text-align:center;"><?php echo  $total_voucher . '/' . $available_voucher; ?></td>
                                            <td style="text-align:center;"><?php echo $rec_campaign->campaign_by_promo_status; ?>
                                            </td>
                                            <td style="text-align:center;">
                                                <?php
                                                    echo anchor(ADMIN_FOLDER . "/campaign_by_promo/details/" . $rec_campaign->campaign_id . "/", "<i class=\"icon-eye-open\"></i>", "title='View' ");
                                                    if ($rec_campaign->campaign_by_promo_status == "Active") {
                                                        echo "&nbsp;&nbsp;&nbsp;";
                                                        echo anchor(ADMIN_FOLDER . "/campaign_by_promo/editCampaign/" . $rec_campaign->campaign_id . "/", "<i class=\"icon-edit\"></i>", "title='Edit' ");
                                                    }
                                                ?>
                                                <?php if($rec_campaign->campaign_by_promo_status <> "Done"): ?>
                                                    <a class ="close_campaign" href="<?php echo site_url(ADMIN_FOLDER . "/campaign_by_promo/close_campaign/" . $rec_campaign->campaign_id . "/"); ?>" title="Close" ><i class="icon-ban-circle"></i></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                            <?php
                                            }
                                                echo '<tr><td colspan="7">' . $links . '</td></tr>';
                                            }
                                            else
                                                echo'<tr><td colspan="7">No records found.</td></tr>';
                                        ?>
                            </table>
                            <?php } elseif ($page_action == "add") { ?>
                            <?php echo form_open(ADMIN_FOLDER . "/campaign_by_promo/AddCampaign", 'name="frmCms" id="campaign_form"') ?>
                            
							
									<div class="content-box">
										<div class="row-fluid">
											<div class="span12">
												<table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
													<tr>
														<td><span>* Campaign </span>  Name</td>
														<td><span>* Promo Code</td>
														<td><span>*</span>Campaign Type</td>
														<td><span>*</span>Voucher Expiry Day</td>
													</tr>
													<tr>
														<td><input type="text" name="campaign_name" value="<?php echo set_value('campaign_name'); ?>"></td>
														<td><input type="text" name="promo_code" value="<?php echo set_value('promo_code'); ?>"></td>
														<td> <select name="campaign_type">
																<option value="">Please Select</option>
																<option <?php if(set_value('campaign_type')=='Consumable') echo ' selected '; ?> value="Consumable">Consumable</option>
																<option <?php if(set_value('campaign_type')=='Assignable') echo ' selected '; ?> value="Assignable">Assignable</option>
															</select></td>
														<td> <input type="text" value="<?php echo set_value('expiry_day'); ?>" name="expiry_day"></td>
													</tr>
													<tr>
														<td  valign="bottom">* Campaign For</td>														
														<td>
															<div>
																<input type="checkbox" style="margin-top:-3px;" <?php echo $enable_time_limit;?> id="enable_time_limit" name="enable_time_limit" value="Y" /><span>&nbsp;Enable time limit</span>
															</div>
															Campaign Start Date
														</td>
														<td valign="bottom">
															Campaign End Date
														</td>														
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td><select name="campaign_for"  >
																<option value="">Please Select</option>
																<option <?php if(set_value('campaign_for')=='Member') echo ' selected '; ?> value="Member">Member</option>
																<option <?php if(set_value('campaign_for')=='Non-member') echo ' selected '; ?> value="Non-member">Non-member</option>
																<option <?php if(set_value('campaign_for')=='Both') echo ' selected '; ?> value="Both">Both</option>
															</select>
														</td>														
														<td><input type="text" <?php if($enable_time_limit=='') echo ' disabled ';?> value="<?php echo set_value('start_date'); ?>" name="start_date" id="start_date" ></td>
														<td><input type="text" <?php if($enable_time_limit=='') echo ' disabled ';?> value="<?php echo set_value('expiry_date'); ?>" name="expiry_date" id="expiry_date" ></td>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td colspan="4"><hr size="1" style="padding:0px;margin:0px;" /></td>
													</tr>


                                                    <tr>
                                                        <td><a href="javascript:void(0)" id="add_products" class="btn btn-info">Add product <i class="icon-plus"></i></a></td>
                                                        <td colspan="3">
                                                            <!-- This is hidden from view-->
                                                            <ul id="product-list" class="row" style="display:none;">
                                                                <?php if(isset($sponsor)):?>
                                                                <li class="row sponsor" style="float:none;text-align:left;padding:0 5px;margin:0; background:#faf5ce;">
                                                                    <div><strong>Sponsor Products</strong></div>
                                                                    <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                        <?php foreach($sponsor as $p):?>
                                                                        <li class="row prod_info"
                                                                            data-region_id="<?php echo $p->merchant_region_id?>"
                                                                            data-merchant_id="<?php echo $p->merchant_id?>"
                                                                            data-keywords="<?php echo strtolower($p->merchant_name.' '.$p->prod_name);?>"
                                                                            data-prod_id="<?php echo $p->prod_id?>"
                                                                            data-prod_name="<?php echo $p->prod_name?>"
                                                                            data-current_qty="<?php echo ($p->current_qty)?$p->current_qty:'0'?>"
                                                                            style="float:none; text-align:left;padding:0;margin:0">
                                                                            <div style="float:left"><?php echo $p->prod_name?> (<?php echo $p->merchant_name?>)</div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo ($p->current_qty)?$p->current_qty:'0'?> left</div>
                                                                        </li>
                                                                        <?php endforeach;?>
                                                                    </ul>
                                                                </li>
                                                                <?php endif;?>
                                                                <?php foreach($merchants as $mid => $m):?>
                                                                <li class="row" data-merchant_id="<?php echo $mid?>" data-region_id="<?php echo $m['region']?>" style="float:none;text-align:left;padding:0 5px;margin:0">
                                                                    <div><strong><?php echo $m['name'];?></strong></div>
                                                                    <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                        <?php foreach( $m['products'] as $p ):?>
                                                                        <li class="row prod_info" data-keywords="<?php echo strtolower($m['name'].' '.$p->prod_name);?>" data-prod_id="<?php echo $p->prod_id?>" data-prod_name="<?php echo $p->prod_name?>" style="float:none; text-align:left;padding:0;margin:0">
                                                                            <div style="float:left"><?php echo $p->prod_name?></div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                        </li>
                                                                        <?php endforeach;?>
                                                                    </ul>
                                                                <?php endforeach;?>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Filter</td>
                                                        <td>* Product</td>
                                                        <td>* Voucher Qty</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="add-product">
                                                        <td>
                                                            <select class="filter_by_region" name="region_id">
                                                                <option value="">Filter By Region</option>
                                                                <?php foreach($regions as $r):?>
                                                                <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td class="add_products">
                                                            <input type="hidden" class="product_id" name="product_id[]" />
                                                            <input type="text" class="trigger_product_list" placeholder="Select a Product" />
                                                            <ul class="place-holder"></ul>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="voucher_qty[]" placeholder="Voucher Quantity" />
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="remove_row">Remove <i class="icon-remove"></i></a>
                                                        </td>
                                                    </tr>



													<tr>
														<td colspan="4" align="left">
															<input type="checkbox" style="margin-top:-3px;" <?php echo $follow_up_checked;?> id="follow_up_campaign" name="follow_up_campaign" value="enable" /><span>&nbsp;Enable follow up campaign</span>
															<div id="follow_up_wrapper" style="background-color:#FDF7F5;padding:15px;border:solid 1px #ddd;display:<?php if($follow_up_checked!='checked') echo 'none';?>">
															<!--follow-up campaign-->
															<table id="camp_follow_up_table">
																<tr>
																	<td colspan="4">
																		<h5>Follow Up Campaign</h5>
																		<div><span>*</span>Campaign Type</div>
																		<div>
																			<select name="follow_up_campagin_type">
																				<option value="">Please Select</option>
																				<option <?php if(set_value('follow_up_campagin_type')=='Consumable') echo ' selected '; ?> value="Consumable">Consumable</option>
																				<option <?php if(set_value('follow_up_campagin_type')=='Assignable') echo ' selected '; ?> value="Assignable">Assignable</option>
																			</select>
																		</div>
																		<hr size="1" style="padding:0px;margin:0px;" />
																	</td>
																</tr>
																<tr>
																	<td><a href="javascript:void(0)" id="f_add_more_tr" class="btn btn-info">Add more product <i class="icon-plus"></i></a></td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td>* Product</td>
																	<td>* Voucher Quantity</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
																<tr id="f_master_tr_1">
																	<td><select name="f_product_id[]" id="f_product_id_">
																			<option value="">Please Choose Product</option>
																			<?php
																				echo $this->product_m->product_by_merchant_dropdown_campaign(set_value('f_product_id'));
																			?>
																		</select>
																	</td>
																	<td><input type="text" value="<?php echo set_value('f_voucher_qty'); ?>" name="f_voucher_qty[]"></td>
																	<td>&nbsp;</td>
																	<td  class="f_hidden_td"><a href="javascript:void(0)"  class="f_remove_tr">Remove <i class="icon-remove"></i></a></td>
																</tr>
															</table>
															<!--follow-up campaign-->
															</div>
														</td>
													</tr>
													<tr>
														<td colspan="4"><hr size="3" style="padding:0px;margin:0px;" /></td>
													</tr>
													<tr>
														<td colspan="4">
															<!-- rule -->
															<h5>Campaign Rules</h5>
															<div class="row-fluid">
															Rule description : <input type="text" style="width:800px" name="rule_desc" class="span3" value="<?php echo set_value('rule_desc'); ?>" />
															</div>
															<div class="row-fluid">
																<div class="span3">
																	Min Age : <input type="text" name="min_age" class="span3" value="<?php if(is_numeric(set_value('min_age'))) echo set_value('min_age'); else echo "0"; ?>" /> </div>
																<div class="span3">
																	Max Age: <input type="text" name="max_age" class="span3"value="<?php if(is_numeric(set_value('max_age'))) echo set_value('max_age'); else echo "0";;?>" /> </div>
																<div class="span3">
																	Gender  : <select name="gender" class="span3">
																	<?php
																	$gender = $rec_rule->gender
																	?>
																		<option value="Both" <?php if(set_value('gender')=='Both') echo ' selected '; ?>>Both</option>
																		<option value="Male" <?php if(set_value('gender')=='Male') echo ' selected '; ?>>Male</option>
																		<option value="Female" <?php if(set_value('gender')=='Female') echo ' selected '; ?>>Female</option>
																	</select>  
																</div>
																<br />
																<table width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tr>
																		<td>All Available Regions</td>
																		<td>Selected Regions</td>
																	</tr>
																	<tr>
																		<td width="36%"> <select name="regions_id_all" id="regions_id_all" size="15" multiple class="span10">
																		<?php echo $this->regions_m->recursive(); ?>
																			</select></td>
																		<td width="64%">
																				
																			<select name="regions_id_selected[]" id="regions_id_selected" size="15" multiple class="span5">
																			
																			</select></td>
																	</tr>
																	<tr>
																		<td><span data-all="regions_id_all" data-selected="regions_id_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                                                                        <td><span  data-selected="regions_id_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
																	</tr>
																</table>
															</div>
															<!-- rule -->
                                                            <?php include_once(_VIEW_INCLUDES_DIR_ADMIN_."while_label_rules.php");?>
                                                            
														</td>
													</tr>
												</table>
												<input class="btn btn-large btn-WI" type="submit" name="create_campaign" id="campaign_submit_btn" value="Create Campaign" >
												<button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href = '<?php echo site_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>		
											</div>
										</div>	
									</div>
                            <?php echo form_close(); ?>
                            <?php } elseif ($page_action == "edit") { ?>
                            <?php echo form_open(ADMIN_FOLDER . "/campaign_by_promo/editCampaign/" . $rec_campaign->campaign_id, 'name="frmCms" id="campaign_form"') ?>
                            <div class="content-box">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td><span>* Campaign </span>  Name</td>
												<td><span>* Promo Code</span></td>
                                                <td><span>*</span>  Campaign Type</td>
                                                <td><span>*</span>  Voucher Expiry Day</td>                                                
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="campaign_name" value="<?php echo $rec_campaign->campaign_name; ?>"></td>
												<td><input type="text" name="promo_code" value="<?php echo $rec_campaign->campaign_by_promo_code; ?>"></td>
                                                <td>
                                                    <?php
                                                        $type = $rec_campaign->campaign_type;

                                                    ?>
                                                    <select name="campaign_type">
                                                        <option value="">Please Select</option>
                                                        <option value="Consumable" <?php echo $selected = ($type == 'Consumable') ? 'selected="selected"' : ''; ?>>Consumable</option>
                                                        <option value="Assignable" <?php echo $selected = ($type == 'Assignable') ? 'selected="selected"' : ''; ?>>Assignable</option>
                                                        <option value="Both" <?php echo $selected = ($type == 'Both') ? 'selected="selected"' : ''; ?>>Both</option>

                                                    </select></td>
                                                <td> <input type="text" value="<?php echo $rec_campaign->campaign_voucher_expiry_day; ?>" name="expiry_day"></td>
                                                <td>&nbsp;</td>
                                            </tr>
											<tr>
														<?php
															$enable_time_limit="";
															if($rec_campaign->campaign_by_promo_enable_time_limit=='Y')
																$enable_time_limit=" checked ";
														?>
														<td  valign="bottom">* Campaign For</td>														
														<td>
															<div>
																<input type="checkbox" style="margin-top:-3px;" <?php echo $enable_time_limit;?> id="enable_time_limit" name="enable_time_limit" value="Y" /><span>&nbsp;Enable time limit</span>
															</div>
															Campaign Start Date
														</td>
														<td valign="bottom">
															Campaign End Date
														</td>														
														<td>&nbsp;</td>
													</tr>
													<?php
															$campaign_for=$rec_campaign->campaign_by_promo_for;
															$start_date=$rec_campaign->campaign_by_promo_start_date;															
															$expiry_date=$rec_campaign->campaign_by_promo_end_date;															
														?>
													<tr>
														<td><select name="campaign_for"  >
																<option value="">Please Select</option>
																<option <?php if($campaign_for=='Member') echo ' selected '; ?> value="Member">Member</option>
																<option <?php if($campaign_for=='Non-member') echo ' selected '; ?> value="Non-member">Non-member</option>
																<option <?php if($campaign_for=='Both') echo ' selected '; ?> value="Both">Both</option>
															</select>
														</td>														
														<td><input type="text" <?php if($enable_time_limit=='') echo ' disabled value=""'; else{ ?>value="<?php echo date('m/d/Y',strtotime($start_date)); } ?>" name="start_date" id="start_date" ></td>
														<td><input type="text" <?php if($enable_time_limit=='') echo ' disabled  value=""'; else{ ?> value="<?php echo date('m/d/Y',strtotime($expiry_date)); } ?>" name="expiry_date" id="expiry_date" ></td>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td colspan="4"><hr size="1" style="padding:0px;margin:0px;" /></td>
													</tr>



                                                    <tr>
                                                        <td><a href="javascript:void(0)" id="add_products" class="btn btn-info">Add product <i class="icon-plus"></i></a></td>
                                                        <td colspan="3">
                                                            <!-- This is hidden from view-->
                                                            <ul id="product-list" class="row" style="display:none;">
                                                                <?php if(isset($sponsor)):?>
                                                                <li class="row sponsor" style="float:none;text-align:left;padding:0 5px;margin:0; background:#faf5ce;">
                                                                    <div><strong>Sponsor Products</strong></div>
                                                                    <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                        <?php foreach($sponsor as $p):?>
                                                                        <li class="row prod_info"
                                                                            data-region_id="<?php echo $p->merchant_region_id?>"
                                                                            data-merchant_id="<?php echo $p->merchant_id?>"
                                                                            data-keywords="<?php echo strtolower($p->merchant_name.' '.$p->prod_name);?>"
                                                                            data-prod_id="<?php echo $p->prod_id?>"
                                                                            data-prod_name="<?php echo $p->prod_name?>"
                                                                            data-current_qty="<?php echo ($p->current_qty)?$p->current_qty:'0'?>"
                                                                            style="float:none; text-align:left;padding:0;margin:0">
                                                                            <div style="float:left"><?php echo $p->prod_name?> (<?php echo $p->merchant_name?>)</div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo ($p->current_qty)?$p->current_qty:'0'?> left</div>
                                                                        </li>
                                                                        <?php endforeach;?>
                                                                    </ul>
                                                                </li>
                                                                <?php endif;?>
                                                                <?php foreach($merchants as $mid => $m):?>
                                                                <li class="row" data-merchant_id="<?php echo $mid?>" data-region_id="<?php echo $m['region']?>" style="float:none;text-align:left;padding:0 5px;margin:0">
                                                                    <div><strong><?php echo $m['name'];?></strong></div>
                                                                    <ul class="row" style="padding:0 0 0 15px;margin:0">
                                                                        <?php foreach( $m['products'] as $p ):?>
                                                                        <li class="row prod_info" data-keywords="<?php echo strtolower($m['name'].' '. $p->prod_name);?>" data-prod_id="<?php echo $p->prod_id?>" data-prod_name="<?php echo $p->prod_name?>" style="float:none; text-align:left;padding:0;margin:0">
                                                                            <div style="float:left"><?php echo $p->prod_name?></div>
                                                                            <div style="float:right;margin-right:10px;"><?php echo $p->prod_point?> points</div>
                                                                        </li>
                                                                        <?php endforeach;?>
                                                                    </ul>
                                                                <?php endforeach;?>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Filter</td>
                                                        <td>* Product</td>
                                                        <td>* Voucher Qty</td>
                                                        <td></td>
                                                    </tr>
                                                    <?php $res_details = $this->campaign_m->campaign_details_by_id_by_type($camp_id,'by_promo_primary'); ?>
                                                    <?php if($res_details->num_rows() > 0):?>
                                                    <?php foreach($res_details->result() as $row_details):?>
                                                    <?php $prod = $this->db->get_where('gl_product',array('prod_id'=>$row_details->camp_prod_id))->row(); ?>
                                                    <tr class="add-product edit-record">
                                                        <td>
                                                            <input type="hidden" name="cam_details_id[]" value="<?php echo $row_details->camp_details_id; ?>" />
                                                            <select class="filter_by_region" name="region_id">
                                                                <option value="">Filter By Region</option>
                                                                <?php foreach($regions as $r):?>
                                                                <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td class="add_products">
                                                            <input type="hidden" class="product_id" name="product_id[]" value="<?php echo $row_details->camp_prod_id?>" />
                                                            <input type="text" class="trigger_product_list" placeholder="Select a Product" value="<?php echo $prod->prod_name?>" />
                                                            <ul class="place-holder"></ul>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="voucher_qty" name="voucher_qty[]" placeholder="Voucher Quantity" value="<?php echo $row_details->camp_voucher_qty ?>" />
                                                        </td>
                                                        <td>
                                                            <a href="/adminControl/campaign_by_promo/remove_cam_details/<?php echo $row_details->camp_details_id; ?>" class="remove_row" data-id="<?php echo $row_details->camp_details_id; ?>">Remove <i class="icon-remove"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                    <?php else:?>
                                                    <tr class="add-product">
                                                        <td>
                                                            <select class="filter_by_region" name="region_id">
                                                                <option value="">Filter By Region</option>
                                                                <?php foreach($regions as $r):?>
                                                                <option value="<?php echo $r->region_id?>"><?php echo $r->region_name?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td class="add_products">
                                                            <input type="hidden" class="product_id" name="new_product_id[]" />
                                                            <input type="text" class="trigger_product_list" placeholder="Select a Product" />
                                                            <ul class="place-holder"></ul>
                                                        </td>
                                                        <td>
                                                            <input type="text" name="new_voucher_qty[]" placeholder="Voucher Quantity" />
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="remove_row">Remove <i class="icon-remove"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php endif;?>
											</table>
											<table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
											<tr>
												<td colspan="4"><hr size="1" style="padding:0px;margin:0px;" /></td>
											</tr>											
											<tr>
												<td colspan="4" align="left">
													<?php
														$follow_up_checked="";
														$follow_up_campagin_type=$rec_campaign->campaign_by_promo_follow_up_campagin_type;														
														if($rec_campaign->campaign_by_promo_follow_up=='Y'){
															$follow_up_checked="checked";
														}
													?>
													<input type="checkbox" style="margin-top:-3px;" <?php echo $follow_up_checked;?> id="follow_up_campaign" name="follow_up_campaign" value="enable" /><span>&nbsp;Enable follow up campaign</span>
													<div id="follow_up_wrapper" style="background-color:#FDF7F5;padding:15px;border:solid 1px #ddd;display:<?php if($follow_up_checked!='checked') echo 'none';?>">
													<!--follow-up campaign-->
													<table id="camp_follow_up_table">
														<tr>
															<td colspan="4">
																<h5>Follow Up Campaign</h5>
																<div><span>*</span>Campaign Type</div>
																<div>
																	<select name="follow_up_campagin_type">
																		<option value="">Please Select</option>
																		<option <?php if($follow_up_campagin_type=='Consumable') echo ' selected '; ?> value="Consumable">Consumable</option>
																		<option <?php if($follow_up_campagin_type=='Assignable') echo ' selected '; ?> value="Assignable">Assignable</option>
																	</select>
																</div>
																<hr size="1" style="padding:0px;margin:0px;" />
															</td>
														</tr>
														<tr>
															<td><a href="javascript:void(0)" id="f_add_more_tr_edit" class="btn btn-info">Add more product <i class="icon-plus"></i></a></td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>* Product</td>
															<td>* Voucher Quantity</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<?php
														$res_details = $this->campaign_m->campaign_details_by_id_by_type($camp_id,'by_promo_follow_up');
														$num_rows = $res_details->num_rows();
														$i = 0;
														if ($num_rows > 0)
														{
															foreach ($res_details->result() as $row_details)
															{
																$tr_id = '';
																$tr_class = '';
																if ($i == 0) {
																	$tr_id = 'id="f_master_tr"';
																}
																?>
																<tr <?php echo $tr_id; ?>>
																	<td>
																		<input type="hidden" name="f_cam_details_id[]" value="<?php echo $row_details->camp_details_id; ?>" />
																		<select name="f_product_id[]" id="f_product_id_<?php echo $i; ?>">
																			<option value="">Please Choose Product</option>
																				<?php
																					echo $this->product_m->product_by_merchant_dropdown_campaign($row_details->camp_prod_id);
																				?>
																		</select></td>
																	<td><input type="text" value="<?php echo $row_details->camp_voucher_qty ?>" name="f_voucher_qty[]"></td>
																	<td>&nbsp;</td>
																			<?php
																			if ($i > 0) {
																				?>
																		<td><a href="javascript:void(0)" id="<?php echo $row_details->camp_details_id; ?>"  class="f_delete_tr">Remove <i class="icon-remove"></i></a></td>

																	<?php }
																	else echo "<td></td>"; ?>
																</tr>
																<?php
																$i++;
															}//for each
														}//if num rows
														?>
													</table>
													<!--follow-up campaign-->
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="4"><hr size="3" style="padding:0px;margin:0px;" /></td>
											</tr>
											<tr>
												<td colspan="4">
													<!-- rule -->
													<?php
														$rec_rule = $res_camp_rule->row();
													?>
													<h5>Campaign Rules</h5>
													<div class="row-fluid">
														Rule description : <input type="text" style="width:800px" name="rule_desc" class="span3" value="<?php echo $rec_campaign->campaign_by_promo_rule_desc; ?>" />
													</div>
													<div class="row-fluid">
														<div class="span3">
															Min Age : <input type="text" name="min_age" class="span3" value="<?php echo $rec_rule->min_age; ?>" /> </div>
														<div class="span3">
															Max Age: <input type="text" name="max_age" class="span3" value="<?php echo $rec_rule->max_age; ?>" /> </div>
														<div class="span3">
															Gender  : 
															<select name="gender" class="span3">
																<?php
																	$gender = $rec_rule->gender;																	
																?>
																<option value="Both" <?php echo ($gender == 'Both') ? 'selected="selected"' : ''; ?>>Both</option>
																<option value="Male" <?php echo ($gender == 'Male') ? 'selected="selected"' : ''; ?>>Male</option>
																<option value="Female" <?php echo ($gender == 'Female') ? 'selected="selected"' : ''; ?>>Female</option>
															</select>  
														</div>
														<br />
														<table width="100%" cellspacing="0" cellpadding="0" border="0">
															<tr>
																<td>All Available Regions</td>
																<td>Selected Regions</td>
															</tr>
															<tr>
																<td width="36%"> <select name="regions_id_all" id="regions_id_all" size="15" multiple class="span10">
																<?php echo $this->regions_m->recursive(); ?>
																	</select></td>
																<td width="64%">
                                                                <?php
                                                                $list = $rec_rule->region_id;
                                                                $res_own_region = $this->campaign_m->select_rule_region($list);
                                                                ?>
                                                            <select name="regions_id_selected[]" id="regions_id_selected" size="15" multiple class="span5">
                                                            <?php
                                                            if ($res_own_region->num_rows() > 0) {
                                                                foreach ($res_own_region->result() as $row) {
                                                                    echo "<option value='" . $row->region_id . "'>" . $row->region_name . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                            </select></td>
															</tr>
															<tr>
																<td><span data-all="regions_id_all" data-selected="regions_id_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                                                                        <td><span  data-selected="regions_id_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
															</tr>
														</table>
													</div>
													<!-- rule -->
                                                    <?php include_once(_VIEW_INCLUDES_DIR_ADMIN_."while_label_rules.php");?>
												</td>
											</tr>
                                        </table>

                                        <input id ="campaign_submit_btn" class="btn btn-large btn-WI" type="submit" name="update_campaign" value="Save" >
                                        <button class="btn btn-large" type="button" name="editCat" onclick="Javascript:location.href = '<?php echo site_url() . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2); ?>'"><?php echo "Cancel"; ?></button>
                                    </div>
                                    <?php echo form_close(); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">
                <script type="text/javascript" src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
                <?php include("includes/footer.php"); ?>
                <!--<script src="custom/lib/bootstrap/js/bootstrap.js"></script> -->
                <script>
                    $('.close_campaign').click(function(e){                        
                        e.preventDefault();
                        var elem = this;
                        
                        var GenericDialog = $('<div id="generic-dialog" >Are you sure you want to close campaign?</div>');
                        GenericDialog.dialog({
                            autoOpen: true,
                            dialogClass: 'noTitleStuff', 
                            resizable: false,
                            modal: true,
                            buttons: [{
                                    text: "Yes",
                                    click: function() {
                                        return window.location = $(elem).attr('href');
                                    }
                                },
                                {
                                    text: "No",
                                    click: function() {
                                        $(this).dialog('close');
                                        $(this).dialog('destroy');
                                    }
                                }
                            ]
                        });
                    });
                    
                    $('#campaign_submit_btn').click(function(e) {
                        e.preventDefault();
                        $('#regions_id_selected option').prop('selected', 'selected');
                        
                        $('#whitelabel_id_selected option').prop('selected', 'selected');
                        $('#whitelabel_actions_selected option').prop('selected', 'selected');
                        $('#whitelabel_merchants_selected option').prop('selected', 'selected');
                        $('#whitelabel_products_selected option').prop('selected', 'selected');
                        
                        $.ajax({
                            url: $('#campaign_form').attr('action'),
                            data: $('#campaign_form').serialize(),
                            dataType: 'json',
                            method: 'post',
                            success: function(r) {
                                if( r.error == '' ) {
                                    return window.location = r.redirect;
                                } else {
                                    $('#error_list').html('');
                                    $.each(r.error, function(i) {
                                        $('#error_list').append('<p>' + r.error[i] + '</p>');
                                        $('#error_list').show();
                                    });
                                }   
                            }
                        });
                        return;
                    });
                </script>
    <style type="text/css">
        .place-holder {
            display:none;position:absolute;height:200px;min-width:400px;overflow:auto;background:#fff;font-size:0.9em;padding:0;margin:0;
        }
        .prod_info {
            cursor:pointer;
        }
        .prod_info:hover {
            background:#a5d7eb;
        }
    </style>
    <script src="/assets/backend/js/campaign.js"></script>
    </body>
</html>



