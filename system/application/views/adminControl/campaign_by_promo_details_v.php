<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?>:: Glomp</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <script src="assets/backend/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/backend/lib/loader.js" type="text/javascript"></script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            $(document).ready(function(e) {
                $('#toogle_voucher_details').click(function(e) {

                    $('#voucher_details_table').collapse('toggle');
                });
				$('#toogle_voucher_details_f').click(function(e) {

                    $('#voucher_details_table_f').collapse('toggle');
                });
                $('#voucher_details_table').collapse('hide');
                
                $('#toogle_campaign_rule').click(function(e) {

                    $('#cmapign_rule_container').collapse('toggle');
                });
                $('#cmapign_rule_container').collapse('show');
                
                $("#remove_selected").click(function() {
                    $("#regions_id_selected > option:selected").each(function() {
                        $(this).remove();
                    });
                });
                
                $('#button_transfer').click(function(e) {
                    $("#regions_id_all > option:selected").each(function() {

                        var t = $(this).text();
                        var v = $(this).val();
                        var str = '<option value="' + v + '" selected="selected">' + t + '</option>';
                        /**************/
                        var exists = false;
                        $('#regions_id_selected option').each(function() {
                            var nt = $(this).val();

                            if (v == this.value)
                                exists = true;
                        });
                        /***************/
                        if (!exists)
                            $('#regions_id_selected').append(str);
                    });
                });
                

            });
            function make_all_seletect()
            {
                $('#regions_id_selected option').prop('selected', 'selected');
            }
            $(document).ready(function(e) {
                $('#btn_execute_rule').click(function(e) {
                    var camp_id = $('#camp_id').val();
                    $('#rule_result_display').html('Please wait...');

                    
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/rule_result'); ?>';
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + camp_id,
                        success: function(html) {
                            $('#rule_result_display').html('Number of People Found : ' + html);
                        }
                    });
                    
                });
                /*execute rule*/
                $('#btn_execute_rule_confirm').click(function(e) {
                    if (!confirm('Are you sure you want to generate voucher?\n Note: You will not be able to change the voucher details once you generate'))
                    {
                        return false;
                    }
                    $('#rule_execution_display').html('Please wait...voucher is generating...');
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/generate_voucher'); ?>';
                    var camp_id = $('#camp_id').val();
                    $.ajax({
                        type: 'GET',
                        url: base_url + "/" + camp_id,
                        success: function(html) {
                            $('#rule_execution_display').html(html);
                            location.href = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/details/'); ?>' + '/' + camp_id;
                        }
                    });
                });
                
                $('#run_campaign').click(function(e) {
                    if (!confirm('Are you sure you want to run this campaign?'))
                    {
                        return false;
                    }

                    $('#display_loading').html('Please wait...');
                    $.fn.loadLoader();
                    var camp_id = $(this).data('campaign_id');
                    var base_url = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/run_campaign'); ?>';
                    window.location = '<?php echo base_url(ADMIN_FOLDER . '/campaign_by_promo/run_campaign'); ?>/' + camp_id;
                    /*$.ajax({
                     type:'GET',
                     url:base_url+'/'+camp_id,
                     success: function(html){
                     alert(html);
                     $('#display_loading').html(html);
                     hook = false;
                     $.fn.removeLoadLoader();
                     }
                     });*/
                });
            });

        </script>
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="page-heading"><?php echo $page_title; ?></div>
                                </div>
                                <div class="span4" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_by_promo/AddCampaign", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                    <?php echo anchor(ADMIN_FOLDER . "/campaign_by_promo", "View All", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                        </div>

                     

                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Information</h5></div>
                            <div class="widget-content">
                                <div class="row-fluid">
                                    <div class="span12 account">
										<?php											
											$start_date=$rec_camp->campaign_by_promo_start_date;															
											$expiry_date=$rec_camp->campaign_by_promo_end_date;															
										?>
                                        Name:<span class="label label-success"><?php echo $rec_camp->campaign_name; ?> </span><br>
										Promo Code:<span class="label label-success"><?php echo $rec_camp->campaign_by_promo_code; ?> </span><br>
										Type:<span class="label label-success"> <?php echo $rec_camp->campaign_type; ?></span><br> 
										Expiry Day :<span class="label label-success"> <?php echo $rec_camp->campaign_voucher_expiry_day; ?> </span><br> 
										Campaign For :<span class="label label-success"> <?php echo $rec_camp->campaign_by_promo_for; ?> </span><br> 
										Enable time limit :<span class="label label-success"> <?php echo $rec_camp->campaign_by_promo_enable_time_limit; ?> </span><br> 
										Campaign Start Date :<span class="label label-success"> <?php echo date('m/d/Y',strtotime($start_date)); ?> </span><br> 
										Campaign End Date :<span class="label label-success"> <?php echo date('m/d/Y',strtotime($expiry_date)); ?> </span><br>
										Enable follow up campaign :<span class="label label-success"> <?php echo $rec_camp->campaign_by_promo_follow_up; ?> </span><br> 
										Follow up campaign type:<span class="label label-success"> <?php echo $rec_camp->campaign_by_promo_follow_up_campagin_type; ?> </span><br> 
										Last Execution Date:<span class="label label-success"><?php echo ($rec_camp->campaign_last_executed_date == '') ? 'Never' : $rec_camp->campaign_last_executed_date; ?></span><br>
										Rule Description:<span class="label label-success"> <?php echo $rec_camp->campaign_by_promo_rule_desc; ?> </span><br> 
										
<?php

										$res_voucher = $this->campaign_m->campaign_voucher($camp_id);
                                        $num_rows = $res_voucher->num_rows();
                                        $used_voucher = $num_rows;//$total_voucher
										
										$total_voucher = $this->campaign_m->get_total_campaign_voucher_by_promo($camp_id);
                                        

                                        $available_voucher = $total_voucher-$used_voucher;

?>
                                        Available Voucher: <span class="label label-success"><?php echo $available_voucher; ?> </span>
                                        <br/><br/>&nbsp;&nbsp;

                                    </div>                                       
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Voucher Details (Primary) <a href="javascript:void(0)" id="toogle_voucher_details" >Show/Hide</a></h5></div>
                            <div id="voucher_details_table">
                                <div class="widget-content" >
                                    <div class="row-fluid">
                                        <div class="span12 account">

                                            <table width="100%"  class="table table-striped table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>Merchant</td>
                                                        <td>Product</td>
                                                        <td>Image</td>
                                                        <td>Total Voucher</td>
                                                        <td>Remaining Voucher</td>
                                                        <td>Total Point</td>
                                                    </tr>
<?php
$num = $res_camp_details->num_rows();
if ($num > 0) {
    $camp_type = $rec_camp->campaign_type;
    $total_voucher = 0;
    $total_point = 0;
    $remaining = 0;
    foreach ($res_camp_details->result() as $row_deails) {
        $img = $this->custom_func->product_logo($row_deails->prod_image);
        $point = $row_deails->camp_voucher_qty * $row_deails->camp_prod_point;
        $total_point += $point;
        ?>
                                                            <tr>
                                                                <td><?php echo $row_deails->merchant_name; ?></td>
                                                                <td> <?php echo $row_deails->prod_name; ?></td>
                                                                <td><img src="<?php echo $img; ?>" title="<?php echo $row_deails->prod_name; ?>" alt="<?php echo $row_deails->prod_name; ?>" width="100" />
                                                            <?php
                                                            $res_remaining = $this->campaign_m->voucherByCampAndProduct($row_deails->camp_campaign_id, $row_deails->camp_prod_id,'','by_promo_primary');
                                                            $remaining_num = $res_remaining->num_rows();
                                                            ?>
                                                                </td>
                                                                <td><?php
                                                            $prod_voucher = $row_deails->camp_voucher_qty;
                                                            if ($camp_type == 'Both') {
                                                                $prod_voucher = $prod_voucher * 2;
                                                            }
                                                            //
                                                            $total_voucher +=$prod_voucher;
                                                            echo $prod_voucher;
                                                            ?></td>
                                                                <td><?php
                                                                    echo $rem = ($prod_voucher - $remaining_num);
                                                                    $remaining +=$rem;
                                                                    ?></td>
                                                                <td><?php echo $point; ?></td>
                                                            </tr>
                                                                    <?php }
                                                                ?>
                                                        <tr>
                                                            <td><strong>Total</strong></td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td><strong><?php echo $total_voucher; ?></strong></td>
                                                            <td><strong><?php echo $remaining; ?></strong></td>
                                                            <td><strong><?php echo $total_point; ?></strong></td>
                                                        </tr>
    <?php
}
?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

						
						<?php
							if($rec_camp->campaign_by_promo_follow_up=='Y')							
							{
						?>

						<div class="widget-box">
                            <div class="widget-title"><h5><i class="icon-briefcase"></i> Campaign Voucher Details (Follow up) <a href="javascript:void(0)" id="toogle_voucher_details_f" >Show/Hide</a></h5></div>
                            <div id="voucher_details_table_f">
                                <div class="widget-content" >
                                    <div class="row-fluid">
                                        <div class="span12 account">

                                            <table width="100%"  class="table table-striped table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>Merchant</td>
                                                        <td>Product</td>
                                                        <td>Image</td>
                                                        <td>Total Voucher</td>
                                                        <td>Remaining Voucher</td>
                                                        <td>Total Point</td>
                                                    </tr>
<?php
$num = $res_camp_details_f->num_rows();
if ($num > 0) {
    $camp_type = $rec_camp->campaign_type;
    $total_voucher = 0;
    $total_point = 0;
    $remaining = 0;
    foreach ($res_camp_details_f->result() as $row_deails) {
        $img = $this->custom_func->product_logo($row_deails->prod_image);
        $point = $row_deails->camp_voucher_qty * $row_deails->camp_prod_point;
        $total_point += $point;
        ?>
                                                            <tr>
                                                                <td><?php echo $row_deails->merchant_name; ?></td>
                                                                <td> <?php echo $row_deails->prod_name; ?></td>
                                                                <td><img src="<?php echo $img; ?>" title="<?php echo $row_deails->prod_name; ?>" alt="<?php echo $row_deails->prod_name; ?>" width="100" />
                                                            <?php
                                                            $res_remaining = $this->campaign_m->voucherByCampAndProduct($row_deails->camp_campaign_id, $row_deails->camp_prod_id,'','by_promo_follow_up');
                                                            $remaining_num = $res_remaining->num_rows();
                                                            ?>
                                                                </td>
                                                                <td><?php
                                                            $prod_voucher = $row_deails->camp_voucher_qty;
                                                            if ($camp_type == 'Both') {
                                                                $prod_voucher = $prod_voucher * 2;
                                                            }
                                                            //
                                                            $total_voucher +=$prod_voucher;
                                                            echo $prod_voucher;
                                                            ?></td>
                                                                <td><?php
                                                                    echo $rem = ($prod_voucher - $remaining_num);
                                                                    $remaining +=$rem;
                                                                    ?></td>
                                                                <td><?php echo $point; ?></td>
                                                            </tr>
                                                                    <?php }
                                                                ?>
                                                        <tr>
                                                            <td><strong>Total</strong></td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td><strong><?php echo $total_voucher; ?></strong></td>
                                                            <td><strong><?php echo $remaining; ?></strong></td>
                                                            <td><strong><?php echo $total_point; ?></strong></td>
                                                        </tr>
    <?php
}
?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>						

                      
						<?php						
						}
						?>


                    </div>
                </div>
            </div>
<?php include("includes/footer.php"); ?>
            <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
    </body>
</html>


