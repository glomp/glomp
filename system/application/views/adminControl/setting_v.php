<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="assets/backend/html5.js"></script>
    <![endif]-->
</head>
<body class="">
    <?php include("includes/header.php"); ?>
    <div class="content">
    <?php include("includes/main-menu.php");?>  
        <div class="container-fluid dashboard">
            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom:10px;">
                        <div class="row-fluid">
                            <div class="span8">
                                <div class="page-heading"><?php echo $page_title;?></div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="tabs" class="ui-tabs">
                        <ul class="ui-tabs-nav ui-helper-clearfix">
                            <li class="ui-state-default ui-state-active ui-tabs-active"><a href="#tab-1" class="ui-tabs-anchor" style="color:#0099FF">Main</a></li>
                            <li class="ui-state-default"><a href="#tab-2" class="ui-tabs-anchor" style="color:#0099FF">Age Limits</a></li>
                        </ul>
                        <div id="tab-1">
                            <form method="post" action="<?php echo ADMIN_FOLDER."/setting"?>" name="frmadmin" id="frmadmin">
                                <div>
                                <?php
                                if(isset($msg))
                                {
                                    echo "<div class=\"success_msg\">".$msg."</div>";
                                }
                                if(isset($error_msg))
                                {
                                    echo "<div class=\"alert alert-error\">".$error_msg."</div>";
                                }
                                if(validation_errors()!="")
                                {
                                    echo "<div class=\"custom-error\">".validation_errors()."</div>";
                                }
                                ?>
                                </div>

                                <div class="content-box" style="margin-top:0;">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <table cellpadding="5" width="100%"   cellspacing="5" border="0">
                                                <tr>
                                                    <td width="22%"><strong>Name</strong></td>
                                                    <td width="34%"><strong>Value</strong></td>
                                                    <td width="44%"><strong>Description</strong></td>
                                                </tr>
                                                <?php
                                                foreach($res_all->result() as $row){
                                                ?>
                                                <tr>
                                                    <td valign="top"><?php echo $row->config_name;?></td>
                                                    <td valign="top">
                                                    <?php
                                                    if($row->config_input_type != 'textarea'){
                                                    ?>
                                                        <input type="text" name="<?php echo $row->config_key;?>" value="<?php echo $row->config_value;?>" /><?php 
                                                    }else{
                                                    ?>
                                                        <textarea rows="3" cols="20" name="<?php echo $row->config_key;?>"><?php echo $row->config_value;?></textarea>
                                                    <?php }?>
                                                    </td>
                                                    <td><?php echo $row->config_desc;?></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                <input class="btn btn-large btn-WI" type="submit" name="Update" value="Update" >
                                <button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                                </div>
                            </form>
                        </div>
                        <div id="tab-2">
                            <div class="content-box row-fluid" style="margin-top:0;">
                                
                                <div class="span7">
                                    <p><em>This settings hides selected categories and it's items throughout the website from under-age users.</em></p>
                                    <p><em>If a region/location is not listed, the age limit defaults to 18.</em></p>
                                    <p><em>Use the form on the right to create a limit.</em></p>
                                    <table class="table" id="tbl_age_limits">
                                        <thead>
                                            <tr>
                                                <td style="font-weight:bold">ID</td>
                                                <td style="font-weight:bold">Location/Region</td>
                                                <td style="font-weight:bold">Age Limit</td>
                                                <td style="font-weight:bold">Delete</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($age_limits):?>
                                            <?php foreach($age_limits as $x):?>
                                            <tr data-id="<?php echo $x->id;?>">
                                                <td><?php echo $x->id;?></td>
                                                <td><?php echo $x->region_name;?></td>
                                                <td><?php echo $x->age_limit;?></td>
                                                <td><a href="<?php echo ADMIN_FOLDER.'/setting/delete_age_limit/'.$x->id;?>" class="btn_delete" data-id="<?php echo $x->id;?>" title="Delete"><i class="icon-trash"></i></a></td>
                                            </tr>
                                            <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                    
                                    
                                </div>
                                <div class="span1"></div>
                                <div class="span4">
                                    <?php if($categories):?>
                                    <strong>Select Category</strong>
                                    <form method="post" action="<?php echo ADMIN_FOLDER.'/setting/add_category_to_limit'?>" id="frm_add_category_to_limit">
                                        <ul class="row-fluid" style="margin:0;">
                                        <?php foreach( $categories as $c):?>
                                            <li class="span4" style="margin-left:0;text-align:left">
                                                <label for="cat_<?php echo $c->cat_id;?>">
                                                    <input type="checkbox" name="category_id[]" value="<?php echo $c->cat_id?>" id="cat_<?php echo $c->cat_id;?>" class="category_id" style="margin:0" <?php echo ($c->is_selected)?'checked="checked"':'';?> />
                                                &nbsp;<?php echo $c->cat_name;?></label>
                                            </li>
                                        <?php endforeach;?>
                                        </ul>
                                    </form>
                                    <?php endif;?>
                                    <strong>Add Limit By Region</strong>
                                    <form method="post" action="<?php echo ADMIN_FOLDER.'/setting/add_age_limit'?>" id="frm_add_age_limit">
                                        <input type="hidden" name="region_id" id="region_id" />
                                        <input type="text" name="region_name" id="region_name" placeholder="Select Region" class="span7" style="margin:0" />&nbsp;
                                        <input type="text" name="age_limit" id="age_limit" placeholder="Age" class="span2" style="margin:0" />&nbsp;
                                        <input type="submit" value="Add" class="btn btn-WI" />
                                        <div id="region_search_result"></div>
                                    </form>
                                    <div id="form_submit_message"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <?php include("includes/footer.php");?>
    <script type="text/javascript" src="assets/backend/lib/jquery.form.min.js"></script>
    <script type="text/javascript">
    jQuery(window).load(function(){
        var tabsection = jQuery('#tabs');
        var tabmenu = tabsection.find('ul:first-child');
        var tabcontent = tabsection.find('> div');
        tabmenu
            .find('li a').each(function(i,o){
            var id = jQuery(o).attr('href');
            jQuery(o).css('cursor','pointer');
            jQuery(id).hide();
        }).on('click',function(e){
            e.preventDefault();
            var id = jQuery(this).attr('href');
            jQuery(id).show();
            jQuery('#tabs > div:not('+id+')').hide();
            jQuery(this).parent().addClass('ui-state-active ui-tabs-active')
                .siblings().removeClass('ui-state-active ui-tabs-active');
        });
        tabmenu.find('li:first-child a')
            .trigger('click');
        
        
        jQuery('#frm_add_age_limit').ajaxForm({
            'dataType' : 'json',
            'beforeSubmit' : function() {
                jQuery('#form_submit_message')
                    .html('')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error');
            },
            'success' : function( response ) {
                var form = jQuery('#frm_add_age_limit');
                if(response.status == 'success') {
                    
                    var new_record = '';
                    new_record += '<tr data-id="'+response.record.id+'">';
                    new_record += '<td>'+response.record.id+'</td>';
                    new_record += '<td>'+response.record.region_name+'</td>';
                    new_record += '<td>'+form.find('[name=age_limit]').val()+'</td>';
                    new_record += '<td><a href="/adminControl/setting/delete_age_limit/'+response.record.id+'" class="btn_delete" data-id="'+response.record.id+'" title="Delete"><i class="icon-trash"></i></a></td>';
                    new_record += '</tr>';
                    
                    jQuery('#tbl_age_limits tbody').prepend(new_record);
                    
                    
                    jQuery('#form_submit_message').html(response.message)
                        .addClass('ui-state-highlight')
                        .css('padding','5px');
                    form.resetForm();
                }
                if(response.status == 'failed') {
                    jQuery('#form_submit_message').html(response.message)
                        .addClass('ui-state-error')
                        .css('padding','5px');
                    
                }
            }
        });
        
        jQuery(document).on('click','.btn_delete',function(e){
            e.preventDefault();
            jQuery.ajax({
                dataType : 'json',
                url : jQuery(this).attr('href'),
                success : function(response){
                    if(response.status == 'success') {
                        console.log(response.status);
                        /*jQuery('#tbl_age_limits tbody tr[data-id='+jQuery(this).attr('data-id')+']').hide();*/
                        jQuery(e.target).parents('tr').fadeOut(300);
                    }
                    
                    if(response.status == 'failed') {
                    
                    }
                }
            });
        });
        
        
        jQuery('#region_name').on('keyup',function(e){
            var result = jQuery('#region_search_result');
            if(e.target.value.length < 2) {
                result.html('');
                return;
            }
            jQuery.ajax({
                dataType: 'json',
                url: 'adminControl/setting/search_region',
                type: 'post',
                data: {'key':e.target.value},
                success: function( response ) {
                    
                    if( response.length ) {
                        var str = '<ul>';
                        jQuery.each(response,function(i,o){
                            console.log(o);
                            str += '<li><a href="javascript:void(0)" data-id="'+o.region_id+'" data-name="'+o.region_name+'" class="region_row">'+o.region_name+'</li>';
                        });
                        str += '</ul>';
                        result.html(str);
                    }
                }
            });
        });
        
        jQuery(document).on('click','.region_row',function(e){
            e.preventDefault();
            jQuery('#region_id').val( jQuery(this).attr('data-id') );
            jQuery('#region_name').val( jQuery(this).attr('data-name') );
            jQuery('#region_search_result').html('');
        });
        
        jQuery(document).on('change','.category_id',function(e){
            var form = jQuery('#frm_add_category_to_limit');
            jQuery.ajax({
                dataType: 'json',
                url: form.attr('action'),
                data: form.serialize(),
                type: 'post',
                success: function( response ) {
                    console.log(response);
                }
            });
        });
    });
    </script>
    <style type="text/css">
        #region_search_result ul {
            padding:0;
            margin:0;
        }
        #region_search_result ul li {
            float:none;
            text-align:left;
            margin:0 0 1px;
        }
        #region_search_result ul li:hover {
            background:#eee;
        }
        #region_search_result ul li a.region_row {
            display:block;
            padding:2px;
        }
    </style>
</body>
</html>


