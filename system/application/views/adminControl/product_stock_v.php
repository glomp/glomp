<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?></title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <script src="assets/backend/lib/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/css/datepicker.css">
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid  dashboard">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="page-heading">Manage Products Stocks for (<?php echo $product->prod_name; ?>)</div>
                                </div>
                                <div class="span6" style="text-align:right;">
                                    <?php echo anchor(ADMIN_FOLDER . "/product/", "View All Products", "class='btn-WI btn ui-state-default ui-corner-all'") ?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12" >
                                    <form name="form1" method="post" action="<?php echo base_url(ADMIN_FOLDER . "/product/addStock/". $prod_id); ?>">
                                    <div class="span6">
                                        <table cellpadding="5" cellspacing="5" id="camp_voucher_table"  border="0" width="100%">
                                            <tr>
                                                <td>Set New Qty.</td>
                                                <td>Remarks</td>
                                                <td></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" name="current_qty" value=""></td>
                                                <td><input type="text" name="details" value=""></td>
                                                <td> </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <input class="btn btn-WI" type="submit" name="update_stock_btn" value="Update Stock" >
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="page-subtitle"><?php
                            echo "History";
                            if (isset($msg)) {
                                echo "<div class=\"success_msg\">" . $msg . "</div>";
                            }
                            if (isset($error_msg)) {
                                echo "<div class=\"custom-error\">" . $error_msg . "</div>";
                            }
                            if (validation_errors() != "") {
                                echo "<div class=\"custom-error\">" . validation_errors() . "</div>";
                            }
                            ?>
                        </div>
                        <table width="100%" class="table table-hover">
                            <tr>
                                <td><strong>SN.</strong></td>
                                <td><strong>Current Stock</strong></td>
                                <td><strong>Stock (+/-)</strong></td>
                                <td><strong>Details</strong></td>
                                <td><strong>Date</strong></td>
                            </tr>
                            <?php
                            if (count($res_product) > 0) {
                                $i = 1;
                                foreach ($res_product as $rec_product) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i;$i++; ?></td>
                                        <td><?php echo $rec_product['current_qty']; ?></td>
                                        <td><?php echo $rec_product['change_qty']; ?></td>
                                        <td><?php echo $rec_product['details']; ?></td>
                                        <td><?php echo $rec_product['timestamp']; ?></td>
                                    </tr>
                                    <?php
                                }
                                echo '<tr><td colspan="5">' . $this->pagination->create_links() . '</td></tr>';
                            }
                            else
                                echo'<tr><td colspan="5">No records found.</td></tr>';
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
        <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
    </body>
</html>