<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
  </head>
  <body class="">
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid dashboard">
    	<div class="row-fluid">
            <div class="span12">
            <div class="page-header">
            	<div class="row-fluid">
                	<div class="span8">
                  		<div class="page-heading"><?php echo $page_title;?></div>
                  	</div>
                  	<div class="span4" style="text-align:right;">
						<?php echo anchor(ADMIN_FOLDER."/categories/addCategories", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
						<?php echo anchor(ADMIN_FOLDER."/categories", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  	</div>
                  </div>
             </div>
             
                <div class="page-subtitle">
                    <?php
                        echo $page_subtitle;
                        if(isset($msg))
                        {
                             echo "<div class=\"success_msg\">".$msg."</div>";
                        }
                        if(isset($error_msg))
                        {
                             echo "<div class=\"alert alert-error\">".$error_msg."</div>";
                        }
                        if(validation_errors()!="")
                        {
                            echo "<div class=\"custom-error\">".validation_errors()."</div>";
                        }
                    ?>
                 </div>
            <?php if($page_action=="view") { ?>
            <table class="table table-condensed table-hover">
            	<tr>
                	<td><strong>SN.</strong></td>
                  	<td><strong>Added Date</strong></td>
                  	<td><strong>Category Name</strong></td>
                  	<td style="text-align:center"><strong>Display Order</strong></td>
                  	<td style="text-align:center;"><strong>Status</strong></td>
                  	<td style="text-align:center;"><strong>Options</strong></td>
                </tr>
				<?php 
					if(isset($res_categories)) { 
				  	$i=1;
				  	echo form_open(admin_url("categories/UpdateDisplayOrder"),'name="frmCms" id="category"');
				  	foreach($res_categories as $rec_categories) {
						$p = $rec_categories->cat_permanent;
				  ?>
                <tr>
                	<td><?php echo $i; $i++; ?></td>
                    <td><?php echo $rec_categories->cat_added; ?></td>
                    <td><?php echo $rec_categories->cat_name; ?></td>
                    <td style="text-align:center">
                  		<input style="text-align:center;" type="text" class="input-mini" name="disp_order[]" value="<?php echo $rec_categories->cat_display_order; ?>">
                  		<input name="cat_id[]" type="hidden" id="cat_id[]" value="<?php echo $rec_categories->cat_id; ?>" />
                  	</td>
                  	<td style="text-align:center;">
                  		<?php
						if($p != 'Y'){
						 if($rec_categories->cat_status=='Active'){?>
				  		<?php echo anchor(ADMIN_FOLDER."/categories/publish/".$rec_categories->cat_id."/N/", '<img src="assets/images/icons/active.png" alt="Active" border="0" title="Active" />');?>					
                  		<?php }else{?>
                  		<?php echo anchor(ADMIN_FOLDER."/categories/publish/".$rec_categories->cat_id."/Y/", '<img src="assets/images/icons/inactive.png" alt="InActive" border="0" title="Inactive" />');?>
                  		<?php }
						}else echo '<img src="assets/images/icons/active.png" alt="Active" border="0" title="Active" />';
						?>
                  	</td>
                  	<td style="text-align:center;">
                   		<?php
						if($p != 'Y'){
							echo anchor(ADMIN_FOLDER."/categories/editCategories/".$rec_categories->cat_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
							echo "&nbsp;&nbsp;&nbsp;";
							echo anchor(ADMIN_FOLDER."/categories/deleteCategories/".$rec_categories->cat_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");
						}else{
							echo "Permanent Category";
							}
					?>
                  	</td>
                  </tr>
                  <?php }
				  echo '<tr>
				  <td colspan="6">
					  <input class="btn btn-WI" style="height:40px;" type="submit" name="updateDispOrder" value="Update Display Order" >
					  </td>
				  </tr>';
				  echo form_close();
				  echo '<tr><td colspan="6">'.$links.'</td></tr>';
				  } 
				  else 
				  	echo'<tr><td colspan="6">No records found.</td></tr>'; ?>
                  </table>
                  <?php } elseif($page_action=="add") { ?>
                  <?php echo form_open(ADMIN_FOLDER."/categories/addCategories",'name="frmCms" id="cmspages"')?>
                  <div class="content-box">
                  		<div class="row-fluid">
                        	<div class="span12">
                                <div class="control-group">
                                        <label class="control-label" for="add"><span>*</span>Category Name</label>
                                        <div class="controls">
                                            <input type="text" name="cat_name" value="<?php echo set_value('cat_name'); ?>">
                                        </div>
                                </div>
						        <div class="control-group">
                                     <label class="control-label" for="add">Display Order</label>
                                     <div class="control">
                                        <input type="text" name="display_order">
                                     </div>
                                </div>
                         	</div>
                      	</div>
                  		<input class="btn btn-large btn-WI" type="submit" name="add" value="Add" >
                  		<button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } elseif($page_action=="edit")
				  { 
				  ?>
                  <?php echo form_open(ADMIN_FOLDER."/categories/editCategories/".$res_categories->cat_id,'name="frmCms" id="cmspages"')?>
                  <div class="content-box">
                  		<div class="row-fluid">
                        	<div class="span12">
                 	 
                             <div class="control-group">
                                    <label class="control-label" for="add"><span>*</span>Category Name:</label>
                                    <div class="controls">
                                        <input type="text" name="cat_name" value="<?php echo $res_categories->cat_name; ?>">
                                    </div>
                             </div>
                            
					   	 <div class="control-group">
                             	<label class="control-label" for="add">Display Order</label>
                                <div class="controls">
                                	<input type="text" name="display_order" value="<?php echo $res_categories->cat_display_order;?>">
                                </div>
                             </div>
                             
                        	</div>
                        </div>
                  		<input class="btn btn-large btn-WI" type="submit" name="edit" value="Save" >
                  		<button class="btn btn-large" type="button" name="editCat" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     	<?php include("includes/footer.php");?>
   		<script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


