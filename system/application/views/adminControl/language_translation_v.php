<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <script src="assets/backend/lib/bootstrap/js/bootstrap.js"></script> 
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">	
	
	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements 
	<link href="assets/frontend/css/table/style.css" rel="stylesheet">	
	<script src="assets/frontend/js/__jquery.tablesorter.min.js" type="text/javascript"></script>
	<script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
	<link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.medium.css" rel="stylesheet">
	-->
    <!--[if lt IE 9]>
      <script src="assests/backend/html5.js"></script>
    <![endif]-->
	
	<script src="assets/frontend/js/jquery-1.9.1.js" type="text/javascript"></script> 
		<link href="assets/frontend/css/table/style.css" rel="stylesheet">
		<script src="assets/frontend/js/__jquery.tablesorter.min.js" type="text/javascript"></script>		
		 
		 <script src="assets/frontend/js/custom.language.translation.js" type="text/javascript"></script>
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  
  <body class=""> 
  <!--<![endif]-->
    
  <?php include("includes/header.php"); ?>


<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
		<div class="page-header">
			<div class="row-fluid">
				<div class="span10" style="float:left;width:30%">
					<div class="page-heading" >Manage Language Translations</div>
				</div>
				<div class="span2" style="text-align:right;float:right;width:67%">
					<a class="btn-WI btn ui-state-default ui-corner-all" href="javascript:void(0)" id="addNewTerm">Add New Term</a>
					<?php echo anchor(ADMIN_FOLDER."/language_translation/", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
					<a class="btn-WI btn ui-state-default ui-corner-all" href="javascript:void(0)" id="generateFile">Generate File</a>
					<a class="btn-WI btn ui-state-default ui-corner-all" href="javascript:void(0)" id="updateAllSelected">Save/Update All Selected</a>
					<a class="btn-WI btn ui-state-default ui-corner-all" href="javascript:void(0)" id="deleteAllSelected">Delete All Selected</a>					
				</div>
			</div>
		</div>
		<div class="page-subtitle">
			<?php
				echo "View All";
				if(isset($msg))
				{
				  echo "<div class=\"success_msg\">".$msg."</div>";
				}
				if(isset($error_msg))
				{
				  echo "<div class=\"custom-error\">".$error_msg."</div>";
				}
				if(validation_errors()!="")
				{
				  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				}
			?>
			<div style="float:right; margin-top:-3px;color:#000;font-size:14px">
				Language (Column 1)&nbsp;<select id="lang_col_1">
				</select>
				Language (Column 2)&nbsp;<select id="lang_col_2">
				</select>									
			</div>
			<div style="clear:both" id="notif_wrapper"></div>
		</div>		
		<table id="translationTable" class="tablesorter table-hover" style="clear:both"> 
			<thead> 				
				<th><b>ID</b></th>
				<th><input type="checkbox" /></th>
				<th><b>Term</b></th>
				<th><b>Description</b></th>
				<th id="lang_col_1_head" style="font-weight:bold"><b>English</b></th>
				<th id="lang_col_2_head" style="font-weight:bold"><b>French</b></th>
			</thead> 
			<tbody>  				
				
			</tbody> 
		</table>				
	</div>
</div>
     <?php include("includes/footer.php");?>   
  </body>
</html