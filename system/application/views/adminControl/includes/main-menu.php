<div id="main-menu">
    <div class="navbar">
        <div class="navbar-inner">
            <ul class="nav">
                <li><a href="<?php echo admin_url('dashboard'); ?>">Dashboard</a></li>
                <li><a href="<?php echo admin_url('categories'); ?>">Categories</a></li>
                <li><a href="<?php echo admin_url('cmspages'); ?>">CMS Pages</a></li>
                <li><a href="<?php echo admin_url('regions'); ?>">Regions</a></li>
                <li class="dropdown">							
                    <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Merchant <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo admin_url('merchant'); ?>">Manage</a></li>
                        <li><a href="<?php echo admin_url('merchant/reports'); ?>/">Reports</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Catalog <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo admin_url('product'); ?>">Product</a></li>
                        <li><a href="<?php echo admin_url('brands'); ?>">Brands</a></li>
                    </ul>
                </li>
                <li class="dropdown">							
                    <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Campaign <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo admin_url('campaign'); ?>/">Campaign by Rule</a></li>
                        <li><a href="<?php echo admin_url('campaign_by_promo'); ?>/">Campaign by Promo Code</a></li>
                        <li><a href="<?php echo admin_url('campaign_newsletter'); ?>">Campaign Newsletter</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo admin_url('user'); ?>">Users</a></li>
                <li><a href="<?php echo admin_url('admin'); ?>">Admin</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" >Email <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo admin_url('email'); ?>">Template</a></li>
                        <li><a href="<?php echo admin_url('email_logs'); ?>">Logs</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo admin_url('language_translation'); ?>">Language Translation</a></li>
                <li><a href="<?php echo admin_url('setting'); ?>">Setting</a></li>
            </ul>
        </div>
    </div>    
</div>