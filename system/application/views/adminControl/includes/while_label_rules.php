<!-- whitelabel rules-->
<div class="row-fluid">
    <div class="span6" style="background-color:#FFFF66;padding-left:20px;">
        <hr/>
        <h2><b>For White Label Campaign</b></h2>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>
                    <input class="check_rule" style="margin: 0px;" <?php echo $whitelabel_id_checked; ?> type="checkbox" name ="whitelabel_id_checked" value="yes">
                    Include White Labels
                </td>
                <td>Selected White Labels</td>
            </tr>
            <tr>    
                <td width="50%">
                    <select name="whitelabel_id_all" id="whitelabel_id_all" size="15" multiple class="span11">
                        <?php foreach ($white_label->all_list as $row)
                            {?>
                            <option value="<?php echo($row['alias']); ?>">
                                <?php echo($row['name']); ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td width="50%">
                    <select name="whitelabel_id_selected[]" id="whitelabel_id_selected" size="15" multiple class="span11">
                    <?php
                    if ($white_label->selected_list!='') {
                        foreach ($white_label->selected_list as $row) {
                            echo "<option value='" . $row. "'>" . $white_label->all_list[$row]['name'] . "</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><span data-all="whitelabel_id_all" data-selected="whitelabel_id_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                <td><span  data-selected="whitelabel_id_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="border-bottom:1px dashed #ddd;margin:10px 0;" ></div>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="check_rule" style="margin: 0px;" <?php echo $whitelabel_actions_checked; ?> type="checkbox" name ="whitelabel_actions_checked" value="yes">
                    Include Actions
                </td>
                <td>Selected Actions</td>
            </tr>
            <tr>
                <td width="36%">
                    <select name="whitelabel_actions_all" id="whitelabel_actions_all" size="15" multiple class="span11">
                        <?php foreach ($white_label->all_actions as $row)
                            {?>
                            <option value="<?php echo($row); ?>">
                                <?php echo($row); ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td width="64%">
                    <select name="whitelabel_actions_selected[]" id="whitelabel_actions_selected" size="15" multiple class="span11">
                    <?php
                    if ($white_label->selected_actions!='') {
                        foreach ($white_label->selected_actions as $row) {
                            echo "<option value='" . $row. "'>" . $row. "</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><span data-all="whitelabel_actions_all" data-selected="whitelabel_actions_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                <td><span  data-selected="whitelabel_actions_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="border-bottom:1px dashed #ddd;margin:10px 0;" ></div>
                </td>
            </tr>
            
            <!-- merchants -->
            <!-- merchants -->
            <!-- merchants -->
            <tr>
                <td>
                    <input class="check_rule" style="margin: 0px;" <?php echo $whitelabel_merchants_checked; ?> type="checkbox" name ="whitelabel_merchants_checked" value="yes">
                    Include Merchants
                </td>
                <td>Selected Merchants</td>
            </tr>
            <tr>
                <td width="36%">
                    <select name="whitelabel_merchants_all" id="whitelabel_merchants_all" size="15" multiple class="span11">
                        <?php foreach ($white_label->all_merchants->result() as $row)
                            { ////white_label_merchants
                                ?>
                            <option value="<?php echo($row->id); ?>">
                                <?php print_r($row->name); ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td width="64%">
                    <select name="whitelabel_merchants_selected[]" id="whitelabel_merchants_selected" size="15" multiple class="span11">
                    <?php
                    if ($white_label->selected_merchants!='') {
                        foreach ($white_label->selected_merchants as $row) {
                            echo "<option value='" . $row . "'>" . $this->brand_m->get_brandproduct($row)->name. "</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><span data-all="whitelabel_merchants_all" data-selected="whitelabel_merchants_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                <td><span  data-selected="whitelabel_merchants_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="border-bottom:1px dashed #ddd;margin:10px 0;" ></div>
                </td>
            </tr>
            <!-- merchants -->
            <!-- merchants -->
            <!-- merchants -->
          
            <!-- products -->
            <!-- products -->
            <!-- products -->
            <tr>
                <td>
                    <input class="check_rule" style="margin: 0px;" <?php echo $whitelabel_products_checked; ?> type="checkbox" name ="whitelabel_products_checked" value="yes">
                    Include Products
                </td>
                <td>Selected Products</td>
            </tr>
            <tr>
                <td width="36%">
                    <select name="whitelabel_products_all" id="whitelabel_products_all" size="15" multiple class="span11">
                        <?php foreach ($white_label->all_products->result() as $row)
                            { ////white_label_merchants
                                ?>
                            <option value="<?php echo($row->id); ?>">
                                [<?php echo ($row->name); ?>] <?php echo ($row->prod_name); ?>
                            </option>
                        <?php } ?>
                    </select>
                </td>
                <td width="64%">
                    <select name="whitelabel_products_selected[]" id="whitelabel_products_selected" size="15" multiple class="span11">
                    <?php
                    if ($white_label->selected_products!='') {
                        foreach ($white_label->selected_products as $row) {
                            
                             $brand_prduct_data = $this->brand_m->get_brandproduct_products($row);
                             $brand_merchant = $this->brand_m->get_brandproduct($brand_prduct_data->brandproduct_id)->name;
                             $prod_name = $this->product_m->selectProductByID($brand_prduct_data->product_id)->row()->prod_name;
                            echo "<option value='" . $row . "'>[".$brand_merchant."]" . $prod_name."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><span data-all="whitelabel_products_all" data-selected="whitelabel_products_selected" class="btn button_transfer" id="">Add Selected <i class="icon-plus"></i></span></td>
                <td><span  data-selected="whitelabel_products_selected" class="btn remove_selected" id="">Remove Selected <i class="icon-trash"></i></span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="border-bottom:1px dashed #ddd;margin:10px 0;" ></div>
                </td>
            </tr>
            <!-- products -->
            <!-- products -->
            <!-- products -->
        </table>
        <div class="row-fluid">
            <div class="span12" align="left">
                <input class="check_rule" style="margin: 0px;" <?php echo $date_from_checked; ?> type="checkbox" name ="date_from_checked" value="yes">
                &nbsp;Include Log Date
                <br>Log Date From:&nbsp;
                <input type="text" name="whitelabel_date_from" class="" value="<?php echo $white_label->date_from; ?>" id="date_from" /> 
                &nbsp;&nbsp;Log Date To:&nbsp;
               <input type="text" name="whitelabel_date_to" class="" value="<?php echo $white_label->date_to; ?>" id="date_to" />
            </div>
        </div>
    <hr/>
    </div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function(e){
    $( "#date_from" ).datepicker({
          maxDate: 0,
          onClose: function( selectedDate ) {
            $( "#date_to" ).datepicker( "option", "minDate", selectedDate );
          }
        })
        .on('change', function (e) {
            
        });
        $( "#date_to" ).datepicker({
          maxDate: 0,
          onClose: function( selectedDate ) {
            $( "#date_from" ).datepicker( "option", "maxDate", selectedDate );
          }
        })
        .on('change', function (e) {
        });
        
        $('.button_transfer').click(function(e)
        {
            var all         = $(this).data('all');
            var selected    = $(this).data('selected');
            
            $("#"+all+" > option:selected").each(function()
            {

                var t = $(this).text();
                var v = $(this).val();
                var str = '<option value="' + v + '" selected="selected">' + t + '</option>';
                /**************/
                var exists = false;
                $('#'+selected+' option').each(function() {
                    var nt = $(this).val();

                    if (v == this.value)
                        exists = true;
                });
                /***************/
                if (!exists)
                    $('#'+selected+'').append(str);
            });
        });
        
        
        $(".remove_selected").click(function() {
            var selected    = $(this).data('selected');
            $("#"+selected+" > option:selected").each(function() {
                $(this).remove();
            });
        });
    });
</script>
<!-- whitelabel rules-->