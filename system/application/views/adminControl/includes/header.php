<div class="navbar navbar-fixed-top">
        <div class="main-header header-background">
                <ul class="nav pull-right">
                    <ul class="login-control">
                      <li>Welcome, <i class="icon-user"></i> <?php echo $this->session->userdata('admin_name'); ?></li>
                      <li><a href="<?php echo admin_url('myaccount');?>" title="My Account"><i class="icon-briefcase"></i></a></li>
                      <li><a href="<?php echo admin_url('login/logout');?>" title="Log Out"><i class="icon-signout"></i></a></li>
                    </ul> 
                </ul>
                <a class="brand" href="<?php echo admin_url('dashboard');?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="glmop!" width="225" /></a>
        </div>
    </div>