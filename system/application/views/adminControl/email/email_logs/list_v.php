<?php echo $main_header; ?>
<?php echo $header; ?>
<div class="content">
    <?php echo $main_menu; ?>
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="page-heading">Manage Email Logs</div>
                        </div>
                    </div>
                </div>
                <div class="page-subtitle">
                </div>
                <table class="table table-hover" width="100%">
                    <tr>
                        <td><strong>SN.</strong></td>
                        <td><strong>Subject</strong></td>
                        <td><strong>From</strong></td>
                        <td><strong>To</strong></td>
                        <td><strong>Date Sent</strong></td>
                        <td><strong>Status</strong></td>
                        <td style="text-align:center;"><strong>Options</strong></td>
                    </tr>
                    <?php foreach ($email_logs as $rows) { ?>
                    <tr>
                        <td><?php echo $rows['id']; ?></td>
                        <td><?php echo $rows['subject']; ?></td>
                        <td><?php echo $rows['from_email']; ?></td>
                        <td><?php echo $rows['to_email']; ?></td>
                        <td><?php echo $rows['date_created']; ?></td>
                        <td><?php 
                            if($rows['id'] <= 8227 && $rows['id'] >=8133 )
                                echo "success";
                            else
                                echo $rows['response']; 
                        ?></td>
                        <td style="text-align:center;">
                            <?php echo anchor(ADMIN_FOLDER."/email_logs/view/".$rows['id']."/", "<i class=\"icon-view\"></i>","title='View' "); ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
</body>
</html>


