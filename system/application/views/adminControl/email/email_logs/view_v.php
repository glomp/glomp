<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="<?php echo base_url(); ?>" />
        <title><?php echo $page_title; ?></title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Glomp">
        <meta name="author" content="Young Mids Creation">
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/backend/html5.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript">
            function redirect()
            {
                location.href = '<?php echo base_url(ADMIN_FOLDER . '/user'); ?>'
            }
            $(document).ready(function(e) {
                $('#user_info_details_click').click(function(e) {
                    $('#user_info_details').toggle();
                });
                $('#user_info_details').toggle('hide');
            });
        </script>
    </head>
    <body class="">
        <?php include("includes/header.php"); ?>
        <div class="content">
            <?php include("includes/main-menu.php"); ?>  
            <div class="container-fluid  dashboard">
                <div class="row-fluid">
                    <?php //include("includes/right-sidebar.php");?> 
                    <div class="span12">

                        <div class="page-header">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="page-heading">Manage Users</div>
                                </div>
                                <div class="span8" style="text-align:right;">

                                    <form name="form1" method="get" action="<?php echo base_url(ADMIN_FOLDER . "/user") ?>" style="margin:0; padding:0;">
                                        <input type="text" style="margin-bottom:1px;"  class="span4" name="keywords" id="keywords" value="<?php if (isset($_GET['keywords'])) echo $_GET['keywords']; ?>">
                                        <input type="submit" name="Search" id="Search" class="btn" value="Search">
                                        <input type="button" name="viewAll" id="viewAll" class="btn-WI btn ui-state-default ui-corner-all" value="View All" onClick="javascript:location.href = '<?php echo base_url(ADMIN_FOLDER . "/user") ?>'">
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                        $recUser = $resUser->row();
                        $profile_pic = $this->custom_func->profile_pic($recUser->user_profile_pic, $recUser->user_gender);
                        //$recSummary = $resSummary->row();

                        $recSummary = json_decode($resSummary);
                        ?>
                        <div class="row-fluid">
                            <div class="span1">
                                <img src="<?php echo $profile_pic; ?>" alt="<?php echo $recUser->user_fname; ?>" class="img-polaroid">
                            </div>
                            <div class="span11">
                                <h4><?php echo $recUser->user_fname ?> <?php echo $recUser->user_lname ?></h4>
                                <strong><?php echo $this->regions_m->selectRegionNameByID($recUser->user_city_id); ?></strong><br />
                                Remaning Balance : <span class="label label-success"><strong> <?php echo $recSummary->point_balance; ?></strong></span> 

                                Total Received Glomp : <span class="label label-success"><strong> <?php echo $recSummary->total_received_glomp; ?></strong></span>


                                Total Sent Glomp : <span class="label label-success"><strong> <?php echo $recSummary->total_sent_sent_glomp; ?></strong></span>




                                Last Glomp Date : <span class="label label-success"><strong> <?php
                                        if ($recSummary->summary_last_glomp_date != "")
                                            echo $this->custom_func->time_stamp($recSummary->summary_last_glomp_date);
                                        else
                                            echo "Never";
                                        ?></strong></span>

                            </div>
                        </div>

                        <div class="widget-box">
                            <div class="widget-title"><a href="javascript:void(0)" id="user_info_details_click"><h5><i class="icon-briefcase"></i> User Information</h5></a></div>

                            <div class="widget-content" id="user_info_details" >
                                <div class="row-fluid">
                                    <div class="span12 account" >

                                        <table width="100%"  class="table table-striped table-hover">

                                            <tbody>

                                                <tr>
                                                    <td width="30%"><span class="control-label">Email</span></td>
                                                    <td><?php echo $recUser->user_email; ?></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="control-label">Gender </span></td>
                                                    <td><?php echo $recUser->user_gender ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Date Of Birth (DOB)</td>
                                                    <td><?php echo $recUser->user_dob; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Region</td>
                                                    <td><?php echo $this->regions_m->selectRegionNameByID($recUser->user_city_id); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td><?php echo $recUser->user_status; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Join Date</td>
                                                    <td><?php echo $this->custom_func->time_stamp($recUser->user_join_date); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Last Updated Date</td>
                                                    <td><?php echo $this->custom_func->time_stamp($recUser->user_last_updated_date); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Created IP</td>
                                                    <td><?php echo $recUser->user_account_created_ip; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Last Login IP</td>
                                                    <td><?php echo $recUser->user_last_login_ip; ?></td>
                                                </tr>
                                                <?php /* ?><tr>
                                                  <td>Email Verified</td>
                                                  <td><?php echo $recUser->user_email_verified;?></td>
                                                  </tr>
                                                  <tr>
                                                  <td>Sign Up Device</td>
                                                  <td><?php echo $recUser->user_signup_device;?></td>
                                                  </tr><?php */ ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>							
                            </div>
                        </div>
                        <?php if ($resActivity->num_rows() > 0) { ?>
                            <div class="widget-box">
                                <div class="widget-title"><h5><i class="icon-briefcase"></i> Activity Log</h5></div>

                                <div class="widget-content">
                                    <div class="row-fluid">
                                        <div class="span12 account">

                                            <table width="100%" class="table">
                                                <?php foreach ($resActivity->result() as $recActivity) { ?>
                                                    <tr>
                                                        <td><?php echo $recActivity->log_timestamp; ?></td>
                                                        <td><?php echo $recActivity->log_title; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                <tr>
                                                    <td colspan="2"><?php echo $this->pagination->create_links(); ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>							
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
        <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
    </body>
</html>


