<?php echo $main_header; ?>
<?php echo $header; ?>
<div class="content">
    <?php echo $main_menu; ?>
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="page-heading">Manage Email Templates</div>
                        </div>
                        <div class="span4" style="text-align:right;">
                            <?php echo anchor(ADMIN_FOLDER."/email/add", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                        </div>
                    </div>
                </div>
                <div class="page-subtitle">
                </div>
                <table class="table table-hover" width="100%">
                    <tr>
                        <td><strong>SN.</strong></td>
                        <td><strong>Created Date</strong></td>
                        <td><strong>Template Name</strong></td>
                        <td><strong>Language</strong></td>
                        <td style="text-align:center;"><strong>Options</strong></td>
                    </tr>
                    <?php foreach ($email_templates as $rows) { ?>
                    <tr>
                        <td><?php echo $rows['id']; ?></td>
                        <td><?php echo $rows['date_created']; ?></td>
                        <td><?php echo $rows['template_name']; ?></td>
                        <td><?php echo $rows['lang_name']; ?></td>
                        <td style="text-align:center;">
                            <?php echo anchor(ADMIN_FOLDER."/email/edit/".$rows['id']."/", "<i class=\"icon-edit\"></i>","title='Edit' "); ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
</body>
</html>


