<?php echo $main_header; ?>
<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/lib/easyui/themes/default/easyui.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/lib/easyui/themes/icon.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/backend/lib/easyui/jquery.easyui.min.js'); ?>"></script>
<div class="content">
    <?php echo $main_menu; ?>
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="page-heading">Email Template</div>
                        </div>
                        <div class="span4" style="text-align:right;"></div>
                    </div>
                </div>

                <div class="page-subtitle">Add Email Template</div>
                <div id="error-div"></div>
                <div class="row-fluid">
                    <?php echo form_open(ADMIN_FOLDER . '/email/save/', 'name="email_template_form" id="email_template_form"') ?>
                    <input type ="hidden" value ="0" name="id" id="id" />
                    <div class="content-box">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="control-group">
                                    <label class="label_form">Template Name<span class="required">*</span></label>
                                    <div class="left">
                                        <input type="text" name="template_name" id ="template_name" >
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="control-group">
                                    <label class="label_form">Language<span class="required">*</span></label>
                                    <div class="left">
                                        <select name="language" id="language" >
                                            <?php foreach ($languages as $l) { ?>
                                                 <option value="<?php echo $l['lang_id'] ?>"><?php echo $l['lang_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="control-group">
                                    <label class="label_form">Subject<span class="required">*</span></label>
                                    <div class="left">
                                        <input type="text" name="subject" id ="subject" >
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="control-group">
                                    <label class="label_form">Contents<span class="required">*</span></label>
                                    <div class="left">
                                        <!--ckeditor starts-->
                                        <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/ckeditor.js"></script>
                                        <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckeditor/adapters/jquery.js"></script>
                                        <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/editor/ckfinder/ckfinder.js"></script>
                                        <script type="text/javascript">
                                            CKFinder.setupCKEditor(null, '<?php echo base_url(); ?>assets/backend/editor/ckfinder');
                                        </script>
                                        <div style="width: 800px;">
                                            <textarea name="body" id="body"></textarea>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="label_form"></label>
                                    <div class="left">
                                        <input class="btn btn-WI" type="submit" id="save_btn" value="Save" >
                                        <?php echo anchor(ADMIN_FOLDER . "/email", "Cancel", "class='btn ui-state-default ui-corner-all'") ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>

<div class="" id="messager" style="">
    <div style="width: 256px;margin:0 auto">
        <span id="note">Loading....</span>
        <div style="width: 100%;margin:0 auto;margin-top: 5px;" align="center">
            <a id="message_close_btn" class="easyui-linkbutton hide" href="#" class="hide">Close</a>
        </div>
    </div>
</div>

<script>
    $('#body').ckeditor();

    $('#messager').window({
        title: 'Submitting form...',
        width: 400,
        height: 110,
        closed: true,
        modal: true,
        maximizable: false,
        minimizable: false,
        resizable: false,
        closable: false,
        collapsible: false
    });

    $('#save_btn').click(function(e) {
        e.preventDefault();

        $('#messager').window('open');
        $('#message_close_btn').removeClass('l-btn');

        $.ajax({
            type: "post",
            url: $('#email_template_form').attr('action'),
            dataType: 'json',
            data: $('#email_template_form').serialize(),
            success: function(response) {
                if (!response.success) {
                    $('#messager').window('close');
                    process_error(response);
                    return;
                }

                process_error(response);
                /*Unhide*/
                $('#note').html('Your request is successfully submitted.');
                $('#message_close_btn').removeClass('hide');
                $('#message_close_btn').addClass('l-btn');
                $('#message_close_btn').click(function(e) {
                    e.preventDefault();
                    window.location = '<?php echo site_url(ADMIN_FOLDER . '/email') ?>';
                });
            }
        });
    });

    function process_error(response) {
        $('#error-div').html('');
        for (var e in response.errors) {
            if (response.errors[e] != '') {
                $('#error-div').addClass('custom-error');
                $('#error-div').append(response.errors[e]);
/*                $('#' + e).tooltip({
                    position: 'right',
                    content: response.errors[e],
                    onShow: function() {
                        $(this).tooltip('tip').css({
                            borderColor: 'red'
                        });
                    }
                });
*/
                $('#' + e).addClass('error_border')
            }

            /*Remove any error message if theres any*/
            if (response.errors[e] == '') {
/*                $('#' + e).tooltip();
                $('#' + e).tooltip('destroy');*/
                $('#' + e).removeClass('error_border');
            }
        }
    }
</script>
</body>
</html>        