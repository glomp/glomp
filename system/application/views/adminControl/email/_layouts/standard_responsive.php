<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title></title>

        <style>/* ------------------------------------- 
                            GLOBAL 
            ------------------------------------- */
            * { 
                margin:0;
                padding:0;
                font-size: 12px;
            }
            * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif !important; }

            img { 
                max-width: 100%; 
            }
            .collapse {
                margin:0;
                padding:0;
            }
            body {
                -webkit-font-smoothing:antialiased; 
                -webkit-text-size-adjust:none; 
                width: 100%!important; 
                height: 100%;
            }


            /* ------------------------------------- 
                            ELEMENTS 
            ------------------------------------- */
            a { color: blue;}
            p {line-height:1.6;}


            /* ------------------------------------- 
                            HEADER 
            ------------------------------------- */
            /*            table.head-wrap { width: 100%;}
            
                        .header.container table td.logo { padding: 15px; }
                        .header.container table td.label { padding: 15px; padding-left:0px;}*/


            /* ------------------------------------- 
                            BODY 
            ------------------------------------- */
            /*            table.body-wrap { width: 100%;}*/


            /* ------------------------------------- 
                            FOOTER 
            ------------------------------------- */



            /* ------------------------------------- 
                            TYPOGRAPHY 
            ------------------------------------- */
            .collapse { margin:0!important;}

            /* ------------------------------------- 
                            SIDEBAR 
            ------------------------------------- */


            /* --------------------------------------------------- 
                            RESPONSIVENESS
                            Nuke it from orbit. It's the only way to be sure. 
            ------------------------------------------------------ */

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display:block;
                max-width:100% !important;
                clear:both!important;
            }
            .table-width {
                max-width:100% !important;
            }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                display:block;
                padding: 25px;
                font-size: 12px !important;
            }
/*            .center {
                max-width: 600px !important;
                margin: 0 auto;
            }*/

            /* Let's make sure tables in the content area are 100% wide */
            /*.content table { width: 100%; border-spacing: 0px !important;}*/

            /* Be sure to place a .clear element after each set of columns, just to be safe */
            .clear { display: block; clear: both; }


            /* ------------------------------------------- 
                            PHONE
                            For clients that support media queries.
                            Nothing fancy. 
            -------------------------------------------- */
/*            @media only screen and (max-width: 320px) {
                .container {
                    display: block;
                    max-width: 320px !important;
                    clear:both!important;
                }
                .content {
                    display:block;
                    font-size: 12px !important;
                    padding: 20px 10px 10px 10px;
                }
                .table-width {
                    max-width: 320px !important;
                }
                .center {
                    max-width: 320px !important;
                    margin: 0 auto;
                }
            }*/
        </style>

    </head>

    <body bgcolor="#FFFFFF">

        <div class="center">
            <!-- BODY -->
            <table cellpadding="0" cellspacing="0"  style="width: 100%">
                <tr>
                    <td class="container" bgcolor="#636466" align="center">
                        <div>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="color:#545456;">
                                        <div>
                                            <img src="<?php echo base_url('assets/frontend/img/email_template_header.png');  ?>" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="container" bgcolor="#E5E5E5">
                        <div class="content" style="padding:25px;">
                            <table cellpadding="0" cellspacing="0" bgcolor="" >
                                <tr>
                                    <td style="color:#545456;" class="table-width">
                                        <div>
                                            <?php echo $content; ?>
                                        </div>
                                    </td> 
                                </tr>
                            </table>
                        </div><!-- /content -->
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td bgcolor ="#fbfcf2">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    <div style="font-size: 8px !important;padding-left: 25px;padding-bottom: 10px;padding-top: 16px;">Having trouble viewing this email? 
                                        <span><a style="font-size: 8px !important;" target="_blank" href="<?php echo site_url('landing/viewEmail/'.$view_id); ?>">View it on your browser</a></span>
                                    </div>
                                    <div style="min-height:20px;color:#545456;padding:16px;padding-bottom: 10px;padding-top: 0px;padding-left:25px">
                                        <span style="font-size: 8px !important">
                                            Subscription settings <a href="<?php echo site_url('landing/notificationRedirect'); ?>" style="font-size: 8px !important;">Click here</a>
                                        </span>
                                    </div>
                                    <div style="min-height: 20px;color:#545456;padding: 16px;padding-bottom: 5px;padding-top: 0px;padding-left: 25px;">
                                        <span style="font-size: 8px !important">Copyright &copy; <?php echo date('Y'); ?> glomp! All rights reserved.</span>
                                    </div>
                                </td>
                                <td valign="top" align="right" >
                                    
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table><!-- /BODY -->
        </div>
    </body>
</html>