<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?>:: Glomp</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
     <script language="javascript" type="text/javascript">
   $(document).ready(function(e) {
    $('#reset_password').click(function(e) {
       
	   if($('#reset_password').is(':checked'))
	   {
	     $('#password').css('display','block')
	   }else
	   {
		   	     $('#password').css('display','none');

		   }
    });
});
   
   </script>
  </head>
  <body class="">
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid dashboard">
    	<div class="row-fluid">
            <div class="span12">
            <div class="page-header">
            	<div class="row-fluid">
                	<div class="span8">
                  		<div class="page-heading"><?php echo $page_title;?></div>
                  	</div>
                  	<div class="span4" style="text-align:right;">
						<?php echo anchor(ADMIN_FOLDER."/admin/addAdmin", "Add New", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
						<?php echo anchor(ADMIN_FOLDER."/admin", "View All", "class='btn-WI btn ui-state-default ui-corner-all'" )?>
                  	</div>
                  </div>
             </div>
             
                <div class="page-subtitle">
                    <?php
                        echo $page_subtitle;
                        if(isset($msg))
                        {
                             echo "<div class=\"success_msg\">".$msg."</div>";
                        }
                        if(isset($error_msg))
                        {
                             echo "<div class=\"alert alert-error\">".$error_msg."</div>";
                        }
                        if(validation_errors()!="")
                        {
                            echo "<div class=\"custom-error\">".validation_errors()."</div>";
                        }
                    ?>
                 </div>
            <?php if($page_action=="view") { ?>
            <table class="table table-condensed table-hover">
            	<tr>
                	<td><strong>SN.</strong></td>
                  	<td><strong>Added Date</strong></td>
                  	<td><strong>Admin Name</strong></td>
                  	<td style="text-align:center"><strong>Admin Email</strong></td>
                  	<td style="text-align:center;"><strong>Last Login</strong></td>
                  	<td style="text-align:center;"><strong>Status</strong></td>
                  	<td style="text-align:center;"><strong>Options</strong></td>
                </tr>
				<?php 
				
					if(isset($res_admin)) { 
				  	$i=1;
					
				  	foreach($res_admin->result() as $rec_admin) {
						
						$tr_class = ($rec_admin->admin_status=="Deleted")?"class='error'":"";
				  ?>
                <tr <?php echo $tr_class;?> >
                	<td><?php echo $i; $i++; ?></td>
                    <td><?php echo $rec_admin->admin_added; ?></td>
                    <td><?php echo $rec_admin->admin_name; ?></td>
                    <td style="text-align:center"><?php echo $rec_admin->admin_email;?></td>
                    <td style="text-align:center;"><?php echo ($rec_admin->admin_last_login=="")?'Never':$rec_admin->admin_last_login;?></td>
                  	<td style="text-align:center;"><?php echo $rec_admin->admin_status;?></td>
                  	<td style="text-align:center;">
                   		<?php
						
							echo anchor(ADMIN_FOLDER."/admin/editAdmin/".$rec_admin->admin_id."/", "<i class=\"icon-edit\"></i>","title='Edit' ");
							if($rec_admin->admin_status!='Deleted'){
							echo "&nbsp;&nbsp;&nbsp;";
							echo anchor(ADMIN_FOLDER."/admin/deleteAdmin/".$rec_admin->admin_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");
							}
					?>
                  	</td>
                  </tr>
                  <?php }
				 
				  echo form_close();
				 
				  } 
				  else 
				  	echo'<tr><td colspan="6">No records found.</td></tr>'; ?>
                  </table>
                  <?php } elseif($page_action=="add") { ?>
                  <?php echo form_open(ADMIN_FOLDER."/admin/addAdmin",'name="frmadmin" id="frmadmin"')?>
                  <div class="content-box">
                  		<div class="row-fluid">
                        	<div class="span12">
                                <div class="control-group">
                                        <label class="control-label" for="add"><span>*</span>Name</label>
                                        <div class="controls">
                                            <input type="text" required name="admin_name" value="<?php echo set_value('admin_name'); ?>">
                                        </div>
                                </div>
						        <div class="control-group">
                                     <label class="control-label" for="add">*Email Address</label>
                                     <div class="control">
                                        <input type="email" required name="email_address" value="<?php echo set_value('email_address'); ?>">
                                     </div>
                                </div>
                                 <div class="control-group">
                                     <label class="control-label" for="add">*Password</label>
                                     <div class="control">
                                        <input type="password" name="password">
                                     </div>
                                </div>
                         	</div>
                      	</div>
                  		<input class="btn btn-large btn-WI" type="submit" name="add" value="Add" >
                  		<button class="btn btn-large" type="button" name="addPage" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <?php } elseif($page_action=="edit")
				  { 
				  ?>
                  <?php echo form_open(ADMIN_FOLDER."/admin/editAdmin/".$res_admin->admin_id,'name="frmCms" id="cmspages"')?>
                  <div class="content-box">
                  		<div class="row-fluid">
                        
                        	<div class="span8">
                 	 
                             <div class="control-group">
                                    <label class="control-label" for="add"><span>*</span>Name:</label>
                                    <div class="controls">
                                        <input type="text" name="admin_name" value="<?php echo $res_admin->admin_name; ?>">
                                    </div>
                             </div>
                            
					   	 <div class="control-group">
                             	<label class="control-label" for="add">Email address</label>
                                <div class="controls">
                                	<input type="email" name="email_address" value="<?php echo $res_admin->admin_email;?>">
                                </div>
                             </div>
                              <div class="control-group">
                             	<label class="control-label" for="add">Admin Status</label>
                                <div class="controls">
                                	<input type="radio" name="admin_status" value="Active" <?php echo $res_admin->admin_status=="Active"?"checked='checked'":"";?> />  Active   
                                 <input type="radio" name="admin_status" value="InActive" <?php echo $res_admin->admin_status=="InActive"?"checked='checked'":"";?> />    InActive  
								 <input type="radio" name="admin_status" value="InActive" <?php echo $res_admin->admin_status=="Deleted"?"checked='checked'":"";?> />   Deleted  
                                </div> 
                             </div>
                              <div class="control-group">
                                     <label class="control-label" for="add">*Reset Password <input id="reset_password" type="checkbox" value="1" name="reset_password" /></label>
                                     <div class="control">
                                        <input type="text"  id="password" name="password" style="display:<?php echo isset($_POST['reset_password'])?'blocked':'none';?>">
                                     </div>
                                </div>
                        	</div>
                            <div class="span4">
                            		<h2>Other Info</h2>
                    	<p class="text-left">Status: <?php echo $res_admin->admin_status;?></p>
                        <p class="text-left">UserType: <?php echo $res_admin->admin_type;?></p>
                        <p class="text-left">Account Created Date: <?php echo $res_admin->admin_added;?></p>
                        <p class="text-left">Last Login: <?php echo $res_admin->admin_last_login;?></p>
                        <p class="text-left">Last Login IP: <?php echo $res_admin->admin_last_ip;?></p>
                        <p class="text-left">Currnt IP: <?php echo $_SERVER['REMOTE_ADDR'];;?></p>
                            </div>
                            
                        </div>
                        <input type="hidden" name="admin_id" value="<?php echo $res_admin->admin_id;?>" />
                  		<input class="btn btn-large btn-WI" type="submit" name="edit" value="Save" />
                  		<button class="btn btn-large" type="button" name="editCat" onclick="Javascript:location.href='<?php echo site_url()."/".$this->uri->segment(1)."/".$this->uri->segment(2);?>'"><?php echo "Cancel";?></button>
                  </div>
                 
                  <?php } ?>
                  
            </div>
            
            
        </div>
    </div>
</div>
     	<?php include("includes/footer.php");?>
   		<script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


