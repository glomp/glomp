<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Brand Admin - Glomp! Admin</title>
        <base href="<?php echo base_url(); ?>" />
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">

        <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">

        <?php /*<link href="assets/backend/lib/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">*/ ?>
        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <style type="text/css">
            .ui-dialog.ui-widget {
                font-size: 85%;
                padding:0;
                border-radius:0px;
            }
            .ui-dialog .ui-dialog-titlebar {
                border-radius:0;
                border:0;
            }
            .ui-dialog.ui-widget-content {
                -webkit-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                -moz-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
            }
        </style>
    </head>
    <body>
        <?php include("includes/header.php"); ?>

        <div class="content">
            <?php include("includes/main-menu.php");?>
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span2">
                            <div class="page-heading">Manage Brands</div>
                        </div>
                        <div class="span10">
                            <div style="float:right">
                                <input type="button" class="btn btn-WI" value="Add New" onclick="window.location='/adminControl/brands/create'" />
                            </div>
                            <div style="float:right;margin-right:10px;">
                                <form id="filter" action="/adminControl/brands/index">
                                    <input type="text" name="key" placeholder="Search..." style="margin:0;" value="<?php echo $this->input->get('key')?>" id="key" />
                                    &nbsp;
                                    <input type="submit" value="Search" class="btn btn-WI" />
                                    <?php if($this->input->get('key')):?>
                                    &nbsp;
                                    <input type="button" value="Clear" class="btn" onclick="document.getElementById('key').value='';document.getElementById('filter').submit()" />
                                    <?php endif;?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-subtitle">All Brands</div>


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <td style="font-weight:bold">ID</td>
                            <td style="font-weight:bold">Name</td>
                            <td style="font-weight:bold">Description</td>
                            <td style="font-weight:bold">Logo</td>
							<td style="font-weight:bold">Type</td>
							<td style="font-weight:bold">Public Alias</td>
                            <td style="text-align:center;font-weight:bold">Status</td>
                            <td style="text-align:center;font-weight:bold">Options</td>
                        </tr>
                    </thead>
                    <tbody id="row-template">
                        <?php echo $records; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" id="paging">
                                <?php echo $paging; ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>

        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
        <?php /*<script type="text/javascript" src="assets/backend/lib/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>*/ ?>

        <script type="text/javascript">
            jQuery(document).ready(function(){

                /* template editing */

                var TemplateDialog = jQuery('<div id="template-dialog" />');
                TemplateDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Edit Template',
                    modal: true,
                    draggable: false,
                    width: 864,
                    buttons: []
                });

                jQuery(document).on('click', '.edit-template', function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url : jQuery(this).attr('href'),
                        dataType: 'json'
                    }).done(function(response){
                        var url = '/adminControl/templates/index/'+response.template.id;
                        var buttons = TemplateDialog.dialog('option','buttons');
                        TemplateDialog
                            .html('<iframe frameborder="0" scrolling="no" width="840" height="600" src="'+url+'"></iframe>')
                            .dialog('option','buttons',[{
                                text: "Mobile Preview",
                                click: function() {
                                    window.open('/m/brands/preview/'+response.template.referer_id, "", "width=320, height=700");
                                }
                            },{
                                text: "Desktop Preview",
                                click: function() {
                                    window.open('/brands/preview/'+response.template.referer_id);
                                }
                            },{
                                text: "Cancel",
                                click: function() {
                                    TemplateDialog.dialog('close');
                                }
                            }])
                            .dialog('open');

                    });
                });

                /* delete record */

                var DeleteDialog = jQuery('<div id="delete-dialog" />');
                DeleteDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Delete Brand',
                    modal: true,
                    draggable: false
                });

                jQuery(document).on('click','.delete-record', function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url : jQuery(this).attr('href'),
                        dataType: 'json'
                    }).done(function(response){
                        DeleteDialog.html(response.confirm);
                        DeleteDialog.dialog('open');
                    });
                    jQuery(document).on('click','#no',function(e){
                        e.preventDefault();
                        DeleteDialog.dialog('close');
                    });
                });
            });
        </script>
    </body>
</html>
