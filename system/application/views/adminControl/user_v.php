<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <base href="<?php echo base_url(); ?>" />
    <title><?php echo $page_title; ?></title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Glomp">
    <meta name="author" content="Young Mids Creation">
    <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/backend/css/common.css">
    <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/backend/html5.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript">
    function redirect()
	{
		location.href='<?php echo base_url(ADMIN_FOLDER.'/user');?>'
		}
	$(document).ready(function(e) {
        $('#user_info_details_click').click(function(e) {
            $('#user_info_details').toggle();
        });
		$('#user_info_details').toggle('hide');
    });
    </script>
  </head>
  <body class="">
  		<?php include("includes/header.php"); ?>
	 	<div class="content">
	<?php include("includes/main-menu.php");?>  
    <div class="container-fluid  dashboard">
        <div class="row-fluid">
			<?php //include("includes/right-sidebar.php");?> 
            <div class="span12">
            
            <div class="page-header">
                  <div class="row-fluid">
                  <div class="span4">
                  <div class="page-heading">Manage Users</div>
                  </div>
                  <div class="span8" style="text-align:right;">
               
                 <form name="form1" method="get" action="<?php echo base_url(ADMIN_FOLDER."/user")?>" style="margin:0; padding:0;">
                   <input type="text" style="margin-bottom:1px;"  class="span4" name="keywords" id="keywords" value="<?php if(isset($_GET['keywords'])) echo $_GET['keywords'];?>">
                   <input type="submit" name="Search" id="Search" class="btn" value="Search">
                   <input type="button" name="viewAll" id="viewAll" class="btn-WI btn ui-state-default ui-corner-all" value="View All" onClick="javascript:location.href='<?php echo base_url(ADMIN_FOLDER."/user")?>'">
                  

                 </form>
                  </div>
                  </div>
                  </div>
     
                  <?php if($page_action=="view") { ?>
                  
                  <div class="page-subtitle"><?php
				  echo "View All ";
				  if(isset($breadcum))
				  {
					  echo $breadcum;
				  }
				  if(isset($msg))
				  {
					  echo "<div class=\"success_msg\">".$msg."</div>";
				  }
				  if(isset($error_msg))
				  {
					  echo "<div class=\"custom-error\">".$error_msg."</div>";
				  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"custom-error\">".validation_errors()."</div>";
				  }
				  ?>
                  </div>
                  <table class="table table-hover" width="100%">
                  <tr>
                  <td><strong>SN.</strong></td>
                  <td><strong>Created Date</strong></td>
                  <td><strong>User Name </strong></td>
                  <td><strong>Email Address</strong></td>
                  <td><strong>Location</strong></td>
                  
                  <td style="text-align:center;"><strong>Status</strong></td>
                  <td style="text-align:center;"><strong>Options</strong></td>
                  </tr>
                  <?php if($resUser) { 
				  $i=1;
				  foreach($resUser ->result() as $recUser) {
					  $tr_class = ($recUser->user_status=="Deleted")?"class='error'":"";
				  ?>
                  <tr <?php echo $tr_class;?>>
                  <td><?php echo $i; $i++; ?></td>
                  <td><?php echo $this->custom_func->time_stamp($recUser->user_join_date);?></td>
                  <td><?php echo $recUser->user_fname;?> <?php echo $recUser->user_lname;?></td>
                  <td><?php echo $recUser->user_email;?></td>
                  <td>
                   <?php
						 echo $this->regions_m->selectRegionNameByID($recUser->user_city_id);
					?>
                  </td>
                   <td style="text-align:center;">
				   
                   
                   <?php
				   if($recUser->user_status=='Active'){
					echo anchor(ADMIN_FOLDER."/user/updateStatus/".$recUser->user_id."/InActive", $recUser->user_status);
						}
						else if($recUser->user_status=='InActive'){
						echo anchor(ADMIN_FOLDER."/user/updateStatus/".$recUser->user_id."/Active", $recUser->user_status);
					}
					else
					echo $recUser->user_status;
					
				   ?>
				 </td>
                  <td style="text-align:center"><?php
						
							echo anchor(ADMIN_FOLDER."/user/viewDetails/".$recUser->user_id."/", "<i class=\"icon-eye-open\"></i>","title='View' ");echo "&nbsp;&nbsp;&nbsp;";
							echo anchor(ADMIN_FOLDER."/user/edit/".$recUser->user_id."/", "<i class=\"icon-edit\"></i>","title='View' ");
							if( $recUser->user_email=='testone.glomp@gmail.com' ||
                                $recUser->user_email=='testtwo.glomp@gmail.com' ||
                                $recUser->user_email=='testthree.glomp@gmail.com' ||
                                $recUser->user_email=='testfour.glomp@gmail.com' ||
                                $recUser->user_email=='testfive.glomp@gmail.com' ||
                                $recUser->user_email=='testsix.glomp@gmail.com')
                            {
							echo "&nbsp;&nbsp;&nbsp;";
							echo anchor(ADMIN_FOLDER."/user/deleteRecord/".$recUser->user_id."/", "<i class=\"icon-trash\"></i>", "title='Delete' onclick=\"javascript:return confirm('Are you sure you want to delete this record?')\"");
							}
					?></td>
                  </tr>
                  <?php }
				  echo '<tr><td colspan="7">'.$this->pagination->create_links().'</td></tr>';
				  } else echo'<tr><td colspan="7">No records found.</td></tr>'; ?>
                  </table>
                  <?php } 
				  elseif($page_action=="viewDetails") { ?>
                  
          <?php $recUser = $resUser->row();
		  $profile_pic = $this->custom_func->profile_pic($recUser->user_profile_pic,$recUser->user_gender);
		  //$recSummary = $resSummary->row();
		  
		 $recSummary =  json_decode($resSummary);
		 
		  ?>
          <div class="row-fluid">
          <div class="span1">
            <img src="<?php echo $profile_pic;?>" alt="<?php echo $recUser->user_fname;?>" class="img-polaroid">
            </div>
            <div class="span11">
            <h4><?php echo $recUser->user_fname?> <?php echo $recUser->user_lname?></h4>
           <strong><?php echo $this->regions_m->selectRegionNameByID($recUser->user_city_id);?></strong><br />
           Remaning Balance : <span class="label label-success"><strong> <?php echo $recSummary->point_balance;?></strong></span> 
           
            Total Received Glomp : <span class="label label-success"><strong> <?php echo $recSummary->total_received_glomp;?></strong></span>
            
            
             Total Sent Glomp : <span class="label label-success"><strong> <?php echo $recSummary->total_sent_sent_glomp;?></strong></span>
             
             
            
            
         Last Glomp Date : <span class="label label-success"><strong> <?php 
		 if($recSummary->summary_last_glomp_date!="")
			 echo $this->custom_func->time_stamp($recSummary->summary_last_glomp_date);
			else
			echo "Never";
			?></strong></span>
           
            </div>
            </div>
            
                 <div class="widget-box">
							<div class="widget-title"><a href="javascript:void(0)" id="user_info_details_click"><h5><i class="icon-briefcase"></i> User Information</h5></a></div>
                            
							<div class="widget-content" id="user_info_details" >
								<div class="row-fluid">
                                    <div class="span12 account" >
                                   
 									 <table width="100%"  class="table table-striped table-hover">
  
   <tbody>
    
  <tr>
    <td width="30%"><span class="control-label">Email</span></td>
    <td><?php echo $recUser->user_email;?></td>
  </tr>
  <tr>
    <td><span class="control-label">Gender </span></td>
    <td><?php echo $recUser->user_gender?></td>
  </tr>
  <tr>
    <td>Date Of Birth (DOB)</td>
    <td><?php echo $recUser->user_dob;?></td>
  </tr>
  <tr>
    <td>Region</td>
    <td><?php echo $this->regions_m->selectRegionNameByID($recUser->user_city_id);?></td>
  </tr>
  <tr>
    <td>Status</td>
    <td><?php echo $recUser->user_status;?></td>
  </tr>
  <tr>
    <td>Join Date</td>
    <td><?php echo $this->custom_func->time_stamp($recUser->user_join_date);?></td>
  </tr>
  <tr>
    <td>Last Updated Date</td>
    <td><?php echo $this->custom_func->time_stamp($recUser->user_last_updated_date);?></td>
  </tr>
  <tr>
    <td>Created IP</td>
    <td><?php echo $recUser->user_account_created_ip;?></td>
  </tr>
  <tr>
    <td>Last Login IP</td>
    <td><?php echo $recUser->user_last_login_ip;?></td>
  </tr>
  <?php /*?><tr>
    <td>Email Verified</td>
    <td><?php echo $recUser->user_email_verified;?></td>
  </tr>
  <tr>
    <td>Sign Up Device</td>
    <td><?php echo $recUser->user_signup_device;?></td>
  </tr><?php */?>
   </tbody>
  </table>
                                  </div>
								</div>							
							</div>
						</div>
                        
                        
                         <?php if($resActivity->num_rows()>0) {?>
            
                 <div class="widget-box">
							<div class="widget-title"><h5><i class="icon-briefcase"></i> Activity Log</h5></div>
                            
							<div class="widget-content">
								<div class="row-fluid">
                                    <div class="span12 account">
                                   
 				<table width="100%" class="table">
  <?php foreach($resActivity->result() as $recActivity){?>
  <tr>
    <td><?php echo $recActivity->log_timestamp;?></td>
    <td><?php echo $recActivity->log_title;?></td>
  </tr>
  <?php 
  }
  ?>
  	<tr>
    	<td colspan="2"><?php echo $this->pagination->create_links();?></td>
  </tr>
</table>
                                  </div>
								</div>							
							</div>
						</div>
                        
                        <?php } ?>
                 
                  <?php } 
				  else if($page_action == 'edit')
				  {
					  //edit start
					  ?>
<?php echo form_open_multipart(ADMIN_FOLDER.'/user/edit/'.$user_id,'name="frmUpdate" class="form-horizontal" id="frmUpdate"')?>
                        
                       <?php
					     if(isset($success_msg))
				  {
					   echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$success_msg."</div>";
					  
					  }
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if(isset($error_msg))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
					 
					   ?>
                        <div class="row-fluid">
                        <div class="span6">
                        
                        <div class="control-group">
    <label class="control-label" for="fname"><span class="required">*</span> <?php echo $this->lang->line('fname');?></label>
    <div class="controls">
    <input type="text"  id="fname" name="fname" value="<?php echo $user_record->user_fname;?>" class="span11" >
    </div>
    </div>
     <div class="control-group">
    <label class="control-label" for="lname"><span class="required">*</span> <?php echo $this->lang->line('lname');?></label>
    <div class="controls">
    <input type="text"  id="lname" name="lname" value="<?php echo $user_record->user_lname;?>" class="span11" >
    </div>
    </div>
    <div class="control-group">
    <?php 
	$dob = explode('-',$user_record->user_dob);
	if(count($dob)!==3){
		$dob[0] = 0;$dob[1] = 0;$dob[2] = 0;
	}
		
	?>
    <label class="control-label" for="dob"><span class="required">*</span> <?php echo $this->lang->line('Date_of_Birth');?></label>
    <div class="controls">
    <input type="text" id="day" name="day" maxlength="2" value="<?php echo $dob[2];?>"  class="span2" placeholder="DD"> /
     <input type="text" id="months" name="months" value="<?php echo  $dob[1];?>" maxlength="2"  class="span2" placeholder="MM"> /
      <input type="text" id="year" name="year" maxlength="4" value="<?php  echo $dob[0];?>"  class="span3" placeholder="YYYY">
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="gender-male"><span class="required">*</span> <?php echo $this->lang->line('Gender');?></label>
    <div class="controls">
    <label class="radio inline">
<input type="radio" name="gender" id="gender-male" value="Male" <?php echo $checked=($user_record->user_gender!='Female')?'Checked="checked"':"";?> /> <?php echo $this->lang->line('Male');?>
</label>
<label class="radio inline">
<input type="radio" name="gender" id="gender-female" <?php echo $checked=($user_record->user_gender=='Female')?'Checked="checked"':"";?> value="Female"> <?php echo $this->lang->line('Female');?>
</label>
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="location"><span class="required">*</span> <?php echo $this->lang->line('Location');?></label>
    <div class="controls">
    
      <select name="location" id="location">
      <option value=""><?php echo $this->lang->line('Select_Location');?></option>
      <?php 
	  echo $this->regions_m->location_dropdown(0,$user_record->user_city_id);
	  ?>
      </select>
    </div>
    </div>
                        </div>
                        
                        <div class="span5">
                         <?php
		  $profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic,$user_record->user_gender);
		  ?>
            <img src="<?php echo $profile_pic;?>" alt="<?php echo $user_record->user_name;?>" class="img-polaroid"><br />
                        <a href="javascript:;" id="uploadFile" ><?php echo $this->lang->line('upload_profile_photo');?></a>
                        <span style="display:none">
                        <input type="file" name="user_photo" id="fileUploadField" />
                        </span>
                        </div>
                        </div>
                        
                        
                      <div class="row-fluid">
   					 <div class="span6">
                    <div class="control-group">
                    <label class="control-label" for="email"><span class="required">*</span> <?php echo $this->lang->line('E-mail');?></label>
                    <div class="controls">
                    <input type="text"  id="email" value="<?php echo $user_record->user_email;?>" name="email" class="span11" >
                    </div>
                    </div>
                        </div>
                        </div>
                        
                        
                        
                      <div class="row-fluid">
   					 <div class="span6">
                    
                    <div class="control-group">
                    <label class="control-label" for="new_pword"><?php echo $this->lang->line('New_password');?></label>
                    <div class="controls">
                    <input type="password"  id="new_pword" name="new_pword" class="span11" maxlength="9" >
                    </div>
                    </div>
                    <div class="control-group">
                    <label class="control-label" for="confirm_pword"><?php echo $this->lang->line('Confirm_Password');?></label>
                    <div class="controls">
                    <input type="password" id="confirm_pword" name="confirm_pword" class="span11" maxlength="9" >
                    </div>
                    </div>
                        </div>
                        <div class="span5">
                        &nbsp;
                        </div>
                        </div>
                       
                        <div class="row-fluid">
   						 <div class="span6">
                        
<div class="control-group">

    <div class="pull-right">
    <input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
      <button type="submit" class="btn btn-WI" name="update_profile" ><?php echo $this->lang->line('Save');?></button>
    <button type="button" class="btn btn-custom-gray" name="Reset" onClick="redirect()" ><?php echo $this->lang->line('Cancel');?></button></div>
    <div class="clearfix"></div>
    </div>
                        </div>
                        </div>
                       
    
    
    </form>
                  
                  <?php
				  //edit end
				  }
				  ?>
                  
                 
                  
            </div>
            
            
        </div>
    </div>
</div>
     <?php include("includes/footer.php");?>
   <script src="custom/lib/bootstrap/js/bootstrap.js"></script> 
  </body>
</html>


