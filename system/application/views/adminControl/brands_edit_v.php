<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit Brand - Glomp! Admin</title>
        <base href="<?php echo base_url(); ?>" />
        <link rel="stylesheet" type="text/css" href="assets/backend/lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/backend/lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/backend/css/common.css">
        <link rel="stylesheet" href="assets/backend/css/jquery-ui.min.css">
        <link rel="stylesheet" href="assets/frontend/css/jcrop/jquery.Jcrop.min.css">

        <link rel="stylesheet" type="text/css" href="assets/backend/stylesheets/theme.css">
        <style type="text/css">
            .ui-dialog.ui-widget {
                font-size: 85%;
                padding:0;
                border-radius:0px;
            }
            .ui-dialog .ui-dialog-titlebar {
                border-radius:0;
                border:0;
            }
            .ui-dialog.ui-widget-content {
                -webkit-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                -moz-box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
                box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.3);
            }
        </style>
    </head>
    <body>
        <?php include("includes/header.php"); ?>

        <div class="content">
            <?php include("includes/main-menu.php");?>
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row-fluid">
                        <div class="span2">
                            <div class="page-heading">Manage Brands</div>
                        </div>
                        <div class="span10">
                            <div style="float:right">
                                <input type="button" class="btn btn-WI" value="Add New" onclick="window.location='/adminControl/brands/create'" />
                            </div>
                            <div style="float:right;margin-right:10px;">
                                <form id="filter" action="/adminControl/brands/index">
                                    <input type="text" name="key" placeholder="Search..." style="margin:0;" value="<?php echo $this->input->get('key')?>" id="key" />
                                    &nbsp;
                                    <input type="submit" value="Search" class="btn btn-WI" />
                                    <?php if($this->input->get('key')):?>
                                    &nbsp;
                                    <input type="button" value="Clear" class="btn" onclick="document.getElementById('key').value='';document.getElementById('filter').submit()" />
                                    <?php endif;?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-subtitle">Edit Brand</div>
                <div class="content-box">

                    <?php echo ($error)?'<div class="ui-state-error" style="padding:10px 5px 0;">'.$error.'</div>&nbsp;':''; ?>

                    <form method="post" enctype="multipart/form-data">
                        <div class="field row-fluid">
                            <label for="name" class="span2">Name *</label>
                            <div class="span10">
                                <input type="text" name="name" id="name" value="<?php echo set_value('name', $brand->name); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="logo" class="span2">Logo *<br /><small>Max of 1200 x 1200 pixels</small></label>
                            <div class="span10">
                                <img src="./custom/uploads/brands/<?php echo $brand->logo?>" width="100" /><br />
                                <input type="file" name="logo" id="logo" value="<?php echo set_value('logo'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="thumbnail" class="span2">Thumbnail *<br /><small>For brand listings<br />Max of 200 x 200 px</small></label>
                            <div class="span10">
                                <img src="./custom/uploads/brands/<?php echo $brand->thumbnail?>" width="100" /><br />
                                <input type="file" name="thumbnail" id="thumbnail" value="<?php echo set_value('thumbnail'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <!-- 12:5 aspect ratio -->
                            <label for="banner" class="span2">Banner<br /><small>Max of 1200 x 1200 pixels<br />Click image to crop</small></label>
                            <div class="span10">
                                <img src="/custom/uploads/brands/thumbnail/cms_<?php echo $brand->banner.'?'.time();?>" width="100" id="crop-banner" style="cursor:pointer" data-crop_target="/custom/uploads/brands/<?php echo $brand->banner?>" data-brand_id="<?php echo $brand->id?>" data-banner="<?php echo $brand->banner?>" /><br />
                                <input type="file" name="banner" id="banner" value="<?php echo set_value('banner'); ?>" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="location_id" class="span2">Location *</label>
                            <div class="span10">
                                <select name="location_id" id="location_id">	
                                        <?php echo $locations; ?>
                                </select>
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="description" class="span2">Description *</label>
                            <div class="span10">
                                <textarea name="description" id="description" class="wysiwyg simple"><?php echo set_value('description', $brand->description); ?></textarea>
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="status" class="span2">Status *</label>
                            <div class="span10">
                                <select name="status" id="status">
                                    <option value="Active" <?php echo set_select('status', 'Active', ($brand->status=='Active')?TRUE:FALSE ) ?>>Active</option>
                                    <option value="Inactive" <?php echo set_select('status', 'Inactive', ($brand->status=='Inactive')?TRUE:FALSE); ?>>Inactive</option>
                                </select>
                            </div>
                        </div>
						<div class="field row-fluid">
                            <label for="type" class="span2">Type *</label>
                            <div class="span10">
                                <select name="type" id="type">
                                    <option value="private" <?php echo set_select('type', 'private', ($brand->type=='private')?TRUE:FALSE ) ?>>Private</option>
                                    <option value="public" <?php echo set_select('type', 'public', ($brand->type=='public')?TRUE:FALSE); ?>>Public</option>
                                </select>
                            </div>
                        </div>
						
						 <div class="field row-fluid">
                            <label for="public_alias" class="span2">Public Alias *<small>(required if type is public)</small></label>
                            <div class="span10">
                                <input type="text" name="public_alias" id="public_alias" value="<?php echo set_value('public_alias', $brand->public_alias); ?>" />
                            </div>
                        </div>
						
						
                        <div class="actions row-fluid">
                            <div class="span2"></div>
                            <div class="span10">
                                <input type="submit" value="Save" class="btn btn-WI" name="save" />&nbsp;
                                <input type="reset" value="Reset" class="btn btn-WI" />&nbsp;
                                <input type="button" value="Template" class="btn btn-WI" id="edit-template" href="/adminControl/brands/template/<?php echo $brand->id?>" />&nbsp;
                                <input type="button" value="Cancel" class="btn btn-WI" onclick="window.location='/adminControl/brands'" />
                            </div>
                        </div>
                    </form>
                </div>

                <div class="content-box" style="margin-top:20px;">
                    <div class="page-subtitle" style="margin-top:5px;">
                        Products
                        <input type="button" value="Add Product" class="btn btn-WI" style="float:right;margin-top:-7px;right:0;" id="add-product" />
                    </div>

                    <table class="table table-hover" id="table-brandproducts">
                        <thead>
                            <tr>
                                <td style="font-weight:bold; width: 1em; text-align:center">ID</td>
                                <td style="font-weight:bold">Product</td>
                                <td style="font-weight:bold">Linked Merchant Products</td>
                                <td style="text-align:center;font-weight:bold;width:100px;">Options</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($products):?>
                            <?php foreach($products as $p):?>
                            <tr data-id="<?php echo $p->id?>">
                                <td class="record-id" style="text-align:right"><?php echo $p->id?></td>
                                <td class="record-thumbnail">
                                    <img src="/custom/uploads/brands/products/thumb_<?php echo $p->image_logo?>" style="width:60px" />&nbsp;
                                    <span><?php echo $p->name?></span></td>
                                <td class="record-linked-products">
                                    <?php if($p->linked_products_count):?>
                                    <div style="float:left;margin:1px;padding:5px"><?php echo $p->linked_products_count?></div>
                                    <ul style="margin:0;padding:0;">
                                        <?php foreach($p->linked_products as $lp):?>
                                        <li style="margin:1px;">
                                        <img src="/custom/uploads/products/thumb/<?php echo $lp->prod_image?>" width="30" alt="<?php echo $lp->merchant_name.': '.$lp->prod_name?>" title="<?php echo $lp->merchant_name.': '.$lp->prod_name?>" />
                                        </li>
                                        <?php endforeach;?>
                                    </ul>
                                    <?php endif;?>
                                </td>
                                <td class="record-options" style="text-align:center">
                                    <a href="/adminControl/brands/delete_product/<?php echo $p->id?>" class="delete-record" data-id="<?php echo $p->id;?>" title="Delete"><i class="icon-trash"></i></a>&nbsp;
                                    <a href="#" class="edit-record" data-id="<?php echo $p->id;?>" data-name="<?php echo $p->name?>" data-description="<?php echo $p->description?>" data-intro_text="<?php echo $p->intro_text?>" data-image_logo="<?php echo $p->image_logo?>" data-image_banner="<?php echo $p->image_banner?>" data-image_thumbnail="<?php echo $p->image_thumbnail?>" title="Edit"><i class="icon-edit"></i></a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <?php if($products):?>
                                <td colspan="4"></td>
                                <?php else:?>
                                <td colspan="4">No Records Found!</td>
                                <?php endif;?>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>


            </div>

        </div>

        <div id="save-product-dialog">
            <div id="tabs" class="ui-tabs">
                <ul class="ui-tabs-nav ui-widget-header ui-helper-clearfix">
                    <li class="ui-state-default ui-tabs-active"><a href="#tab-1" class="ui-tabs-anchor">Details</a></li>
                    <li class="ui-state-default ui-tabs-active merchant_products_toggle"><a href="#tab-2" class="ui-tabs-anchor">Merchant Products</a></li>
                </ul>

                <div id="tab-1" class="ui-tabs-panel">
                    <form method="post" id="form-save-brandproduct" action="/adminControl/brands/save_brandproduct" enctype="multipart/form-data">
                        <input type="hidden" name="brand_id" value="<?php echo $brand->id;?>" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="status" value="Active" />
                        <div class="field row-fluid">
                            <label for="name" class="span2">Name *</label>
                            <div class="span10">
                                <input type="text" name="name" id="name" value="" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="description" class="span2">Description *</label>
                            <div class="span10">
                                <textarea name="description" id="description" class="wysiwyg simple"></textarea>
                            </div>
                        </div>
                        <!--<div class="field row-fluid">
                            <label for="intro_text" class="span2">Intro Text</label>
                            <div class="span10">
                                <textarea name="intro_text" id="intro_text"></textarea>
                            </div>
                        </div>-->
                        <div class="field row-fluid">
                            <label for="name" class="span2">Image *</label>
                            <div class="span10">
                                <input type="file" name="image_logo" id="image_logo" />
                            </div>
                        </div>
                        <?php
                        /*<div class="field row-fluid">
                            <label for="image_banner" class="span2">Banner *</label>
                            <div class="span10">
                                <input type="file" name="image_banner" id="image_banner" />
                            </div>
                        </div>
                        <div class="field row-fluid">
                            <label for="image_thumbnail" class="span2">Thumbnail *</label>
                            <div class="span10">
                                <input type="file" name="image_thumbnail" id="image_thumbnail" />
                            </div>
                        </div>*/
                        ?>
                        <div class="field row-fluid">
                            <div class="span2"></div>
                            <div class="span10">
                                <input type="submit" name="submit" value="Submit" class="btn btn-WI" />
                            </div>
                        </div>
                    </form>
                </div>
                <div id="tab-2" class="ui-tabs-panel">
                    <form method="post" id="form-search-products" action="/adminControl/brands/search_for_products">
                        <input type="hidden" name="brand_id" value="<?php echo $brand->id;?>" />
                        <input type="hidden" name="brandproduct_id" value="" />
                        <input type="text" name="product_search_key" placeholder="Search Merchant Products..." style="margin:0;" />
                        <input type="submit" name="submit" value="Search" class="btn btn-WI" />
                    </form>
                    <div id="result-box">
                        <form method="get" id="form-link-products" action="/adminControl/brands/link_products">
                            <input type="hidden" name="brand_id" value="<?php echo $brand->id;?>" />
                            <input type="hidden" name="brandproduct_id" value="" />
                            <div class="table"></div>
                        </form>
                    </div>
                    <div id="merchant-product-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align:center; width:1em;">ID</th>
                                    <th>Product</th>
                                    <th>Merchant</th>
                                    <th style="text-align:center">Unlink</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="assets/backend/lib/jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script type="text/javascript" src="assets/backend/lib/jquery.form.min.js"></script>
        <script type="text/javascript" src="/assets/frontend/js/jquery.Jcrop.min.js"></script>
        <script src="//cdn.ckeditor.com/4.4.5/basic/ckeditor.js"></script>
        <script src="//cdn.ckeditor.com/4.4.5/full-all/adapters/jquery.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                var brand_id = <?php echo $brand->id?>;
                
                /* Add Product Form */
                var AddProductDialog = jQuery('#save-product-dialog');
                AddProductDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    draggable: false,
                    title: 'Product',
                    width: 800,
                    minHeight: 500,
                    maxHeight: 600,
                    buttons: {
                        'Close': function() {
                            AddProductDialog.dialog('close');
                        }
                    }
                }).parent().css({'position':'fixed'});
                
                jQuery('#add-product').on('click', function(){
                    jQuery('#form-save-brandproduct').find('input[name=id]').val('');
                    jQuery('#form-save-brandproduct').resetForm();
                    jQuery('#form-search-products').find('[name=brandproduct_id]').val('');
                    jQuery('#form-link-products').find('[name=brandproduct_id]').val('');
                    jQuery('.merchant_products_toggle').hide();
                    tabmenu.find('li:first-child a').trigger('click');
                    AddProductDialog.dialog('open');
                });
                jQuery(document).on('click','.edit-record', function(e){
                    e.preventDefault();
                    jQuery('#form-save-brandproduct').find('[name=id]').val(jQuery(this).attr('data-id'));
                    jQuery('#form-save-brandproduct').find('[name=name]').val(jQuery(this).attr('data-name'));
                    jQuery('#form-save-brandproduct').find('[name=description]').val(jQuery(this).attr('data-description'));
                    jQuery('#form-save-brandproduct').find('[name=intro_text]').val(jQuery(this).attr('data-intro_text'));
                    /*jQuery('#form-save-brandproduct').find('[name=image_logo]').val(jQuery(this).attr('data-image_logo'));
                    jQuery('#form-save-brandproduct').find('[name=image_banner]').val(jQuery(this).attr('data-image_banner'));
                    jQuery('#form-save-brandproduct').find('[name=image_thumbnail]').val(jQuery(this).attr('data-image_thumbnail'));*/

                    jQuery('#form-search-products').find('[name=brandproduct_id]').val(jQuery(this).attr('data-id'));
                    jQuery('#form-link-products').find('[name=brandproduct_id]').val(jQuery(this).attr('data-id'));
                    jQuery('.merchant_products_toggle').show();
                    tabmenu.find('li:first-child a').trigger('click');
                    
                    jQuery(document).trigger('list-linked-merchant-products');
                    
                    AddProductDialog.dialog('open');
                });
                jQuery(document).on('list-linked-merchant-products', function(){
                    jQuery('#merchant-product-list table tbody').html('');
                    jQuery.ajax({
                        url: 'adminControl/brands/list_linked_merchant_products',
                        data: jQuery('#form-link-products').serialize(),
                        dataType: 'json',
                        success: function(response) {
                            if( response.result == 'success' ) {
                                var html = '';
                                if(response.products.length > 0) {
                                    jQuery.each(response.products, function(i,o){
                                        html += '<tr data-id="'+o.id+'">';
                                        html += '<td style="text-align:right">'+o.id+'</td>';
                                        html += '<td>'+o.prod_name+'</td>';
                                        html += '<td>'+o.merchant_name+'</td>';
                                        html += '<td style="text-align:center"><a href="/adminControl/brands/unlink_product/" class="unlink-product btn btn-WI" data-id="'+o.id+'">Unlink</a></td>';
                                        html += '</tr>';
                                    });
                                }
                                jQuery('#merchant-product-list table tbody').html(html);
                            }   
                        }
                    });
                });
                jQuery(document).on('click','a.unlink-product', function(e){
                    e.preventDefault();
                    var id = jQuery(e.target).attr('data-id'),
                        href = jQuery(e.target).attr('href');
                    jQuery.getJSON( href, {'id':id}, function(response) {
                        if(response.result == 'success') {
                            console.log(response.message);
                            jQuery('#merchant-product-list tr[data-id='+id+']')
                                .html('<td colspan="4" style="text-align:center;" class="ui-state-error">'+response.message+'</td>');
                            setTimeout(function(){
                                jQuery('#merchant-product-list tr[data-id='+id+']').fadeOut();
                            },1000);
                        }
                    });
                });

                jQuery('#form-search-products').on('submit', function(e){
                    e.preventDefault();
                    var Form = jQuery(this);
                    jQuery.post( Form.attr('action'), Form.serialize() , function(response){

                        var ResultBox = jQuery('#result-box');
                        ResultBox.dialog({
                            autoOpen:false,
                            minWidth:600,
                            title: 'Select Product',
                            modal: true,
                            resizable: false,
                            maxHeight: 600,
                            buttons: {
                                'Link Selected' : function() {
                                    jQuery('#form-link-products').ajaxSubmit();
                                    if(response.result == 'success') {
                                        jQuery(document).trigger('list-linked-merchant-products');
                                        jQuery('#result-box').dialog('close');
                                    }
                                },
                                'Close' : function() {
                                    ResultBox.dialog('close');
                                }
                            }
                        }).css({'margin-top':'0'}).parent().css({'position':'fixed'});
                        

                        if(response.result == 'error') {
                            ResultBox.find('.table').html(response.message);
                            ResultBox.dialog('open');
                            return;
                        }

                        if(response.result == 'success') {
                            
                            var html = '<table width="100%" class="table table-hover">';

                            html += '<thead><tr>';
                            html += '<th><input type="checkbox" name="select_all" class="select_all" /></th>';
                            html += '<th>Product</th>';
                            html += '<th>Status</th>';
                            html += '<th>Merchant</th>';
                            html += '<th style="text-align:center">Link</th>';
                            html += '</tr></thead><tbody>';
                            jQuery.each(response.products, function(i,o) {
                                html += '<tr data-id="'+o.prod_id+'">';
                                html += '<td><input type="checkbox" name="product_id[]" value="'+o.prod_id+'" class="chkbox_product" /></td>';
                                html += '<td class="name" data-imagesrc="'+o.prod_image+'"><img src="/custom/uploads/products/thumb/'+o.prod_image+'" style="width:60px" /> <span>'+o.prod_name+'</span></td>';
                                html += '<td class="status">'+o.prod_status+'</td>';
                                html += '<td class="merchant" data-id="'+o.prod_merchant_id+'">'+o.merchant_name+'</td>';
                                html += '<td style="text-align:center">';
                                html += '<a href="/adminControl/brands/link_products/" class="btn btn-WI btn-link-product" data-id="'+o.prod_id+'">Link</a>';
                                html += '</td>';
                                html += '</tr>';
                            });
                            html += '</tbody><table>';

                            ResultBox.find('.table').html(html);
                            ResultBox.dialog('open');
                        }

                    }, 'json');
                    return false;
                });
                jQuery(document).on('click','.select_all', function(e){
                    if(e.target.checked) {
                        jQuery('.chkbox_product').each(function(i,o){
                            o.checked = true;
                        });
                    } else {
                        jQuery('.chkbox_product').each(function(i,o){
                            o.checked = false;
                        });
                    }
                });
                
                jQuery(document).on('click', 'a.btn-link-product', function(e){
                    e.preventDefault();
                    var prod_id = jQuery(this).attr('data-id');
                    jQuery.getJSON( jQuery(this).attr('href'), {'product_id[]':jQuery(this).attr('data-id')}, function(response) {
                        if(response.result == 'success') {
                            jQuery(document).trigger('list-linked-merchant-products');
                            jQuery('#result-box').dialog('close');
                        }
                    });
                });

                var DeleteDialog = jQuery('<div id="delete-dialog">Are you sure?</div>');
                DeleteDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Delete Product',
                    modal: true,
                    width:400,
                    draggable: false,
                    buttons: {
                        'Yes' : function(e) {

                            jQuery.getJSON( DeleteDialog.attr('data-link'), function(response) {
                                if(response.result=='success') {
                                    window.location = '/adminControl/brands/edit/'+brand_id;
                                } else {
                                    DeleteDialog.html(response.message);
                                }
                            });

                        },
                        'Cancel' : function() {
                            DeleteDialog.dialog('close');
                        }
                    }
                });

                jQuery(document).on('click','.delete-record', function(e){
                    e.preventDefault();
                    DeleteDialog.attr('data-link',jQuery(this).attr('href'));
                    DeleteDialog.dialog('open');

                });
                
                var tabsection = jQuery('#tabs');
                var tabmenu = tabsection.find('ul:first-child');
                var tabcontent = tabsection.find('> div');
                tabmenu
                    .css({'border':'0','float':'left','margin-left':'1em'})
                    .find('li a').each(function(i,o){
                    var id = jQuery(o).attr('href');
                    jQuery(o).css('cursor','pointer');
                    jQuery(id).hide();
                }).on('click',function(e){
                    e.preventDefault();
                    var id = jQuery(this).attr('href');
                    jQuery(id).show();
                    jQuery('#tabs > div:not('+id+')').hide();
                    jQuery(this).parent().addClass('ui-state-active')
                        .siblings().removeClass('ui-state-active');
                });
                tabmenu.find('li:first-child a')
                    .trigger('click');
                AddProductDialog
                    .parents('.ui-dialog')
                    .addClass('ui-tabs')
                    .find('.ui-dialog-titlebar')
                        .css('padding','0')
                    .find('> span')
                    .css({'width':'auto','padding':'0.5em 1em'})
                    .after(tabmenu);
                
                /* save brandproduct */
                jQuery('#form-save-brandproduct').ajaxForm({
                    dataType: 'json',
                    beforeSubmit: function() {
                        jQuery('#form-save-brandproduct .ui-state-error').removeClass('ui-state-error');
                    },
                    success: function( response ){
                        if(response.result == 'failed') {
                            jQuery.each(response.errors, function(i,o){
                                if(o != '') {
                                    jQuery('#form-save-brandproduct [name='+i+']').addClass('ui-state-error');
                                }
                            });
                        }
                        if(response.result == 'saved') {
                            var newRow = jQuery('<tr data-id="'+response.data.id+'"><td>'+response.data.id+'</td><td><img src="/custom/uploads/brands/products/'+response.data.image_thumbnail+'" style="width:60px" />&nbsp;'+response.data.name+'</td><td style="text-align:center"><a href="/adminControl/brands/delete_product/'+response.data.id+'" class="delete-record" data-id="'+response.data.id+'" title="Delete"><i class="icon-trash"></i></a>&nbsp;<a href="#" class="edit-record" data-id="'+response.data.id+'" title="Edit" data-name="'+response.data.name+'" data-description="'+response.data.description+'" data-intro_text="'+response.data.intro_text+'" data-image_logo="'+response.data.image_logo+'" data-image_banner="'+response.data.image_banner+'" data-image_thumbnail="'+response.data.image_thumbnail+'"><i class="icon-edit"></i></a></td></tr>');
                            jQuery('#table-brandproducts').prepend(newRow);
                            jQuery('#form-search-products').find('[name=brandproduct_id]').val(response.data.id);
                            jQuery('#form-link-products').find('[name=brandproduct_id]').val(response.data.id);
                            jQuery('#form-save-brandproduct').find('[name=id]').val(response.data.id);
                            jQuery('#form-save-brandproduct').find('[name=submit]').after('<span class="btn response_message ui-state-highlight"><strong>'+response.message+'</strong></span>');
                            setTimeout(function(){
                                jQuery('span.response_message').fadeOut();
                                jQuery('.merchant_products_toggle').toggle('pulsate');
                            },1000);
                        }
                        if(response.result == 'updated') {
                            var oldRow = jQuery('#table-brandproducts tr[data-id='+response.data.id+']');
                            oldRow.find('.record-thumbnail img').attr('src','/custom/uploads/brands/products/thumb_'+response.data.image);
                            oldRow.find('.record-thumbnail span').html(response.data.name);
                            oldRow.find('.edit-record')
                                .attr('data-name',response.data.name)
                                .attr('data-description',response.data.description)
                                .attr('data-intro_text',response.data.intro_text)
                                .attr('data-image_logo',response.data.image_logo)
                                .attr('data-image_banner',response.data.image_banner)
                                .attr('data-image_thumbnail',response.data.iamge_thumbnail);
                            
                            jQuery('#form-save-brandproduct').find('[name=submit]').after('<span class="btn response_message ui-state-highlight"><strong>'+response.message+'</strong></span>');
                            setTimeout(function(){
                                jQuery('span.response_message').fadeOut();
                            },1000);
                        }
                    }
                });
                
                
                var TemplateDialog = jQuery('<div id="template-dialog" />');
                TemplateDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Edit Template',
                    modal: true,
                    draggable: false,
                    width: 864,
                    buttons: []
                });
                jQuery(document).on('click', '#edit-template', function(e){
                    e.preventDefault();
                    jQuery.ajax({
                        url : jQuery(this).attr('href'),
                        dataType: 'json'
                    }).done(function(response){
                        var url = '/adminControl/templates/index/'+response.template.id;
                        var buttons = TemplateDialog.dialog('option','buttons');
                        TemplateDialog
                            .html('<iframe frameborder="0" scrolling="no" width="840" height="600" src="'+url+'"></iframe>')
                            .dialog('option','buttons',[{
                                text: "Mobile Preview",
                                click: function() {
                                    window.open('/m/brands/preview/'+response.template.referer_id, "", "width=320, height=700");
                                }
                            },{
                                text: "Desktop Preview",
                                click: function() {
                                    window.open('/brands/preview/'+response.template.referer_id);
                                }
                            },{
                                text: "Cancel",
                                click: function() {
                                    TemplateDialog.dialog('close');
                                }
                            }])
                            .dialog('open');

                    });
                });
                
                var CropBannerDialog = jQuery('<div id="crop-banner-dialog" />');
                CropBannerDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    title: 'Crop Banner',
                    modal: true,
                    draggable: false,
                    width: 800
                });
                /*CropBannerDialog.parent().css({position:"fixed"});*/
                jQuery(document).on('click','#crop-banner', function(){
                    
                    CropBannerDialog.html('');
                    
                    var $this = jQuery(this);
                    var imgTarget = $this.attr('data-crop_target');
                    var brand_id = $this.attr('data-brand_id');
                    var banner = $this.attr('data-banner');
                    var html = '';
                    var d = new Date;

                    html += '<img src="'+imgTarget+'?'+d.getTime()+'" id="jcrop-target" style="width:100%" />';
                    html += '<form method="post" action="/adminControl/brands/crop_banner/" enctype="multipart/form-data" id="crop-banner-form">';
                    html += '<input name="brand_id" value="'+brand_id+'">';
                    html += '<input name="banner" value="'+banner+'">';
                    html += '<input name="x" /><input name="y" /><input name="x2" /><input name="y2" /><input name="w" /><input name="h" />';
                    html += '</form>';
                    
                    CropBannerDialog.html(html);
                    
                    jQuery('#crop-banner-form').hide();
                    
                    jQuery('#jcrop-target').on('load',function(){
                        var img = new Image();
                        img.src = jQuery('#jcrop-target').attr('src');
                        
                        CropBannerDialog.dialog('open');

                        jQuery('#jcrop-target').Jcrop({
                            aspectRatio: 720 / 300,
                            trueSize: [img.naturalWidth,img.naturalHeight],
                            onSelect: function(c){
                                jQuery('input[name=x]').val(c.x);
                                jQuery('input[name=y]').val(c.y);
                                jQuery('input[name=x2]').val(c.x2);
                                jQuery('input[name=y2]').val(c.y2);
                                jQuery('input[name=w]').val(c.w);
                                jQuery('input[name=h]').val(c.h);
                            }
                        });
                        
                        CropBannerDialog.dialog('option','buttons',[{
                            text: 'Crop',
                            click: function() {
                                jQuery('#crop-banner-form').ajaxSubmit({
                                    success: function(response) {
                                        CropBannerDialog.dialog('close');
                                        
                                        jQuery.post('/adminControl/brands/crop_banner_thumbnail',{'banner':banner},function(){
                                            $this.attr('src','/custom/uploads/brands/thumbnail/cms_'+banner+'?'+d.getTime());
                                        });
                                    }
                                });
                            }
                        },{
                            text: 'Cancel',
                            click: function() {
                                CropBannerDialog.dialog('close');
                            }
                        }]);
                    });
                });
                
                /*jQuery('.wysiwyg.simple').ckeditor({
                    toolbar: [ {name: 'mode', items:['Source']}, { name: 'basicstyles', items: [ 'Bold', 'Italic' ] } ],
					toolbarGroups: [{name:'mode'}],
                    height: '100'
                });*/
				jQuery('.wysiwyg.simple').ckeditor({
                   	toolbarGroups: [						
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
						{ name: 'links' }
					],
                    height: '100'
                });
            });
        </script>
    </body>
</html>
