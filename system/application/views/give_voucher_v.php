<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Search Friends | Glomp</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->  
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">

        <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet">

        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script src="assets/frontend/js/custom.js" type="text/javascript"></script>

        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <script>
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_INVITE_REQUEST_SENT_BODY = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_BODY'); ?>";
            var GLOMP_INVITE_REQUEST_SENT_TITLE = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_TITLE'); ?>";
            var GLOMP_MESSAGE_JOIN = "<?php echo $this->lang->line('GLOMP_MESSAGE_JOIN'); ?>";
            var GLOMP_FB_FRIENDS_LIST_TITLE = "<?php echo $this->lang->line('GLOMP_FB_FRIENDS_LIST_TITLE'); ?>";
            var GLOMP_GETTING_FB_FRIENDS = "<?php echo $this->lang->line('GLOMP_GETTING_FB_FRIENDS'); ?>";
            var GLOMP_PUBLIC_IMAGE_LOC = "<?php echo base_url("assets/images/"); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_FB_POPUP_TITLE_ADD = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_ADD'); ?>";
            var GLOMP_FB_POPUP_TITLE_INVITE = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_INVITE'); ?>";
            var GLOMP_ADD_FRIEND_SUCCESS = "<?php echo $this->lang->line('GLOMP_ADD_FRIEND_SUCCESS'); ?>";
            var GLOMP_FB_INVITE_PRE_MESSAGE = "<?php echo $this->lang->line('GLOMP_FB_INVITE_PRE_MESSAGE'); ?>";
            var GLOMP_ASK_OPTION = "<?php echo $this->lang->line('GLOMP_ASK_OPTION'); ?>";
            var GLOMP_FB_INVITE_REMINDER = "<?php echo $this->lang->line('GLOMP_FB_INVITE_REMINDER'); ?>";



        </script>
        <!-- the javascript file has been moved the below file-->
        <script src="assets/frontend/js/custom.user_assign_voucher.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.ba-throttle-debounce.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".searchUser").nanoScroller();
<?php
if (isset($getFb) && $getFb == 'true') {
    echo 'askFbOption();';
}
?>
            });
        </script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>		
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="middleBorder topPaddingZero">
                        <div class="row-fluid searchPageHeader" >
                            <h1 class="glompFont"><?php echo $this->lang->line('Potential_Matches'); ?></h1>
                        </div>
                        <div class="row-fluid">
                            <div class="span7">
                                <div class="searchFriends">
                                    <form name="form1" id="searchForFriendForm" method="post" action="" style="float:left;width: 100%">
                                        <input class="fl"type="text" placeholder="Search or Find Friend" name="keywords" value="<?php //echo $keywords     ?>" id="searchForFriend">
                                        <div class="fl" style="border:0px solid;margin-left:20px;display:none" id="fb_paginator_wrapper">
                                            <div class="fl" style="border:0px solid; ">
                                                <button type="button" id="page_next" disabled  class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px 2px !important;" name="" >&nbsp;&gt;&nbsp;</button>
                                                <button type="button" id="page_prev" disabled class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px  2px !important;" name="" >&nbsp;&lt;&nbsp;</button>							
                                            </div>
                                        </div>
                                    </form>
                                    <div style="border:0px solid #000000; float:right; color:#FFFFFF; margin-top:2px; display:none"; id="fb_invite_button_wrapper" >
                                        <button type="button" id="invite_fb_friends" class="btn-custom-primary" name="" ><?php echo $this->lang->line('invite'); ?></button>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="chk_select_all" ><label style="margin-top:6px; float:right">&nbsp;Select All</label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="searchUser nano" id="searchUserScroller">
                                        <div class="content" id="friendsList" style="">
                                            <!-- end content -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span5">
                                <?php include('includes/glomp_side_form.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div style="position: absolute;top: 222px; height: 0px;">
                <div id ="addAsFriendPopUpID" class="addAsFriendPopUp" style="padding: 20px;display: none; position: relative; left: 329px;">
                    <h1 style="text-align: left;padding: 0px;">Oops!</h1>
                    <div class="nameHolder" style="width: auto;">
                        <p>
                            <span id ="member_name"></span> is already on glomp! Please select another friend to treat and invite to glomp!.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="btns" style="height: 20px;">
                        <form name="form45" method="post" action="">
                            <button type="button" class="btn-custom-gray addFren" id ="member_id" user_id ="">Add Friend</button>
                            <button type="button" class="btn-custom-gray _hide" name="Invite"><?php echo ucfirst($this->lang->line('Close')); ?></button>
                        </form>
                    </div>
                </div>
            </div>
            <script>
                $('._hide').click(function() {
                    $('#addAsFriendPopUpID').hide(100);
                });
                
                  $('#member_id').on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).html("<i class=\"icon-ok\"></i> Friend");
                    $.ajax({
                        type: "GET",
                        url: 'profile/add_friend2/' + $(this).attr('user_id'),
                        cache: false,
                        success: function(html) {
                            if (html == 'success') {
                            }
                        }

                    });
                });
            </script>
        </div>
    </body>
</html>