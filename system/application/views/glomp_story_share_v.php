<?php    
    $prod_info = json_decode($this->product_m->productInfo($product_id));    
    $merchant_name = $prod_info->product->$product_id->merchant_name;
    $prod_name = $prod_info->product->$product_id->prod_name;
    $prod_image= $prod_info->product->$product_id->prod_image;        
    $product_logo = base_url().$this->custom_func->product_logo($prod_image);
?>
<!DOCTYPE html>
<html>
     <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# glomp_app: http://ogp.me/ns/fb/glomp_app#">
        <title><?php echo $purchaser_data->user_name;?> glomp'ed <?php echo $belongs_data->user_name;?> to a <?php echo $merchant_name;?> <?php echo stripslashes($prod_name);?>.</title>      
<meta property="og:type" content="glomp_app:story" />		
        <meta property="og:title" content="glomp!" />
        <meta property="og:image" content="<?php echo $product_logo;?>" />
        <meta property="og:description" content="Up your &#39Like&#39-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!"/>
        <meta property="og:url" content="<?php echo base_url('glomp_story/share/'.$voucher.'?st='.$story_type);?>" />
        
        <meta property="glomp_app:story_title_ext"  content="a glomp' story - <?php echo $purchaser_data->user_name;?> glomp'ed <?php echo $belongs_data->user_name;?> to a <?php echo $merchant_name;?> <?php echo stripslashes($prod_name);?>" />
        
    </head>    
    <body style=" background:#d3d7e1;">
        <div>
            <?php echo $purchaser_data->user_name;?> glomp!ed <?php echo $belongs_data->user_name;?> to a <?php echo $merchant_name;?> <?php echo stripslashes($prod_name);?>.
        </div>
    </body>
    <script>
        setTimeout(function(){
           window.location = '<?php echo base_url('landing/?voucher='.$voucher); ?>';
        }, 1000);
    </script>
</html>