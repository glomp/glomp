<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Register | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">        
        <link href="assets/frontend/css/jcrop/jquery.Jcrop.css" rel="stylesheet">        		
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="assets/frontend/js/jquery.Jcrop.min.js"></script>		
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/validate.register.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <?php /* <script src="<?php echo minify('assets/frontend/js/custom.landing.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script> */ ?>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
    
    <!-- Facebook SDK init -->
    <script type="text/javascript">
        window.fbAsyncInit = function() {
            FB.init({
                appId      : FB_APP_ID,
                xfbml      : true,
                version    : 'v2.0',
                status     : true,
                cookie     : true
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>
<body>
    <?php include_once("includes/analyticstracking.php") ?>
    <div id="fb-root"></div>
    
    <?php $this->load->view('includes/header.php') ?>

    <div class="container">
        <div class="inner-content">
            <div class="row-fluid">
                <div class="outerBorder">
                    <div class="innerBorder">
                        <div class="register">
                            <div class="innerPageTitleRed glompFont"><?php echo $this->lang->line('Register'); ?></div>
                            <div class="content">


                                <!-- FORM STARTS HERE -->
                                <form action="<?php echo base_url('landing/register');?>" method="post" name="frmCms" class="form-horizontal" id="frmRegister" enctype="multipart/form-data" >
                                    
                                    <!-- FACEBOOK FIELDS -->
                                    <input type="hidden" name="fb_user_id" id="fb_user_id" value="<?php echo $this->input->get_post('fb_user_id')?>" />
                                    <input type="hidden" name="fb_fname" id="fb_fname" value="<?php echo $this->input->get_post('fb_fname')?>" />
                                    <input type="hidden" name="fb_mname" id="fb_mname" value="<?php echo $this->input->get_post('fb_mname')?>" />
                                    <input type="hidden" name="fb_lname" id="fb_lname" value="<?php echo $this->input->get_post('fb_lname')?>" />
                                    <input type="hidden" name="fb_email" id="fb_email" value="<?php echo $this->input->get_post('fb_email')?>" />
                                    <input type="hidden" name="fb_gender" id="fb_gender" value="<?php echo $this->input->get_post('fb_gender')?>" />
                                    <input type="hidden" name="fb_day" id="fb_day" value="<?php echo $this->input->get_post('fb_day')?>" />
                                    <input type="hidden" name="fb_month" id="fb_month" value="<?php echo $this->input->get_post('fb_month')?>" />
                                    <input type="hidden" name="fb_year" id="fb_year" value="<?php echo $this->input->get_post('fb_year')?>" />
                                    <input type="hidden" name="fb_birthdate" id="fb_birthdate" value="<?php echo $this->input->get_post('fb_birthdate')?>" />
                                    <input type="hidden" name="fb_picture" id="fb_picture" value="<?php echo $this->input->get_post('fb_picture')?>" />
                                    
                                    <!-- LINKEDIN FIELDS -->
                                    <input type="hidden"  id="li_user_id" name="li_user_id" value="<?php echo $this->input->get_post('li_user_id')?>" />
                                    <input type="hidden"  id="li_emailAddress" name="li_emailAddress" value="<?php echo $this->input->get_post('li_emailAddress')?>" />
                                    <input type="hidden"  id="li_firstName" name="li_firstName" value="<?php echo $this->input->get_post('li_firstName')?>" />
                                    <input type="hidden"  id="li_lastName" name="li_lastName" value="<?php echo $this->input->get_post('li_lastName')?>" />

                                    <!-- TITLE -->
                                    <div class="row-fluid"><div class="span12">
                                        <h1><?php echo $this->lang->line('Personal_Details'); ?></h1>
                                    </div></div>

                                    <!-- MESSAGE PROMPT -->
                                    <div id="register_error"></div>
                                    <?php if( validation_errors() ): ?>
                                    <div id="validation-errors" style="display:none"><div id="errors-inner" class="alert alert-error" style="font-size:12px !important; margin: 0px !important;"><?php echo validation_errors()?></div></div>
                                    <?php endif;?>

                                    <!-- FIELDS -->
                                    <div class="row-fluid">

                                        <!-- LEFT Fields -->
                                        <div class="span6">

                                            <!-- fname -->
                                            <div class="control-group">
                                                <label class="control-label" for="name">
                                                    <?php echo $this->lang->line('Fname'); ?><span class="required">*</span>
                                                </label>
                                                <div class="controls">
                                                    <input type="text"  id="fname" name="fname" value="<?php echo $this->input->post( 'fname' ); ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>

                                            <!-- lname -->
                                            <div class="control-group">
                                                <label class="control-label" for="lname"><?php echo $this->lang->line('Lname'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text"  id="lname" name="lname" value="<?php echo $this->input->post( 'lname' ); ?>" class="span11 textBoxGray" >
                                                </div>
                                            </div>

                                            <!-- birthdate -->
                                            <div class="control-group">
                                                <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="day" name="day" maxlength="2" value="<?php echo $this->input->post( 'day' ); ?>"  class="span2 textBoxGray" placeholder="DD"> /
                                                    <input type="text" id="months" name="month" value="<?php echo $this->input->post( 'month' ); ?>" maxlength="2"  class="span2 textBoxGray" placeholder="MM"> /
                                                    <input type="text" id="year" name="year" maxlength="4" value="<?php echo $this->input->post( 'year' ); ?>"  class="span3 textBoxGray" placeholder="YYYY">
                                                </div>
                                            </div>

                                            <!-- birthdate format -->
                                            <div class="control-group">
                                                <label class="control-label" for="display">Display</label>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" name="dob_display" id="dob_display_w_year" value="dob_display_w_year" <?php echo ($this->input->post('dob_display') == 'dob_display_w_year' || $this->input->post('dob_display') == '') ? 'Checked="checked"' : ""; ?> /> DD / MM / YYYY
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" name="dob_display" id="dob_display_wo_year" <?php echo ($this->input->post('dob_display') == 'dob_display_wo_year') ? 'Checked="checked"' : ""; ?> value="dob_display_wo_year"> DD / MM
                                                    </label>
                                                </div>
                                            </div>

                                            <!-- gender -->
                                            <div class="control-group">
                                                <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender'); ?><span class="required"></span></label>
                                                <div class="controls">
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo ($this->input->post('gender') != 'Female') ? '' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                                    </label>
                                                    <label class="radio inline">
                                                        <input type="radio" name="gender" id="gender-female" <?php echo ($this->input->post('gender') == 'Female') ? '' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <!-- location -->
                                            <div class="control-group">
                                                <label class="control-label" for="location"><?php echo $this->lang->line('Location'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input class="textBoxGray" type="text" name="location_text" id="location" value="<?php echo $this->input->post('location_text'); ?>" style="width:250px"  autocomplete="off" placeholder="">
                                                    <input type="hidden" name="location" id="location_id" value="<?php echo $this->input->post('location'); ?>" autocomplete="off" class="" >
                                                    <div class="sutoSugSearch" id="autoSugSearch" style="color:#59606B !important;margin-left:20px;width:244px">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- RIGHT Fields -->
                                        <div class="span5">
                                            <?php
                                            /* to check user agent*/
                                            $ua = $this->custom_func->browser_info();
                                            if(isset($ua['_browser']) && $ua['_browser'] == 'msie' && $ua['_version'] <= 9.0)
                                            {
                                                include_once("includes/register_for_ie9_v2.php");
                                            } /*if( $ua['_browser'] == 'msie')*/
                                            else 
                                            {
                                            ?>
                                                <div class="uploadImage" style="height:140px;width:140px;overflow:visible; border:0px solid">
                                                <img src="assets/frontend/img/default_profile_pic_register.jpg" alt="" id="profile_pic" style="cursor:pointer;"  />
                                                </div>
                                                <div class="upload">
                                                    <span id="uploadFile" style="cursor:pointer;" ><?php echo $this->lang->line('upload_profile_photo'); ?> <span class="required">*</span></span>
                                                    
                                                    <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note'); ?></p>
                                                </div>
                                                <input type="hidden" name="coords" value="" />
                                                <input type="file" name="user_photo" accept="image/*" style="visibility:hidden;" id="user_photo" />
                                            <?php
                                            } /* else on if( $ua['_browser'] == 'msie')*/
                                            ?>
                                        </div>
                                    </div>

                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>

                                    <!-- FACEBOOK LINK-->
                                    <div class="row-fluid">
                                        <div class="span6" >
                                            <button type="button" class="btn-custom-blue glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_Facebook" id="Link_Facebook" >Link Facebook account</button>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <!-- LINKEDIN LINK -->
                                    <div class="row-fluid">
                                        <div class="span6" >
                                            <button type="button" class="btn-custom-007BB6 glompFont" style="font-weight:normal !important;border-radius: 0px !important; width:383px !important" name="Link_LinkedIn" id="Link_LinkedIn" onclick ="liAuth()" >Link LinkedIn Account</button>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">						
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="email"><?php echo $this->lang->line('email'); ?><span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="email"  id="email" value="<?php echo $this->input->post('email'); ?>" name="email" class="span11 textBoxGray" rel="popover" data-content="<?php echo $this->lang->line('emailTips'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="pword"><?php $temp = $this->lang->line('Create_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right">
                                                            <input type="password" maxlength="9"  id="pword" name="pword" class="span11 textBoxGray" value="<?php echo $this->input->post('pword'); ?>" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" > </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label" for="cpword"><?php $temp = $this->lang->line('Confirm_Password');
                                    echo str_replace(' ', '&nbsp;', $temp) ?><span class="required">*</span></label></td>
                                                        <td align="right"><input maxlength="9" type="password" id="cpword" name="cpword" class="span11 textBoxGray" value="<?php echo $this->input->post('cpword'); ?>" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" ></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <div class="createPasswordTips">
                                                <?php echo $this->lang->line('passwors_tips'); ?><br /><br />
                                                <?php echo $this->lang->line('passwors_tips_length','(6-9 characters)'); ?>
                                            </div>&nbsp;
                                        </div>
                                    </div>

                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="createPasswordTable" >
                                                <table width="100%" border="0" >
                                                    <tr>
                                                        <td><label class="control-label" for="promo_code"><?php $temp = $this->lang->line('Promotional_Code');
                                    echo str_replace(' ', '&nbsp;', $temp) ?></label></td>
                                                        <td align="right">
                                                            <input type="text" maxlength="45"  id="promo_code" name="promo_code" placeholder="(optional)" class="span11 textBoxGray" value="<?php echo $this->input->post('promo_code'); ?>" >
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </div>
                                        </div>                                        
                                    </div>
                                    
                                    <!-- seperator --><div class="row-fluid"><div class="span12 border"></div></div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <label class="checkbox">
                                                <input type="checkbox" name="agree">
<?php echo $this->lang->line('I_Agree_to_the'); ?><strong> 
<?php echo $this->lang->line('termNconditi0n'); ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('index.php/page/index/terms'); ?>" target="_blank"><strong><?php echo $this->lang->line('Read'); ?></strong></a> <br/><br/>
                                            </label>

                                            <br/><br/>

                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="control-group">
                                                <input type="hidden" name="token" value="<?php echo $token; ?>" />
                                                <button type="submit" id="register_btn" class=" btn-custom-reg" style=" padding:10px 40px; font-size:14px;" name="Register" ><?php echo $this->lang->line('Submit'); ?></button>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="span5" style="">
                                            <button type="reset" style="margin-left:3px; padding:10px 19px; font-size:14px;" class="btn-custom-reset" name="Reset" id="reset" ><?php echo $this->lang->line('Reset_All_Fields'); ?></button>
                                        </div>
                                    </div>

                                
                                </form>
                                <!-- FORM ENDS HERE -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- generic dialog box -->
    <div id="dialog" title="">
        <div id="dialog-content" style="line-height:120%; font-size:16px" align="center"></div>
    </div>
    
    <script type="text/javascript">

        /* TODO: REMOVE THIS QUICK FIX */
        <?php if (isset($_GET['fname'])) { ?>
            $('#email').val('<?php echo $_GET['email']; ?>');
            $('#fname').val('<?php echo $_GET['fname']; ?>');
            $('#lname').val('<?php echo $_GET['lname']; ?>');
        <?php } ?>
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            // Great success! All the File APIs are supported.
        } else {
            console.log('Your image preview is NOT supported.');
        }
        jQuery('#uploadFile, .uploadImage').on('click', function(e){
            jQuery('#user_photo').click();
        });
        
        
        /* Generic Dialog Box */
        function prompt( msg, titleYes, titleNo, callbackYes, callbackNo ){
            jQuery('#dialog').dialog({
                dialogClass: 'noTitleStuff',
                resizable: false,
                modal: true,
                buttons: [
                    {
                        text: titleYes,
                        "class": 'btn-custom-darkblue',
                        click: callbackYes
                    },
                    {
                        text: titleNo,
                        "class": 'btn-custom-white2',
                        click: callbackNo
                    }
                ]
            }).find('div').html(msg);
        }
        
        function clearFacebookFields() {
            jQuery('#fb_user_id, #fb_lname, #fb_mname, #fb_fname, #fb_email, #fb_gender, #fb_birthdate').val('');
        }
        
        function clearLinkedinFields() {
            jQuery('#li_user_id, #li_emailAddress, #li_firstName, #li_lastName').val('');
        }
        
        function clearSocialMediaAutofillFields() {
            jQuery('#fname, #lname, #day, #months, #year, #email').val('');
        }
        
        var ImageTooLarge = jQuery('<div id ="ImageTooLarge" style="font-size:12px; text-align:center">Your photo has reached the size limit. PLease select a new photo smaller than 2000 x 2000 pixels.</div>');
        ImageTooLarge.dialog({
            autoOpen: false,
            closeOnEscape: false ,
            resizable: false,					
            dialogClass:'dialog_style_glomp_wait noTitleStuff ',
            modal: true,
            title:'Please wait...',
            position: 'center',
            width:280,                                
            buttons:[
                {text: "Close",
                "class": 'btn-custom-green w200px2',
                click: function() {
                    $(this).dialog("close");
                }}
            ]
        });

        /* IMAGE PREVIEW */
        var file, img, reader;
        
        jQuery('#user_photo').on('change', function(e) {
            file = e.target.files[0];
            img = document.createElement("img");
            reader = new FileReader();
            
            // for dimensions and size
            img.file = file;
            reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(file); 
            
            // for preview
            var url = window.URL.createObjectURL(file);
            
            
            // for cropping
            img.onload = function() { 
            // max-dimension: 2000x2000
            console.log(img.width);
                if(img.width > 2000 || img.height > 2000 ) {
                    ImageTooLarge.dialog('open');
                    jQuery(this).val('');
                    return;
                }
                
                if(img.width > 200 || img.height > 200 ) {
                    
                    // huge dialog box for cropping
                    var cropBox = jQuery('<div id="jCropDialog" />');
                    var imgBox = jQuery('<img id="tempImage" />');
                        
                    cropBox
                        .append(imgBox)
                        .dialog({
                            dialogClass:'dialog_style_glomp_wait noTitleStuff',
                            modal: true,
                            resizable: false,
                            position: [ 'center', 'top+80' ],
                            buttons: [
                                {
                                    text: "Crop",
                                    'class': "btn-custom-blue",
                                    click: function() {
                                        var data =  jQuery('[name=coords]').val().split(','),
                                            percentile = ( 140 / data[4] ) * 100, // from width
                                            computedW = data[6] * (percentile/100), // width
                                            computedH = data[7] * (percentile/100), // height
                                            computedT = -data[1] * (percentile/100), // Top
                                            computedL = -data[0] * (percentile/100); // Left
                                        jQuery(cropBox).dialog('close');
                                        jQuery('#profile_pic')
                                            .attr("src",url)
                                            .attr("width", computedW)
                                            .attr("height", computedH)
                                            .css({"max-width":"none","height":"auto"}) //--> because of stupid bootstrap
                                            .css({"position":"absolute","top":computedT,"left":computedL})
                                            .parent().css({"position":"relative","overflow":"hidden"}); 
                                    }
                                },
                                {
                                    text: "Cancel",
                                    'class': "btn-custom-white2",
                                    click: function() {
                                        jQuery(cropBox).dialog('close');
                                    }
                                }
                            ]
                        });
                    
                    imgBox
                        .attr("src", url)
                        .removeAttr('style')
                        .Jcrop({
                            aspectRatio: 1 / 1,
                            minSize: [140,140],
                            maxSize: 0,
                            setSelect: [0,0,140,140],
                            onChange: function(c) {
                                var imgW = parseInt(imgBox.css('width').replace('px','')),
                                    imgH = parseInt(imgBox.css('height').replace('px',''))
                                jQuery('[name=coords]').val(
                                    c.x+","+c.y+","+c.x2+","+c.y2+","+c.w+","+c.h+"," +
                                    imgW+","+imgH
                                );
                            }
                        });
                } else {
                    jQuery('#profile_pic').attr("src",url);
                }
                jQuery('#fb_picture').val('');
            }
            
            // cleanup
            window.URL.revokeObjectURL(file);
        });

        /* FORM VALIDATION */
        $("#frmRegister").validate({
            rules: {
                fname: "required",
                lname: "required",
                day: {
                    required: true,
                    minlength: 2,
                    maxlength: 2,
                    number: true,
                    range: [01, 31]
                },
                month: {
                    required: true,
                    minlength: 2,
                    maxlength: 2,
                    number: true,
                    range: [01, 12]
                },
                year: {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    number: true,
                    range: [1900, 3000]
                },
                gender: {required: false},
                location_text: "required",
                email: {
                    required: true,
                    email: true
                },
                pword: "required",
                cpword: "required",
                user_photo: {
					required: function() {
						if( jQuery('#profile_pic').attr('src') == '' || jQuery('#profile_pic').attr('src').indexOf('default_profile_pic') > -1 ) {
							return true;
						}
						return false;
					}
				},
                agree: "required"
            }
            ,
            messages: {
                fname: "Incorrect or missing entry. Please re-enter the red boxes.",
                lname: "Incorrect or missing entry. Please re-enter the red boxes.",
                day: "Incorrect or missing entry. Please re-enter the red boxes.",
                month: "Incorrect or missing entry. Please re-enter the red boxes.",
                year: "Incorrect or missing entry. Please re-enter the red boxes.",
                gender: "Incorrect or missing entry. Please re-enter the red boxes.",
                location_text: "Incorrect or missing entry. Please re-enter the red boxes.",
                email: "Incorrect or missing entry. Please re-enter the red boxes.",
                pword: "Incorrect or missing entry. Please re-enter the red boxes.",
                cpword: "Incorrect or missing entry. Please re-enter the red boxes.",
                agree: "Incorrect or missing entry. Please re-enter the red boxes.",
                user_photo: "You have not attached a profile picture."
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = errors == 1 ? 'You missed 1 field. It has been highlighted below' : 'You missed ' + errors + ' fields.  They have been highlighted below';
                    /*$("div.error span").html(message);
                    $( "#dialog-form" ).dialog( "open" );
                    */
                    var NewDialog =$('#register_error');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_error_alerts ',
                        modal: true,
                        title:'Please wait...',
                        position: [ 'center', 'top' ],
                        width:400,                                
                        buttons:[
                            {text: "Close",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#waitPopup').dialog('destroy').remove();
                                }, 500);
                            }}
                        ]

                    });
                    NewDialog.dialog('open');
                }

            },
            onkeyup: false,
            onfocusout:false
         });
        
        /* FACEBOOK ACCOUNT MERGING */
        var fbId;
        jQuery('#Link_Facebook').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var facebookDialog = jQuery('<div id="facebookConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">Connecting to Facebook...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
            facebookDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    title: '',
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 300,
                    height: 110,
                    show: '',
                    hide: ''
                })
            
            function propagateFacebookFields() {
                FB.api('/me', function(response){
                    clearLinkedinFields();
                    clearSocialMediaAutofillFields();
                    jQuery('#fb_user_id').val(response.id);
                    jQuery('#fb_email').val(response.email);
                    jQuery('#fb_gender').val(response.gender);
                    jQuery('#fb_fname').val(response.first_name);
                    jQuery('#fb_mname').val(response.middle_name);
                    jQuery('#fb_lname').val(response.last_name);
                    jQuery('#fb_birthdate').val(response.birthday);
                    
                    checkRecords();
                    facebookDialog.dialog('close');
                });
            }
            
            function useFacebookPicture() {
                FB.api('/me/picture?width=140&height=140', function(response){
                    jQuery('#profile_pic').attr("src",response.data.url);
                    jQuery('#fb_picture').val(response.data.url);
                });
            }
            
            function useFacebookInfo() {
                jQuery('#fname').val( jQuery('#fb_fname').val() );
                jQuery('#lname').val( jQuery('#fb_lname').val() );
                jQuery('#email').val( jQuery('#fb_email').val() );
                if( jQuery('#fb_gender').val() == 'male' )
                    jQuery('#gender-male').attr('checked','checked');
                if( jQuery('#fb_gender').val() == 'female' )
                    jQuery('#gender-female').attr('checked','checked');
                var dob = jQuery('#fb_birthdate').val().split("/");
                jQuery('#day').val(dob[1]);
                jQuery('#months').val(dob[0]);
                jQuery('#year').val(dob[2]);
            }
            
            function checkRecords() {
                jQuery.ajax({
                    type: "POST",
                    url: 'ajax_post/checkIfUserHasAnExistingAcct/?id=' + jQuery('#fb_user_id').val(),
                    data: 'fbID=' + jQuery('#fb_user_id').val(),
                    dataType: 'json'
                }).done(function(response){
                    
                    if(response.data == 1 && response.data_status == 'Active') {
                        jQuery("#dialog-content").html(SIGNUP_WITH_FB_BUT_REG);
                        jQuery('#dialog').dialog({
                            dialogClass: 'noTitleStuff',
                            resizable: false,
                            modal: true,
                            buttons: [
                                {
                                    text: "Login with facebook",
                                    "class": 'btn-custom-darkblue',
                                    click: function() {
                                        var params = {
                                                id: jQuery('#fb_user_id').val(),
                                                first_name: jQuery('#fb_fname').val(),
                                                middle_name: jQuery('#fb_mname').val(),
                                                last_name: jQuery('#fb_lname').val(),
                                                gender: jQuery('#fb_gender').val(),
                                                email: jQuery('#fb_email').val(),
                                                birthday: jQuery('#fb_birthdate').val(),
                                                fb_login: 'fb_login'
                                            }
                                        
                                        jQuery.ajax({
                                            type: 'post',
                                            url:  GLOMP_BASE_URL + 'landing',
                                            data: params,
                                            dataType: 'json'
                                        });
                                        jQuery(this).dialog('close');
                                        window.setTimeout(function(){
                                            window.location = GLOMP_BASE_URL + 'user/dashboard';
                                        }, 2000);
                                    }
                                },
                                {
                                    text: "Cancel",
                                    "class": 'btn-custom-white2',
                                    click: function() {
                                        clearFacebookFields();
                                        jQuery(this).dialog('close');
                                    }
                                }
                            ]
                        });
                    } else {
                        useFacebookPicture();
                        useFacebookInfo();
                    }
                });
            }
            
            FB.getLoginStatus(function(response){
                if(response.status != 'connected') {
                    FB.login(function(response){
                        if(response.status == 'connected') {
                            propagateFacebookFields();
//                            window.setTimeout(function(){
//                                checkRecords();
//                                facebookDialog.dialog('close');
//                            }, 2000);
                        } else {
                            facebookDialog.dialog('close');
                        }
                    },{scope: 'publish_actions,email'});
                } else {
                    propagateFacebookFields();
//                    window.setTimeout(function(){
//                        checkRecords();
//                        facebookDialog.dialog('close');
//                    }, 2000);
                }
            });
        });

        if( jQuery('input[name=fb_user_id]').val() != '' ) {
            window.fbAsyncInit();
            jQuery('#Link_Facebook').trigger('click');
        }

        /* LINKEDIN ACCOUNT MERGING */
        function liInitOnload() {
            if (IN.User.isAuthorized()) {
                //NO NEED TO DO ANYTHING        
            }
        }
        function liAuth() {
            IN.User.authorize(function() {
                //If authorized and logged in
                var params = {
                    type: 'linked',
                    type_string: 'LinkedIn',
                    func: function(obj) {
                        return login_linkedIN(obj);
                    }
                };

                processIntegratedApp(params);
            });
        }
        function processIntegratedApp(params) {
            var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
                    Connecting to ' + params.type_string + '...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
            </div></div>');

            connectDialog.dialog({
                dialogClass: 'noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 300,
                height: 120,
                show: '',
                hide: ''
            });

            connectDialog.dialog("open");
            params.func(connectDialog);
        }
        /* @todo remove ajax_post/processIntegratedApp controller function */
        function login_linkedIN(obj) {
            IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {
                
                var hasActiveRecord = checkIfIntegrated({
                    id: data.values[0].id,
                    field: 'user_linkedin_id'
                });
                
                
                if( hasActiveRecord ) {
                    prompt(SIGNUP_WITH_FB_BUT_REG,'Login with LinkedIn','Cancel',
                        function(){
                            var params = {
                                    id: jQuery('#li_user_id').val(),
                                    email: jQuery('#li_emailAddress').val(),
                                    middle_name: jQuery('#li_firstName').val(),
                                    last_name: jQuery('#li_lastName').val(),
                                    field: 'user_linkedin_id',
                                    mode: 'linkedIN',
                                    device: 'desktop',
                                    data: [{
                                        '_key': '~',
                                        'emailAddress': jQuery('#li_emailAddress').val(),
                                        'firstName': jQuery('#li_firstName').val(),
                                        'lastName': jQuery('#li_emailAddress').val(),
                                        'id': jQuery('#li_user_id').val()
                                    }]
                                }

                            jQuery.ajax({
                                type: 'post',
                                url:  GLOMP_BASE_URL + 'ajax_post/processIntegratedApp',
                                data: params,
                                dataType: 'json'
                            });
                            jQuery(this).dialog('close');
                            window.setTimeout(function(){
                                window.location = GLOMP_BASE_URL + 'user/dashboard';
                            }, 2000);                            
                        },
                        function(){
                            jQuery('#dialog').dialog('close').dialog('destroy');
                            obj.dialog('close');
                        }
                    );
                } else {
                    
                    propagateLinkedinFields(data);
                    
                    useLinkedinInfo();
                    useLinkedinPicture();
                    
                    obj.dialog('close');
                }
                
            });
        }
        function checkIfIntegrated(param) {
            var ret = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: 'ajax_post/checkIfIntegrated',
                data: {
                    id: param.id,
                    email: param.email,
                    field: param.field,
                },
                success: function(res) {
                    if (res) {
                        ret = true;
                    } else {
                        ret = false;
                    }
                }
            });

            return ret;
        }
        function propagateLinkedinFields(data) {
            clearFacebookFields();
            clearSocialMediaAutofillFields();
            jQuery('#li_user_id').val(data.values[0].id);
            jQuery('#li_emailAddress').val(data.values[0].emailAddress);
            jQuery('#li_firstName').val(data.values[0].firstName);
            jQuery('#li_lastName').val(data.values[0].lastName);
        }
        function useLinkedinInfo(){
            jQuery('#fname').val( jQuery('#li_firstName').val() );
            jQuery('#lname').val( jQuery('#li_lastName').val() );
            jQuery('#email').val( jQuery('#li_emailAddress').val() );
        }
        function useLinkedinPicture() {
            IN.API.Raw("/people/~/picture-urls::(original)").result(function(r){ 
                $("#profile_pic").prop("src", r.values);
                $("#fb_picture").val(r.values);
            });        
        }
        
    jQuery(window).load(function(){
		
        var ValidationErrorsPrompt = jQuery('#validation-errors');
        if(ValidationErrorsPrompt.length > 0) {
ValidationErrorsPrompt.dialog({
                autoOpen: true,
                closeOnEscape: false ,
                resizable: false,					
                dialogClass:'dialog_style_error_alerts noTitleStuff ',
                modal: true,
                title:'Please wait...',
                position: 'center',
                width:400,
                buttons:[
                    {text: "Close",
                    "class": 'btn-custom-white2',
                    click: function() {
                        $(this).dialog("close");
                    }}
                ]
            });
        }
        

        var prompt = jQuery('<div />');
        prompt.dialog({
            autoOpen:false,
            resizable:false,
            dialogClass:'dialog_style_error_alerts noTitleStuff ',
            modal:true,
            width:300,
            buttons:[
                {
                    text: 'Close',
                    class: 'btn-custom-white2',
                    click: function(){
                        prompt.html('');
                        prompt.dialog('close');
                    }
                }
            ]
        });
        jQuery('#pword,#cpword').on('keydown',function(e){
            var k = e.keyCode
            if(k==9||k==8||k==20||k==18||k==17||k==16||k==13||k==27||k==91) return;
            if(e.target.value.length == 9) {
                prompt.html( '<div class="_error alert alert-error">'+jQuery(e.target).attr('prompt-max-reached')+'</div>' );
                prompt.dialog('open');
            }
        });
    });
        
    </script>
</body>
</html>
