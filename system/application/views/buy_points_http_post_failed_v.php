<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('Buy_points');?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
     <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script src="assets/frontend/js/custom.js" type="text/javascript"></script>
</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
<div class="outerBorder">
	<div class="inner-content">
      <div class="clearfix"></div>
      <div class="row-fluid">
      <div class="span12">
       <div class="middleBorder topPaddingZero">
            <div class="buyPoints">      
                <div class="row-fluid">
                    <div class="span12">
                        <div class="content alert alert-success" align="center" style="margin:130px 0px;padding:30px 10px;font-size:14px" id="message_content">
                            <span style="font-size:14px" >Sorry but there was an unexpected error. Please <a href="<?php echo base_url('buyPoints');?>">try again</a>.</span>
                        </div> 
                    </div>
                </div>        
            </div>
      </div>
      </div>
      </div>
      </div>
      </div>      
</div>
</body>
</html>