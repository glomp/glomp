<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('Support');?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script src="assets/frontend/js/validate.js" type="text/javascript"></script>

<script type="text/javascript">
	$().ready(function() {
		/* validate signup form on keyup and submit*/
		$("#frmSupport").validate({
			rules: {
				subject	:"required",
				message:"required"
				}
				,
			messages: {
				subject:"",
				message	:""
			}
	});
	});
	</script>

</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      <div class="innerBorder">
      	<div class="register">
                    <div class="innerPageTitleWhite glompFont"><?php echo $this->lang->line('contact_support');?>
                   </div>
                    <div class="content">
                      <?php
					 if(isset($success_msg)){
					  ?>
                        <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $success_msg;?>
    </div>
    <?php
	}
	if(validation_errors()!=""){
	?>
    <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $validation_errors();?>
    </div>
    <?php 
	}
	?>
    
     <form method="post" name="frmSupport" id="frmSupport">
    <table width="100%" border="0">
  <tr>
    <td><strong><?php echo $this->lang->line('Subject');?>*</strong></td>
  </tr>
   <tr>
    <td><input type="text" id="subject" name="subject" class="span8 textLightBoxGray" placeholder=""></td>
  </tr>
   <tr>
    <td> <strong> <?php echo $this->lang->line('Description');?>*</strong> </td>
  </tr>
   <tr>
    <td><textarea name="message" id="message" rows="10" class="span8 textLightBoxGray"></textarea> </td>
  </tr>
  <tr>
    <td><strong><?php //echo $this->lang->line('required_field');?><strong>&nbsp;</td>
  </tr>
  <tr>
  	<td align="center">&nbsp;&nbsp;&nbsp;<button type="submit" name="sendMessage" class="btn-custom-support"><?php echo $this->lang->line('Submit');?></button></td>
  </tr>
</table>

    </form>
                    </div>
                </div>
       </div>
      </div>
      </div>
      </div>
</div>
</body>
</html>