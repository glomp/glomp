<style type="text/css">
.templateWrapper{ width:595px; border:2px solid #eee; min-height:400px;}
.header{ background:url(<?php echo base_url();?>/assets/frontend/img/email_template_header_bg.jpg); height:133px; width:595px;}
.body{ padding:30px;}
.body a{ color:#0072bc;}
.body p{ margin:5px; padding:5px;}
</style>
<div class="templateWrapper">
<div class="header">&nbsp;</div>
<div class="body">
<p>Its time for a treat! Congratulations, you have been treated to a <a href="#">[insert merchant brand and product]</a> from glomp! glomp! is a new online and mobile social networking platform where you can give and receive enjoyable treats such as a coffee, ice-cream, a beer and so on from a host of quality retail and service outlets. We cordially invite you to join glomp! to redeem and enjoy your <a href="#">[insert merchant brand and product]</a>. </p>
<p>In fact, we are treating you to 2 <a href="#">[insert merchant brand and product]</a>! One is for you to enjoy yourself and the other is for you to treat (we say glomp!) one of your friends with! Only thing is that your friend must not yet be a glomp! member.</p>
<p>You have 7 days to accept this offer, so get started and <a href="#">sign up</a> now!</p>
</div>
</div>