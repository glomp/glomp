<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('Buy_points');?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
     <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script src="assets/frontend/js/custom.js" type="text/javascript"></script>
<script type="text/javascript">
$().button('loading');
$(document).ready(function(e) {
    $('.glompSelPoints').on('click',function(){
		var _this = $(this);
		var _id = _this.data('id');
		var _value = _this.data('value');
		var _cost = _this.data('cost');
		$('#_id').val(_id);
		$('#_value').val(_value);
		$('#_cost').val(_cost);
		$('#_glomp_points').text(_value);
		$('#_glomp_cost').text(_cost);
		if(_id>0 && _cost>0)
			{
				$('#btnpay').removeAttr("disabled");
			}
		else
			{
				$('#btnpay').attr("disabled", "disabled");
			}
		});
		$('#btnpay').click(function(){
			$(".Loader").show();
                        $("#paypalSuccessMessage").hide();
                        $("#paypalErrorMessage").hide();
                        
			$.ajax({
					type: "POST",
					url:"buyPoints/addPoints/",
					data: $('#frmPaypal').serialize(),
					success: function(data){
						var data = eval("("+data+")");
						if(data.status=='success')
						{
							$("#paypalSuccessMessage").show();
							document.frm_confirm_paypal_checkout.custom.value=data.inserted_id;
							document.frm_confirm_paypal_checkout.amount.value=data.cost;
							/*document.frm_confirm_paypal_checkout.invoice.value=data.invoiceID;*/
							document.frm_confirm_paypal_checkout.submit();
							/*alert(data.cost);*/
						} else {
                                                    $("#paypalSuccessMessage").hide();
                                                    $(".Loader").hide();
                                                    $("#paypalErrorMessage").show();
                                                }
					}
				});
			
			});
});
</script>
</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
<div class="outerBorder">
	<div class="inner-content">
      <div class="clearfix"></div>
      <div class="row-fluid">
      <div class="span12">
       <div class="middleBorder topPaddingZero">
      <div class="buyPoints">
      <img src="assets/frontend/img/Payment_Header.png" alt="<?php echo $this->lang->line('Payment', 'Payment');?>" class="img_payment"  />
      <div class="row-fluid">
       <div class="span12">
      <div class="content">
              <div class="row-fluid">
              <div class="span4">
              <div class="row-fluid">
              <div class="span12">
              <div class="innerContent buyPointsLeftBG">
              
              <img src="assets/frontend/img/glomped_points.jpg">
              <div class="glompoints">
              <div class="level"><?php echo $this->lang->line('Remaning', 'Remaining');?></div>
              <div class="points"><?php echo $this->user_account_m->user_balance($this->session->userdata('user_id'));?></div>
              </div>
              </div>  
              </div>
              </div>
               
              <div class="row-fluid">
              <div class="span12">
              <div class="innerContent" style="margin-top:15px;padding-left:12px" align="center">
               <div class="fl">
                   <img src="assets/frontend/img/buy-points.jpg">
               </div>
                  <div style="margin-top: -2px;margin-left: -6px;"class="fl info_tooltip" rel="popover" data-content="<?php echo $this->lang->line('help_buypoints_buypoints', 'Select the number of <b>glomp!</b> points you would like to store. Storing more saves time making multiple purchases.'); ?>" >
                      <img src="<?php echo base_url() ?>assets/images/q-mark.png" >
                  </div>
               <div class="clearfix"></div>
               <?php 
			   foreach($resPoints->result() as $recPoints)
			   {
			   ?>
               <div class="glompSelPoints" data-id='<?php echo $recPoints->package_id;?>' data-value='<?php echo $recPoints->package_point;?>' data-cost='<?php echo $recPoints->package_price;?>'><?php echo $recPoints->package_point;?></div>
               <?php }?>
               
              
               <div class="clearfix"></div>
              </div>  
              </div>
              </div>
              
               </div>
               <div class="span8">
               <div class="alert alert-success" style="display:none;" id="paypalSuccessMessage">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  <?php echo $this->lang->line('paupal_redirect_msg', 'Please be patient, you will be redirected to');?> <em><strong>
  <?php echo $this->lang->line('PayPal', 'PayPal');?>
</strong></em>.... 
    </div>
               <div class="alert alert-error" style="display:none;" id="paypalErrorMessage">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  <?php echo $this->lang->line('paypal_error_msg_1', 'Sorry. An error occurred');?> <em><strong>
</strong></em>.... 
    </div>
               <div class="innerContent border_radious"> <p>&nbsp;</p>
               <h1> <?php echo $this->lang->line('Purchase_Detals', 'Purchase Details');?></h1>
               <div class="innerWhiteBg">
               
               <table width="100%" border="0" cellpadding="10" cellspacing="15">
               <tr style="line-height:1px;">
               	<td>&nbsp;</td>
                <td>&nbsp;</td>
               </tr>
  <tr>
    <td width="23%"><?php echo $this->lang->line('You_Selected', 'You Selected');?> </td>
    <td width="77%">&nbsp;&nbsp;<span class="point_" id="_glomp_points">0</span>  <img src="assets/frontend/img/glomped_points_white_bg.jpg"></td>
  </tr>
  <tr>
  	<td colspan="2"><?php echo $this->lang->line('please_pay', 'Please Pay');?>  USD  <span id="_glomp_cost">0</span></td>
  </tr>
</table>

     <div class="btnHolder">
                   <form name="frmPaypal" id="frmPaypal" method="post" action="">
                     <input type="hidden" name="_id" id="_id">
                     <input type="hidden" name="_value" id="_value">
                     <input type="hidden" name="_cost" id="_cost">
                     <input type="hidden" name="session_payment_visisted_id" value="<?php echo $session_payment_visisted_id;?>" />
                     <button type="button"  id="btnpay"class="fl btn-custom-checkout" disabled data-loading-text="Loading..."><?php echo $this->lang->line('checkout', 'Checkout');?></button>
                     <div class="fl info_tooltip" style="margin-top: 7px;margin-left: -5px;" rel="popover" data-content="<?php echo $this->lang->line('help_buypoints_checkout', 'Checkout takes you to PayPal where you can pay by credit card or with your PayPal account.'); ?>" >
                         <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
                     </div>
                     <span style="font-size:12px">&nbsp;&nbsp;<?php echo $this->lang->line('Payment_lang_13', 'Pay with your credit card or PayPal account.'); ?> &nbsp;&nbsp;</span>
                      <img src="assets/frontend/img/paypal.jpg" alt="">
                     <span class="Loader"><i class="icon-spinner icon-spin icon-large"></i></span >
                   </form>
                   </div>
				</div>
              </div> 
               </div>
               </div>
         </div>
       </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      <form id="frm_confirm_paypal_checkout" name="frm_confirm_paypal_checkout" method="post" action="<?php echo base_url('buyPoints/express_checkout');?>" >        
        <input type="hidden" name="amount" value="" />        
        <input type="hidden" name="custom" value="" />    
        <?php /*?> <input type="hidden" name="invoice" value="" /><?php */?>
    </form>
</div>
</body>
</html>