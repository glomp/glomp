<?php
    $profile_id = $user_record->user_id;

	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	if($inTour=='yes'){
		$inTour_display='display:none;';
		$inTour_header_display='';
		$inTour_header='Add favourites';
		$inTour_header_top_position='0';
		$inTour_header_top_margin='100';
		$inTour_header_top_position_2='48';
		$inTour_display_footer='';
	}

    //get user dob
    $is_18 = $this->users_m->check_if_18($this->session->userdata('user_id'));        
?><!DOCTYPE html>
<html>
    <head>
        <title><?php echo $user_record->user_fname.' '.$user_record->user_lname;?>'s menu | glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="<?php echo $user_record->user_fname.' '.$user_record->user_lname." menu on glomp!."; ?>" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
    </head>
    <body style="background: white;" style="background:#5A606C" class="mobile">
        <?php include_once("includes/analyticstracking.php") ?>

        <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
            <div class="header_navigation_wrapper fl" align="center">
                <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                    <nav>
                        <a href="#" id="menu-icon-nav"></a>
                        <ul>
                            <?php if (isset($_GET['mDetail'])) { ?>
                            <li>
                                <a href="#" class="white "  id = "back_btn">
                                    <div class="w100per fl white">
                                        Back
                                    </div>
                                </a>
                            </li>
                            <?php } ?>
                            <li>
                                <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                    <div class="w100per fl white">
                                    <?php echo $this->lang->line('Home'); ?>
                                    </div>
                                </a>
                            </li>
                            <?php
                            if($this->session->userdata('is_user_logged_in')== true)
                            {
                            ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('profile'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                 if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                                 { ?>

                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$user_record->user_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('edit_favourite'); ?>
                                            </div>
                                        </a>
                                    </li>

                                <?php } ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Log_Out'); ?>
                                        </div>
                                    </a>
                                </li>
                            <?php
                            }
                            ?>
                        </ul>

                    </nav>
                </div>
                <a href="javascript:void(0);" >
                    <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                </a>
            </div>
            <!-- hidden navigations
            <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >
                <?php if (isset($_GET['mDetail'])) { ?>
                <a id = "back_btn" class=" cl hidden_nav_link fl w200px" href="#">
                    <div class="hidden_nav">
                        Back<?php //echo $this->lang->line('Back'); ?>
                    </div>
                </a>
                <div class="cl hidden_nav_seperator fl w200px"></div>
                <?php } ?>
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>
                <div class="cl hidden_nav_seperator fl w200px"></div>
                <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/profile') ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('profile'); ?>
                    </div>
                </a>
                <?php
                 if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                 { ?>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('edit_favourite'); ?>
                        </div>
                    </a>
                <?php } ?>
                <div class="cl hidden_nav_seperator fl w200px"></div>
                <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('Log_Out'); ?>
                    </div>
                </a>
            </div>
            <!-- hidden navigations -->
        </div>
        <!--<div class="p20px_0px" style="margin-top:10px;"></div>-->
        <div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
            <div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
        </div>
        <div class="menu_header_wrapper">
            <div class="user-menu">
                <ul>
                    <li class="fl">
                        <div class="user-icon">
                            <div class="user-image" style="background-image:url(/custom/uploads/users/thumb/<?php echo $user_record->user_profile_pic?>)">
                                <span>&nbsp;</span>
                            </div>
                            <div class="user-details">
                                <p style="background:#EB2227"><?php echo $user_record->user_fname.' '.$user_record->user_lname?></p>
                                <p style="background:#EB2227"><?php echo (isset($user_record->location_string))?$user_record->location_string:'&nbsp;';?></p>
                            </div>
                        </div>
                    </li>
                    <li class="fl"><a href="#" class="menu_header_selected"><?php echo $this->lang->line('brands','Brands'); ?></a></li>
                    <li class="fl"><a href="/m/profile/menu/<?php echo $profile_id?>/?tab=merchants&inTour=<?php echo $inTour?>"><?php echo $this->lang->line('Merchants','Merchants'); ?></a></li>
                    <li class="fl"><a href="/m/profile/menu/<?php echo $profile_id?>/?tab=products&inTour=<?php echo $inTour?>"><?php echo $this->lang->line('Products','Products'); ?></a></li>
                    <li class="fl"><a href="/m/profile/menu/<?php echo $profile_id?>/?tab=favourites&inTour=<?php echo $inTour?>"><?php echo $this->lang->line('favourites','Favourites'); ?></a></li>
                </ul>
            </div>
        </div>
        

<style type="text/css">
    .page-line {
        padding-top:20px;
        background:#5A606C;
    }
    .page-line:after {
        content: " ";
        display:block;
        clear: left;
    }
    .brand-info {
        float:left;
        width: 42.5%;
        margin-left: 5%;
        margin-bottom: 5%;
    }
    .brand-info a {
        display:block;
        overflow:hidden;
        background:#eee;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
    }
    .brand-info a img {
        width:100%; /* must have same dimension as height */
    }
</style>

        <div class="page brand-page">
            <div class="page-line">
                <?php if($user_record->user_city_id==2)
                {
                    
                ?>
                    <div class="brand-info"  align="center" >
                        <a href="<?php echo ($is_18) ? site_url("m/amex-sg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                            <img src="<?php echo base_url('assets/images/amex_logo.png');?>" title="Amex">
                        </a>
                    </div>
                    <div class="brand-info"  align="center" >
                        <a href="<?php echo ($is_18) ? site_url("m/uobsg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                            <img src="<?php echo base_url('assets/images/uob_logo.png');?>" title="UOB">
                        </a>
                    </div>

                    <div class="brand-info"  align="center" >
                        <a href="<?php echo ($is_18) ? site_url("m/dbssg"): 'javascript:void(0);';?>" class="<?php echo ($is_18) ? '': 'brand_not_18';?>" >
                            <img src="<?php echo base_url('assets/images/dbs-2.jpg');?>" title="DBS">
                        </a>
                    </div>

                    
                    <div style="clear:both"></div>
                <?php                
                }
                ?>
                <?php if ($brands):?>
                    <?php foreach($brands as $b):?>
                    <div class="brand-info" data-id="<?php echo $b->id?>" data-name="<?php echo $b->name?>">
                        <a                             
                            href="<?php echo ($is_18) ? site_url("/m/profile/menu/brands/".$profile_id."/".$b->id): 'javascript:void(0);' ; ?> "
                            title="<?php echo $b->name?>"
                            class="<?php echo ($is_18) ? '': 'brand_not_18';?>"
                            >
                            <img src="/custom/uploads/brands/<?php echo $b->thumbnail?>" title="<?php echo $b->name?>" />
                        </a>
                    </div>
                    <?php endforeach;?>
                <?php else:
                if($user_record->user_city_id!=2)
                {
                ?>
                    <div class="" style="padding:10px;color:#eee"><?php echo $this->lang->line('no_brands_found','No brands featured as yet.');?></div>
                    
                <?php
                }
                endif;?>
            </div>
        </div>
<script type="text/javascript">
    jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
        jQuery('.user-menu .user-icon .user-details').toggle();
    });
</script>
<?php
                    if(!$is_18)        
                    {
                        ?>
                        <script language="javascript" type="application/javascript">

                             $(document).ready(function() {

                                $('.brand_not_18').on({
                                    click: function(e) {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        var NewDialog = $(' <div id="hsbc_dialog" align="center" style="padding-top:25px !important; ">\
                                                            <div class="alert alert-error"  id="showError_brand" style="font-size:14px" align="left">\
                                                                <span id="showErrorMessage_brand">\
                                                                You must be at least 18 years of age to enter this page.\
                                                                </span>\
                                                                </div>\
                                                            </div>');
                                        NewDialog.dialog({                    
                                           dialogClass:'noTitleStuff dialog_style_glomp_wait',
                                            title: "",
                                            autoOpen: false,
                                            resizable: false,
                                            modal: true,
                                            width: 300,
                                            height:160,
                                            show: '',
                                            hide: '',
                                            buttons: [{text: "OK",
                                                    "class": 'btn-custom-blue_xs w80px cancel_glomp_option',
                                                    click: function() {
                                                        $("#hsbc_dialog").dialog("close").dialog('destroy').remove();
                                                    }}
                                            ]
                                        });
                                        NewDialog.dialog('open');
                                    }
                                });

                            });
                         </script>
                        <?php
                    }
                ?>

    </body>
</html>
