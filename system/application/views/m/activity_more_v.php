<?php 
if($activity->num_rows()>0)
{
    foreach($activity->result() as $rec)
    {
        $start_count++;
        $details = json_decode($rec->details);
        $user_id=$this->session->userdata('user_id');
        switch($rec->log_type)
        {
            case 'glomp_someone':                                 
                $product_id=$details->product_id;
                $recipient_id=$details->recipient_id;                                
                $recipient_name =$details->recipient_name;                                
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>">                                 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>glomp!</b>ed</span>&nbsp;
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$recipient_id);?>" class="red">
                                    <b><?php echo $recipient_name;?></b>
                                </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'received_a_glomp':
                $product_id=$details->product_id;
                $glomper_id=$details->glomper_id;                                
                $glomper_name =$details->glomper_name;
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$glomper_id);?>" class="red">
                                    <b><?php echo $glomper_name;?></b>
                                </a> <span class="red"><b>glomp!</b>ed</span>&nbsp; a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'received_a_reward':  
                if($selected=='')
                {

                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>Rewarded</b></span>&nbsp;
                                    a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> from&nbsp;
                                    <span class="red"><b>glomp!</b></span>
                                </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'redeemed_a_voucher': 
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>Redeemed</b></span>&nbsp;
                                a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> voucher
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'friend_redeemed_a_voucher':
                                                            
                $friend_id=$details->friend_id;                                
                $friend_name =$details->friend_name;                           
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                Your friend&nbsp;
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                    <b><?php echo $friend_name;?></b>
                                </a>&nbsp;
                                <span class="red"><b>redeemed</b></span>&nbsp;
                                a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> voucher
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'added_a_friend':                                                                                                                                    
                $friend_id=$details->friend_id;                                
                $friend_name =$details->friend_name;                                
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>Added</b></span>&nbsp;
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                    <b><?php echo $friend_name;?></b>
                                </a> to your Friends List
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'friend_added_you':                                                                            
                $friend_id=$details->friend_id;                                
                $friend_name =$details->friend_name;                                
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                    <b><?php echo $friend_name;?></b>
                                </a> <span class="red"><b>added</b></span>&nbsp;you to their Friends List
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'unfriend_a_friend':                                                                            
                $friend_id=$details->friend_id;                                
                $friend_name =$details->friend_name;                                
                if($selected=='')
                {
                ?>
                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                    <div class="container p5px_0px global_margin">	
                        <div class="row">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>Removed</b></span>&nbsp;                                                                                    
                                <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                    <b><?php echo $friend_name;?></b>
                                </a> to your Friends List
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                break;
            case 'invited_a_friend':
            if($selected=='')
            {
                if($details->invited_through=='facebook')
                {
                    ?>
                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                        <div class="container p5px_0px global_margin">	
                            <div class="row">
                                <div style="margin-bottom:2px;">
                                    <b>
                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                    </b>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <span class="red"><b>Invited</b></span>&nbsp;                                                                                    
                                    <a href="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>" target="_blank" class="red">
                                        <b><?php echo $details->invited_name;?></b>
                                    </a> from Facebook to join <span class="red"><b>glomp</b>!ed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                else if($details->invited_through=='linkedIn')
                { 
                    ?>
                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                        <div class="container p5px_0px global_margin">	
                            <div class="row">
                                <div style="margin-bottom:2px;">
                                    <b>
                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                    </b>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                    <b><?php echo $details->invited_name;?></b>
                                    from LinkedIn to join <span class="red"><b>glomp</b>!ed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php                                                                            
                }
                else if($details->invited_through=='email')
                { 
                    ?>
                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                        <div class="container p5px_0px global_margin">	
                            <div class="row">
                                <div style="margin-bottom:2px;">
                                    <b>
                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                    </b>
                                </div>
                                <div style="margin-bottom:10px;">
                                    <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                    <b><?php echo $details->invited_name;?></b>
                                    &nbsp;through email to join <span class="red"><b>glomp</b>!ed</span>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php                                                                            
                }
            }
            break;
        case 'friend_added_by_friend':
            $friend_id=$details->friend_id;
            $added_friend_id=$details->added_friend_id;
            $friend_name =$details->friend_name;                            
            $added_friend_name =$details->added_friend_name;
            if($selected=='')
            {
            ?>
            <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                <div class="container p5px_0px global_margin">	
                    <div class="row">
                        <div style="margin-bottom:2px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div style="margin-bottom:10px;">
                            Your friend&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a>&nbsp;
                            <span class="red"><b>added</b></span>&nbsp;
                            your friend&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$added_friend_id);?>" class="red">
                                <b><?php echo $added_friend_name;?></b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            break;
        case 'points_purchased':
            if($selected=='points')
            {
            ?>
            <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                <div class="container p5px_0px global_margin">	
                    <div class="row">
                        <div class="pull-left" style="width:80%">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>Purchased</b></span>&nbsp;                                                                                        
                                <?php echo $details->points;?> points
                            </div>
                        </div>
                        <div class="pull-right" style="width:20%;">
                            <div class="col-xs-12" style="padding:4px;" align="right">
                                <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                    <?php echo $details->balance;?>
                                </div>                                                                                            
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            break;
        case 'points_spent':                                                                            
            $product_id=$details->product_id;
            $recipient_id=$details->recipient_id;
            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
            $recipient_name =$info->friends->$recipient_id->user_name;
            $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
            
            $prod_info = json_decode($this->product_m->productInfo($product_id));
            $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
            $points =((int) $details->points) *-1;
            if($points>0)
            {
                $points =" + ".abs($points);
            }
            else
            {
                $points =" - ".abs($points);
            }
            
            if($selected=='points')
            {
            ?>
            <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                <div class="container p5px_0px global_margin">	
                    <div class="row">
                        <div class="pull-left" style="width:80%">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>glomp!</b>ed</span>&nbsp;
                                <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                    <b><?php echo $recipient_name;?></b>
                                </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>&nbsp;
                                (<?php echo $points;?>)
                            </div>
                        </div>
                        <div class="pull-right" style="width:20%;">
                            <div class="col-xs-12" style="padding:4px;" align="right">
                                <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                    <?php echo $details->balance;?>
                                </div>                                                                                            
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            break;
        case 'points_reversed':                                                                            
            $voucher_id=$details->voucher_id;
            $voucher_data=$this->users_m->get_specific_voucher_details($voucher_id);
            $voucher_data=$voucher_data->row();
            
            
            $product_id     =$voucher_data->voucher_product_id;
            $recipient_id   =$voucher_data->voucher_belongs_usser_id;
            
            
            
            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
            $recipient_name =$info->friends->$recipient_id->user_name;
            $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
            
            $prod_info = json_decode($this->product_m->productInfo($product_id));
            $prod_image= $prod_info->product->$product_id->prod_image;
            
            $merchant_name  =$prod_info->product->$product_id->merchant_name;
            $product_name   =$prod_info->product->$product_id->prod_name;
            $points =((int) $details->points) *-1;
            if($points>0)
            {
                $points =" + ".abs($points);
            }
            else  if($points<0)
            {
                $points =" - ".abs($points);
            }
            else
            {
                $points =" ".abs($points);
            }
            
            if($selected=='points')
            {
            ?>
            <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                <div class="container p5px_0px global_margin">	
                    <div class="row">
                        <div class="pull-left" style="width:80%">
                            <div style="margin-bottom:2px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div style="margin-bottom:10px;">
                                <span class="red"><b>glomp!</b></span> of a&nbsp;
                                <?php echo $merchant_name;?> <?php echo $product_name;?>&nbsp;
                                was <span class="red"><b>unredeemed</b></span> by&nbsp;
                                <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                    <b><?php echo $recipient_name;?></b>
                                </a>&nbsp;
                                (<?php echo $points;?>)
                            </div>
                        </div>
                        <div class="pull-right" style="width:20%;">
                            <div class="col-xs-12" style="padding:4px;" align="right">
                                <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                    <?php echo $details->balance;?>
                                </div>                                                                                            
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            break;

        }
    }
}
else
{
    echo '';
}
?>