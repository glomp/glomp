<?php
if (($this->session->userdata('temp_user_fb_id')) != "") {
    $user_fb_id = ($this->session->userdata('temp_user_fb_id'));
    $fname = ($this->session->userdata('temp_fname'));
    $mname = ($this->session->userdata('temp_mname'));
    $lname = ($this->session->userdata('temp_lname'));
    $email = ($this->session->userdata('temp_email'));
    $gender = ($this->session->userdata('temp_gender'));
    $day = ($this->session->userdata('temp_day'));
    $months = ($this->session->userdata('temp_months'));
    $year = ($this->session->userdata('temp_year'));



    $array_items = array(
        'temp_user_fb_id' => '',
        'temp_fname' => '',
        'temp_lname' => '',
        'temp_mname' => '',
        'temp_email' => '',
        'temp_gender' => '',
        'temp_day' => '',
        'temp_months' => '',
        'temp_year' => ''
    );
    $this->session->unset_userdata($array_items);
} else if (($this->session->userdata('temp_user_linkedin_id')) != "") {
    //LinkedIN
    //TODO: Please clean code block. All the social app thing.
    $user_linkedin_id = ($this->session->userdata('temp_user_linkedin_id'));
    $fname = ($this->session->userdata('temp_fname'));
    $mname = ($this->session->userdata('temp_mname'));
    $lname = ($this->session->userdata('temp_lname'));
    $email = ($this->session->userdata('temp_email'));
    $gender = ($this->session->userdata('temp_gender'));
    $day = ($this->session->userdata('temp_day'));
    $months = ($this->session->userdata('temp_months'));
    $year = ($this->session->userdata('temp_year'));
} else {
    $fname = ($this->session->userdata('fname')) ? $this->session->userdata('fname') : set_value('fname');
    $lname = ($this->session->userdata('lname')) ? $this->session->userdata('lname') : set_value('lname');
    $email = ($this->session->userdata('email')) ? $this->session->userdata('email') : set_value('email');
    $gender = ($this->session->userdata('gender')) ? $this->session->userdata('gender') : set_value('gender');
    $day = ($this->session->userdata('day')) ? $this->session->userdata('day') : set_value('day');
    $months = ($this->session->userdata('months')) ? $this->session->userdata('months') : set_value('months');
    $year = ($this->session->userdata('year')) ? $this->session->userdata('year') : set_value('year');
}
$promo_code = "";
if (isset($_POST['promo_code']))
    $promo_code = $_POST['promo_code'];
?>	
<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
    <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>    
    <body style="background: white;">
        <?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" align="center">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>" class="white ">
                                        <div class="w100per fl white">
                                        Back
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp_register.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations       
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>">
                        <div class="hidden_nav" align="left">
                            Back
                        </div>
                    </a>                    
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="p20px_0px global_margin" style="background-color: white;font-size: 14px;font-weight: bold; color:#808283">
                <div id="error_container" class="alert alert-error hide">
                    <button type="button" class="close" id="error_close_btn">&times;</button>
                    <div id ="errors" align="left" >
                        <div class="alert alert-error"  id ="errors_inner" style="font-size:11px !important; margin: 0px !important;" >
                        </div>
                    </div>
                </div>

                <form id="register_form" method="post" action="<?php echo site_url(MOBILE_M . '/landing/register2') ?>" enctype='multipart/form-data'>
                    <?php
                    if (isset($user_fb_id) && $user_fb_id != "") {
                        echo '			
                            <input type="hidden"  id="fb_user_photo" name="fb_user_photo" value="">
                            <input type="hidden"  id="fb_user_id" name="fb_user_id" value="' . $user_fb_id . '">
                            <input type="hidden"  id="fb_fname" name="fb_fname" value="' . $fname . '">
                            <input type="hidden"  id="fb_mname" name="fb_mname" value="' . $mname . '">
                            <input type="hidden"  id="fb_lname" name="fb_lname" value="' . $lname . '">
                            <input type="hidden"  id="fb_email" name="fb_email" value="' . $email . '">
                            <input type="hidden"  id="fb_gender" name="fb_gender" value="' . $gender . '">
                            <input type="hidden"  id="fb_day" name="fb_day" value="' . $day . '">
                            <input type="hidden"  id="fb_months" name="fb_months" value="' . $months . '">
                            <input type="hidden"  id="fb_year" name="fb_year" value="' . $year . '">
                            ';
                    } else {
                        /* If not login with FB */
                        echo '			
                            <input type="hidden"  id="fb_user_photo" name="fb_user_photo" value="">
                            <input type="hidden"  id="fb_user_id" name="fb_user_id" >
                            <input type="hidden"  id="fb_fname" name="fb_fname" >
                            <input type="hidden"  id="fb_mname" name="fb_mname" >
                            <input type="hidden"  id="fb_lname" name="fb_lname" >
                            <input type="hidden"  id="fb_email" name="fb_email" >
                            <input type="hidden"  id="fb_gender" name="fb_gender" >
                            <input type="hidden"  id="fb_day" name="fb_day" >
                            <input type="hidden"  id="fb_months" name="fb_months" >
                            <input type="hidden"  id="fb_year" name="fb_year" >';
                    }
                    if (isset($user_linkedin_id) && $user_linkedin_id != "") {
                        echo '
                            <input type="hidden"  id="user_linkedin_id" name="user_linkedin_id" value="' . $user_linkedin_id . '">
                        ';
                    }
                    ?>
                    
                    <div class="global_border" style="min-width: 100px;min-height: 100px;">
                        <?php
                        if (validation_errors() != "") {
                            echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . validation_errors() . "</div>";
                        }
                        if (isset($error_msg)) {
                            echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error_msg . "</div>";
                        }
                        ?>
                        <div class="register_form_gray" align="left">
                            <input name="fname" id="fname" type="text"  value="<?php echo $fname; ?>"  placeholder="First Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="register_form_gray" align="left">
                            <input name="lname" id="lname" type="text"   value="<?php echo $lname; ?>"  placeholder="Last Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="border_topbot_gray" style="padding-right: 15px;" align="left">
                            <!-- row1 -->
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;">Date of Birth</div>
                                <div class="fl">
                                    <div class="fl">
                                        <input name="day" id="day" maxlength="2"  value="<?php echo $day; ?>" class="register_form_gray" type="text" placeholder="DD" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 15px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="months" id="months" maxlength="2"  value="<?php echo $months; ?>"  class="register_form_gray" type="text" placeholder="MM" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="year" id="year" maxlength="4" value="<?php echo $year; ?>"  class="register_form_gray" type="text" placeholder="YYYY" style="width: 49px; border: 0px solid; font-weight: bold;margin-left: 15px;margin-right: 7px;" />
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row2 -->
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Display</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="dob_display" checked id="dob_display_w_year" value="dob_display_w_year"  /> DD / MM / YYYY
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="dob_display" id="dob_display_wo_year"  value="dob_display_wo_year"> DD / MM
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row2 -->
                            <!--
<div class="fl" style="width: 100%"> 
    <div class="fl" style="padding:10px 10px 10px 0px;margin-right: 38px;">Display</div>
    <div class="fl">
        <div class="fl" style="margin-left: 16px;">
            DD/MM
            <input class="register_form_gray" type="radio" name="dob_display" id="dob_display1" placeholder="DD" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;" />
        </div>
        <div class="fl">
            DD/MM/YYYY
            <input class="register_form_gray" type="radio" name="dob_display" id="dob_display2" placeholder="DD" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;" />
        </div>
    </div>
    <div class="cl"></div>
</div>
                            -->
                            <div class="cl"></div>
                            <!-- row2 -->
                        </div>
                        <div class="register_form_gray" align="left">
                            <select name="location" id="location" class="textBoxGray" style="width:100%">
                                <option value=""><?php echo $this->lang->line('Select_Location'); ?></option>
                                <?php echo $this->regions_m->location_dropdown(0, set_value('location')); ?>
                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="global_border" style="min-width: 100px; height:100px;" align="left">
                        <div class="cl"></div>
                        <?php
                        $profile_pic = "assets/frontend/img/default_profile_pic_register.jpg"
                        ?>
                        <div class="fl" style="margin-right:5px;">
                            <img src="<?php echo base_url() . $profile_pic; ?>" alt="<?php /* echo $user_record->user_name; */ ?>" id="profile_pic" style="height:80px;width:65px"    class="img-polaroid"><br />
                        </div>
                        <div class="fl" style="border:0px solid; width:180px">
                            <div class="upload">
                                <a href="javascript:;" id="uploadFilePic" ><?php echo $this->lang->line('upload_profile_photo'); ?></a>
                                <span id="upload_loader"></span>
                                <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note'); ?></p>
                                <input type="file"  name="user_photo" id="profilePhotoUpload" accept="image/*" style="display:none;visibility: hidden;" onchange='profilePic.UpdatePreview(this)' />
                            </div>
                        </div>						
                    </div>
                    <br />
                    <div class="global_border" style="min-width: 100px;">
                        <div class="fl" style="width: 100%;margin-left: 18px;margin-top: 10px;margin-right: 0px;padding-right: 32px;display:none" align="left">
                            <div class="fl"><input type="checkbox" name="chk_notif_via_fb" id="chk_notif_via_fb" value ="Y" /></div>
                            <div class="fl" style="margin-left: 11px;">Notify via Facebook Message</div>
                        </div>
                        <div class="cl"></div>
                        <div class="register_form_gray" align="left">
                            <input name="email" id="email" type="email" value="<?php echo $email; ?>" placeholder="Email" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                    </div>
                    <br />
                    <div class="global_border" style="min-width: 100px;">
                        <div class="fl">
                            <div class="register_form_gray" align="left">
                                <input name="pword" id="pword" type="password" maxlength="9" placeholder="Enter password" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                            <div class="register_form_gray" align="left">
                                <input name="cpword" id="cpword" type="password"  maxlength="9"  placeholder="Confirm password" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; max-width: 100%">
                            You will need to enter this Password when sending and redeeming products so please remember it.
                        </div>
                        <div class="cl"></div>
                    </div>
                    <br />
                    <div class="global_border">
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; margin-top: 10px;">
                            <?php $temp = $this->lang->line('Promotional_Code');
                            echo str_replace(' ', '&nbsp;', $temp)
                            ?>
                        </div>
                        <div class="fl" align="left">
                            <div class="register_form_gray" align="left">
                                <input value="<?php echo $promo_code; ?>" maxlength="40" name="promo_code" id="promo_code" type="text" placeholder="(Optional)" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="cl"></div>
                    </div>
                    <br />
                    <div class="">
                        <div class="fl" style="margin-left: 14px;"><input name="agree" id="agree" type="checkbox" /></div>
                        <div class="fl" style="margin-left: 10px;">I agree to the <span style="color:#3ACD00">Terms & Conditions</span></div>
                        <div class="cl"></div>
                    </div>
                    <br />
                    <div class="">
                        <div class="fl" style="margin-left: 14px; margin-top: 10px">
                            <button type="submit" id="register_btn" class="btn-lg btn-custom-blue-grey btn-block w202px" >Submit</button>
                        </div>
                        <div class="fl" style="margin-left: 14px;margin-top: 10px">
                            <a href="<?php echo site_url(MOBILE_M . '/landing/register_landing'); ?>" class="btn-lg btn-custom-blue-grey btn-block w202px" type="submit">Cancel</a>
                        </div>
                        <div class="cl"></div>
                    </div>
                </form>
            </div>
            <!-- /container -->
        </div> <!-- /global_wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>

        <script>
                                    var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
                                    var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
                                    var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
                                    $(document).ready(function(e) {
                                        if ($('#fb_user_id').val() != '') {
                                            getFbPhoto($('#fb_user_id').val());
                                        }
                                        
                                        <?php
                                        if (($this->session->userdata('temp_user_linkedin_id')) != "") { ?>
                                            setTimeout(function() {
                                                params = {
                                                    js_func_li: '',
                                                    api: '/v1/people/~:(picture-urls::(original))'
                                                };
                                                var data = linkedin_api(params);
                                                
                                                $("#profile_pic").prop("src", data.values[0].pictureUrls.values[0]);
                                                $("#fb_user_photo").val(data.values[0].pictureUrls.values[0]);
                                            }, 1000);
                                            
                                        <?php } ?>
                                    });
                                    fbApiInit = false;

                                    /* Load the SDK asynchronously*/
                                    (function(d) {
                                        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement('script');
                                        js.id = id;
                                        js.async = true;
                                        js.src = "//connect.facebook.net/en_US/all.js";
                                        ref.parentNode.insertBefore(js, ref);
                                    }(document));
                                    
                                    
                                     function linkedin_api(obj) {
                                        var data;
                                        obj.mail = obj.mail || '';
                                        obj.body = obj.body || '';

                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            async: false,
                                            url: GLOMP_BASE_URL + 'ajax_post/linkedIn_callback',
                                            data: {
                                                redirect_li: window.location.href,
                                                js_func_li: obj.js_func_li,
                                                api: obj.api,
                                                mail: obj.mail,
                                                body: obj.body,
                                            },
                                            success: function(_data) {
                                                if (_data.redirect_window != '') {
                                                    window.location = _data.redirect_window;
                                                    data = false;
                                                } else {
                                                    data = _data;
                                                }
                                            }
                                        });
                                        return data;
                                    }
                                    
                                    /*Get profile pic*/
                                    function getFbPhoto(fb_id) {
                                        $('#fb_user_id').val();
                                        var url = "http://graph.facebook.com/" + fb_id + "/picture?width=110&height=140&redirect=false";
                                        $.get(url, function(resp) {
                                            var tempURL = resp.data.url;
                                            $("#fb_user_photo").val(tempURL);
                                            $("#profile_pic").prop("src", tempURL);
                                            var log_details = {
                                                type: 'fb_api',
                                                event: 'Auto grab profile picture',
                                                log_message: 'Grabbing facebook profile picture',
                                                url: window.location.href,
                                                response: tempURL
                                            };
                                            var logs = [];
                                            logs.push(JSON.stringify(log_details));
                                            /*save_system_log(logs);*/
                                        });

                                        if (fbApiInit) {
                                            getLocationFromFB(fb_id);
                                        }
                                        else {
                                            window.fbAsyncInit = function() {
                                                FB.init({
                                                    appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                                                    status: true, /* check login status*/
                                                    cookie: true, /* enable cookies to allow the server to access the session*/
                                                    xfbml: true  /* parse XFBML	  		*/
                                                });
                                                fbApiInit = true;
                                                getLocationFromFB(fb_id);
                                            }
                                        }
                                        ;
                                    }

                                    $('#error_close_btn').click(function(e) {
                                        e.preventDefault();                                        
                                        $('#errors').addClass('hide');
                                    });

                                    $('#register_btn').click(function(e) {

                                        e.preventDefault();
                                        var form = $('#register_form');

                                        var NewDialog = $('<div id="waitPopup" align="center">\
                                                                                        <div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                        NewDialog.dialog({
                                            autoOpen: false,
                                            closeOnEscape: false,
                                            resizable: false,
                                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                            title: 'Please wait...',
                                            modal: true,
                                            position: 'center',
                                            width: 200,
                                            height: 120
                                        });
                                        NewDialog.dialog('open');

                                        $(form).ajaxSubmit({
                                            type: "post",
                                            url: $(form).attr('action'),
                                            dataType: 'json',
                                            data: $(form).serialize(),
                                            success: function(response) {
                                                $("#waitPopup").dialog('destroy').remove();
                                                if (!response.success) {
                                                    process_error(response);
                                                    return;
                                                }
                                                window.location = response.location;
                                            }
                                        });
                                    });

                                    function process_error(response) {
                                        $('#errors_inner').html('');
                                        for (var e in response.errors) {
                                            if (response.errors[e] != '') {
                                                //$('#error_container').removeClass('hide');
                                                $('#errors_inner').append(response.errors[e]);
                                                $('#' + e).addClass('error_border');
                                            }

                                            /*Remove any error message if theres any*/
                                            if (response.errors[e] == '') {
                                                $('#' + e).removeClass('error_border');
                                             }                                            
                                         }
                                        var NewDialog =$('#errors');
                                        NewDialog.dialog({						
                                            autoOpen: false,
                                            closeOnEscape: false ,
                                            resizable: false,					
                                            dialogClass:'dialog_style_glomp_wait noTitleStuff ',
                                            modal: true,
                                            title:'Please wait...',
                                            position: 'center',
                                            width:280,                                
                                            buttons:[
                                                {text: "Close",
                                                "class": 'btn-custom-white_xs',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 500);
                                                }}
                                            ]
                                            
                                        });
                                        NewDialog.dialog('open');
                                    }

                                    function getLocationFromFB(fb_id) {
                                        var log_details = {
                                            type: 'fb_api',
                                            event: 'Auto grab location',
                                            log_message: 'Trying to get facebook member location',
                                            url: window.location.href,
                                            response: ''
                                        };
                                        var logs = [];
                                        logs.push(JSON.stringify(log_details));
                                        /*save_system_log(logs);*/
                                        
                                        FB.getLoginStatus(function(response) {
                                            if (response.status == "connected") {
                                                var log_details = {
                                                    type: 'fb_api',
                                                    event: 'Auto grab location',
                                                    log_message: 'Connected to facebook; Getting location from JS FQL API.',
                                                    url: window.location.href,
                                                    response: JSON.stringify(response)
                                                };
                                                var logs = [];
                                                logs.push(JSON.stringify(log_details));
                                                /*save_system_log(logs);*/
                                                
                                                FB.api({
                                                    method: 'fql.query',
                                                    query: 'SELECT first_name, last_name, current_location  FROM user WHERE uid = ' + fb_id
                                                },
                                                function(response) {
                                                    var country = "";
                                                    var first_name = response[0]['first_name'];
                                                    var last_name = response[0]['last_name'];
                                                    if (response[0]['current_location'] != null)
                                                        country = response[0]['current_location']['country'];
                                                    if (country != "") {
                                                        $("#location option").filter(function() {
                                                            return this.text == country;
                                                        }).attr('selected', true);
                                                        var log_details = {
                                                            type: 'fb_api',
                                                            event: 'Auto grab location',
                                                            log_message: 'location successfully obtain.',
                                                            url: window.location.href,
                                                            response: JSON.stringify(response)
                                                        };
                                                        var logs = [];
                                                        logs.push(JSON.stringify(log_details));
                                                        /*save_system_log(logs);*/
                                                    } else {
                                                        var log_details = {
                                                            type: 'fb_api',
                                                            event: 'Auto grab location',
                                                            log_message: 'Cannot grab location. Location is set private.',
                                                            url: window.location.href,
                                                            response: JSON.stringify(response)
                                                        };
                                                        var logs = [];
                                                        logs.push(JSON.stringify(log_details));
                                                        /*save_system_log(logs);*/
                                                    }
                                                });
                                            }
                                            else {
                                                FB.login
                                                        (
                                                                function(response)
                                                                {
                                                                    if (response.status === 'connected') {
                                                                        var log_details = {
                                                                            type: 'fb_api',
                                                                            event: 'Auto grab location',
                                                                            log_message: 'Connected to facebook; Getting location from JS FQL API.',
                                                                            url: window.location.href,
                                                                            response: JSON.stringify(response)
                                                                        };
                                                                        var logs = [];
                                                                        logs.push(JSON.stringify(log_details));
                                                                        /*save_system_log(logs);*/
                                                                        
                                                                        FB.api({
                                                                            method: 'fql.query',
                                                                            query: 'SELECT first_name, last_name, current_location  FROM user WHERE uid = ' + fb_id
                                                                        },
                                                                        function(response) {
                                                                             
                                                                            var country = "";
                                                                            var first_name = response[0]['first_name'];
                                                                            var last_name = response[0]['last_name'];
                                                                            if (response[0]['current_location'] != null)
                                                                                country = response[0]['current_location']['country'];
                                                                            if (country != "") {
                                                                                var log_details = {
                                                                                    type: 'fb_api',
                                                                                    event: 'Auto grab location',
                                                                                    log_message: 'location successfully obtain.',
                                                                                    url: window.location.href,
                                                                                    response: JSON.stringify(response)
                                                                                };
                                                                                var logs = [];
                                                                                logs.push(JSON.stringify(log_details));
                                                                                /*save_system_log(logs);*/                                                                                
                                                                                
                                                                                $("#location option").filter(function() {
                                                                                    return this.text == country;
                                                                                }).attr('selected', true);
                                                                            } else {
                                                                                var log_details = {
                                                                                    type: 'fb_api',
                                                                                    event: 'Auto grab location',
                                                                                    log_message: 'Cannot grab location. Location is set private.',
                                                                                    url: window.location.href,
                                                                                    response: JSON.stringify(response)
                                                                                };
                                                                                var logs = [];
                                                                                logs.push(JSON.stringify(log_details));
                                                                                /*save_system_log(logs);*/
                                                                            }
                                                                        });
                                                                    }
                                                                }, {scope: 'publish_actions,email'}
                                                        );
                                            }
                                        });
                                    }
        </script>
        <script>
            /*Manual Registration            */
            var importOnly = "";
            $(function() {
                $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();
                });
                $('#uploadFilePic').click(function(e) {
                    /*$('#profilePhotoUpload').click();*/
                    /*$('#upload_loader').html('Please wait...<i class="icon-spinner"></i>');*/
                    e.preventDefault();
                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"></div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        resizable: false,
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                        title: '',
                        modal: true,
                        width: 260,
                        height: 100,
                        show: '',
                        hide: '',
                        buttons: [
                            {text: "<?php echo $this->lang->line('upload_from_device'); ?>",
                                "class": 'btn-custom-green w200px2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    $('#profilePhotoUpload').click();
                                }},
                            {text: "<?php echo $this->lang->line('import_from_facebook'); ?>",
                                "class": 'btn-custom-blue_fb w200px2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    importFromFacebook();
                                }},
                            {text: "<?php echo $this->lang->line('Cancel'); ?>",
                                "class": 'btn-custom-ash_xs w200px2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                }}
                        ]
                    });
                    NewDialog.dialog('open');

                });

            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });

            /* for profile picture*/
            $(function() {
                profilePic = {
                    UpdatePreview: function(obj) {

                        if (!window.FileReader) {

                        } else {
                            var reader = new FileReader();
                            var target = null;



                            reader.onload = function(e) {
                                target = e.target || e.srcElement;
                                var upload_string = target.result;
                                var n = upload_string.search(':image/');
                                /*find for :image/*/
                                if (n >= 0)
                                {
                                    $("#profile_pic").prop("src", target.result);
                                } else
                                {
                                    alert("Invalid image extension.");
                                }
                                $('#upload_loader').html('');
                            };
                            reader.readAsDataURL(obj.files[0]);
                        }
                    }
                };
            });
            function linkFbAccount() {

            }
            function importFromFacebook() {
                importOnly = "yes";
                if (fbApiInit)
                {
                }
                else {
                    FB.init({
                        appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                        status: true, /* check login status*/
                        cookie: true, /* enable cookies to allow the server to access the session*/
                        xfbml: true  /* parse XFBML	  		*/
                    });
                    fbApiInit = true;
                }
                FB.getLoginStatus(function(response) {
                    if (response.status == "connected")
                    {
                        FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                            getFbPhoto(response.id);
                        });
                    }
                    else
                    {
                        if(facebook_alert_error())
                        {
                            FB.login(
                                    function(response)
                                    {
                                        if (response.status === 'connected') {
                                            FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                                                getFbPhoto(response.id);
                                            });
                                        }
                                    }, {scope: 'publish_actions,email'}
                            );
                        }                        
                    }
                });
            }
            /* Linked IN */
            function liInitOnload() {
                if (IN.User.isAuthorized()) {
                    /*NO NEED TO DO ANYTHING*/
                }
            }           
            function liAuth() {
                IN.User.authorize(function() {
                    /*If authorized and logged in*/
                    var params = {
                        type: 'linked',
                        type_string: 'linkedIn',
                        func: function(obj) {
                            return login_linkedIN(obj);
                        }
                    };
                    
                    processIntegratedApp(params);
                });
            }
        </script>        
    </body>
</html>
