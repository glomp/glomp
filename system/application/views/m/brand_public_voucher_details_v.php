<?php
	$user_id = $this->session->userdata('user_id');


	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	

?>
<!DOCTYPE html>
<html>
    <head>
        <title> Amex| glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
		
		<meta property="og:title" content="You have been glomp!" /> 
		<meta property="og:type" content="website" />        
		<meta property="og:url" content="<?php echo site_url('m/brands/details/'.$gl_voucher_brand_data->group_voucher_id);?>" />
		<meta property="og:image" content="<?php echo site_url('assets/images/FB_invite_icon.png');?>"/>
		<meta property="og:description"  content="<?php echo $gl_voucher_brand_data->from_first_name ;?> has treated you a great product! To redeem,  please click here and enjoy." />	
		
		
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
		<script src="<?php echo minify('assets/frontend/js/jquery.mask.min.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<script type="text/javascript">
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var items = [];
			
        </script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
								<?php
									if(
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url(MOBILE_M) ?>/brands/view/amex/" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('Back','Back'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>/amex-sg" class="white ">
                                        <div class="w100per fl white">
                                        Amex <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
								<?php
									if(
										$this->session->userdata('user_id') =='' &&
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url() ?>brands/logout/m" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('exit','Exit'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                    </a>
                </div>
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>

            <div class="page brand-page">
                
                <div style="padding:10px">
					
					<h4 style="padding:0px ;margin:0px;">Order Summary for <b><?php echo $gl_voucher_brand_data->group_voucher_id;?><b></h4>
				</div>
				
				<hr size="1" style="margin:1px;">
				
				
				<div class="page-line cl container p5px_0px global_margin">
				<form method="POST" id="details_form">
					<?php
						if(validation_errors())
						{
					?>
						<div class="alert alert-error"  id="" style=";font-size:12px" align="left">
							<?php echo validation_errors();?>
						</div>
					<?php
						}
					?>
					<?php
						if($success_message !='')
						{
					?>
						<div class="alert alert-success"  id="" style=";font-size:12px" align="left">
							<?php echo $success_message;?>
						</div>
					<?php
						}
					?>
					<input type="hidden" name="transaction_type" id="transaction_type" value="<?php echo $gl_voucher_brand_data->transaction_type;?>" />
					<?php
						if($gl_voucher_brand_data->transaction_type =='glomp_to_fb')
						{
						?>
							<div class="row">
								<div class="col-xs-4" align="left">
									<b>Email:</b>
								</div>
								<div class="col-xs-8" align="left">
									<?php
									if($gl_voucher_brand_data->first_name =='N/A')
									{
									?>
									<input type="text" data-label="Email:" class="helvetica_font form-control" name="email" value="<?php echo ($_POST) ? ($this->input->post('email')): ((( strpos( $gl_voucher_brand_data->email, '@facebook.com' ) !== false ))? '':$gl_voucher_brand_data->email);?>"id="email" placeholder="Email" />
									<?php
									}
									else
									{
										echo $gl_voucher_brand_data->email;
									}
									?>
								</div>
							</div><br>
							<div class="row">
								<div class="col-xs-4" align="left">
									<b>First Name:</b>
								</div>
								<div class="col-xs-8" align="left">
									<?php
									if($gl_voucher_brand_data->first_name =='N/A')
									{
									?>
									<input type="text" data-label="First Name:" class="helvetica_font form-control" name="first_name" value="<?php echo ($_POST) ? ($this->input->post('first_name')): (($gl_voucher_brand_data->first_name =='N/A')? '':$gl_voucher_brand_data->first_name);?>"id="first_name" placeholder="First Name" />
									<?php
									}
									else
									{
										echo $gl_voucher_brand_data->first_name;
									}
									?>
								</div>
							</div><br>
							<div class="row">
								<div class="col-xs-4" align="left">
									<b>Last Name:</b>
								</div>
								<div class="col-xs-8" align="left">
									<?php
									if($gl_voucher_brand_data->first_name =='N/A')
									{
									?>
									<input type="text" data-label="Last Name:" class="helvetica_font form-control" name="last_name" value="<?php echo ($_POST) ? ($this->input->post('last_name')): (($gl_voucher_brand_data->last_name =='N/A')? '':$gl_voucher_brand_data->last_name);?>"id="last_name" placeholder="Last Name" />
									<?php
									}
									else
									{
										echo $gl_voucher_brand_data->last_name;
									}
									?>
								</div>
							</div><br>
						<?php
						}
						else
						{
					?>
							<div class="row">
								<div class="col-xs-4" align="left">
									<b>Name:</b>
								</div>
								<div class="col-xs-8" align="left">
									<div style="font-weight:bold;font-size:16px;">
										<?php echo $gl_voucher_brand_data->first_name;?> <?php echo $gl_voucher_brand_data->last_name;?>
									</div>
								</div>
							</div><br>
					<?php
						}
					?>
					
					
					
					
					<div class="row">
						<div class="col-xs-4" align="left">
							<b>Contact Number:</b>
						</div>
						<div class="col-xs-8" align="left">
							<?php
							if($gl_voucher_brand_data->contact_num =='' || $gl_voucher_brand_data->contact_num =='0')
							{
							?>
							<input type="text" data-label="Contact Number:" class="helvetica_font form-control" name="contact_num" value="<?php echo ($_POST) ? ($this->input->post('contact_num')): (($gl_voucher_brand_data->contact_num ==0)? '':$gl_voucher_brand_data->contact_num);?>"id="contact_num" placeholder="Contact Number" />
							<?php
							}
							else
							{
								echo $gl_voucher_brand_data->contact_num;
							}
							?>	
						</div>
					</div><br>
					<div class="row">
						<div class="col-xs-4" align="left">
							<b>Delivery Address:</b>
						</div>
						<div class="col-xs-8" align="left">
							<?php
							if($gl_voucher_brand_data->contact_num =='' || $gl_voucher_brand_data->contact_num =='0')
							{
							?>
							<textarea data-label="Delivery Address:" class="helvetica_font form-control" name="delivery_address" id="delivery_address" placeholder="Delivery Address" ><?php echo ($_POST) ? ($this->input->post('delivery_address')) : (($gl_voucher_brand_data->delivery_address ==0)? '':$gl_voucher_brand_data->delivery_address);?></textarea>
							<?php
							}
							else
							{
								echo $gl_voucher_brand_data->delivery_address;
							}
							?>	
						</div>
					</div><br>
                    <div class="row">
						<div class="col-xs-4" align="left">
							<b>Delivery Postcode:</b>
						</div>
						<div class="col-xs-8" align="left">
							<?php
							if($gl_voucher_brand_data->contact_num =='' || $gl_voucher_brand_data->contact_num=='0')
							{
							?>
								<input type="text" data-label="Delivery Postcode:" class="helvetica_font form-control" name="delivery_postcode" value="<?php echo ($_POST) ? ($this->input->post('delivery_postcode')): (($gl_voucher_brand_data->delivery_postcode ==0)? '':$gl_voucher_brand_data->delivery_postcode);?>"id="delivery_postcode" placeholder="Delivery Postcode" />
							<?php
							}
							else
							{
								echo $gl_voucher_brand_data->contact_num;
							}
							?>	
						</div>
					</div><br>
                    
                    
					<div style="font-weight:;font-size:px;display:none">
						<b>Delivery Type:</b> For <?php echo $gl_voucher_brand_data->delivery_type;?>
					</div>
					<br>
					<div class="row">
						<div class="col-xs-8" align="left">
							
						</div>
						<div class="col-xs-4" align="right">
							<?php
							if($gl_voucher_brand_data->contact_num =='' || $gl_voucher_brand_data->contact_num =='0')
							{
							?>
							<button type="button" id="save" class="btn-custom-blue-grey_normal" >Save</button>
							<?php
							}
							?>
						</div>
					</div>
				</form>
				</div>
				<br>
				<hr size="1" style="margin:5px;">
					<div class="row">
						<div class="col-xs-12" align="left">
							<div style="font-weight:bold;font-size:14px;">
								Cart Details
							</div>
						</div>
					</div>
				<hr size="1" style="margin:5px;">
				<?php 
				if($cart_data!="")
				{
				
					$grand_total = 0;

					foreach($cart_data as $row):
					$product = json_decode($row->prod_details);
					//print_r($product);
						$grand_total += (($product->prod_merchant_cost  ) * $row->prod_qty);
				?>
					<div class="page-line cl body_bottom_1px merchant-products" id="item_wrapper_<?php echo $product->product_id;?>" style="background:#fff;">
                        <div class="container p5px_0px global_margin">
                            <div class="row">
								<div class="col-xs-2" style="border:solid 0px #333">
									<div align="left">
										<img style="width:40px;height:40px;" src="<?php echo site_url();?>/custom/uploads/products/thumb/<?php echo $product->prod_image;?>" alt="<?php echo $product->prod_name;?>">
									</div>
								</div>
								<div class="col-xs-9" >
									<b class="menu_merchant" ><?php echo $product->merchant_name;?></b>
									<div class="menu_product" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:100% !important;border:solid 0px #333;"><?php echo $product->prod_name;?></div>
								</div>
								<!--
								<div class="col-xs-1" align="right" style=";padding:0px;">
									<div class="menu_merchant">
										$&nbsp;<?php echo number_format(($product->prod_merchant_cost  ),2);?>
									</div>
								</div>
								-->
								<div class="col-xs-1 menu_merchant" style="border:solid 0px #333;" >
									Qty:<?php echo $row->prod_qty;?>
								</div>
								<!--
								<div class="col-xs-3 menu_merchant" align="right" style="font-weight:bold;font-size:14px;" id="total_<?php echo $product->product_id;?>">
									$ <?php echo number_format(( ($product->prod_merchant_cost  ) ) * ($row->prod_qty),2);?>
								</div>
								-->
							</div>
                        </div>
					</div>
				<?php endforeach;
				}
				else
				{
					echo "No items on your cart yet.";
				}
				?>
				<div class="page-line cl body_bottom_1px" style="margin-bottom:2px" >
				</div>
				<?php
				if($grand_total > 0)
				{
				?>
				<!--
				<div class="row">
					<div class="col-xs-8">
					</div>
					<div class="col-xs-4" align="right" style="font-size:14px;font-weight:bold">
						Total: $ <span id="grand_total">
							<?php echo number_format($grand_total,2);?>
						</span>&nbsp;
					</div>
				</div>
				-->
				<?php
				}
				?>
				
                    
                    
            </div>


			<!--body-->

				<div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/m/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <div id="fb-root"></div>
        <script>
            var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
            <?php
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) {
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            
            var GlompFormDialog = jQuery('form#GlompFormDialog').dialog({
                autoOpen: false,
                dialogClass : 'dialog_style_glomp',
                width: '80%',
                modal: true,
                buttons: [
                    {
                        text: 'Confirm',
                        class: 'fl btn-custom-blue-glomp_xs w100px',
                        click: function() {
                            GlompFormDialog.submit();
                        },
                        style: 'margin:1px;'
                    },
                    {
                        text: 'Cancel',
                        class: 'fr btn-custom-ash_xs w100px',
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }
                ]
            });
            
            var GlompFormAlert = jQuery('<div class="form-alert" />').dialog({
                autoOpen: false,
                width: '70%',
                modal: true,
                dialogClass: 'dialog_style_glomped_alerts',
                minHeight: 'auto'
            });
            
            var GlompFormPreload = jQuery('<div class="form-preload" />').dialog({
                autoOpen: false,
                width: '60%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait',
                title: 'Please wait...',
                minHeight: 'auto'
            }).html('<div style="text-align:center"><img width="40" src="/assets/m/img/ajax-loader.gif" /></div>');
            
            var GlompShareDialog = jQuery('#share-dialog').dialog({
                autoOpen: false,
                width: '90%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                minHeight: 'auto'
            });
            
            $(function() {
			
				$('#delivery_postcode').mask('000000');
				
				$('#save').click(function(e)
				{	
					var view="";
					$("#details_form input[type='text']").each(function(){
						 view +='<tr>\
								<th>'+$(this).data('label')+'</th>\
								<td valign="top">\
									<div style="width:215px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">\
									'+$(this).val()+'\
									</div>\
								</td>\
								</tr>\
						 ';
					});
					view +='<tr>\
								<th>Delivery Address</th>\
								<td>\
									<div style="width:215px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">\
										'+$('#delivery_address').val()+'\
									</div>\
								</td>\
								</tr>\
						 ';
					view="<table border='0' cellpadding='2'>"+view+"<table>";
				
					var NewDialog = $('<div id="" align="center" style="padding-top:5px !important; font-size:14px">\
									<p align="justify">\
										Please make sure that you have fill-up your form with correct details.\
										<hr>\
										'+view+'\
									</p>\
									</div>');
					NewDialog.dialog({                    
						dialogClass:'noTitleStuff dialog_style_glomp_wait',
						title: "",
						autoOpen: false,
						resizable: false,
						modal: true,
						width: 300,
						show: '',
						hide: '',
						buttons: [
								{text: "OK",
								"class": 'btn-custom-blue_xs w80px',
								click: function() {
									$(this).dialog("close");
									$("#details_form").submit();
								}},
								{text: "Cancel",
								"class": 'btn-custom-ash_xs w80px',
								click: function() {
									$(this).dialog("close");
								}}
						]
					});
					NewDialog.dialog('open');
					
				});
				
                $('#back_btn').click(function(e) {
                   e.preventDefault();
                   window.history.back();
                });
                $("#tour_cancel").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
				$("#tour_done").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
               
                
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});

            jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
                jQuery('.user-menu .user-icon .user-details').toggle();
            });
            
            jQuery(document).on('click','.toggle',function(e){
                e.preventDefault();
                toggleIn = jQuery(e.target).attr("toggle-in");
                toggleOut = jQuery(e.target).attr("toggle-out");
                
                jQuery(toggleIn).show();
                jQuery(toggleOut).hide();
            });

        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
		<script src="<?php echo minify('assets/m/js/custom.glomp_amex.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
</html>
