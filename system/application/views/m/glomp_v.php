<!DOCTYPE html>
<html>
    <head>
        <title>Glomped | glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">    
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>


        <script>
            $(function() {
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
                });
				$(".glomped_view").click(function() {
					var id = $(this).attr('id');
					var _merchant	=$('#v_'+id+'_merchant').html();
					var _item		=$('#v_'+id+'_item').html();
					var _img			=$('#v_'+id+'_img').html();
					var _desc		=$('#v_'+id+'_desc').html();
					
					var _by_name		=$('#v_'+id+'_by_name').html();
					var _by_image		=$('#v_'+id+'_by_image').html();
					var _by_link		=$('#v_'+id+'_by_link').html();
					var _redeem_link	=$('#v_'+id+'_redeem_link').html();
			
					
					var NewDialog = $('<div id="glompedDetails" align="center cl"> \
												<div class="cl fl menu_item_photo_wrapper">\
													<img class="menu_item_photo_2" alt="'+_item+'" src="'+_img+'"></img>\
												</div>\
												<div class="fl menu_item_details">\
													<div class="menu_merchant white">'+_merchant+'</div>\
													<div class="menu_product">'+_item+'</div>\
													<div class="">'+_desc+'</div>\
													<div class="fl menu_item_points_wrapper_2" align="center">'+_by_name+'</div>\
												</div>\
												<div class="cl"></div>\
											</div>');
											
					NewDialog.dialog({						
					autoOpen: false,
					resizable: false,					
					dialogClass:'dialog_style_glomped_details',
					title:'',
					modal: true,
					width:280,
					height:200,
					buttons: [                							
						{text: "Redeem", 
						"class": 'btn-custom-white_xs fl',
						click: function() {
							window.location.href = _redeem_link;
						}},
						{text: "Cancel", 
						"class": 'btn-custom-blue-grey_xs fr',
						click: function() {
							$(this).dialog("close");
							setTimeout(function() {
								$('#glompedDetails').dialog('destroy').remove();
							}, 500 );						
						}}
					]
				});
				NewDialog.dialog('open');					
				
				});                
			
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('profile'); ?>
                                        </div>
                                    </a>
                                </li>
                                
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>
                    </a>
                </div>
                 <!-- hidden navigations      
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/profile') ?>">
                        <div class="hidden_nav">                        
                            <?php echo $this->lang->line('profile'); ?>
                        </div>
                    </a>				                
                </div>
                <!-- hidden navigations -->
            </div>

           <div class="p20px_0px" style="margin-top:12px;"></div>			
			
			<!--body-->     
			<div class="container p5px_0px global_margin " style="text-align:justify ">
            	<div class="container p10px_0px ">
                    <div class="row red harabarabold" style="font-size: 20px;"><?php echo $this->lang->line('Received');?></div>
				</div>
			</div>	
            <div class="cl body_bottom_1px"></div>
            
            
            <?php 							
			if($glomped_me->num_rows()>0)
			{
				foreach($glomped_me->result() as $rec_glomped_me)
				{			
					/*/sleep(1);*/
                    
					$voucher_id					= $rec_glomped_me->voucher_id;
					
					$prod_id 					= $rec_glomped_me->voucher_product_id;
					$glomped_message 			= $rec_glomped_me->voucher_sender_glomp_message;
					$glomped_by_name 			= $rec_glomped_me->user_name;
					$glomped_by_photo 			= $this->custom_func->profile_pic($rec_glomped_me->user_profile_pic,$rec_glomped_me->user_gender);
					$glomped_by_name_link		= 	base_url((MOBILE_M.'/profile/view/'.$rec_glomped_me->voucher_purchaser_user_id));
				 
					$glomped_me_prod 			= json_decode($this->product_m->productInfo($prod_id));
					$prod_name 					= $glomped_me_prod->product->$prod_id->prod_name;
					$product_logo 				= $this->custom_func->product_logo($glomped_me_prod->product->$prod_id->prod_image);
					$product_desc 				= $this->product_m->product_desc($prod_id);
					
					
					$merchant_name 				= $glomped_me_prod->product->$prod_id->merchant_name;
					$merchant_name_id			= $rec_glomped_me->voucher_merchant_id;
					$merchant_logo 				= $this->custom_func->merchant_logo($glomped_me_prod->product->$prod_id->merchant_logo);
					$remaining_days 				=  $this->custom_func->compute_remaining_days( date('Y-m-d'), ($rec_glomped_me->voucher_expiry_date));
			
					$glomped_redeem_link		=base_url(MOBILE_M.'/redeem/voucher/'.$voucher_id);		
					
					if($rec_glomped_me->voucher_orginated_by>0) /*/ given by glomp*/
					{ 					
						if($rec_glomped_me->voucher_type=='Assignable')
						{ /*/ given by glomp Assignable 											*/
							if((isset($rec_glomped_me->voucher_assigned_date) && ($rec_glomped_me->voucher_assigned_date=="" || $rec_glomped_me->voucher_assigned_date==NULL)) &&  $rec_glomped_me->voucher_status== 'Assigned' )
							{ /*/ given by glomp Assignable   status no assigned friend yet                            */
							?>
								
                                 <!--reward voucher to glomp-->
                                <div class="body_bottom_1px" >
                                    <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                        <div class="fl" style="width:54%;">
                                            <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/voucher_reward.png');" ></div>
                                            <div class="glomped_user" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                            <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                                <div class="fl glomped_remaining"><?php echo $remaining_days;?> <?php echo $this->lang->line('remaining'); ?></div>
                                                <div class="fr glomped_share"></div>
                                            </div>  
                                        </div>
                                        <div class="fr p8px_0px_0px_0px" style="width:44%;" align="right">
                                        
	                                        <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">
	                                            <div><button class="btn-custom-blue-grey_xs w80px "  >Locate</button></div>
                                            </a>    
                                            <div><button class="btn-custom-blue-grey_xs w80px  glomp_white_icon" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_gray.png');" >Glomp!</button></div>
                                        </div>
                                    </div>  
                                </div>
                                <!--reward voucher to glomp-->
                                
							<?php 											
							} /*/ given by glomp Assignable   status no assigned friend yet end											*/
							else if($rec_glomped_me->voucher_status=='Expired')
							{												
							?>
								<!--reward voucher expired-->
                                <div class="body_bottom_1px glomped_expired" >
                                    <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                        <div class="fl" style="width:54%;">
                                            <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_voucher_rewards-expired-02.png');" ></div>
                                            <div class="glomped_user_expired" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                            <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                                <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                                            </div>  
                                        </div>
                                        <div class="fr p8px_0px_0px_0px" style="width:44%;" align="right">
                                            <div><button class="fr btn-custom-blue-grey_xs_2 w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>                                                                                    
                                            <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">	                                            
                                                <div><button class="fr btn-custom-blue-locate_xs_inactive" ></button></div>
                                            </a>
                                        </div>
                                    </div>  
                                </div>
                                <!--reward voucher expired-->                               
							<?php } /*/ given by glomp Assignable   status expired 											*/
						} /*/ given by glomp Assignable end*/
						else if($rec_glomped_me->voucher_type=='Consumable') /*/ given by glomp Consumable*/
						{
							if($rec_glomped_me->voucher_status== 'Consumable' )
							{ /*/ given by glomp Consumable   status Consumable*/
							?>
								
                                <!--reward voucher to redeem-->
                                <div class="body_bottom_1px" >
                                    <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                        <div class="fl" style="width:54%;">
                                            <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/voucher_reward.png');" ></div>
                                            <div class="glomped_user" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                            <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                                <div class="fl glomped_remaining"><?php echo $remaining_days;?> <?php echo $this->lang->line('remaining'); ?></div>
                                                <div class="fr glomped_share"></div>
                                            </div>  
                                        </div>
                                        <div class="fr " style="width:44%;" align="right">                                            
											<a class="" href="<?php echo base_url(MOBILE_M.'/redeem/voucher/'.$voucher_id);?>">
												<div><button class="fr btn-custom-green-grey_xs w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>
											</a>
                                            <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">	                                            
                                                <div><button class="fr btn-custom-blue-locate_xs_active " ></button></div>
                                            </a>
                                        </div>
                                    </div>  
                                    <div class="hidden">
                                        <div id="v_<?php echo $voucher_id;?>_merchant"><?php echo stripslashes( $merchant_name);?></div>
                        
                                        <div id="v_<?php echo $voucher_id;?>_item"><?php echo stripslashes( $prod_name);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_img"><?php echo base_url().$product_logo;?></div>
                                        <div id="v_<?php echo $voucher_id;?>_desc"><?php echo stripslashes( $product_desc);?></div>
                                        
                                        <div id="v_<?php echo $voucher_id;?>_by_name"><?php echo stripslashes( $glomped_by_name);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_by_image"><?php echo stripslashes( $glomped_by_photo);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_by_link"><?php echo stripslashes( $glomped_by_name_link);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_redeem_link"><?php echo stripslashes( $glomped_redeem_link);?></div>
                                        
                                    </div>
                                </div>
                                <!--reward voucher to redeem-->                               
							<?php 											
							} /*/ given by glomp Consumable   status Consumable end*/
							else if($rec_glomped_me->voucher_status=='Expired')
							{ /* given by glomp Consumable   status Expired*/
							?>
								<!--reward voucher expired-->
                                <div class="body_bottom_1px glomped_expired" >
                                    <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                        <div class="fl" style="width:54%;">
                                            <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_voucher_rewards-expired-02.png');" ></div>
                                            <div class="glomped_user_expired" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                            <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                                <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                                            </div>  
                                        </div>
                                        <div class="fr p8px_0px_0px_0px" style="width:44%;" align="right">
                                            <div><button class="fr btn-custom-blue-grey_xs_2 w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>                                                                                    
                                            <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">	                                            
                                                <div><button class="fr btn-custom-blue-locate_xs_inactive" ></button></div>
                                            </a>
                                        </div>
                                    </div>  
                                </div>
                                <!--reward voucher expired--> 
							<?php } /* given by glomp Consumable   status Expired end*/
						} /* given by glomp consumable end*/
					} /* given by glomp end*/
					else{ /* given by friend					*/
						if($rec_glomped_me->voucher_status=='Redeemed')
						{  /* given by friend status redeemed*/
						?>
                        	<!-- glomped expired ! -->
                            <div class="body_bottom_1px glomped_expired" >
                                <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                    <div class="fl" style="width:54%;">
                                        <div class="glomped_merchant_prod_name_expired" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                        <div class="glomped_user_expired"><?php echo $glomped_by_name;?></div>
                                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                            <div class="fl glomped_expired"><?php echo $this->lang->line('Redeemed'); ?></div>
                                        </div>  
                                    </div>
                                    <div class="fr p8px_0px_0px_0px" style="width:44%;" align="right">
                                        <a class="" href="<?php echo base_url(MOBILE_M.'/redeem/voucher/'.$voucher_id);?>">
                                            <div><button class="fr btn-custom-blue-grey_xs_2 w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>                                                                                    
                                        </a>        
                                        <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">	                                            
                                            <div><button class="fr btn-custom-blue-locate_xs_inactive" ></button></div>
                                        </a>
                                    </div>
                                </div>  
                            </div>
                            <!-- glomped expired ! -->   
                        
                        <?php } /* given by friend status redeemed end*/
						else if($rec_glomped_me->voucher_status=='Consumable')
						{ /* given by friend status Consumable*/
						?>
                        	<!-- glomped! -->
                            <div class="body_bottom_1px" >
                                <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                    <div class="fl" style="width:54%;">
                                        <div class="glomped_merchant_prod_name" ><?php echo $merchant_name;?> <?php echo $prod_name;?> </div>
                                        <div class="glomped_user"><?php echo $glomped_by_name;?></div>
                                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                            <div class="fl glomped_remaining"><?php echo $remaining_days;?> <?php echo $this->lang->line('remaining'); ?></div>
                                            <div class="fr glomped_share"></div>
                                        </div>  
                                    </div>
                                    <div class="fr " style="width:44%;" align="right">
                                        <a class="" href="<?php echo base_url(MOBILE_M.'/redeem/voucher/'.$voucher_id);?>">
												<div><button class="fr btn-custom-green-grey_xs w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>
										</a>
                                        <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">
	                                            <div><button class="fr btn-custom-blue-locate_xs_active " ></button></div>
                                            </a>                                        
                                    </div>
                                    <div class="hidden">
                                        <div id="v_<?php echo $voucher_id;?>_merchant"><?php echo stripslashes( $merchant_name);?></div>
                        
                                        <div id="v_<?php echo $voucher_id;?>_item"><?php echo stripslashes( $prod_name);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_img"><?php echo base_url().$product_logo;?></div>
                                        <div id="v_<?php echo $voucher_id;?>_desc"><?php echo stripslashes( $product_desc);?></div>
                                        
                                        <div id="v_<?php echo $voucher_id;?>_by_name"><?php echo stripslashes( $glomped_by_name);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_by_image"><?php echo stripslashes( $glomped_by_photo);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_by_link"><?php echo stripslashes( $glomped_by_name_link);?></div>
                                        <div id="v_<?php echo $voucher_id;?>_redeem_link"><?php echo stripslashes( $glomped_redeem_link);?></div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <!-- glomped! -->
						<?php } /* given by friend status Consumable end*/
						else if($rec_glomped_me->voucher_status=='Expired')
						{ /* given by friend status Expired*/
						?>
                        	<!-- glomped expired ! -->
                            <div class="body_bottom_1px glomped_expired" >
                                <div class="cl container p5px_0px global_margin" style="text-align:left ">
                                    <div class="fl" style="width:54%;">
                                        <div class="glomped_merchant_prod_name_expired" ><?php echo $merchant_name;?> <?php echo $prod_name;?></div>
                                        <div class="glomped_user_expired"><?php echo $glomped_by_name;?></div>
                                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                                            <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                                        </div>  
                                    </div>
                                    <div class="fr p8px_0px_0px_0px" style="width:44%;" align="right">                                                                                
                                        <div><button class="fr btn-custom-blue-grey_xs_2 w80px " id="<?php echo $voucher_id;?>" style="">View</button></div>                                                                                    
                                        <a class="" href="<?php echo base_url(MOBILE_M).'/profile/menu?tab=merchants&mID='.$merchant_name_id.'&mDetail=locations';?>">	                                            
                                            <div><button class="fr btn-custom-blue-locate_xs_inactive" ></button></div>
                                        </a>
                                    </div>
                                </div>  
                            </div>
                            <!-- glomped expired ! -->                        	
						<?php } /* given by friend status Expired end*/
					} /* given by friend end*/
					?>		
				<?php }/* for each*/?>
			<?php } /*if*/ ?>
            
            
            <!--- templates -->
            <!--- templates -->
            <!-- glomped! --><!--
            <div class="body_bottom_1px" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_merchant_prod_name" >Merchant name Product</div>
                        <div class="glomped_user">Friend's name</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_remaining">2 days <?php echo $this->lang->line('remaining'); ?></div>
                            <div class="fr glomped_share">Share</div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                        <div><button class="btn-custom-blue-grey_xs w80px "  >Locate</button></div>
                        <div><button class="btn-custom-blue-grey_xs w80px "  >View</button></div>
                    </div>
				</div>  
            </div>
            <!-- glomped! -->
            
            <!-- glomped expired ! --><!--
            <div class="body_bottom_1px glomped_expired" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_merchant_prod_name_expired" >Merchant name Product</div>
                        <div class="glomped_user_expired">Friend's name</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                    </div>
				</div>  
            </div>
            <!-- glomped expired ! -->
            
            
            <!--reward voucher to redeem--><!--
            <div class="body_bottom_1px" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/voucher_reward.png');" ></div>
                        <div class="glomped_user" >Merchant name Product</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_remaining">2 days <?php echo $this->lang->line('remaining'); ?></div>
                            <div class="fr glomped_share">Share</div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                        <div><button class="btn-custom-blue-grey_xs w80px "  >Locate</button></div>
                        <div><button class="btn-custom-blue-grey_xs w80px "  >View</button></div>
                    </div>
				</div>  
            </div>
            <!--reward voucher to redeem-->
            
            
            <!--reward voucher to glomp--><!--
            <div class="body_bottom_1px" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/voucher_reward.png');" ></div>
                        <div class="glomped_user" >Merchant name Product</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_remaining">2 days <?php echo $this->lang->line('remaining'); ?></div>
                            <div class="fr glomped_share">Share</div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                        <div><button class="btn-custom-blue-grey_xs w80px "  >Locate</button></div>
                        <div>
                        	<button class="btn-custom-blue-grey_xs w80px  glomp_white_icon" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_gray.png');" >Glomp!</button>
                        </div>
                    </div>
				</div>  
            </div>
            <!--reward voucher to glomp-->
            
            
            <!--reward voucher expired--><!--
            <div class="body_bottom_1px glomped_expired" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_voucher_rewards-expired-02.png');" ></div>
                        <div class="glomped_user_expired" >Merchant name Product</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                    </div>
				</div>  
            </div>
            <!--reward voucher expired-->
            
            
            <!--birthday voucher--><!--
            <div class="body_bottom_1px" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/voucher_birthday.png');" ></div>
                        <div class="glomped_user" >Merchant name Product</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_remaining">2 days <?php echo $this->lang->line('remaining'); ?></div>
                            <div class="fr glomped_share">Share</div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                        <div><button class="btn-custom-blue-grey_xs w80px "  >Locate</button></div>
                        <div><button class="btn-custom-blue-grey_xs w80px "  >View</button></div>
                    </div>
				</div>  
            </div>
            <!--birthday voucher-->
            
            
            <!--birthday voucher expired--><!--
            <div class="body_bottom_1px glomped_expired" >
            	<div class="cl container p5px_0px global_margin" style="text-align:justify ">
                    <div class="fl" style="width:70%;">
                        <div class="glomped_voucher" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_voucher_birthday_expired.png');" ></div>
                        <div class="glomped_user_expired" >Merchant name Product</div>
                        <div class="cl p5px_0px_0px_0px" style="width:100%;">
                            <div class="fl glomped_expired"><?php echo $this->lang->line('Expired'); ?></div>
                        </div>  
                    </div>
                    <div class="fr p8px_0px_0px_0px" style="width:24%;" align="right">
                    </div>
				</div>  
            </div>
            <!--birthday voucher expired-->
            <!--- templates -->
            <!--- templates -->
            
            
            
	        <div class="cl p20px"></div>
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->
        
    </body>
</html>
