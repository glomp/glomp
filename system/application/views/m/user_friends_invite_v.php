<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <a href="<?php echo site_url(MOBILE_M.'/user/searchFriends'); ?>" >
                        <div class="glomp_header_logo_2" style="height: 25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-friends-logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations                       
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                </div>
                <!-- hidden navigations -->
            </div>
            
            <div class="p20px_0px" style="margin-top:12px;"></div>			
            <div class="container p20px_0px global_margin " style="background-color: white;">
                <?php // Sets error message ?>
                <?php if ( validation_errors() != '' || isset($alert)) { ?>
                    <div id="error_container" class="alert alert-error">
                        <button type="button" class="close" id="error_close_btn" data-dismiss="alert">&times;</button>
                        <div id ="errors" align="left">
                            <?php echo validation_errors(); 
                                if (isset($alert)) {
                                    echo $alert;
                                }
                            ?>
                        </div>
                    </div>
                <?php } ?>
                
                <form method="POST">                    
						<div class="container p10px_0px ">
							<div class="row red harabarabold" style="font-size: 20px;">Invite</div>
						</div>
						<div class="row" style="margin-top: 10px;">
							<div class="fl" style="width: 50px;color:#708DA9;font-size:10px;">Name</div>
							<div class="fl" style="width: 200px;"><input id="name" name="name" type="text" value="<?php echo $name; ?>" class="register_form_gray" style="border: 0px solid; margin: 0px; width: 100%"/></div>
						</div>

						<div class="row" style="margin-top: 10px;">
							<div class="fl" style="width: 50px;color:#708DA9;font-size:10px;">Email</div>
							<div class="fl" style="width: 200px;"><input id="email" name="email" type="text" class="register_form_gray" value="<?php echo $email; ?>" style="border: 0px solid; margin: 0px; width: 100%"/></div>
						</div>
						<div class="row" style="margin-top: 10px;">
							<div class="fl" style="width: 50px;color:#708DA9;font-size:10px;">Location</div>
							<div class="fl" style="width: 200px;">
								<select name="location" id="location" class="textBoxGray" style="width: 200px;">
									<option value=""><?php echo $this->lang->line('Select_Location'); ?></option>
									<?php echo $this->regions_m->location_dropdown(0, set_value('location')); ?>
								</select>
							</div>
						</div>						
						<button name="submit_btn" value="nothing" class="btn-custom-blue-grey_xs w40px" style="position:absolute;top:-1000px;width: 69px;height: 22px;"></button>
						<?php if ($glomp_only == '0') { ?>
							<div class="row" style="margin-top: 10px;">
								<div class="fl" style="width: 50px;color:#708DA9;font-size:10px;">Message</div>
								<div class="fl" style="width: 200px;"><textarea id="message" name="message" type="text" class="register_form_gray" value="<?php echo $message; ?>" style="border: 0px solid; margin: 0px; width: 100%"></textarea></div>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="fl" style="width: 50px;height: 1px;"></div>
								<div class="fl"><button name="submit_btn" type="submit" value="invite" class="btn-custom-blue-grey_xs w40px" style="width: 69px;height: 22px;">Invite</button></div>
							</div>
						<?php } ?>
						<div class="row" style="margin-top: 5px;">
							<div class="fl" style="width: 50px;height: 1px;"></div>
							<button class="glomp_green" name="submit_btn" type="submit" value="glomp" style="background-image:url('<?php echo base_url('/assets/m/img/glomp-green.png'); ?>');width: 69px;background-color:#ABD03A;padding: 6px; height: 24px;background-size: 45px;background-position: center;"></button>
						</div>					
                </form>
            </div>   
            <!-- /footer -->
            <div class="footer_wrapper"></div>
            <!-- /footer --> 
        </div> <!-- /global_wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script>
           $(function() {	
           $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();										
                });
            
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
        </script>   
    </body>
</html>
