<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?></title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url()?>assets/m/css/style.css" rel="stylesheet" media="screen">    
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>	
  </head>
  <body>
	<?php include_once("includes/analyticstracking.php") ?>	
	<div class="global_wrapper" align="center">	
	  <a href="<?php echo base_url(MOBILE_M)?>">
		<div class="main_logo" style="background-image:url('<?php echo base_url()?>assets/m/img/logo.png');"></div>
	  </a>
      <div class="container p24px_p24px">
			<div class="row" align="left">
				<h3 class="what_is_glomp_red "><?php echo $this->lang->line('what_is_glomp');?></h3>
			</div>
			<div class="row" align="left" >			
				<p class="what_is_glomp_details">
					<?php echo $this->lang->line('what_is_glomp_details');?>
				</p>
			</div>
			<a href="http://www.youtube.com/embed/u97L6GuBbAE?wmode=opaque" target="_blank">
				<div class="row p24px_p24px" align="center">
					<div class="glomp_play" style="background-image:url('<?php echo base_url()?>assets/m/img/glomp_play.png');"></div>	
					<div class="learn_more" align="center"><?php echo $this->lang->line('learn_more');?></div>
				</div>
			</a>
		</div> <!-- /container -->
		<div class="footer_wrapper"> <!-- /footer -->
		</div> <!-- /footer -->
	</div> <!-- /global_wrapper -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/m/js/bootstrap.min.js"></script>
  </body>
</html>
