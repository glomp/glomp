<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?></title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Contact glomp! support. Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
    <meta name="keywords" content="<?php echo meta_keywords();?>" >
    <meta name="author" content="<?php echo meta_author();?>" >
    <!-- Bootstrap -->
    <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">   
	</style>
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>	
  </head>
  <body style="background-color:#FFFFFF; border-left: #B0B0B0  solid 1px;	border-right: #B0B0B0 solid 1px;		">
	<?php include_once("includes/analyticstracking.php") ?>	
	<div class="global_wrapper" align="center">		      
         <div class="navbar navbar-default ">
            <div class="header_navigation_wrapper fl" align="center">				
                <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                    <nav>
                        <a href="#" id="menu-icon-nav"></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                    <div class="w100per fl white">
                                    <?php echo $this->lang->line('Home'); ?>
                                    </div>
                                </a>
                            </li>                                    
                        </ul>

                    </nav>
                </div>
                <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url()?>assets/m/img/contact_support.png');"></div>
            </div>
        </div>		
         <!-- hidden navigations -->        
        <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
            <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                <div class="hidden_nav" align="left">
                    <?php echo $this->lang->line('Home'); ?>
                </div>
            </a>
        </div>
        <!-- hidden navigations -->	
		<!--body -->			
		<div class="container p20px_0px global_margin ">						
			<form method="post" name="frmSupport" id="frmSupport">
				<div class="cl " style="text-align:justify;color:#585F6B">
					<div class="cl ">
						<?php if(isset($success_msg))
						{?>
							 <div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<?php echo $success_msg;?>
							</div>
						<?php
						}
						if(validation_errors()!="")
						{		?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<?php echo $validation_errors();?>
							</div>
						<?php 	}	?>
					</div>
					<div class="register_form_gray" align="left">
						<input name="subject" required  id="subject" type="text" placeholder="Subject*" style="text-align:left !important;padding:5px 5px; font-size:13px;width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
					</div>
					<div class="register_form_gray" align="left">
						<textarea name="message" required   id="message" placeholder="Description*" style="text-align:left !important;padding:5px 5px; font-size:13px;width: 100%; height:150px; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" ></textarea>						
					</div>
					<div class=""> 					
						* Required field
					</div>										
					<div class="fr ">
						<button class="btn-lg btn-custom-blue-grey border_radius_4px" name="sendMessage" type="submit"><?php echo $this->lang->line('Submit'); ?></button>
					</div>					
				</div>				
			</form>			
		</div>
		<!--body -->	
		<div class="footer_wrapper"> <!-- /footer --></div> <!-- /footer -->
	</div> <!-- /global_wrapper -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/m/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/frontend/js/validate.js"></script>	
    <script>
       $(function() {	
	   $("#main_menu").click(function() {
				$("#hidden_menu").toggle();										
			});
		
		});
		$(document).mouseup(function(e)
		{
			var container = $(".hidden_menu_class");
			if (!container.is(e.target) /* if the target of the click isn't the container...*/
					&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
			{
				$('#hidden_menu').hide();
			}
		});
	</script>  
  </body>
</html>
