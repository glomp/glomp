<?php
$user_id = $this->session->userdata('user_id');
$inTour_display = '';
$inTour_header = '';
$inTour_header_display = 'display:none;';
$inTour_display_footer = 'display:none;';
$header_text = '';
?>
<!DOCTYPE html>
<html>
    <head>
        <title> Amex| glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords(''); ?>" >
        <meta name="author" content="<?php echo meta_author(); ?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript">
            <?php $public_user_data = $this->session->userdata('public_user_data'); ?>
            var public_email = "<?php echo $public_user_data['user_email']; ?>";
            var public_fname = "<?php echo $public_user_data['user_fname']; ?>";
            var public_lname = "<?php echo $public_user_data['user_lname']; ?>";
            var items = [];
        </script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
        <?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display; ?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                    <li>
                                        <a href="#" class="white "  id = "back_btn">
                                            <div class="w100per fl white">
                                                Back
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php
                                if ($this->session->userdata('public_user_since_user_logged_in') != '') { ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/brands/view/amex/'. WINESTORE_BRAND_PRODUCT_ID) ?>" class="white ">
                                            <div class="w100per fl white">
                                                <?php echo $this->lang->line('Back', 'Back'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>/amex-sg" class="white ">
                                        <div class="w100per fl white"><?php echo $this->lang->line('Home'); ?></div>
                                    </a>
                                </li>
                                <?php if ($this->session->userdata('is_user_logged_in') == true) { ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M . '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                                <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M . '/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                                <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->session->userdata('user_id') == '' && $this->session->userdata('public_user_since_user_logged_in') != '' ) { ?>
                                    <li>
                                        <a href="<?php echo base_url() ?>brands/logout/m" class="white ">
                                            <div class="w100per fl white">
                                                <?php echo $this->lang->line('exit', 'Exit'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </nav>
                    </div>
                    <?php $header_text = 'Winestore'; ?>
                    <div style="font-size: 24px;color: white;padding: 8px;"><?php echo $header_text; ?></div>
                </div>
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
            <div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display; ?>">
                <div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header; ?></div>
            </div>

            <style type="text/css">
                .brand-page-header,
                .brand-page-logo,
                .brand-page-description {
                    position: relative;
                }
                .brand-page-title {
                    position:absolute;
                    top:10%;
                    right:0;
                    background: #5A606C;
                    -webkit-border-top-left-radius: 10px;
                    -webkit-border-bottom-left-radius: 10px;
                    -moz-border-radius-topleft: 10px;
                    -moz-border-radius-bottomleft: 10px;
                    border-top-left-radius: 10px;
                    border-bottom-left-radius: 10px;
                    padding:10px 40px 10px 15px;
                }
                .brand-page-title h1 {
                    margin:0;
                    font-size: 1.5em;
                    font-weight: bold;
                    color:#eee;
                }
                .brand-page-logo {
                    padding: 0 20px;
                    max-width: 100%;
                }
                .brand-page-logo img {
                    width:100%;
                }
                .brand-page-description {
                    padding: 0 20px;
                    text-align: justify;
                }
                .brand-page-products-link {
                    padding: 0 20px;
                }
                .brand-page-products-link a {
                    background: #5A606C;
                    padding:8px 20px;
                    font-size: 1.5em;
                    font-weight: bold;
                    color:#eee;
                    display: inline-block;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .brand-page-products-link a:active {
                    background: #777;
                    text-decoration: none;
                }
                .brand-page .page-line {
                    margin-top:20px;
                    position: relative;
                }
                .brand-section {
                    margin-bottom:2px;
                }

                .product-list:after {
                    content: " ";
                    display:block;
                    clear: left;
                }
                .product-list a.product {
                    width:47%;
                    margin-left: 2%;
                    float: left;
                    background: #ddd;
                    margin-bottom: 2%;
                }
                .product-list a.product img {
                    width:100%;
                    background: #222;
                    border:0;
                }
                .product-list a.product p {
                    margin:0;
                    padding:5px 10px;
                    color: #555;
                }
                div.product {
                }
                div.product .product-image {
                    text-align: center;
                }
                div.product .product-image img {
                    width: 96%;
                }
                div.product .product-info {
                    padding: 0 20px;
                }
                div.product .product-info h3 {
                    margin:0;
                    padding: 0.6em 0;
                    font-size:1.4em;
                }
                div.product-glomp {
                    padding: 20px;
                }
                div.product-glomp a {
                    display:block;
                    background: #A8CF3E;
                    padding: 5px;
                    text-align:center;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                }
                div.product-glomp a img {
                    height:2em;
                }
                .btn-back {
                    position:absolute;
                    top:35%;
                    right:1em;
                    padding:5px 20px;
                    background:#5A606C;
                    color:#eee;
                    font-weight:bold;
                    font-size: 1.2em;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                }
                div.product-merchants {

                }
                div.product-merchants a {
                    float: left;
                    width: 24%;
                    margin-left: 4%;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                    -webkit-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    -moz-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    overflow: hidden;
                }
                div.merchants-list {
                    overflow: auto;
                }
                div.product-merchants a img {
                    width: 100%;
                }
                div.search-info {
                    margin-bottom: 20px;
                }
                div.search-info h4 {
                    font-size: 0.9em;
                    text-transform: uppercase;
                    margin:0;
                    padding: 5px;
                    background: #5A606C;
                    color: #eee;
                }
                div.search-info .search-data {
                    padding:5px;
                    background: #bfc4ce;
                }
                div.search-info .search-data:after {
                    content: "";
                    display:block;
                    clear:left;
                }
                div.search-info .search-data img {
                    float:left;
                    width: 25%;
                }
                div.search-info .search-data p {
                    float:left;
                    margin-left:5px;
                }

                .btn-custom-blue_xs a {
                    color: white;
                    text-decoration: none;
                }

                .col-xs-6 {padding: 0px !important}
                .menu_item_points_wrapper_amex {margin: 5px 0px 10px 8px; !important}
                .menu_item_description_wrapper { padding: 10px 0 10px; min-height: 63px; }
                .menu_item_points_wrapper_amex { margin: 5px 0px 10px; }
                .menu_item_photo_wrapper {margin-left: -20px; }
                .menu_and_point_wrapper {position: relative} 
            </style>
            <div class="page brand-page">

                <div style="padding:10px">
                    <a href="<?php echo site_url(MOBILE_M); ?>/brands/cart" >
                        <div style="float:right;" class="btn-custom-blue-grey_xs" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty') == '' ? 'class="gray">0' : 'class="red">' . $this->session->userdata('session_cart_qty'); ?></span>)</div>
                    </a>
                </div>

                <div class="page-line cl body_bottom_1px merchant-products" style="background:#fff;">
                    <div class="container p5px_0px global_margin">
                        <div class="row">
                            <?php $list = get_winestore('list'); ?>		
                            <div class="row">
                                <div class="col-xs-12" style="background-color: #DEE5ED;padding: 15px;border-radius: 10px;margin: 10px 30px 10px 0px;color: #646B78;overflow: hidden;position: relative;">
                                    <div class="row"><?php echo $list[$group_id]['name']; ?></div>
                                    <br />
                                    <div class="row"><?php echo $list[$group_id]['desc_m']; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--body-->

            <div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer; ?> ">
                <div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
                    <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
                    <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
                </div>
            </div>
            <div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <div id="fb-root"></div>
        <script>
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var from_android = true;
            var is_user_logged_in = "<?php echo $this->session->userdata('is_user_logged_in'); ?>";
        </script>
    </body>
</html>
