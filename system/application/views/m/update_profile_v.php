<!DOCTYPE html>
<html>
    <head>
        <title>Settings | glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
		<style>
			.w200px2{
				width:200px !important;
				margin-left:10px !important;
			}
            ._error {
                border: 1px solid red !important;
            }
		</style>
    <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" align="center">   
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white"><?php echo $this->lang->line('Home'); ?></div>
                                    </a>
                                </li>                                    
                                <li>
                                    <a href="<?php echo site_url(MOBILE_M. '/user/logout') ?>" class="white ">
                                        <div class="w100per fl white">
                                            <?php echo $this->lang->line('log_out'); ?>
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>
                        </nav>
                    </div>	
                    <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url()?>assets/m/img/settings.png');"></div>
                </div>
            </div>		
            <div class="p20px_0px" style="margin-top:12px;"></div>	
            <div class="container p20px_0px global_margin" style="background-color: white;font-size: 14px;font-weight: bold; color:#808283">
                <div id="error_container" class="alert alert-error hide">
                    <button type="button" class="close" id="error_close_btn">&times;</button>
                    <div id ="errors" align="left" >
                        <div class="alert alert-error"  id ="errors_inner" style="font-size:11px !important; margin: 0px !important;" >
                        </div>
                    </div>
                </div>

                <form id="register_form" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					 <?php
						  
							 if(isset($success_msg))
					  {
						   echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$success_msg."</div>";
						  
						  }						 
						 
                    ?>      
                    <div style="display:none"> 
                        <div class="" id="register_error_from_load" style="" >
                            <?php
                            if (validation_errors() != "" || isset($error_msg) || isset($uploadEerror)) 
                            {
                            ?>
                                <div class="alert alert-error" style="font-size:11px !important; margin: 0px !important;" >
                                <?php
                                if (validation_errors() != "") {
                                    echo validation_errors();
                                }
                                if (isset($error_msg)) {
                                    echo $error_msg;
                                }
                                if (isset($uploadEerror)) {
                                    echo $uploadEerror;
                                }
                                ?>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class=" global_border" style="min-width: 100px;min-height: 100px;">
                        <div class="register_form_gray" align="left">
                            <input name="fname" id="fname" type="text" placeholder="First name"  value="<?php echo $user_record->user_fname;?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="register_form_gray" align="left">
                            <input name="lname" id="lname" type="text" placeholder="last Name"  value="<?php echo $user_record->user_lname;?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="border_topbot_gray" style="padding-right: 15px;" align="left">
                            <!-- row1 -->
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;">Date of Birth</div>
									<?php 
									$dob = explode('-',$user_record->user_dob);
									if(count($dob)!==3){
										$dob[0] = '';$dob[1] = '';$dob[2] = '';
									}
										
									?>
                                <div class="fl">
                                    <div class="fl">
                                        <input name="day" id="day" maxlength="2"  value="<?php echo $dob[2];?>"   class="register_form_gray" type="text" placeholder="DD" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="months" id="months" maxlength="2"  value="<?php echo $dob[1];?>"   class="register_form_gray" type="text" placeholder="MM" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;margin-left: 15px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="year" id="year" maxlength="4"  value="<?php echo $dob[0];?>"   class="register_form_gray" type="text" placeholder="YYYY" style="width: 49px; border: 0px solid; font-weight: bold;margin-left: 15px;margin-right: 7px;" />
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row1 -->
                            <!-- row2 -->
                            <?php 
                                /*TODO: CHANGE THIS QUICK FIX*/
                                if (empty($user_record->user_dob_display)) {
                                    $user_record->user_dob_display = 'dob_display_w_year';
                                }
                            ?>
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Display</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="dob_display" id="dob_display_w_year" value="dob_display_w_year" <?php echo $checked = ($user_record->user_dob_display == 'dob_display_w_year') ? 'Checked="checked"' : ""; ?> /> DD / MM / YYYY
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="dob_display" id="dob_display_wo_year" <?php echo $checked = ($user_record->user_dob_display == 'dob_display_wo_year') ? 'Checked="checked"' : ""; ?> value="dob_display_wo_year"> DD / MM
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row2 -->
                            <div class="fl" style="width: 100%">
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Gender</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo ($user_record->user_gender == 'Male') ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="gender" id="gender-female" <?php echo ($user_record->user_gender == 'Female') ? 'Checked="checked"' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="cl"></div>
                            <!-- row2 -->
                        </div>
                        <div class="register_form_gray" align="left">
                            <select name="location" id="location" class="textBoxGray" style="width:100%">
                                <option value=""><?php echo $this->lang->line('Select_Location');?></option>
								  <?php 
								  echo $this->regions_m->location_dropdown(0,$user_record->user_city_id);
								  ?>
                            </select>
                        </div>
                        <div align="left">
                            <div class="fl" style="width:95px"><?php echo $this->lang->line('language','Language');?></div>
                            <div class="fl">
                                <label for="lang_en">
                                    <input type="radio" name="user_lang_id" value="1" id="lang_en" <?php echo ($user_record->user_lang_id==1)?'checked':'';?> />
                                    <?php echo $this->lang->line('lang_english', 'English')?>
                                </label>
                                <label for="lang_ch">
                                    <input type="radio" name="user_lang_id" value="2" id="lang_ch" <?php echo ($user_record->user_lang_id==2)?'checked':'';?> />
                                    <?php echo $this->lang->line('lang_chinese', 'Chinese')?>
                                </label>
                            </div>
                            <div class="cl"></div>
                        </div>
                    </div>
					<br />
                    <div class="global_border" style="min-width: 100px; height:100px;" align="left">
						<div class="cl"></div>
						<?php
							$profile_pic = $this->custom_func->profile_pic($user_record->user_profile_pic,$user_record->user_gender);
						?>
						<div class="fl" style="margin-right:5px;">
							<img src="<?php echo base_url().$profile_pic;?>" alt="<?php echo $user_record->user_name;?>" id="profile_pic" style="height:70px;width:70px"    class="img-polaroid"><br />
						</div>
						<div class="fl" style="border:0px solid; width:180px">
							<div class="upload">
								<a href="javascript:;" id="uploadFilePic" ><?php echo $this->lang->line('upload_profile_photo');?></a>
							   <span id="upload_loader"></span>
							   <p><?php echo $this->lang->line('Note');?>: <?php  
							   echo $this->lang->line('profile_image_upload_note'); ?></p>
							   <input type="file" name="user_photo" id="profilePhotoUpload" accept="image/*" style="visibility: hidden;" onchange='profilePic.UpdatePreview(this)' />
							</div>
						</div>						
					</div>
					<br />
                    <div class="global_border" style="min-width: 100px;">
                        <div class="cl"></div>
						<?php
							if (isset($user_record->user_fb_id) && $user_record->user_fb_id != "")
							{
							/*echo $user_record->user_fb_id;*/
							}
							else{
							?>
							<div align="left">
																	<div class="span6" >
																		<button type="button" class=" btn-custom-blue_fb glompFont"  style="font-weight:normal !important;border-radius: 0px !important;width: 100%;!important"  name="Link_Facebook" id="Link_Facebook" >Link Facebook account</button>
																	</div>
																</div>
																 <div class="row-fluid">
																	<div class="span12 border">

																	</div>
																</div>
							<?php
							}
							if (empty($user_record->user_linkedin_id)) { ?>
                                <div class="row-fluid">
                                    <div class="span6" >
                                        <button type="button" class=" btn-custom-blue_li glompFont"  style="font-weight:normal !important;border-radius: 0px !important;width: 100%;!important"  name="Link_LinkedIn" id="Link_LinkedIn" onclick ="linkLinkedInAccount(false, false)" >Link LinkedIn Account</button>                                        
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12 border">

                                    </div>
                                </div>
                            <?php } ?>
                            
                        <div class="register_form_gray" align="left">
                            <input name="email" id="email" type="text"  value="<?php echo $user_record->user_email;?>" placeholder="Email" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                    </div>
                    <br />
                    <div class="global_border" style="min-width: 100px;">
                        <div class="">
                            <div class="register_form_gray" align="left">
                                <input name="pword" id="pword" type="password"  maxlength="9"  placeholder="Current password" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                            <div class="register_form_gray" align="left">
                                <input name="npword" id="npword" type="password"  maxlength="9"  placeholder="New password" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
							<div class="register_form_gray" align="left">
                                <input name="cnpword" id="cnpword" type="password"  maxlength="9"  placeholder="Confirm new password" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
							<div  align="center">
								<a style="color:#59606B; font-size:14px; text-decoration:underline" href="<?php echo base_url(MOBILE_M.'/landing/forgotPassword');?>">
								Forgot Password?
								</a>
							</div>
                        </div>
                        <div class="" align="center" style="padding:10px 10px 10px 10px; max-width: 100%">
                            <?php echo $this->lang->line('passwors_tips'); ?>
                        </div>						
                        <div class="cl" align="right" style="padding-top:5px;">
                            <a href="javascript:void(0)" id="terminate_account" style="color:#59606B; text-decoration: underline">
                                Deactivate account
                            </a>
                        </div>
                        <div class="cl"></div>
						  
						
                    </div>
                    <br />
                  

                    <br />
                    <div class="">
                        <div class="fl" style="margin-left: 14px; margin-top: 10px">
                            <button type="submit"  name="update_profile" class="btn-lg btn-custom-blue-grey btn-block w100px" >Submit</button>
                        </div>
                        <div class="fr" style="margin-left: 14px;margin-top: 10px">
                            <a href="<?php echo site_url(MOBILE_M . '/landing/'); ?>" class="btn-lg btn-custom-blue-grey btn-block w100px" type="submit">Cancel</a>
                        </div>
                        <div class="cl"></div>
                    </div>
                </form>
            </div>
            <!-- /container -->
        </div> <!-- /global_wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
		<script>
                    var FB_APP_ID="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";									
                    var GLOMP_BASE_URL = "<?php echo base_url('');?>";
                    var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
                    var importOnly="";
                    
                    function liInitOnload() {
                        if (IN.User.isAuthorized()) {
                            /*NO NEED TO DO ANYTHING*/
                        }
                    }
                    
		   $(function() {

                /* highlight incomplete */
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    url: GLOMP_DESK_SITE_URL + 'user2/welcome_incomplete_profile',
                    success: function(r) {
                        if (! r.data.user_dob)  $('#day, #months, #year').addClass('_error');
                        if (! r.data.user_password)  $('#npword').addClass('_error');
                        if (! r.data.location)  $('#location').addClass('_error');
                        if (! r.data.profile_pic)  $('#profile_pic').addClass('_error');
                        return false;
                    }
                });

                $('#terminate_account').click(function(e)
                {
                    var NewDialog = $('<div id="deactivatePopup" align="left" style="color:">\
                    <h1 style="font-size:16px;margin:0px;padding:0px;font-weight:bold;color:#f00">Are you sure you want to deactivate your account?</h1>\
                    <br>\
                    <div class="row-fluid" style="font-size:12px;">\
                        <div class="span12">\
                            <b>Reason for leaving</b> <small class="red">(required)</small>\
                        </div>\
                        <div class="span12">\
                            <label class="radio inline" for="reason_1"><input id="reason_1" name="reason" type="radio" value="1" />I don\'t understand how to use glomp!</label>\
                            <label class="radio inline" for="reason_2"><input id="reason_2" name="reason" type="radio" value="2" />This is temporary. I\'ll be back.</label>\
                            <label class="radio inline" for="reason_3"><input id="reason_3" name="reason" type="radio" value="3" />I have a privacy concern.</label>\
                            <label class="radio inline" for="reason_4"><input id="reason_4" name="reason" type="radio" value="4" />I don\'t find glomp! useful.</label>\
                            <label class="radio inline" for="reason_5"><input id="reason_5" name="reason" type="radio" value="5" />I have another glomp! account.</label>\
                            <label class="radio inline" for="reason_6"><input id="reason_6" name="reason" type="radio" value="6" />Other, please explain further:</label>\
                        </div>\
                    </div>\
                    <br>\
                    <div class="row-fluid" style="font-size:12px;">\
                        <div class="span12">\
                            <b>Please explain further</b>\
                        </div>\
                        <div class="span12">\
                            <textarea id="comment" id= rows="5"  style="resize:none;width:260px;" ></textarea>\
                        </div>\
                    </div>\
                    </div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_glomped_ash_blue noTitleStuff',
                        title: '',
                        modal: true,
                        position: 'center',
                        width: 280,
                        buttons: [
                            {text: "Deactivate",
                                "class": 'btn-custom-blue-grey_normal',
                                click: function() {
                                    var reason = $('input:radio[name=reason]:checked').val();
                                    if(reason == undefined)
                                    {
                                        var NewDialog =$('<div><div style="font-size:12px;" class="_error  alert alert-error" >Please select a reason.</div></div>');
                                        NewDialog.dialog({						
                                            autoOpen: false,
                                            closeOnEscape: false ,
                                            resizable: false,					
                                            dialogClass:'dialog_style_glomped_ash_blue noTitleStuff ',
                                            modal: true,
                                            title:$('#Please_wait').text(),
                                            position: 'center',
                                            width:280,
                                            height:120,
                                            buttons:[
                                                {text: "Ok",
                                                "class": 'btn-custom-blue-grey_normal',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#waitPopup').dialog('destroy').remove();
                                                    }, 500);
                                                }}
                                            ]
                                            
                                        });

                                        NewDialog.dialog('open');
                                        return;
                                    }
                                    
                                    /*deactivate confirm popup*/
                                    var NewDialog = $('<div id="deactivateConfirmPopup" align="center">Are you sure you want to deactivate your account?</div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_glomped_ash_blue noTitleStuff',
                                        title: '',
                                        modal: true,
                                        position: 'center',
                                        width: 280,
                                        height: 120,
                                        buttons: [
                                            {text: "Deactivate Now",
                                                "class": 'btn-custom-red_normal',
                                                click: function() {
                                                    /**deactivate now */
                                                    var reason = $('input:radio[name=reason]:checked').val();
                                                    var comment = $('#comment').val();
                                                    var data = 'reason='+reason+'&comment='+encodeURIComponent(comment);
                                                    
                                                    $('#deactivateConfirmPopup').dialog("close");
                                                    setTimeout(function() {
                                                        $('#deactivatePopup').dialog("close");
                                                        $('#terminatePopup').dialog('destroy').remove();
                                                        $('#deactivatePopup').dialog('destroy').remove();
                                                    }, 10);
                                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                                       <div align="center" style="margin-top:5px;">Please wait...<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                                    NewDialog.dialog({
                                                        autoOpen: false,
                                                        closeOnEscape: false,
                                                        resizable: false,
                                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                        title: 'Please wait...',
                                                        modal: true,
                                                        position: 'center',
                                                        width: 200,
                                                        height: 120
                                                    });
                                                    NewDialog.dialog('open');
                                                    /**/
                                                    $.ajax({
                                                        type: "POST",
                                                        dataType: 'json',
                                                        data:'deactivate=deactivate&'+data,
                                                        
                                                        url: GLOMP_BASE_URL + 'ajax_post/deactivateAccount',
                                                        success: function(response) {
                                                            if(response.status=='Deactivated')
                                                            {
                                                                window.location.href =GLOMP_BASE_URL+'m/landing/logout/deactivated';
                                                            }
                                                        
                                                        }
                                                    });
                                                    /*deactivate now*/
                                                }
                                            },
                                            {text: "Cancel",
                                                "class": 'btn-custom-white_normal',
                                                click: function() {
                                                    $(this).dialog("close");
                                                    setTimeout(function() {
                                                        $('#deactivateConfirmPopup').dialog('destroy').remove();
                                                    }, 10);
                                                }
                                            }
                                        ]
                                    });
                                    NewDialog.dialog('open');
                                    /*deactivate confirm popup*/
                                }
                            },
                            {text: "Cancel",
                                "class": 'btn-custom-white_normal',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#deactivatePopup').dialog('destroy').remove();
                                    }, 500);
                                }
                            }
                        ]
                    });
                    NewDialog.dialog('open');
                    /*
                    var NewDialog = $('<div id="terminatePopup" align="center">Are you sure you want to terminate your account?</div>');
                    NewDialog.dialog({
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                        title: '',
                        modal: true,
                        position: 'center',
                        width: 280,
                        height: 120,
                        buttons: [
                            {text: "Yes",
                                "class": 'btn-custom-blue-grey_normal',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#terminatePopup').dialog('destroy').remove();
                                    }, 500);
                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                       <div align="center" style="margin-top:5px;">Please wait...<br><img style="width:40px" width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                        title: 'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width: 200,
                                        height: 120
                                    });
                                    NewDialog.dialog('open');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        data:'deactivate=deactivate',
                                        
                                        url: GLOMP_BASE_URL + 'ajax_post/deactivateAccount',
                                        success: function(response) {
                                            if(response.status=='Deactivated')
                                            {
                                                window.location.href =GLOMP_BASE_URL+'m/landing/logout/deactivated'
                                            }
                                        
                                        }
                                    });
                                   
                                    
                                }
                            },
                            {text: "No",
                                "class": 'btn-custom-white_normal',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#terminatePopup').dialog('destroy').remove();
                                    }, 500);
                                }
                            }
                        ]
                    });
                    NewDialog.dialog('open');
                    
                    */
                });
                
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
				});
				$('#uploadFilePic').click(function(e) {						   
					/*$('#profilePhotoUpload').click();*/
					/*$('#upload_loader').html('Please wait...<i class="icon-spinner"></i>');*/
					e.preventDefault();
					var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"></div>');
					NewDialog.dialog({						
						autoOpen: false,
						resizable: false,
						dialogClass: 'dialog_style_glomp_wait noTitleStuff',
						title:'',
						modal: true,
						width:260,
						height:100,				
						show: '',
						hide: '',
						buttons: [                															
							{text: "<?php echo $this->lang->line('upload_from_device');?>", 
							"class": 'btn-custom-green w200px2',
							click: function() {
								$(this).dialog("close");
								setTimeout(function() {
									$('#messageDialog').dialog('destroy').remove();
								}, 500 );
                                                                profilePhotoUploadFunction();
							}},
							{text: "<?php echo $this->lang->line('import_from_facebook');?>", 
							"class": 'btn-custom-blue_fb w200px2',
							click: function() {
								$(this).dialog("close");
								setTimeout(function() {
									$('#messageDialog').dialog('destroy').remove();
								}, 500 );						
								importFromFacebook();
							}},
                            {text: "Import from LinkedIn <?php //Add language file; ?>",
                                "class": 'btn-custom-blue_li w200px2',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);
                                    
                                    var log_details = {
                                        type: 'linkedin_api',
                                        event: 'Import profile picture from linkedIn',
                                        log_message: 'Grabbing member profile picture',
                                        url: window.location.href,
                                        response: ''
                                    };

                                    var logs = [];
                                    logs.push(JSON.stringify(log_details));
                                    /*save_system_log(logs);*/
                                    
                                    /*Import pic only*/
                                    linkLinkedInAccount(true);
                                }},
							{text: "<?php echo $this->lang->line('Cancel');?>", 
							"class": 'btn-custom-ash_xs w200px2',
							click: function() {
								$(this).dialog("close");
								setTimeout(function() {
									$('#messageDialog').dialog('destroy').remove();
								}, 500 );					
							}}
						]
					});
					NewDialog.dialog('open');
							
				 });
			
			});
			$(document).mouseup(function(e)
			{
				var container = $(".hidden_menu_class");
				if (!container.is(e.target) /* if the target of the click isn't the container...*/
						&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
				{
					$('#hidden_menu').hide();
				}
			});
		</script>
        
        <script>
            
            $('#error_close_btn').click(function(e){
                e.preventDefault();
                $('#error_container').addClass('hide');
            });
            
            $('#register_btn').click(function(e) {
                e.preventDefault();
                var form = $('#register_form');

                $.ajax({
                    type: "post",
                    url: $(form).attr('action'),
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(response) {
                        if (!response.success) {
                            process_error(response);
                            return;
                        }

                        window.location = response.location;
                    }
                });
            });

            function process_error(response) {
                $('#errors_inner').html('');
                for (var e in response.errors) {
                    if (response.errors[e] != '') {
                        //$('#error_container').removeClass('hide');
                        $('#errors_inner').append(response.errors[e]);
                        $('#' + e).addClass('error_border');
                    }

                    /*Remove any error message if theres any*/
                    if (response.errors[e] == '') {
                        $('#' + e).removeClass('error_border');
                    }
                }
            }
			
			
			
			fbApiInit=false;
window.fbAsyncInit = function() {
  FB.init({
	appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
	status     : true, /* check login status*/
	cookie     : true, /* enable cookies to allow the server to access the session*/
	xfbml      : true  /* parse XFBML	  		*/
});
	fbApiInit=true;
        $(document).trigger("facebook:ready");
};
/* Load the SDK asynchronously*/
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document)); 
	$().ready(function() {
		/* validate signup form on keyup and submit*/
         <?php
        if (validation_errors() != "" || isset($error_msg) || isset($uploadEerror)) 
        {
            echo 'var NewDialog =$("#register_error_from_load");
            NewDialog.dialog({						
                autoOpen: false,
                closeOnEscape: false ,
                resizable: false,					
                dialogClass:"dialog_style_glomp_wait noTitleStuff",
                modal: true,
                title:"",
                position: "center",
                width:280,                                
                buttons:[
                    {text: "Close",
                    "class": "btn-custom-white_xs",
                    click: function() {
                        $(this).dialog("close");
                        setTimeout(function() {
                            $("#waitPopup").dialog("destroy").remove();
                        }, 500);

                    }}
                ]
                
            });
            NewDialog.dialog("open");';
        }
        ?>
		
		$("#Link_Facebook").click(function(e){
                        var log_details = {
                            type: 'fb_api',
                            event: 'FB click button "link to facebook"',
                            log_message: 'Connecting to facebook',
                            url: window.location.href,
                            response: ''
                        };
                        var logs = [];
                        logs.push(JSON.stringify(log_details));
                        /*save_system_log(logs);*/
                    
			if(fbApiInit)
			{}
			else{
				FB.init({
					appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
					status     : true, /* check login status*/
					cookie     : true, /* enable cookies to allow the server to access the session*/
					xfbml      : true  /* parse XFBML	  		*/
				});
				fbApiInit=true;
			}
			if(facebook_alert_error())
            {	
                FB.login
                (
                    function( response )
                    {
                        if (response.status === 'connected') {
                                                var log_details = {
                                                    type: 'fb_api',
                                                    event: 'FB click button "link to facebook"',
                                                    log_message: 'Connected to facebook',
                                                    url: window.location.href,
                                                    response: JSON.stringify(response)
                                                };
                                                var logs = [];
                                                logs.push(JSON.stringify(log_details));
                                                /*save_system_log(logs);*/
                                                
                                                getUserDetailsAndConfirm();
                        }
                    }, {scope: 'publish_actions,email'}
                );
            }
		});
        setTimeout(function() {
            <?php            
                if ( isset($_GET['import_from']) && $_GET['import_from']=='facebook')
                {
                    echo "importFromFacebook();";
                }
                if ( isset($_GET['import_from']) && $_GET['import_from']=='linkedin')
                {
                    echo "linkLinkedInAccount(true);";
                }
            
                
            ?>
                        
        }, 500);
	});
    
        function linkedin_api(obj) {
            var data;
            obj.mail = obj.mail || '';
            obj.body = obj.body || '';

            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: GLOMP_BASE_URL + 'ajax_post/linkedIn_callback',
                data: {
                    redirect_li: window.location.href,
                    js_func_li: obj.js_func_li,
                    api: obj.api,
                    mail: obj.mail,
                    body: obj.body,
                },
                success: function(_data) {
                    if (_data.redirect_window != '') {
                        window.location = _data.redirect_window;
                        data = false;
                    } else {
                        data = _data;
                    }
                }
            });
            return data;
        }    
    
    function linkLinkedInAccount(importOnly, auth) {
        var importOnly = false || importOnly;
        var auth = false || auth;
        
        if ( ! auth) {
            /*IN.User.authorize(function() { */
                return linkLinkedInAccount(importOnly, true);
            /*});*/
            /*return;*/
        }
        
        params = {
            js_func_li: 'linkLinkedInAccount('+importOnly+ ','+ auth+')',
            api: '/v1/people/~:(id,email-address,picture-urls::(original))'
        };
        
        var data = linkedin_api(params);
        
        if (data != false) {
            var log_details = {
                type: 'linkedin_api',
                event: 'Link LinkedIn account',
                log_message: 'Grabbing linkedIn information',
                url: window.location.href,
                response: JSON.stringify(data)
            };

            var logs = [];
            logs.push(JSON.stringify(log_details));
            /*save_system_log(logs);*/
        } else {
            return;
        }
        
        /*If authorized and logged in*/
        /*IN.API.Profile("me").fields("id", "email-address", "picture-urls::(original)").result(function(data) {*/
            params = {
                id: data.values[0].id,
                email: data.values[0].emailAddress,
                pic: data.values[0].pictureUrls.values[0],
                importOnly: '',
            };
            
            if (importOnly) {
                params.importOnly = 'yes';
            }
            
            var NewDialog = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
			NewDialog.dialog({						
				autoOpen: false,
				closeOnEscape: false ,
				resizable: false,					
				dialogClass:'dialog_style_glomp_wait noTitleStuff',
				title:'Please wait...',
				modal: true,
				position: 'center',
				width:200,
				height:120
			});
            NewDialog.dialog('open');
            
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: GLOMP_BASE_URL + 'ajax_post/linkThisLinkedInAccount',
                data: params,
                success: function(response) {
                    $('#waitPopup').dialog("close");
                    $('#waitPopup').dialog('destroy').remove();

                    if (response.status == "Ok")
                    {
                        if (response.data == 'importOnly') {
                            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                <p style="padding:15px 0px;">' + response.message + '</p> \
                                            </div>');
                            NewDialog.dialog({
                                autoOpen: false,
                                resizable: false,
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                title: '',
                                modal: true,
                                width: 280,
                                height: 160,
                                show: 'clip',
                                hide: 'clip',
                                buttons: [
                                    {text: "Ok",
                                        "class": 'btn-custom-ash_xs w80px',
                                        click: function() {
                                            $(this).dialog("close");
                                            window.location.href = document.URL.split("?")[0];
                                            setTimeout(function() {
                                                $('#messageDialog').dialog('destroy').remove();

                                            }, 500);
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                        else
                        {
                            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                                <p style="padding:15px 0px;">' + response.message + '</p> \
                                            </div>');
                            NewDialog.dialog({
                                autoOpen: false,
                                resizable: false,
                                dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                title: '',
                                modal: true,
                                width: 280,
                                height: 120,
                                show: 'clip',
                                hide: 'clip',
                                buttons: [
                                    {text: "Invite",
                                        "class": 'btn-custom-ash_xs w80px',
                                        click: function() {
                                            $(this).dialog("close");
                                            window.location.href = GLOMP_BASE_URL + 'm/user/linkedinFriends';
                                            setTimeout(function() {
                                                $('#messageDialog').dialog('destroy').remove();
                                            }, 500);
                                        }},
                                    {text: "Close",
                                        "class": 'btn-custom-ash_xs w80px',
                                        click: function() {
                                            $(this).dialog("close");
                                            window.location.href = document.URL.split("?")[0];
                                            setTimeout(function() {
                                                $('#messageDialog').dialog('destroy').remove();

                                            }, 500);
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }

                    }
                    else {
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:14px"> \
                                            <p style="padding:15px 0px;">' + response.message + '</p> \
                                        </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass:'dialog_style_glomp_wait noTitleStuff',
                            title: '',
                            modal: true,
                            width: 280,
                            height: 120,
                            show: 'clip',
                            hide: 'clip',
                            buttons: [
                                {text: "Close",
                                    "class": 'btn-custom-ash_xs w80px',
                                    click: function() {
                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#messageDialog').dialog('destroy').remove();
                                        }, 500);
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                }
            });	/*ajax*/                    
        /*});*/
    }
    <?php if(isset($_GET['js_func_li'])) {
        /*LinkedIn function REST on JS*/
        echo $_GET['js_func_li'].';';
        }
    ?>
	function getUserDetailsAndConfirm(){
	FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                var log_details = {
                    type: 'fb_api',
                    event: 'FB click button "link to facebook"',
                    log_message: 'Grabbing facebook logged user information',
                    url: window.location.href,
                    response: JSON.stringify(response)
                };
                var logs = [];
                logs.push(JSON.stringify(log_details));
                /*save_system_log(logs);*/
                
		var output = '';
		var data= '&importOnly='+importOnly;
		var ii=0;
		for (property in response) {
		  output += property + ': ' + response[property]+'; ';

			  data += '&'+property + '=' + (response[property])+'';

		}                                                        
		for (property in response.hometown) {
		  output += property.hometown + ': ' + response.hometown[property]+'; ';
		  data +='&'+property.hometown + '=' + response.hometown[property]+'';
		}                                                        		
		/*
        console.log(output);
		console.log(data);					
        post_to_url((document.URL),response,'post')*/
		/*console.log('Good to see you, ' + response.id + '::'+response.email );		*/
		 var NewDialog = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
			NewDialog.dialog({						
				autoOpen: false,
				closeOnEscape: false ,
				resizable: false,					
				dialogClass:'dialog_style_glomp_wait noTitleStuff',
				title:'Please wait...',
				modal: true,
				position: 'center',
				width:200,
				height:120
			});			
			NewDialog.dialog('open');
			$.ajax({
				type: "POST",				
				dataType:'json',
				url: GLOMP_BASE_URL+'ajax_post/linkThisFacebookAccount',
				data:data,
				success: function(response){					
					$('#waitPopup').dialog("close");
					$('#waitPopup').dialog('destroy').remove();
					
					if(response.status=="Ok")
					{
						if(response.data=='importOnly'){
							var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px"> \
													<p style="padding:15px 0px;">'+response.message+'</p> \
												</div>');
							NewDialog.dialog({						
								autoOpen: false,
								resizable: false,
								dialogClass: 'dialog_style_glomp_wait noTitleStuff',
								title:'',
								modal: true,
								width:280,
								height:160,				
								show: '',
								hide: '',
								buttons: [         													
									{text: "Ok", 
									"class": 'btn-custom-ash_xs w80px',
									click: function() {
										$(this).dialog("close");
										window.location.href=GLOMP_BASE_URL+'m/user/update';
										setTimeout(function() {
											$('#messageDialog').dialog('destroy').remove();
											
										}, 500 );						
									}}
								]
							});
							NewDialog.dialog('open');
						}
						else
						{
							var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px"> \
													<p style="padding:15px 0px;">'+response.message+'</p> \
												</div>');
							NewDialog.dialog({						
								autoOpen: false,
								resizable: false,
								dialogClass: 'dialog_style_glomp_wait noTitleStuff',
								title:'',
								modal: true,
								width:280,
								height:160,				
								show: '',
								hide: '',
								buttons: [         
									{text: "Invite", 
									"class": 'btn-custom-blue_fb w80px',
									click: function() {
										$(this).dialog("close");
										window.location.href=GLOMP_BASE_URL+'m/user/facebookFriends';
										setTimeout(function() {
											$('#messageDialog').dialog('destroy').remove();
										}, 500 );						
									}},       							
									{text: "Close", 
									"class": 'btn-custom-ash_xs w80px',
									click: function() {
										$(this).dialog("close");
										window.location.href=document.URL;
										setTimeout(function() {
											$('#messageDialog').dialog('destroy').remove();
											
										}, 500 );						
									}}
								]
							});
							NewDialog.dialog('open');
						}
					
					}
					else{
						var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px"> \
												<p style="padding:15px 0px;">'+response.message+'</p> \
											</div>');
						NewDialog.dialog({						
							autoOpen: false,
							resizable: false,
							dialogClass: 'dialog_style_glomp_wait noTitleStuff',
							title:'',
							modal: true,
							width:280,
							height:160,				
							show: '',
							hide: '',
							buttons: [                															
								{text: "Close", 
								"class": 'btn-custom-ash_xs w80px',
								click: function() {
									$(this).dialog("close");
									setTimeout(function() {
										$('#messageDialog').dialog('destroy').remove();
									}, 500 );						
								}}
							]
						});
						NewDialog.dialog('open');
					}
					
				}
			});	/*ajax*/
	});
}
/* for profile picture*/
	$(function(){
    profilePic = {
        UpdatePreview: function(obj){
         
          if(!window.FileReader){
             
          } else {
             var reader = new FileReader();
             var target = null;
			 
			 
			
             reader.onload = function(e) {
              target =  e.target || e.srcElement;
			 var  upload_string = target.result;
			 var n=upload_string.search(':image/'); 
			 /*find for :image/*/
			 if(n>=0)
			 {
               $("#profile_pic").prop("src", target.result);
			 }else
			 {
				alert("invalid_image_extension");
			}
			  $('#upload_loader').html('');
             };
              reader.readAsDataURL(obj.files[0]);
          }
        }
    };
});
function linkFbAccount(){

}
function profilePhotoUploadFunction()
{
    <?php
        if($this->session->userdata('from_android')=='true')
        {
            echo "window.location.href =GLOMP_BASE_URL+'m/user/update/uploadPhotoFromDevice?".$user_record->user_id."';";
        }
        else
        {
            echo "$('#profilePhotoUpload').click();";
        }
    ?>
    
}
function importFromFacebook(){
	importOnly="yes";
	if(fbApiInit)
			{}
			else{
				FB.init({
					appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
					status     : true, /* check login status*/
					cookie     : true, /* enable cookies to allow the server to access the session*/
					xfbml      : true  /* parse XFBML	  		*/
				});
				fbApiInit=true;
			}
			FB.getLoginStatus(function (response) {
				if(response.status=="connected")
				{
                                    FB.api('/me/permissions', function(response) {
                                        var perms  = response.data;
                                        for (i = 0; i < perms.length; i++) { 
                                            if (perms[i].permission == 'email' && perms[i].status != 'granted') {
                                                var return_url = GLOMP_BASE_URL +  'm/user/update?js_func_fb=importFromFacebook()';
                                                return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=email");     
                                            }
                                        }
                                    });
                                    getUserDetailsAndConfirm();
				}
				else
				{
                                    if(facebook_alert_error())
                                    {
                                        var return_url = GLOMP_BASE_URL +  'm/user/update?js_func_fb=importFromFacebook()';
                                        return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=email");
                                    }
				}
			});
}
        $(document).on("facebook:ready", function() {
            <?php if(isset($_GET['js_func_fb']) && ! isset($_GET['error']))   { ?>
                <?php echo urldecode($_GET['js_func_fb']).';'; ?>
            <?php } ?>
        });
        
        </script>        
        
    </body>
</html>
