<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?> | glomp! mobile</title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Frequently Asked Questions. Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
    <meta name="keywords" content="<?php echo meta_keywords('faq');?>" >
    <meta name="author" content="<?php echo meta_author();?>" >
    <!-- Bootstrap -->
    <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	</style>
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>	
  </head>
  <body style="background-color:#FFFFFF; border-left: #B0B0B0  solid 1px;border-right: #B0B0B0 solid 1px;">
	<?php include_once("includes/analyticstracking.php") ?>	
	<div class="global_wrapper" align="center">		      		
        <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
        <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
            <div class="header_navigation_wrapper fl" align="center">				
                <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                    <nav>
                        <a href="#" id="menu-icon-nav"></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                    <div class="w100per fl white">
                                    <?php echo $this->lang->line('Home'); ?>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>	
                <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url()?>assets/m/img/faq_icon.png');"></div>
            </div>
            
             <!-- hidden navigations       
            <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav" align="left">
                        <?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>
            </div>
            <!-- hidden navigations -->	
        </div>		
        <div class="p20px_0px" style="margin-top:12px;"></div>	
		<!--body -->
		<div class="container p20px_0px global_margin">
			<a name="top"></a> 
                <?php if( $this->session->userdata('user_id') != false  ):?>
				<div class="row "> 
					<a  href="javascript:void(0);" id="start_tour" class="fl btn-lg btn-custom-blue-grey  border_radius_4px" type="submit"><?php echo $this->lang->line(''); ?>Tour</a>
					<a href="<?php echo base_url(MOBILE_M.'/landing/support')?>" class="fr btn-lg btn-custom-blue-grey  border_radius_4px" type="submit"><?php echo $this->lang->line('Support'); ?></a>
				</div>
                <?php else: ?>
                <div class="row ">
                    <a  href="javascript:history.go(-1);" class="fr btn-lg btn-custom-blue-grey  border_radius_4px"><?php echo $this->lang->line('back','Back'); ?></a>
                </div>
                <?php endif;?>
				<div class="cl p20px_0px " style="text-align:justify;color:#585F6B;font-size:13px">
					<?php echo stripslashes($res_privacy->content_content); ?>
					<a href="#top"	>back to the top</a>
				</div>					
		</div>
		<!--body -->	
		<div class="footer_wrapper"> <!-- /footer --></div> <!-- /footer -->
	</div> <!-- /global_wrapper -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/m/js/bootstrap.min.js"></script>	
    <script>
		$(document).ready(function(e) {
			$('#start_tour').on('click',function(e){				
				var stat='Y';
				data='&tour_stat='+stat;
				$.ajax({
					type: "POST",	
					dataType:'json',							
					url: '<?php echo base_url();?>ajax_post/setTourStat',
					data:data,
					success: function(response){				
						window.location.href='<?php echo base_url();?>m/user/dashboard';
							
					}
				});
			});				 
		});
	

       $(function() {	
	   $("#main_menu").click(function() {
				$("#hidden_menu").toggle();										
			});
		
		});
		$(document).mouseup(function(e)
		{
			var container = $(".hidden_menu_class");
			if (!container.is(e.target) /* if the target of the click isn't the container...*/
					&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
			{
				$('#hidden_menu').hide();
			}
		});
	</script>   
  </body>
</html>
