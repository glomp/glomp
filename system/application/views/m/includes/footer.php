    </div>
</div>


<!-- Modal -->
<div id="termModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $this->lang->line('termNcondition'); ?></h3>
    </div>
    <div class="modal-body">
        <?php
        $res_privacy = $this->cmspages_m->selectStaticPage(2, DEFAULT_LANG_ID);
        if ($res_privacy->num_rows() > 0) {
            $rec_privacy = $res_privacy->row();
            echo $rec_privacy->content_content;
        }
        ?>


    </div>
    <div class="modal-footer">
        <button class="btn-custom-primary" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('close'); ?></button>

    </div>
</div>
</body>
</html>