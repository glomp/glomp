<div class="widget-photostory <?php echo $widget->data->bg_color?> <?php echo $widget->data->type?>">
     <div class="widget-title" style="">
		<?php 
			if($widget->data->brand_product_button_id !=0)
			{
		?>	
		
			<a href="<?php echo site_url(MOBILE_M);?>/brands/view/<?php echo $brand->public_alias;?>/<?php echo $widget->data->brand_product_button_id;?>" style="float:right;margin:8px 0px;" class="btn-custom-transparent_white">Products</a>
		
			<div style="float:left;border: solid 0px">
		<?php
			}
		?>
			
				<h3 class="widget-title-head"><?php echo $widget->data->title;?></h3>
				<p class="widget-title-sub"><?php echo $widget->data->subtitle;?></p>
		<?php 
			if($widget->data->brand_product_button_id !=0)
			{
		?>
			</div>
			<div style="clear:both"></div>
		<?php
			}
		?>
		
    </div>
    <div class="widget-photostory-body">
        <div class="widget-photostory-image">
            <img src="<?php echo $widget->data->image;?>" width="100%" />
        </div>
        <div class="widget-photostory-context">
            <div><?php echo $widget->data->context;?></div>
        </div>
    </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
        var hold_msg = $('<div style="padding-top: 15px;font-size: 14px;">Please check back shortly for great products on offer.</div>');
        hold_msg.dialog({
            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
            title: '',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            height: 120,
            show: '',
            hide: '',
            buttons: [
                {
                    text: "Close",
                    "class": 'btn-custom-blue_xs w80px',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        });
        
        
        $('.hold_on').unbind('click').click(function(e){
           e.preventDefault();
           hold_msg.dialog('open');
        });
	});
</script>
