<style type="text/css">
	.fixed {
  position: fixed !important;
  top:0; left:0;
  width: 100%; }
</style>
<script type="text/javascript">
if (typeof jQuery == 'undefined') {
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
}
</script>
<?php include_once((_VIEW_INCLUDES_DIR_)."/analyticstracking.php") ?>

<?php
if (isset($is_landing))
{

}
else
{
 	$show_ad=true;
    // Safari (in-app)
    $isMobileSafari = (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari/') == false);
    if ($isMobileSafari) {
        //echo '<hr>Safari (in-app)';
        $show_ad =false;
    }
    // Android (in-app)
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "it.glomp")
    { 
        //echo 'Android (in-app)';
        $show_ad =false;
    }


    if($show_ad)
    {
	
        if ($this->agent->is_mobile('iphone'))
        {
            $is_iphone =  true;
        }
        else
        {
            $is_iphone =  false;
        }
	?>
		<div id="app_alert_wrap" style="background-color: #DCDCDC; color:#686868;padding: 10px;" align="left">
			<p style="font-size: 12px">For a better experience, download the <b>glomp!</b> app now</p>
			<table width="100%">
				<tr>
					<td width="70">
						<a href="<?php echo ($is_iphone==true) ? 'https://itunes.apple.com/us/app/glomp!/id823405266?mt=8':'https://play.google.com/store/apps/details?id=it.glomp';?>" target="_blank">
							<img  style="width:60px;border-radius: 20%;" src="<?php echo base_url('assets/m/img/glomp_logo_121.png');?>" />			
						</a>
					</td>
					<td width="">
						<b>glomp!</b> <?php echo ($is_iphone==true) ? 'iOS':'Android';?> App
					</td>
					<td width="30" valign="bottom" align="right">
						<b id="app_alert_close" style="color:#2FA6E8;font-size: 16px;font-weight: bolder;cursor: pointer;">X</b>
					</td>
				</tr>
			</table>
			
		</div>
		<div class="cl"></div>
		<script type="text/javascript">
			$("#app_alert_close").click(function() {
                $("#app_alert_wrap").hide();
                $('.navbar_relative').css('position','fixed');

                if ( $( ".activity_wrapper" ).length )
                {
                	$('.activity_wrapper').css('position','fixed').css('top','51px');
                	$('.activity_log_content').css('margin-top','101px');	
                }

                if ( $( ".user_fb_invite" ).length )
                	$('.user_fb_invite').css('position','fixed');

            	if ( $( ".menu_header_wrapper" ).length )
                {
                	$('.menu_header_wrapper').css('margin-top','52px');
                }
            });
		</script>
	<?php
	}
	else
	{
		?>
		<script type="text/javascript">
			$(document).ready(function(e)
			{
                $('.navbar_relative').css('position','fixed');

                if ( $( ".activity_wrapper" ).length )
                {
                	$('.activity_wrapper').css('position','fixed').css('top','51px');
                	$('.activity_log_content').css('margin-top','101px');	
                }

                if ( $( ".user_fb_invite" ).length )
                	$('.user_fb_invite').css('position','fixed');

            	if ( $( ".menu_header_wrapper" ).length )
                {
                	$('.menu_header_wrapper').css('margin-top','52px');
                }
            });
		</script>
		<?php
	}
}
?>
<script type="text/javascript">

var stickyOffset="a";
$(document).ready(function(e)
{
	if ( $( ".navbar_relative" ).length )
		stickyOffset = $('.navbar_relative').offset().top;
});
$(document).scroll(function(){
	if(stickyOffset!="a" && $("#app_alert_wrap").css('display') != 'none')
	{
	  	var scroll = $(window).scrollTop();
	  	if (scroll >= stickyOffset) 
	  	{
	  		$('.navbar_relative').css('position','fixed');

	  		if ( $( ".activity_wrapper" ).length )
	  			$('.activity_wrapper').css('position','fixed').css('top','51px');

	  		if ( $( ".user_fb_invite" ).length )
	  			$('.user_fb_invite').css('position','fixed');
	  	}
	 	else 
	 	{
		 	$('.navbar_relative').css('position','relative');


		 	if ( $( ".activity_wrapper" ).length )
	  			$('.activity_wrapper').css('position','relative').css('top','0');
  			if ( $( ".user_fb_invite" ).length )
	  			$('.user_fb_invite').css('position','relative');
	 	}
	 	
  		
  	}
  	
	});
</script>
