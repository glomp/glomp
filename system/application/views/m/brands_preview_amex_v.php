<?php
	$user_id = $this->session->userdata('user_id');


	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	if($inTour=='yes'){
		$inTour_display='display:none;';
		$inTour_header_display='';
		$inTour_header='Add favourites';
		$inTour_header_top_position='0';
		$inTour_header_top_margin='100';
		$inTour_header_top_position_2='48';
		$inTour_display_footer='';
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo ucfirst($brand->public_alias);?> | glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                $public_alias_address  =$brand->public_alias;
                                if($brand->public_alias=='hsbc' || $brand->public_alias =='uob' || $brand->public_alias =='dbs')
                                    $public_alias_address  .='sg';
                                else if($brand->public_alias =='amex')
                                    $public_alias_address  .='-sg';
                                ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/'.$public_alias_address) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo ucfirst($brand->public_alias);?> <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <?php
                                     if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                                     { ?>

                                        <li>
                                            <a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$user_record->user_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>" class="white ">
                                                <div class="w100per fl white">
                                                <?php echo $this->lang->line('edit_favourite'); ?>
                                                </div>
                                            </a>
                                        </li>

                                    <?php } ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
								<?php
									if(
										$this->session->userdata('user_id') =='' &&
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url() ?>brands/logout/<?php echo $public_alias_address;?>/m" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('exit','Exit'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >
                    <?php if (isset($_GET['mDetail'])) { ?>
                    <a id = "back_btn" class=" cl hidden_nav_link fl w200px" href="#">
                        <div class="hidden_nav">
                            Back<?php //echo $this->lang->line('Back'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <?php } ?>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/profile') ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('profile'); ?>
                        </div>
                    </a>
                    <?php
                     if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                     { ?>
                        <div class="cl hidden_nav_seperator fl w200px"></div>
                        <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$user_record->user_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>">
                            <div class="hidden_nav">
                                <?php echo $this->lang->line('edit_favourite'); ?>
                            </div>
                        </a>
                    <?php } ?>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Log_Out'); ?>
                        </div>
                    </a>
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>


            
            
            


<style type="text/css">
    .brand-page-header,
    .brand-page-logo,
    .brand-page-description {
        position: relative;
    }
    .brand-page-title {
        position:absolute;
        top:10%;
        right:0;
        background: #5A606C;
        -webkit-border-top-left-radius: 10px;
        -webkit-border-bottom-left-radius: 10px;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-bottomleft: 10px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        padding:10px 40px 10px 15px;
    }
    .brand-page-title h1 {
        margin:0;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
    }
    .brand-page-logo {
        padding: 0 20px;
        max-width: 100%;
    }
    .brand-page-logo img {
        width:100%;
    }
    .brand-page-description {
        padding: 0 20px;
        text-align: justify;
    }
    .brand-page-products-link {
        padding: 0 20px;
    }
    .brand-page-products-link a {
        background: #5A606C;
        padding:8px 20px;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
        display: inline-block;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    .brand-page-products-link a:active {
        background: #777;
        text-decoration: none;
    }
    .brand-page .page-line {
        margin-top:20px;
        position: relative;
    }
    .brand-section {
        margin-bottom:2px;
    }

    .widget-photostory h3,
    .widget-photostory p {
        margin:0;
    }
    .widget-photostory h3 {
        font-size:1.4em
    }
    .widget-photostory p {
        font-size:0.8em;
        font-weight: bold;
        line-height: 1em;
    }
    .widget-photostory .widget-title,
    .widget-photostory .widget-photostory-context {
        padding:5px 10px 4px;
        color: #333;
    }
    .widget-photostory .widget-title {
        color: #eee;
        background: #5A606C;
    }
    .widget-photostory .widget-photostory-context {
        color: #333;
        background: #fff;
        padding: 5px 20px 2px;
    }
    .widget-photostory.grey .widget-title {
        color: #eee;
        background: #5A606C;
    }
    .widget-photostory.grey .widget-photostory-context {
        color: #333;
        background: #fff;
        padding: 10px 20px 5px;
    }
    .tri {
        width : 0;
        height : 0;
        border-left : 15px solid transparent;
        border-right : 15px solid transparent;
        border-top : 15px solid #AACA38;
        position : relative;
    }
    .tri .tri2 {
        width : 0;
        height : 0;
        border-left : 13px solid transparent;
        border-right : 13px solid transparent;
        border-top : 13px solid #5A606C;
        position : absolute;
        top : -15px;
        left : -13px;
    }
    .tri.up {
        border-bottom : 15px solid #AACA38;
        border-top : 0;
    }
    .tri.up .tri2 {
        border-bottom : 13px solid #5A606C;
        border-top : 0;
        top : 2px;
    }

    .product-list:after {
        content: " ";
        display:block;
        clear: left;
    }
    .product-list a.product {
        width:47%;
        margin-left: 2%;
        float: left;
        background: #ddd;
        margin-bottom: 2%;
    }
    .product-list a.product img {
        width:100%;
        background: #222;
        border:0;
    }
    .product-list a.product p {
        margin:0;
        padding:5px 10px;
        color: #555;
    }
    div.product {
    }
    div.product .product-image {
        text-align: center;
    }
    div.product .product-image img {
        width: 96%;
    }
    div.product .product-info {
        padding: 0 20px;
    }
    div.product .product-info h3 {
        margin:0;
        padding: 0.6em 0;
        font-size:1.4em;
    }
    div.product-glomp {
        padding: 20px;
    }
    div.product-glomp a {
        display:block;
        background: #A8CF3E;
        padding: 5px;
        text-align:center;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
    }
    div.product-glomp a img {
        height:2em;
    }
    .btn-back {
        position:absolute;
        top:35%;
        right:1em;
        padding:5px 20px;
        background:#5A606C;
        color:#eee;
        font-weight:bold;
        font-size: 1.2em;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
    }
    div.product-merchants {
    
    }
    div.product-merchants a {
        float: left;
        width: 24%;
        margin-left: 4%;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
        -webkit-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        -moz-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        overflow: hidden;
    }
    div.merchants-list {
        overflow: auto;
    }
    div.product-merchants a img {
        width: 100%;
    }
    div.search-info {
        margin-bottom: 20px;
    }
    div.search-info h4 {
        font-size: 0.9em;
        text-transform: uppercase;
        margin:0;
        padding: 5px;
        background: #5A606C;
        color: #eee;
    }
    div.search-info .search-data {
        padding:5px;
        background: #bfc4ce;
    }
    div.search-info .search-data:after {
        content: "";
        display:block;
        clear:left;
    }
    div.search-info .search-data img {
        float:left;
        width: 25%;
    }
    div.search-info .search-data p {
        float:left;
        margin-left:5px;
    }
</style>
            <div class="page brand-page">
                
                
                <!-- Shows up on every page under brands -->
                <div class="page-line default-header">
                    <div class="brand-page-header">
                        <!--<div class="brand-page-title"><h1><?php echo $brand->name;?></h1></div>-->
                        <div class="brand-page-banner"><img src="/custom/uploads/brands/<?php echo $brand->banner;?>" width="100%" /></div>
                    </div>
                </div>
                <div class="page-line default-header">
                    <?php if(isset($products)):?>
					
                    <div class="brand-page-logo" style="width:70%">
                        <img src="/custom/uploads/brands/<?php echo $brand->logo;?>" width="100%" />
                    </div>
                    <a href="#" class="btn-back" onclick="window.history.back(); return false;">Back</a>
                    <?php else:?>
                    <div class="brand-page-logo">
                        <img src="/custom/uploads/brands/<?php echo $brand->logo;?>" width="100%" />
                    </div>
                    <?php endif;?>
                </div>

                <?php if(!isset($active_brandproduct)):?>

                    <?php if(isset($brand->description) && !isset($products)):?>
                    <div class="page-line">
                        <div class="brand-page-description">
                            <?php echo $brand->description?>
                        </div>
                    </div>
                    <?php endif;?>

                    <!--
					<?php if(!isset($products)):?>
                    <div class="page-line">
                        <div class="brand-page-products-link">
                            <a href="/m/brands/preview/<?php echo $brand->id;?>/products"><?php echo $this->lang->line('products', 'Products'); ?></a>
                        </div>
                    </div>
                    <?php endif;?>
					
					-->

                    <?php if(isset($sections) && !isset($products)):?>
                    <div class="page-line">
                        <div class="page-sections">
                            <?php if($sections):?>
                            <?php foreach($sections as $section):?>
                            <div class="brand-section">
                                <div class="section-type-<?php echo strtolower($section->type);?> section-row">
                                    <?php if($widgets && array_key_exists($section->id,$widgets)):?>
                                    <?php foreach($widgets[$section->id] as $widget):?>
                                    <div class="section-col">
                                        <?php include('includes/widget_type_'.$widget->widget_type.'.php'); ?>
                                    </div>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <?php endforeach;?>
                            <?php endif;?>
                        </div>
                        <script type="text/javascript">
                            jQuery('.widget-photostory-context').removeAttr('style');
                        </script>
                    </div>
                    <?php endif;?>

                    <?php if( isset($products) ):?>
                    <div class="page-line">&nbsp;</div>
                    <div class="page-line product-data">

                        <?php if(is_array($products) && count($products) > 0):?>
                        <div class="product-list">
                            <?php foreach($products as $p):?>
                            <?php if($is_preview):?>
                            <a class="product" href="javascript:void(0)">
                                <img src="/custom/uploads/brands/products/<?php echo $p->image_logo?>" />
                                <p><?php echo $p->name?></p>
                            </a>
                            <?php else:?>
                            <a class="product" href="/m/profile/menu/brands/<?php echo $user_record->user_id?>/<?php echo $brand->id?>/products/<?php echo $p->id;?>">
                                <img src="/custom/uploads/brands/products/<?php echo $p->image_logo?>" />
                                <p><?php echo $p->name?></p>
                            </a>
                            <?php endif;?>
                            <?php endforeach;?>
                        </div>
                        <?php else:?>
                        <?php echo $this->lang->line('no_brandproducts', 'Product is not available at present.');?>
                        <?php endif;?>


                    </div>
                    <?php endif;?>

                <?php endif;?>
                    
                
                
                <?php if(isset($active_brandproduct)):?>
                    
                    <div class="page-line product">
                        <div class="product-image">
                            <img src="/custom/uploads/brands/products/<?php echo (isset($active_brandproduct->image_logo))?$active_brandproduct->image_logo:''?>" />
                        </div>
                        <div class="product-info">
                            <h3><?php echo (isset($active_brandproduct->name)) ? $active_brandproduct->name : '';?></h3>
                            <div><?php echo (isset($active_brandproduct->description)) ? $active_brandproduct->description : '';?></div>
                            <div class="page-line">
                                <a href="#" class="toggle tgl-glomp" toggle-in=".active-brandproduct,.merchant-products" toggle-out=".page-line.product,.default-header">
                                    <span>glomp!</span>
                                </a>
                            </div>
                            <div class="page-line">&nbsp;</div>
                        </div>
                    </div>
                
                
                    <div class="page-line row active-brandproduct" style="display:none;">
                        <h4 class="row">
                            <span class="fl"><?php echo $this->lang->line('product_search','Product Search');?></span>
                            <span class="fr">
                                <a class="close toggle" href="#" toggle-in=".page-line.product,.default-header" toggle-out=".active-brandproduct,.merchant-products">&times;</a>
                            </span>
                        </h4>
                        
                        <div class="fl brandproduct-image">
                            <img src="/custom/uploads/brands/products/<?php echo $active_brandproduct->image_logo?>" />
                        </div>
                        <div class="fl brandproduct-detail">
                            <div class="brandproduct-name"><?php echo $active_brandproduct->name;?></div>
                            <div class="brand-name"><?php echo $brand->name;?></div>
                        </div>
                    </div>

                    <?php if(isset($merchant_products) && is_array($merchant_products)):?>
                    <div class="page-line cl body_bottom_1px merchant-products" style="background:#fff;display:none;">
                        
                        <?php foreach($merchant_products as $mp):?>
                        
                        <div class="container p5px_0px global_margin">
                            <div class="row">
                                <div class="fl menu_and_point_wrapper" style="width:70%">
                                    <div class="fl menu_item_photo_wrapper">
                                        <img class="menu_item_photo" src="/custom/uploads/products/thumb/<?php echo $mp->prod_image?>" />
                                    </div>
                                    <div class="fl menu_item_description_wrapper">
                                        <div class="menu_merchant"><?php echo $mp->merchant_name?></div>
                                        <div class="menu_product"><?php echo $mp->prod_name?></div>
                                    </div>
                                    <div class="fr menu_item_points_wrapper"><?php echo $mp->prod_point?><div style="font-size:0.8em; line-height:0.4em; font-weight: normal">pts</div></div>
                                </div>
                                <div class="fr">
                                    <button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper btn-glomp"
                                            id="<?php echo $mp->prod_id;?>" align="center"
                                            style="background-image:url('http://dev.glomp.it/assets/m/img/glomp_gray.png');"
                                            data-id="<?php echo $mp->prod_id;?>"
                                            data-merchant_id="<?php echo $mp->prod_merchant_id?>"
                                            data-merchant_name="<?php echo $mp->merchant_name?>"
                                            data-prod_image="<?php echo $mp->prod_image?>"
                                            data-prod_name="<?php echo $mp->prod_name?>"
                                            data-prod_point="<?php echo $mp->prod_point?>"></button>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>

                        <form method="post" style="" id="GlompFormDialog" action="/m/profile/glompToUser/<?php echo $user_record->user_id?>">
                            <input type="hidden" name="user_fname" id="user_fname" value="<?php echo $user_record->user_fname?>" />
                            <input type="hidden" name="user_lname" id="user_lname" value="<?php echo $user_record->user_lname?>" />
                            <input type="hidden" name="user_email" id="user_email" value="<?php echo $user_record->user_email?>" />
                            <input type="hidden" name="product_id" id="product_id" value="" />
                            <input type="hidden" name="merchant_id" id="merchant_id" value="" />
                            <div class="cl fl menu_item_photo_wrapper">
                                <img class="menu_item_photo_3" src="" width="100%" id="prod_image" />
                            </div>
                            <div class="fl menu_item_details">
                                <div class="menu_merchant white" id="menu_merchant"></div>
                                <div class="menu_product"><?php /*echo (isset($products->prod_name))?$products->prod_name:'';*/?></div>
                                <div class="fl menu_item_points_wrapper_3" align="center"><?php /*echo (isset($products->prod_point))?$products->prod_point:'';*/?></div>
                            </div>
                            <div class="cl">
                                <div class="">
                                    <div class="p8px_5px_0px_5px" align="left">
                                        <textarea name="message" id="glomp_message" style="height:40px;" class="glomp_input" placeholder="<?php echo $this->lang->line('add_a_message');?>" data-error="<?php echo $this->lang->line('no_glomp_message', 'Are you sure you dont want to include a message?');?>" data-allow-null="false"></textarea>
                                    </div>
                                    <div class="p2px_5px_0px_5px" align="left" style="margin-top: 6px;">
                                        <input name="password" id="glomp_password" type="password" class="glomp_input" placeholder="Password" data-error="<?php echo $this->lang->line('invalid_password', 'Invalid Password');?>" />
                                    </div>
                                    <div class="p8px_5px_0px_5px" align="center">
                                        <a href="<?php echo base_url(MOBILE_M).'/landing/forgotPassword/';?>" class="forgot_pass white">Forgot Password?</a>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div id="share-dialog" style="text-align:center">
                            <div class="response"></div>
                            <div class="content"><big>Would you like to share the news?</big></div>
                            <div class="options" style="margin-top:20px;">
                                <button id="option-share" class="glomp_fb_share btn-custom-blue-grey_xs w80px fl">Share</button>
                                <div class="shares" style="float:left">
                                    <button id="share-fb" class="share_on_facebook btn-custom-blue_xs_fb" style="background:url('/assets/frontend/img/fb_icon_mini.jpg'); background-size: 27px;background-position: center;"></button>
                                    <button id="share-li" class="share_on_linkedin btn-custom-light_blue_xs_tweet" style="background:url('/assets/frontend/img/li_icon_mini.jpg'); background-size: 27px;background-position: center;"></button>
                                    <button id="share-tw" class="share_on_twitter btn-custom-light_blue_xs_tweet" style="background:url('/assets/frontend/img/tweeter_icon_mini.jpg'); background-size: 27px;background-position: center;"></button>
                                </div>
                                <button id="option-close" class="btn-custom-white_xs w80px fr">Close</button>
                            </div>
                        </div>
                    <?php else:?>
                        <div class="page-line"><?php echo $this->lang->line('no_merchantproducts', 'Product is not available at present.');?></div>
                    <?php endif;?>
                
                <?php endif;?>
                
                </div>
            </div>


			<!--body-->

				<div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/m/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <div id="fb-root"></div>
        <script>
            var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
            <?php
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) {
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            
            var GlompFormDialog = jQuery('form#GlompFormDialog').dialog({
                autoOpen: false,
                dialogClass : 'dialog_style_glomp',
                width: '80%',
                modal: true,
                buttons: [
                    {
                        text: 'Confirm',
                        class: 'fl btn-custom-blue-glomp_xs w100px',
                        click: function() {
                            GlompFormDialog.submit();
                        },
                        style: 'margin:1px;'
                    },
                    {
                        text: 'Cancel',
                        class: 'fr btn-custom-ash_xs w100px',
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }
                ]
            });
            
            var GlompFormAlert = jQuery('<div class="form-alert" />').dialog({
                autoOpen: false,
                width: '70%',
                modal: true,
                dialogClass: 'dialog_style_glomped_alerts',
                minHeight: 'auto'
            });
            
            var GlompFormPreload = jQuery('<div class="form-preload" />').dialog({
                autoOpen: false,
                width: '60%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait',
                title: 'Please wait...',
                minHeight: 'auto'
            }).html('<div style="text-align:center"><img width="40" src="/assets/m/img/ajax-loader.gif" /></div>');
            
            var GlompShareDialog = jQuery('#share-dialog').dialog({
                autoOpen: false,
                width: '90%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                minHeight: 'auto'
            });
            
            $(function() {

                $('#back_btn').click(function(e) {
                   e.preventDefault();
                   window.history.back();
                });
                $("#tour_cancel").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
				$("#tour_done").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
				
			
                /* photostory widget */
				/*
                if( jQuery('.widget-photostory').length > 0 ) {
                    var toggle = jQuery('<a href="#" class="widget-photostory-text-toggle">[+]</a>');
                    toggle.css({'position':'absolute','top':'2px','right':'0','padding':'10px','color':'#eee'});
                    jQuery('.widget-photostory .widget-title')
                        .css({'position':'relative'})
                        .append(toggle);
                    jQuery('.widget-photostory .widget-photostory-context').hide();
                    jQuery('.widget-photostory-text-toggle')
                        .html('<div class="tri"><div class="tri2"></div></div>')
                        .click(function(e){
                        e.preventDefault();
                        jQuery(this)
                            .find('.tri')
                                .toggleClass('up')
                            .parents('.widget-photostory')
                            .find('.widget-photostory-context')
                                .slideToggle(100);
                    });
                }
                
                /* glomp */
                jQuery('.btn-glomp').on('click',function(e){
                    
                    GlompFormDialog.find('#product_id').val( jQuery(e.target).attr('data-id') );
                    GlompFormDialog.find('#merchant_id').val( jQuery(e.target).attr('data-merchant_id') );
                    GlompFormDialog.find('#prod_image').attr( 'src', '/custom/uploads/products/thumb/' + jQuery(e.target).attr('data-prod_image') );
                    GlompFormDialog.find('.menu_merchant').html( jQuery(e.target).attr('data-merchant_name') );
                    GlompFormDialog.find('.menu_product').html( jQuery(e.target).attr('data-prod_name') );
                    GlompFormDialog.find('.menu_item_points_wrapper_3').html( String(jQuery(e.target).attr('data-prod_point')) );
                    
                    GlompFormDialog.dialog('open');
                });
                GlompFormDialog.on('submit', function(){
                    /* check/validate message */
                    if( jQuery('#glomp_message').attr('data-allow-null') == 'false' && jQuery('#glomp_message').val() == '' ) {
                        GlompFormAlert
                            .html( jQuery('#glomp_message').attr('data-error') )
                            .dialog('open')
                            .dialog('option','title','Confirm')
                            .dialog('option','buttons',{
                                'Yes' : function() {
                                    jQuery('#glomp_message').attr('data-allow-null','true');
                                    jQuery(this).dialog('close');
                                },
                                'Cancel' : function() {
                                    jQuery(this).dialog('close');
                                }
                            })
                            .parents('.ui-dialog').find('.ui-button:not(.ui-dialog-titlebar-close)').addClass('btn-custom-blue-grey_xs w80px');
                        return false;
                    }
                    /* check/validate password */
                    if( jQuery('#glomp_password').val() == '' ) {
                        GlompFormAlert
                            .html( jQuery('#glomp_password').attr('data-error') )
                            .dialog('open')
                            .dialog('option','title','Error!')
                            .dialog('option','buttons',{
                                'Ok' : function() {
                                    jQuery(this).dialog('close');
                                }
                            })
                            .parents('.ui-dialog').find('.ui-button:not(.ui-dialog-titlebar-close)').addClass('btn-custom-blue-grey_xs w80px');
                        return false;
                    }
                    
                    
                    jQuery(document).trigger('doGlomp');
                    return false;
                }); 
                jQuery(document).on('doGlomp', function(){
                    
                    GlompFormPreload.dialog('open');
                    
                    jQuery.ajax({
                        type: 'post',
                        cache: false,
                        url: GlompFormDialog.attr('action'),
                        data: GlompFormDialog.serialize(),
                        success: function(response) {
                            var response = eval("("+response+")");
                            if( response.status == 'success' ) {
                                GlompFormDialog.dialog('close');
                                
                                tryAutoShare(response);
                                
                                GlompShareDialog
                                    .dialog('open')
                                    .find('.response').html(response.msg);
                                
                            } else if(response.status=='error') {
                                $('#glomp_password').val('');
                                GlompFormAlert
                                    .html(response.msg)
                                    .dialog('open')
                                    .dialog('option','title','Error!')
                                    .dialog('option','buttons',{
                                        'Ok' : function() {
                                            GlompFormAlert.dialog('close');
                                        }
                                    })
                                    .parents('.ui-dialog').find('.ui-button:not(.ui-dialog-titlebar-close)').addClass('btn-custom-blue-grey_xs w80px');

                            }
                            
                            GlompFormPreload.dialog('close');
                        }
                    });
                });
                
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});

            jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
                jQuery('.user-menu .user-icon .user-details').toggle();
            });
            
            jQuery(document).on('click','.toggle',function(e){
                e.preventDefault();
                toggleIn = jQuery(e.target).attr("toggle-in");
                toggleOut = jQuery(e.target).attr("toggle-out");
                
                jQuery(toggleIn).show();
                jQuery(toggleOut).hide();
            });
            
        $(document).ready(function() {    
            welcome_msg("<?php echo $public_alias;?>");
        });
        function welcome_msg(public_alias)
        {
            if(public_alias=='hsbcsg')
                var msg =  'Welcome to limited time exclusive offers for HSBC Premier, Infinite or Platinum Credit Card Members.';
            else if(public_alias=='uobsg')
                var msg =  'Welcome to limited time exclusive offers for UOB Card Members.';
            else if(public_alias=='dbssg')
                var msg =  'Welcome to limited time exclusive offers for DBS Card Members.';
            else
                var msg =  'Welcome to limited time exclusive offers for American Express® Platinum Reserve or Platinum Credit Card Members.';
            var NewDialog = $('<div id="glomp_this_to_email_dialog" align="center" style="font-size:14px;">\
                    '+msg+'\
            </div>');
            NewDialog.dialog({                    
                dialogClass:'noTitleStuff dialog_style_glomp_wait',
                title: "",
                autoOpen: false,
                resizable: false,
                modal: true,
                width: 300,
                show: '',
                hide: '',
                buttons: [{text: "OK",
                        "class": 'btn-custom-blue_xs w80px cancel_glomp_option',
                        click: function() {
                            $(this).dialog("close").dialog('destroy').remove();
                        }}
                ]
            });
            NewDialog.dialog('open');
        }
        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
</html>
