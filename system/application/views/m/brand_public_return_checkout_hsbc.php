<?php
	$user_id = $this->session->userdata('user_id');


	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	

?>
<!DOCTYPE html>
<html>
    <head>
        <title> Amex| glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
		<script type="text/javascript">
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var items = [];
			
			var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
			$(document).ready(function(e) {
				
			});
			function do_send_fb_confirm()
			{
				var NewDialog = $('<div id="" align="center" style="padding-top:5px !important; font-size:14px">\
				<p align="justify">If your friend is new to glomp!, we recommend that you send a personal message to inform them that you have glomp!ed (treated) them in order to ensure that they accept the treat.</p>\
								</div>');
				NewDialog.dialog({                    
					dialogClass:'noTitleStuff dialog_style_glomp_wait',
					title: "",
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 300,
					show: '',
					hide: '',
					buttons: [
							{text: "OK",
							"class": 'btn-custom-blue_xs w80px',
							click: function() {
								$(this).dialog("close");
								window.location ='http://www.facebook.com/dialog/send?app_id='+FB_APP_ID+'&display=page&to='+fb_id+'&link=<?php echo site_url('m/brands/details/'.$gl_voucher_brand_data->group_voucher_id);?>&redirect_uri=<?php echo site_url('m/brands/success_checkout/'.$gl_voucher_brand_data->group_voucher_id);?>&success=true';
								return;
								/*
								
								FB.ui({
								  to: fb_id,
								  method: 'send',
								  link: '',
								  
								},
								  function(response) {
									var msg ='Message successfully sent.';
									if (response && !response.error_code)
									{
									  
										
									} else {
									  msg = 'Message not sent. Refresh the page to retry.'
									}
									
									var NewDialog = $('<div id="" align="center" style="padding-top:20px !important; font-size:14px">\
										<p align="center">'+msg+'</p>\
														</div>');
										NewDialog.dialog({                    
											dialogClass:'noTitleStuff dialog_style_glomp_wait',
											title: "",
											autoOpen: false,
											resizable: false,
											modal: true,
											width: 250,
											height:120,
											show: '',
											hide: '',
											buttons: [
													{text: "OK",
													"class": 'btn-custom-blue_xs w80px',
													click: function() {
														$(this).dialog("close");
													}}
											]
										});
										NewDialog.dialog('open');
								  }
								);*/
							}}
					]
				});
				/*link: '<?php echo site_url('brands/success_checkout'.$gl_voucher_brand_data->group_voucher_id);?>'*/
				NewDialog.dialog('open');
				
			}
        </script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
								<?php
									if($this->session->userdata('public_user_since_user_logged_in') !='')
									{
								?>
										 <li>
											<a href="<?php echo base_url(MOBILE_M) ?>/brands/view/<?php echo $public_alias; ?> 	" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('Back','Back'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M). '/'. $public_alias. 'sg'; ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
								<?php
									if(
										$this->session->userdata('user_id') =='' &&
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url() ?>brands/logout/m" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('exit','Exit'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                    </a>
                </div>
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>

            <div class="page brand-page">
            	<?php if ( ! $data['success_payment']) { ?>
            	<div id = "wait_message" class="alert alert-success"><?php echo $this->lang->line('payment_wait', 'Please wait while we verify your payment.'); ?><img src="<?php echo base_url('assets/images/ajax-loader-1.gif'); ?>" /></div>
            	<script>
					/* Check payment record if completed */
					setInterval(function() {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: GLOMP_BASE_URL + 'brands/get_checkout/<?php echo $public_alias; ?>',
                            data: {
                        		token: '<?php echo $data['token']; ?>'
                            },
                            success: function(r){ 
                                if (r.success) {
                                	return window.location = GLOMP_BASE_URL + 'm/brands/get_checkout/<?php echo $public_alias; ?>/' + r.group_voucher_id
                                }
                            }
                        });
                    }, 3000);
				</script>


            	<?php } ?> 
            	<?php if ( $data['success_payment']) { ?>
            	<div style="padding:10px">
                	<h3><b>Congratulations!</b></h3>
            	</div>

                <div style="padding:10px">
					<h4 style="padding:0px ;margin:0px;">Order Summary for <b><?php echo $data['group_voucher_id']; ?><b></h4>
				</div>
				
				<hr size="1" style="margin:1px;">
				
				<?php 
				if($cart_data!="")
				{
					$accumulated_300 = false;
					$accumulated =0;

					foreach($cart_data as $brand_row)
					{
						$per_brand_total = 0;

						foreach($brand_row['prod_list'] as $row)
						{					
							$row = $row['product'];																																		
							$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
						}
						switch ($brand_row['brand_id'])
						{
							case HSBC_BRAND_PROD_ID_MACALLAN:
							case HSBC_BRAND_PROD_ID_SNOW_LEOPARD:
							case HSBC_BRAND_PROD_ID_LONDON_DRY:
								$accumulated += $per_brand_total;
						}
					}

					if($accumulated>=300)
					{
						$accumulated_300 = true;
					}
				
					$grand_total = 0;													
					$delivery_total = 0;
					$color="#ddd";
					foreach($cart_data as $brand_row)
					{
						if($color=="#eee")
							$color="#ddd";
						else
							$color="#eee";
						?>
						<div id="brand_wrapper_<?php echo $brand_row['brand_id'];?>" style="background-color:<?php echo $color?>;padding:10px 10px;">
							<?php
							$per_brand_total = 0;

							foreach($brand_row['prod_list'] as $row)
							{
								$options = $row['option'];
								$row = $row['product'];																				
								$product = json_decode($row->prod_details);
								$grand_total += (($row->prod_cost ) * $row->prod_qty);
								$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
								?>
								<div class="page-line cl body_bottom_1px merchant-products" id="item_wrapper_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>" style="background:<?php echo $color;?>;">
			                        <div class="container p5px_0px ">
			                            <div class="row">
											<div class="col-xs-2" style="border:solid 0px #333">
												<div align="left">
													<?php
													$src = "http://placehold.it/190x102";
													if($product->prod_image !="")
														$src = site_url('custom/uploads/products/'.$product->prod_image);
													?>
													<img style="width:40px;height:40px;" src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>">
												</div>
											</div>
											<div class="col-xs-4" >
												<b class="menu_merchant" ><?php echo $product->merchant_name;?></b>
												<div class="menu_product" style="overflow: hidden;word-wrap: break-word;width:100% !important;border:solid 0px #333;"><?php echo $product->prod_name;?></div>
												<?php
												if(count($options) > 0)
												{
													?>
													<div class="menu_product" style="font-weight:normal;font-size:11px;padding:0px 4px; border:solid 0px; padding-left:0px;">
														<b>Flavor(s):</b>
														<div style="padding-left: 8px;">																											
															<?php																									
															foreach ($options as $p)
															{
																?>																										
																<div>
																	<?php echo $p->option_qty;?> - <?php echo $p->option_value;?>
																</div>
																<?php
															}																										
															?>																																																				
														</div>
													</div>
													<?php
												}
												?>	
											</div>
											<div class="col-xs-2" align="right" style=";padding:0px;">
												<div class="menu_product" style="line-height: 1.3em;">
													S$&nbsp;<span id="prod_cost_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>"><?php echo number_format(($row->prod_cost  ),2);?></span>
												</div>
											</div>
											<div class="col-xs-1" style="border:solid 0px #333;padding-left:8px;" >
												<?php echo $row->prod_qty; ?>
											</div>
											<div class="col-xs-3 menu_merchant" align="right" style="font-weight:bold;font-size:11px;line-height: 1em;" id="total_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
												S$ <?php echo number_format(( ($row->prod_cost ) ) * ($row->prod_qty),2);?>
											</div>
										</div>
			                        </div>
								</div>
								<?php 
							}
							$has_delivery = false;
							$brand_eror='';
							switch ($brand_row['brand_id']) {
								case HSBC_BRAND_PROD_ID_MACALLAN:
								case HSBC_BRAND_PROD_ID_SNOW_LEOPARD:
								case HSBC_BRAND_PROD_ID_LONDON_DRY:
										if($per_brand_total<300)
										{
											if($accumulated_300 === false)
											{
												$brand_eror = "";
											}
										}
									break;
								case HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP:
									$has_delivery = 16;
									break;
								case HSBC_BRAND_PROD_ID_SINGBEV:
									$has_delivery = 35;
									break;
								case HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN:
									$has_delivery = 25;
									break;
								case DBS_BRAND_PROD_ID_EURACO:
									if ($per_brand_total < 300) {
										$has_delivery = 30;
									}
									break;
							}
							$brand_display_error=($brand_eror=='') ? 'none' :'block';
							?>	
							<div class="container p5px_0px ">	
							<div class="row"  >																	
								<div 	data-brand_id="<?php echo $brand_row['brand_id'];?>" 
										data-brand_name="<?php echo $brand_row['brand_name'];?>" 
										class="brand_error alert alert-error col-xs-12" 
										id="error_<?php echo $brand_row['brand_id'];?>"
										style="display:<?php echo $brand_display_error;?>;";
										align="center"
								><?php echo $brand_eror;?></div>
							</div>
							<?php
								$has_delivery_display = ($has_delivery===false) ? 'none':'';
								$delivery_total += $has_delivery;

								$no_forpickup='';
								$delivery_only='';

								$no_fordelivery='';
								$pickup_only='';
								if($brand_row['brand_id'] == HSBC_BRAND_PROD_ID_SINGBEV || $brand_row['brand_id'] == HSBC_BRAND_PROD_ID_DAILY_JUICE || $brand_row['brand_id'] == DBS_BRAND_PROD_ID_EURACO )
								{
									$no_forpickup='none';
									$delivery_only=' Only';
								}																				
								if($brand_row['brand_id'] == HSBC_BRAND_PROD_ID_NASSIM_HILL ||
								$brand_row['brand_id'] == HSBC_BRAND_PROD_ID_KPO
								)
								{
									$no_fordelivery='none';
									$pickup_only=' Only';
								}
								?>
								<!-- delivery charge and subtotal -->
								
									<div class="row"  >
										<div class="col-xs-8 form-inline" align="left" >
											<label style="font-size:13px;font-weight:bold" class="form-group">Delivery Type: </label>
											<label class="form-group"  for="for_pickup_<?php echo $brand_row['brand_id'];?>" style="display:<?php echo $no_forpickup;?>;">
												<?php if ($row->delivery_type == 'delivery') { ?>
													For Delivery
												<?php } else { ?>
													For Pick up
												<?php } ?>
											</label>
										</div>
										<div 	class="col-xs-4 delivery_charge" 
												data-delivery_charge="<?php echo $has_delivery;?>"
												data-delivery_type="delivery"
												data-id="<?php echo $brand_row['brand_id'];?>" 
											 	align="right" style="font-size:12px;font-weight:bold;display:<?php echo $has_delivery_display;?>" id="delivery_charge_<?php echo $brand_row['brand_id'];?>">
											Delivery Charge: S$ <span id="delivery_charge_value_<?php echo $brand_row['brand_id'];?>"><?php echo number_format($has_delivery,2);?></span>
										</div>
									</div>
								</div>
								<!-- delivery charge and subtotal -->
						</div><!--<div style="background-color:<?php echo $color?>;padding:10px 10px;"> -->
					<?php
						
						
					}
				}
				else
				{
					echo "No items on your cart yet.";
				}
				?>
				<div class="page-line cl body_bottom_1px" style="" >
				</div>
				<?php
				if(isset( $grand_total) && $grand_total > 0)
				{
				?>
				<div class="container p5px_0px ">
					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Sub Total: S$ <span id="sub_total">
								<?php echo number_format($grand_total,2);?>
							</span>&nbsp;
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							GST: S$ <span id="gst">
								<?php echo number_format($grand_total * 0.07, 2);?>
							</span>&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Total: S$ <span id="grand_total">
								<?php echo number_format(($grand_total * 0.07) + $grand_total + $delivery_total,2);?>
							</span>&nbsp;
						</div>
					</div>
					<div class="row hide">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Total: USD$ <span id="grand_total_usd">
								<?php echo number_format(($grand_total + $delivery_total) * BRAND_FOREX_AMEX,2);?>
							</span>&nbsp;
						</div>
					</div>
					<br>
					<div class="row">
                        <div class="col-xs-12 col-sm-4" align="right" style="font-size:14px;font-weight:bold">
                            <button id="order_confirm_ok" class="btn-custom-green" style="font-size:16px !important">OK</button>
                        </div>
					</div>
				</div>
				<?php
				}
				?>

				<?php } ?>
                  
                    
            </div>


			<!--body-->

				<div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/m/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <div id="fb-root"></div>
        <script>
            
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
            <?php
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) {
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            
            var GlompFormDialog = jQuery('form#GlompFormDialog').dialog({
                autoOpen: false,
                dialogClass : 'dialog_style_glomp',
                width: '80%',
                modal: true,
                buttons: [
                    {
                        text: 'Confirm',
                        class: 'fl btn-custom-blue-glomp_xs w100px',
                        click: function() {
                            GlompFormDialog.submit();
                        },
                        style: 'margin:1px;'
                    },
                    {
                        text: 'Cancel',
                        class: 'fr btn-custom-ash_xs w100px',
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }
                ]
            });
            
            var GlompFormAlert = jQuery('<div class="form-alert" />').dialog({
                autoOpen: false,
                width: '70%',
                modal: true,
                dialogClass: 'dialog_style_glomped_alerts',
                minHeight: 'auto'
            });
            
            var GlompFormPreload = jQuery('<div class="form-preload" />').dialog({
                autoOpen: false,
                width: '60%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait',
                title: 'Please wait...',
                minHeight: 'auto'
            }).html('<div style="text-align:center"><img width="40" src="/assets/m/img/ajax-loader.gif" /></div>');
            
            var GlompShareDialog = jQuery('#share-dialog').dialog({
                autoOpen: false,
                width: '90%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                minHeight: 'auto'
            });
            
            $(function() {

                $('#back_btn').click(function(e) {
                   e.preventDefault();
                   window.history.back();
                });
                $("#tour_cancel").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
				$("#tour_done").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
               
                
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});

            jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
                jQuery('.user-menu .user-icon .user-details').toggle();
            });
            
            jQuery(document).on('click','.toggle',function(e){
                e.preventDefault();
                toggleIn = jQuery(e.target).attr("toggle-in");
                toggleOut = jQuery(e.target).attr("toggle-out");
                
                jQuery(toggleIn).show();
                jQuery(toggleOut).hide();
            });
            
            
            $('#order_confirm_ok').click(function(e){
               e.preventDefault();
               window.location = '<?php echo site_url('m/'.$public_alias.'sg'); ?>';
            });
        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
		<script src="<?php echo minify('assets/m/js/custom.glomp_amex.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
</html>
