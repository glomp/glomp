<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background:#DEE6EB;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div id="redeem_bg" class="global_wrapper" style="background-color: #d5d8e4">
        <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
        <div class="navbar navbar-default navbar_relative" style="relative:fixed;width:100%;top:0px;left:0px;">
            <div class="header_navigation_wrapper fl" align="center">        				
                <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                    <nav>
                        <a href="#" id="menu-icon-nav"></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                    <div class="fl white"><?php echo $this->lang->line('Home'); ?></div>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>	
                <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>
            </div>
        </div>
			<!--body-->  
			<div class="cl p20px_0px" style="margin-top:12px;"></div>		
           <div class="container p5px_0px global_margin white" style="text-align:justify ">
				<div class="p5px_20px" align="center" style="background-color:#585F6B;border-radius:5px;color:#A8CD39;font-size:12px;">
                                                <div><?php echo (is_string($response) && isset($response)) ? $response : ''; ?></div>
						<?php echo $this->lang->line('your_voucher_has_been_validated'); ?>						
						<div style="font-size:14px;"><strong><?php echo ucfirst($this->lang->line('enjoy'));?>!</strong></div>						
				</div>
            </div>
			<div class="cl p2px_20px">	</div>			
			<div class="container p5px_0px global_margin white" style="text-align:justify ">
				<div class="p5px_20px" align="center" style="background-color:#FFF;border-radius:5px;color:#000;font-size:16px;">
						<strong><?php echo $this->lang->line('verification_code');?></strong>
						<div><strong class="red" style="margin:0px; padding:2px; font-size:30px; color:#FF0000 !important;"><?php echo $bar_code;?></strong></div>
						<div class="barcode39" style="width:58%;height:50px; margin:0 auto">
							<?php echo $bar_code;?>
						</div>						
				</div>
				
            </div>		
			<div class="container p5px_0px global_margin" align="center" style="font-size:16px; font-weight:bold">   				
				<?php echo $merchant_name;?>, <?php echo $outlet_name;?>
				<div style="font-size:14px"><?php echo   $this->users_m->customFormat($redeem_timestamp);?></div>	
			</div>		
			<div class="" align="center" style="width:70px; height:70px; border:0px solid #333;position:absolute; left:50%; margin-left:-35px;overflow:visible">   				
				<div class="validate_success_ball" id="pulse" >
					<img src="<?php echo base_url() ?>assets/m/img/validate_success_ball_logo.png" id="pulseImg" />
					<div class="validate_success_glows"  ></div>					
				</div> 				
            </div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
            <div class="container p5px_0px global_margin white" style="text-align:justify;" align="center">
				<div class="p5px_20px" align="center" style="background-color:#585F6B;border-radius:5px;color:#A8CD39;font-size:12px;">
						<?php echo $this->lang->line('reedmed_success_message_note'); ?>						
				</div>
            </div>
            
            <div class="cl p5px_20px" align="center">
                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs " data-voucher_id="" >share</button>
                <div style="height:0px;margin-top:25px;">
                    <div id="share_wrapper_box_" class="share_wrapper_box_up" data-voucher_id="" style="" align="center">
                        <button data-voucher_id="<?php echo $voucher_id;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook_redeem btn-custom-blue_xs_fb"   ></button>
                        <button id="share_linkedin_btn_redeem" data-voucher_id="<?php echo $voucher_id;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin_redeem btn-custom-light_blue_xs_tweet"   ></button>
                        <button  data-from="redeem_m" data-voucher_id="<?php echo $voucher_id;?>" title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter_redeem btn-custom-light_blue_xs_tweet"  ></button>
                    </div>
                </div>
            </div>	            
            
			
			
			<div class="cl p20px">	</div>								
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/m/js/jquery.pulse.min.js"></script>        
		<script src="<?php echo base_url() ?>assets/m/js/jquery.barcode.0.3.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
		  <script language="javascript" type="text/javascript">
					$(document).ready(function(){
						
							$('.barcode39') .barcode({code:'code39'});
						}
					);
			</script>
        <script>
            var FB_APP_ID                               ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            $(document).ready(function(e) {
                details=new Object();
                details.voucher_id="<?php echo $voucher_id;?>";                
                if(details.voucher_id!="")
                {
                    setTimeout(function() {
                        tryAutoShare_redeem(details);
                    },1500);
                    
                }
                
				var el = $('#pulse_');
				var originalWidth = el.width();
				var originalHeight = el.height();

				var properties = {
				   width :64,
				   height :64,
				   marginTop :3,
				   marginLeft :3
				};

				el.pulse(properties, {duration : 2000, pulses : -1});					
				
				var e2 = $('#pulseImg_');
				var originalWidth2 = e2.width();					

				var properties2 = {					 
				   width :28,
				   height :20,
				   marginTop :22,
				   marginLeft :2					   
				};
				e2.pulse(properties2, {duration : 2000, pulses : -1});
			});
			$(function() {	
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
				});
			
			});
			$(document).mouseup(function(e)
			{
				var container = $(".hidden_menu_class");
				if (!container.is(e.target) /* if the target of the click isn't the container...*/
						&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
				{
					$('#hidden_menu').hide();
				}
			});

        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <script>
            /* On ready */
            $(function() {
                animate();
                
                function animate() {
                    $("#redeem_bg").animate({
                        backgroundColor: "#a1bcd2",
                    }, 1000 , function(){
                        animate2();
                    });
                }
                
                function animate2() {
                    $("#redeem_bg").animate({
                        backgroundColor: "#d5d8e4",
                    }, 1000 , function(){
                        animate();
                    });
                }
                
                function share_linkedIn_redeem() {
                    $('#share_linkedin_btn_redeem').trigger('click');
                }
                <?php
                  //run share
                  $func = (isset($_GET['js_func_li'])) ? $_GET['js_func_li'].';' : '';
                  echo $func;
                  
                  $tweet_status = $this->session->flashdata('tweet_status');
                  $tweet_message = $this->session->flashdata('tweet_message');        
                  if (! empty($tweet_status)) {
                    echo "doTweetSucces('".$tweet_status."','".$tweet_message."');";
                  }
                ?> 
           });
        </script>        
    </body>
    
</html>