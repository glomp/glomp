<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">        
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="">            
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>		
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>         
        <script>
            $(function() {
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
                });
                $('#btnpay').click(function(){
                    $(".Loader").show();
                    $("#paypalSuccessMessage").hide();
                    $("#paypalErrorMessage").hide();
                    
                    $.ajax({
                        type: "POST",
                        url:"<?php echo base_url(MOBILE_M) ?>/buyPoints/addPoints/",
                        data: $('#frmPaypal').serialize(),
                        success: function(data){
                            var data = eval("("+data+")");
                            if(data.status=='success')
                            {
                                $("#paypalSuccessMessage").show();
                                document.frm_confirm_paypal_checkout.custom.value=data.inserted_id;
                                document.frm_confirm_paypal_checkout.amount.value=data.cost;                                
                                document.frm_confirm_paypal_checkout.submit();
                                
                            } else {
                                $("#paypalSuccessMessage").hide();
                                $(".Loader").hide();
                                $("#paypalErrorMessage").show();
                            } 
                        }
                    });
                    
                });                
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
             <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-points-logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations        
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>      
                    <div class="cl hidden_nav_seperator fl w200px"></div>                
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>/buyPoints">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line(''); ?>Buy Points
                        </div>
                    </a>                
                </div>
                <!-- hidden navigations -->
            </div>
            
			<div class="cl p20px_0px" style="margin-top:12px;"></div>	
			<!--body-->  
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <div style="font-size:18px;padding:5px 0px 8px 0px;color:#465069">
                    <strong>Purchase Details</strong>
                </div>	
            </div>	
            <div class="container global_margin " style="">
                <div class="alert alert-success" style="display:none;" id="paypalSuccessMessage">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $this->lang->line('paupal_redirect_msg');?> <em><strong>
                    <?php echo $this->lang->line('PayPal');?>
                    </strong></em>.... 
                </div>
                <div class="alert alert-error" style="display:none;" id="paypalErrorMessage">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $this->lang->line('paypal_error_msg_1', 'Sorry. An error occurred');?> <em><strong></strong></em>.... 
                </div>
            </div>    
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <div class="" style="float:left;margin-top:10px;font-size:14px;">
                    You selected
                </div>
                <div class="glompoints_2" style="float:left">
                    <?php
                        $selectedPoint=$resPoints->row();
                    ?>
                    <div class=""  style="font-size:14px;font-weight:bold"><?php echo $selectedPoint->package_point;?></div>
                </div>
                <div style="float:left;margin-top:13px;">                    
                    <img src="<?php echo base_url() ?>assets/m/img/glomped_list_header1.png" alt="" style="width:55px;float:left;margin-right:1px">
                    <span class="harabarabold" style="float:left;margin-top:-5px;font-size:17px;color:#465069">points</span>
                </div>
            </div>	
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <span style="font-size:12px">Pay with your credit card or PayPal account.&nbsp;&nbsp;</span>
                <div align="center" class="Loader"><i class="icon-spinner icon-spin icon-large"></i></div>
            </div>	
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <button class="btn-custom-green_big" id="btnpay" style="width:100%">Checkout</button>
            </div>
            <form name="frmPaypal" id="frmPaypal" method="post" action="">            
                <input type="hidden" name="_id" id="_id" value="<?php echo $selectedPoint->package_id;?>" >
                <input type="hidden" name="_value" id="_value" value="<?php echo $selectedPoint->package_point;?>" >
                <input type="hidden" name="_cost" id="_cost" value="<?php echo $selectedPoint->package_price;?>" >
                <input type="hidden" name="session_payment_visisted_id" value="<?php echo $session_payment_visisted_id;?>" />
            </form>
            <form id="frm_confirm_paypal_checkout" name="frm_confirm_paypal_checkout" method="post" action="<?php echo base_url(MOBILE_M.'/buyPoints/express_checkout');?>">
                <input type="hidden" name="amount" value="" />
                <input type="hidden" name="custom" value="" />
            </form>
			<!--body-->
            <div class="cl p20px"></div>    
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->
        
    </body>
</html>
