<?php
	$profile_id = $user_record->user_id;
	$user_id = $this->session->userdata('user_id');
	
	
	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	if($inTour=='yes'){			
		$inTour_display='display:none;';
		$inTour_header_display='';
		$inTour_header='Add favourites';
		$inTour_header_top_position='0';
		$inTour_header_top_margin='100';
		$inTour_header_top_position_2='48';
		$inTour_display_footer='';
	}
	
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $user_record->user_fname.' '.$user_record->user_lname;?>'s menu | glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="<?php echo $user_record->user_fname.' '.$user_record->user_lname." menu on glomp!."; ?>" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">            
    </head>
    <body style="background: white;" ci-tpl="menu">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">            
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>                                
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>                                
                                <?php } ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php 
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>                                    
                                    <?php
                                     if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                                     { ?>                                        
                                        
                                        <li>
                                            <a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>" class="white ">
                                                <div class="w100per fl white">
                                                <?php echo $this->lang->line('edit_favourite'); ?>
                                                </div>
                                            </a>
                                        </li>
                                        
                                    <?php } ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>                                    
                                <?php
                                }
                                ?>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations         
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <?php if (isset($_GET['mDetail'])) { ?>
                    <a id = "back_btn" class=" cl hidden_nav_link fl w200px" href="#">
                        <div class="hidden_nav">
                            Back<?php //echo $this->lang->line('Back'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <?php } ?>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/profile') ?>">
                        <div class="hidden_nav">                        
                            <?php echo $this->lang->line('profile'); ?>
                        </div>
                    </a>
                    <?php
                     if(($tabSelected=='favourites' || $tabSelected=='')  && $thisIsOtherProfile==false)
                     { ?>
                        <div class="cl hidden_nav_seperator fl w200px"></div>
                        <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=favourites&action=edit') ?>">
                            <div class="hidden_nav">                        
                                <?php echo $this->lang->line('edit_favourite'); ?>
                            </div>
                        </a>				
                    <?php } ?>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class="cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>">
                        <div class="hidden_nav">                        
                            <?php echo $this->lang->line('Log_Out'); ?>
                        </div>
                    </a>				
                </div>
                <!-- hidden navigations -->  
            </div>
            <!--<div class="p20px_0px" style="margin-top:10px;"></div>	-->
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>

                            
			
			<!--body-->     
			<?php
				/*if ($this->session->userdata('user_id') && $this->session->userdata('user_id') > 0) {

					$user_id = $this->session->userdata('user_id');
					$res_header = json_decode($this->user_account_m->user_summary($user_id));
					#user info
					$user_info = json_decode($this->user_account_m->user_short_info($user_id));
										
				}*/
				/*$rec_user= $this->users_m->user_info_by_id($user_id);*/
				$rec_user= $this->users_m->user_info_by_id($profile_id);                
				$user_info = json_decode($this->user_account_m->user_short_info($profile_id));
				/*$profile_id=$user_id;*/
				$user_recod = $rec_user->row();
                
                //print_r($user_recod);
				
				$tab1='';$tab2='';$tab3='';
				 if($tabSelected=='merchants'){$tab1='menu_header_selected';}
				 if($tabSelected=='products'){$tab2='menu_header_selected';}
				 if($tabSelected=='favourites' || $tabSelected==''){$tab3='menu_header_selected';}
				?>							
			<div class="menu_header_wrapper" align="center">
				<div class="" align="center">
                    <div class="menu_header">
                        <div class="user-icon">
                            <div class="user-image" style="background-image:url(/custom/uploads/users/thumb/<?php echo $user_record->user_profile_pic?>)">
                                <div class="menu_header_content_2" style="background:none">&nbsp;</div></div>
                            <div class="user-details">
                                <p style="background:#EB2227"><?php echo $user_record->user_fname.' '.$user_record->user_lname?></p>
                                <p style="background:#EB2227"><?php echo (isset($user_record->location_string))?$user_record->location_string:'&nbsp;';?></p>
                            </div>
                        </div>
					</div>
                    <div class="menu_header">
                        <a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/brands/'.$user_record->user_id.'?inTour='.$inTour) ?>">
                            <div class="menu_header_content_2"><?php echo $this->lang->line('brands', 'Brands'); ?></div>
                        </a>
                    </div>
					<div class="menu_header">
						<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=merchants') ?>">
							<div class="menu_header_content <?php echo $tab1;?>"><?php echo $this->lang->line('Merchants'); ?></div>
						</a>
					</div>
					<div class="menu_header">
						<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products') ?>">
							<div class="menu_header_content <?php echo $tab2;?>"><?php echo $this->lang->line('Products'); ?></div>
						</a>
					</div>
					<div class="menu_header">
						<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=favourites') ?>">
                                                    <div class="menu_header_content  <?php echo $tab3;?>"><?php echo $this->lang->line('favourites'); ?></div>
						</a>
					</div>
                    <div class="clearfix"></div>
				</div>
			</div>
                        <div class="clearfix"></div>
                        
			
				<!--$tabSelected=='merchants' -->
				<?php if($tabSelected=='merchants')
				{?>					
					<?php if(is_numeric($selectedMerchantID)  &&   $this->merchant_m->is_mercahnt($selectedMerchantID))
					{ ?>
						<!--$tabSelected=='merchants' specific merchant page-->
							<?php if($mDetail=='info')
							{ ?>
                                <!--$tabSelected=='merchants' specific merchant page  INFO-->
                                <?php $this->load->view('m/profile_menu_merchant_v');?>
                                
							<?php } 
							else if($mDetail=='locations')
							{ ?>
								<!--$tabSelected=='merchants' specific merchant page  LOCATIONS-->
								<div class="container p5px_0px global_margin">								
										<img class="fl menu_merchant_photo_2" src="<?php echo base_url().$this->custom_func->merchant_logo($res_merchant->merchant_logo); ?>"   alt="<?php echo $res_merchant->merchant_name;?>" />					
										<div class="fl menu_merchant_name_wrapper" style="width:75%;">
											<div class="menu_merchant_name" ><?php echo $res_merchant->merchant_name;?></div>										
											<div><button class="btn-custom-blue-grey_xs w40px"  >Like</button></div>
										</div>
								</div>								
								
									<div class="container p5px_0px global_margin" style="text-align:justify ">
										<div class="container p10px_0px ">
											<div class="row red harabarabold" style="font-size: 20px;"><?php echo $this->lang->line('merchant_locations');?></div>
										</div>
										<?php 											
												foreach($merchant_outlet->result() as $row)
												{
													$formatted=$this->address_m->address_format($row->address_1, $row->address_2, $row->address_street, $row->address_locality, $row->address_city_town, $row->address_region, $row->address_zip_code,  $row->region_name);                                                
													$outlet_name=$row->outlet_name;
													echo '
														<div class="cl body_bottom_1px p5px_0px">
															<strong>'.$outlet_name.'</strong><br>
															'.$formatted->address_formatted.'														
														</div>
													';
												}/*foreach*/
												
											?>
									</div>	
								<div class="cl p20px">	</div>								
								<!--$tabSelected=='merchants' specific merchant page  LOCATIONS-->
							<?php }
							else if($mDetail=='tnc')
							{ ?>
								<!--$tabSelected=='merchants' specific merchant page  TNC-->
								<div class="container p5px_0px global_margin">								
										<img class="fl menu_merchant_photo_2" src="<?php echo base_url().$this->custom_func->merchant_logo($res_merchant->merchant_logo); ?>"   alt="<?php echo $res_merchant->merchant_name;?>" />					
										<div class="fl menu_merchant_name_wrapper" style="width:75%;">
											<div class="menu_merchant_name" ><?php echo $res_merchant->merchant_name;?></div>										
											<div><button class="btn-custom-blue-grey_xs w40px"  >Like</button></div>
										</div>
								</div>								
                                
								<div class="container p5px_0px global_margin" style="text-align:justify ">
									<div class="container p10px_0px ">
										<div class="row red harabarabold" style="font-size: 20px;"><?php echo $this->lang->line('termNconditi0n');?></div>
									</div>									
									<?php echo $res_merchant->merchant_terms;?>
								</div>
								<div class="cl p20px">	</div>								
								<!--$tabSelected=='merchants' specific merchant page  TNC-->
							<?php }
							else
							{
							?>
								<!--$tabSelected=='merchants' specific merchant page  MENU-->
								<div class="menu_merchant_wrapper ">
									<div class="container p5px_0px global_margin">								
											<div class="fl" style="width:78%;"><?php echo $res_merchant->merchant_name;?></div>
											<div class="fr" style="width:20%;" align="right">										
												<a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=merchants&mID=').$selectedMerchantID?>&mDetail=info">
													<div><button class="btn-custom-blue_xs " >&nbsp;info&nbsp;</button></div>
												</a>
											</div>
									</div>
								</div>
								<?php						
									$num_product = $merchant_product->num_rows();
									if($num_product>0)
									{			  
										foreach($merchant_product->result() as $row_product)
										{
									?>
											<div class="cl body_bottom_1px">
												<div class="container p5px_0px global_margin">	
													<div class="row">
														<div class="fl menu_and_point_wrapper" id="<?php echo $row_product->prod_id;?>" style="width:70%;">
															<div class="hidden">
																<div id="prod_<?php echo $row_product->prod_id;?>_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($row_product->prod_merchant_id));?></div>
																<div id="prod_<?php echo $row_product->prod_id;?>_item"><?php echo stripslashes( $row_product->prod_name);?></div>
																<div id="prod_<?php echo $row_product->prod_id;?>_img"><?php echo base_url().$this->custom_func->product_logo($row_product->prod_image);?></div>
																<div id="prod_<?php echo $row_product->prod_id;?>_desc"><?php echo stripslashes( $row_product->prod_details);?></div>
																<div id="prod_<?php echo $row_product->prod_id;?>_point"><?php echo stripslashes( $row_product->prod_point);?></div>
															</div>
															<div class="fl menu_item_photo_wrapper">
																<img class="menu_item_photo" src="<?php echo base_url().$this->custom_func->product_logo($row_product->prod_image);?>" alt="<?php echo stripslashes($row_product->prod_name);?>" />
															</div>
															<div class="fl menu_item_description_wrapper" style="width:45%;border:0px solid #333">
																<div class="menu_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($row_product->prod_merchant_id));?></div>									
																<div class="menu_product"><?php echo stripslashes($row_product->prod_name);?></div>
															</div>
															<div class="fr menu_item_points_wrapper" align="center">
                                                                                                                            <?php echo $row_product->prod_point; ?>
                                                                                                                            <br />
                                                                                                                            <div style="font-size: 10px;line-height: 4px;margin-left: 1px;">pts</div>
															</div>
														</div>
														<div class="fr " style="border:0px solid #333">	
															<?php if($thisIsOtherProfile==false && $this->product_m->checkAddFevitemToFevList($row_product->prod_id)==0)
															{ ?>
																<a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab='.$tabSelected.'&sub='.$tabSelectedSub.'&mID='.$selectedMerchantID.'&add='.$row_product->prod_id) ?>" ref="">
																	<div class="menu_item_operation_wrapper" align="center" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp-add.png');"></div>	
																</a>
															<?php 
															} else  if($thisIsOtherProfile==true)
															{?>													
																<button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper" id="<?php echo $row_product->prod_id;?>" align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_gray.png'); ?>');"></button>
															<?php }?>															
															</div>
													</div>
												</div>
											</div>
									<?php } /* for each*/
									}?>	
									<!--$tabSelected=='merchants' specific merchant page  MENU-->
							<?php } ?>						
						<!--$tabSelected=='merchants' specific merchant page -->
					<?php 
					}
					else
					{ ?>
						<!--$tabSelected=='merchants' all merchants page -->
						<div class="container p5px_0px global_margin">	
							<?php 
							   $num_merchant = $region_wise_merchant->num_rows();
							   if($num_merchant>0)
								{						
									foreach($region_wise_merchant->result() as $row_merchant)
									{ ?>
										<a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=merchants&mID=').$row_merchant->merchant_id?>" >
											<div class="nameOfMerchant">
												<img class="fl menu_merchant_photo" src="<?php echo base_url().$this->custom_func->merchant_logo($row_merchant->merchant_logo); ?>"   alt="<?php echo $row_merchant->merchant_name;?>" />					
											</div>
										<a>										
									<?php } 
								}?>							
								
						</div>	
						<!--$tabSelected=='merchants' all merchants page -->
					<?php 
					}
					?>
				
				<!--$tabSelected=='merchants' -->	
				
				<!--$tabSelected=='products' -->
				<?php }
				else if($tabSelected=='products')
				{ 
					$subTab1="";$subTab2="";$subTab3="";$subTab4="";$subTab5="";
					if($tabSelectedSub=='drinks' || $tabSelectedSub=='')		{$subTab1='menu_header_selected_2';}
					if($tabSelectedSub=='snacks')	{$subTab2='menu_header_selected_2';}
					if($tabSelectedSub=='cocktails')	{$subTab3='menu_header_selected_2';}
					if($tabSelectedSub=='sweets')	{$subTab4='menu_header_selected_2';}
					if($tabSelectedSub=='others')		{$subTab5='menu_header_selected_2';}
				?>
					<div class="cl menu_header_wrapper_2 " align="center">
						<div class="" align="center">					
							<div class="menu_header_2">
								<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products&sub=drinks') ?>">
									<div class="menu_header_content_2  <?php echo $subTab1;?>" style="border:0px;"><?php echo ucfirst($this->lang->line('drinks'));?></div>
							</div>
							<div class="menu_header_2">
								<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products&sub=snacks') ?>">
									<div class="menu_header_content_2 <?php echo $subTab2;?>"><?php echo ucfirst($this->lang->line('snacks'));?></div>
								</a>
							</div>
                            <?php if(!$this->user_login_check_m->underage()):?>
							<div class="menu_header_2">
								<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products&sub=cocktails') ?>">
									<div class="menu_header_content_2  <?php echo $subTab3;?>"><?php echo ucfirst($this->lang->line('cocktails'));?></div>
								</a>
							</div>
                            <?php endif;?>
							<div class="menu_header_2">
								<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products&sub=sweets') ?>">
									<div class="menu_header_content_2  <?php echo $subTab4;?>"><?php echo ucfirst($this->lang->line('sweets'));?></div>
								</a>
							</div>
							<div class="menu_header_2">
								<a class="white" href="<?php echo base_url(MOBILE_M . '/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=products&sub=others') ?>">
									<div class="menu_header_content_2  <?php echo $subTab5;?>"><?php echo ucfirst($this->lang->line('others'));?></div>
								</a>
							</div>
						</div>
					</div>							
					<!--$tabSelectedSub-->
							<?php 
							$profile_region_id = $user_recod->user_city_id;												
							$cat_id=$cat_id;						
							$users_merchant_id_list = $this->merchant_m->regionwise_merchant_list($profile_region_id);
							$user_specfic_product = $this->product_m->users_cat_product($users_merchant_id_list,$cat_id);			
							$num_product = $user_specfic_product->num_rows();
							if($num_product>0)
							{			  
								foreach($user_specfic_product->result() as $row_product)
								{
							?>
									<div class="cl body_bottom_1px">
										<div class="container p5px_0px global_margin">	
											<div class="row">
												<div class="fl menu_and_point_wrapper" id="<?php echo $row_product->prod_id;?>" style="width:70%;">
													<div class="hidden">
														<div id="prod_<?php echo $row_product->prod_id;?>_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($row_product->prod_merchant_id));?></div>
														<div id="prod_<?php echo $row_product->prod_id;?>_item"><?php echo stripslashes( $row_product->prod_name);?></div>
														<div id="prod_<?php echo $row_product->prod_id;?>_img"><?php echo base_url().$this->custom_func->product_logo($row_product->prod_image);?></div>
														<div id="prod_<?php echo $row_product->prod_id;?>_desc"><?php echo stripslashes( $row_product->prod_details);?></div>
														<div id="prod_<?php echo $row_product->prod_id;?>_point"><?php echo stripslashes( $row_product->prod_point);?></div>
													</div>
													<div class="fl menu_item_photo_wrapper">
														<img class="menu_item_photo" src="<?php echo base_url().$this->custom_func->product_logo($row_product->prod_image);?>" alt="<?php echo stripslashes($row_product->prod_name);?>" />
													</div>
													<div class="fl menu_item_description_wrapper" style="width:55%;border:0px solid #333">
														<div class="menu_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($row_product->prod_merchant_id));?></div>									
														<div class="menu_product"><?php echo stripslashes($row_product->prod_name);?></div>
													</div>
													<div class="fr menu_item_points_wrapper" align="center">
                                                                                                            <?php echo $row_product->prod_point; ?>
                                                                                                            <br />
                                                                                                            <div style="font-size: 10px;line-height: 4px;margin-left: 1px;">pts</div>
													</div>
												</div>
												<div class="fr " style="border:0px solid #333">	
													<?php if($thisIsOtherProfile==false && $this->product_m->checkAddFevitemToFevList($row_product->prod_id)==0)
													{ ?>
														<a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab='.$tabSelected.'&sub='.$tabSelectedSub.'&add='.$row_product->prod_id) ?>" ref="">
															<div class="menu_item_operation_wrapper" align="center" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp-add.png');"></div>	
														</a>
													<?php 
													} else  if($thisIsOtherProfile==true)
													{?>													
														<button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"  id="<?php echo $row_product->prod_id;?>" align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_gray.png'); ?>');"></button>
													<?php }?>
												</div>
											</div>
										</div>
									</div>
							<?php } /* for each*/
							}
							else
							{ ?>
								<p class="alert alert-info"><?php echo $this->lang->line('sorry_no_product_available');?></p>
							<?php } ?>							
					<!--$tabSelectedSub-->
				<!--$tabSelected=='products' -->
				<!--$tabSelected=='favourites' -->				
				<?php }
				else if($tabSelected=='favourites' || $tabSelected=='')
				{ 
					$resFev = $this->product_m->userFevProduct($profile_id);
					$febCount = $resFev->num_rows();
				?>
				
					<?php
					if($febCount>0){
						foreach($resFev->result() as $recFev) {
                            if( in_array( $recFev->prod_cat_id, $underage_filtered ) && $this->user_login_check_m->underage() ) continue;
                            ?>
							<div class="cl body_bottom_1px">
								<div class="container p5px_0px global_margin">	
									<div class="row">
										<div class="fl menu_and_point_wrapper" id="<?php echo $recFev->prod_id;?>" style="width:70%;">
											<div class="hidden">
												<div id="prod_<?php echo $recFev->prod_id;?>_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($recFev->merchant_id));?></div>
												<div id="prod_<?php echo $recFev->prod_id;?>_item"><?php echo stripslashes( $recFev->prod_name);?></div>
												<div id="prod_<?php echo $recFev->prod_id;?>_img"><?php echo base_url().$this->custom_func->product_logo($recFev->prod_image);?></div>
												<div id="prod_<?php echo $recFev->prod_id;?>_desc"><?php echo stripslashes( $recFev->prod_details);?></div>
												<div id="prod_<?php echo $recFev->prod_id;?>_point"><?php echo stripslashes( $recFev->prod_point);?></div>
											</div>
											<div class="fl menu_item_photo_wrapper">
												<img class="menu_item_photo" src="<?php echo base_url().$this->custom_func->product_logo($recFev->prod_image);?>" alt="<?php echo stripslashes($recFev->prod_name);?>">
											</div>
											<div class="fl menu_item_description_wrapper" style="width:45%;border:0px solid #333">
												<div class="menu_merchant"><?php echo stripslashes( $this->merchant_m->merchantName($recFev->merchant_id));?></div>									
												<div class="menu_product"><?php echo stripslashes($recFev->prod_name);?></div>									
											</div>
											<div class="fr " align="center">
												
											</div>
										</div>
										<div class="fr " style="border:0px solid #333">												
											<?php if($thisIsOtherProfile==false && $action=='edit')
												{ ?>
													<a href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'?inTour='.$inTour.'&tab=favourites&action=edit&del='.$recFev->prod_id) ?>"ref="">
														<div class="menu_item_operation_wrapper" align="center" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp_minus.png');"></div>
													</a>
												<?php 
												} else  if($thisIsOtherProfile==true)
												{?>																									
													<button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"  id="<?php echo $recFev->prod_id;?>"  align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_gray.png'); ?>');"></button>
												<?php }?>
											
										</div>
									</div>
								</div>
							</div>
						
						
						<?php }/*for each*/
					}					
					else
					{ ?>
						<div class="alert">									
								<strong><?php echo $user_record->user_name;?> <?php echo $this->lang->line('has_not_added_favourite');?>
						</div>
					<?php } ?>
				<!--$tabSelected=='favourites' -->
				<?php } ?>			
			<!--body-->
				<div class="" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>					   
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>	
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script>
            var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
            <?php 
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) { 
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            $(function() {	
                
            $('#back_btn').click(function(e) {
               e.preventDefault();
               window.history.back();
            });
				function submitGlomp()
				{
					console.log($('#glomp_form').serialize());
							var glomp_message=$('#glomp_message').val();
							var glomp_password=$('#glomp_password').val();										
							if(glomp_message==''){
								var NewDialog = $('<div id="confirmPopup" align="center">\
													<div align="center" style="margin-top:-5px;"><strong>Confirm</strong></div>\
													<div align="center" style="margin-top:5px;">Are you sure you dont want to include a message?</div></div>');																
								NewDialog.dialog({						
									autoOpen: false,
									resizable: false,					
									dialogClass:'dialog_style_glomped_alerts',
									title:'',
									modal: true,
									width:200,
									height:120,
									position: 'center',
									buttons: [						
										{text: "Yes", 
										"class": 'btn-custom-blue-grey_xs w80px',
										click: function() {
											if(glomp_password==''){
												$('#confirmPopup').dialog('destroy').remove();
												var NewDialog = $('<div id="confirmPopup" align="center">\
																	<div align="center" style="margin-top:-5px;"><strong>Erorr!</strong></div>\
																	<div align="center" style="margin-top:5px;">Incorrect Password</div></div>');																
												NewDialog.dialog({						
													autoOpen: false,
													resizable: false,					
													dialogClass:'dialog_style_glomped_alerts',
													title:'',
													modal: true,
													width:200,
													height:120,
													buttons: [													
														{text: "Ok", 
														"class": 'btn-custom-blue-grey_xs w80px',
														click: function() {
															$(this).dialog("close");
															setTimeout(function() {
																$('#confirmPopup').dialog('destroy').remove();
															}, 500 );						
														}}
													]
												});
												NewDialog.dialog('open');
											}/*if(glomp_password==''){*/
											else{
												doGlomp();
											}
										}},
										{text: "Cancel", 
										"class": 'btn-custom-blue-grey_xs w80px',
										click: function() {
											$(this).dialog("close");
											setTimeout(function() {
												$('#confirmPopup').dialog('destroy').remove();
											}, 500 );						
										}}
									]
								});
								NewDialog.dialog('open');
							}/*if(glomp_message==''){*/
							else if(glomp_password==''){
								var NewDialog = $('<div id="confirmPopup" align="center">\
													<div align="center" style="margin-top:-5px;"><strong>Erorr!</strong></div>\
													<div align="center" style="margin-top:5px;">Incorrect Password</div></div>');																
								NewDialog.dialog({						
									autoOpen: false,
									resizable: false,					
									dialogClass:'dialog_style_glomped_alerts',
									title:'',
									modal: true,
									width:200,
									position: 'center',
									height:120,
									buttons: [													
										{text: "Ok", 
										"class": 'btn-custom-blue-grey_xs w80px',
										click: function() {
											$(this).dialog("close");
											setTimeout(function() {
												$('#confirmPopup').dialog('destroy').remove();
											}, 500 );						
										}}
									]
								});
								NewDialog.dialog('open');
							
							}/*else if(glomp_password==''){*/
							else{
								doGlomp();
							}
				}
				function doGlomp()
                {		
                    doGlomp_ga_gs();
					var data=$('#glomp_form').serialize();
					/*$('#glomp_form').dialog('destroy').remove();*/
					$('#confirmPopup').dialog('destroy').remove();
					var NewDialog = $('<div id="confirmPopup" align="center">\
										<div align="center" style="margin-top:5px;"><img width="40" src="<?php echo base_url() ?>assets/m/img/ajax-loader.gif" /></div></div>');																
					NewDialog.dialog({						
						autoOpen: false,
						closeOnEscape: false ,
						resizable: false,					
						dialogClass:'dialog_style_glomp_wait',
						title:'Please wait...',
						modal: true,
						position: 'center',
						width:200,
						height:120
					});
					NewDialog.dialog('open');
					
					
					/*do the ajax glomp*/
					$.ajax({
						type: "POST",
						cache: false,						
						url:"<?php echo base_url(MOBILE_M) ?>/profile/glompToUser/<?php echo $profile_id;?>",
						data:data,
						success: function(response){						
							var response = eval("("+response+")");							
							if(response.status=='success')
							{   
                                var details=response;
                                /*console.log(details);*/
                                
                                tryAutoShare(details);
                                var linkedIn_share = '<button data-merchant="'+details.merchant_name+'"  data-belongs="'+details.to_name_real+'" data-purchaser="'+details.form_name_real+'" data-from_fb_id="'+details.from_fb_id+'" data-to_fb_id="'+details.to_fb_id+'" data-story_type="'+details.story_type+'" data-prod_name="'+details.prod_name+'" data-prod_img="'+details.product_logo+'"  data-voucher_id="'+details.voucher_id+'" title="Share on LinkedIn" style="background:url(<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>); background-size: 37px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet"></button>';
                                linkedIn_share = (from_android) ? '' : linkedIn_share;
                                var facebook_share = '<button data-merchant="'+details.merchant_name+'"  data-belongs="'+details.to_name_real+'" data-purchaser="'+details.form_name_real+'" data-from_fb_id="'+details.from_fb_id+'" data-to_fb_id="'+details.to_fb_id+'" data-story_type="'+details.story_type+'" data-prod_name="'+details.prod_name+'" data-prod_img="'+details.product_logo+'"  data-voucher_id="'+details.voucher_id+'" title="Share on Facebook" style="background:url(<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>); background-size: 45px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb"></button>';
                                var twitter_share = '<button  data-from="dashboard_m" data-voucher_id="'+details.voucher_id+'" title="Share on Twitter"  style="background:url(<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>); background-size: 41px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet"  ></button>';
                                
								$('#glomp_form').dialog('destroy').remove();
								$('#confirmPopup').dialog('destroy').remove();
								var NewDialog = $('<div id="confirmPopup" align="center">\
													<div align="center" style="margin:5px 0px;">'+response.msg+'.<br><span style="font-size:14px;">Would you like to share the news?</span></div>\
                                                    <div id="success_buttons_wrapper" class="cl" align="left" style="padding:15px 0px;">' + facebook_share + linkedIn_share + twitter_share + '</div>\
                                                    <div style="height:0px;" class="cl">\
                                                        <div id="share_wrapper_box_up" class="share_wrapper_box_up" style="" align="center">' + facebook_share + linkedIn_share + twitter_share + '</div>\
                                                    </div>\
                                                    </div>');
								NewDialog.dialog({
									autoOpen: false,
									closeOnEscape: false ,
									resizable: false,
									dialogClass:'dialog_style_glomp_wait noTitleStuff',
									title:'',
									modal: true,
									width:280,
									position: 'center',
									height:120
								});
                                NewDialog.dialog('open');
                                $elem = $('<button type="button" class="glomp_fb_share btn-custom-blue-grey_xs w80px fl" style="height:25px"  >Share</button>');
                                $elem.on('click', function(e){
                                    $('#share_wrapper_box_up').toggle();
                                });
                                /*$("#success_buttons_wrapper").append($elem);*/                                
                                
                                $elem = $('<button type="button" class="btn-custom-white_xs w80px fr"   style="height:25px"  >Close</button>');
                                $elem.on('click', function(e) {
                                    $('#confirmPopup').dialog("close");
                                    setTimeout(function() {
                                        $('#confirmPopup').dialog('destroy').remove();
                                    }, 500 );
                                });
                                $("#success_buttons_wrapper").append($elem);
                                
                                
                                /*share function*/
                                $('.share_on_facebook').click(function(){                
                                    $this=$(this);
                                    checkLoginStatus($this,'share');
                                });
                                 $('.share_on_twitter').click(function(){                
                                    $data=$(this).data();
                                    doTwitterShare($data)
                                    
                                });
                                $('.share_on_linkedin').click(function(){                
                                    $this=$(this);        
                                    $data=$(this).data();
                                    doLinkedInShare($data);        
                                });
                                
                                /*share function*/
								
								
							}/*	if(response.status=='success')*/
							else if(response.status=='error')
							{
                                                            $('#glomp_password').val('');
								$('#confirmPopup').dialog('destroy').remove();
								var NewDialog = $('<div id="confirmPopup" align="center">\
													<div align="center" style="margin-top:-5px;"><strong>Erorr!</strong></div>\
													<div align="center" style="margin-top:5px;">'+response.msg+'</div></div>');																
								NewDialog.dialog({						
									autoOpen: false,
									resizable: false,					
									dialogClass:'dialog_style_glomped_alerts',
									title:'',
									modal: true,
									width:200,
									position: 'center',
									height:120,
									buttons: [													
										{text: "Ok", 
										"class": 'btn-custom-blue-grey_xs w80px',
										click: function() {
											$(this).dialog("close");
											setTimeout(function() {
												$('#confirmPopup').dialog('destroy').remove();
											}, 500 );						
										}}
									]
								});
								NewDialog.dialog('open');                                
                                
                                
                                
							}/*	else if(response.status=='error')*/
						}
					});
										
				}
				$(".glomp_button_wrapper").click(function() {
					var id = $(this).attr('id');
					var merchant	=$('#prod_'+id+'_merchant').html();
					var item		=$('#prod_'+id+'_item').html();
					var img			=$('#prod_'+id+'_img').html();
					var desc		=$('#prod_'+id+'_desc').html();
					var point		=$('#prod_'+id+'_point').html();
					
					var NewDialog = $('<form name="glomp_form" id="glomp_form" method="post"><div id="" align="left"> \
											<input type="hidden" name="user_fname"  id="" value="<?php echo $user_recod->user_fname;?>" />\
										    <input type="hidden" name="user_lname"  id="" value="<?php echo $user_recod->user_lname;?>" />\
											 <input type="hidden" name="user_email"  id="" value="<?php echo $user_recod->user_email;?>"/>\
											 <input type="hidden" name="product_id"  id="product_id" value="'+id+'" />\
											<div class="cl fl menu_item_photo_wrapper">\
												<img class="menu_item_photo_3" alt="" src="'+img+'"></img>\
											</div>\
											<div class="fl menu_item_details">\
												<div class="menu_merchant white">'+merchant+'</div>\
												<div class="menu_product">'+item+'</div>\
												<div class="fl menu_item_points_wrapper_3" align="center">'+point+'</div>\
											</div>\
											<div class="cl">\
												<div class="">\
													<div class="p8px_5px_0px_5px" align="left">\
														<textarea name="message" id="glomp_message" style="height:40px;" id="glomp_message" class="glomp_input" placeholder="<?php echo $this->lang->line('add_a_message');?>"></textarea>\
													</div>\
													<div class="p2px_5px_0px_5px" align="left" style="margin-top: 6px;">\
														<input name="password" id="glomp_password" type="password" class="glomp_input" placeholder="Password">\
													</div>\
													<div class="p8px_5px_0px_5px" align="center">\
														<a href="<?php echo base_url(MOBILE_M).'/landing/forgotPassword/';?>" class="forgot_pass white">Forgot Password?</a>\
													</div>\
												</div>\
											</div>\
										</div></form>');
										
											
					NewDialog.dialog({						
					autoOpen: false,
					resizable: false,					
					dialogClass:'dialog_style_glomp',
					title:'',
					modal: true,
					width:260,
					height:240,
					position: 'center',
					buttons: [	
						{text: "Confirm", 
						"class": 'fl btn-custom-blue-glomp_xs w100px _send_glomp_m',
						click: function() {
							submitGlomp();
							
						}},
						{text: "Cancel", 
						"class": 'fr btn-custom-ash_xs w100px',
						click: function() {
							$(this).dialog("close");
							$('#glomp_form').dialog('destroy').remove();							
						}}
					]					
				});
				NewDialog.dialog('open');	
				
				$( "#glomp_form" ).submit(function( event ) {									
					submitGlomp();				
					event.preventDefault();
				});
				 $('textarea').blur();
				 $('input').blur();
				
				});
				$("#tour_cancel").click(function() {
					/*checkIfInTour();*/
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';					
				});	
				$("#tour_done").click(function() {
					/*checkIfInTour();*/
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';					
				});	
				$(".menu_and_point_wrapper").click(function() {
					var id = $(this).attr('id');
					var merchant	=$('#prod_'+id+'_merchant').html();
					var item		=$('#prod_'+id+'_item').html();
					var img			=$('#prod_'+id+'_img').html();
					var desc		=$('#prod_'+id+'_desc').html();
					var point		=$('#prod_'+id+'_point').html();
					
					var NewDialog = $('<div id="" align="center"> \
												<div class="cl fl menu_item_photo_wrapper_2">\
													<img class="menu_item_photo_2" alt="'+item+'" src="'+img+'"></img>\
												</div>\
												<div class="fl menu_item_details">\
													<div class="menu_merchant white">'+merchant+'</div>\
													<div class="menu_product">'+item+'</div>\
													<div class="">'+desc+'</div>\
													<div class="fl menu_item_points_wrapper_2" align="center">'+point+'</div>\
												</div>\
											</div>');
											
					NewDialog.dialog({
					autoOpen: false,
					resizable: false,
					dialogClass:'dialog_style_item_details',
					title:'',
					modal: true,
					position: 'center',
					width:300,
					height:145
				});
				NewDialog.dialog('open');
				
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
			
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
			$( "#glomp_form" ).submit(function( event ) {
				alert("asd");
				event.preventDefault();
			});
		
            jQuery(document).on('click','.menu_header .user-icon .user-image', function(e){
                jQuery('.menu_header .user-icon .user-details').toggle();
            });
        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        
    </body>
</html>