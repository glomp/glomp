<?php
switch ($public_alias) {
        case 'hsbc':
            $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
            $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  HSBC_BRAND_PROD_ID_KPO;
            $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona = '0';
            $laurent_perrier =  '0';
            $providore =  '0';
            $glenfiddich =  '0';
            $moet_chandon =  '0';
            $kusmi_tea =  '0';
            $euraco = '0';
            break;
        case 'uob':
            $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
            $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  UOB_BRAND_PROD_ID_KPO;
            $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
            $laurent_perrier =  '0';
            $providore =  '0';
            $glenfiddich =  '0';
            $moet_chandon =  '0';
            $kusmi_tea =  '0';
            $euraco = '0';
            
            break;
        case 'dbs':
            $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
            $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  DBS_BRAND_PROD_ID_KPO;
            $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
            $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
            $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
            $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
            $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
            $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
            $euraco = DBS_BRAND_PROD_ID_EURACO;
            break;

        case 'amex':
            $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
            $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  AMEX_BRAND_PROD_ID_KPO;
            $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
            $laurent_perrier =  '0';
            $providore =  '0';
            $glenfiddich =  '0';
            $moet_chandon =  '0';
            $kusmi_tea =  '0';
            $euraco = '0';
            break;
    }


    if(!defined('BRAND_PROD_ID_MACALLAN'))     define('BRAND_PROD_ID_MACALLAN',$macallan);
    if(!defined('BRAND_PROD_ID_SNOW_LEOPARD')) define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
    if(!defined('BRAND_PROD_ID_LONDON_DRY'))   define('BRAND_PROD_ID_LONDON_DRY',$london);

    define('BRAND_PROD_ID_SINGBEV',$singbev);
    define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
    define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

    define('BRAND_PROD_ID_KPO',$kpo);
    define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
    define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
    define('BRAND_PROD_ID_VALRHONA',$valrhona);
    define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
    define('BRAND_PROD_ID_PROVIDORE',$providore);
    define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
    define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
    define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
    define('BRAND_PROD_ID_EURACO',$euraco);
?>
<?php
	$user_id = $this->session->userdata('user_id');


	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	$header_text = '';

?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo ucfirst($brand->public_alias);?>| glomp! mobile </title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
		<script type="text/javascript">
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var items = [], items_options = [];
        </script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
								<?php
									if(
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url(MOBILE_M.'/brands/view/'.$brand->public_alias) ?>" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('Back','Back'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                $public_alias_address  =$brand->public_alias;
                                if($brand->public_alias=='hsbc' || $brand->public_alias =='uob' || $brand->public_alias =='dbs')
                                    $public_alias_address  .='sg';
                                else if($brand->public_alias =='amex')
                                    $public_alias_address  .='-sg';


                                ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/'.$public_alias_address) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo ucfirst($brand->public_alias);?> <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
								<?php
									if(
										$this->session->userdata('user_id') =='' &&
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url() ?>brands/logout/<?php echo $public_alias_address;?>/m" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('exit','Exit'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                            </ul>

                        </nav>
                    </div>
                    <?php
                        switch ($brand_product_id) {
                            case WINESTORE_BRAND_PRODUCT_ID:
                                $header_text  = 'Winestore';
                                break;
                            case MACALLAN_BRAND_PRODUCT_ID:
                                $header_text  = 'The Macallan';
                                break;


                            case BRAND_PROD_ID_MACALLAN:
                                $header_text  = 'The Macallan';
                                break;
                            case BRAND_PROD_ID_SNOW_LEOPARD:
                                $header_text  = 'Snow Leopard';
                                break;
                            case BRAND_PROD_ID_LONDON_DRY:
                                $header_text  = 'No.3 London Dry Gin';
                                break;
                            case BRAND_PROD_ID_SINGBEV:
                                $header_text  = 'SingBev';
                                break;

                            case BRAND_PROD_ID_SWIRLS_BAKESHOP:
                                $header_text  = 'Swirls Bakeshop';
                                break;
                            case BRAND_PROD_ID_DAILY_JUICE:
                                $header_text  = 'Daily Juice';
                                break;
                            case BRAND_PROD_ID_KPO:
                                $header_text  = 'KPO';
                                break;
                            case BRAND_PROD_ID_NASSIM_HILL:
                                $header_text  = 'NASSIM HILL';
                                break;
                            case BRAND_PROD_ID_DELIGHTS_HEAVEN:
                                $header_text  = 'The Delights Heaven';
                                break;
                            case BRAND_PROD_ID_VALRHONA:
                                $header_text  = 'Valrhona';
                                break;
                            case BRAND_PROD_ID_LAURENT_PERRIER:
                                $header_text  = 'Laurent Perrier';
                                break;
                            case BRAND_PROD_ID_PROVIDORE:
                                $header_text  = 'The Providore';
                                break;
                            case BRAND_PROD_ID_GLENFIDDICH:
                                $header_text  = 'Dlenfiddich';
                                break;
                            case BRAND_PROD_ID_MOET_CHANDON:
                                $header_text  = 'Moet & Chandon';
                                break;
                            case BRAND_PROD_ID_KUSMI_TEA:
                                $header_text  = 'Kusmi Tea';
                                break;
                            case BRAND_PROD_ID_EURACO:
                                $header_text  = 'Euraco';
                                break;
                        }
                    ?>
                    <div style="font-size: 24px;color: white;padding: 8px;"><?php echo $header_text; ?></div>
                </div>
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>


            
            
            


<style type="text/css">
    .brand-page-header,
    .brand-page-logo,
    .brand-page-description {
        position: relative;
    }
    .brand-page-title {
        position:absolute;
        top:10%;
        right:0;
        background: #5A606C;
        -webkit-border-top-left-radius: 10px;
        -webkit-border-bottom-left-radius: 10px;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-bottomleft: 10px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        padding:10px 40px 10px 15px;
    }
    .brand-page-title h1 {
        margin:0;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
    }
    .brand-page-logo {
        padding: 0 20px;
        max-width: 100%;
    }
    .brand-page-logo img {
        width:100%;
    }
    .brand-page-description {
        padding: 0 20px;
        text-align: justify;
    }
    .brand-page-products-link {
        padding: 0 20px;
    }
    .brand-page-products-link a {
        background: #5A606C;
        padding:8px 20px;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
        display: inline-block;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    .brand-page-products-link a:active {
        background: #777;
        text-decoration: none;
    }
    .brand-page .page-line {
        margin-top:20px;
        position: relative;
    }
    .brand-section {
        margin-bottom:2px;
    }

    .product-list:after {
        content: " ";
        display:block;
        clear: left;
    }
    .product-list a.product {
        width:47%;
        margin-left: 2%;
        float: left;
        background: #ddd;
        margin-bottom: 2%;
    }
    .product-list a.product img {
        width:100%;
        background: #222;
        border:0;
    }
    .product-list a.product p {
        margin:0;
        padding:5px 10px;
        color: #555;
    }
    div.product {
    }
    div.product .product-image {
        text-align: center;
    }
    div.product .product-image img {
        width: 96%;
    }
    div.product .product-info {
        padding: 0 20px;
    }
    div.product .product-info h3 {
        margin:0;
        padding: 0.6em 0;
        font-size:1.4em;
    }
    div.product-glomp {
        padding: 20px;
    }
    div.product-glomp a {
        display:block;
        background: #A8CF3E;
        padding: 5px;
        text-align:center;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
    }
    div.product-glomp a img {
        height:2em;
    }
    .btn-back {
        position:absolute;
        top:35%;
        right:1em;
        padding:5px 20px;
        background:#5A606C;
        color:#eee;
        font-weight:bold;
        font-size: 1.2em;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
    }
    div.product-merchants {
    
    }
    div.product-merchants a {
        float: left;
        width: 24%;
        margin-left: 4%;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
        -webkit-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        -moz-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
        overflow: hidden;
    }
    div.merchants-list {
        overflow: auto;
    }
    div.product-merchants a img {
        width: 100%;
    }
    div.search-info {
        margin-bottom: 20px;
    }
    div.search-info h4 {
        font-size: 0.9em;
        text-transform: uppercase;
        margin:0;
        padding: 5px;
        background: #5A606C;
        color: #eee;
    }
    div.search-info .search-data {
        padding:5px;
        background: #bfc4ce;
    }
    div.search-info .search-data:after {
        content: "";
        display:block;
        clear:left;
    }
    div.search-info .search-data img {
        float:left;
        width: 25%;
    }
    div.search-info .search-data p {
        float:left;
        margin-left:5px;
    }
    
    .btn-custom-blue_xs a {
        color: white;
        text-decoration: none;
    }
    
    .col-xs-6 {padding: 0px !important}
    .menu_item_points_wrapper_amex {margin: 5px 0px 10px 8px; !important}
    .menu_item_description_wrapper { padding: 10px 0 10px; min-height: 63px; }
    .menu_item_points_wrapper_amex { margin: 5px 0px 10px; }
    .menu_item_photo_wrapper {margin-left: -20px; }
    .menu_and_point_wrapper {position: relative} 
</style>
            <div class="page brand-page">
                
                <div style="padding:10px">
					<a href="<?php echo site_url(MOBILE_M.'/brands/cart/'.$brand->public_alias);?>" >
						<div style="float:right;" class="btn-custom-blue-grey_xs" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty_'.$brand->public_alias)=='' ? 'class="gray">0': 'class="red">'.$this->session->userdata('session_cart_qty_'.$brand->public_alias);?></span>)</div>
					</a>
				</div>
				<!--
                <div class="page-line default-header" style="clear:both">
                    
					
                    <div class="brand-page-logo" style="width:100%">
                        <img src="<?php echo site_url();?>custom/uploads/brands/products/<?php echo $brandproducts[0]->image_logo;?>" alt="<?php echo  $brandproducts[0]->name;?>">
					   
                    </div>
					
                </div>
				
				<hr>
				-->

               

                    
                    <div class="page-line cl body_bottom_1px merchant-products" style="background:#fff;">
                        <div class="container p5px_0px global_margin">
                            
							<div class="row">
							<?php
							if($brand_product_id == WINESTORE_BRAND_PRODUCT_ID)
							{
							
								$list = get_winestore('list');
								$x = 0;
								foreach($list as $key=>$l):
									$x++;
							?>		
									<div class="row">
										<div class="col-xs-12" style="background-color: #DEE5ED;padding: 15px;border-radius: 10px;margin: 10px 30px 10px 0px;color: #646B78;overflow: hidden;position: relative;">
                                                                                    <table width ="100%">
                                                                                        <tr>
                                                                                            <td width ="98%"><?php echo $l['name'];?></td>
                                                                                            <td width ="2%" valign="top" style="padding-left: 2px;"><div class="btn-custom-blue_xs"><a href="<?php echo site_url('m/brands/info/'.$key); ?>"/>i</div></td>
                                                                                        </tr>
                                                                                    </table>
										</div>
									</div>
									<div class="row">
									<?php foreach($merchant_products as $product):
										$d = get_winestore_prod_data($product->product_id);
										if($d['group']==$x)
										{
									?>
											<div class="col-xs-6 menu_and_point_wrapper" id="<?php echo $product->product_id;?>" style="border:0px solid #333;margin-bottom:20px;">
												<script type="text/javascript">
													items[<?php echo $product->product_id;?>] =<?php echo json_encode($product);?>;
												</script>
												<div class="fl " style="width:100%">
													<div class="fl menu_item_photo_wrapper">
														<img style="width:72px;height:72px;" align="left" class="menu_item_photo" id="prod_<?php echo $product->product_id;?>_img" src="/custom/uploads/products/thumb/<?php echo $product->prod_image?>" />
													</div>
													<div class="fr menu_item_points_wrapper_amex" id="prod_<?php echo $product->product_id;?>_point" style="position: absolute; right: 15px;  z-index: 1;">
													<span style="font-size:10px;">2&minus;3 bottles S$ <?php echo number_format(($d['a_20']  ),2);?></span>
													<br>
													<span style="font-size:10px;">4+ bottles S$ <?php echo number_format(($d['a_30']  ),2);?></span>
													</div>
													<div class="cl menu_item_description_wrapper">
														<div class="menu_merchant" id="prod_<?php echo $product->product_id;?>_merchant"><?php echo $product->merchant_name?></div>
														<div class="menu_product" id="prod_<?php echo $product->product_id;?>_item" style="width:100% !important;border:solid 0px #333;"><?php echo $product->prod_name?></div>
														<div class="" style="display:none" id="prod_<?php echo $product->product_id;?>_desc" ><?php echo $product->prod_details;?></div>
													</div>
												
												<div class="cl">
													<div align="left">
													<span style="color:#000">Qty: <select name="quantity" autocomplete="off" id="quantity_<?php echo $product->product_id;?>" class="cart_qty_2" style="width:55px;">
														<option value="1" selected="">1
														</option>
														<option value="2">2
														</option>
														<option value="3">3
														</option>
														<option value="4">4
														</option>
														<option value="5">5
														</option>
														<option value="6">6
														</option>
													</select>
													</div>
													<div align="left" style="padding:6px 0px;">
														<button id="cart_<?php echo $product->product_id;?>" data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" data-id="<?php echo $product->product_id;?>" class="btn-custom-green_normal glompItemMerchant" style="padding:3px 6px !important">Add to Cart</button>
													</div>
												</div>
												</div>
											</div>
									<?php } endforeach;?>
									</div>
								<?php endforeach;?>
							</div>
							<?php
							}
							else
							{
							?>
							<div class="row">
								<?php foreach($merchant_products as $product):
								?>
								<script type="text/javascript">
									items[<?php echo $product->product_id;?>] =<?php echo json_encode($product);?>;
								</script>
									<div class="col-xs-6 menu_and_point_wrapper" id="<?php echo $product->product_id;?>" style="border:0px solid #333;margin-bottom:20px;">
										<div class="fl" style="width:100%">
											<div class="fl menu_item_photo_wrapper" style="margin-left:0px;">
                                            <?php
                                                $src = "http://placehold.it/64x64";
                                                if($product->prod_image !="" )
                                                    $src = site_url('custom/uploads/products/thumb/'.$product->prod_image);

                                                $prod_cost=$product->prod_merchant_cost;
                                                $has_option = "no";
                                                $prod_data="";
                                                if($brand_product_id !=MACALLAN_BRAND_PRODUCT_ID)
                                                {
                                                    $prod_data = get_hsbc_prod_data($product->product_id);
                                                    $prod_cost = $prod_data["sale_price"];
                                                    $has_option= $prod_data["has_options"];
                                                }
                                            ?>
                                            <script type="text/javascript">
                                                items_options[<?php echo $product->product_id;?>] =<?php echo json_encode($prod_data);?>;
                                            </script>
												<img style="width:64px;height:64px" id="prod_<?php echo $product->product_id;?>_img" class="menu_item_photo" src="<?php echo $src;?>" />
                                                <?php
                                                    $sold_out = is_sold_out($product->product_id);
                                                ?>
                                                <?php
                                                if($sold_out)
                                                {
                                                    ?>
                                                    <img style="width:65px;margin-left:-65px;background-color:rgba(255,255,255,0.4)" src="<?php echo site_url();?>assets/images/sold-out-m.PNG" />
                                                    
                                                    <?php
                                                }
                                                ?>
											</div>
											<div class="fl menu_item_points_wrapper_amex" id="prod_<?php echo $product->product_id;?>_point" style="position: absolute;right: 15px;  z-index: 1;">S$&nbsp;<?php echo number_format(($prod_cost  ),2);?></div>
											<div class="cl menu_item_description_wrapper">
												<div class="menu_merchant" id="prod_<?php echo $product->product_id;?>_merchant"><?php echo $product->merchant_name?></div>
												<div class="menu_product" id="prod_<?php echo $product->product_id;?>_item" style="width:100% !important;border:solid 0px #333; height: 44px"><?php echo $product->prod_name?></div>
												<div class="" style="display:none" id="prod_<?php echo $product->product_id;?>_desc" ><?php echo $product->prod_details;?></div>
											</div>
										
										<div class="cl">
											<div align="left">
											<span style="color:#000">Qty:</span> <select name="quantity" autocomplete="off" id="quantity_<?php echo $product->product_id;?>" class="cart_qty_2" style="width:55px;"
                                                <?php echo ($sold_out) ? 'disabled':'';?>
                                                >
												<option value="1" selected="">1
												</option>
												<option value="2">2
												</option>
												<option value="3">3
												</option>
												<option value="4">4
												</option>
												<option value="5">5
												</option>
												<option value="6">6
												</option>
												<option value="7">7
												</option>
												<option value="8">8
												</option>
												<option value="9">9
												</option>
												<option value="10">10
												</option>
											</select>
											</div>
											<div align="left" style="padding:6px 0px;">
												<button id="cart_<?php echo $product->product_id;?>" 
                                                        data-public_alias="<?php echo $brand->public_alias;?>" 
                                                        data-prod_merchant_cost="<?php echo $prod_cost;?>" 
                                                        data-has_option="<?php echo $has_option;?>" 
                                                        data-id="<?php echo $product->product_id;?>" 
                                                        class="btn-custom-green_normal glompItemMerchant" 
                                                        <?php echo ($sold_out) ? 'disabled':'';?>
                                                        style="padding:3px 6px !important;<?php echo ($sold_out) ? 'background-color:#ddd !important; border-color:#ddd !important':'';?>"
                                                        >Add to Cart</button>
											</div>
										</div>
										</div>
									</div>
									
									
									
								<?php endforeach;?>
							</div>
							<?php
							}
							?>
							
                        </div>

                       
                    
                
                
                
                </div>
            </div>


			<!--body-->

				<div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/m/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <div id="fb-root"></div>
        <script>
            var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
            <?php
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) {
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            
            var GlompFormDialog = jQuery('form#GlompFormDialog').dialog({
                autoOpen: false,
                dialogClass : 'dialog_style_glomp',
                width: '80%',
                modal: true,
                buttons: [
                    {
                        text: 'Confirm',
                        class: 'fl btn-custom-blue-glomp_xs w100px',
                        click: function() {
                            GlompFormDialog.submit();
                        },
                        style: 'margin:1px;'
                    },
                    {
                        text: 'Cancel',
                        class: 'fr btn-custom-ash_xs w100px',
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }
                ]
            });
            
            var GlompFormAlert = jQuery('<div class="form-alert" />').dialog({
                autoOpen: false,
                width: '70%',
                modal: true,
                dialogClass: 'dialog_style_glomped_alerts',
                minHeight: 'auto'
            });
            
            var GlompFormPreload = jQuery('<div class="form-preload" />').dialog({
                autoOpen: false,
                width: '60%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait',
                title: 'Please wait...',
                minHeight: 'auto'
            }).html('<div style="text-align:center"><img width="40" src="/assets/m/img/ajax-loader.gif" /></div>');
            
            var GlompShareDialog = jQuery('#share-dialog').dialog({
                autoOpen: false,
                width: '90%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                minHeight: 'auto'
            });
            
            $(function() {

                $('#back_btn').click(function(e) {
                   e.preventDefault();
                   window.history.back();
                });
                $("#tour_cancel").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
				$("#tour_done").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
				
				var text="";
				<?php
					switch ($brand_product_id) {
                        case WINESTORE_BRAND_PRODUCT_ID:
                            echo '
                                text ="Welcome to this special offer for great NZ wines. Choose either a mix of wines here or select a multiple of your favourite choice. Minimum order of 2 bottles. Enjoy 20% off for 2-3 bottles and 30% off for 4 or more bottles. Delivery in Singapore only is complementary.";
                            ';
                            break;
                        case MACALLAN_BRAND_PRODUCT_ID:
                            echo '
                                text ="Welcome to this special offer for The Macallan Highland Single Malt Scotch Whisky. Enjoy 15% off Recommended Retail Prices. Delivery in Singapore only is complementary.";
                            ';
                            break;
                        case BRAND_PROD_ID_MACALLAN:
                        case BRAND_PROD_ID_SNOW_LEOPARD:
                        case BRAND_PROD_ID_LONDON_DRY:
                            echo '
                                text ="Welcome to this very special offer for The Macallan Highland Single Malt Scotch Whisky. Enjoy up to 25% off Recommended Retail Prices. Delivery in Singapore is complementary for combined orders of $300 and above of The Macallan whiskies, Snow Leopold Vodka and No.3 London Dry Gin. Orders below $300 can be collected during office hours at Asia Square.";
                            ';
                            break;
                        case BRAND_PROD_ID_SINGBEV:
                            echo '
                                text ="Welcome to this very special offer for a wide variety of renowned alcoholic beverages. Enjoy up to 30% off Recommended Retail Prices. Delivery in Singapore is complementary for combined orders of $350 and above from SingBev. A $35 delivery fee applies for smaller orders.";
                            ';
                            break;
                        case BRAND_PROD_ID_SWIRLS_BAKESHOP:
                            echo '
                                text ="Welcome to this very special offer for delicious gourmet cupcakes from Swirls Bakeshop. Enjoy 10% off a wide variety of fun and delicious flavours. Pick up at 2 shop locations or have them delivered to you for $16.";
                            ';
                            break;
                        case BRAND_PROD_ID_DAILY_JUICE:
                            echo '
                                text ="Welcome to this very special offer for healthy, revitalising juice packages from Daily Juice. Enjoy 10% off the retail price. Complimentary delivery in Singapore.";
                            ';
                            break;
                        case BRAND_PROD_ID_DELIGHTS_HEAVEN:
                            echo '
                                text ="Welcome to this very special offer from The Delights Heaven. Enjoy 15% off a special Premium Gift box of macaroons. Pick up at our shop at Bugis Cube or have them delivered to you for $25.";
                            ';
                            break;
                        case BRAND_PROD_ID_KPO:
                            echo '
                                text ="";
                            ';
                            break;
                        case BRAND_PROD_ID_NASSIM_HILL:
                            echo '
                                text ="";
                            ';                        
                            break;
                        case BRAND_PROD_ID_VALRHONA:
                            echo '
                                text ="Welcome to this very special offer from Valrhona Gourmet chocolates. Enjoy 28% off Recommended Retail Prices for a selection of Gift boxes. Delivery in Singapore is complementary for orders of $300 and above. Orders below $300 can be delivered for $35 or collected during office hours at Euraco Finefood in Henderson Road.";
                            ';
                            break;
                        case BRAND_PROD_ID_LAURENT_PERRIER:
                            echo '
                                text ="Welcome to this very special offer for Laurent Perrier champagne with special gift box. Enjoy over to 20% off Recommended Retail Prices. Delivery in Singapore is complimentary for orders of 3 bottles or above otherwise a delivery charge of $20 applies. Whilst stocks last.";
                            ';
                            break;
                        case BRAND_PROD_ID_PROVIDORE:
                            echo '
                                text ="Welcome to this very special offer for The Providore hampers. Enjoy 13% off these quality hampers from The Providore. Delivery in Singapore is free on all orders above $200 otherwise a delivery fee of $20 applies. Otherwise, please pick up your purchases at any of The Providore stores. Please go to www.theprovidore.com/store/ for address and opening hours.";
                            ';
                            break;
                        case BRAND_PROD_ID_GLENFIDDICH:
                            echo '
                                text ="Welcome to this exclusive offer for this Glenfiddich 18 Year Old Scotch Whisky Limited Edition Gift Set. Delivery in Singapore is complementary. Whilst stocks last.";
                            ';
                            break;
                        case BRAND_PROD_ID_MOET_CHANDON:
                            echo '
                                text ="Welcome to this exclusive for this Moet & Chandon Ice Imperial, Customized Bottle with Swarovski components. Delivery in Singapore is complementary. Whilst stocks last.";
                            ';
                            break;
                        case BRAND_PROD_ID_KUSMI_TEA:
                            echo '
                                text ="Welcome to this special offer for Kusmi Tea Paris. These fantastic gourmet tea sets are newly launched in Singapore. Delivery in Singapore is complementary.";
                            ';
                            break;
                        case BRAND_PROD_ID_EURACO:
                            echo '
                                text ="Welcome to this special offer for Euraco Finefood hampers. Enjoy these fantastic gourmet hampersat 15% off recommended retail prices. Delivery in Singapore is complementar for orders of $300 or over. A $30 delivery fee is applicable otherwise.";
                            ';
                            break;
                    }
				?>
				alert_winer_welcome_pop(text);
				
				
				
				$(".menu_and_point_wrapper").click(function() {
					var id = $(this).attr('id');
					var merchant	=$('#prod_'+id+'_merchant').html();
					var item		=$('#prod_'+id+'_item').html();
					var img			=$('#prod_'+id+'_img').attr('src');
					var desc		=$('#prod_'+id+'_desc').html();
					var point		=$('#prod_'+id+'_point').html();
					
					var NewDialog = $('<div id="" align="center" style="margin:0px 6px !important;"> \
												<p class="" style="margin:6px !important;">\
													<img style="width:72px;height:72px;margin-right:8px;margin-bottom:4px;" align="left" class="menu_item_photo" src="'+img+'" />\
													<span class="menu_merchant white">'+merchant+'</span><br>\
													<span class="menu_product">'+item+'</span>\
													<p class="" style="text-align:justify;margin-top:10px;padding-bottom:10px;" 	>'+desc+'</p>\
												</p>\
											</div>');
											
					NewDialog.dialog({
					autoOpen: false,
					resizable: false,
					dialogClass:'dialog_style_item_details',
					title:'',
					modal: true,
					position: 'center',
					width:300,
				});
				NewDialog.dialog('open');	
				
				});
				
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});

            jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
                jQuery('.user-menu .user-icon .user-details').toggle();
            });
            
            jQuery(document).on('click','.toggle',function(e){
                e.preventDefault();
                toggleIn = jQuery(e.target).attr("toggle-in");
                toggleOut = jQuery(e.target).attr("toggle-out");
                
                jQuery(toggleIn).show();
                jQuery(toggleOut).hide();
            });

        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
		<script src="<?php echo minify('assets/m/js/custom.glomp_amex.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
</html>
