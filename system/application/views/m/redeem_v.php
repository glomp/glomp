<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">
		<link href="<?php echo base_url() ?>assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background:#DEE6EB;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/glomp/') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('back'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>
                    </a>
                </div>
            </div>

            <!-- hidden navigations     
            <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav">
<?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>                
				<div class="cl hidden_nav_seperator fl w200px"></div>				
				<a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/glomp/') ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('back'); ?>
                    </div>
                </a>                
            </div>
            <!-- hidden navigations -->			
			<!--body-->     
            <div class="container p5px_0px global_margin " style="text-align:justify ">
            	
			<!-- Voucher Details-->	
			<?php
				$res_product = $this->product_m->productAndMerchant($prod_id);
				$row_product = $res_product->row();
				$prod_image = $this->custom_func->product_logo($row_product->prod_image);
			?>
			<div class="cl container p5px_0px">
                <div style="border:solid 12px #595F6D;border-top:solid 10px #595F6D;border-radius:5px;background-color:#fff;padding:5px 3px;">
                    <div class="row" stlye="display:table">                                            
                        <div style="display:table-row">
                        <div class="" style="display:table-cell;width:30%;padding:5px 8px;vertical-align:top">
                            <div class="menu_item_photo_wrapper">
                                <img class="redeem_prod_photo" style="width:100%;max-width:150px;margin-bottom:10px;" alt="<?php echo $row_product->prod_name;?>" src="<?php echo base_url().$prod_image;?>"></img>
                            </div>
                            <div class="redeem_merchant_text"  ><?php echo $row_product->merchant_name;?></div>
                        </div>
                        <div class="" style="width:70%;display:table-cell;border-left:solid 1px #595F6D; display:table-cell;vertical-align:top">
                            <div class="redeem_prod_text" style="padding:0px 8px 15px 8px;margin:0px;font-size:18px;text-align:left;"><b><?php echo $row_product->prod_name;?></b></div>
                            <div class="redeem_prod_text" style="padding:5px 8px;" ><?php echo $row_product->prod_details;?></div>
                        </div>
                        </div>
                    </div>
                </div>				
			</div>
			<!-- Voucher Details-->	
			
			<!-- Merchant  terms-->	
			<div class="cl container p5px_0px " style="background-color:#6D8BA5;border-radius:5px;padding:10px 12px 14px 12px;color:#fff">                            
                <div class="cl" id="term_button">
                    <div class="fl" style="font-size:14px;font-weight:bold" >
                        Terms & Conditions<?php //echo $this->lang->line('termNcondition');?>
                        <em style="font-size:12px;font-weight:normal">(Please read)</em>
                    </div>
                    <div class="fr"  style="background:#fff;width:24px;height:24px;border-radius:24px;cursor:pointer" align="center">
                       <span class="glyphicon glyphicon-chevron-down" style="color:#595F6D;font-size:16px;margin-top:3px;margin-left:0px"></span> 
                    </div>
                </div>
                <div class="cl" id="term_content" style="display:none;padding:10px 0px 5px 0px;font-size:12px;font-weight:normal">
                    <?php //if($row_product->merchant_terms!=0 && $row_product->merchant_terms!="") {
						echo stripslashes($row_product->merchant_terms);
						
					 ?>                     
                </div>
            </div>
            <!-- Merchant  terms-->	
            <!-- Glomp Notes-->				
			<div class="cl container p5px_0px bold" style="margin:5px 0px;background-color:#595F6D;border-radius:5px;padding:10px 12px 14px 12px;color:#F42F8F">
				<div style="text-align:justify ">
					<?php echo $this->lang->line('redeem_note');?>			
				</div>					
			</div>
			<!-- Glomp Notes-->	
           
			<!-- Glomp Form-->	
            <form method="post" action="<?php echo base_url(MOBILE_M).'/redeem/validating/'.$voucher_id?>" name="redeem_form" id="redeem_form">
            <div class="cl container p5px_0px " style="background-color:#6D8BA5;border-radius:5px;padding:10px 12px 14px 12px;color:#fff">                            
                <div class="cl">
                     <div class="fl" style="width:35%;font-size:14px;font-weight:bold" >
                        Outlet&nbsp;Code                        
                    </div>
                    <div class="fr" style="width:65%;">
                        <input type="tel" class="redeem_input fr" style="" name="outlet_code"  id="outlet_code">
                    </div>
                </div>
            </div>
            <div class="cl p5px_0px" align="center">
                <button type="button" id="validate" class="btn-custom-blue-grey_normal w200px" style="font-size:20px !important;"  >Validate</button>
            </div>	
            </form>		
			<!-- Glomp Form-->			
				<div class="cl container p5px_0px gray" style="text-align:justify ">
					<div class="" align="left">
						
							<div class="cl  p2px_0px" align="left">
								
								
							</div>
							<!--<div class="cl fl  p2px_0px" align="left">
								<span class="redeem_input_label fl" style="width:110px !important;" >glomp! Password</span>
								<input type="password" class="redeem_input fl" style="width:150px !important;" placeholder="glomp! Password" name="glomp_password" id="glomp_password">
							</div>
							<div class="cl fl  p2px_0px" align="left">
								<span class="redeem_input_label fl" style="width:120px !important;" >&nbsp;</span>
								<a href="<?php echo base_url(MOBILE_M).'/landing/forgotPassword/';?>" class="fl forgot_pass gray">Forgot Password?</a>
							</div>-->
                            <div class="cl p2px_0px" align="center"></div>	
							
										
					</div>
				</div>
			
			<!-- Glomp Form-->
			<div class="cl p20px">	</div>								
			
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  
        <div id="waitPopup" align="center" style="display:none;">
            <div align="center" style="margin-top:-5px;"><strong>Please wait...</strong></div>
            <div align="center" style="margin-top:5px;"><img src="<?php echo base_url() ?>assets/images/ajax-loader-1.gif" /></div>
        </div>
        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script>
            (function($) {
                $.fn.onEnter = function(func) {
                    this.bind('keypress', function(e) {
                        if (e.keyCode == 13) func.apply(this, [e]);    
                    });               
                    return this; 
                 };
            })(jQuery);
            $(document).ready(function(e)
            {
                $("#term_button").click(function() {
                    $('#term_content').toggle("slide", { direction: "up" }, 500);                
                });
                var isAndroid = navigator.userAgent.match(/android/i) ? true : false;
                var isIOS = navigator.userAgent.match(/(ipod|ipad|iphone)/i) ? true : false;

                if(isIOS)
                    $('#outlet_code').attr("type", "tel");
                if(isAndroid)
                {
                    $('#outlet_code').attr("type", "number");
                }
            });
            $(function() {
                var waitPopUp = $('#waitPopup');
                
                waitPopUp.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomped_alerts',
                        title:'',
                        modal: true,
                        position: 'center',
                        width:200,
                        height:120
                });
                
                 $("#outlet_code").onEnter( function(e) {
                    /*$(this).val("Enter key pressed"); */
                    e.preventDefault();                    
                    $("#validate").click();
                }).focus( function(e) {
                    
                });
                
                $( "#redeem_form" ).submit(function( event ) {									
					/*$("#validate").click();*/
					/*event.preventDefault();*/
				});
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
				});
				$("#validate").click(function(e) {
					waitPopUp.dialog('open');
					$.ajax({
						type: "POST",
						url: '<?php echo base_url(MOBILE_M).'/redeem/voucher/'.$voucher_id?>',
						data: $('#redeem_form').serialize(),
						cache: false,
						dataType:'json',
						success: function(response){                                
                                                        waitPopUp.dialog('close');
							if(response.status=='Ok')
							{
								$("#redeem_form").submit();
								/*window.location.href = '<?php echo base_url(MOBILE_M).'/redeem/validating/'.$voucher_id?>';*/
							}
							else
							{
								var NewDialog = $('<div id="redeemPopup" align="center">\
																<div align="center" style="margin-top:-5px;"><strong>'+response.status+'</strong></div>\
																<div align="center" style="margin-top:5px;">'+response.message+'</div></div>');																
								NewDialog.dialog({						
								autoOpen: false,
                                position: 'center',
								resizable: false,					
								dialogClass:'dialog_style_glomped_alerts',
								title:'',
								modal: true,
								width:180,
								height:120,
								buttons: [						
									{text: "Cancel", 
									"class": 'btn-custom-blue-grey_xs',
									click: function() {
										$(this).dialog("close");
										setTimeout(function() {
											$('#redeemPopup').dialog('destroy').remove();
										}, 500 );						
									}}
								]
							});
							NewDialog.dialog('open');
							}
						}
					});		
					return false;
					
				});	
				
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
    </body>
    <?php //include_once("includes/node.php") ?>
</html>
