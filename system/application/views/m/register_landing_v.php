<!DOCTYPE html>
<html>
    <head>
        <title>Sign up | glomp! mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="Sign up on glomp!. Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
        <meta name="keywords" content="<?php echo meta_keywords();?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
		<style>
		.w120px {
				width:180px !important;
				margin-left:10px !important;
			}
		 </style>
    </head>    
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" align="center">	            
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        Back
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp_register.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations       
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav" align="left">
                            Back
                        </div>
                    </a>                    
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="container p40px" style="background-color: white;">
                <div class="row font-22px" style="margin-top: 70px;" >
                    <a class="btn-lg btn-custom-blue_fb p40px c_pointer btn-block" name="Login_Facebook" id="Login_Facebook" style="font-size:16px;">
                        <img alt="Facebook icon" style="width: 37px;padding-right: 10px;" src="<?php echo base_url('assets/m/img/fb-icon.png') ?>" />Login with Facebook
                    </a>
                </div>
                <div class="row p20px" style="color:#59606b;font-size: 14px">Logging in with Facebook account is the easiest way to register!</div>
                <div class="row font-22px" style="margin: 20px 0px;" >
                    <a class="btn-lg btn-custom-blue_li p40px c_pointer btn-block" name="Login_linkedIn" id="Login_linkedIn" style="font-size:16px;">
                        <img alt="LinkedIn icon" style="width: 37px;padding-right: 10px;" src="<?php echo base_url('assets/images/icons/li-icon.jpg') ?>" />Login with linkedIn
                    </a>
                </div>
                <div class="row font-22px">
                    <a href="<?php echo site_url(MOBILE_M.'/landing/register_form'); ?>" class="btn-lg btn-custom-blue-grey-background-only btn-block" type="submit" style="font-size:16px;">Manually Register</a>
                </div>
            </div> 
            <!-- /container -->
            <!-- /footer -->
            <div class="footer_wrapper"> 
            </div>
            <!-- /footer -->
        </div> <!-- /global_wrapper -->
		<!-- ui-dialog -->
		<div id="dialog" title="">
			<div id="dialog-content" style="line-height:120%; font-size:14px" align="center"></div>
		</div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
		<div id="fb-root"></div>    
        <script type="text/javascript">   
		var FB_APP_ID="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
		var LOCATION_REGISTER="<?php echo base_url('index.php/landing/register');?>";
		var SIGNUP_POPUP_TEXT="<?php echo $this->lang->line('signup_popup_text');?>";
		var SIGNUP_WITH_FB_BUT_REG="<?php echo $this->lang->line('signup_with_fb_but_already_registered');?>";
		var GLOMP_BASE_URL									="<?php echo base_url('');?>";
                var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
		var firstRun=true;
		var fbApiInit = false;
		window.fbAsyncInit = function() {
		  FB.init({
			appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
			status     : true, /* check login status*/
			cookie     : true, /* enable cookies to allow the server to access the session*/
			xfbml      : true  /* parse XFBML	  		*/
		});		
		 
			FB.Event.subscribe('auth.authResponseChange', function(response) {			
				if (response.status === 'connected') {
				  /* The response object is returned with a status field that lets the app know the current
				   login status of the person. In this case, we're handling the situation where they 
				   have logged in to the app.			*/
				 if(!firstRun){
					/*getUserDetailsAndDoPost();			*/
				}
				  else	{	  		
					firstRun=false;			  				
					}
				} else if (response.status === 'not_authorized') {
				  /* In this case, the person is logged into Facebook, but not into the app, so we call
				   FB.login() to prompt them to do so. 
				   In real-life usage, you wouldn't want to immediately prompt someone to login 
				   like this, for two reasons:
				   (1) JavaScript created popup windows are blocked by most browsers unless they 
				   result from direct interaction from people using the app (such as a mouse click)
				   (2) it is a bad experience to be continually prompted to login upon page load.*/
                    if(facebook_alert_error())
                    {
                        FB.login(function(response) {
                            if (response.status === 'connected') {																							
                            }
                        }, {scope: 'publish_actions,email'});			  
                    }
					firstRun=false;	
				} else {
					firstRun=false;
				  /* In this case, the person is not logged into Facebook, so we call the login() 
				   function to prompt them to do so. Note that at this stage there is no indication
				   of whether they are logged into the app. If they aren't then they'll see the Login
				   dialog right after they log in to Facebook. 
				   The same caveats as above apply to the FB.login() call here.
				  FB.login();*/
                  if(facebook_alert_error())
                  {
                    FB.login(
						function( response )
						{
							if (response.status === 'connected') {												
								/*getUserDetailsAndDoPost();*/								
							}
						}, {scope: 'publish_actions,email'}			  
					);	
                  }                    
				}
				 /*console.log(response+"H");*/
			});
			 FB.getLoginStatus(function (response) {
				if(response.status=="unknown"){
					firstRun=false;
				}
					console.log(response);
			   });
			
		};
		/* Load the SDK Asynchronously*/
		  (function(d){
			 var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
			 js = d.createElement('script'); js.id = id; js.async = true;
			 js.src = "//connect.facebook.net/en_US/all.js";
			 d.getElementsByTagName('head')[0].appendChild(js);
		   }(document));
		
		var facebookDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
            Connecting to Facebook...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
        </div></div>');
        
		$(function() {
            $("#main_menu").click(function() {
				$("#hidden_menu").toggle();										
			});
            function login_linkedIn_click() {
                $('#Login_linkedIn').trigger('click');
            }
            $( "#Login_linkedIn" ).click(function( event ) {
                /*IN.User.authorize(function() {*/
                    var params = {
                        type: 'linked',
                        type_string: 'linkedIn',
                        func: function(obj) {
                            return liCheckIfConnected(obj);
                        }
                    };
                    
                    
                    $("#dialog").dialog({
                           dialogClass: 'noTitleStuff',
                           resizable: false,
                           modal: true,
                           autoOpen: false,
                    });
                    
                    processIntegratedApp(params);
                /*});*/
            
            });
            <?php if(isset($_GET['js_func_li'])) {
                    /*LinkedIn function REST on JS*/
                    echo $_GET['js_func_li'].';';
                }
            ?>
			$( "#Login_Facebook" ).click(function( event ) {					
                facebookDialog.dialog({
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title:'',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false ,
                    modal: true,
                    width:280,
                    height:120,				
                    show: '',
                    hide: ''                
                });
                facebookDialog.dialog("open");
                
                var log_details = {
                    type: 'fb_api',
                    event: 'FB login button click',
                    log_message: 'Connecting to facebook',
                    url: window.location.href,
                    response: ''
                };
                var logs = [];
                logs.push(JSON.stringify(log_details));
                /*save_system_log(logs);*/
                
                setTimeout( function ()
                {
                    if(fbApiInit)
                    {}
                    else{
                        FB.init({
                            appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                            status     : true, /* check login status*/
                            cookie     : true, /* enable cookies to allow the server to access the session*/
                            xfbml      : true  /* parse XFBML	  		*/
                        });
                        fbApiInit=true;
                    }
                    FB.getLoginStatus(function(response) {
                        if (response.status == "connected")
                        {
                            checkIfUserHasAnExistingAcct();									
                            /*FB.api('/me', function(response) {
                                getFbPhoto(response.id);
                            });*/
                        }
                        else
                        {
                            if(facebook_alert_error())
                            {
                                FB.login
                                (
                                    function( response )
                                    {
                                        if (response.status === 'connected') {
                                            var log_details = {
                                                type: 'fb_api',
                                                event: 'FB login button click',
                                                log_message: 'Connected to facebook',
                                                url: window.location.href,
                                                response: JSON.stringify(response)
                                            };
                                            
                                            var logs = [];
                                            logs.push(JSON.stringify(log_details));
                                            /*save_system_log(logs);*/
                                            checkIfUserHasAnExistingAcct();									
                                        }
                                        else
                                        {
                                            var log_details = {
                                                type: 'fb_api',
                                                event: 'FB login button click',
                                                log_message: 'User cancelled login or did not fully authorize.',
                                                url: window.location.href,
                                                response: JSON.stringify(response)
                                            };
                                            var logs = [];
                                            logs.push(JSON.stringify(log_details));
                                            facebookDialog.dialog("close");
                                            /*save_system_log(logs);*/
                                        }
                                    }, {scope: 'publish_actions,email'}
                                );
                            }                        
                        }
                    });
                    
                    
                
                
                },500);				
			});
		});        
		function getUserDetailsAndDoPost(){    
            doFacebookDelay('facebookLoadingDialog', 'friendsList_loading',1000, 'popup' ,'facebookConnectDialog');
			FB.api('/me', function(response) {      
				var output = '';
				for (property in response) {
				  output += property + ': ' + response[property]+'; ';
				}                                                        
				for (property in response.hometown) {
				  output += property.hometown + ': ' + response.hometown[property]+'; ';
				}                                                        
                                facebookDialog.dialog("close");
                                clearFacebookDelay();
				post_to_url((GLOMP_BASE_URL+'m/landing/'),response,'post')
				/*console.log('Good to see you, ' + response.id + '::'+response.email );*/
			});
		}
        var timerFacebookDelay=null;
        function doFacebookDelay(targetPopup, targetElement, timeout, outputType, closeThisFirst)
        {
            
            var message='We are experiencing a loading delay from Facebook.<br>Please wait a moment.';
            message='<span style="font-size:14px">'+message+'</span>&nbsp;<img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />';
            timerFacebookDelay = setTimeout( function ()
            {
                if(closeThisFirst!='')
                {
                    $("#"+closeThisFirst).dialog('close');
                }
                if(outputType=='popup')
                {            
                    
                    if( $('#'+targetPopup).doesExist() )
                    {/*target popupExist*/
                        $('#'+targetElement).html(message);                    
                    }
                    else
                    {/*create a new popup                    */
                        
                        var NewDialog = $(' <div id="'+targetPopup+'" align="center">\
                                                <div align="center" style="margin-top:15px;" id="'+targetElement+'">'+message+'</div>\
                                            </div>');
                        NewDialog.dialog({						
                            autoOpen: false,
                            closeOnEscape: false ,
                            resizable: false,					
                            dialogClass:'dialog_style_glomp_wait noTitleStuff',
                            title:'',
                            modal: true,
                            position: 'center center',
                            width:280,
                            height:130
                        });
                        NewDialog.dialog('open');
                    }
                }
                else
                {
                    
                    $('#'+targetElement).html(message);
                }
                
                
            }, timeout);
        }
        
        function clearFacebookDelay()
        {
            clearTimeout(timerFacebookDelay);
            if( $('#facebookLoadingDialog').doesExist() )
            {
                $("#facebookLoadingDialog").dialog('close');
                $("#facebookLoadingDialog").dialog('destroy').remove();
            }        
        }
		function post_to_url(path, params, method) {
			method = method || "post"; /* Set method to post by default if not specified.
			 The rest of this code assumes you are not using a library.
			 It can be made less wordy if you use one.*/
			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);		

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);
					form.appendChild(hiddenField);
				
				}
			}
			
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "fb_login");
			hiddenField.setAttribute("value", "fb_login");
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
		}
		function checkIfUserHasAnExistingAcct(){
			FB.api('/me', function(response) {
                                var log_details = {
                                    type: 'fb_api',
                                    event: 'FB login button click',
                                    log_message: 'Grabbing facebook logged user information',
                                    url: window.location.href,
                                    response: JSON.stringify(response)
                                };
                                var logs = [];
                                logs.push(JSON.stringify(log_details));
                                /*save_system_log(logs);*/
				var fbID=response.id;
				$.ajax({
					type: "POST",
					url: GLOMP_BASE_URL+'ajax_post/checkIfUserHasAnExistingAcct/',
					data:'fbID='+fbID				
				}).done(function(response) {
					if(response!=""){		
						response=jQuery.parseJSON(response);				
						if(response.data=='1' && response.data_status=="Active"){
                            facebookDialog.dialog("close");
                            clearFacebookDelay();
							$( "#dialog" ).dialog({						/*dialogClass: 'noTitleStuff'*/
								dialogClass: 'noTitleStuff dialog_style_glomp_wait',
								resizable: false,
								modal: true,
								autoOpen: false,
								width: 280,
								height: 200,
								buttons: [
									{
										text: "Login with Facebook",
										"class": 'btn-custom-blue_fb w120px ',
										click: function() {                                            
											$( this ).dialog( "close" );
                                            facebookDialog.dialog("open");
                                            FB.getLoginStatus(function(response) {
                                                if (response.status == "connected")
                                                {
                                                    getUserDetailsAndDoPost();
                                                }
                                                else
                                                {
                                                    if(facebook_alert_error())
                                                    {
                                                        FB.login
                                                        (
                                                            function( response )
                                                            {
                                                                if (response.status === 'connected') {
                                                                                                                            var log_details = {
                                                                                                                                type: 'fb_api',
                                                                                                                                event: 'FB login button click',
                                                                                                                                log_message: 'Processing glomp! through facebook.',
                                                                                                                                url: window.location.href,
                                                                                                                                response: JSON.stringify(response)
                                                                                                                            };
                                                                                                                            var logs = [];
                                                                                                                            logs.push(JSON.stringify(log_details));
                                                                                                                            /*save_system_log(logs);*/
                                                                    getUserDetailsAndDoPost();							
                                                                }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        var log_details = {
                                                                                                                            type: 'fb_api',
                                                                                                                            event: 'FB login button click',
                                                                                                                            log_message: 'User cancelled login or did not fully authorize.',
                                                                                                                            url: window.location.href,
                                                                                                                            response: JSON.stringify(response)
                                                                                                                        };
                                                                                                                        var logs = [];
                                                                                                                        logs.push(JSON.stringify(log_details));
                                                                                                                        facebookDialog.dialog("close");
                                                                                                                        /*save_system_log(logs);*/
                                                                                                                    }
                                                            }, {scope: 'publish_actions,email'}
                                                        );
                                                    }                       
                                                }
                                            });
                                            
                                            
                                                                                        
										}
									},
									{
										text: "Cancel",									
										"class": ' btn-custom-ash_xs w120px ',
										click: function() {
                                                                                    var log_details = {
                                                                                        type: 'fb_api',
                                                                                        event: 'FB login button click',
                                                                                        log_message: 'User cancelled.',
                                                                                        url: window.location.href,
                                                                                        response: JSON.stringify(response)
                                                                                    };
                                                                                    var logs = [];
                                                                                    logs.push(JSON.stringify(log_details));
                                                                                    $( "#dialog" ).dialog( "close" );
                                                                                    /*save_system_log(logs);*/
										}
									}
								]
							});
							$( "#dialog-content" ).html(SIGNUP_WITH_FB_BUT_REG);
							$( "#dialog" ).dialog( "open" );	
							
						}
						else{		
                                                        facebookDialog.dialog("open");
                                                        var log_details = {
                                                            type: 'fb_api',
                                                            event: 'FB login button click',
                                                            log_message: 'Processing glomp! through facebook.',
                                                            url: window.location.href,
                                                            response: JSON.stringify(response)
                                                        };
                                                        var logs = [];
                                                        logs.push(JSON.stringify(log_details));
                                                        /*save_system_log(logs);*/
                                                        getUserDetailsAndDoPost();
						}
					}
				});
			});
		}
		$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
        });
        $.fn.doesExist = function(){
            return jQuery(this).length > 0;
        };	  
        
        
       
        
        /* Linked IN */
        function liInitOnload() {
            if (IN.User.isAuthorized()) {
                /*NO NEED TO DO ANYTHING*/
            }
        }

        function liAuth() {
            /*IN.User.authorize(function() {*/
                /*If authorized and logged in*/
                var params = {
                    type: 'linked',
                    type_string: 'linkedIn',
                    func: function(obj) {
                        return login_linkedIN(obj);
                    }
                };
                
                processIntegratedApp(params);
            /*});*/
        }
        function linkedin_api(obj) {
            var data;
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: GLOMP_BASE_URL + 'ajax_post/linkedIn_callback',
                data: {
                    redirect_li: window.location.href,
                    js_func_li: obj.js_func_li,
                    api: obj.api,
                },
                success: function(_data) {
                    if (_data.redirect_window != '') window.location = _data.redirect_window;
                    data = _data;
                }
            });
            return data;
        }
        function liCheckIfConnected(obj) {
            params = {
                js_func_li: 'login_linkedIn_click()',
                api: '/v1/people/~:(id,email-address,first-name,last-name,date-of-birth)'      
            };
            
            var data = linkedin_api(params);
                /*If authorized and logged in*/
                /*IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {*/
                    params = {
                        id: data.values[0].id,
                        email: data.values[0].emailAddress,
                        field: 'user_linkedin_id'
                    };
                    
                    var ret = checkIfIntegrated(params);
                    
                    obj.dialog('close');
                    obj.dialog('destroy');
                    
                    if (ret) {
                        /*TODO: MAKE THIS GENERIC FUNCTION */
                        $("#dialog").dialog('destroy'); /*THIS IS NEEDED TO BE ABLE TO USE DIALOG AGAIN! */
                        
                        $("#dialog").dialog({
                            dialogClass: 'noTitleStuff dialog_style_glomp_wait',
                            resizable: false,
                            modal: true,
                            autoOpen: false,
                            width: 280,
                            height: 200,
                            buttons: [
                                {
                                    text: "Login with linkedIn",
                                    "class": 'btn-custom-blue_li w120px',
                                    click: function() {
                                        $(this).dialog("close");
                                        return liAuth();
                                    }
                                },
                                {
                                    text: "Cancel",
                                    "class": 'btn-custom-ash_xs w120px',
                                    click: function() {
                                        $(this).dialog("close");
                                        $(this).dialog('destroy'); /*THIS IS NEEDED TO BE ABLE TO USE DIALOG AGAIN! */
                                    }
                                }
                            ]
                        });
                        
                        $("#dialog-content").html(SIGNUP_WITH_FB_BUT_REG);
                        $("#dialog").dialog("open");
                    }
                    else {
                        return liAuth();
                    }
                /*});*/
        }

        function login_linkedIN(obj) {
            params = {
                js_func_li: '$("#Login_linkedIn").trigger("click")',
                api: '/v1/people/~:(id,email-address,first-name,last-name,date-of-birth)'      
            };
            
            var data = linkedin_api(params);
            /*IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {*/
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: GLOMP_BASE_URL+'ajax_post/processIntegratedApp',
                    data: {
                        id: data.values[0].id,
                        email: data.values[0].emailAddress,
                        field: 'user_linkedin_id',
                        mode: 'linkedIN',
                        device: 'mobile',
                        data: data.values[0]
                    },
                    success: function(res) {
                        if (res != false) {
                            if (res.post_to_url) {
                                /* The rest of this code assumes you are not using a library.*/
                                /* It can be made less wordy if you use one.*/
                                var form = document.createElement("form");
                                form.setAttribute("method", 'post');
                                form.setAttribute("action", res.redirect_url);

                                for (var key in res.data) {
                                    if (res.data.hasOwnProperty(key)) {
                                        var hiddenField = document.createElement("input");
                                        hiddenField.setAttribute("type", "hidden");
                                        hiddenField.setAttribute("name", key);
                                        hiddenField.setAttribute("value", res.data[key]);
                                        form.appendChild(hiddenField);
                                    }
                                }

                                document.body.appendChild(form);
                                obj.dialog('close');
                                return form.submit();
                            }
                            obj.dialog('close');
                            return window.location = res.redirect_url;
                        }
                    }
                });
            /*});*/
        }
        /* Usable functions */
        function processIntegratedApp(params) {
            /*var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
                    Connecting to ' + params.type_string + '...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
            </div></div>');*/
            var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
            Connecting to connectDialog...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
        </div></div>');


            connectDialog.dialog({
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                title: '',
                autoOpen: false,
                resizable: false,
                closeOnEscape: false,
                modal: true,
                width: 280,
                height: 120,
                show: '',
                hide: ''
            });
            
            connectDialog.dialog("open");
            params.func(connectDialog);
        }

        function checkIfIntegrated(param) {
            var ret = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: GLOMP_BASE_URL+'ajax_post/checkIfIntegrated',
                data: {
                    id: param.id,
                    email: param.email,
                    field: param.field,
                },
                success: function(res) {
                    if (res) {
                        ret = true;
                    } else {
                        ret = false;
                    }
                }
            });
            
            return ret;
        }
     </script>
    </body>    
</html>
