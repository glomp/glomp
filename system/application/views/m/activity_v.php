<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet" media="screen">            
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">            
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/user/update') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('settings'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/user/logOut') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Log_Out'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp-profile-logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations        
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav" align="left">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/user/update') ?>">
                        <div class="hidden_nav" align="left">
                            <?php echo $this->lang->line('settings'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/user/logOut') ?>">
                        <div class="hidden_nav" align="left">        
                            <?php echo $this->lang->line('Log_Out'); ?>
                        </div>
                    </a>				
                </div>
                <!-- hidden navigations --> 
            </div>            
            <?php
                $activity_class='';
                $points_class='';
                if($selected=='')
                {
                    $activity_class='activity_header_active';
                }
                else
                {
                    $points_class='activity_header_active';
                }
            ?>
            <div class="activity_wrapper" align="center" style="position:relative;width:100%;top:0px;left:0px;z-index:1;">
                <a href="<?php echo base_url(MOBILE_M . '/activity/') ?>">
                    <div class="activity_header <?php echo $activity_class;?> fl" align="center">
                        Activity
                    </div>
                </a>
                <a href="<?php echo base_url(MOBILE_M . '/activity/points') ?>">
                    <div class="activity_header <?php echo $points_class;?> fr" align="center">
                        glomp!points
                    </div>
                </a>
			</div>
            <div class="p20px_0px cl body_bottom_1px activity_log_content" style="margin-top:0px;"></div>	
            <div style="font-size:12px;" id="log_content">
                <?php 
                    $start_count=0;                                                                                
                        foreach($activity->result() as $rec)
                        {
                            $start_count++;
                            $details = json_decode($rec->details);
                            $user_id=$this->session->userdata('user_id');
                            switch($rec->log_type)
                            {
                                case 'glomp_someone':                                 
                                    $product_id=$details->product_id;
                                    $recipient_id=$details->recipient_id;                                
                                    $recipient_name =$details->recipient_name;                                
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>">                                 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>glomp!</b>ed</span>&nbsp;
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$recipient_id);?>" class="red">
                                                        <b><?php echo $recipient_name;?></b>
                                                    </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'received_a_glomp':
                                    $product_id=$details->product_id;
                                    $glomper_id=$details->glomper_id;                                
                                    $glomper_name =$details->glomper_name;
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$glomper_id);?>" class="red">
                                                        <b><?php echo $glomper_name;?></b>
                                                    </a> <span class="red"><b>glomp!</b>ed</span>&nbsp; a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'received_a_reward':  
                                    if($selected=='')
                                    {

                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>Rewarded</b></span>&nbsp;
                                                        a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> from&nbsp;
                                                        <span class="red"><b>glomp!</b></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'redeemed_a_voucher': 
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>Redeemed</b></span>&nbsp;
                                                    a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> voucher
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'friend_redeemed_a_voucher':
                                                                                
                                    $friend_id=$details->friend_id;                                
                                    $friend_name =$details->friend_name;                           
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    Your friend&nbsp;
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                                        <b><?php echo $friend_name;?></b>
                                                    </a>&nbsp;
                                                    <span class="red"><b>redeemed</b></span>&nbsp;
                                                    a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> voucher
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'added_a_friend':                                                                                                                                    
                                    $friend_id=$details->friend_id;                                
                                    $friend_name =$details->friend_name;                                
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>Added</b></span>&nbsp;
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                                        <b><?php echo $friend_name;?></b>
                                                    </a> to your Friends List
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'friend_added_you':                                                                            
                                    $friend_id=$details->friend_id;                                
                                    $friend_name =$details->friend_name;                                
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                                        <b><?php echo $friend_name;?></b>
                                                    </a> <span class="red"><b>added</b></span>&nbsp;you to their Friends List
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'unfriend_a_friend':                                                                            
                                    $friend_id=$details->friend_id;                                
                                    $friend_name =$details->friend_name;                                
                                    if($selected=='')
                                    {
                                    ?>
                                    <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                        <div class="container p5px_0px global_margin">	
                                            <div class="row">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>Removed</b></span>&nbsp;                                                                                    
                                                    <a href="<?php echo base_url(MOBILE_M . '/profile/view/'.$friend_id);?>" class="red">
                                                        <b><?php echo $friend_name;?></b>
                                                    </a> to your Friends List
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    break;
                                case 'invited_a_friend':
                                if($selected=='')
                                {
                                    if($details->invited_through=='facebook')
                                    {
                                        ?>
                                        <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                            <div class="container p5px_0px global_margin">	
                                                <div class="row">
                                                    <div style="margin-bottom:2px;">
                                                        <b>
                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                        </b>
                                                    </div>
                                                    <div style="margin-bottom:10px;">
                                                        <span class="red"><b>Invited</b></span>&nbsp;                                                                                    
                                                        <a href="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>" target="_blank" class="red">
                                                            <b><?php echo $details->invited_name;?></b>
                                                        </a> from Facebook to join <span class="red"><b>glomp</b>!ed</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    else if($details->invited_through=='linkedIn')
                                    { 
                                        ?>
                                        <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                            <div class="container p5px_0px global_margin">	
                                                <div class="row">
                                                    <div style="margin-bottom:2px;">
                                                        <b>
                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                        </b>
                                                    </div>
                                                    <div style="margin-bottom:10px;">
                                                        <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                                        <b><?php echo $details->invited_name;?></b>
                                                        from LinkedIn to join <span class="red"><b>glomp</b>!ed</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php                                                                            
                                    }
                                    else if($details->invited_through=='email')
                                    { 
                                        ?>
                                        <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                            <div class="container p5px_0px global_margin">	
                                                <div class="row">
                                                    <div style="margin-bottom:2px;">
                                                        <b>
                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                        </b>
                                                    </div>
                                                    <div style="margin-bottom:10px;">
                                                        <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                                        <b><?php echo $details->invited_name;?></b>
                                                        &nbsp;through email to join <span class="red"><b>glomp</b>!ed</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   <?php                                                                            
                                    }
                                }
                                break;
                            case 'friend_added_by_friend':
                                $friend_id=$details->friend_id;
                                $added_friend_id=$details->added_friend_id;
                                $friend_name =$details->friend_name;                            
                                $added_friend_name =$details->added_friend_name;
                                if($selected=='')
                                {
                                ?>
                                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                    <div class="container p5px_0px global_margin">	
                                        <div class="row">
                                            <div style="margin-bottom:2px;">
                                                <b>
                                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                </b>
                                            </div>
                                            <div style="margin-bottom:10px;">
                                                Your friend&nbsp;
                                                <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                    <b><?php echo $friend_name;?></b>
                                                </a>&nbsp;
                                                <span class="red"><b>added</b></span>&nbsp;
                                                your friend&nbsp;
                                                <a href="<?php echo base_url('profile/view/'.$added_friend_id);?>" class="red">
                                                    <b><?php echo $added_friend_name;?></b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                break;
                            case 'points_purchased':
                                if($selected=='points')
                                {
                                ?>
                                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                    <div class="container p5px_0px global_margin">	
                                        <div class="row">
                                            <div class="pull-left" style="width:80%">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>Purchased</b></span>&nbsp;                                                                                        
                                                    <?php echo $details->points;?> points
                                                </div>
                                            </div>
                                            <div class="pull-right" style="width:20%;">
                                                <div class="col-xs-12" style="padding:4px;" align="right">
                                                    <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                                        <?php echo $details->balance;?>
                                                    </div>                                                                                            
                                                </div>                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                break;
                            case 'points_spent':                                                                            
                                $product_id=$details->product_id;
                                $recipient_id=$details->recipient_id;
                                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                                $recipient_name =$info->friends->$recipient_id->user_name;
                                $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                                
                                $prod_info = json_decode($this->product_m->productInfo($product_id));
                                $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                                $points =((int) $details->points) *-1;
                                if($points>0)
                                {
                                    $points =" + ".abs($points);
                                }
                                else
                                {
                                    $points =" - ".abs($points);
                                }
                                
                                if($selected=='points')
                                {
                                ?>
                                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                    <div class="container p5px_0px global_margin">	
                                        <div class="row">
                                            <div class="pull-left" style="width:80%">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>glomp!</b>ed</span>&nbsp;
                                                    <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                                        <b><?php echo $recipient_name;?></b>
                                                    </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>&nbsp;
                                                    (<?php echo $points;?>)
                                                </div>
                                            </div>
                                            <div class="pull-right" style="width:20%;">
                                                <div class="col-xs-12" style="padding:4px;" align="right">
                                                    <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                                        <?php echo $details->balance;?>
                                                    </div>                                                                                            
                                                </div>                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                break;
                            case 'points_reversed':                                                                            
                                $voucher_id=$details->voucher_id;
                                $voucher_data=$this->users_m->get_specific_voucher_details($voucher_id);
                                $voucher_data=$voucher_data->row();
                                
                                
                                $product_id     =$voucher_data->voucher_product_id;
                                $recipient_id   =$voucher_data->voucher_belongs_usser_id;
                                
                                
                                
                                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                                $recipient_name =$info->friends->$recipient_id->user_name;
                                $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                                
                                $prod_info = json_decode($this->product_m->productInfo($product_id));
                                $prod_image= $prod_info->product->$product_id->prod_image;
                                
                                $merchant_name  =$prod_info->product->$product_id->merchant_name;
                                $product_name   =$prod_info->product->$product_id->prod_name;
                                $points =((int) $details->points) *-1;
                                if($points>0)
                                {
                                    $points =" + ".abs($points);
                                }
                                else  if($points<0)
                                {
                                    $points =" - ".abs($points);
                                }
                                else
                                {
                                    $points =" ".abs($points);
                                }
                                
                                if($selected=='points')
                                {
                                ?>
                                <div class="cl body_bottom_1px" data-id="<?php echo $start_count;?>"> 
                                    <div class="container p5px_0px global_margin">	
                                        <div class="row">
                                            <div class="pull-left" style="width:80%">
                                                <div style="margin-bottom:2px;">
                                                    <b>
                                                    <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                    </b>
                                                </div>
                                                <div style="margin-bottom:10px;">
                                                    <span class="red"><b>glomp!</b></span> of a&nbsp;
                                                    <?php echo $merchant_name;?> <?php echo $product_name;?>&nbsp;
                                                    was <span class="red"><b>unredeemed</b></span> by&nbsp;
                                                    <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                                        <b><?php echo $recipient_name;?></b>
                                                    </a>&nbsp;
                                                    (<?php echo $points;?>)
                                                </div>
                                            </div>
                                            <div class="pull-right" style="width:20%;">
                                                <div class="col-xs-12" style="padding:4px;" align="right">
                                                    <div class="fr" style="padding:10px 10px 5px 10px;height:40px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:16px" align="center">
                                                        <?php echo $details->balance;?>
                                                    </div>                                                                                            
                                                </div>                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                break;

                            }                        
                        }                    
                    if($start_count==0)
                    { ?>
                        <div class="container p5px_0px global_margin">	
                            <div class="row" align="center">
                                No activity logs yet.
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                 
            </div>

            
            <!-- /container -->
            
            <div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>        
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>    
        <script>
            var FB_APP_ID                               ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var loadingMoreStories=false;
            $(function() {
                $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();
                });
            });
                 function loadMore()
         {
            if(!loadingMoreStories)
            {
                loadingMoreStories=true;            
                start_count=$('#log_content').children().last().data().id;
                var type="<?php echo $selected;?>";
                if(start_count>0)
                {
                    $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">Loading more stories...<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                    $('#log_content').append($loader);
                    data ='start_count='+start_count;
                    setTimeout(function() {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: '<?php echo base_url();?>m/activity/getMoreActivity/'+type,
                            data: data,
                            success: function(response){ 
                                if(response!='' &&  response!='<!---->')
                                {
                                     $('#id_loader').remove();
                                    $('#log_content').append(response);
                                    $(window).bind('scroll', bindScroll);
                                    loadingMoreStories=false;
                                    
                                    
                                }
                                else
                                {                                
                                    $('#id_loader').remove();
                                    $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">No more logs to load.</div>');
                                    $('#log_content').append($no_more);
                                }
                            }
                        });
                    }, 1);
                }                    
            }
           
         }

            

             function bindScroll(){
               if($(window).scrollTop() + $(window).height() > $(document).height() -10) {
                   $(window).unbind('scroll');
                   loadMore();
               }
            }             
            $(window).scroll(bindScroll);
            
        </script>        
        
    </body>
    <?php //include_once("includes/node.php") ?>
</html>
