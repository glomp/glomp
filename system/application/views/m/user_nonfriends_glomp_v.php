<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/user/searchFriends?edit=1') ?>" class="white ">
                                        <div class="w100per fl white">
                                        Edit Friends
                                        <?php echo $this->lang->line('Edit_Friends'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <a href="<?php echo site_url(MOBILE_M.'/user/searchFriends'); ?>" >
                        <div class="glomp_header_logo_2" style="height: 25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-friends-logo.png');"></div>
                    </a>
                </div>
                 <!-- hidden navigations        
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo site_url(MOBILE_M.'/user/searchFriends?edit=1'); ?>">
                        <div class="hidden_nav">
                            Edit Friends
                            <?php echo $this->lang->line('Edit_Friends'); ?>
                        </div>
                    </a>
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="p20px_0px" style="margin-top:12px;"></div>			

           
            <div class="row border_bottom_gray">
                <div class="container p10px_20px">
                    <div class="fl">
                        <span class="red" style="font-size: 10px;">Is your friend shown below?</span>
                    </div>
                    <div class="fr">
						<?php echo  $name."-".$email."-".$location_name; ?>
						<a class="" href="<?php echo base_url(MOBILE_M.'/profile/menu/?tab=merchants&from=non-friend&name='.$name.'&email='.$email.'&location='.$location)?>">
							<button class="btn-custom-blue-grey_xs w40px">No</button>
						<a>
                    </div>
                </div>
            </div>

            <?php if ($resFriendList_count == 0) { ?>
                <div class="row border_bottom_gray">
                    <div class="container p10px_20px">
                        <div class="fl">
                            <span class="red" style="font-size: 10px;">No result found</span>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php foreach ($resFriendList->result() as $resFriendList) { ?>
                <div class="row border_bottom_gray">
                    <div class="container p10px_20px">
                        <div class="fl">
                            <a href="<?php echo site_url(MOBILE_M . '/profile/view/' . $resFriendList->user_id); ?>" >
                                <img style="width: 40px;" src="<?php echo base_url() . '/' . $this->custom_func->profile_pic($resFriendList->user_profile_pic, $resFriendList->user_gender); ?>" alt="" >
                            </a>
                        </div>
                        <div class="fl" style="margin-left: 5px;">
                            <a href="<?php echo site_url(MOBILE_M . '/profile/view/' . $resFriendList->user_id); ?>" >
                                <span class="red" style="font-size: 10px;"><?php echo $resFriendList->user_fname . ' ' . $resFriendList->user_lname ?></span>
                                <br />
                                <span style="color:#585F6B"><?php echo $this->regions_m->region_name($resFriendList->user_city_id); ?></span>
                            </a>
                        </div>
                        <div class="fr" >
                            <a href="#<?php /*echo site_url(MOBILE_M.'/user/searchFriends?edit=1&remove_friend_id='.$recFriendList->user_id)*/?>">
                                <div class="fr glomp_add" style="background-image:url('<?php echo base_url('assets/m/img/glomp-add.png') ?>'); width: 21px;"></div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <!-- /footer -->
            <div class="footer_wrapper"></div>
            <!-- /footer --> 
        </div> <!-- /global_wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
		<script>
           $(function() {	
           $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();										
                });
            
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
        </script>        
    </body>
</html>
