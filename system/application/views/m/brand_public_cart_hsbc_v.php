<?php
switch ($public_alias) {
        case 'hsbc':
            $macallan =  HSBC_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  HSBC_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  HSBC_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  HSBC_BRAND_PROD_ID_SINGBEV;
            $swirls =  HSBC_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  HSBC_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  HSBC_BRAND_PROD_ID_KPO;
            $nassim =  HSBC_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  HSBC_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona = '0';
            $laurent_perrier =  '0';
            $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
            break;
        case 'uob':
            $macallan =  UOB_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  UOB_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  UOB_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  UOB_BRAND_PROD_ID_SINGBEV;
            $swirls =  UOB_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  UOB_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  UOB_BRAND_PROD_ID_KPO;
            $nassim =  UOB_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  UOB_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona =  UOB_BRAND_PROD_ID_VALRHONA;
            $laurent_perrier =  '0';
            $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
            
            break;
        case 'dbs':
            $macallan =  DBS_BRAND_PROD_ID_MACALLAN;
            $snowleopard =  DBS_BRAND_PROD_ID_SNOW_LEOPARD;
            $london =  DBS_BRAND_PROD_ID_LONDON_DRY;
            $singbev =  DBS_BRAND_PROD_ID_SINGBEV;
            $swirls =  DBS_BRAND_PROD_ID_SWIRLS_BAKESHOP;
            $daily_juice =  DBS_BRAND_PROD_ID_DAILY_JUICE;
            $kpo =  DBS_BRAND_PROD_ID_KPO;
            $nassim =  DBS_BRAND_PROD_ID_NASSIM_HILL;
            $delights_heaven =  DBS_BRAND_PROD_ID_DELIGHTS_HEAVEN;
            $valrhona =  DBS_BRAND_PROD_ID_VALRHONA;
            $laurent_perrier =  DBS_BRAND_PROD_ID_LAURENT_PERRIER;
            $providore =  DBS_BRAND_PROD_ID_PROVIDORE;
	        $glenfiddich =  DBS_BRAND_PROD_ID_GLENFIDDICH;
	        $moet_chandon =  DBS_BRAND_PROD_ID_MOET_CHANDON;
	        $kusmi_tea =  DBS_BRAND_PROD_ID_KUSMI_TEA;
	        $euraco = DBS_BRAND_PROD_ID_EURACO;
            break;
        case 'amex':
	        $macallan =  AMEX_BRAND_PROD_ID_MACALLAN;
	        $snowleopard =  AMEX_BRAND_PROD_ID_SNOW_LEOPARD;
	        $london =  AMEX_BRAND_PROD_ID_LONDON_DRY;
	        $singbev =  AMEX_BRAND_PROD_ID_SINGBEV;
	        $swirls =  AMEX_BRAND_PROD_ID_SWIRLS_BAKESHOP;
	        $daily_juice =  AMEX_BRAND_PROD_ID_DAILY_JUICE;
	        $kpo =  AMEX_BRAND_PROD_ID_KPO;
	        $nassim =  AMEX_BRAND_PROD_ID_NASSIM_HILL;
	        $delights_heaven =  AMEX_BRAND_PROD_ID_DELIGHTS_HEAVEN;
	        $valrhona =  AMEX_BRAND_PROD_ID_VALRHONA;
	        $laurent_perrier =  '0';
	        $providore =  '0';
		    $glenfiddich =  '0';
		    $moet_chandon =  '0';
		    $kusmi_tea =  '0';
		    $euraco = '0';
	        break;
    }


    if(!defined('BRAND_PROD_ID_MACALLAN'))     define('BRAND_PROD_ID_MACALLAN',$macallan);
    if(!defined('BRAND_PROD_ID_SNOW_LEOPARD')) define('BRAND_PROD_ID_SNOW_LEOPARD',$snowleopard);
    if(!defined('BRAND_PROD_ID_LONDON_DRY'))   define('BRAND_PROD_ID_LONDON_DRY',$london);

    define('BRAND_PROD_ID_SINGBEV',$singbev);
    define('BRAND_PROD_ID_SWIRLS_BAKESHOP',$swirls);
    define('BRAND_PROD_ID_DAILY_JUICE',$daily_juice);

    define('BRAND_PROD_ID_KPO',$kpo);
    define('BRAND_PROD_ID_NASSIM_HILL',$nassim);
    define('BRAND_PROD_ID_DELIGHTS_HEAVEN',$delights_heaven);
    define('BRAND_PROD_ID_VALRHONA',$valrhona);
    define('BRAND_PROD_ID_LAURENT_PERRIER',$laurent_perrier);
    define('BRAND_PROD_ID_PROVIDORE',$providore);
	define('BRAND_PROD_ID_GLENFIDDICH',$glenfiddich);
	define('BRAND_PROD_ID_MOET_CHANDON',$moet_chandon);
	define('BRAND_PROD_ID_KUSMI_TEA',$kusmi_tea);
	define('BRAND_PROD_ID_EURACO',$euraco);
?>
<?php
	$user_id = $this->session->userdata('user_id');
	$grand_total=0;

	$inTour_display='';
	$inTour_header='';
	$inTour_header_display='display:none;';
	$inTour_display_footer='display:none;';
	

?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo ucfirst($public_alias);?>| glomp! mobile </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords('');?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
		<script src="<?php echo minify('assets/frontend/js/jquery.mask.min.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
		<script type="text/javascript">
			<?php $public_user_data = $this->session->userdata('public_user_data');?>
			var public_email = "<?php echo $public_user_data['user_email'];?>";
			var public_fname = "<?php echo $public_user_data['user_fname'];?>";
			var public_lname = "<?php echo $public_user_data['user_lname'];?>";
			var items = [];
			var delivery_type="delivery";
			var public_alias="<?php echo $public_alias;?>";
			var accumulated_brand_list = [<?php echo BRAND_PROD_ID_MACALLAN;?>,<?php echo BRAND_PROD_ID_SNOW_LEOPARD;?>,<?php echo BRAND_PROD_ID_LONDON_DRY;?>]
        </script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper mobile" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;<?php echo $inTour_display;?>">
                <div class="header_navigation_wrapper fl" align="center">
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <?php if (isset($_GET['mDetail'])) { ?>
                                <li>
                                    <a href="#" class="white "  id = "back_btn">
                                        <div class="w100per fl white">
                                            Back
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
								<?php
									if(
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url(MOBILE_M) ?>/brands/view/<?php echo $public_alias;?>" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('Back','Back'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                 <?php
                                $public_alias_address  =$public_alias;
                                if($public_alias=='hsbc' || $public_alias =='uob' || $public_alias =='dbs')
                                    $public_alias_address  .='sg';
                                else if($public_alias =='amex')
                                    $public_alias_address  .='-sg';
                                ?>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/'.$public_alias_address) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo ucfirst($public_alias);?> <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <?php
                                if($this->session->userdata('is_user_logged_in')== true)
                                {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M. '/profile') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('profile'); ?>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(MOBILE_M.'/user/logOut') ?>" class="white ">
                                            <div class="w100per fl white">
                                            <?php echo $this->lang->line('Log_Out'); ?>
                                            </div>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
								<?php
									if(
										$this->session->userdata('user_id') =='' &&
										$this->session->userdata('public_user_since_user_logged_in') !=''
										)
									{
								?>
										 <li>
											<a href="<?php echo base_url() ?>brands/logout/<?php echo $public_alias_address;?>/m" class="white ">
												<div class="w100per fl white">
												<?php echo $this->lang->line('exit','Exit'); ?>
												</div>
											</a>
										</li>
								<?php
									}
								?>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:22px;background-image:url('<?php echo base_url() ?>assets/m/img/menu_logo.png');"></div>
                    </a>
                </div>
            </div>
            <div class="p20px_0px" style="margin-top:10px;"></div>
			<div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: normal; color:#4C5E6B;<?php echo $inTour_header_display;?>">
				<div class="fl row red harabarabold" style="font-size: 18px;"><?php echo $inTour_header;?></div>
			</div>

            <div class=" ">
                
                <div style="padding:10px">
                	<?php
					if($public_alias=='hsbc' || $public_alias=='uob' || $public_alias=='dbs')		
					{
					?>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_MACALLAN);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< The Macallan
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SNOW_LEOPARD);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< Snow Leopard Vodka
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_LONDON_DRY);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< No.3 London Dry Gin
							</div>
						</a>


						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SINGBEV);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< SingBev
							</div>
						</a>


						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_SWIRLS_BAKESHOP);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< Swirls Bakeshop
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_DELIGHTS_HEAVEN);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< The Delights Heaven
							</div>
						</a>


						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_DAILY_JUICE);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< Daily Juice
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_KPO);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< KPO
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_NASSIM_HILL);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< Nassim Hill
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_VALRHONA);?>" class="hide" >
							<div style="float:left;" class="btn-custom-black" >
								<< Valrhona
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_LAURENT_PERRIER);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< Laurent Perrier
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_PROVIDORE);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< The Providore
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_GLENFIDDICH);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< Glenfiddich
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_MOET_CHANDON);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< Moet & Chadon
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_KUSMI_TEA);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< Kusmi Tea
							</div>
						</a>
						<a href="<?php echo site_url(MOBILE_M.'/brands/view/'.$public_alias.'/'.BRAND_PROD_ID_EURACO);?>" class="" >
							<div style="float:left;" class="btn-custom-black" >
								<< Euraco
							</div>
						</a>


					<?php
					}
					?>
					<a href="<?php echo site_url(MOBILE_M.'/brands/cart/'.$public_alias);?>" >						
						<div style="float:right;" class="btn-custom-blue-grey_xs" >Cart (<span id="session_cart_qty" <?php echo $this->session->userdata('session_cart_qty_'.$public_alias)=='' ? 'class="gray">0': 'class="red">'.$this->session->userdata('session_cart_qty_'.$public_alias);?></span>)</div>
					</a>
				</div>
				
				<div class="page-line cl body_bottom_1px" style="padding:6px;" >
				</div>
				
				
				<?php 
				if($cart_data!="")
				{
					$accumulated_300 = false;
					$accumulated =0;

					foreach($cart_data as $brand_row)
					{
						$per_brand_total = 0;
						$per_brand_qty = 0;

						foreach($brand_row['prod_list'] as $row)
						{					
							$row = $row['product'];																																		
							$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
							$per_brand_qty += $row->prod_qty;
						}
						switch ($brand_row['brand_id'])
						{
							case BRAND_PROD_ID_MACALLAN:
							case BRAND_PROD_ID_SNOW_LEOPARD:
							case BRAND_PROD_ID_LONDON_DRY:
								$accumulated += $per_brand_total;
						}
					}

					if($accumulated>=300)
					{
						$accumulated_300 = true;
					}
				
					$grand_total = 0;													
					$delivery_total = 0;
					$color="#ddd";
					foreach($cart_data as $brand_row)
					{
						if($color=="#eee")
							$color="#ddd";
						else
							$color="#eee";
						?>
						<div id="brand_wrapper_<?php echo $brand_row['brand_id'];?>" style="background-color:<?php echo $color?>;padding:10px 10px;">
							<?php
							$per_brand_total = 0;
							$per_brand_qty = 0;

							foreach($brand_row['prod_list'] as $row)
							{
								$options = $row['option'];
								$row = $row['product'];																				
								$product = json_decode($row->prod_details);
								$grand_total += (($row->prod_cost ) * $row->prod_qty);
								$per_brand_total += (($row->prod_cost ) * $row->prod_qty);
								$per_brand_qty += $row->prod_qty;
								?>
								<div class="page-line cl body_bottom_1px merchant-products" id="item_wrapper_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>" style="background:<?php echo $color;?>;">
			                        <div class="container p5px_0px ">
			                            <div class="row">
											<div class="col-xs-2" style="border:solid 0px #333">
												<div align="left">
													<?php
													$src = "http://placehold.it/190x102";
													if($product->prod_image !="")
														$src = site_url('custom/uploads/products/'.$product->prod_image);
													?>
													<img style="width:40px;height:40px;" src="<?php echo $src;?>" alt="<?php echo $product->prod_name;?>">
												</div>
											</div>
											<div class="col-xs-4" >
												<b class="menu_merchant" ><?php echo $product->merchant_name;?></b>
												<div class="menu_product" style="overflow: hidden;word-wrap: break-word;width:100% !important;border:solid 0px #333;"><?php echo $product->prod_name;?></div>
												<?php
												if(count($options) > 0)
												{
													?>
													<div class="menu_product" style="font-weight:normal;font-size:11px;padding:0px 4px; border:solid 0px; padding-left:0px;">
														<b>Flavor(s):</b>
														<div style="padding-left: 8px;">																											
															<?php																									
															foreach ($options as $p)
															{
																?>																										
																<div>
																	<?php echo $p->option_qty;?> - <?php echo $p->option_value;?>
																</div>
																<?php
															}																										
															?>																																																				
														</div>
													</div>
													<?php
												}
												?>	
											</div>
											<div class="col-xs-2" align="right" style=";padding:0px;">
												<div class="menu_product" style="line-height: 1.3em;">
													S$&nbsp;<span id="prod_cost_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>"><?php echo number_format(($row->prod_cost  ),2);?></span>
												</div>
											</div>
											<div class="col-xs-1" style="border:solid 0px #333;padding-left:8px;" >
												<select name="quantity" autocomplete="off" 
														id="quantity_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>" 
														data-prod_group="<?php echo $row->prod_group;?>" 
														data-public_alias="<?php echo $public_alias;?>" 
														data-prod_merchant_cost="<?php echo $product->prod_merchant_cost;?>" 
														data-id="<?php echo $product->product_id;?>" 
														data-brand_id="<?php echo $brand_row['brand_id'];?>"
														class="cart_qty" style="width:38px;">
													<?php 
														for($x=0;$x<=10;$x++)
														{
															?>
															<option value="<?php echo $x;?>" 
															<?php
																if($x == $row->prod_qty)
																{
																	?>
																	selected="selected"
																	<?php
																}
															?>
															>
															<?php echo $x;?>
															</option>
															<?php
														} 
													?>
												</select>
											</div>
											<div class="col-xs-3 menu_merchant" align="right" style="font-weight:bold;font-size:11px;line-height: 1em;" id="total_<?php echo $product->product_id;?>_<?php echo $row->prod_group;?>">
												S$ <?php echo number_format(( ($row->prod_cost ) ) * ($row->prod_qty),2);?>
											</div>
										</div>
			                        </div>
								</div>
								<?php 
							}
							$has_delivery = false;
							$brand_eror='';
							switch ($brand_row['brand_id']) {
								case BRAND_PROD_ID_MACALLAN:
								case BRAND_PROD_ID_SNOW_LEOPARD:
								case BRAND_PROD_ID_LONDON_DRY:
										if($per_brand_total<300)
										{
											if($accumulated_300 === false)
											{
												$brand_eror = "
												Orders for combined orders of The Macallan whiskies, Snow Leopold Vodka and No.3 London Dry Gin should be S$300 and above to select Delivery.<br>
												Please order more items or change your Delivery Type to <b>For Pick Up</b>.
												";
											}
										}
									break;
								case BRAND_PROD_ID_SWIRLS_BAKESHOP:
									$has_delivery = 16;
									break;
								case BRAND_PROD_ID_SINGBEV:								
									if($per_brand_total<350)
									{

										$has_delivery = 35;
									}	
									break;
								case BRAND_PROD_ID_DELIGHTS_HEAVEN:
									$has_delivery = 25;
									break;
								case BRAND_PROD_ID_VALRHONA:
									if($per_brand_total<300)
										$has_delivery = 35;
									break;
								case BRAND_PROD_ID_LAURENT_PERRIER:
									if($per_brand_qty < 3)
										$has_delivery = 20;
									break;
								case BRAND_PROD_ID_PROVIDORE:
									if($per_brand_total < 200)
										$has_delivery = 20;
									break;
								case BRAND_PROD_ID_EURACO:
									if($per_brand_total < 300)
										$has_delivery = 30;
									break;
							}
							$brand_display_error=($brand_eror=='') ? 'none' :'block';
							?>	
							<div class="container p5px_0px ">	
							<div class="row"  >																	
								<div 	data-brand_id="<?php echo $brand_row['brand_id'];?>" 
										data-brand_name="<?php echo $brand_row['brand_name'];?>" 
										class="brand_error alert alert-error col-xs-12" 
										id="error_<?php echo $brand_row['brand_id'];?>"
										style="display:<?php echo $brand_display_error;?>;";
										align="center"
								><?php echo $brand_eror;?></div>
							</div>
							<?php
								$has_delivery_display = ($has_delivery===false) ? 'none':'';
								$delivery_total += $has_delivery;

								$no_forpickup='';
								$delivery_only='';

								$no_fordelivery='';
								$pickup_only='';
								if($brand_row['brand_id'] == BRAND_PROD_ID_SINGBEV || $brand_row['brand_id'] == BRAND_PROD_ID_DAILY_JUICE || $brand_row['brand_id'] == BRAND_PROD_ID_LAURENT_PERRIER || $brand_row['brand_id'] == BRAND_PROD_ID_GLENFIDDICH || $brand_row['brand_id'] == BRAND_PROD_ID_MOET_CHANDON || $brand_row['brand_id'] == BRAND_PROD_ID_KUSMI_TEA || $brand_row['brand_id'] == BRAND_PROD_ID_EURACO)
								{
									$no_forpickup='none';
									$delivery_only=' Only';
								}																				
								if($brand_row['brand_id'] == BRAND_PROD_ID_NASSIM_HILL ||
								$brand_row['brand_id'] == BRAND_PROD_ID_KPO
								)
								{
									$no_fordelivery='none';
									$pickup_only=' Only';
								}
								?>
								<!-- delivery charge and subtotal -->
								
									<div class="row" style="font-size: 11px" >
										<div class="col-xs-8 form-inline" align="left" >	
											<div style="font-weight:bold">Delivery Type: </div>
											
											<label class="form-group"  for="for_pickup_<?php echo $brand_row['brand_id'];?>" style="display:<?php echo $no_forpickup;?>;">
												<input 	type="radio" 
														style="display:<?php echo $no_fordelivery;?>;"
														class="delivery_type" 
														value="pickup" 
														name="for_delivery_<?php echo $brand_row['brand_id'];?>" 
														id="for_pickup_<?php echo $brand_row['brand_id'];?>" 
														data-id="<?php echo $brand_row['brand_id'];?>" 
														/>
												For Pick Up<?php echo $pickup_only;?>
											</label>
											<label class="form-group"  for="for_delivery_<?php echo $brand_row['brand_id'];?>" style="display:<?php echo $no_fordelivery;?>;">
												&nbsp;<input 	type="radio" 
														style="display:<?php echo $no_forpickup;?>;"
														class="delivery_type" 
														value="delivery" 
														name="for_delivery_<?php echo $brand_row['brand_id'];?>" 
														id="for_delivery_<?php echo $brand_row['brand_id'];?>" 
														checked="checked"
														data-id="<?php echo $brand_row['brand_id'];?>" 
														data-delivery_charge="<?php echo $has_delivery;?>"
														 />
												For Delivery<?php echo $delivery_only;?>
											</label>
										</div>
										<div 	class="col-xs-4 delivery_charge" 
												data-delivery_charge="<?php echo $has_delivery;?>"
												data-delivery_type="delivery"
												data-id="<?php echo $brand_row['brand_id'];?>" 
											 	align="right" style="font-size:11px;font-weight:bold;display:<?php echo $has_delivery_display;?>" id="delivery_charge_<?php echo $brand_row['brand_id'];?>">
											Delivery Charge: S$ <span id="delivery_charge_value_<?php echo $brand_row['brand_id'];?>"><?php echo number_format($has_delivery,2);?></span>
										</div>
									</div>
								</div>
								<!-- delivery charge and subtotal -->
						</div><!--<div style="background-color:<?php echo $color?>;padding:10px 10px;"> -->
					<?php
						
						
					}
				}
				else
				{
					echo "No items on your cart yet.";
				}
				?>
				<div class="page-line cl body_bottom_1px" style="" >
				</div>
				<?php
				if(isset( $grand_total) && $grand_total > 0)
				{
				?>
				<div class="container p5px_0px ">
					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Sub Total: S$ <span id="sub_total">
								<?php echo number_format($grand_total + $delivery_total, 2);?>
							</span>&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							GST: S$ <span id="gst">
								<?php echo number_format(($grand_total * 0.07) ,2);?>
							</span>&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Total: S$ <span id="grand_total">
								<?php echo number_format(($grand_total * 0.07) + $grand_total + $delivery_total ,2);?>
							</span>&nbsp;
						</div>
					</div>
					<div class="row hide">
						<div class="col-xs-12" align="right" style="font-size:14px;font-weight:bold">
							Total: USD$ <span id="grand_total_usd">
								<?php echo number_format((($grand_total+$delivery_total)*BRAND_FOREX_AMEX),2);?>
							</span>&nbsp;
						</div>
					</div>
					<br>
					<div class="row">
                        <div class="col-xs-12 col-sm-8" style="">
                            <label for="chk_confirm" style="font-weight:normal"><input type ="checkbox"  style="" id="chk_confirm" />&nbsp;I understand that card purchases processed are not refundable nor returnable.</label>
                        </div>
                        <div class="col-xs-12 col-sm-4" align="right" style="font-size:14px;font-weight:bold">
                            <button id="checkout" class="btn-custom-white_grey_normal" style="font-size:16px !important">Checkout</button>
                        </div>
					</div>
				</div>
				<?php
				}
				?>
				
                    
                    
            </div>


			<!--body-->

				<div class="page-line" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer;?> ">
					<div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
					   <button type="button" id="tour_done" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Done</button>
					   <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
					</div>
				</div>
				<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->

        <!-- /footer -->
        <!-- /footer -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/m/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
        <div id="fb-root"></div>
        <script>
            var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
			var AMEX_CARD_BIN									="<?php echo AMEX_CARD_BIN;?>";
            var from_android = true;
            var is_user_logged_in ="<?php echo $this->session->userdata('is_user_logged_in');?>";
			var winery_count = <?php echo $winery_count;?>;
            <?php
                $frm_and = $this->session->userdata('from_android');
                if (empty($frm_and)) {
            ?>
                var from_android = false;/*it should be false */
            <?php } ?>
            
            var GlompFormDialog = jQuery('form#GlompFormDialog').dialog({
                autoOpen: false,
                dialogClass : 'dialog_style_glomp',
                width: '80%',
                modal: true,
                buttons: [
                    {
                        text: 'Confirm',
                        class: 'fl btn-custom-blue-glomp_xs w100px',
                        click: function() {
                            GlompFormDialog.submit();
                        },
                        style: 'margin:1px;'
                    },
                    {
                        text: 'Cancel',
                        class: 'fr btn-custom-ash_xs w100px',
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }
                ]
            });
            
            var GlompFormAlert = jQuery('<div class="form-alert" />').dialog({
                autoOpen: false,
                width: '70%',
                modal: true,
                dialogClass: 'dialog_style_glomped_alerts',
                minHeight: 'auto'
            });
            
            var GlompFormPreload = jQuery('<div class="form-preload" />').dialog({
                autoOpen: false,
                width: '60%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait',
                title: 'Please wait...',
                minHeight: 'auto'
            }).html('<div style="text-align:center"><img width="40" src="/assets/m/img/ajax-loader.gif" /></div>');
            
            var GlompShareDialog = jQuery('#share-dialog').dialog({
                autoOpen: false,
                width: '90%',
                modal: true,
                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                minHeight: 'auto'
            });
            
            $(function() {

                $('#back_btn').click(function(e) {
                   e.preventDefault();
                   window.history.back();
                });
                $("#tour_cancel").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
				$("#tour_done").click(function() {
					window.location.href='<?php echo base_url() ?>m/user/dashboard/?inTour=4';
				});
                $("#main_menu").click(function() {
					$("#hidden_menu").toggle();
                });
               
			   <?php if ($winery_count==1) echo "alert_winery();";?>
                
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});

            jQuery(document).on('click','.user-menu .user-icon .user-image', function(e){
                jQuery('.user-menu .user-icon .user-details').toggle();
            });
            
            jQuery(document).on('click','.toggle',function(e){
                e.preventDefault();
                toggleIn = jQuery(e.target).attr("toggle-in");
                toggleOut = jQuery(e.target).attr("toggle-out");
                
                jQuery(toggleIn).show();
                jQuery(toggleOut).hide();
            });

        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
		<script src="<?php echo minify('assets/m/js/custom.glomp_amex.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
</html>
