<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">        
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="">            
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>		
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>        
        <script type="text/javascript">                       
            $(function() {
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
                });                        
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-points-logo.png');"></div>
                    </a>
                </div>
            </div>

            <!-- hidden navigations      
            <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>                
            </div>
            <!-- hidden navigations -->
			
			<!--body-->
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <div class="content alert alert-success" align="center" style="margin:130px 0px;padding:30px 10px;font-size:14px" id="message_content">                    
                    <span style="font-size:14px" >Sorry but there was an unexpected error. Please <a href="<?php echo base_url(MOBILE_M.'/buyPoints');?>">try again</a>.</span>                    
                </div>                        
            </div>
			<!--body-->
            <div class="cl p20px"></div>    
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->
        
    </body>
</html>
