<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/megapix-image.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/exif.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
        </script>
    </head>    
    <body style="background: white;">
        <?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" align="center" style="background-color: #D2D7E1">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>" class="white ">
                                        <div class="w100per fl white">
                                        Back
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp_register.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations       
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>">
                        <div class="hidden_nav" align="left">
                            Back
                        </div>
                    </a>                    
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="p10px_0px global_margin" style="font-size: 14px;font-weight: bold; color:#808283; padding-top:13px;">
                

                <form id="register_form" method="post" action=<?php echo site_url('/m/landing/register_form'); ?> enctype='multipart/form-data'>
                    <input type="hidden" name="token" value="<?php echo $token; ?>" />
                    

                    <!-- FACEBOOK FIELDS -->
                    <input type="hidden" name="fb_user_id" id="fb_user_id" value="<?php echo $this->input->get_post('fb_user_id')?>" />
                    <input type="hidden" name="fb_fname" id="fb_fname" value="<?php echo $this->input->get_post('fb_fname')?>" />
                    <input type="hidden" name="fb_mname" id="fb_mname" value="<?php echo $this->input->get_post('fb_mname')?>" />
                    <input type="hidden" name="fb_lname" id="fb_lname" value="<?php echo $this->input->get_post('fb_lname')?>" />
                    <input type="hidden" name="fb_email" id="fb_email" value="<?php echo $this->input->get_post('fb_email')?>" />
                    <input type="hidden" name="fb_gender" id="fb_gender" value="<?php echo $this->input->get_post('fb_gender')?>" />
                    <input type="hidden" name="fb_birthdate" id="fb_birthdate" value="<?php echo $this->input->get_post('fb_birthdate')?>" />
                    <input type="hidden" name="fb_picture" id="fb_picture" value="<?php echo $this->input->get_post('fb_picture')?>" />

                    <!-- LINKEDIN FIELDS -->
                    <input type="hidden"  id="li_user_id" name="li_user_id" value="<?php echo $this->input->get_post('li_user_id')?>" />
                    <input type="hidden"  id="li_emailAddress" name="li_emailAddress" value="<?php echo $this->input->get_post('li_emailAddress')?>" />
                    <input type="hidden"  id="li_firstName" name="li_firstName" value="<?php echo $this->input->get_post('li_firstName')?>" />
                    <input type="hidden"  id="li_lastName" name="li_lastName" value="<?php echo $this->input->get_post('li_lastName')?>" />
                    
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;min-height: 100px;">
                        <?php
                        if (validation_errors() != "") {
                            echo "<div class=\"alert alert-error\" id=\"errors-flat\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . validation_errors() . "</div>";
                        }
                        if (isset($error_msg)) {
                            echo "<div class=\"alert alert-error\" id=\"errors-flat\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error_msg . "</div>";
                        }
                        ?>
                        <div class="register_form_gray" align="left">
                            <input name="fname" id="fname" type="text" value="<?php echo $this->input->post('fname'); ?>" placeholder="First Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="register_form_gray" align="left">
                            <input name="lname" id="lname" type="text" value="<?php echo $this->input->post('lname'); ?>" placeholder="Last Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="border_topbot_gray" style="padding-right: 15px;" align="left">
                            <!-- row1 -->
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;">Date of Birth</div>
                                <div class="fl">
                                    <div class="fl">
                                        <input name="day" id="day" maxlength="2"  value="<?php echo $this->input->post('day'); ?>" class="register_form_gray" type="text" placeholder="DD" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 15px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="month" id="month" maxlength="2"  value="<?php echo $this->input->post('month'); ?>"  class="register_form_gray" type="text" placeholder="MM" style="width: 34px; border: 0px solid; font-weight: bold;margin-right: 0px;" />
                                    </div>
                                    <div class="fl">
                                        <input name="year" id="year" maxlength="4" value="<?php echo $this->input->post('year'); ?>"  class="register_form_gray" type="text" placeholder="YYYY" style="width: 49px; border: 0px solid; font-weight: bold;margin-left: 15px;margin-right: 7px;" />
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row2 -->
                            <div class="fl" style="width: 100%"> 
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Display</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="dob_display" checked id="dob_display_w_year" value="dob_display_w_year"  /> DD / MM / YYYY
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="dob_display" id="dob_display_wo_year"  value="dob_display_wo_year"> DD / MM
                                    </div>
                                </div>    
                                <div class="cl"></div>
                            </div>
                            <!-- row2 -->
                            <div class="fl" style="width: 100%">
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Gender</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo ($this->input->post('gender') != 'Female') ? '' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="gender" id="gender-female" <?php echo ($this->input->post('gender') == 'Female') ? '' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="cl"></div>
                            <!-- row2 -->
                        </div>
                        <div class="register_form_gray" align="left">
                            <select name="location" id="location" class="textBoxGray" style="width:100%">
                                <option value=""><?php echo $this->lang->line('Select_Location'); ?></option>
                                <?php echo $this->regions_m->location_dropdown(0, set_value('location')); ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;" align="left">
                        <div class="cl"></div>
                        <div class="fl img-framed" style="margin-right:5px;">
                            <div>
                            <img src="" alt="<?php /* echo $user_record->user_name; */ ?>" id="profile_pic">
                            </div>
                        </div>
                        <div class="fl img-framed" style="margin-right:5px;position:absolute; top: -1000px;">
                            <div>
                                <img src="" alt="<?php /* echo $user_record->user_name; */ ?>" id="profile_pic_2">
                            </div>
                        </div>
                        <div class="fl" style="border:0px solid; max-width:170px">
                            <div class="upload">
                                <a href="javascript:;" id="uploadFilePic" ><?php echo $this->lang->line('upload_profile_photo'); ?></a>
                                <span id="upload_loader"></span>
                                <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note'); ?></p>
                            </div>
                        </div>
                        <div class="cl"></div>
                        <input type="file" name="user_photo" id="profilePhotoUpload" accept="image/*" style="visibility:hidden; position:absolute" />
                        <div id="no-preview-support" style="color: #900"></div>
                        <script>
                            if ( !(window.File) ) {
                                jQuery('#no-preview-support').html('* Image preview is NOT supported when uploading from this device.');
                            }
                        </script>
                    </div>
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;">
                        <div class="fl" style="width: 100%;margin-left: 18px;margin-top: 10px;margin-right: 0px;padding-right: 32px;display:none" align="left">
                            <div class="fl"><input type="checkbox" name="chk_notif_via_fb" id="chk_notif_via_fb" value ="Y" /></div>
                            <div class="fl" style="margin-left: 11px;">Notify via Facebook Message</div>
                        </div>
                        <div class="cl"></div>
                        <div class="register_form_gray" align="left">
                            <input name="email" id="email" type="email" value="<?php echo $this->input->post('email'); ?>" placeholder="Email" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                    </div>
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;">
                        <div class="fl">
                            <div class="register_form_gray" align="left">
                                <input name="pword" id="pword" type="password" maxlength="9" placeholder="Enter password" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                            <div class="register_form_gray" align="left">
                                <input name="cpword" id="cpword" type="password"  maxlength="9"  placeholder="Confirm password" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; max-width: 100%">
                            <?php echo $this->lang->line('passwors_tips','You will need to enter this Password when sending and redeeming products so please remember it.'); ?><br />
                            <?php echo $this->lang->line('passwors_tips_length','(6-9 characters)'); ?>
                        </div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="global_border form-fieldset">
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; margin-top: 10px;">
                            <?php $temp = $this->lang->line('Promotional_Code');
                            echo str_replace(' ', '&nbsp;', $temp)
                            ?>
                        </div>
                        <div class="fl" align="left">
                            <div class="register_form_gray" align="left">
                                <input value="<?php echo $this->input->post('promo_code'); ?>" maxlength="40" name="promo_code" id="promo_code" type="text" placeholder="(Optional)" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="">
                        <div class="fl" style="margin-left: 14px;"><input name="agree" id="agree" type="checkbox" /></div>
                        <div class="fl" style="margin-left: 10px;"><label for="agree">I agree to the <a href="<?php echo base_url('index.php/page/index/terms'); ?>" target="_blank"><span style="color:#3ACD00">Terms & Conditions</span></a></label></div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="">
                        <div class="fl" style="margin-left: 14px; margin-top: 10px">
                            <button type="submit" id="register_btn" class="btn-lg btn-custom-blue-grey btn-block w202px" >Submit</button>
                        </div>
                        <div class="fl" style="margin-left: 14px;margin-top: 10px">
                            <a href="<?php echo site_url(MOBILE_M . '/landing/register_landing'); ?>" class="btn-lg btn-custom-blue-grey btn-block w202px" type="submit">Cancel</a>
                        </div>
                        <div class="cl"></div>
                    </div>
                </form>
            </div>
            <!-- /container -->
        </div> <!-- /global_wrapper -->

        <div id="fb-root"></div>
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : FB_APP_ID,
                    xfbml      : true,
                    version    : 'v2.0',
                    status     : true,
                    cookie     : true
                });
				FB.getLoginStatus(function(r){
					if(r.status == 'connected') {
						jQuery(document).trigger('fbConnected');
					}
				});
                
            };
            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>



<script type="text/javascript">
    jQuery(window).load(function(){
    var dataUrl = false;    
    
    var Loader = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
    Loader.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
        title: 'Please wait...',
        modal: true,
        position: 'center',
        width: 200,
        height: 120
    }).extend({
        open: function(){
            jQuery(this).dialog('open');
        },
        close: function(){
            jQuery(this).dialog('close');
        }
    });
    
    var Prompt = jQuery('<div id ="errors" align="left" ><div class="alert alert-error" id ="errors_inner" style="font-size:11px !important; margin: 0px !important;" ></div></div>');
    Prompt.dialog({
        autoOpen: false,
        closeOnEscape: false ,
        resizable: false,					
        dialogClass:'dialog_style_glomp_wait noTitleStuff ',
        modal: true,
        title:'Please wait...',
        position: 'center',
        width:280,                                
        buttons:[
            {text: "Close",
            "class": 'btn-custom-white_xs',
            click: function() {
                $(this).dialog("close");
                setTimeout(function() {
                    
                }, 500);
            }}
        ]
    });
    
    var ImageToUpload = jQuery('<div id ="ImageToUpload"></div>');
    ImageToUpload.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
        title: '',
        modal: true,
        show: '',
        hide: '',
        buttons: [
            {text: "Upload from Device",
             class: "btn-custom-green w200px2",
             click: function(){
                    jQuery('#profilePhotoUpload').trigger('click');
                    dataUrl = false;
                    ImageToUpload.dialog('close');
                }
            },
            {text: "Import from Facebook",
             class: "btn-custom-blue_fb w200px2",
             click: function(){
                    /*useFacebookPicture();*/
                    FB.getLoginStatus(function(r){
                        if(r.status == 'connected') {
                            FB.api('/me/permissions', function(response) {
                                var email_perm = false;
                                for( var key in response.data ) {
                                    if (response.data[key].permission === 'email') {
                                        email_perm = true;
                                    }
                                }

                                if( ! email_perm) {
                                    var return_url = GLOMP_BASE_URL +  'm/landing/register_form?js_func_fb=grabFBProfilePic()';
                                    return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=email");
                                } 
                            });
                            
                            grabFBProfilePic();
                            ImageToUpload.dialog('close');
                        } else {
                            var return_url = GLOMP_BASE_URL +  'm/landing/register_form?js_func_fb=grabFBProfilePic()';
                            return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=email");
                        }
                    });
                }
            },
            {text: "Cancel",
             class: "btn-custom-ash_xs w200px2",
             click: function(){
                    ImageToUpload.dialog('close');
                }
            }
        ]
    }).css('min-height','0px');
    /*TODO: REMOVE THIS FUNCTION IN HERE */
    function grabFBProfilePic() {
        FB.api('/me/picture?width=280&height=280', function(response){
            jQuery('#profile_pic').attr("src",response.data.url);
            jQuery('#fb_picture').val(response.data.url);
            jQuery('#profilePhotoUpload').val('');
        });
    }
    var ImageTooLarge = jQuery('<div id ="ImageTooLarge">Due to the large file size, it may take longer to upload your photo. Please wait until it is complete or select a new photo to upload.</div>');
    ImageTooLarge.dialog({
        autoOpen: false,
        closeOnEscape: false ,
        resizable: false,					
        dialogClass:'dialog_style_glomp_wait noTitleStuff ',
        modal: true,
        title:'Please wait...',
        position: 'center',
        width:280,                                
        buttons:[
            {text: "Select Another Image",
             class: "btn-custom-green w200px2",
             click: function() {
                dataUrl = false;
                $(this).dialog("close");
                jQuery('#profilePhotoUpload').trigger('click');
            }},
            {text: "Use this image",
            "class": 'btn-custom-green w200px2',
            click: function() {
                resize_image($("#profile_pic")[0], $('#profile_pic_2')[0]);
                $(this).dialog("close");
            }}
        ]
    });
    
        
    jQuery('.ui-dialog').css('position','fixed');

    
    /* FORM VALIDATION */
    $('#register_form').on('submit',function(e) {
        e.preventDefault();
        Loader.open();
        var data = $(this).serializeArray();
        console.log(dataUrl);
        if (dataUrl != false) {
            $('#profilePhotoUpload').val('');
            var image_64 = dataUrl.replace(/^data:image\/(jpeg|png|jpg);base64,/, "");
            data.push({name: 'image_64', 'value' : image_64});
            //$('#register_form').append('<input type ="hidden" id = "image_64" name = "image_64" value = "'+image_64 +'" />');
        }
        $(this).ajaxSubmit({
            type: "post",
            url: $(this).attr('action'),
            dataType: 'json',
            data: data,
            success: function(response) {
                Loader.close();
                if (response.success == 'false') {
                    var msg='';
                    jQuery('.error_border').removeClass('error_border');
                    jQuery.each(response.errors, function(key,val) {
                        jQuery('#'+key).addClass('error_border');
                        if(key=='user_photo')
                            jQuery('#profile_pic').addClass('error_border');
                        msg += val;
                    });
                    Prompt.find('div').html(msg);
                    Prompt.dialog('open');
                    return;
                }

                window.location = response.location;
            }
        });
    });
        
        
        function useFacebookPicture() {
            FB.getLoginStatus(function(r){
                if(r.status == 'connected') {
                    FB.api('/me/picture?width=280&height=280', function(response){
                        jQuery('#profile_pic').attr("src",response.data.url);
                        jQuery('#fb_picture').val(response.data.url);
                        jQuery('#profilePhotoUpload').val('');
                    });
                } else {
                    FB.login(function(r){
                        if(r.status == 'connected') {
                            FB.api('/me/picture?width=280&height=280', function(response){
                                jQuery('#profile_pic').attr("src",response.data.url);
                                jQuery('#fb_picture').val(response.data.url);
                                jQuery('#profilePhotoUpload').val('');
                            });
                        }
                    });
                }
            });
        }

        jQuery(document).on('fbConnected', function(){
        
            if (window.location.search.search('js_func_fb') > 0 && window.location.search.search('error')  == -1) {
                grabFBProfilePic();
            }

            if(window.location.href.indexOf('facebook')) {

                if( jQuery('#fb_user_id').val() != '' ) {
                    jQuery('#fname').val( jQuery('#fb_fname').val() + ' ' + jQuery('#fb_mname').val() );
                    jQuery('#lname').val( jQuery('#fb_lname').val() );
                    jQuery('#email').val( jQuery('#fb_email').val() );
                    dob = jQuery('#fb_birthdate').val().split('/');
                    jQuery('#day').val( dob[1] );
                    jQuery('#month').val( dob[0] );
                    jQuery('#year').val( dob[2] );
                    jQuery('#fb_picture').val( '1' );
                    if( jQuery('#fb_gender').val() == 'male' ) {
                        jQuery('#gender-male').attr('checked','checked');
                    }
                    if( jQuery('#fb_gender').val() == 'female' ) {
                        jQuery('#gender-female').attr('checked','checked');
                    }
                    FB.getLoginStatus(function(r){
						
                        if(r.status == 'connected') {
                            useFacebookPicture();
                        } else {
                            FB.login(function(r){
                                if(r.status == 'connected') {
                                    useFacebookPicture();
                                }
                            });
                        }
                    });
                }
            }
            
        });
        
        
            $(function() {
                profilePic = {
                    UpdatePreview: function(obj) {

                        var reader = new FileReader();

                        reader.onload = function(e) {
                            var target = e.target || e.srcElement;
                            var upload_string = target.result;
                            var n = upload_string.search(':image/');
                            /*find for :image/*/
                            if ( target.result ) {
                                $("#profile_pic").attr("src", target.result);
                                
                                if(obj.files[0].size > 500000) {
                                    resize_image($("#profile_pic")[0], $('#profile_pic_2')[0]);
                                    //ImageTooLarge.dialog('open');
                                } else {
                                    resize_image($("#profile_pic")[0], $('#profile_pic_2')[0]);
                                }

                                
                                jQuery('#fb_picture').val('');
                                jQuery('#no-preview-support').html('File chosen: '+obj.files[0].name);
                                profilePic.AdjustPreview(jQuery("#profile_pic"));
                                
                                /*console.log(image_64);
                                console.log(image_64.length +': resize val');
                                console.log(target.result.length +': true val');*/
                            } else {
                                alert("Invalid image extension.");
                            }
                            //$('#upload_loader').html('');
                        };
                        reader.readAsDataURL(obj.files[0]);
                    },
                    AdjustPreview: function(obj) {
                        /*obj.css('margin','0');
                        if( obj[0].naturalWidth >= obj[0].naturalHeight ) {
                            /* landscape oriented 
                            var diff;
                            obj.height(70);
                            obj.width('auto');
                            diff = obj.width() - 70;
                            diff = diff/2;
                            obj.css('margin-left','-'+diff+'px');
                        } else {
                            /* portrait oriented 
                            obj.height('auto');
                            obj.width('70');
                            obj.css('margin-top','-20%');
                        }*/
                    }
                };
            });
        
        jQuery('#profilePhotoUpload').on('change', function(e){
            if(jQuery(this).val() != '') {
                /*if( window.File ) {
                    var file = e.target.files[0];
                    var url = window.URL.createObjectURL(file);
                    jQuery('#profile_pic').attr('src',url);
                    jQuery('#fb_picture').val('');
                    console.log(url);
                    if(jQuery('#no-preview-support').html() != '') {
                        jQuery('#no-preview-support').html('File chosen: '+file.name);
                        return;
                    }
                }*/
                if( window.FileReader) {
                    profilePic.UpdatePreview(this);
                }
            }
        });
        
        function resize_image(src, dst, type, quality) {
            var tmp = new Image(), canvas, context, cW, cH;
            
            type = type || 'image/jpeg';
            quality = quality || 0.92;
            
            cW = src.naturalWidth;
            cH = src.naturalHeight;

            tmp.src = src.src;
            tmp.onload = function() {
               canvas = document.createElement( 'canvas' );
               //var context = canvas.getContext( '2d' );
               var cW = 140;
               var cH = 140;
               canvas.width = cW;
               canvas.height = cH;

                var mpImg = new MegaPixImage(tmp);
                var orientation;
                EXIF.getData(this, function() {
                    orientation = EXIF.getTag(this, "Orientation");
                });
                
                mpImg.render(canvas, { width: cW, height: cH, orientation: orientation });
                dst.src = canvas.toDataURL( type, quality );
                dataUrl = dst.src;
                return;
            };
            return;
        }
        
        
        /* LINKED IN */
        if(window.location.href.indexOf('linkedin')) {

            if( jQuery('#li_user_id').val() != '' ) {
                jQuery('#fname').val( jQuery('#li_firstName').val() );
                jQuery('#lname').val( jQuery('#li_lastName').val() );
                jQuery('#email').val( jQuery('#li_emailAddress').val() );
            }
        }

        jQuery('#uploadFilePic, #profile_pic').on('click', function(e){
            e.preventDefault();
            ImageToUpload.dialog('open').css('min-height','0');
        })


        var prompt = jQuery('<div />');
        prompt.dialog({
            autoOpen:false,
            resizable:false,
            dialogClass:'dialog_style_glomp_wait noTitleStuff',
            modal:true,
            width:300,
            buttons:[
                {
                    text: 'Close',
                    class: 'btn-custom-white_xs',
                    click: function(){
                        prompt.html('');
                        prompt.dialog('close');
                    }
                }
            ]
        });
        jQuery('#pword,#cpword').on('keydown',function(e){
            var k = e.keyCode
            if(k==9||k==8||k==20||k==18||k==17||k==16||k==13||k==27||k==91) return;
            if(e.target.value.length == 9) {
                prompt.html( '<div class="_error alert alert-error">'+jQuery(e.target).attr('prompt-max-reached')+'</div>' );
                prompt.dialog('open');
            }
        });
    
    });
</script>

</body>
</html>
