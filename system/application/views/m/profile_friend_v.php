<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">                    
    <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style=""  >		                   
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
			<div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>

                        </nav>
                    </div>	
                    <div class="glomp_header_logo_2" style="background-image:url('<?php echo base_url()?>assets/m/img/glomp-friends-logo.png');"></div>
                </div>
                <!-- hidden navigations      
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                </div>
                <!-- hidden navigations -->	
            </div>		             
            <div class="p20px_0px" style="margin-top:12px;"></div>	
            <div class="container p20px_0px global_margin " style="background-color: white;font-size: 8px;font-weight: bold; color:#3D5067">
                <div class="row">
                    <div class="fl">
                        <div style="font-size: 16px;" ><?php echo $user_record->user_fname; ?>  <?php echo $user_record->user_lname; ?></div>
                        <div style="font-size: 13px;margin:5px 0px;"  >
                            <div><?php echo $this->regions_m->region_name($user_record->user_city_id); ?></div>
                            <?php 
                                $dob = explode('-',$user_record->user_dob);
                                if(count($dob)!==3){
                                    $dob[0] = 0;
                                    $dob[1] = 0;
                                    $dob[2] = 0;
                                }
                            
                                $dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
                                if ($user_record->user_dob_display == 'dob_display_wo_year') {
                                    $dob = date('M d', strtotime($dob));
                                } else {
                                    $dob = date('M d, Y', strtotime($dob));
                                }
                            if($user_record->user_dob!="")
                                {
                            ?>
                                    <div>DOB: <?php echo $dob; ?></div>                                                                                      
                                <?php }?> 
                        </div>
                    </div>
                    <div class="fr">
                        <div class="fr" style="margin-bottom:5px;width: 90px; height: 100px; border:0px solid #333;background:url('<?php echo base_url($profile_pic); ?>');background-position:center;background-size:contain;background-repeat:no-repeat;" >							
						</div>
                    </div>                
                    <?php
                    $profile_id = $user_record->user_id;
                    $user_id = $this->session->userdata('user_id');
                    $is_friend = $this->users_m->is_friend($user_id, $profile_id);
                    if ($is_friend <= 0 && ($user_record->user_status != 'Pending') ) {
                        ?>
						<a class="white" href="<?php echo base_url(MOBILE_M.'/profile/add_friend/'.$profile_id) ?>">
							<div class="fr glomp_add" style="background-image:url('<?php echo base_url('assets/m/img/glomp-add.png') ?>');"></div>
						</a>
                    <?php } ?>
                </div>
				<div class="cl row">					
                    <div class="fl">					
                        <a class="white" href="<?php echo base_url(MOBILE_M.'/profile/menu/'.$profile_id.'/?tab=favourites') ?>">
                            <div align="center" style="width: 120px; height:44px;background-color:#ABD03A !important;color:white;padding: 13px 0px 4px 0px; font-size: 14px;">Menu</div>
                        </a>
                    </div>
                    <!--<div class="fr">
                        <div style="width: 90px;height:44px; background-color: red;color:white;padding: 2px;" align="center">
                            <span style="font-size:12px;font-weight: normal">Remaining</span> 
                            <span style="font-size: 16px;font-weight: bold"><?php echo $points; ?></span>&nbsp;<span style="font-size:12px;font-weight: normal">pts</span>  
                        </div>
                    </div>-->
                </div>                
            </div>
            <!-- /container -->
			<?php 
			if(($user_record->user_status != 'Pending'))
			{ ?>
            <div class="body_topbottom_1px">
				<div class="container p10px_0px margin_left">                    					                
                    <div class="row red harabarabold" style="font-size: 20px;">Activity</div>
                </div>
            </div>
            <div class="buzzScroll nano">

                <?php $bg = ($glom_buzz_count == 0) ? "#d3d7e1" : "#fff"; ?>

                <div class="content" style=" background:<?php echo $bg; ?>" align="left" id="glomp_buzz_content" >

            <?php
			}
			
            if ($glom_buzz_count > 0  && $user_record->user_status != 'Pending') {
                $base_url = site_url(MOBILE_M);
                $to_a = $this->lang->line('to_a');
                foreach ($glomp_buzz->result() as $glomp) {

                    /*product and merchant info*/
                    $prod_id = $glomp->voucher_product_id;
                    $prod_info = json_decode($this->product_m->productInfo($prod_id));
                    if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check_m->underage() ) continue;
                    $prod_name = $prod_info->product->$prod_id->prod_name;
                    $prod_image= $prod_info->product->$prod_id->prod_image;        
                    $product_logo = $this->custom_func->product_logo($prod_image);
                    $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
                    $merchant_name = $prod_info->product->$prod_id->merchant_name;
                    
                    $from_id = $glomp->voucher_purchaser_user_id;
                    $to_id = $glomp->voucher_belongs_usser_id;
                    $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                    $ago_date = $glomp->voucher_purchased_date;

                    $form_name = $frn_info->friends->$from_id->user_name;
                    $form_name_real = $frn_info->friends->$from_id->user_name;

                    if ($buzz_user_id == $from_id) {
                        $form_name = $this->lang->line('You');
                    }

                    $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

                    $to_name = $frn_info->friends->$to_id->user_name;
                    $to_name_real = $frn_info->friends->$to_id->user_name;
                    if ($buzz_user_id == $to_id) {
                        $to_name = $this->lang->line('You');
                    }
                    
                    $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                    $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
                    
                    $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
                    $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;

                    $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
                    $story_type="1";
                    if($from_id!=$this->session->userdata('user_id'))
                    {
                        $story_type="2";
                    }
                    ?>
                    <div class="body_bottom_1px" data-id="<?php echo $start_count;?>">        
                        <div class="container p10px_0px global_margin ">
                            <div class="row">
                                <div class="fl" style="width:73%;">
                                    <?php if ($from_id == 1) { ?>
                                        <a class="red" href="javascript:void(0);"><?php echo $form_name; ?></a>
                                    <?php } else { ?>
                                        <a class="red" href="<?php echo $base_url . '/profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a>
                                    <?php } ?> &nbsp;glomp!ed <a class="red" href="<?php echo $base_url . '/profile/view/' . $to_id; ?>"><?php echo $to_name; ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?>                                    
                                    &nbsp;<span class='red'><?php  echo str_replace(' ','&nbsp;',($this->custom_func->ago($ago_date))); ?></span>
                                </div>
                                <div class="fr" style="width:20%;border:0px solid #333;" align="right">
                                    <div class="fr">
                                        <!--<div><button class="btn-custom-blue-grey_xs w50px"  >Like</button></div>-->												
                                        <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs w50px" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line(''); ?>share</button>
                                        <div style="height:0px;">
                                            <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                                <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_li_id="<?php echo $from_li_id;?>" data-to_li_id="<?php echo $to_li_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="dashboard_m" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                            </div>
                                        </div>
                                    </div>	
                                </div>    
                            </div>                            
                        </div>
                    </div>
                    <?php
                    $start_count++;
                }
            }
			else
			{
            ?>
				<div class="body_topbottom_1px">
					<div class="container p10px_0px global_margin "  style="color:#888;font-size:14px;">				
						We are awaiting <b><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?></b> to verify their account. Go ahead and glomp! <b><?php echo $user_record->user_fname ?></b> more!																
					</div>
				</div>			
			<?php			
			}
			?>
            </div>
            </div>
            
            <div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>     
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>    
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script>
            var FB_APP_ID                               ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            var loadingMoreStories=false;
            $(function() {
                $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();
                });
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
            function loadMore()
            {
                if(!loadingMoreStories)
                {
                    loadingMoreStories=true;            
                    start_count=$('#glomp_buzz_content').children().last().data().id;
                    if(start_count>0)
                    {
                        $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">Loading more stories...<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                        $('#glomp_buzz_content').append($loader);
                        data ='start_count='+start_count+'&from_view=profile&profile_id='+<?php echo $profile_id;?>;
                        setTimeout(function() {
                            $.ajax({
                                type: "POST",
                                dataType: 'html',
                                url: '<?php echo base_url();?>m/profile/getMoreStories',
                                data: data,
                                success: function(response){ 
                                    if(response!='' &&  response!='<!---->')
                                    {
                                         $('#id_loader').remove();
                                        $('#glomp_buzz_content').append(response);
                                        $(window).bind('scroll', bindScroll);
                                        loadingMoreStories=false;
                                        
                                        $('.share_on_facebook, .share_on_linkedin, .share_on_twitter').unbind('click');
                                        $('.share_on_facebook').click(function(){                
                                            $this=$(this);
                                            checkLoginStatus($this,'share');
                                        });
                                        $('.share_on_linkedin').click(function(){                
                                            $this=$(this);        
                                            $data=$(this).data();
                                            doLinkedInShare($data);        
                                        });    
                                        $('.share_on_twitter').click(function(){                
                                            $data=$(this).data();
                                            doTwitterShare($data)
                                            
                                        });
                                        
                                        $('.glomp_fb_share').unbind('click');
                                        $('.glomp_fb_share').click(        
                                            function(e){
                                                $this=$(this);
                                                $data=$this.data();                          
                                                
                                                if($data.voucher_id in share_wrapper)
                                                {
                                                 
                                                    clearTimeout( share_wrapper[$data.voucher_id] );                
                                                    delete share_wrapper[$data.voucher_id];
                                                    
                                                }
                                                else
                                                {
                                                    
                                                }
                                                
                                                $('.glomp_fb_share').each(function(){
                                                    $this_temp=$(this);
                                                    $data_temp=$this_temp.data();                 
                                                    if($data_temp.voucher_id!=$data.voucher_id){                    
                                                        $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                                                        clearTimeout( share_wrapper[$data.voucher_id] );                
                                                        delete share_wrapper[$data.voucher_id];
                                                    }
                                                    else
                                                    {
                                                        /*$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); */
                                                    }
                                                });
                                                $('#share_wrapper_box_'+$data.voucher_id).toggle();            
                                                e.stopPropagation();
                                            });

                                    }
                                    else
                                    {                                
                                        $('#id_loader').remove();
                                        $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">No more stories to load.</div>');
                                        $('#glomp_buzz_content').append($no_more);
                                    }
                                }
                            });
                        }, 1);
                    }                    
                    /*console.log($('#glomp_buzz_content').children().last().data().id);*/
                    /*xx=1;*/
                }
               
             }
             function bindScroll(){
               if($(window).scrollTop() + $(window).height() > $(document).height() -10) {
                   $(window).unbind('scroll');
                   loadMore();
               }
            }             
            $(window).scroll(bindScroll);
        </script>        
        <script src="<?php echo base_url() ?>assets/m/js/custom.glomp_share.js"></script>        

    </body>
</html>
