<?php
	if(!isset($user_start_tour))/*/check for tour*/
		$user_start_tour='N';	
		
	$hasPromoResponse='Y';
	$promo_prod_details='';
	if($res_info_prod=="")
		$res_info_prod="''";
	if($user_promo_code=="")
    {
		$hasPromoResponse='N';
		$user_promo_code="''";
	}
	else
	{
		$promo_res=($user_promo_code);		
	}	
    
    $is_reactivated =  $this->session->userdata('reactivated');
    if($is_reactivated=='true')
        $this->session->unset_userdata('reactivated');
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $page_title; ?></title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">	
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
    <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
</head>
<body style="background-color:#FFFFFF; border-left: #B0B0B0  solid 1px;	border-right: #B0B0B0 solid 1px;		">
	<?php include_once("includes/analyticstracking.php") ?>
	<?php
	if ($this->session->userdata('user_id') && $this->session->userdata('user_id') > 0) {

		$user_id = $this->session->userdata('user_id');
		$res_header = json_decode($this->user_account_m->user_summary($user_id));
		#user info
		$user_info = json_decode($this->user_account_m->user_short_info($user_id));
		$active_voucher = $this->users_m->count_active_voucher($user_id);
	}	
	?>
    <div class="global_wrapper" align="center">		      
        <!--
        <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
        <div class="navbar navbar-default" style="position:fixed;width: 100%;top:0px;left: 0px;border-left: #B0B0B0  solid 1px;	border-right: #B0B0B0 solid 1px;">
            <div class="header_navigation_wrapper fl" align="center">				                
                <div class="glomp_header_logo_3" id="" style="background-image:url('<?php echo base_url() ?>assets/m/img/logo.png');background-size: 120px;background-position:center;border:0px solid #333;width:100%;height:40px;margin:10px 5px 10px 0px;background-repeat:no-repeat;z-index:100;"></div>
            </div>
            <div class="cl"></div>
        </div>	
        -->                
        <div class="navbar navbar-default ">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="header_navigation_wrapper" style="display: table;">        
				<div class="header_logo_wrapper" style="" align="center">
					<div class="c_pointer hidden_menu_glomp_class  glomp_header_logo" id="glomp_menu" style="background-image:url('<?php echo base_url() ?>assets/m/img/logo.png');"></div>
				</div>					
                <div class="fr header_logos_wrapper" >
                    <div class="header_logos_wrapper_sub_1" style="" >
                        <a class="" href="<?php echo base_url(MOBILE_M . '/glomp/') ?>">
                            <div class="header_icons_thumb_wrapper fl" align="center" >
                                <div class="header_icons_thumb " style="background-size:26px;background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_glomped_icon.png');">
                                    <?php if ($active_voucher > 0) { ?>
                                        <div class="red_notification"><?php echo $active_voucher; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="header_icons_text" style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_glomped_text_2.png');">&nbsp;</div>						
                            </div>
                        </a>
						<a class="" href="<?php echo base_url(MOBILE_M . '/user/searchFriends') ?>">
							<div class="header_icons_thumb_wrapper fl" align="center" >
								<div class="header_icons_thumb" style="background-size:34px;background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_notify_req_icon_2.png');"></div>
								<div class="header_icons_text" align="center" ><?php echo $this->lang->line('Friends'); ?></div>
							</div>
						</a>
                    </div>
                    <div class="header_logos_wrapper_sub_1" >
                        <div id="main_menu"  class="header_icons_thumb_wrapper fl b_bot_0px hidden_menu_class" align="center" >
                            <div  class="header_icons_thumb" style="background-size:36px;background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_menu_icon.png');">													
                            </div>
                            <div class="header_icons_text" align="center"  ><?php echo $this->lang->line('menu'); ?></div>
                        </div>
                        <div class="header_icons_thumb_wrapper fl b_bot_0px" align="center" >
							<a class="" href="<?php echo base_url(MOBILE_M.'/buyPoints') ?>" class="white">
								<div class="header_icons_thumb" style="background-size:30px;background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_points_icon.png');">
									<div class="red_notification">
										<?php if (isset($res_header->point_balance))
											echo $res_header->point_balance;
										else
											echo "0";
										?>
									</div>
								</div>
								<div class="header_icons_text white" align="center"  ><?php echo $this->lang->line('points'); ?></div>
							</a>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        <!-- hidden navigations -->
        <div class="hidden_menu hidden_menu_class" id="hidden_menu" >
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M) ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('Home'); ?>
                </div>
            </a>
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/landing/faq') ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('faq'); ?>
                </div>
            </a>
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/landing/terms_and_conditions') ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('termNconditi0n'); ?>
                </div>
            </a>		
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/user/update') ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('settings'); ?>
                </div>
            </a>	
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/activity') ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('activity_log'); ?>
                </div>
            </a>            
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/user/logout'); ?>">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('log_out'); ?>
                </div>
            </a>
        </div>
        <!-- hidden navigations -->

        <!-- user quick links  -->
        <div class="quick_links_wrapper cl fl">
			 <a class="" href="<?php echo base_url(MOBILE_M.'/profile	') ?>" style="color:#585F6B">
				<div class="fl">				
					<div class="fl">				
						<div class="quick_links_icon fl margin_left" style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_profile_icon_2.png');"></div>
					</div>
					<div class="fl quick_links_username">								
						<?php echo stripslashes($user_info->user_name); ?>
					</div>
				</div>
			</a>
            <div class="quick_links_icons_main_wrapper fr">
                <div class="fl">				
                    <!--
                    <div class="quick_links_icons_wrapper fl">
                        <a class="" href="<?php echo base_url(MOBILE_M.'/message') ?>" style="color:#585F6B">
                            <div class="quick_links_icons" style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_notify_msg_icon.png');"></div>
                            <div class="quick_links_icons_text" ><?php echo $this->lang->line('messages'); ?></div>
                            <div class="red_notification_message" id="messageCounter">
                                <?php 
                                  /*$message_count = $this->users_m->count_unread_messages($user_id);              
                                    $message_show='';
                                    if($message_count>0)
                                    {
                                        $message_show='<span class="messageCounter">'.$message_count.'</span>';
                                    }
                                    echo $message_show;*/
                                  ?>
                            </div>
                        </a>
                    </div>
                    <div class="quick_links_icons_wrapper fl">
                        <div class="quick_links_icons" style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_notify_event_icon.png');"></div>
                        <div class="quick_links_icons_text" ><?php echo $this->lang->line('events'); ?></div>
                    </div>
                    <div class="quick_links_icons_wrapper fl">
                        <div class="quick_links_icons" style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_main_notify_req_icon.png');"></div>
                        <div class="quick_links_icons_text" ><?php echo $this->lang->line('requests'); ?></div>
                    </div>
                    -->
                </div>
            </div>
        </div>		
        <!-- user quick links  -->

        <!-- hidden navigations 2 -->
        <div class="cl hidden_menu hidden_menu_glomp_class" id="hidden_menu_glomp" style="z-index:101" >
            <a class="hidden_nav_link" href="<?php echo site_url(MOBILE_M.'/user/searchFriends?glomp=1') ?>" onclick="return checkIfWalkthrough('Friend');">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('Friends'); ?>
                </div>
            </a>
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M.'/user/facebookFriends') ?>" onclick="return checkIfWalkthrough('Facebook');">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('Facebook_Friend'); ?> 
                </div>
            </a>
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M.'/user/linkedinFriends') ?>" onclick="return checkIfWalkthrough('LinkedIn');" >
                <div class="hidden_nav">
                    <?php echo $this->lang->line(''); ?>LinkedIn Connections
                </div>
            </a>
            <div class="hidden_nav_seperator"></div>
            <a class="hidden_nav_link" href="<?php echo base_url(MOBILE_M . '/user/inviteFriends?glomp_only=1') ?>" onclick="return checkIfWalkthrough('Email');">
                <div class="hidden_nav">
                    <?php echo $this->lang->line('Via_Email'); ?>
                </div>
            </a>				
        </div>
        <!-- hidden navigations  2-->	

        <!--body -->
        <div class="cl body_wrapper" align="left">
            <!--do you have to-->            
            <div id="do_you_have_to" class="cl " style="background-color:#585F6B;color:#fff;font-size:12px;cursor:pointer">
                <div class="cl container p5px_0px global_margin">
                    <div class="row" align="center" style="font-size:16px;">
                        Do you have to:
                    </div>
                </div>
                <div style="cursor:pointer">
                    <div class="row cl">
                        <div class="fl " style="height:50px;width:33.33%;background-color:#00827A" align="center">
                            <div class="margin_left" style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px;">
                                Thank a friend?
                            </div>
                        </div>
                        <div class="fl" style="height:50px;width:33.33%;background-color:#0054A6" align="center">
                            <div style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px 5px;">
                                Cheer a buddy&nbsp;up?
                            </div>
                        </div>
                        <div class="fl " style="height:50px;width:33.33%;background-color:#6F8BA8" align="center">
                            <div class="margin_right" style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px;">
                                Congratulate someone?
                            </div>
                        </div>                   
                     </div>
                     
                     <div class="row cl">
                        <div class="fl" style="height:50px;width:33.33%;background-color:#ED1D24" align="center">
                            <div class="margin_left" style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px;">
                                Send a birthday&nbsp;wish?
                            </div>
                        </div>
                        <div class="fl" style="height:50px;width:33.33%;background-color:#B2D235" align="center">
                            <div style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px;">
                                Impress anyone?
                            </div>
                        </div>
                        <div class="fl " style="height:50px;width:33.33%;background-color:#636466" align="center">
                            <div class="margin_right" style="border:solid 0px;height:50px;display:table-cell;vertical-align:middle;padding:2px;">
                                Touch base with&nbsp;a&nbsp;client?
                            </div>
                        </div>                   
                     </div>
                </div>
            </div>            
            <!--do you have to-->                        
            
            <!--how to-->
            <div id="how_to_wrapper" class="" style="position:fixed;bottom:0px;left:0px;width:100%;border:0px solid; background-color:rgba(0, 0, 0, 0.75);font-size:14px;z-index:101;">
                <div class="cl" style="border:solid 1px;">
                    <img id="how_to_close" title="close" class="fr" src="<?php echo base_url('assets/frontend/img/Shape_X.png');?>" style="width:24px; margin:6px 4px 2px 4px; margin-right:2.2%;cursor:pointer" />
                </div>
                <div class="cl" style="border:solid 0px;">
                    <div id="action_treat_friend" class="fl" style="cursor:pointer;border:solid 1px #CBDB2A;background-color:#8DC641;color:#fff;padding:6px 2px; margin:5px 2px;margin-left:2.2%;width:47%;text-align:center;border-radius:8px">
                        How to <b>glomp!</b><br>(treat) a friend
                    </div>
                    <div id="action_redeem_voucher" class="fr" style="cursor:pointer;border:solid 1px #CBDB2A;background-color:#8DC641;color:#fff;padding:6px 2px; margin:5px 2px;margin-right:2.2%;width:47%;text-align:center;border-radius:8px">
                        How to redeem<br>a voucher
                    </div>
                </div>                
            </div>
            <!--how to-->            
            <!--blinker-->
                     
                <div title="Tap here" class="cl fl  round_cursor" id="pulse_1" style="position:absolute;top:0px;z-index:101;padding:2px;border:0px;display:none;overflow:visible">
                    <div class=" gps_ring fl round_cursor"  id="pulse_2" title="Tap here">
                        <div class="fl gps_ring round_cursor" style="padding:8px;"  id="pulse_3" title="Tap here">                            
                        </div>
                    </div>
                    <div class="cl" style="height:0px;width:0px;overflow:visible;color:#FFF;font-size:9px;">
                        <div style="padding-top:2px">
                            Tap&nbsp;here
                        </div>
                    </div>
                </div>
                <div class="cl fl" id="pulse_0" style="width:50px;height:50px;position:absolute;top:0px;z-index:101;padding:2px;border:0px solid #f00;display:none;overflow:visible">
                </div>
            <!--blinker-->
            
            <!--glomp buzz -->
			<div class="cl body_bottom_1px ">
                <div class="container p10px_0px margin_left">                    
					<div class="row">
						<div class="glomp_buzz_logo " style="background-image:url('<?php echo base_url() ?>assets/m/img/home/home_buzz.png');"></div>		  					
					</div>
                </div>
            </div>
            <!--glomp buzz -->			

            <!--glomp buzz details -->			



            <div class="buzzScroll nano">

                <?php $bg = ($glom_buzz_count == 0) ? "#d3d7e1" : "#fff"; ?>

                <div class="content" style=" background:<?php echo $bg; ?>" align="left" id="glomp_buzz_content" >

                    <?php
                    if ($glom_buzz_count > 0) {
                        $base_url = base_url();
                        $to_a = $this->lang->line('to_a');
                        foreach ($glomp_buzz->result() as $glomp) {
                            $from_id = $glomp->voucher_purchaser_user_id;
                            $to_id = $glomp->voucher_belongs_usser_id;
                            $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
                            /*print_r($frn_info);*/
                            $ago_date = $glomp->voucher_purchased_date;

                            $form_name = $frn_info->friends->$from_id->user_name;
                            $form_name_real = $frn_info->friends->$from_id->user_name;
                            
									
							if ($buzz_user_id == $from_id) {
								$form_name = $this->lang->line('You');
							}		
                            $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

                            $to_name = $frn_info->friends->$to_id->user_name;
                            $to_name_real = $frn_info->friends->$to_id->user_name;
                            
							 if ($buzz_user_id == $to_id) {
								$to_name = $this->lang->line('You');
							}
                            
                            $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
                            $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
                            
                            
                            $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
                            $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;
                            
                            
                            $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
                            /*product and merchant info*/
                            $prod_id = $glomp->voucher_product_id;
                            $prod_info = json_decode($this->product_m->productInfo($prod_id));
                            $prod_name = $prod_info->product->$prod_id->prod_name;
                            $prod_image= $prod_info->product->$prod_id->prod_image;        
                            $product_logo = $this->custom_func->product_logo($prod_image);
                            $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
                            $merchant_name = $prod_info->product->$prod_id->merchant_name;
                            
                            $story_type="1";
                            if($from_id!=$this->session->userdata('user_id'))
                            {
                                $story_type="2";
                            }
                            ?>
                            <div class="body_bottom_1px" data-id="<?php echo $start_count;?>">        
                                <div class="cl container p5px_0px global_margin">
									<div class="row">
										<div class="fl" style="width:73%;">
											<?php if ($from_id == 1) {
												?>
												<a class="red" href="javascript:void(0);"><?php echo $form_name; ?></a>
											<?php } else { ?>
												<a class="red" href="<?php echo $base_url . MOBILE_M.'/profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a> <?php } ?> <b>glomp</b>!ed <a class="red" href="<?php echo $base_url . MOBILE_M.'/profile/view/' . $to_id; ?>"><?php echo str_replace('ñ','&ntilde',$to_name); ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?>
											<div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
										</div>
										<div class="fr" style="width:20%;border:0px solid #333;" align="right">
											<div class="fr">												
                                                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs w50px" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line(''); ?>share</button>
                                                <div style="height:0px;">
                                                    <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_li_id="<?php echo $from_li_id;?>" data-to_li_id="<?php echo $to_li_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                        <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="dashboard_m" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>
                                </div>
								<!--
                                <div class="span6" style="text-align:right;">
                                    <?php if ($from_id == 1) {
                                        ?>
                                        <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
                                        <?php
                                    } else {
                                        ?>
                                      <a href="<?php echo base_url('profile/view/' . $from_id); ?>"><img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>

                                    <?php } ?>

                                    
									<img src="<?php echo base_url() ?>assets/m/img/glomp_arrow_red.jpg" />
                                    <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                                    <img src="<?php echo base_url() ?>assets/m/img/glomp_arrow_red.jpg" />									

                                    <a href="<?php echo base_url('profile/view/' . $to_id); ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
									
                                </div>
								-->
                            </div>
                            <?php
                            $start_count++;
                        }/*foeach close*/
                    }/*um count close*/
                    else {
                        if ($this->session->userdata('user_id') == $buzz_user_id) {
                            ?>

                            <div class="no_more_buzz p5px_0px global_margin">
                                <p><?php echo $this->lang->line('create_more_buzz_msg'); ?> </p> <br />

                                <a id ="add_friend_btn_opt" class="red" href="<?php echo base_url(MOBILE_M.'/user/inviteFriends'); ?>" class="btn-custom-primary-big"><?php echo $this->lang->line('Add_Friends'); ?></a>
                            </div>

                            <?php
                        }
                    }
                    ?>
                </div>
            </div>






            <!--glomp buzz details -->			
			<div class="p10px"></div>
        </div>
        <!--body -->	
        <div class="footer_wrapper"> <!-- /footer --></div> <!-- /footer -->
    </div> <!-- /global_wrapper -->
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>	
    <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>    
    <script src="<?php echo base_url() ?>assets/m/js/jquery.pulse.min.js"></script>		
    <script src="<?php echo base_url() ?>assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
    <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
    <script>
        var is_walkthrough=false;
        var hidden_menu_glomp_visible="2";   
        var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
        var GLOMP_START_TOUR = "<?php echo $user_start_tour;?>";
        var INVITE_THROUGH = "Add friend through?<?php //echo $this->lang->line('ADD_FRIEND_THROUGH');?>";
        var GLOMP_TOUR_START = "<?php echo $this->lang->line('GLOMP_TOUR_START');?>";
        var GLOMP_BASE_URL = "<?php echo base_url('');?>";
        var GLOMP_M_TOUR_DESC_1 = "<?php echo $this->lang->line('GLOMP_M_TOUR_DESC_1');?>";
        var GLOMP_M_TOUR_DESC_2 = "<?php echo $this->lang->line('GLOMP_M_TOUR_DESC_2');?>";
        var GLOMP_M_TOUR_DESC_3 = "<?php echo $this->lang->line('GLOMP_M_TOUR_DESC_3');?>";
        var inTour="<?php echo $inTour;?>";
        var is_connected_to_fb ="<?php echo $is_connected_to_fb;?>";
        var is_reactivated ="<?php echo $is_reactivated;?>";
        var GLOMP_PC_RESPONSE = "<?php echo $hasPromoResponse;?>";
        var GLOMP_PC_DATA = <?php echo json_encode($user_promo_code);?>;	
        var GLOMP_PC_DATA_PROD = <?php echo json_encode($res_info_prod);?>;		
        var GLOMP_PROMO_RULE_DESC = "<?php echo $promo_rule_desc;?>";
        var PROMO_CODE_DESC_INVALID = "<?php echo $this->lang->line('PROMO_CODE_DESC_INVALID');?>";
        var PROMO_CODE_DESC_INVALID_2 = "<?php echo $this->lang->line('PROMO_CODE_DESC_INVALID_2');?>";
        var PROMO_CODE_DESC_ENDED_ALREADY = "<?php echo $this->lang->line('PROMO_CODE_DESC_ENDED_ALREADY');?>";
        var PROMO_CODE_DESC_FOR_MEMBER_ONLY = "<?php echo $this->lang->line('PROMO_CODE_DESC_FOR_MEMBER_ONLY');?>";
        var PROMO_CODE_DESC_NO_VOUCHER_LEFT = "<?php echo $this->lang->line('PROMO_CODE_DESC_NO_VOUCHER_LEFT');?>";
        var PROMO_CODE_DESC_SUCCESS_PRIMARY = "<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_PRIMARY');?>";
        var PROMO_CODE_DESC_SUCCESS_ASSIGNABLE = "<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_ASSIGNABLE');?>";
        var PROMO_CODE_DESC_SUCCESS_FOLLOW_UP = "<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_FOLLOW_UP');?>";
        var NOT_CLAIM_MESSAGE = "<?php echo $not_claim_message; ?>";
        var loadingMoreStories = false;
        var walkthroughImages = new Array(5);
        for (var i = 0; i < 5; i++) {
            walkthroughImages[i] = new Array(5);
        }
        var current_frame=0;
        var walkthrough_type=0;
        /*friend*/        
        walkthroughImages[0][0]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_friend_F1.png';
        walkthroughImages[0][1]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_friend_F2.png';
        walkthroughImages[0][2]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_friend_F3.png';
        walkthroughImages[0][3]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_friend_F4.png';
        walkthroughImages[0][4]='';
        /*facebook*/
        walkthroughImages[1][0]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_fbook_F1.png';
        walkthroughImages[1][1]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_fbook_F2.png';
        walkthroughImages[1][2]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_fbook_F3.png';
        walkthroughImages[1][3]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_fbook_F4.png';
        walkthroughImages[1][4]='';
        /*email*/
        walkthroughImages[2][0]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_email_F1.png';
        walkthroughImages[2][1]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_email_F2.png';
        walkthroughImages[2][2]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_email_F3.png';
        walkthroughImages[2][3]='';
        /*voucher*/
        walkthroughImages[3][0]='http://glomp.it/assets/images/walkthrough/Glomp_WT_redeem_F1.png';
        walkthroughImages[3][1]='http://glomp.it/assets/images/walkthrough/Glomp_WT_redeem_F2.png';
        walkthroughImages[3][2]='http://glomp.it/assets/images/walkthrough/Glomp_WT_redeem_F3.png';
        walkthroughImages[3][3]='http://glomp.it/assets/images/walkthrough/Glomp_WT_redeem_F4.png';
        walkthroughImages[3][4]='';
        /*linkedIn*/
        walkthroughImages[4][0]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_b-linked_F1.png';
        walkthroughImages[4][1]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_b-linked_F2.png';
        walkthroughImages[4][2]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_b-linked_F3.png';
        walkthroughImages[4][3]='http://glomp.it/assets/images/walkthrough/Glomp_WT_glomp_b-linked_F4.png';
        walkthroughImages[4][4]='';
        
        

        $(function() {
            $("#main_menu").click(function() {
                $("#hidden_menu").toggle();
            });
            
            $('#add_friend_btn_opt').click(function(e){
                e.preventDefault();
                
                var NewDialog = $('<div id="messageDialog" align="center"> \
                        <p style="padding:4px 0px;" align="center">' + INVITE_THROUGH + '</p> \
                </div>');
                            
                NewDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title: INVITE_THROUGH,
                    modal: true,
                    width: 300,
                    height: 130,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: "Facebook",
                            "class": 'btn-custom-blue-grey_xs_nopadding w80px',
                            click: function() {
                                window.location = GLOMP_BASE_URL + 'm/user/facebookFriends';
                                $(this).dialog("close");
                            }},
                        {text: "LinkedIn",
                            "class": 'btn-custom-blue-grey_xs_nopadding w80px',
                            click: function() {
                                window.location = GLOMP_BASE_URL + 'm/user/linkedinFriends';
                                $(this).dialog("close");
                            }},
                        {text: "Email",
                            /*"class": 'btn-custom-blue-grey_xs w164px',*/
                            "class": 'btn-custom-blue-grey_xs_nopadding w80px',
                            click: function() {
                                window.location = GLOMP_BASE_URL + 'm/user/inviteFriends';
                                $(this).dialog("close");
                        }},
                        {text: "Cancel",
                            /*"class": 'btn-custom-blue-grey_xs w164px',*/
                            "class": 'btn-custom-transparent-green_xs',
                            click: function() {
                                $(this).dialog("close");
                        }}
                    ]
                });
                NewDialog.dialog('open');                
                
            });
            $("#do_you_have_to").click(function(){
                if(hidden_menu_glomp_visible=="2")
                {
                    hidden_menu_glomp_visible="0";                  
                }
                else{
                    if(hidden_menu_glomp_visible=="0")
                    {
                        hidden_menu_glomp_visible="1";
                    }
                    else
                    {
                        hidden_menu_glomp_visible="0";                    
                    }
                }                
                if(hidden_menu_glomp_visible=="1")
                {
                    $("#hidden_menu_glomp").hide();
                }
                else
                {
                    $("#hidden_menu_glomp").show();
                }
            });
            $("#glomp_menu").click(function() {
                /*$("#hidden_menu_glomp").toggle();*/
                if(hidden_menu_glomp_visible=="2")
                {
                    hidden_menu_glomp_visible="0";                  
                }
                else{
                    if(hidden_menu_glomp_visible=="0")
                    {
                        hidden_menu_glomp_visible="1";
                    }
                    else
                    {
                        hidden_menu_glomp_visible="0";                    
                    }
                }                
                if(hidden_menu_glomp_visible=="1")
                {
                    $("#hidden_menu_glomp").hide();
                }
                else
                {
                    $("#hidden_menu_glomp").show();
                }
            });	                                
            $("#pulse_0").click(function(e) {                
                e.stopPropagation();                                
                
                
                if(hidden_menu_glomp_visible=="2")
                {
                    hidden_menu_glomp_visible="0";                  
                }
                else{
                    if(hidden_menu_glomp_visible=="0")
                        hidden_menu_glomp_visible="1";
                    else
                        hidden_menu_glomp_visible="0";
                }
                /*console.log('pulse:0:'+hidden_menu_glomp_visible);*/
                if(hidden_menu_glomp_visible=="1")
                {
                    $("#hidden_menu_glomp").hide();
                }
                else
                {
                    $("#hidden_menu_glomp").show();
                }
            });
            
            $("#how_to_close").click(function() {
                $("#pulse_1").hide("normal" );
                $("#pulse_0").hide("normal" );
                $("#how_to_wrapper").hide( "drop", { direction: "down" }, "normal" );
                is_walkthrough=false;
                $('#overlay').remove();
            });	
            $("#action_treat_friend").click(function(){
                is_walkthrough=true;
                $('#overlay').remove();
                $elem=$('<div id="overlay" style="width:6000px;height:6000px;position:fixed;top:0px;left:0px;background-color:rgba(0,0,0,0.5);z-index:100";></div>');
                $('body').append($elem);
                $("#pulse_1").show("fast" );
                $("#pulse_0").show("fast" );
                hidden_menu_glomp_visible="2";
                $('html, body').animate({ scrollTop: 0 }, 0);
            });
            $("#action_redeem_voucher").click(function(){
                is_walkthrough=true;
                $('#overlay').remove();
                $elem=$('<div id="overlay" style="width:6000px;height:6000px;position:fixed;top:0px;left:0px;background-color:rgba(0,0,0,0.5);z-index:100";></div>');
                $('body').append($elem);                
                $('html, body').animate({ scrollTop: 0 }, 0);
                checkIfWalkthrough('Voucher');
            });
        });
          
        function checkIfWalkthrough(type)
        {
            if(is_walkthrough==false)
            {
                return true;
            }
            else
            {
               /*console.log('start walkthrough')*/
                $("#pulse_1").hide("fast");
                $("#pulse_0").hide("fast");
                hidden_menu_glomp_visible="0";
                $("#hidden_menu_glomp").hide("fast");
                $("#how_to_wrapper").hide( "drop", { direction: "down" }, "fast" );
                
                var rgba='rgba(203,18,139,0.7)';
                if(type=='Friend')
                {
                    rgba='rgba(203,18,139,0.7)';
                }
                else if(type=='Facebook')
                {
                    rgba='rgba(4,140,209,0.7)';
                }
                else if(type=='LinkedIn')
                {
                    rgba='rgba(133,85,163,0.7)';
                }
                else if(type=='Email')
                {
                    rgba='rgba(242,121,37,0.7)';
                }
                else if(type=='Voucher')
                {
                    rgba='rgba(185,35,39,0.7)';
                }
                $elem=$('<div id="walkthrough_wrapper" style="width:100%;height:100%;position:fixed;top:0px;left:0px;z-index:101";>\
                            <div class="global_margin" style="height:100%;border:solid 0px #fff; color:#fff;overflow:visible;z-index:102">\
                                <div class="" style="height:4%;">\
                                </div>\
                                <div id="walkthrough_frame_wrapper" align="center" class="" style="height:92%;border:solid 0px #f00;">\
                                    <img id="walkthrough_frame_loader" src="'+GLOMP_BASE_URL+'assets/images/ajax-loader-1.gif" class="" style="position:fixed;top:20%;left:50%;width:20px;margin-left:-10px;border:solid 0px #f00;z-index:102;">\
                                    <img id="walkthrough_frame" src="" class="" style="max-width: 100%; max-height: 100%;border:solid 0px #f00;z-index:103;">\
                                </div>\
                                <div class="" style="height:4%;">\
                                </div>\
                            </div>\
                        </div>');
                $('body').append($elem);
                $elem=$('<div id="walkthrough_wrapper_2" style="width:100%;height:10%;position:fixed;bottom:0px;left:0px;z-index:102";>\
                            <div class="cl global_margin" style="border:solid 0px #f00;height:100%;">\
                                <div class="fl" id="walkthrough_prev" style="position:fixed;bottom:8px;left:30px;cursor:pointer">\
                                    <div class="cl" style="color:#fff;width:45px;height:45px;border-radius:100%;background-color:'+rgba+'">\
                                        <div class="arrow_prev fl"></div>\
                                        <div class="fl" style="font-size:9px;margin-top:17px;">&nbsp;PREV</div>\
                                    </div>\
                                </div>\
                                <div class="fr" id="walkthrough_next" style="position:fixed;bottom:8px;right:30px;cursor:pointer">\
                                    <div class="cl" style="color:#fff;width:45px;height:45px;border-radius:100%;background-color:'+rgba+'">\
                                        <div id="walkthrough_next_icon" class="arrow_next fr"></div>\
                                        <div id="walkthrough_next_inner" class="fr" style="font-size:9px;margin-top:17px;">NEXT&nbsp;</div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>');
                        
                $('body').append($elem);
                
                if(type=='Friend')
                {
                    walkthrough_type=0;
                    current_frame=0;
                    loadWalkthrough((current_frame));
                    changeNextToClose();                    
                }
                else if(type=='Facebook')
                {
                    walkthrough_type=1;                                    
                    current_frame=0;
                    loadWalkthrough((current_frame));
                    changeNextToClose();
                }
                else if(type=='Email')
                {
                    walkthrough_type=2;
                    current_frame=0;
                    loadWalkthrough((current_frame));
                    changeNextToClose();
                }                
                else if(type=='Voucher')
                {
                    walkthrough_type=3;
                    current_frame=0;
                    loadWalkthrough((current_frame));
                    changeNextToClose();
                }
                else if(type=='LinkedIn')
                {
                    walkthrough_type=4;                                    
                    current_frame=0;
                    loadWalkthrough((current_frame));
                    changeNextToClose();
                }
                
                $('#walkthrough_next').click(function()
                {
                    loadWalkthrough((current_frame+1));                     
                    changeNextToClose();
                });
                $('#walkthrough_prev').click(function()
                {
                    loadWalkthrough((current_frame-1));
                    /*if(current_frame>0)*/
                    /*current_frame--;*/
                    
                    changeNextToClose();
                });                
                
                return false;
            }
        
        }
        function loadWalkthrough(current_frame_temp)
        {
            current_frame=current_frame_temp;
            if(walkthroughImages[walkthrough_type][current_frame]!='')
            {
                /*$('#walkthrough_frame').css('background-image','url('+walkthroughImages[walkthrough_type][current_frame]+')');*/
                $("#walkthrough_frame").hide( "slide", { direction: "left" }, "normal", function(){
                    $("#walkthrough_frame_loader").fadeIn("fast");
                    $('#walkthrough_frame').prop("src",walkthroughImages[walkthrough_type][current_frame]);
                    $('#walkthrough_frame').waitForImages(function() {
                        setTimeout(function()
                        {                            
                            $("#walkthrough_frame_loader").fadeOut("fast",function(){                                
                                $("#walkthrough_frame").show( "slide", { direction: "right" }, "normal");
                            });                            
                        }, 500);
                    });					                    
                });
                /*console.log('image loading');*/
                
                
            }
            else
            {
                /*close the frames and back to start */
                
                $('#walkthrough_wrapper').remove();
                $('#walkthrough_wrapper_2').remove();
                $('#overlay').remove();
                is_walkthrough=false;
                hidden_menu_glomp_visible="1";
                /*
                if(walkthrough_type<3)
                {
                    $("#pulse_1").show("fast");
                    hidden_menu_glomp_visible="1";
                    $("#hidden_menu_glomp").show("fast");
                }
                $("#how_to_wrapper").show( "drop", { direction: "down" }, "fast" );
                */
                current_frame=0;
                walkthrough_type=0;
                /*close the frames and back to start*/
            }
        }
        function changeNextToClose()
        {
            /*if (current_frame < walkthroughImages[walkthrough_type].length) {*/
            if(walkthroughImages[walkthrough_type][(current_frame+1)]=='')
            {
              /* do stuff*/
                $("#walkthrough_next_icon").addClass('arrow_close');
                $("#walkthrough_next_icon").removeClass('arrow_next');
                $("#walkthrough_next_inner").html('');
                
            }
            else
            {
                $("#walkthrough_next_icon").removeClass('arrow_close');
                $("#walkthrough_next_icon").addClass('arrow_next');
                $("#walkthrough_next_inner").html('NEXT&nbsp;');
            }
            if(current_frame==0)
            {
                $('#walkthrough_prev').hide('fast');
            }
            else
            {
                $('#walkthrough_prev').show('fast');
            }
        }
        function check_if_reactivated(is_reactivated)
        {
            if(is_reactivated=='true')
            {
                var NewDialog =$('  <div id="reactivated_popup">\
                                        <div align="left">\
                                            Welcome back to glomp!. We\'re very glad that you have decided to reactivate your account. You\'re ready now to glomp! away!\
                                        </div><br>\
                                        <div align="center">\
                                            <button type="button" class="btn-custom-darkblue_big " id="reactivated_closed">Close</button>\
                                        </div>\
                                    </div>');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:' noTitleStuff dialog_style_glomp_wait',
                        modal: true,
                        title:$('#Please_wait').text(),
                        position: 'center',
                        width:280,
                        open: function() {
                            $('#reactivated_closed').click(function(){
                                NewDialog.dialog('close');
                                NotClaimedVoucher(NOT_CLAIM_MESSAGE);
                            });
                        }
                    });

                    NewDialog.dialog('open');
                    return;
                
            }
            NotClaimedVoucher(NOT_CLAIM_MESSAGE);
        }
        function NotClaimedVoucher(message) {
            var showTourOrPromo = function (message) {
                if (message == '') {
                    if (GLOMP_PC_RESPONSE == 'Y')
                    {
                        promoCodePopup(GLOMP_PC_DATA);
                    }
                    else
                    {
                        welcompePopup(GLOMP_START_TOUR);
                    }
                    return;
                }

                var NewDialog = $('<div class="tourDialog" align="left" style="min-height: 10px !important;font-size:12px!important"> \
                            <div align="center">\
                                <div>' + message + '</div> \
                                <br /> \
                                <button type="button" class="btn-custom-darkblue_big w100px "  id="not_claimed_btn" >Ok</button> \
                            </div>');

                NewDialog.dialog({
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 280,
                    height: 180,
                });

                NewDialog.dialog('open');

                $('#not_claimed_btn').click(function() {
                    NewDialog.dialog('close');
                    if (GLOMP_PC_RESPONSE == 'Y')
                    {
                        promoCodePopup(GLOMP_PC_DATA);
                    }
                    else
                    {
                        welcompePopup(GLOMP_START_TOUR);
                    }
                });
            };            

            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: GLOMP_BASE_URL + 'user2/welcome_incomplete_profile',
                success: function(r) {
                    if(! r.bypass && r.incomplete) {
                        
                        var NewDialog = $('<div align="left" style="min-height: 10px !important;font-size:12px!important"> \
                        <div align="justify">\
                            <div>' + r.msg.body + '</div> \
                            <br /> \
                        </div> \
                        <div align="left">\
                            <button id= "incomplete_btn_ok" class="btn-custom-white_xs">'+ r.msg.ok +'</button>\
                            <button id= "close_me" class="btn-custom-blue_xs" style="padding-top: 5px !important;margin-left: 3px;padding-bottom: 1px !important;">'+ r.msg.later +'</button>\
                        </div></div>\\');

                        NewDialog.dialog({
                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                            autoOpen: false,
                            resizable: false,
                            modal: true,
                            width: 300,
                            open: function() {
                                $('#incomplete_btn_ok').click(function(){
                                    return window.location = GLOMP_BASE_URL + 'm/user/update';
                                });
                                
                                $('#close_me').click(function(){
                                    return NewDialog.dialog('close');
                                });
                            }
                        });

                        NewDialog.dialog('open');
                        return;
                    } else {
                        return showTourOrPromo(message);
                    }
                }
            });
        }        
		function adjustHeader(){
			var cw = $('.header_icons_thumb_wrapper').width();	
			/*if(cw>100)*/
			{
				$('.header_icons_thumb_wrapper').css({'height':cw+'px'});							

			}				
		}
		var promoCodePopupLoop=0;
		function promoCodePopup(data)
		{	
			var applyOkayButton=false;
			var NewDialog2=null;
			var _H=160;
			switch(data.status)
			{
				case 'Invalid':
					if(promoCodePopupLoop==0)
					{
						var NewDialog = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+PROMO_CODE_DESC_INVALID+'</div> \
						<div style="text-align:center;"><input type="text" name="promo_code" id="promo_code" style="margin:15px 0px 0px 0px;text-align:center" placeholder="Promo Code" /></div> \
						<div style="padding:20px 0px;width:100%;" class="fl" id="tour_button_wrapper" ></div> \
						<div class="cl"></div> \
						</div> \
						</div>');
					}
					else
					{
						var NewDialog = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+PROMO_CODE_DESC_INVALID_2+'</div> \
						<div style="text-align:center;"><input type="text" name="promo_code" id="promo_code" style="margin:15px 0px 0px 0px;text-align:center" placeholder="Promo Code" /></div> \
						<div style="padding:20px 0px;width:100%;" class="fl" id="tour_button_wrapper" ></div> \
						<div class="cl"></div> \
						</div> \
						</div>');
					}
					/*dialogClass:'dialog_style_tour',*/
					NewDialog.dialog({						
						closeOnEscape: false ,
						dialogClass: 'dialog_style_glomp_wait noTitleStuff',
						autoOpen: false,
						resizable: false,
						modal: true,
						width: 280,
						height: 180,
						show: 'fade',
						hide: '',
					});
					$elem = $('<button type="button" class="btn-custom-darkblue_big w100px fl"  id="" >SUBMIT</button>');
					$elem.on('click', function(e) {
						
						var NewDialog = $('<div id="waitPopup" align="center">\
												<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
							NewDialog.dialog({						
								autoOpen: false,
								closeOnEscape: false ,
								resizable: false,					
								dialogClass:' dialog_style_glomp_wait noTitleStuff',
								title:'Please wait...',
								modal: true,
								position: 'center',
								width:200,
								height:120
							});
							NewDialog.dialog('open');                            
							data = '&promo_code='+escape($('#promo_code').val());
                            $('#promoCodeDialog').dialog("close");
                            $('#promoCodeDialog').dialog('destroy').remove();
							$.ajax({
                            
								type: "POST",
								dataType: 'json',
								url: GLOMP_BASE_URL+'ajax_post/promo_code_save_to_user',
								data: data,
								success: function(response) {
									$('#waitPopup').dialog("close");
									$('#waitPopup').dialog('destroy').remove();
									/*if(response.status=='Error' || response.status=='Ok')*/
									{
										promoCodePopupLoop++;
										GLOMP_PC_DATA=response.user_promo_code;
										GLOMP_PC_DATA_PROD=response.res_info_prod;
                                        GLOMP_PROMO_RULE_DESC=response.rule_desc;
										promoCodePopup(GLOMP_PC_DATA);                                        
									
									}
								}
							});
							
						

					});			
					$("#tour_button_wrapper").append($elem);
					$skip = $('<button type="button" class="btn-custom-white_big w100px fr"  id="" >Cancel</button>');
					$skip.on('click', function(e){
						$('#promoCodeDialog').dialog("close");
						$('#promoCodeDialog').dialog('destroy').remove();
						var NewDialog = $('<div id="waitPopup" align="center">\
											<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
						NewDialog.dialog({						
							autoOpen: false,
							closeOnEscape: false ,
							resizable: false,					
							dialogClass:' dialog_style_glomp_wait noTitleStuff',
							title:'Please wait...',
							modal: true,
							position: 'center',
							width:200,
							height:120
						});
						NewDialog.dialog('open');
						data = '';
						$.ajax({
							type: "POST",
							dataType: 'json',
							url: GLOMP_BASE_URL+'ajax_post/clear_promo_from_user',
							data: data,
							success: function(response) {
								$('#waitPopup').dialog("close");
								$('#waitPopup').dialog('destroy').remove();
								welcompePopup(GLOMP_START_TOUR);
							}
						});
					});
					$("#tour_button_wrapper").append($skip);
					NewDialog.dialog('open');
				
				
				break;
				case 'DoneAlready':
					 NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+PROMO_CODE_DESC_ENDED_ALREADY+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');				
					applyOkayButton=true;
                    _H=140;
				break;
				case 'ForMemberOnly':                    
					var temp=PROMO_CODE_DESC_FOR_MEMBER_ONLY.replace("[rule_criteria]",GLOMP_PROMO_RULE_DESC); 								
					 NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+temp+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');
					applyOkayButton=true;
                    _H=150;
				break;
				case 'DidNoTPassedTheRule':
                    var temp=PROMO_CODE_DESC_FOR_MEMBER_ONLY.replace("[rule_criteria]",GLOMP_PROMO_RULE_DESC); 								
					NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+temp+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');
					applyOkayButton=true;
                    _H=150;
				break;
				case 'NoVoucherLeft':
					NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+PROMO_CODE_DESC_NO_VOUCHER_LEFT+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');
					applyOkayButton=true;                    
                    _H=140;
				break;
				case 'SuccessPrimary':		
					/**console.log(GLOMP_PC_DATA_PROD['product']);*/
                                        var Success_msg = PROMO_CODE_DESC_SUCCESS_PRIMARY;
                                        if (GLOMP_PC_DATA_PROD['product'].voucher_type == 'Assignable') {
                                            Success_msg = PROMO_CODE_DESC_SUCCESS_ASSIGNABLE;
                                            _H=200;
                                        }
					tempProd=GLOMP_PC_DATA_PROD['product'];
					temp=Success_msg.replace("[brand_name]", tempProd.merchant_name); 			
					temp=temp.replace("[product_name]", tempProd.prod_name); 
					NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+temp+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');
					applyOkayButton=true;
				break;
				case 'SuccessFollowUp':
					tempProd=GLOMP_PC_DATA_PROD['product'];
					temp=PROMO_CODE_DESC_SUCCESS_FOLLOW_UP.replace("[brand_name]", tempProd.merchant_name); 			
					temp=temp.replace("[product_name]", tempProd.prod_name); 
					NewDialog2 = $('<div id="promoCodeDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content">\
						<div style="text-align:left;">'+temp+'</div> \
						<div style="padding:20px 0px;width:100%;" id="tour_button_wrapper" align="center" ></div> \
						</div> \
						</div>');	
					_H=180;
					applyOkayButton=true;
				break;		
			}
			if(applyOkayButton)
			{
				NewDialog2.dialog({					
					closeOnEscape: false ,
					dialogClass: 'dialog_style_glomp_wait noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: _H,
					show: 'fade',
					hide: '',					
				});
				$okay = $('<button type="button" class="btn-custom-darkblue_big w100px "  id="" >Ok</button>');
				$okay.on('click', function(e){
					$('#promoCodeDialog').dialog("close");
					$('#promoCodeDialog').dialog('destroy').remove();
					var NewDialog = $('<div id="waitPopup" align="center">\
										<div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');
					NewDialog.dialog({						
						autoOpen: false,
						closeOnEscape: false ,
						resizable: false,					
						dialogClass:'dialog_style_glomp_wait noTitleStuff',
						title:'Please wait...',
						modal: true,
						position: 'center',
						width:200,
						height:120
					});
					NewDialog.dialog('open');
					data = '';
					$.ajax({
						type: "POST",
						dataType: 'json',
						url: GLOMP_BASE_URL+'ajax_post/clear_promo_from_user',
						data: data,
						success: function(response) {
							$('#waitPopup').dialog("close");
							$('#waitPopup').dialog('destroy').remove();
							welcompePopup(GLOMP_START_TOUR);
						}
					});
				});
				$("#tour_button_wrapper").append($okay);
				NewDialog2.dialog('open');
			
			}

		}
		function welcompePopup(open){
			if (open == 'Y' && (inTour=='2' || inTour=='3' || inTour=='4')) {
				startTour(inTour);
			}
			else if (open == 'Y' ) {
				var NewDialog = $('<div id="startTourDialog" class="tourDialog" align="center"> \
						<div id="tourDialog_1_content" class="tourDialog_1_content"><div align="center" style="font-size:18px;padding:10px 0px 20px 0px;">Welcome to <b>glomp!</b></div> \
						<div style="text-align:center;">Thank you for joining <b>glomp!</b> </div> \
						<div style="padding:20px 0px;" id="tour_button_wrapper" > \
						</div> \
						</div> \
						</div>');
				/*dialogClass:'dialog_style_tour',*/
				NewDialog.dialog({					
					closeOnEscape: false ,
					dialogClass: 'dialog_style_glomp_wait noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: 180,
					show: 'fade',
					hide: '',
				});
				$elem = $('<button type="button" class="btn-custom-darkblue_big btn-block"  id="" >' + GLOMP_TOUR_START + '</button>');
				$elem.on('click', function(e) {
					$('#startTourDialog').dialog("close");										
					$('#startTourDialog').dialog('destroy').remove();
					startTour(2);					
				});
				$("#tour_button_wrapper").append($elem);
				$skip = $('<div class="tour_skip" align="center" >Skip</div> ');
				$skip.on('click', function(e) {
					data = '&tour_stat=N';
					$.ajax({
						type: "POST",
						dataType: 'json',
						url: GLOMP_BASE_URL+'ajax_post/setTourStat',
						data: data,
						success: function(response) {
							$('#startTourDialog').dialog("close");
							setTimeout(function() {
								
							}, 500);
						}
					});
				});
				$("#tour_button_wrapper").append($skip);
				NewDialog.dialog('open');
			}
		
		}		
		function startTour(step){
			if (step == 1) {				
				var NewDialog = $('<div id="TourDialog1" class="tourDialog" align="center"> \
						<div id="" class="c_pointer" onClick="startTour(2)"><div align="center" style="font-size:18px;font-weight:bold;border-bottom:1px solid #B0B0B0;padding:12px 0px 12px 0px;">Add friends</div></div>\
						<div id="" class="c_pointer" onClick="startTour(3)"><div align="center" style="font-size:18px;font-weight:bold;border-bottom:1px solid #B0B0B0;padding:12px 0px 12px 0px;">Add favourites</div></div>\
						<div id="" class="c_pointer" onClick="startTour(4)"><div align="center" style="font-size:18px;font-weight:bold;padding:12px 0px 12px 0px;">Start glomp!ing</div></div>\
						</div>');
				/*dialogClass:'dialog_style_tour',*/
				NewDialog.dialog({		
					closeOnEscape: false ,				
					dialogClass: 'dialog_style_glomp_white noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: 155
				});				
				NewDialog.dialog('open');
			}
			else if (step == 2) {
				$('#TourDialog1').dialog('close');
				$('#TourDialog1').dialog('destroy').remove();
				var NewDialog = $('<div id="TourDialog2" class="tourDialog" align="center">\
				<div ><div align="center" style="font-size:18px;font-weight:bold:;padding:12px 0px 0px 0px;">Add friends</div> \
				<div ><div align="center" style="text-align:justify;font-size:12px;font-weight:normal;height:78px;padding:5px 15px !important;">'+GLOMP_M_TOUR_DESC_1+'</div> \
				<div style=";padding:5px 15px !important;" id="tour_button_wrapper2" ></div> \
				</div>');
				/*dialogClass:'dialog_style_tour',*/
				NewDialog.dialog({		
					closeOnEscape: false ,				
					dialogClass: 'dialog_style_glomp_white noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: 330
				});				
				$elem = $('<button type="button" class="c_pointer btn-custom-blue_fb_big fl "  id="" >Invite from Facebook</button>');
				$elem.on('click', function(e) {
					$('#TourDialog2').dialog("close");										
					$('#TourDialog2').dialog('destroy').remove();
					window.location.href = GLOMP_BASE_URL + 'm/user/facebookFriends?inTour=yes';
								
				});
				$("#tour_button_wrapper2").append($elem);
				$skip = $('<div class="btn-custom-darkblue_big fr c_pointer " align="center" >&nbsp;Skip&nbsp;</div> ');
				$skip.on('click', function(e) {
					startTour(3);
				});
				$("#tour_button_wrapper2").append($skip);
				NewDialog.dialog('open');				
			}
			else if (step == 3) {				
				$('#TourDialog1').dialog('close');
				$('#TourDialog1').dialog('destroy').remove();
				$('#TourDialog2').dialog('close');
				$('#TourDialog2').dialog('destroy').remove();
				var NewDialog = $('<div id="TourDialog3" class="tourDialog" align="center">\
				<div ><div align="center" style="font-size:18px;font-weight:bold:;padding:12px 0px 0px 0px;">Add favourites</div> \
				<div ><div align="center" style="text-align:justify;font-size:12px;font-weight:bold:;padding:5px 15px !important;">'+GLOMP_M_TOUR_DESC_2+'</div> \
				<div style=";padding:5px 15px !important;" id="tour_button_wrapper3" ></div> \
				</div>');
				/*dialogClass:'dialog_style_tour',*/
				NewDialog.dialog({		
					closeOnEscape: false ,				
					dialogClass: 'dialog_style_glomp_white noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: 365
				});				
				$elem = $('<button type="button" class="btn-custom-green_big fl c_pointer "  id="" >Add Favourites</button>');
				$elem.on('click', function(e) {
					$('#TourDialog3').dialog("close");										
					$('#TourDialog3').dialog('destroy').remove();
					window.location.href = GLOMP_BASE_URL + 'm/profile/menu?inTour=yes&tab=products&sub=snacks';
				});
				$("#tour_button_wrapper3").append($elem);
				$skip = $('<div class="btn-custom-darkblue_big fr c_pointer " align="center" >&nbsp;Skip&nbsp;</div> ');
				$skip.on('click', function(e) {
					startTour(4);
				});
				$("#tour_button_wrapper3").append($skip);
				NewDialog.dialog('open');				
			}
			else if (step == 4) {
				$('#TourDialog1').dialog('close');
				$('#TourDialog1').dialog('destroy').remove();
				$('#TourDialog2').dialog('close');
				$('#TourDialog2').dialog('destroy').remove();
				$('#TourDialog3').dialog('close');
				$('#TourDialog3').dialog('destroy').remove();
				var NewDialog = $('<div id="TourDialog4" class="tourDialog" align="center">\
				<div ><div align="center" style="font-size:18px;font-weight:bold:;padding:12px 0px 0px 0px;">Start glomp!ing</div> \
				<div ><div align="center" style="text-align:justify;font-size:12px;font-weight:bold:;padding:5px 15px !important;">'+GLOMP_M_TOUR_DESC_3+'</div> \
				<div style=";padding:5px 15px !important;" id="tour_button_wrapper4" align="center" ></div> \
				</div>');
				/*dialogClass:'dialog_style_tour',*/
				NewDialog.dialog({		
					closeOnEscape: false ,				
					dialogClass: 'dialog_style_glomp_white noTitleStuff',
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 280,
					height: 175
				});								
				$skip = $('<div class="btn-custom-green_big c_pointer" align="center" >&nbsp;End Tour&nbsp;</div> ');
				$skip.on('click', function(e) {
					var stat='N';
					data='&tour_stat='+stat;
					$.ajax({
						type: "POST",	
						dataType:'json',							
						url: '<?php echo base_url();?>ajax_post/setTourStat',
						data:data,
						success: function(response){											
							$('#TourDialog4').dialog("close");										
							$('#TourDialog4').dialog('destroy').remove();	
							if(is_connected_to_fb!=""){
								/*
                                window.location.href = GLOMP_BASE_URL + 'm/user/facebookFriends/';
                                */
							}
						}
					});		
				});
				$("#tour_button_wrapper4").append($skip);
				NewDialog.dialog('open');
				
			}
		}
		$(window).bind( 'orientationchange', function(e){
			
			if ($.event.special.orientationchange.orientation() == "portrait") {
					adjustHeader();
			} else {
			
			}			
		});
		$(document).ready(function(e)
        {
            var options = {
                "my": "left center-5",
                "at": "right-55 center",
                "of": "#glomp_menu"
            };
            $("#pulse_1").position(options);
            $("#pulse_0").position(options);
            
            
            
            var p = $("#glomp_menu");
            var offset = p.offset();
            
            /*el.offset({ top: offset.top, left: (offset.left+100)})*/
            
			adjustHeader();
            check_if_reactivated(is_reactivated);
            <?php
                $tweet_status = $this->session->flashdata('tweet_status');
                $tweet_message = $this->session->flashdata('tweet_message');        
                  if (! empty($tweet_status)) {
                      echo "doTweetSucces('".$tweet_status."','".$tweet_message."');";
                  }
            
            ?>
            
            
           
		});
        $(document).mouseup(function(e)
        {
            var container = $(".hidden_menu_class");
            if (!container.is(e.target) /* if the target of the click isn't the container...*/
                    && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
            {
                $('#hidden_menu').hide();
            }
            var container = $(".hidden_menu_glomp_class");
            if (!container.is(e.target) /* if the target of the click isn't the container...*/
                    && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
            {
                $('#hidden_menu_glomp').hide();
            }
        });
		$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
                var options = {
                    "my": "left center-5",
                    "at": "right-55 center",
                    "of": "#glomp_menu"
                };
                $("#pulse_1").position(options);
                $("#pulse_0").position(options);
			});
        function loadMore()
         {
            
            if(!loadingMoreStories)
            {            
                loadingMoreStories=true;            
                start_count=$('#glomp_buzz_content').children().last().data().id;                                   
                if(start_count>0)
                {
                    $loader=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">Loading more stories...<img width="25" src="' + GLOMP_BASE_URL + 'assets/images/ajax-loader-1.gif" /></div>');
                    $('#glomp_buzz_content').append($loader);
                    data ='start_count='+start_count;
                    setTimeout(function() {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: '<?php echo base_url();?>m/user/getMoreStories',
                            data: data,
                            success: function(response){ 
                                if(response!='' &&  response!='<!---->')
                                {
                                     $('#id_loader').remove();
                                    $('#glomp_buzz_content').append(response);
                                    $(window).bind('scroll', bindScroll);
                                    loadingMoreStories=false;
                                    
                                    $('.share_on_facebook, .share_on_linkedin, .share_on_twitter').unbind('click');
                                    $('.share_on_facebook').click(function(){                
                                        $this=$(this);
                                        checkLoginStatus($this,'share');
                                    });
                                    $('.share_on_linkedin').click(function(){                
                                        $this=$(this);        
                                        $data=$(this).data();
                                        doLinkedInShare($data);        
                                    });    
                                    $('.share_on_twitter').click(function(){                
                                        $data=$(this).data();
                                        doTwitterShare($data)
                                        
                                    });
                                    
                                    $('.glomp_fb_share').unbind('click');
                                    $('.glomp_fb_share').click(        
                                        function(e){
                                            $this=$(this);
                                            $data=$this.data();                          
                                            
                                            if($data.voucher_id in share_wrapper)
                                            {
                                             
                                                clearTimeout( share_wrapper[$data.voucher_id] );                
                                                delete share_wrapper[$data.voucher_id];
                                                
                                            }
                                            else
                                            {
                                                
                                            }
                                            
                                            $('.glomp_fb_share').each(function(){
                                                $this_temp=$(this);
                                                $data_temp=$this_temp.data();                 
                                                if($data_temp.voucher_id!=$data.voucher_id){                    
                                                    $('#share_wrapper_box_'+$data_temp.voucher_id).hide();
                                                    clearTimeout( share_wrapper[$data.voucher_id] );                
                                                    delete share_wrapper[$data.voucher_id];
                                                }
                                                else
                                                {
                                                    /*$('#share_wrapper_box_'+$data.voucher_id).fadeIn(); */
                                                }
                                            });
                                            $('#share_wrapper_box_'+$data.voucher_id).toggle();            
                                            e.stopPropagation();
                                        }                                    
                                    );
                                }
                                else
                                {                                
                                    $('#id_loader').remove();
                                    $no_more=$('<div id="id_loader" align="center" class="row-fluid" style="padding:15px 15px 5px 15px ;">No more stories to load.</div>');
                                    $('#glomp_buzz_content').append($no_more);
                                }
                            }
                        });
                    }, 1);
                }
                    
                /*console.log($('#glomp_buzz_content').children().last().data().id);*/
                /*xx=1;*/
            }
           
         }

         function bindScroll(){
           if($(window).scrollTop() + $(window).height() > $(document).height() -10) {
               $(window).unbind('scroll');
               loadMore();
               
           }
        }
         
        $(window).scroll(bindScroll);
    </script>
    <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>         
</body>
    <?php //include_once("includes/node.php") ?>
</html>