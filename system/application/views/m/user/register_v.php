<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo base_url() ?>assets/frontend/js/jquery.form.min.js"></script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
        </script>
    </head>    
    <body style="background: white;">
        <?php $this->load->view("includes/analyticstracking"); ?>
        <div class="global_wrapper" align="center" style="background-color: #D2D7E1">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>" class="white ">
                                        <div class="w100per fl white">
                                        Back
                                        </div>
                                    </a>
                                </li>                                    
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp_register.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations       
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M.'/landing/register_landing') ?>">
                        <div class="hidden_nav" align="left">
                            Back
                        </div>
                    </a>                    
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="p10px_0px global_margin" style="font-size: 14px;font-weight: bold; color:#808283; padding-top:13px;">
                

                <form id="register_form" method="post" action=<?php echo site_url('m/user/register'); ?> enctype='multipart/form-data'>
                    <input type="hidden" id="social_id" name="social_id" value = "<?php echo $social_id; ?>" />
                    <input type="hidden" id="registration_type" name="registration_type" value = "<?php echo $registration_type; ?>" />
                    <input type="hidden" id="profile_pic" name="profile_pic" />
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;min-height: 100px;">
                        <?php
                        if (validation_errors() != "") {
                            echo "<div class=\"alert alert-error\" id=\"errors-flat\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . validation_errors() . "</div>";
                        }
                        if (isset($error_msg)) {
                            echo "<div class=\"alert alert-error\" id=\"errors-flat\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error_msg . "</div>";
                        }
                        ?>
                        <div class="register_form_gray" align="left">
                            <input name="fname" id="fname" type="text" value="<?php echo $this->input->post('fname'); ?>" placeholder="First Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="register_form_gray" align="left">
                            <input name="lname" id="lname" type="text" value="<?php echo $this->input->post('lname'); ?>" placeholder="Last Name" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                        <div class="register_form_gray" align="left">
                            <select name="location" id="location" class="textBoxGray" style="width:100%">
                                <option value=""><?php echo $this->lang->line('Select_Location'); ?></option>
                                <?php echo $this->regions_m->location_dropdown(0, set_value('location')); ?>
                            </select>
                        </div>
                        <div class="border_topbot_gray" style="padding-right: 15px;" align="left">
                            <div class="fl" style="width: 100%">
                                <div class="fl" style="padding:10px 10px 10px 0px;width: 95px;">Gender</div>
                                <div class="fl" style="padding:10px 10px 10px 0px;">
                                    <div class="fl">
                                        <input type="radio" name="gender" id="gender-male" value="Male" <?php echo ($this->input->post('gender') != 'Female') ? 'Checked="checked"' : ""; ?> /> <?php echo $this->lang->line('Male'); ?>
                                    </div>
                                    <div class="fl" style="margin-left: 10px;">
                                        <input type="radio" name="gender" id="gender-female" <?php echo ($this->input->post('gender') == 'Female') ? 'Checked="checked"' : ""; ?> value="Female"> <?php echo $this->lang->line('Female'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                    </div>
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;">
                        <div class="fl" style="width: 100%;margin-left: 18px;margin-top: 10px;margin-right: 0px;padding-right: 32px;display:none" align="left">
                            <div class="fl"><input type="checkbox" name="chk_notif_via_fb" id="chk_notif_via_fb" value ="Y" /></div>
                            <div class="fl" style="margin-left: 11px;">Notify via Facebook Message</div>
                        </div>
                        <div class="cl"></div>
                        <div class="register_form_gray" align="left">
                            <input name="email" id="email" type="email" value="<?php echo $this->input->post('email'); ?>" placeholder="Email" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                        </div>
                    </div>
                    
                    <div class="global_border form-fieldset" style="min-width: 100px;">
                        <div class="fl">
                            <div class="register_form_gray" align="left">
                                <input name="pword" id="pword" type="password" maxlength="9" placeholder="Enter password" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                            <div class="register_form_gray" align="left">
                                <input name="cpword" id="cpword" type="password"  maxlength="9"  placeholder="Confirm password" prompt-max-reached="<?php echo $this->lang->line('max_pword_chars_reached','Sorry. Maximum of 9 characters.')?>" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; max-width: 100%">
                            <?php echo $this->lang->line('passwors_tips','You will need to enter this Password when sending and redeeming products so please remember it.'); ?><br />
                            <?php echo $this->lang->line('passwors_tips_length','(6-9 characters)'); ?>
                        </div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="global_border form-fieldset">
                        <div class="fl" align="left" style="padding:0px 10px 0px 10px; margin-top: 10px;">
                            <?php $temp = $this->lang->line('Promotional_Code');
                            echo str_replace(' ', '&nbsp;', $temp)
                            ?>
                        </div>
                        <div class="fl" align="left">
                            <div class="register_form_gray" align="left">
                                <input value="<?php echo $this->input->post('promo_code'); ?>" maxlength="40" name="promo_code" id="promo_code" type="text" placeholder="(Optional)" style="width: 100%; border: 0px solid; font-weight: bold; background-color: #E3E5EB;" />
                            </div>
                        </div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="">
                        <div class="fl" style="margin-left: 14px;"><input name="agree" id="agree" type="checkbox" /></div>
                        <div class="fl" style="margin-left: 10px;"><label for="agree">I agree to the <a href="<?php echo base_url('index.php/page/index/terms'); ?>" target="_blank"><span style="color:#3ACD00">Terms & Conditions</span></a></label></div>
                        <div class="cl"></div>
                    </div>
                    
                    <div class="">
                        <div class="fl" style="margin-left: 14px; margin-top: 10px">
                            <button type="submit" id="register_btn" class="btn-lg btn-custom-blue-grey btn-block w202px" >Submit</button>
                        </div>
                        <div class="fl" style="margin-left: 14px;margin-top: 10px">
                            <a href="<?php echo site_url(MOBILE_M . '/landing/register_landing'); ?>" class="btn-lg btn-custom-blue-grey btn-block w202px" type="submit">Cancel</a>
                        </div>
                        <div class="cl"></div>
                    </div>
                </form>
            </div>
            <!-- /container -->
        </div> <!-- /global_wrapper -->

        <div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId      : FB_APP_ID,
            xfbml      : true,
            version    : 'v2.0',
            status     : true,
            cookie     : true
        });
		FB.getLoginStatus(function(r){
			if(r.status == 'connected') {
                <?php if (isset($_GET['fb_user_id'])) { ?>
                jQuery(document).trigger('fbConnected');
                <?php } ?>
			}
		});
        
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>



<script type="text/javascript">
    jQuery(window).load(function(){
    <?php if (isset($_GET['linkedin'])) { ?>
        liAuth();
    <?php } ?>

    var social_response;


    function loginToFB(params) {
        var method = method || "post"; /* Set method to post by default if not specified.
        The rest of this code assumes you are not using a library.
        It can be made less wordy if you use one.*/
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", GLOMP_DESK_SITE_URL + 'm/landing');

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "fb_login");
        hiddenField.setAttribute("value", "fb_login");
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        return form.submit();
    }


    $(document).on("fbConnected", function(){
        propagateFacebook();    

        function propagateFacebook() {
            FB.api('/me', function(response){
                FB.api('/me/picture?width=140&height=140', function(r){
                    response.profile_pic = r.data.url
                    checkRecords(response);
                });
            });
        }
            
            
        function checkRecords(r) {
            jQuery.ajax({
                type: "POST",
                url: GLOMP_DESK_SITE_URL  + 'ajax_post/checkIfUserHasAnExistingAcct/?id=' + r.id,
                data: 'fbID=' + r.id,
                dataType: 'json'
            }).done(function(response){
                if(response.data == 1 && response.data_status == 'Active') {
                    return;
                } else {
                    //save data to global variable
                    social_response = r;

                    $('#registration_type').val('FB');
                    $('#lname').val(r.last_name);
                    $('#fname').val(r.first_name);
                    $('#email').val(r.email);
                    $('#social_id').val(r.id);
                    $('#profile_pic').val(r.profile_pic);
                    return;    
                }
            });
        }
    });

    /* Linked IN */
    function liAuth() {
        var params = {
            type: 'linked',
            type_string: 'LinkedIN',
            func: function(obj) {
                return login_linkedIN(obj);
            }
        };

        checkIntegratedAppConnected(params);
    }

    /* Usable functions */
    function checkIntegratedAppConnected(params) {
        var connectDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
            Connecting to '+ params.type_string +'...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
        </div></div>');


        connectDialog.dialog({
          dialogClass: 'dialog_style_glomp_wait noTitleStuff',      
          title:'',
          autoOpen: false,
          resizable: false,
          closeOnEscape: false ,
          modal: true,
          width:280,
          height:120,               
          show: '',
          hide: ''
        });

        connectDialog.dialog("open");
        params.func(connectDialog);
    }


    function login_linkedIN(obj){
      params = {
          js_func_li: 'liAuth()',
          api: '/v1/people/~:(id,email-address,first-name,last-name,date-of-birth,picture-urls::(original),location:(country:(code)))',
          dialog: obj,
          func: function(){}
      };
      /*IN.API.Profile("me").fields("id","email-address", "first-name", "last-name", "date-of-birth").result(function(data){*/
      params.func = function(data) {
              $.ajax({
              type: "POST",
              dataType: 'json',
              url: GLOMP_DESK_SITE_URL + 'ajax_post/processIntegratedApp',
              data: {
                  id: data.values[0].id,
                  email: data.values[0].emailAddress,
                  field: 'user_linkedin_id',
                  mode: 'linkedIN',
                  device: 'mobile',
                  data: data.values[0]
              },
              success: function(res) {
                  if (res != false) {
                      if (res.status == 'register') {
                            $('#registration_type').val('LI');
                            $('#lname').val(data.values[0].lastName);
                            $('#fname').val(data.values[0].firstName);
                            $('#email').val(data.values[0].emailAddress);
                            $('#social_id').val(data.values[0].id);
                            $('#profile_pic').val(data.values[0].pictureUrls.values[0]);
                            obj.dialog('close');
                            return;
                            /*end dialog */
                        } else {
                                if (res.post_to_url) {
                                    var form = document.createElement("form");
                                    form.setAttribute("method", 'post');
                                    form.setAttribute("action", res.redirect_url);      

                                      for (var key in res.data) {
                                          if (res.data.hasOwnProperty(key)) {
                                              var hiddenField = document.createElement("input");
                                              hiddenField.setAttribute("type", "hidden");
                                              hiddenField.setAttribute("name", key);
                                              hiddenField.setAttribute("value", res.data[key]);
                                              form.appendChild(hiddenField);
                                          }
                                      }

                                        document.body.appendChild(form);
                                        obj.dialog('close');
                                        return form.submit();
                                }
                                obj.dialog('close');

                                return window.location = res.redirect_url;
                        }
                    }
              }
          });
        };
          
        linkedin_api(params);
    }


    function linkedin_api(obj) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            async: false,
            url: GLOMP_DESK_SITE_URL + 'ajax_post/linkedIn_callback',
            data: {
                redirect_li: GLOMP_BASE_URL + 'm/landing',
                js_func_li: obj.js_func_li,
                api: obj.api
            },
            success: function(_data) {
                if (_data.values[0] == 'error') {
                    var log_details = {
                        type: 'linkedin_api',
                        event: 'LinkedIn login button click',
                        log_message: 'An error occur, linkedIn server is down. Retry Later.',
                        url: window.location.href,
                        response: JSON.stringify(_data)
                    };

                    var logs = [];
                    logs.push(JSON.stringify(log_details));
                    /*save_system_log(logs);*/

                     var NewDialog = $('<div id="messageDialog" align="center"> \
                        <p style="padding:15px 0px;">An error occur, linkedIn server is down. Retry Later.</p> \
                    </div>');

                    NewDialog.dialog({
                        autoOpen: false,
                        resizable: false,
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                        title: '',
                        modal: true,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: "Ok",
                                "class": 'btn-custom-blue-grey_xs w80px',
                                click: function() {
                                    $(this).dialog("close");
                                    $(this).dialog("destroy");
                                    obj.dialog.dialog('close'); /*close loading popup*/
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                    return false;
                }
                if (_data.redirect_window != '')  {
                    return window.location = _data.redirect_window;
                } else {
                  obj.func(_data);
                }
            }
        });
    }

    
    var Loader = $('<div id="waitPopup" align="center"><div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
    Loader.dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
        title: 'Please wait...',
        modal: true,
        position: 'center',
        width: 200,
        height: 120
    }).extend({
        open: function(){
            jQuery(this).dialog('open');
        },
        close: function(){
            jQuery(this).dialog('close');
        }
    });
    
    var Prompt = jQuery('<div id ="errors" align="left" ><div class="alert alert-error" id ="errors_inner" style="font-size:11px !important; margin: 0px !important;" ></div></div>');
    Prompt.dialog({
        autoOpen: false,
        closeOnEscape: false ,
        resizable: false,					
        dialogClass:'dialog_style_glomp_wait noTitleStuff ',
        modal: true,
        title:'Please wait...',
        position: 'center',
        width:280,                                
        buttons:[
            {text: "Close",
            "class": 'btn-custom-white_xs',
            click: function() {
                $(this).dialog("close");
                setTimeout(function() {
                    
                }, 500);
            }}
        ]
    });
    
    jQuery('.ui-dialog').css('position','fixed');

    
    /* FORM VALIDATION */
    $('#register_form').on('submit',function(e) {
        e.preventDefault();
        Loader.open();
            
        $(this).ajaxSubmit({
            type: "post",
            url: $(this).attr('action'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function(r) {
                Loader.close();
                
                if (r.error != '') {
                    $('.error_border').removeClass('error_border');
                    $.each(r.error_list, function(key,val) {
                        $('#'+key).addClass('error_border');
                    });
                    
                    Prompt.find('div').html(r.error);
                    Prompt.dialog('open');
                    return;
                } else if (r.status == 'login') {
                    if ($('#registration_type').val() == 'FB' ) {
                        return loginToFB(social_response);
                    } 

                    if ($('#registration_type').val() == 'LI' ) {
                        var params = {
                            type: 'linked',
                            type_string: 'LinkedIN',
                            func: function(obj) {
                                return login_linkedIN(obj);
                            }
                        };

                        return checkIntegratedAppConnected(params);
                    }
                }
                 else {
                    return window.location = r.location;
                }   
            }
        });
    });
        
    var prompt = jQuery('<div />');
    prompt.dialog({
        autoOpen:false,
        resizable:false,
        dialogClass:'dialog_style_glomp_wait noTitleStuff',
        modal:true,
        width:300,
        buttons:[
            {
                text: 'Close',
                class: 'btn-custom-white_xs',
                click: function(){
                    prompt.html('');
                    prompt.dialog('close');
                }
            }
        ]
    });
    jQuery('#pword,#cpword').on('keydown',function(e){
        var k = e.keyCode
        if(k==9||k==8||k==20||k==18||k==17||k==16||k==13||k==27||k==91) return;
        if(e.target.value.length == 9) {
            prompt.html( '<div class="_error alert alert-error">'+jQuery(e.target).attr('prompt-max-reached')+'</div>' );
            prompt.dialog('open');
        }
    });
    
    });
</script>

</body>
</html>
