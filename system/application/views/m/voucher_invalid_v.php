<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background:#DEE6EB;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>
                    </a>
                </div>
            </div>

            
			<!--body-->  
			<div class="cl p20px">	</div>
            <div class="container p5px_0px global_margin  white" style="text-align:justify ">
				<div class="p10px_20px" align="center" style="background-color:#ff0000;border-radius:5px">
				<?php echo $response->message;?>
				</div>
            </div>
			<div class="cl p8px_0px_0px_0px" align="center">
				<a href="<?php echo base_url(MOBILE_M.'/redeem/voucher/'.$voucher_id)?>" class="btn-custom-blue-grey_normal white" type="button"><?php echo $this->lang->line('Close');?></a>
			</div>	
			<div class="cl p20px">	</div>
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script>
            
        </script>
    </body>
    <?php //include_once("includes/node.php") ?>
</html>
