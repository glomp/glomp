<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?></title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <!-- Bootstrap -->    
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">    
  </head>
  <body>
	<?php //include_once("includes/analyticstracking.php") ?>
	<div id="fb-root"></div>	
	<div class="global_wrapper" align="center">	
	  
	</div> <!-- /global_wrapper -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>	
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
	<script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
	<div id="fb-root"></div>    
     <script type="text/javascript">   
		var FB_APP_ID="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
		var LOCATION_REGISTER="<?php echo base_url('index.php/landing/register');?>";
		var SIGNUP_POPUP_TEXT="<?php echo $this->lang->line('signup_popup_text');?>";
		var SIGNUP_WITH_FB_BUT_REG="<?php echo $this->lang->line('signup_with_fb_but_already_registered');?>";
		var GLOMP_BASE_URL									="<?php echo base_url('');?>";
		var firstRun=true;
		var fbApiInit = false;
		window.fbAsyncInit = function() {
		  FB.init({
			appId   : FB_APP_ID,/*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
			status     : true, /* check login status*/
			cookie     : true, /* enable cookies to allow the server to access the session*/
			xfbml      : true  /* parse XFBML	  		*/
		});		
            
		 
			/*FB.Event.subscribe('auth.authResponseChange', function(response) {			
            
				if (response.status === 'connected') {				  
                    
					getUserDetailsAndDoPost();
				
				} else if (response.status === 'not_authorized') {				  
					FB.login(function(response) {
						if (response.status === 'connected') {																							
						}
					}, {scope: 'publish_actions,email'});			  					
				} else {					
				  FB.login(
						function( response )
						{
							if (response.status === 'connected') {												
								getUserDetailsAndDoPost();
								
							}
						}, {scope: 'publish_actions,email'}			  
					);			
				}
				 /*console.log(response+"H");///
			}); */
			 FB.getLoginStatus(function (response) {
				if(response.status=="unknown"){
					firstRun=false;
                    
                    if(!firstRun)
                    {
                        FB.login(
						function( response )
						{
							if (response.status === 'connected') {												
								getUserDetailsAndDoPost();
								
							}
						}, {scope: 'publish_actions,email'}			  
					);
                    }
				}
                else if(response.status=="connected")
                {
					getUserDetailsAndDoPost();        
                }
			   });
			
		};
		/* Load the SDK Asynchronously*/
		  (function(d){
			 var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
			 js = d.createElement('script'); js.id = id; js.async = true;
			 js.src = "//connect.facebook.net/en_US/all.js";
			 d.getElementsByTagName('head')[0].appendChild(js);
		   }(document));
		
		
        var facebookDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
            Connecting to Facebook...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
        </div></div>');
                $(document).ready(function() {
					facebookDialog.dialog({
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',		
                        title:'',
                        autoOpen: false,
                        resizable: false,
                        closeOnEscape: false ,
                        modal: true,
                        width:280,
                        height:120,				
                        show: '',
                        hide: ''                
                    });
                    facebookDialog.dialog("open");
				});
		
		function getUserDetailsAndDoPost(){    
            doFacebookDelay('facebookLoadingDialog', 'friendsList_loading',5000, 'popup' ,'facebookConnectDialog');
			FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {      
				var output = '';
				for (property in response) {
				  output += property + ': ' + response[property]+'; ';
				}                                                        
				for (property in response.hometown) {
				  output += property.hometown + ': ' + response.hometown[property]+'; ';
				}                                                        
				console.log(output);		
                facebookDialog.dialog("close");
                clearFacebookDelay();
				post_to_url((GLOMP_BASE_URL+'m/landing/?from_android=true'),response,'post');
				/*console.log('Good to see you, ' + response.id + '::'+response.email );*/
                
                /*console.log(property);*/
			});
		}
        var timerFacebookDelay=null;
        function doFacebookDelay(targetPopup, targetElement, timeout, outputType, closeThisFirst)
        {
            
            var message='We are experiencing a loading delay from Facebook.<br>Please wait a moment.';
            message='<span style="font-size:14px">'+message+'</span>&nbsp;<img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />';
            timerFacebookDelay = setTimeout( function ()
            {
                if(closeThisFirst!='')
                {
                    $("#"+closeThisFirst).dialog('close');
                }
                if(outputType=='popup')
                {            
                    
                    if( $('#'+targetPopup).doesExist() )
                    {/*target popupExist*/
                        $('#'+targetElement).html(message);                    
                    }
                    else
                    {/*create a new popup                    */
                        
                        var NewDialog = $(' <div id="'+targetPopup+'" align="center">\
                                                <div align="center" style="margin-top:15px;" id="'+targetElement+'">'+message+'</div>\
                                            </div>');
                        NewDialog.dialog({						
                            autoOpen: false,
                            closeOnEscape: false ,
                            resizable: false,					
                            dialogClass:'dialog_style_glomp_wait noTitleStuff',
                            title:'',
                            modal: true,
                            position: 'center center',
                            width:280,
                            height:130
                        });
                        NewDialog.dialog('open');
                    }
                }
                else
                {
                    
                    $('#'+targetElement).html(message);
                }
                
                
            }, timeout);
        }
        
        function clearFacebookDelay()
        {
            clearTimeout(timerFacebookDelay);
            if( $('#facebookLoadingDialog').doesExist() )
            {
                $("#facebookLoadingDialog").dialog('close');
                $("#facebookLoadingDialog").dialog('destroy').remove();
            }        
        }
		function post_to_url(path, params, method)
        {
			method = method || "post"; /* Set method to post by default if not specified.
			 The rest of this code assumes you are not using a library.
			 It can be made less wordy if you use one.*/
			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);		

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);
					form.appendChild(hiddenField);				
				}
			}			
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "fb_login");
			hiddenField.setAttribute("value", "fb_login");
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
		}
		function checkIfUserHasAnExistingAcct()
        {
			FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
				var fbID=response.id;
				$.ajax({
					type: "POST",
					url: GLOMP_BASE_URL+'ajax_post/checkIfUserHasAnExistingAcct/',
					data:'fbID='+fbID				
				}).done(function(response) {
					if(response!=""){		
						response=jQuery.parseJSON(response);				
						if(response.data=='1' && response.data_status=="Active"){
                            facebookDialog.dialog("close");
                            clearFacebookDelay();
							$( "#dialog" ).dialog({						/*dialogClass: 'noTitleStuff'*/
								dialogClass: 'noTitleStuff dialog_style_glomp_wait',
								resizable: false,
								modal: true,
								autoOpen: false,
								width: 280,
								height: 200,
								buttons: [
									{
										text: "Login with Facebook",
										"class": 'btn-custom-blue_fb w120px ',
										click: function() {                                            
											$( this ).dialog( "close" );
                                            facebookDialog.dialog("open");
											FB.login
											(
												function( response )
												{
													if (response.status === 'connected') {												
														getUserDetailsAndDoPost();									
													}
                                                    else
                                                    {
                                                        facebookDialog.dialog("close");
                                                    }
												}, {scope: 'publish_actions,email'}
											);									
										}
									},
									{
										text: "Cancel",									
										"class": ' btn-custom-ash_xs w120px ',
										click: function() {										
											$( "#dialog" ).dialog( "close" );	                                            
										}
									}
								]
							});
							$( "#dialog-content" ).html(SIGNUP_WITH_FB_BUT_REG);
							$( "#dialog" ).dialog( "open" );	
							
						}
						else{		
                            facebookDialog.dialog("open");
							getUserDetailsAndDoPost();
						}
					}
				});
			});
		}
		$(window).resize(function(){
			$(".ui-dialog-content").dialog("option", "position", "center");
        });
        $.fn.doesExist = function(){
            return jQuery(this).length > 0;
        };
     </script>	 	 
  </body>
</html>
