<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">        
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="">            
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>		
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script>
            $(function() {
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
                });
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
    </head>
    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
           <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
           <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>	
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-points-logo.png');"></div>
                    </a>
                </div>
                <!-- hidden navigations       
                <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>                
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="cl p20px_0px" style="margin-top:12px;"></div>	
            
			
			<!--body-->   
            <div style=" background:#DEE6EB">
                <div class="container p5px_0px global_margin " style="text-align:justify;">
                    <div class="container p10px_0px ">                
                         <div style="float:left;background-size: contain;width:68px;height:18px;background-position:left;background-repeat:no-repeat;	background-image:url('<?php echo base_url() ?>assets/m/img/glomped_list_header1.png');"></div>
                        <div class="red harabarabold" style="float:left;margin-top:-6px;font-size:20px;">points</div>
                    </div>
                </div>
                <div class="container global_margin " style="">
                     <div class="glompoints">
                      <div class="level"><?php echo $this->lang->line('Remaning');?></div>
                      <div class="points"><?php echo $this->user_account_m->user_balance($this->session->userdata('user_id'));?></div>
                      </div>
                </div>                   
            </div>	
            <div class="container p5px_0px global_margin " style="text-align:justify;">
                <div style="font-size:18px;padding:5px 0px 8px 0px;color:#465069">
                    <strong>Buy Points</strong>
                </div>	
            </div>	
            <div class="" style="border-top:1px solid #8e959d;border-left:1px solid #8e959d;" >
                <div class="clearfix"></div>
                <?php 
                    foreach($resPoints->result() as $recPoints)
                    {
                ?>      <a href="<?php echo base_url(MOBILE_M).'/buyPoints/buy/'.$recPoints->package_id;?>" >
                            <div class="glompSelPoints" data-id='<?php echo $recPoints->package_id;?>' data-value='<?php echo $recPoints->package_point;?>' data-cost='<?php echo $recPoints->package_price;?>'>
                                <?php echo $recPoints->package_point;?>
                                <p class="glompPackagePrice">USD <?php echo $recPoints->package_price; ?></p>
                            </div>
                        </a>
               <?php
                    }?>
                <div class="clearfix"></div>
            </div>
            
			<!--body-->
            <div class="cl p20px"></div>    
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->
        
    </body>
</html>
