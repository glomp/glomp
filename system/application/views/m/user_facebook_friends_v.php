<?php
$what_to_do = '';
$fb_func = '';
$inTour = '';
$inTour_display = '';
$inTour_display_footer = 'display:none;';
$inTour_header = 'Invite';
$inTour_header_top_position = '50';
$inTour_header_top_margin = '150';
$inTour_header_top_position_2 = '98';
if (isset($_GET['inTour'])) {
    $inTour = $_GET['inTour'];
    if ($inTour == 'yes') {
        $inTour_display = 'display:none;';
        $inTour_display_footer = '';
        $inTour_header = 'Add some friends';
        $inTour_header_top_position = '0';
        $inTour_header_top_margin = '100';
        $inTour_header_top_position_2 = '48';
    }
}
$RELOAD="";
$RELOAD_DATA="";
if(isset($_GET['reload'])) {
    $RELOAD = $_GET['reload'];
    $RELOAD_DATA = ($_GET['data']);
}
if(isset($_GET['what_to_do'])) {
    $what_to_do = $_GET['what_to_do'];
}

if(isset($_GET['js_fb_func'])) {
    list($fb_func, $what_to_do) = explode('|', urldecode($_GET['js_fb_func']));
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">            
    <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    

    <body style="background: white;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;<?php echo $inTour_display; ?>">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>	
                    <a href="<?php echo site_url(MOBILE_M . '/user/searchFriends'); ?>" >
                        <div class="glomp_header_logo_2" style="height: 25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-friends-logo.png');"></div>
                    </a>
                </div>
            </div>

            <!-- hidden navigations       
            <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu"  style="position:fixed;width:100%;top:50px;z-index:6" >		
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>                
            </div>
            <!-- hidden navigations -->			 
            <div class="body_bottom_1px user_fb_invite" style="position:relative;width:100%;top:<?php echo $inTour_header_top_position;?>px;background:#fff;z-index:1">
                <div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B">
                    <div class=" ">						
                        <div class="fl row red harabarabold" style="font-size: 20px;"><?php echo $inTour_header; ?></div>
                        <div class="fr" style="border:0px solid; display:none " id="fb_invite_button_wrapper">
                            <div style="border:0px solid #000000;width:100px; float:right;"  >
                                <button type="button" id="invite_fb_friends" class="fr btn-custom-blue-grey_xs w60px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('invite'); ?></button>
                                <label style="margin-top:5px; float:right;color:#585F6B" class="fr">&nbsp;&nbsp;</label><input type="checkbox" class="checkbox fr" id="chk_select_all" >
                            </div>							
                        </div>
                        <div class="fr" style="border:0px solid; display:none " id="li_invite_button_wrapper">
                            <div style="border:0px solid #000000;width:100px; float:right;"  >
                                <button type="button" id="invite_li_friends" class="fr btn-custom-blue-grey_xs w60px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('invite'); ?></button>
                                <label style="margin-top:5px; float:right;color:#585F6B" class="fr">&nbsp;&nbsp;</label><input type="checkbox" class="checkbox fr" id="li_chk_select_all" >
                            </div>							
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="cl body_bottom_1px" style="border-top:1px solid #B0B0B0;position:fixed;width:100%;top:<?php echo $inTour_header_top_position_2; ?>px;background:#fff;z-index:1; display:none " id="fb_invite_button_wrapper_2">
                <div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B">
                    <div class=" ">
                        <div class="fl" style="width: 165px;">
                            <div class="global_border"  style=" padding: 4px;width: 100%;">
                                <div class="search_icon fl" style="background-image:url('<?php echo base_url('assets/m/img/search_icon.png') ?>');" ></div>
                                <div class="fl"><input name="keywords" id="searchTextField" style="color:#585F6B;border: 0px solid;width:132px;font-size:12px" type="text" placeholder="Search Friend" value=""></div>
                                <div class="cl"></div>
                            </div>
                        </div>
                        <div class="fr" style="border:0px solid;display:none" id="fb_paginate">
                            <div class="fr" style="border:0px solid; ">
                                <button type="button" id="page_next" disabled  class="fr btn-custom-ash_xs w50px" style="margin-right:0px !important;" name="" >&gt;</button>							
                                <button type="button" id="page_prev" disabled class="fr btn-custom-ash_xs w50px" style="margin-right:0px !important;" name="" >&lt;</button>							
                            </div>
                        </div>
                        <div class="fr" style="border:0px solid;display:none" id="li_paginate">
                            <div class="fr" style="border:0px solid; ">
                                <button type="button" id="li_page_next" disabled  class="fr btn-custom-ash_xs w50px" style="margin-right:0px !important;" name="" >&gt;</button>							
                                <button type="button" id="li_page_prev" disabled class="fr btn-custom-ash_xs w50px" style="margin-right:0px !important;" name="" >&lt;</button>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div id="friendsList" style="margin-top:<?php echo $inTour_header_top_margin; ?>px">			
                <!-- /container -->           
            </div>
            <div class="" style="position:fixed;width:100%;bottom:0px;background:#fff;z-index:5; border-top:#B0B0B0 solid  1px;;<?php echo $inTour_display_footer; ?> ">
                <div class="cl container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B" align="center">
                    <button type="button" id="tour_add" class="fl btn-custom-green_normal w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Add</button>
                    <button type="button" id="tour_add_all" class=" btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line(''); ?>Add All</button>
                    <button type="button" id="tour_cancel" class="fr btn-custom-blue-grey_xs w80px" style="margin-right:0px !important;" name="" ><?php echo $this->lang->line('Cancel'); ?></button>
                </div>
            </div>			
            <div class="footer_wrapper"></div>
            <div id="fb-root"></div>		
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>

        <script>
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_INVITE_REQUEST_SENT_BODY = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_BODY'); ?>";
            var GLOMP_INVITE_REQUEST_SENT_TITLE = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_TITLE'); ?>";
            var GLOMP_MESSAGE_JOIN = "<?php echo $this->lang->line('GLOMP_MESSAGE_JOIN'); ?>";
            var GLOMP_FB_FRIENDS_LIST_TITLE = "<?php echo $this->lang->line('GLOMP_FB_FRIENDS_LIST_TITLE'); ?>";
            var GLOMP_GETTING_FB_FRIENDS = "<?php echo $this->lang->line('GLOMP_GETTING_FB_FRIENDS'); ?>";
            var GLOMP_GETTING_LI_FRIENDS = "<?php echo $this->lang->line('GLOMP_GETTING_LI_FRIENDS', 'Sorry. Linkedin recently made changes to 3rd party access and therefore glomp! is temporarily unable to access your Linkedin connections. Please connect to your Linkedin connections via Facebook or email in the meantime. We will have this issue resolved soon.'); ?>";
            var GLOMP_PUBLIC_IMAGE_LOC = "<?php echo base_url("assets/images/"); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_FB_POPUP_TITLE_ADD = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_ADD'); ?>";
            var GLOMP_FB_POPUP_TITLE_INVITE = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_INVITE'); ?>";
            var GLOMP_ADD_FRIEND_SUCCESS = "<?php echo $this->lang->line('GLOMP_ADD_FRIEND_SUCCESS'); ?>";
            var GLOMP_FB_INVITE_PRE_MESSAGE = "<?php echo $this->lang->line('GLOMP_FB_INVITE_PRE_MESSAGE'); ?>";
            var GLOMP_ASK_OPTION = "<?php echo $this->lang->line('GLOMP_ASK_OPTION'); ?>";
            var GLOMP_FB_INVITE_REMINDER = "<?php echo $this->lang->line('GLOMP_FB_INVITE_REMINDER'); ?>";
            var glomp_user_fname = "<?php echo $user_short_info->user_fname; ?>";
            var glomp_user_id = "<?php echo $user_short_info->user_id; ?>";
            var inTour = "<?php echo $inTour; ?>";
            var get_type = "<?php echo $get_type; ?>";
            var countries = <?php echo $countries_json; ?>;
            var GLOMP_DESK_SITE_URL = "<?php echo site_url();?>";
            var linkedIn = false;
            var FB_WHAT_TO_DO = "<?php echo $what_to_do; ?>";
            var RELOAD_DATA = '<?php echo $RELOAD_DATA; ?>';
            if(RELOAD_DATA!="")
                    RELOAD_DATA = JSON.parse(RELOAD_DATA);
            var RELOAD = "<?php echo $RELOAD; ?>";
            var max_limit = 500;
            var global_fbID = "";
            var global_Country = "";
            var global_first_name = "";
            var global_last_name = "";
            var arrSelected = "";
            var _friend_data = null;
            var _friend_data_global = null;
            var global_page = 1;
            var global_per_page = 30;
            var _selectedFriends = [];
            var fbApiInit = false;
            window.fbAsyncInit = function() {
                FB.init({
                    appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                    status: true, /* check login status*/
                    cookie: true, /* enable cookies to allow the server to access the session*/
                    xfbml: true, /* parse XFBML	  });		*/
                });
                fbApiInit = true;
            };
            /* Load the SDK Asynchronously*/
            (function(d) {
                var js, id = 'facebook-jssdk';
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

            $(document).ready(function(e)
            {
                if (inTour == 'yes') {
                    FB_WHAT_TO_DO = 'invite';
                    $('#fb_invite_button_wrapper_2').show();
                    /*$('#fb_invite_button_wrapper').show();*/
                    checkLoginStatus();
                }
                else {
                    if (get_type == 'facebook')
                    {
                        if(FB_WHAT_TO_DO != '' && RELOAD == '') {
                            return <?php echo $fb_func;?>;
                        }
                        else if(RELOAD == 'true')
                        {
                            window.fbAsyncInit = function() {
                            FB.init({
                                appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                                status: true, /* check login status*/
                                cookie: true, /* enable cookies to allow the server to access the session*/
                                xfbml: true  /* parse XFBML*/
                            });
                            fbApiInit = true;
                            FB.getLoginStatus(function(response)
                            {
                                if (response && response.status == 'connected')
                                {
                                    continue_fb(RELOAD_DATA);
                                }
                                else
                                {
                                }
                            });
                            }
                           return;
                        }
                        
                        return askFbOption();
                    }
                    else
                    {
                        <?php
                        if(isset($_GET['js_func_li'])) {
                            //LinkedIn function REST on JS
                            echo $_GET['js_func_li'].';';
                        } else {
                            echo 'askLinkedInOption();';
                        }
                        ?>
                    }
                }
            });

            $(function() {
                $("#tour_add").click(function() {
                    $("#invite_fb_friends").click();
                });
                $("#tour_add_all").click(function() {
                    $("#chk_select_all").click();
                    $("#invite_fb_friends").click();
                });
                $("#tour_cancel").click(function() {
                    /*checkIfInTour();*/
                    window.location.href = GLOMP_BASE_URL + 'm/user/dashboard/?inTour=3';
                });


                $('#searchTextField').keyup(function() {
                    getKey();
                });

                $("#page_prev").click(function() {
                    global_page--;
                    loadPage();
                });
                $("#page_next").click(function() {
                    global_page++;
                    loadPage();
                });
                
                $("#li_page_prev").click(function() {
                    global_page--;
                    liloadPage();
                });
                $("#li_page_next").click(function() {
                    global_page++;
                    liloadPage();
                });
                
                $("#chk_select_all").click(function() {
                    $(".friendThumbSelected").prop('checked', ($("#chk_select_all").prop('checked')));
                    
                    if ($("#chk_select_all").prop('checked')) {
                        hasList = false;
                        idList = new Array();
                        $.each(_friend_data_global, function(i, item) {
                            idList.push(item.id);
                            hasList = true;
                        });
                        if (hasList)
                        {
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus',
                                data: 'idList=' + idList,
                                success: function(response) {
                                    friendsStatus = response;
                                    ctr = 0;
                                    $.each(_friend_data_global, function(i, item) {
                                        tempID = 'fbid_' + item.id;
                                        friendStat = friendsStatus[tempID];
                                        friendStat = friendStat.split(":");
                                        if (friendStat[0] == -1)
                                        {
                                            var friendID = item.id;
                                            _selectedFriends.push(friendID);
                                        }
                                    });
                                }
                            });
                        }
                    }
                    else
                    {
                        _selectedFriends = [];
                    }

                    $('.friendThumbSelected').each(function(i, item) {
                        if ($(this).prop('checked')) {
                            var friendID = $(this).val();
                            removeByValue(_selectedFriends, friendID);
                            _selectedFriends.push(friendID);
                        }
                        else
                        {
                            var friendID = $(this).val();
                            removeByValue(_selectedFriends, friendID);
                        }
                    });
                    
                });
                $("#li_chk_select_all").click(function() {
                    $(".friendThumbSelected").prop('checked', ($("#li_chk_select_all").prop('checked')));
                    
                    if ($("#li_chk_select_all").prop('checked')) {
                        hasList = false;
                        idList = new Array();
                        $.each(_friend_data_global, function(i, item) {
                            idList.push(item.id);
                            hasList = true;
                        });
                        if (hasList)
                        {
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_2',
                                data: {
                                    idList: idList,  
                                    field: 'user_linkedin_id',  
                                },
                                success: function(response) {
                                    friendsStatus = response;
                                    ctr = 0;
                                    $.each(_friend_data_global, function(i, item) {
                                        tempID = 'linkedInId_' + item.id;
                                        friendStat = friendsStatus[tempID];
                                        friendStat = friendStat.split(":");
                                        if (friendStat[0] == -1)
                                        {
                                            var friendID = item.id;
                                            _selectedFriends.push(friendID);
                                        }
                                    });
                                }
                            });
                        }
                    }
                    else
                    {
                        _selectedFriends = [];
                    }

                    $('.friendThumbSelected').each(function(i, item) {
                        if ($(this).prop('checked')) {
                            var friendID = $(this).val();
                            removeByValue(_selectedFriends, friendID);
                            _selectedFriends.push(friendID);
                        }
                        else
                        {
                            var friendID = $(this).val();
                            removeByValue(_selectedFriends, friendID);
                        }
                    });
                    
                });
                $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();
                });
                $("#fb_button").click(function() {
                    askFbOption();
                });
                $('#invite_li_friends').click(function(){
                    $("#invite_fb_friends").click(); 
                });
                $("#invite_fb_friends").click(function() {
                    /* check if some friends are selected*/
                    var j = 0;
                    var selectedFriends = [];
                    $('.friendThumbSelected').each(function(i, item) {
                        if ($(this).prop('checked')) {
                            var friendID = $(this).val();
                            selectedFriends.push(friendID);
                            j++;
                        }
                    });
                    if (j > 0) {
                        if (get_type =='linkedin') return doInviteLiFriends(selectedFriends);
                        doInviteFbFriends(selectedFriends);
                        return false;
                    }
                    else {
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                        <div id="personalMessageTips" style="padding:2px;font-size:14px;">No friends selected.</div> \
                                                        <p></div>');
                        NewDialog.dialog({
                            dialogClass: 'dialog_style_glomped_alerts',
                            autoOpen: false,
                            resizable: false,
                            modal: true,
                            width: 220,
                            height: 120,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $(this).dialog("close");
                                        $('#messageDialog').dialog('destroy').remove();
                                        $('#listDialog').dialog("close");
                                        $('#listDialog').dialog('destroy').remove();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                    /* check if some friends are selected*/


                    /*	$(this).submit(function() {
                     return false;
                     });
                     return true;*/
                });
                $(".addThisFriendClass").click(function(ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                    /*alert($(this).prop("id"));*/
                    var id = $(this).prop("id");
                    var name = $('#data_name_' + id).html();
                    var loc = $('#data_loc_' + id).html();
                    var desc = "Add " + name + " to Your glomp! network?";
                    var img = $('#data_img_' + id).prop('src');
                    var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
                                                                                        <div class="friendThumb3" > \
                                                                                                <div style="width:60;height:80px; border:0px solid #333; overflow:hidden" > \
                                                                                                        <image id="fb_friend_popup_img" style="width: inherit; height: inherit;float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
                                                                                                </div> \
                                                                                        </div>\
                                                                                        <div class="friendThumb3_name" > \
                                                                                                <b>' + name + '</b><br /><span style="">' + loc + '</span>\
                                                                                        </div>\
                                                                                        <div class="friendThumb3_desc"  >' + desc + ' \
                                                                                        </div><div style="clear:both"></div> \
                                                                                </div>');
                    NewDialog.dialog({
                        dialogClass: 'dialog_style_glomped_ash_blue',
                        title: "",
                        autoOpen: false,
                        resizable: false,
                        modal: true,
                        width: 260,
                        height: 250,
                        show: '',
                        hide: '',
                        buttons: [{text: 'Confirm',
                                "class": 'btn-custom-blue-grey_xs w80px',
                                click: function() {
                                    $(this).dialog("close");
                                    $('#FriendPopupDialog').dialog('destroy').remove();
                                    addThisFriend(id);
                                }},
                            {text: "Cancel",
                                "class": 'btn-custom-white_xs w80px',
                                click: function() {
                                    $('#FriendPopupDialog').dialog('destroy').remove();
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                });
            });
            function askAddThisFriend(id, type) {
                var type = type || 'fb';
                var name = $('#search_name_' + id).html();
                var loc = $('#search_loc_' + id).html();
                var desc = "Add " + name + " to Your glomp! network?";
                var img = $('#img_fb_' + id).prop('src');
                var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
                                                                                <div class="friendThumb3" > \
                                                                                        <div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
                                                                                                <image id="fb_friend_popup_img" style="width: inherit; height: inherit;float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
                                                                                        </div> \
                                                                                </div>\
                                                                                <div class="friendThumb3_name" > \
                                                                                        <b>' + name + '</b><br /><span style="">' + loc + '</span>\
                                                                                </div>\
                                                                                <div class="friendThumb3_desc"  >' + desc + ' \
                                                                                </div> \
                                                                                <div class="cl"  ></div> \
                                                                        </div>');
                NewDialog.dialog({
                    dialogClass: 'noTitleStuff',
                    dialogClass:'dialog_style_glomped_ash_blue',
                            title: "",
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 300,
                    height: 250,
                    show: '',
                    hide: '',
                    buttons: [{text: 'Confirm',
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#FriendPopupDialog').dialog('destroy').remove();
                                    if (type == 'fb')
                                    {
                                        addThisFriendFB(id);
                                    }
                                    else
                                    {
                                        addThisFriendLI(id);
                                    }
                                }, 500);
                            }},
                        {text: "Cancel",
                            "class": 'btn-custom-white_xs w80px',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#FriendPopupDialog').dialog('destroy').remove();
                                }, 500);
                            }}
                    ]
                });
                NewDialog.dialog('open');
            }
            function addThisFriendLI(fbID) {
                var data = '&linkedInId=' + fbID;
                $.ajax({
                    type: "POST",
                    url: GLOMP_BASE_URL + 'ajax_post/addThisFriend',
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        $('#fb_plus_' + fbID).hide();
                        var NewDialog = $('<div id="messageDialog" align="center"> \
                                                                                                                <p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '.</p> \
                                                                                                        </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomped_alerts',
                            title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                            modal: true,
                            width: 200,
                            height: 140,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                });
            }
            function addThisFriendFB(fbID) {
                var data = '&fbID=' + fbID;
                $.ajax({
                    type: "POST",
                    url: GLOMP_BASE_URL + 'ajax_post/addThisFriend',
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        $('#fb_plus_' + fbID).hide();
                        var NewDialog = $('<div id="messageDialog" align="center"> \
                                                                                                                <p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '.</p> \
                                                                                                        </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomped_alerts',
                            title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                            modal: true,
                            width: 200,
                            height: 140,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                });
            }
            function doInviteFbFriends(selectedFriends) {
                /*console.log(selectedFriends);*/
                var names = "";
                selectedCtr = 0;
                var firstID = "";
                var tags = "";
                arrSelected = new Array();
                var notes = "";
                $.each(selectedFriends, function(i, item) {
                    selectedCtr++;

                    tags += ' @[' + item + ']\n';
                    arrSelected.push(item);
                    var friendName = $("#search_name_" + item).html();
                    if (firstID == "")
                        firstID = item;
                    if (selectedCtr <= 5) {
                        if (names != "")
                            names += ', ';
                        names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
                    }

                });

                if (selectedCtr > 5) {
                    names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

                }
                if (selectedCtr > 10)
                {
                    notes = '<hr size="1" style="padding:0px;margin:2px 0px 2px 0px ;"><div style="font-size:9px;width:280px;text-align:justify;padding:0px 0px 0px 3px" >' + GLOMP_FB_INVITE_REMINDER + '</div>';
                }
                if ($('#messageDialog').doesExist() == true) {
                    $('#messageDialog').dialog('destroy').remove();
                }

                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="15" height="15" title="Remove this message" style="float:right;margin-top:4px" onClick="removeMessage()" />';
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:11px !important;"> \
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                        <td><b>To</b>:' + names + '</td> \
                                                </div> \
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                        <hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:10px;" align="left"></div>\
                                                </div> \
                                                <div style="font-size:11px;padding:0px;text-align:justify;width:280px;" >\
                                                        <textarea id="personalMessage"  style="font-size:10px;resize:none;width:280px;height:30px;" placeholder="Your personal message"></textarea>\
                                                </div>\
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" id="definedMessage_wrap" >' + close_button + '<div id="definedMessage"  style="font-size:10px;padding:3px;text-align:justify;width:280px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div>\
                                                </div>\
                                                <div style="font-size:8px;padding:0px;text-align:justify;width:280px;" >' + notes + '</div> \
                                        </div>');
                NewDialog.dialog({
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    autoOpen: false,
                    resizable: true,
                    modal: true,
                    width: 300,
                    height: 390,
                    show: '',
                    hide: '',
                    buttons: [
                        {text: "Invite",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                var bValid = true;
                                $("#personalMessage").removeClass("ui-state-error");
                                bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 100, '#personalMessageTips');
                                if (bValid) {
                                    var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                                                <div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_glomp_wait',
                                        title: 'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width: 200,
                                        height: 120
                                    });
                                    NewDialog.dialog('open');

                                    personalMessage = $("#personalMessage").val();
                                    definedMessage = $("#definedMessage").html();
                                    definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                    definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);

                                    var profile = 'http://glomp.it/welcome.html?from=' + glomp_user_id;
                                    doGlompPost(profile, personalMessage, definedMessage, 0);
                                }

                            }},
                        {text: "Cancel",
                            "class": 'btn-custom-white_xs w80px',
                            click: function() {
                                $('#messageDialog').dialog("close");
                                $('#messageDialog').dialog('destroy').remove();
                            }}
                    ]
                });
                NewDialog.dialog('open');

            }
            function doInviteLiFriends(selectedFriends) {
                /*console.log(selectedFriends);*/
                var names = "";
                selectedCtr = 0;
                var firstID = "";
                var tags = "";
                arrSelected = new Array();
                var notes = "";
                
                $.each(selectedFriends, function(i, item) {
                    selectedCtr++;

                    tags += ' @[' + item + ']\n';
                    arrSelected.push(item);
                    var friendName = "";/*$("#search_name_"+item).html();*/
                    
                    $.each(_friend_data_global, function(i2, item2) {
                        if (item2.id == item) {
                            friendName = item2.formattedName;
                            /*break;*/
                        }
                    });
                    
                    if (firstID == "")
                        firstID = item;
                    if (selectedCtr <= 5) {
                        if (names != "")
                            names += ', ';
                        names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
                    }

                });

                if (selectedCtr > 5) {
                    names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

                }
                if (selectedCtr > 10)
                {
                    notes = '';
                }
                if ($('#messageDialog').doesExist() == true) {
                    $('#messageDialog').dialog('destroy').remove();
                }

                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("icon", "link");
                var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="15" height="15" title="Remove this message" style="float:right;margin-top:4px" onClick="removeMessage()" />';
                var NewDialog = $('<div id="messageDialog" align="center" style="font-size:11px !important;"> \
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                        <td><b>To</b>:' + names + '</td> \
                                                </div> \
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                        <hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:10px;" align="left"></div>\
                                                </div> \
                                                <div style="font-size:11px;padding:0px;text-align:justify;width:280px;" >\
                                                        <textarea id="personalMessage"  style="font-size:10px;resize:none;width:280px;height:30px;" placeholder="Your personal message"></textarea>\
                                                </div>\
                                                <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" id="definedMessage_wrap" >' + close_button + '<div id="definedMessage"  style="font-size:10px;padding:3px;text-align:justify;width:280px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div>\
                                                </div>\
                                                <div style="font-size:8px;padding:0px;text-align:justify;width:280px;" >' + notes + '</div> \
                                        </div>');
                NewDialog.dialog({
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    autoOpen: false,
                    resizable: true,
                    modal: true,
                    width: 300,
                    height: 390,
                    show: '',
                    hide: '',
                    buttons: [
                        {text: "Invite",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                var bValid = true;
                                $("#personalMessage").removeClass("ui-state-error");
                                bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 100, '#personalMessageTips');
                                if (bValid) {
                                    var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                                                <div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                    NewDialog.dialog({
                                        autoOpen: false,
                                        closeOnEscape: false,
                                        resizable: false,
                                        dialogClass: 'dialog_style_glomp_wait',
                                        title: 'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width: 200,
                                        height: 120
                                    });
                                    NewDialog.dialog('open');

                                    personalMessage = $("#personalMessage").val();
                                    definedMessage = $("#definedMessage").html();
                                    definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                    definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }, 500);

                                    var profile = 'http://glomp.it/?from=' + glomp_user_id;
                                    
                                    if (personalMessage == '') {
                                        personalMessage = 'Its time for a treat!'
                                    }
                                    
                                    doGlompPostLi(profile, personalMessage, definedMessage, 0);
                                }

                            }},
                        {text: "Cancel",
                            "class": 'btn-custom-white_xs w80px',
                            click: function() {
                                $('#messageDialog').dialog("close");
                                $('#messageDialog').dialog('destroy').remove();
                            }}
                    ]
                });
                NewDialog.dialog('open');

            }            
            function doGlompPostLi(profile, personalMessage, definedMessage, ctr) {
                var lim = 10;
                var tags = "";
                var invitedIDs = new Array();
                var invitedNames = new Array();
                var receipients = new Array();
                for (; ctr < selectedCtr; ) {
                    tags += ' @[' + arrSelected[ctr] + ']\n';

                    invitedIDs.push(arrSelected[ctr]);
                    $.each(_friend_data_global, function(i2, item2) {
                        if (item2.id == arrSelected[ctr]) {
                            friendName = item2.name;
                            receipient = {
                                "person": {
                                    "_path": "/people/" + item2.id,
                                }
                            };
                        }
                    });
                    invitedNames.push(friendName);
                    receipients.push(receipient);

                    lim--;
                    ctr++;
                    if (lim == 0)
                        break;
                }
                /*if(ctr<selectedCtr){
                 doGlompPost(profile, personalMessage, definedMessage, ctr);
                 }*/
                params = {
                    js_func_li: '',
                    api: '/v1/people/~/mailbox',
                    mail: 'true',
                    body: JSON.stringify({
                        "recipients": {
                            "values": receipients
                        },
                        "subject": personalMessage,
                        "body": '\n' + personalMessage + '\n\n' + definedMessage + '\n\n' + profile
                    }),
                };
            
                var res = linkedin_api(params);
                
                /*IN.API.Raw("people/~/mailbox")
                        .method("POST")
                        .body(JSON.stringify({
                    "recipients": {
                        "values": receipients
                    },
                    "subject": personalMessage,
                    "body": '\n' + personalMessage + '\n\n' + definedMessage + '\n\n' + profile
                })
                        ).result(function(result) { */
                    $.ajax({
                        type: "POST",
                        url: GLOMP_BASE_URL + 'ajax_post/li_log_invite',
                        data: 'invitedIDs=' + invitedIDs + '&invitedNames=' + invitedNames,
                    });

                    if (ctr < selectedCtr) {
                        doGlompPostLi(profile, personalMessage, definedMessage, ctr);
                    }
                    else {
                        arrSelected = "";
                        _selectedFriends = "";
                        $('#confirmPopup').dialog('destroy').remove();
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                            <div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
                        <p></div>');
                        NewDialog.dialog({
                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                            autoOpen: false,
                            resizable: false,
                            modal: true,
                            width: 220,
                            height: 120,
                            show: 'clip',
                            hide: 'clip',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('#messageDialog').dialog("close");
                                        $('#messageDialog').dialog('destroy').remove();
                                        $('#listDialog').dialog("close");
                                        $('#listDialog').dialog('destroy').remove();
                                        checkIfInTour();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }

                /*});*/

            }
            function doGlompPost_v2(profile, personalMessage, definedMessage, ctr, not_registered_ids,idList, list) {
                var lim = 10;
                var tags = "";
                for (; ctr < selectedCtr; ) {
                    //tags += ' @[' + not_registered_ids[ctr] + ']\n';
                    if(tags!='')
                        tags+=',';
                    tags += '' + not_registered_ids[ctr] + '';
                    
                           
                    lim--;
                    ctr++;
                    if (lim == 0)
                        break;
                }
                console.log(ctr + "=" + tags);
                /*
                FB.api(
                        'me/glomp_app:invite',
                        'post',
                        {
                            profile: profile,
                            message: '\n' + tags + '\n' + personalMessage + '\n\n' + definedMessage
                        },*/
                 FB.api(
                    '/me/feed',
                     "POST",
                    {
                        "message": personalMessage + '\n\n' + definedMessage,
                        "tags"  :tags,
                        "link" : profile
                    },
                function(response) {
                    var log_details = {
                        type: 'fb_api',
                        event: 'FB ' + FB_WHAT_TO_DO,
                        log_message: 'Inviting member.',
                        url: window.location.href,
                        response: JSON.stringify(response)
                    };

                    $('#confirmPopup').dialog('destroy').remove();
                    if (hasOwnProperty(response, 'id')) {
                        if (ctr < selectedCtr) {
                            doGlompPost(profile, personalMessage, definedMessage, ctr);
                        }
                        else {
                            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                                        <div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
                                                                        <p></div>');
                            NewDialog.dialog({
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 220,
                                height: 120,
                                show: 'clip',
                                hide: 'clip',
                                buttons: [
                                    {text: "Ok",
                                        "class": 'btn-custom-white_xs w80px',
                                        click: function() {
                                            $('#messageDialog').dialog("close");
                                            $('#messageDialog').dialog('destroy').remove();
                                            $('#listDialog').dialog("close");
                                            $('#listDialog').dialog('destroy').remove();
                                            
                                            
                                            if (not_registered_ids.length != idList.length)
                                            {
                                                var text = 'Some of your selected FB friends are already in glomp!: ';
                                                var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                        <div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
                                                                        <div style="max-height:250px;overflow-y:auto;float:left; border-radius:4px; border: solid #fff 1px;width:291px;padding:4px 4px">\
                                                                        '+list+'\
                                                                        </div>\
                                                                        </div>');
                                                    NewDialog.dialog({						
                                                        autoOpen: false,
                                                        closeOnEscape: false ,
                                                        resizable: false,					
                                                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                                        title:'',
                                                        modal: true,
                                                        width:320,
                                                        position: 'center',
                                                        buttons: [{text: 'OK',
                                                                "class": 'btn-custom-darkblue_xs',
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                                                                    //checkIfInTour();
                                                                }}
                                                        ]
                                                            
                                                    });
                                                NewDialog.dialog('open');
                                            }/*if (not_registered_ids.length != idList.length)*/
                                            else
                                            {
                                                //checkIfInTour();
                                                window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                                            }
                                            
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    }
                    else if (response.error) {
                        $('#confirmPopup').dialog('destroy').remove();
                        $('#listDialog').dialog("close");
                        setTimeout(function() {
                            $('#listDialog').dialog('destroy').remove();
                        }, 500);
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                                        Sorry there appears to be a loading error. Please click <a href="javascript:void(0);" onclick="reloadPage()" style="text-decoration:underline" >here</a> to try again.\
                                    </div> \
                                    <p></div>');
                        NewDialog.dialog({
                            dialogClass: 'dialog_style_glomp_wait',
                            autoOpen: false,
                            resizable: false,
                            closeOnEscape: false,
                            modal: true,
                            width: 280,
                            height: 150,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Cancel",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('.friendThumbSelected').each(function(i, item) {
                                            if ($(this).prop('checked')) {
                                                $(this).prop('checked', false);
                                            }
                                        });

                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#messageDialog').dialog('destroy').remove();
                                        }, 500);
                                        $('#listDialog').dialog("close");
                                        setTimeout(function() {
                                            $('#listDialog').dialog('destroy').remove();
                                        }, 500);
                                        window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                                        
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                    else {
                        $('#listDialog').dialog("close");
                        setTimeout(function() {
                            $('#listDialog').dialog('destroy').remove();
                        }, 500);
                    }
                    /* handle the response
                     console.log(response);
                     /*var URL='http://facebook.com/'+response.id;*/
                    /*window.open(URL);*/
                }
                );

            }
            function doGlompPost(profile, personalMessage, definedMessage, ctr) {
                var lim = 10;
                var tags = "";
                var invitedIDs = new Array();
                var invitedNames = new Array();
                for (; ctr < selectedCtr; ) {
                    tags += ' @[' + arrSelected[ctr] + ']\n';

                    invitedIDs.push(arrSelected[ctr]);
                    $.each(_friend_data_global, function(i2, item2) {
                        if (item2.id == arrSelected[ctr]) {
                            friendName = item2.name;
                        }
                    });
                    invitedNames.push(friendName);

                    lim--;
                    ctr++;
                    if (lim == 0)
                        break;
                }
                console.log(ctr + "=" + tags);
                /*if(ctr<selectedCtr){
                 doGlompPost(profile, personalMessage, definedMessage, ctr);
                 }*/

                FB.api(
                        'me/glomp_app:invite',
                        'post',
                        {
                            profile: profile,
                            message: '\n' + tags + '\n' + personalMessage + '\n\n' + definedMessage
                        },
                function(response) {
                    var log_details = {
                        type: 'fb_api',
                        event: 'FB ' + FB_WHAT_TO_DO,
                        log_message: 'Inviting member.',
                        url: window.location.href,
                        response: JSON.stringify(response)
                    };

                    var logs = [];
                    logs.push(JSON.stringify(log_details));
                    /*save_system_log(logs);*/
                    /*log_invite*/
                    $.ajax({
                        type: "POST",
                        url: GLOMP_BASE_URL + 'ajax_post/fb_log_invite',
                        data: 'invitedIDs=' + invitedIDs + '&invitedNames=' + invitedNames,
                        success: function(res) {

                        }
                    });
                    /*log_invite*/

                    $('#confirmPopup').dialog('destroy').remove();
                    if (hasOwnProperty(response, 'id')) {
                        if (ctr < selectedCtr) {
                            doGlompPost(profile, personalMessage, definedMessage, ctr);
                        }
                        else {
                            var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                                        <div id="personalMessageTips" style="padding:2px;font-size:14px;">Invite sent.</div> \
                                                                        <p></div>');
                            NewDialog.dialog({
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 220,
                                height: 120,
                                show: 'clip',
                                hide: 'clip',
                                buttons: [
                                    {text: "Ok",
                                        "class": 'btn-custom-white_xs w80px',
                                        click: function() {
                                            $('#messageDialog').dialog("close");
                                            $('#messageDialog').dialog('destroy').remove();
                                            $('#listDialog').dialog("close");
                                            $('#listDialog').dialog('destroy').remove();
                                            checkIfInTour();
                                        }}
                                ]
                            });
                            NewDialog.dialog('open');
                        }
                    }
                    else if (response.error) {
                        $('#confirmPopup').dialog('destroy').remove();
                        $('#listDialog').dialog("close");
                        setTimeout(function() {
                            $('#listDialog').dialog('destroy').remove();
                        }, 500);
                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                    <div id="personalMessageTips" style="padding:2px;font-size:14px;">\
                                        Sorry there appears to be a loading error. Please click <a href="javascript:void(0);" onclick="reloadPage()" style="text-decoration:underline" >here</a> to try again.\
                                    </div> \
                                    <p></div>');
                        NewDialog.dialog({
                            dialogClass: 'dialog_style_glomp_wait',
                            autoOpen: false,
                            resizable: false,
                            closeOnEscape: false,
                            modal: true,
                            width: 280,
                            height: 150,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Cancel",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('.friendThumbSelected').each(function(i, item) {
                                            if ($(this).prop('checked')) {
                                                $(this).prop('checked', false);
                                            }
                                        });

                                        $(this).dialog("close");
                                        setTimeout(function() {
                                            $('#messageDialog').dialog('destroy').remove();
                                        }, 500);
                                        $('#listDialog').dialog("close");
                                        setTimeout(function() {
                                            $('#listDialog').dialog('destroy').remove();
                                        }, 500);
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                    else {
                        $('#listDialog').dialog("close");
                        setTimeout(function() {
                            $('#listDialog').dialog('destroy').remove();
                        }, 500);
                    }
                    /* handle the response
                     console.log(response);
                     /*var URL='http://facebook.com/'+response.id;*/
                    /*window.open(URL);*/
                }
                );

            }
            function reloadPage()
            {
                $('#messageDialog').dialog("close");
                setTimeout(function() {
                    $('#messageDialog').dialog('destroy').remove();
                }, 500);
                location.reload();
            }

            /*search friends*/

            function searchFriends() {
                var searchThis = ($.trim($("#searchTextField").val())).toLowerCase();
                if (searchThis != "") {
                    var found = 0;
                    _friend_data = new Array();
                    /*console.log(_friend_data_global);*/
                    $.each(_friend_data_global, function(i, item) {
                        if (get_type == 'facebook')
                        {
                            var name = (item.name).toLowerCase();
                        }
                        else
                        {
                            var name = (item.formattedName).toLowerCase();
                        }
                        if (name.indexOf(searchThis) >= 0) {
                            _friend_data.push(item);
                            found++;
                        }
                    });
                    global_page = 1;
                    if (found == 0) {
                        $('#friendsList').html('');
                        afterCreateFbFriendsList();
                    }
                    else {
                        var count = _friend_data.length;
                        if (get_type == 'facebook')
                        {
                            paginateFbFriends(global_page, global_per_page, count, _friend_data);
                        }
                        else
                        {
                            paginateLiFriends(global_page, global_per_page, count, _friend_data);
                        }
                    }
                }
                else {
                    _friend_data = _friend_data_global;
                    global_page = 1;
                    var count = _friend_data.length;
                    if (get_type == 'facebook')
                    {
                        paginateFbFriends(global_page, global_per_page, count, _friend_data);
                    }
                    else
                    {
                            paginateLiFriends(global_page, global_per_page, count, _friend_data);
                    }
                }
            }

            var cc = 0;
            var t;
            var timer_is_on = 0;

            function timedCount(type)
            {
                cc = cc + 1;
                if (cc == 2) {
                    searchFriends();
                }
                else
                    t = setTimeout("timedCount()", 500);
            }
            function doTimer() {
                cc = 0;
                if (!timer_is_on) {
                    timer_is_on = 1;
                    timedCount();
                }
            }
            function stopCount() {
                cc = 0;
                clearTimeout(t);
                timer_is_on = 0;
            }
            function getKey() {
                stopCount();
                doTimer();
            }
            function getCheck(_this) {
                _this.checked = !(_this.checked);
            }
            function checkLength(o, n, min, max, tipsID) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips(tipsID, "Length of " + n + " must be between " + min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }
            function updateTips(tipsID, t) {
                $(tipsID).html(t);
                /*.addClass( "ui-state-highlight" );*/
                setTimeout(function() {
                    $(tipsID).removeClass("ui-state-highlight", 1500);
                }, 500);
            }
            function addThisFriend(id) {
                var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                <div align="center" style="margin-top:5px;"><img width="40" src="<?php echo base_url() ?>assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait',
                    title: 'Please wait...',
                    modal: true,
                    position: 'center',
                    width: 200,
                    height: 120
                });
                NewDialog.dialog('open');
                var data = '&id=' + id;
                $.ajax({
                    type: "POST",
                    url: GLOMP_BASE_URL + 'ajax_post/addThisFriendNotFB',
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        $('#confirmPopup').dialog('destroy').remove();
                        $('#right_wrapper_' + id).html($('#hidden_' + id).html());
                        var NewDialog = $('<div id="messageDialog" align="center"> \
                                                                                                                <p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '.</p> \
                                                                                                        </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomped_alerts',
                            title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                            modal: true,
                            width: 200,
                            height: 140,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                });
            }
            function removeMessage() {
                $('#definedMessage_wrap').html('');
                $('#messageDialog').dialog({
                    width: 300,
                    height: 230,
                });
            }
            function askFbOption() {
                var NewDialog = $('<div id="confirmPopup" align="center">\
                    <div align="center" style="margin-top:-5px;"><strong>'+GLOMP_ASK_OPTION+'</strong></div>\
                </div>');
                NewDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait',
                    title: '',
                    modal: true,
                    width: 200,
                    position: 'center',
                    height: 120,
                    buttons: [
                        {text: "glomp!",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                $('#confirmPopup').dialog('destroy').remove();
                                FB_WHAT_TO_DO = 'glomp';
                                _fb_config();
                            }},
                        {text: "Invite",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                $('#confirmPopup').dialog('destroy').remove()
                                FB_WHAT_TO_DO = 'invite';
                                _fb_config();
                            }},
                        {text: "Cancel",
                            /*"class": 'btn-custom-blue-grey_xs w164px',*/
                            "class": 'btn-custom-transparent-green_xs',
                            click: function() {
                                window.location = GLOMP_BASE_URL + 'm/user/searchFriends';
                                $(this).dialog("close");
                        }}
                    ]
                });
                NewDialog.dialog('open');
            }

            function askLinkedInOption() {
                var NewDialog = $('<div id="messageDialog" align="center"> \
                                    <div align="center"><p style="padding:15px 0px;">' + GLOMP_ASK_OPTION + '</p></div> \
                                </div>');
                NewDialog.dialog({
                    autoOpen: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                    modal: true,
                    width: 200,
                    height: 120,
                    show: 'clip',
                    hide: 'clip',
                    buttons: [
                        {text: "glomp!",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                _glomp_li_config();
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);
                            }},
                        {text: "Invite",
                            "class": 'btn-custom-blue-grey_xs w80px',
                            click: function() {
                                _invite_li_config();
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#messageDialog').dialog('destroy').remove();
                                }, 500);
                            }},
                        {text: "Cancel",
                            /*"class": 'btn-custom-blue-grey_xs w164px',*/
                            "class": 'btn-custom-transparent-green_xs',
                            click: function() {
                                window.location = GLOMP_BASE_URL + 'm/user/searchFriends';
                                $(this).dialog("close");
                        }}
                    ]
                });
                NewDialog.dialog('open');
            }
            function _glomp_li_config() {
                $('#li_invite_button_wrapper').hide();
                $('#fb_invite_button_wrapper_2').show();
                $('#li_paginate').show();
                FB_WHAT_TO_DO = 'glomp';
                createLiFriendsList();
            }
            
            function _invite_li_config() {
                $('#li_invite_button_wrapper').show();
                $('#fb_invite_button_wrapper_2').show();
                $('#li_paginate').show();
                FB_WHAT_TO_DO = 'invite';
                createLiFriendsList();
            }
            function _fb_config() {
                if (FB_WHAT_TO_DO == 'glomp') {
                    $('#fb_invite_button_wrapper').hide();
                    $('#fb_invite_button_wrapper_2').show();
                    $('#fb_paginate').show();
                    return checkLoginStatus();
                }
                
                if (FB_WHAT_TO_DO == 'invite') {
                    $('#fb_invite_button_wrapper').show();
                    $('#fb_invite_button_wrapper_2').show();
                    $('#fb_paginate').show();
                    return checkLoginStatus();
                }
            }

            var facebookDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
                Connecting to Facebook...<br><br><img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
            </div></div>');
            function checkLoginStatus() {
                facebookDialog.dialog({
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title: '',
                    autoOpen: false,
                    resizable: false,
                    closeOnEscape: false,
                    modal: true,
                    width: 280,
                    height: 120,
                    show: '',
                    hide: ''
                });
                facebookDialog.dialog("open");
                var log_details = {
                    type: 'fb_api',
                    event: 'FB ' + FB_WHAT_TO_DO,
                    log_message: 'Connecting to facebook',
                    url: window.location.href,
                    response: '',
                    from_url: 'facebook_friends',
                    from_what_to_do: FB_WHAT_TO_DO
                };
                var logs = [];
                logs.push(JSON.stringify(log_details));
                /*save_system_log(logs);*/

                setTimeout(function()
                {

                    if (fbApiInit)
                    {
                        FB.getLoginStatus(function(response)
                        {
							console.log(response);
                            if (response && response.status == 'connected') {
                                var log_details = {
                                    type: 'fb_api',
                                    event: 'FB ' + FB_WHAT_TO_DO,
                                    log_message: 'Connected to facebook',
                                    url: window.location.href,
                                    response: JSON.stringify(response)
                                };
                                
                                var logs = [];
                                //logs.push(JSON.stringify(log_details));
                                /*save_system_log(logs);*/
                                checkFBMatch();
                                /*sendRequestToRecipients();*/
                            } else {
                                {
                                    var return_url = GLOMP_BASE_URL +  'm/user/facebookFriends?js_fb_func=_fb_config()|'+FB_WHAT_TO_DO;
                                    return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token");
                                }
                                /* Display the login button*/
                            }
                        }
                        );
                    }
                    else {
                        window.fbAsyncInit = function() {
                            FB.init({
                                appId: FB_APP_ID, /*    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
                                status: true, /* check login status*/
                                cookie: true, /* enable cookies to allow the server to access the session*/
                                xfbml: true  /* parse XFBML*/
                            });
                            fbApiInit = true;
                            FB.getLoginStatus(function(response)
                            {
                                if (response && response.status == 'connected') {
                                    var log_details = {
                                        type: 'fb_api',
                                        event: 'FB ' + FB_WHAT_TO_DO,
                                        log_message: 'Connected to facebook',
                                        url: window.location.href,
                                        response: JSON.stringify(response)
                                    };

                                    var logs = [];
                                    logs.push(JSON.stringify(log_details));
                                    /*save_system_log(logs);*/
                                
                                    createFbFriendsList();
                                    /*sendRequestToRecipients();*/
                                } else {
                                    if(facebook_alert_error())
                                    {
                                        FB.login(
                                                function(response)
                                                {
                                                    if (response.status === 'connected') {
                                                        var log_details = {
                                                            type: 'fb_api',
                                                            event: 'FB ' + FB_WHAT_TO_DO,
                                                            log_message: 'Connected to facebook',
                                                            url: window.location.href,
                                                            response: JSON.stringify(response)
                                                        };

                                                        var logs = [];
                                                        logs.push(JSON.stringify(log_details));
                                                        /*save_system_log(logs);*/
                                                    
                                                        createFbFriendsList();
                                                        /*sendRequestToRecipients();*/
                                                    }
                                                    else
                                                    {
                                                        var log_details = {
                                                            type: 'fb_api',
                                                            event: 'FB ' + FB_WHAT_TO_DO,
                                                            log_message: 'User cancelled login or did not fully authorize.',
                                                            url: window.location.href,
                                                            response: JSON.stringify(response)
                                                        };

                                                        var logs = [];
                                                        logs.push(JSON.stringify(log_details));
                                                        facebookDialog.dialog("close");
                                                        /*save_system_log(logs);*/
                                                    
                                                    }
                                                }, {scope: 'publish_actions,email'}
                                        );
                                    }
                                    /* Display the login button          */
                                }
                            }
                            );
                        }
                    }
                }, 500);
            }
            function loadPage() {
                if (_friend_data == null) {
                    FB.api('/me/friends', {fields: 'name,id,location,birthday'}, function(response) {
                        _friend_data = response.data.sort(sortByName);
                        _friend_data_global = _friend_data;
                        count = _friend_data.length;
                        paginateFbFriends(global_page, global_per_page, count, _friend_data);
                    });
                }
                else {
                    /*_friend_data = response.data.sort(sortByName);*/
                    count = _friend_data.length;
                    paginateFbFriends(global_page, global_per_page, count, _friend_data);
                }
            }

            function liloadPage() {
                if (_friend_data == null) {
                    params = {
                        js_func_li: '',
                        api: '/v1/people/~/connections:(id,formatted-name,email-address,first-name,last-name,location:(country:(code)),picture-urls::(original))'
                    };
            
                    var res = linkedin_api(params);
                    res = res.values[0];
                    
                    /*IN.API.Connections("me")*/
                    /*.fields("id","formatted-name", "email-address", "first-name", "last-name", "location:(country:(code))", "picture-urls::(original)")*/
                    /*.result(function(res) {*/
                        res.values.removeByValue('private');

                        /*TODO: create a generic sorting function */
                       _friend_data = res.values.sort(function(a, b){
                            var x = a.formattedName.toLowerCase();
                            var y = b.formattedName.toLowerCase();

                            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                       });

                        _friend_data_global = _friend_data;

                       count = _friend_data.length;
                       paginateLiFriends(global_page, global_per_page, count, _friend_data);
                    /*});*/
                }
                else {
                    /*_friend_data = response.data.sort(sortByName);*/
                    count = _friend_data.length;
                    paginateLiFriends(global_page, global_per_page, count, _friend_data);
                }
            }
            function paginateFbFriends(page, limit, max, data) {
                max = Math.ceil(max / limit);
                if (global_page <= 1) {
                    $("#page_prev").prop("disabled", true);
                }
                else {
                    $("#page_prev").prop("disabled", false);
                }
                if (global_page >= max) {
                    $("#page_next").prop("disabled", true);
                }
                else {
                    $("#page_next").prop("disabled", false);
                }

                start_here = (page - 1) * limit;
                stop_here = start_here + limit;
                hasList = false;
                idList = new Array();
                ctr = 0;
                $.each(data, function(i, item) {
                    if (ctr >= start_here && stop_here > ctr) {
                        idList.push(item.id);
                        hasList = true;
                    }
                    ctr++;
                });
                if (hasList)
                {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus',
                        data: 'idList=' + idList,
                        success: function(response) {
                            friendsStatus = response;
                            $('#friendsList').html('');
                            $('#friendsList').addClass('content2');
                            /*(response.fbid_24102928);*/
                            ctr = 0;
                            $.each(data, function(i, item) {

                                if (ctr >= start_here && stop_here > ctr) {
                                    setTimeout(function() {
                                        tempID = 'fbid_' + item.id;
                                        friendStat = friendsStatus[tempID];
                                        addToList(item, friendStat, function(status) {
                                            if (status == 'OK') {
                                                /*do stuff...*/
                                            }
                                        });
                                    }, i * 1);
                                }
                                ctr++;
                            });
                            setTimeout(function() {
                                setCheckSelecAllValue();
                            }, ctr * 1);
                        }
                    });
                }
                else {
                    /*afterCreateFbFriendsList();*/
                }

            }
            function linkedin_api(obj) {
                var data;
                obj.mail = obj.mail || '';
                obj.body = obj.body || '';
                
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    url: GLOMP_BASE_URL + 'ajax_post/linkedIn_callback',
                    data: {
                        redirect_li: window.location.href,
                        js_func_li: obj.js_func_li,
                        api: obj.api,
                        mail: obj.mail,
                        body: obj.body,
                    },
                    success: function(_data) {
                        if (_data.redirect_window != '') window.location = _data.redirect_window;
                        data = _data;
                    }
                });
                return data;
            }
            /* temporary until LinkedIn okay */
            function returnHome() {
                return window.location = GLOMP_BASE_URL + 'm/user';
            }
            
            function createLiFriendsList() {
                var NewDialog = $('<div id="messageDialog" align="left"> \
                                    <div style="width: auto; height: auto;margin-bottom: 12px;"><div style="float: right; cursor: pointer;" onclick="returnHome();">X</div></div> \
                                    <p style="padding:15px 0px;">' + GLOMP_GETTING_LI_FRIENDS + '</p> \
                                </div>');
                NewDialog.dialog({
                    autoOpen: true,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                    title: '',
                    modal: true,
                    width: 280,
                    show: 'clip',
                    hide: 'clip',
                });
                return;
                
                $('#friendsList').html('<div align="center" id="friendsList_loading" style="margin-top:70px;text-align:center">' + GLOMP_GETTING_LI_FRIENDS + '<img style = "width="40" src="<?php echo base_url() ?>assets/m/img/ajax-loader.gif" /></div>');
                if (_friend_data == null)
                {
                    if (FB_WHAT_TO_DO == 'glomp') {
                        var return_js_func = '_glomp_li_config()'
                    } else {
                        var return_js_func = '_invite_li_config()'
                    }
                    
                    params = {
                        js_func_li: return_js_func,
                        api: '/v1/people/~/connections:(id,formatted-name,email-address,first-name,last-name,location:(country:(code)),picture-urls::(original))'
                    };
            
                    var res = linkedin_api(params);
                    
                    /*IN.API.Connections("me")*/
                    /*        .fields("id", "formatted-name", "email-address", "first-name", "last-name", "location:(country:(code))", "picture-urls::(original)")*/
                    /*       .result(function(res) {*/

                        /* Removing data with private value */
                        res = res.values[0]; /*Remove this when using JSAPI */
                        var rec = [];
                        for (var i = 0; i < res.values.length; i++) {
                            if (res.values[i].firstName != 'private' && res.values[i].lastName != 'private') {
                                rec.push(res.values[i]);
                            }
                        }
                        /*TODO: create a generic sorting function */
                        _friend_data = rec.sort(function(a, b) {
                            var x = a.formattedName.toLowerCase();
                            var y = b.formattedName.toLowerCase();
                            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                        });

                        _friend_data_global = rec.sort(function(a, b) {
                            var x = a.formattedName.toLowerCase();
                            var y = b.formattedName.toLowerCase();
                            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                        });

                        count = _friend_data.length;
                        paginateLiFriends(global_page, global_per_page, count, _friend_data);
                    /*});*/
                }
                else {
                    /*_friend_data = response.data.sort(sortByName);*/
                    count = _friend_data.length;
                    paginateLiFriends(global_page, global_per_page, count, _friend_data);
                }
            }
            function paginateLiFriends(page, limit, max, data) {
                max = Math.ceil(max / limit);
                if (global_page <= 1) {
                    $("#li_page_prev").prop("disabled", true);
                }
                else {
                    $("#li_page_prev").prop("disabled", false);
                }
                if (global_page >= max) {
                    $("#li_page_next").prop("disabled", true);
                }
                else {
                    $("#li_page_next").prop("disabled", false);
                }

                start_here = (page - 1) * limit;
                stop_here = start_here + limit;
                hasList = false;
                idList = new Array();
                ctr = 0;
                $.each(data, function(i, item) {
                    if (ctr >= start_here && stop_here > ctr) {
                        idList.push(item.id);
                        hasList = true;
                    }
                    ctr++;
                });
                if (hasList)
                {

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_2',
                        data: {
                            idList: idList,
                            field: 'user_linkedin_id',
                        },
                        success: function(response) {
                            friendsStatus = response;
                            $('#friendsList').html('');
                            $('#friendsList').addClass('content2');
                            /*(response.fbid_24102928);*/
                            ctr = 0;
                            $.each(data, function(i, item) {
                                if (ctr >= start_here && stop_here > ctr) {
                                    setTimeout(function() {
                                        tempID = 'linkedInId_' + item.id;
                                        friendStat = friendsStatus[tempID];
                                        addToListLi(item, friendStat, function(status) {
                                            if (status == 'OK') {
                                                /*do stuff...*/
                                            }
                                        });
                                    }, i * 0);
                                }
                                ctr++;
                            });
                            setTimeout(function() {
                                setCheckSelecAllValueLi();
                            }, ctr * 1);
                        }
                    });
                }
                else {
                    afterCreateFbFriendsList();
                }
            }
            function createFbFriendsList() {
			
                try{facebookDialog.dialog("close");} catch(e){}
                //$('#friendsList').html('<div align="center" id="friendsList_loading" style="margin-top:70px;text-align:center">' + GLOMP_GETTING_FB_FRIENDS + '<img width="40" src="<?php echo base_url() ?>assets/m/img/ajax-loader.gif" /></div>');
                doFacebookDelay('facebookLoadingDialog', 'friendsList_loading', 1000, 'no_popup', 'facebookConnectDialog');
                clearFacebookDelay();//clear the timeout
				if (true)
				{
                       
					FB.ui({method: 'apprequests',
						message: 'glomp!',
					}, function(response)
					{
                         if ( hasOwnProperty(response, 'error_code') ) 
                             window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                        else
                        window.location =  GLOMP_BASE_URL +'m/user/facebookFriends?what_to_do='+FB_WHAT_TO_DO+'&reload=true&data='+JSON.stringify(response);
					});
                }
                else {
                    /*_friend_data = response.data.sort(sortByName);*/
                    count = _friend_data.length;
                    clearFacebookDelay();
                    paginateFbFriends(global_page, global_per_page, count, _friend_data);
                }
            }
            var timerFacebookDelay = null;
            function continue_fb(response)
            {
                    if (typeof response === "undefined") 
							return;
						
						else if(FB_WHAT_TO_DO == 'glomp')
						{
							if (response.to.length == 1)
							{
								/* do your fb glomping here*/
								/* do your fb glomping here*/
                                FB.getLoginStatus(function(response)
                                {
                                    console.log(response);
                                }
                                );

                                var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                    <div align="center" style="margin-top:5px;">Loading...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                                </div>');

                                NewDialog.dialog({
                                    dialogClass: 'dialog_style_glomp_wait',
                                    title: '',
                                    autoOpen: false,
                                    resizable: false,
                                    modal: true,
                                    width: 200,
                                    closeOnEscape: false,
                                    height: 150,
                                    show: '',
                                    hide: ''
                                });
                                NewDialog.dialog('open');


								idList = response.to;
								var list='';
								var not_registered_ids = new Array();

                                //Get real FB ID
                                $.ajax({
                                    type: "GET",
                                    dataType: 'json',
                                    url: GLOMP_BASE_URL + 'api1/service/fb_data?param='+ idList.join(',') +'&format=json',
                                    success: function(response) {
                                        NewDialog.dialog('close');
                                        var fbdata = response.data;
                                        var idList = [];

                                        //loop ids
                                        for (fid = 0; fid < fbdata.length; fid++) {
                                            idList.push(fbdata[fid].id);
                                        }

                                        //start check friendlist
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_new',
                                            data: 'idList=' + idList,
                                            success: function(response) {
                                                console.log(response);
                                                friendsStatus = response;
                                                
                                                tempID = 'fbid_' + idList;
                                                    friendStat = friendsStatus[tempID];
                                                    tempID = 'fbid__name_' + idList;
                                                    friend_Name =friendsStatus[tempID];
                                                    
                                                    tempID = 'fbid_user_id' + idList;
                                                    friend_user_id =friendsStatus[tempID];
                                                    
                                                    if(friendStat == 'not_registered')
                                                    {
                                                        /*fb glomp to non member*/
                                                        /*fb glomp to non member*/
                                                        FB.api(idList+ '?fields=first_name, last_name, location',
                                                        function(response) {
                                                            if(response.length)
                                                            console.log(response);
                                                            var first_name = response['first_name'];
                                                            var last_name = response['last_name'];
                                                            var name =first_name + ' '+last_name;
                                                            var elemID =idList;
                                                            
                                                            $.ajax({
                                                                type: "POST",
                                                                url: GLOMP_BASE_URL+'ajax_post/getCountryList',
                                                                success: function(res) {
                                                                    console.log(res);
                                                                    $("#waitDialog").dialog("close");
                                                                    $("#waitDialog").dialog('destroy').remove();
                                                                    var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                                                                      <div align="center" style="margin-top:5px;"><b>' + name + '</b> '+ $('#hidden_location_msg').text().replace(new RegExp('%newline%', 'g'), '<br>') +'\
                                                                                                      <br><br><select id="selected_country" name="selected_country">' + res + '</select></div>\
                                                                                                      </div>');
                                                                    NewDialog.dialog({
                                                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                                        title: '',
                                                                        autoOpen: false,
                                                                        resizable: false,
                                                                        modal: true,
                                                                        width: 320,
                                                                        height: 130,
                                                                        show: '',
                                                                        hide: '',
                                                                        buttons: [
                                                                            {text: 'OK',
                                                                                "class": 'btn-custom-blue-grey_xs',
                                                                                click: function() {
                                                                                    country = $('#selected_country').val();
                                                                                    $('#waitDialog').dialog("close");
                                                                                    $('#waitDialog').dialog('destroy').remove();
                                                                                    showGlompMenu(elemID, country, first_name, last_name);
                                                                                }},
                                                                            {text: 'Cancel',
                                                                                "class": 'btn-custom-white_xs ',
                                                                                click: function() {
                                                                                    $('#waitDialog').dialog("close");
                                                                                    $('#waitDialog').dialog('destroy').remove();
                                                                                    window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                                                                                }}
                                                                        ]
                                                                    });
                                                                    NewDialog.dialog('open');
                                                                }
                                                            });
                                                        });
                                                        
                                                        /*fb glomp to non member*/
                                                        /*fb glomp to non member*/
                                                    }
                                                    else
                                                    {
                                                         list +='<div style="width:100px;min-height:90px; margin: 3px 0px 3px 5px; background: #fff;padding: 3px;border: solid 0px #000000;color: #59606B;"> \
                                                                <div class=""> \
                                                                        <div class="" > \
                                                                            <a target="_blank" href="'+GLOMP_BASE_URL+'m/profile/view/'+friend_user_id+'"> \
                                                                            <div  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
                                                                                <image style="float:left;z-index:1;" src="https://graph.facebook.com/' + idList+ '/picture?type=large&return_ssl_results=1"   \> \
                                                                            </div> \
                                                                            <p><b style="font-size:12px;color:#333;" >' + friend_Name + '</b></p> \
                                                                            </a> \
                                                                        </div> \
                                                                </div> \
                                                            </div>';
                                                        var text = friend_Name +' is already on glomp!.';
                                                    
                                                        var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                <div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
                                                                                <div style="" align="center">\
                                                                                '+list+'\
                                                                                </div>\
                                                                                </div>');
                                                            NewDialog.dialog({                      
                                                                autoOpen: false,
                                                                closeOnEscape: false ,
                                                                resizable: false,                   
                                                                dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                                                title:'',
                                                                modal: true,
                                                                width:250,
                                                                position: 'center',
                                                                buttons: [{text: 'Go to profile',
                                                                        "class": 'btn-custom-darkblue_xs',
                                                                        click: function() {
                                                                            $(this).dialog("close");
                                                                            window.location =GLOMP_BASE_URL+'m/profile/view/'+friend_user_id;
                                                                        }}
                                                                ]
                                                                    
                                                            });
                                                            NewDialog.dialog('open');   
                                                        }   
                                                
                                            }
                                        });
                                        //end check friendlist

                                    }
                                });
								
								/* do your fb glomping here*/
								/* do your fb glomping here*/
							}
							else
							{
								if(response.to.length>1)
									var text = 'Please select 1 Facebook friend only to glomp!.';
								else
									var text = 'Please select 1 Facebook friend to glomp!.';
						 
								var NewDialog = $('<div id="confirmPopup" align="center">\
													<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
													</div>');
									NewDialog.dialog({						
										autoOpen: false,
										closeOnEscape: false ,
										resizable: false,					
										dialogClass:'dialog_style_glomp_wait noTitleStuff',
										title:'',
										modal: true,
										width:320,
										height:130,
										position: 'center',
										buttons: [{text: 'OK',
												"class": 'btn-custom-blue-grey_xs',
												click: function() {
													$("#confirmPopup").dialog("close");
													createFbFriendsList();
												}},
												{text: 'Cancel',
												"class": 'btn-custom-white_xs ',
												click: function() {
													$("#confirmPopup").dialog("close");
													window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
												}}
										]
											
									});
									NewDialog.dialog('open');
							}
						
						}
						/*if(FB_WHAT_TO_DO == 'glomp')*/
						/*if(FB_WHAT_TO_DO == 'glomp')*/
						else if(FB_WHAT_TO_DO == 'invite')
						{
							console.log(response);
							if (response.length == 0)
							{
								var NewDialog = $('<div id="confirmPopup" align="center">\
														<div align="center" style="margin:5px 0px;"><span style="font-size:14px;">Please select atleast 1 Facebook friend to invite.</span></div>\
														</div>');
									NewDialog.dialog({						
										autoOpen: false,
										closeOnEscape: false ,
										resizable: false,					
										dialogClass:'dialog_style_glomp_wait noTitleStuff',
										title:'',
										modal: true,
										width:300,
										height:130,
										position: 'center',
										buttons: [{text: 'OK',
												"class": 'btn-custom-blue-grey_xs',
												click: function() {
													$(this).dialog("close");
													createFbFriendsList();
												}},
												{text: 'Cancel',
												"class": 'btn-custom-white_xs  ',
												click: function() {
													$(this).dialog("close");
                                                    window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
													
												}}
										]
											
									});
									NewDialog.dialog('open');
							}
							else
							{
								 
								console.log(response.to);
								idList = response.to;
								var list='';
								var not_registered_ids = new Array();
								$.ajax({
									type: "POST",
									dataType: 'json',
									url: GLOMP_BASE_URL + 'ajax_post/checkThisFriendListStatus_new',
									data: 'idList=' + idList,
									success: function(response) {
										console.log(response);
										friendsStatus = response;
										$.each(idList, function(i, item)
										{
											tempID = 'fbid_' + item;
											friendStat = friendsStatus[tempID];
											tempID = 'fbid__name_' + item;
											friend_Name =friendsStatus[tempID];
											
											tempID = 'fbid_user_id' + item;
											friend_user_id =friendsStatus[tempID];
											
											
											if(friendStat == 'not_registered')
											{
												not_registered_ids.push(item);
												
											}
											else
											{
												 list +='<div style="float:left; width:100px;min-height:90px; margin: 3px 0px 3px 5px; background: #fff;padding: 3px;border: solid 0px #000000;color: #59606B;"> \
														<div class=""> \
																<div class="" > \
																	<a target="_self" href="'+GLOMP_BASE_URL+'m/profile/view/'+friend_user_id+'"> \
																	<div  style="width:70;height:100px; border:0px solid #333;cursor:pointer; overflow:hidden;" > \
																		<image style="float:left;z-index:1;" src="https://graph.facebook.com/' + item+ '/picture?type=large&return_ssl_results=1"   \> \
																	</div> \
																	<p><b style="font-size:12px;color:#333;" >' + friend_Name + '</b></p> \
																	</a> \
																</div> \
														</div> \
													</div>';
											}
										});
										console.log(not_registered_ids);
										
										
										
										/*not_registered_ids*/
										/*not_registered_ids*/
										if (not_registered_ids.length == 0)
										{
											if(idList.length==1)
												var text = 'Your selected friend is already in glomp!.';
											else
												var text = 'All of the selected FB friends are already in glomp!:';
											
											var NewDialog = $('<div id="confirmPopup" align="center">\
																	<div align="left" style="margin:5px 0px;"><span style="font-size:14px;">'+text+'</span></div>\
																		<div style="max-height:250px;overflow-y:auto;float:left; border-radius:4px; border: solid #fff 1px;width:291px;padding:4px 4px">\
																	'+list+'\
																	</div>\
																	</div>');
												NewDialog.dialog({						
													autoOpen: false,
													closeOnEscape: false ,
													resizable: false,					
													dialogClass:'dialog_style_glomp_wait noTitleStuff',
													title:'',
													modal: true,
													width:320,
													position: 'center',
													buttons: [{text: 'OK',
															"class": 'btn-custom-darkblue_xs',
															click: function() {
																$(this).dialog("close");
                                                                window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
															}}
													]
														
												});
												NewDialog.dialog('open');
										}
										else
										{
											
											//send fb message
											selectedCtr=0;
                                            var tags='';
                                            var names ='';
                                            var notes ='';
                                            var firstID ='';
                                            $.each(not_registered_ids, function(i, item)
                                            {
                                                selectedCtr++;

                                                tags += ' @[' + item + ']\n';
                                                var friendName = "";
                                                
                                                
                                                FB.api(item+ '?fields=first_name, last_name, location',
                                                function(response) {
                                                    console.log(response);
                                                    var country = "";
                                                    var first_name = response['first_name'];
                                                    var last_name = response['last_name'];
                                                    friendName =  first_name + ' ' + last_name;
                                                    
                                                    
                                                    if (firstID == "")
                                                        firstID = item;
                                                    if (selectedCtr <= 5) {
                                                        if (names != "")
                                                            names += ', ';
                                                        names += '<span style="padding:2px;font-size:12px;">' + friendName + '</span>';
                                                    }
                                                    if (selectedCtr > 5) {
                                                        names += '<span style="padding:2px;font-size:12px;"> and <b>' + (selectedCtr - 5) + '</b> other(s)</span>';

                                                    }
                                                    if (selectedCtr > 10)
                                                    {
                                                        notes = '<tr> \
                                                                            <td ><hr size="1" style="padding:0px;margin:0px;"><div style="font-size:9px;width:400px;text-align:justify;padding:0px 0px 0px 3px" >' + GLOMP_FB_INVITE_REMINDER + '</div></td> \
                                                                        </tr>';
                                                    }
                                                
                                                
                                                    if(selectedCtr == not_registered_ids.length)
                                                    {
                                                        if ($('#messageDialog').doesExist() == true) {
                                                            $('#messageDialog').dialog('destroy').remove();
                                                        }
                                                        GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                                                        GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<name>", glomp_user_fname);
                                                        GLOMP_FB_INVITE_PRE_MESSAGE = GLOMP_FB_INVITE_PRE_MESSAGE.replace("<newline>", "<br><br>");
                                                        var close_button = '<img src="' + GLOMP_BASE_URL + 'assets/images/icons/inactive.png" width="15" height="15" title="Remove this message" style="float:right;margin-top:4px" onClick="removeMessage()" />';
                                                        var NewDialog = $('<div id="messageDialog" align="center" style="font-size:11px !important;"> \
                                                                                        <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                                                                <td><b>To</b>:' + names + '</td> \
                                                                                        </div> \
                                                                                        <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" >\
                                                                                                <hr size="1" style="padding:0px;margin:0px;"><div id="personalMessageTips" style="color:#F00;padding:2px;font-size:10px;" align="left"></div>\
                                                                                        </div> \
                                                                                        <div style="font-size:11px;padding:0px;text-align:justify;width:280px;" >\
                                                                                                <textarea id="personalMessage"  style="font-size:10px;resize:none;width:280px;height:30px;" placeholder="Your personal message"></textarea>\
                                                                                        </div>\
                                                                                        <div style="font-size:10px;padding:0px;text-align:justify;width:280px;" id="definedMessage_wrap" >' + close_button + '<div id="definedMessage"  style="font-size:10px;padding:3px;text-align:justify;width:280px;" >' + GLOMP_FB_INVITE_PRE_MESSAGE + '</div>\
                                                                                        </div>\
                                                                                        <div style="font-size:8px;padding:0px;text-align:justify;width:280px;" >' + notes + '</div> \
                                                                                </div>');
                                                        NewDialog.dialog({
                                                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                            autoOpen: false,
                                                            resizable: true,
                                                            modal: true,
                                                            width: 300,
                                                            show: '',
                                                            hide: '',
                                                            buttons: [
                                                                {text: "Invite",
                                                                    "class": 'btn-custom-blue-grey_xs w80px',
                                                                    click: function() {
                                                                        ga_invite_by_facebook();
                                                                        var bValid = true;
                                                                        $("#personalMessage").removeClass("ui-state-error");
                                                                        bValid = bValid && checkLength($("#personalMessage"), "Personal message", 1, 100, '#personalMessageTips');
                                                                        if (bValid) {
                                                                            var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                                                                                        <div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div></div>');
                                                                            NewDialog.dialog({
                                                                                autoOpen: false,
                                                                                closeOnEscape: false,
                                                                                resizable: false,
                                                                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                                                                title: 'Please wait...',
                                                                                modal: true,
                                                                                position: 'center',
                                                                                width: 200,
                                                                                height: 120
                                                                            });
                                                                            NewDialog.dialog('open');

                                                                            personalMessage = $("#personalMessage").val();
                                                                            definedMessage = $("#definedMessage").html();
                                                                            definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                                                            definedMessage = definedMessage.replace("<br><br>", "\n\n");
                                                                            $(this).dialog("close");
                                                                            setTimeout(function() {
                                                                                $('#messageDialog').dialog('destroy').remove();
                                                                            }, 500);

                                                                            var profile = 'http://glomp.it/welcome.html?from=' + glomp_user_id;
                                                                            doGlompPost_v2(profile, personalMessage, definedMessage, 0, not_registered_ids,idList, list );
                                                                        }

                                                                    }},
                                                                {text: "Cancel",
                                                                    "class": 'btn-custom-white_xs w80px',
                                                                    click: function() {
                                                                        $('#messageDialog').dialog("close");
                                                                        $('#messageDialog').dialog('destroy').remove();
                                                                        window.location =  GLOMP_BASE_URL +'m/user/searchFriends';
                                                                    }}
                                                            ]
                                                        });
                                                        NewDialog.dialog('open');
                                                    }/*if(selectedCtr == not_registered_ids.length)*/
                                                    
                                                });
                                            });/*$.each(not_registered_ids, function(i, item)*/
											//send fb message
											
										}
										
										/*not_registered_ids*/
										/*not_registered_ids*/
									}
								});	
							}
						}
            }
           
            function doFacebookDelay(targetPopup, targetElement, timeout, outputType, closeThisFirst)
            {

                var message = 'We are experiencing a loading delay from Facebook.<br>Please wait a moment.';
                message = '<span style="font-size:14px">' + message + '</span>&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />';
                timerFacebookDelay = setTimeout(function()
                {
                    if (closeThisFirst != '')
                    {
                        $("#" + closeThisFirst).dialog('close');
                    }
                    if (outputType == 'popup')
                    {

                        if ($('#' + targetPopup).doesExist())
                        {/*target popupExist*/
                            $('#' + targetElement).html(message);
                        }
                        else
                        {/*create a new popup*/

                            var NewDialog = $(' <div id="' + targetPopup + '" align="center">\
                                                    <div align="center" style="margin-top:15px;" id="' + targetElement + '">' + message + '</div>\
                                                </div>');
                            NewDialog.dialog({
                                autoOpen: false,
                                closeOnEscape: false,
                                resizable: false,
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                title: '',
                                modal: true,
                                position: 'center center',
                                width: 280,
                                height: 130
                            });
                            NewDialog.dialog('open');
                        }
                    }
                    else
                    {

                        $('#' + targetElement).html(message);
                    }


                }, timeout);
            }

            function clearFacebookDelay()
            {
                clearTimeout(timerFacebookDelay);
                if ($('#facebookLoadingDialog').doesExist())
                {
                    $("#facebookLoadingDialog").dialog('close');
                    $("#facebookLoadingDialog").dialog('destroy').remove();
                }
            }
            function sortByName(a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            }
            function addToListLi(item, thisFriendStatus) {
                var loc = countries[item.location.country.code.toUpperCase()].printable_name;
                var firstName = item.firstName;
                var lastName = item.lastName;
                thisFriendStatus = thisFriendStatus.split(":");
                pic = GLOMP_BASE_URL + 'custom/uploads/users/thumb/Male.jpg';
                try {
                    var pic = item.pictureUrls.values[0];
                } catch (e) {
                }

                if (loc === '') {
                    loc = '';
                }


                var icon = '';
                var plus = '';
                var href = '';
                var hrefLink = '';
                if (thisFriendStatus[1] == -1) {
                    icon = '<image class="fr" style="margin-top:20px;margin-right:0px" width="30" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/li-icon.jpg" \>';
                    href = '<a href="javascript:void(0);" >';
                    hrefLink = 'javascript:void(0);';
                    if (FB_WHAT_TO_DO == 'invite') {
                        var chkVal = checkIfThisHasBeenChecked(_selectedFriends, item.id);
                        if (chkVal)
                            chkVal = 'checked';
                        else
                            chkVal = '';
                        plus = '<input onclick="getCheck(this)" ' + chkVal + '  type="checkbox" value="' + item.id + '" id="chk_' + item.id + '" class="hasCheckBox checkbox friendThumbSelected fr" style="margin-top:25px;margin-right:10px" />';
                    }
                    else {
                        icon = "";
                        plus = '<button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url(\'' + GLOMP_BASE_URL + '/assets/m/img/glomp_gray.png\');"></button>';
                    }
                }
                else if (thisFriendStatus[0] == 0)
                {
                    icon = '<image class="_plus" onClick="askAddThisFriend(\'' + item.id + '\',\'li\')" id="fb_plus_' + item.id + '" style="margin-top:25px;margin-right:0px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
                    href = '<a href="' + GLOMP_BASE_URL + 'm/profile/view/' + thisFriendStatus[1] + '" target="" >';
                    hrefLink = 'javascript:void(0);';
                }
                else
                {
                    href = '<a href="' + GLOMP_BASE_URL + 'm/profile/view/' + thisFriendStatus[1] + '" target="" >';
                    hrefLink = GLOMP_BASE_URL + 'm/profile/menu/' + thisFriendStatus[1] + '/?tab=favourites';
                    icon = ' <a href="' + hrefLink + '" >\
                                                                        <button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url(\'' + GLOMP_BASE_URL + '/assets/m/img/glomp_gray.png\');"></button>\
                                                                        </a>';
                }
                $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
                                                                        <div class="">' + href + '<div class="" > \
                                                                                                <div style="width:70;height:100px; border:0px solid #333; overflow:hidden;" > \
                                                                                                        <image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="' + pic + '" alt="' + item.name + '"  \> \
                                                                                                </div>' + icon + '' + plus + '<p><b id="search_name_' + item.id + '" >' + item.formattedName + '</b><br /><span style="" id="search_loc_' + item.id + '" >' + loc + '</span></p> \
                                                                                        </div> \
                                                                                </a> \
                                                                        </div> \
                                                                </div>');
                $elem = $('\
                                                <div class="body_bottom_1px friendSearcheable"  id="search_wrapper_' + item.id + '">\
                        <div class="container  p2px_0px global_margin">\
                            <div class="row">\
                                                                <div class="fl" style="border:0px solid;width:70%;">\
                                                                        <div class="fl">\
                                                                                <a href="' + hrefLink + '" >\
                                                                                        <div style="width:70px;height:70px; border:0px solid #333; overflow:hidden;" >\
                                                                                                <image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="' + pic + '" alt="' + item.name + '"  \> \
                                                                                        </div>\
                                                                                </a>\
                                                                        </div>\
                                                                        <div class="fl" style="margin-left: 5px;border:0px solid;width:65%;">\
                                                                                <a class="red" href="' + hrefLink + '" style="color:#FF0000">\
                                                                                        <b id="search_name_' + item.id + '" class="red" style="font-size: 14px;" >' + item.formattedName + '</b>\
                                                                                </a><br>\
                                                                                <a href="' + hrefLink + '" style="color:#768385" >\
                                                                                        <span  style="color:#768385;font-size: 13px;" id="search_loc_' + item.id + '" >' + loc + '</span>\
                                                                                </a>\
                                                                        </div>\
                                                                </div>\
                                                                <div class="fr" >' + icon + ' ' + plus + '\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </div>');
                $elem.val(item.id);


                $elem.on("click", function(ev) {
                    var elemID = $(this).val();
                    var name = $('#search_name_' + elemID).html();
                    if (FB_WHAT_TO_DO == 'invite') {
                        var chk = !($('#chk_' + elemID).prop('checked'));
                        $('#chk_' + elemID).prop('checked', chk);
                    }
                    else {
                        var t = $(ev.target);
                        if (t.prop("class") != "_plus")
                        {
                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                                                                          <div align="center" style="margin-top:5px;">Getting your friend\'s location...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                                                                                                          </div>');
                            NewDialog.dialog({
                                dialogClass: 'dialog_style_glomp_wait',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 200,
                                closeOnEscape: false,
                                height: 150,
                                show: '',
                                hide: ''
                            });
                            NewDialog.dialog('open');
                            var country = loc;
                            $("#waitDialog").dialog("close");
                            $("#waitDialog").dialog('destroy').remove();
                            showGlompMenuLi(elemID, country, firstName, lastName);

                        }


                    }


                });


                $('#friendsList').append($elem);

                var img = document.getElementById('img_fb_' + item.id);
                /*or however you get a handle to the IMG*/
                var width = parseInt(img.clientWidth);
                var height = parseInt(img.clientHeight);
                if ((width) > height) {
                    $('#img_fb_' + item.id).css({'height': 70});
                }
                else {
                    $('#img_fb_' + item.id).css({'width': 70});
                }

            }
            function addToList(item, thisFriendStatus) {
                thisFriendStatus = thisFriendStatus.split(":");
                loc = "";
                item.loc = loc;
                if (hasOwnProperty(item, 'location')) {
                    temp = item.location;
                    loc = temp.name;
                    item.loc = loc;
                }
                var icon = '';
                var plus = '';
                var href = '';
                var hrefLink = '';
                if (thisFriendStatus[0] == -1) {
                    icon = '<image class="fr" style="margin-top:20px;margin-right:0px" width="30" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/fb-icon.png" \>';
                    href = '<a href="javascript:void(0);" >';
                    hrefLink = 'javascript:void(0);';
                    if (FB_WHAT_TO_DO == 'invite') {
                        var chkVal = checkIfThisHasBeenChecked(_selectedFriends, item.id);
                        if (chkVal)
                            chkVal = 'checked';
                        else
                            chkVal = '';
                        plus = '<input onclick="getCheck(this)" ' + chkVal + '  type="checkbox" value="' + item.id + '" id="chk_' + item.id + '" class="hasCheckBox checkbox friendThumbSelected fr" style="margin-top:25px;margin-right:10px" />';
                    }
                    else {
                        icon = "";
                        plus = '<button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url(\'' + GLOMP_BASE_URL + '/assets/m/img/glomp_gray.png\');"></button>';
                    }
                }
                else if (thisFriendStatus[0] == 0)
                {
                    icon = '<image class="_plus" onClick="askAddThisFriend(' + item.id + ',\'fb\')" id="fb_plus_' + item.id + '" style="margin-top:25px;margin-right:0px" width="25" src="' + GLOMP_PUBLIC_IMAGE_LOC + '/icons/plus.png" \>';
                    href = '<a href="' + GLOMP_BASE_URL + 'm/profile/view/' + thisFriendStatus[1] + '" target="" >';
                    hrefLink = 'javascript:void(0);';
                }
                else
                {
                    href = '<a href="' + GLOMP_BASE_URL + 'm/profile/view/' + thisFriendStatus[1] + '" target="" >';
                    hrefLink = GLOMP_BASE_URL + 'm/profile/menu/' + thisFriendStatus[1] + '/?tab=favourites';
                    icon = ' <a href="' + hrefLink + '" >\
                                                                        <button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url(\'' + GLOMP_BASE_URL + '/assets/m/img/glomp_gray.png\');"></button>\
                                                                        </a>';
                }
                $elem = $('<div class="friendThumb2 friendSearcheable " id="search_wrapper_' + item.id + '"  style="float:left; width:100px;"> \
                                                                        <div class="">' + href + '<div class="" > \
                                                                                                <div style="width:70;height:100px; border:0px solid #333; overflow:hidden;" > \
                                                                                                        <image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="https://graph.facebook.com/' + item.id + '/picture?type=large&return_ssl_results=1" alt="' + item.name + '"  \> \
                                                                                                </div>' + icon + '' + plus + '<p><b id="search_name_' + item.id + '" >' + item.name + '</b><br /><span style="" id="search_loc_' + item.id + '" >' + loc + '</span></p> \
                                                                                        </div> \
                                                                                </a> \
                                                                        </div> \
                                                                </div>');
                $elem = $('\
                                                <div class="body_bottom_1px friendSearcheable"  id="search_wrapper_' + item.id + '">\
                        <div class="container  p2px_0px global_margin">\
                            <div class="row">\
                                                                <div class="fl" style="border:0px solid;width:70%;">\
                                                                        <div class="fl">\
                                                                                <a href="' + hrefLink + '" >\
                                                                                        <div style="width:70px;height:70px; border:0px solid #333; overflow:hidden;" >\
                                                                                                <image id="img_fb_' + item.id + '" style="float:left;z-index:1;" src="https://graph.facebook.com/' + item.id + '/picture?type=large&return_ssl_results=1" alt="' + item.name + '"  \> \
                                                                                        </div>\
                                                                                </a>\
                                                                        </div>\
                                                                        <div class="fl" style="margin-left: 5px;border:0px solid;width:65%;">\
                                                                                <a class="red" href="' + hrefLink + '" style="color:#FF0000">\
                                                                                        <b id="search_name_' + item.id + '" class="red" style="font-size: 14px;" >' + item.name + '</b>\
                                                                                </a><br>\
                                                                                <a href="' + hrefLink + '" style="color:#768385" >\
                                                                                        <span  style="color:#768385;font-size: 13px;" id="search_loc_' + item.id + '" >' + loc + '</span>\
                                                                                </a>\
                                                                        </div>\
                                                                </div>\
                                                                <div class="fr" >' + icon + ' ' + plus + '\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </div>');
                $elem.val(item.id);


                $elem.on("click", function(ev) {
                    var elemID = $(this).val();
                    var name = $('#search_name_' + elemID).html();
                    if (FB_WHAT_TO_DO == 'invite') {
                        var chk = !($('#chk_' + elemID).prop('checked'));
                        $('#chk_' + elemID).prop('checked', chk);
                    }
                    else {
                        var t = $(ev.target);
                        if (t.prop("class") != "_plus")
                        {
                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                                                                          <div align="center" style="margin-top:5px;">Getting your friend\'s location...<br><br><img width="40" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" /></div>\
                                                                                                          </div>');
                            NewDialog.dialog({
                                dialogClass: 'dialog_style_glomp_wait',
                                title: '',
                                autoOpen: false,
                                resizable: false,
                                modal: true,
                                width: 200,
                                closeOnEscape: false,
                                height: 150,
                                show: '',
                                hide: ''
                            });
                            NewDialog.dialog('open');
                            FB.api(elemID+ '?fields=first_name, last_name, location',
                            function(response) {
                                var country = "";
                                var first_name = response['first_name'];
                                var last_name = response['last_name'];
                                if (response['country'] != undefined)
                                    country = response['country'];
                                if (country == "") {
                                    $.ajax({
                                        type: "POST",
                                        url: GLOMP_BASE_URL + 'ajax_post/getCountryList',
                                        success: function(res) {
                                            console.log(res);
                                            $("#waitDialog").dialog("close");
                                            $("#waitDialog").dialog('destroy').remove();
                                            var NewDialog = $('<div id="waitDialog" align="center" style="font-size:12px">\
                                                                                                                          <div align="center" style="margin-top:5px;"><b>' + name + '</b> has hidden their location in Facebook.<br><br>Please enter their location here:\
                                                                                                                          <br><br><select id="selected_country" name="selected_country" style="width:280px">' + res + '</select></div>\
                                                                                                                          </div>');
                                            NewDialog.dialog({
                                                dialogClass: 'dialog_style_glomp_wait',
                                                title: '',
                                                autoOpen: false,
                                                closeOnEscape: false,
                                                resizable: false,
                                                modal: true,
                                                width: 300,
                                                height: 200,
                                                show: '',
                                                hide: '',
                                                buttons: [
                                                    {text: "Continue",
                                                        "class": 'btn-custom-blue-grey_xs  ',
                                                        click: function() {
                                                            country = $('#selected_country').val();
                                                            $('#waitDialog').dialog("close");
                                                            $('#waitDialog').dialog('destroy').remove();
                                                            showGlompMenu(elemID, country, first_name, last_name);
                                                        }},
                                                    {text: "Cancel",
                                                        "class": 'btn-custom-white_xs ',
                                                        click: function() {
                                                            $(this).dialog("close");
                                                            setTimeout(function() {
                                                                $('#waitDialog').dialog('destroy').remove();
                                                            }, 500);
                                                        }}
                                                ]
                                            });
                                            NewDialog.dialog('open');
                                        }
                                    });
                                }
                                else {
                                    $("#waitDialog").dialog("close");
                                    $("#waitDialog").dialog('destroy').remove();
                                    showGlompMenu(elemID, country, first_name, last_name);
                                }


                            });
                        }


                    }


                });


                $('#friendsList').append($elem);

                var img = document.getElementById('img_fb_' + item.id);
                /*or however you get a handle to the IMG*/
                var width = parseInt(img.clientWidth);
                var height = parseInt(img.clientHeight);
                if ((width) > height) {
                    $('#img_fb_' + item.id).css({'height': 70});
                }
                else {
                    $('#img_fb_' + item.id).css({'width': 70});
                }
            }
            function hasOwnProperty(obj, prop) {
                var proto = obj.__proto__ || obj.constructor.prototype;
                return (prop in obj) &&
                        (!(prop in proto) || proto[prop] !== obj[prop]);
            }
            function afterCreateFbFriendsList() {
                $elem = $('<div id="search_not_found"  style="display:; clear:both;" align="center"> \
                                        No results found... \
                                        </div>');
                $('#friendsList').append($elem);
            }
            function showGlompMenu(id, country, first_name, last_name)
            {
                window.location.href = GLOMP_BASE_URL + 'm/profile/menu/?tab=merchants&from=non-friend-fb&fbID=' + id + '&first_name=' + first_name + '&last_name=' + last_name + '&location=' + country;
            }
            function showGlompMenuLi(id, country, first_name, last_name)
            {
                window.location.href = GLOMP_BASE_URL + 'm/profile/menu/?tab=merchants&from=non-friend-li&liID=' + id + '&first_name=' + first_name + '&last_name=' + last_name + '&location=' + country;
            }
            function checkIfInTour()
            {
                if (inTour == 'yes')
                {
                    var NewDialog = $('<div id="messageDialog" align="center" style="font-size:12px !important;"> \
                                                                        <div id="personalMessageTips" style="padding:2px;font-size:14px;" align="center">Add more Friends?</div> \
                                                                        <p></div>');
                    NewDialog.dialog({
                        dialogClass: 'dialog_style_glomp_wait',
                        autoOpen: false,
                        resizable: false,
                        modal: true,
                        width: 270,
                        height: 120,
                        show: 'clip',
                        hide: 'clip',
                        buttons: [
                            {text: "Add more!",
                                "class": 'btn-custom-white_xs margin_left_10px ',
                                click: function() {
                                    $('#messageDialog').dialog("close");
                                    $('#messageDialog').dialog('destroy').remove();
                                    window.location.href = GLOMP_BASE_URL + 'm/user/facebookFriends';
                                }},
                            {text: "Return to Tour",
                                "class": 'btn-custom-blue-grey_xs ',
                                click: function() {
                                    $('#messageDialog').dialog("close");
                                    $('#messageDialog').dialog('destroy').remove();
                                    window.location.href = GLOMP_BASE_URL + 'm/user/dashboard/?inTour=3';
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                }
            }
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
            $(window).resize(function() {
                $(".ui-dialog-content").dialog("option", "position", "center");
            });
            $.fn.doesExist = function() {
                return jQuery(this).length > 0;
            };

            /* Linked IN */
            function liInitOnload() {
                if (IN.User.isAuthorized()) {
                    /*NO NEED TO DO ANYTHING*/
                }
            }

            function liAuth() {
                IN.User.authorize(function() {
                    /*If authorized and logged in*/
                    var params = {
                        type: 'linked',
                        type_string: 'LinkedIN',
                        func: function(obj) {
                            return login_linkedIN(obj);
                        }
                    };

                    checkIntegratedAppConnected(params);
                });
            }
            function login_linkedIN(obj)
            {
                IN.API.Profile("me").fields("id", "email-address", "first-name", "last-name", "date-of-birth").result(function(data) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: GLOMP_DESK_SITE_URL + 'ajax_post/checkIfIntegrated',
                        data: {
                            id: data.values[0].id,
                            email: data.values[0].emailAddress,
                            field: 'user_linkedin_id',
                            mode: 'linkedIN',
                            device: 'mobile',
                            data: data.values[0]
                        },
                        success: function(res) {
                            if (res != false) {
                                if (res.post_to_url) {
                                    /* The rest of this code assumes you are not using a library.*/
                                    /* It can be made less wordy if you use one.*/
                                    var form = document.createElement("form");
                                    form.setAttribute("method", 'post');
                                    form.setAttribute("action", res.redirect_url);

                                    for (var key in res.data) {
                                        if (res.data.hasOwnProperty(key)) {
                                            var hiddenField = document.createElement("input");
                                            hiddenField.setAttribute("type", "hidden");
                                            hiddenField.setAttribute("name", key);
                                            hiddenField.setAttribute("value", res.data[key]);
                                            form.appendChild(hiddenField);
                                        }
                                    }
                                    document.body.appendChild(form);
                                    obj.dialog('close');
                                    return (form.submit());
                                }
                                obj.dialog('close');
                                window.location = res.redirect_url;
                                return (window.location);
                            }
                        }
                    });
                });
            }
            function removeByValue(arr, val) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] == val) {
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
            function setCheckSelecAllValue() {
                var all = 0
                var chk = 0;
                $('.hasCheckBox').each(function(i, item) {
                    all++;
                });
                $('.friendThumbSelected').each(function(i, item) {
                    if ($(this).prop('checked')) {
                        chk++;
                    }
                });
                if (all > 0 && all == chk) {
                    $("#chk_select_all").prop('checked', true);
                }
                else {
                    $("#chk_select_all").prop('checked', false);
                }
            }
            function setCheckSelecAllValueLi() {
                var all = 0
                var chk = 0;
                $('.hasCheckBox').each(function(i, item) {
                    all++;
                });
                $('.friendThumbSelected').each(function(i, item) {
                    if ($(this).prop('checked')) {
                        chk++;
                    }
                });
                if (all > 0 && all == chk) {
                    $("#li_chk_select_all").prop('checked', true);
                }
                else {
                    $("#li_chk_select_all").prop('checked', false);
                }
            }
            function checkIfThisHasBeenChecked(arr, val) {
                var hasChecked = false;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] == val) {
                        hasChecked = true;
                        break;
                    }
                }
                return hasChecked;
            }
            function checkFBMatch()
            {
                FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                    var fetch_fb_id = response.id;

                    $.ajax({
                        type: "POST",
                        url: GLOMP_DESK_SITE_URL + 'ajax_post/check_fb_match',
                        data: {
                            fetch_fb_id: fetch_fb_id
                        },
                        async: false,
                        dataType: 'json',
                        success: function(response) {
                            if (response.check_fb_id == 'prompt') {
                                facebookDialog.dialog("close");
                                var NewDialog = $('<div id="messageDialog" align="center" class="extra">\
                                    Seems like you haven\'t link your Facebook account.\n\
                                    Do you like to link it?</div>');

                                NewDialog.dialog({
                                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                    autoOpen: false,
                                    resizable: false,
                                    closeOnEscape: false,
                                    modal: true,
                                    width: 250,
                                    show: 'clip',
                                    hide: 'clip',
                                    buttons: [
                                        {text: "Yes",
                                            "class": 'btn-custom-blue-grey_xs_nopadding margin_bottom_7px w200px',
                                            click: function() {
                                                $(this).dialog("close");
                                                setTimeout(function() {
                                                    $('#messageDialog').dialog('destroy').remove();
                                                }, 500);
                                                autoLink();
                                            }
                                        },
                                        {text: "No",
                                            "class": 'btn-custom-blue-grey_xs_nopadding margin_bottom_7px w200px',
                                            click: function() {
                                                $(this).dialog("close");
                                                window.location = GLOMP_BASE_URL + 'm/user/searchFriends/';
                                            }
                                        }
                                    ]
                                });
                                return NewDialog.dialog('open');
                                //dialog box here
                            }

                            if (response.check_fb_id == 'true') {
                                return createFbFriendsList();
                            }
                            
                            facebookDialog.dialog("close");
                            var NewDialog = $('<div id="messageDialog" align="center" class="extra"> \
                                We\'ve noticed that you are logged into a different Facebook account than the one you have linked on glomp!. \
                                Would you like to login with the other account or to continue?</div>');
                            NewDialog.dialog({
                                dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                autoOpen: false,
                                resizable: false,
                                closeOnEscape: false,
                                modal: true,
                                width: 250,
                                show: 'clip',
                                hide: 'clip',
                                buttons: [
                                    {text: "Login using your linked Facebook account",
                                        "class": 'btn-custom-blue-grey_xs_nopadding margin_bottom_7px w200px',
                                        click: function() {
                                            FB.logout(function(response) {
                                               return checkLoginStatus();
                                            });
                                            $(this).dialog("close");
                                            setTimeout(function() {
                                                $('#messageDialog').dialog('destroy').remove();
                                            }, 500);
                                        }
                                    },
                                    {text: "Continue",
                                        "class": 'btn-custom-blue-grey_xs_nopadding margin_bottom_7px w200px',
                                        click: function() {
                                            $(this).dialog("close");
                                            setTimeout(function() {
                                                $('#messageDialog').dialog('destroy').remove();
                                            }, 500);
                                            return createFbFriendsList();
                                        }
                                    }
                                ]
                            });
                            NewDialog.dialog('open');
                            //dialog box here
                        }
                    });
                });
            }
            function autoLink() {
                FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                    var fetch_fb_id = response.id;

                    $.ajax({
                        type: "POST",
                        url: GLOMP_DESK_SITE_URL + 'ajax_post/auto_link_fb',
                        data: {
                            fetch_fb_id: fetch_fb_id
                        },
                        async: false,
                        dataType: 'json',
                        success: function(response) {
                            if (response.success == 'false') {
                                var NewDialog = $('<div id="messageDialog" align="center" class="extra"> \
                                We\'ve noticed that you are logged into a facebook account that is already linked on glomp!. \
                                Would you like to login with another account?</div>');

                                NewDialog.dialog({
                                    dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                    autoOpen: false,
                                    resizable: false,
                                    closeOnEscape: false,
                                    modal: true,
                                    width: 250,
                                    show: 'clip',
                                    hide: 'clip',
                                    buttons: [
                                        {text: "Login using different Facebook account",
                                            "class": 'btn-custom-blue-grey_xs margin_bottom_7px w200px',
                                            click: function() {
                                                FB.logout(function(response) {
                                                   return checkLoginStatus();
                                                });
                                                $(this).dialog("close");
                                                setTimeout(function() {
                                                    $('#messageDialog').dialog('destroy').remove();
                                                }, 500);
                                            }
                                        },
                                        {text: "Cancel",
                                            "class": 'btn-custom-blue-grey_xs margin_bottom_7px w200px',
                                            click: function() {
                                                $(this).dialog("close");
                                                window.location = GLOMP_BASE_URL + 'm/user/searchFriends/';
                                            }
                                        }
                                    ]
                                });

                                return NewDialog.dialog('open');
                            }

                            return checkLoginStatus();
                        }
                    });
                });
            }            
        </script>               
    </body>
</html>