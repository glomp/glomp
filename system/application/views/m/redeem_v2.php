<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url() ?>">
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">
		<link href="<?php echo base_url() ?>assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background:#DEE6EB;">
		<?php include_once("includes/analyticstracking.php") ?>
        <?php
        if($voucher['voucher_status']=='Redeemed')
        {   
        ?>
        <div id="redeem_bg" class="global_wrapper" style="background-color: #d5d8e4">
        <?php
        }
        else
        {
        ?>
        <div class="global_wrapper" style="">
        <?php
        }
        ?>
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/glomp/') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('back'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <a href="javascript:void(0);" >
                        <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>
                    </a>
                </div>
            </div>

            <!-- hidden navigations     
            <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
                <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                    <div class="hidden_nav">
<?php echo $this->lang->line('Home'); ?>
                    </div>
                </a>                
				<div class="cl hidden_nav_seperator fl w200px"></div>				
				<a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M . '/glomp/') ?>">
                    <div class="hidden_nav">
                        <?php echo $this->lang->line('back'); ?>
                    </div>
                </a>                
            </div>
            <!-- hidden navigations -->			
			<!--body-->     
            <div class="container p5px_0px global_margin " style="text-align:justify ">
            	
			
			
            <?php
            if($voucher['voucher_status']=='Redeemed')
            {   $response ="";
                ?>
                    <div class="container p5px_0px global_margin  white" style="text-align:justify ">
                        <div class="p10px_20px" align="center" style="background-color:#ff0000;border-radius:5px">
                        <?php echo $this->lang->line('voucher_alrady_reddemed_msg');?>
                        </div>
                    </div>
			<div class="cl p2px_20px">	</div>			
			<div class="container p5px_0px global_margin white" style="text-align:justify ">
				<div class="p5px_20px" align="center" style="background-color:#FFF;border-radius:5px;color:#000;font-size:16px;">
						<strong><?php echo $this->lang->line('verification_code');?></strong>
						<div><strong class="red" style="margin:0px; padding:2px; font-size:30px; color:#FF0000 !important;"><?php echo $voucher['voucher_code'];?></strong></div>
						<div class="barcode39" style="width:58%;height:50px; margin:0 auto">
							<?php echo $voucher['voucher_code'];?>
						</div>						
				</div>
				
            </div>		
            <?php 
            
            
                $voucher_id = $voucher['voucher_id'];
                $merchant = $this->db->get_where('gl_merchant',array('merchant_id'=>$voucher['voucher_merchant_id']))->row();
                $outlet = $this->db->get_where('gl_outlet',array('outlet_id'=>$voucher['voucher_outlet_id']))->row();
                
            ?>
			<div class="container p5px_0px global_margin" align="center" style="font-size:16px; font-weight:bold">   				
				<?php echo $merchant->merchant_name;?>, <?php echo $outlet->outlet_name;?>
				<div style="font-size:14px"><?php echo   $this->users_m->customFormat($voucher['voucher_redemption_date']);?></div>	
			</div>		
			<div class="" align="center" style="width:70px; height:70px; border:0px solid #333;position:absolute; left:50%; margin-left:-35px;overflow:visible">   				
				<div class="validate_success_ball" id="pulse" >
					<img src="<?php echo base_url() ?>assets/m/img/validate_success_ball_logo.png" id="pulseImg" />
					<div class="validate_success_glows"  ></div>					
				</div> 				
            </div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
			<div class="cl p5px_20px">	</div>			
            <div class="container p5px_0px global_margin white" style="text-align:justify;" align="center">
				<div class="p5px_20px" align="center" style="background-color:#585F6B;border-radius:5px;color:#A8CD39;font-size:12px;">
						<?php echo $this->lang->line('reedmed_success_message_note'); ?>						
				</div>
            </div>
            
            <div class="cl p5px_20px" align="center">
                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs " data-voucher_id="" >share</button>
                <div style="height:0px;margin-top:25px;">
                    <div id="share_wrapper_box_" class="share_wrapper_box_up" data-voucher_id="" style="" align="center">
                        <button data-voucher_id="<?php echo $voucher_id;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook_redeem btn-custom-blue_xs_fb"   ></button>
                        <button id="share_linkedin_btn_redeem" data-voucher_id="<?php echo $voucher_id;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin_redeem btn-custom-light_blue_xs_tweet"   ></button>
                        <button  data-from="redeem_m" data-voucher_id="<?php echo $voucher_id;?>" title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter_redeem btn-custom-light_blue_xs_tweet"  ></button>
                    </div>
                </div>
            </div>	
                    
                <?php
            }
            else
            {
            ?>
            
            
            <!-- Voucher Details-->	
			<?php
				$res_product = $this->product_m->productAndMerchant($voucher['voucher_product_id']);
				$row_product = $res_product->row();
				$prod_image = $this->custom_func->product_logo($row_product->prod_image);
			?>
			<div class="cl container p5px_0px">
                <div style="border:solid 12px #595F6D;border-top:solid 10px #595F6D;border-radius:5px;background-color:#fff;padding:5px 3px;">
                    <div class="row" stlye="display:table">                                            
                        <div style="display:table-row">
                        <div class="" style="display:table-cell;width:30%;padding:5px 8px;vertical-align:top">
                            <div class="menu_item_photo_wrapper">
                                <img class="redeem_prod_photo" style="width:100%;max-width:150px;margin-bottom:10px;" alt="<?php echo $row_product->prod_name;?>" src="<?php echo base_url().$prod_image;?>"></img>
                            </div>
                            <div class="redeem_merchant_text"  ><?php echo $row_product->merchant_name;?></div>
                        </div>
                        <div class="" style="width:70%;display:table-cell;border-left:solid 1px #595F6D; display:table-cell;vertical-align:top">
                            <div class="redeem_prod_text" style="padding:0px 8px 15px 8px;margin:0px;font-size:18px;text-align:left;"><b><?php echo $row_product->prod_name;?></b></div>
                            <div class="redeem_prod_text" style="padding:5px 8px;" ><?php echo $row_product->prod_details;?></div>
                        </div>
                        </div>
                    </div>
                </div>				
			</div>
			<!-- Voucher Details-->	
            
            
			<!-- Merchant  terms-->	
			<div class="cl container p5px_0px " style="background-color:#6D8BA5;border-radius:5px;padding:10px 12px 14px 12px;color:#fff">                            
                <div class="cl" id="term_button">
                    <div class="fl" style="font-size:14px;font-weight:bold" >
                        Terms & Conditions<?php //echo $this->lang->line('termNcondition');?>
                        <em style="font-size:12px;font-weight:normal">(Please read)</em>
                    </div>
                    <div class="fr"  style="background:#fff;width:24px;height:24px;border-radius:24px;cursor:pointer" align="center">
                       <span class="glyphicon glyphicon-chevron-down" style="color:#595F6D;font-size:16px;margin-top:3px;margin-left:0px"></span> 
                    </div>
                </div>
                <div class="cl" id="term_content" style="display:none;padding:10px 0px 5px 0px;font-size:12px;font-weight:normal">
                    <?php //if($row_product->merchant_terms!=0 && $row_product->merchant_terms!="") {
						echo stripslashes($row_product->merchant_terms);
						
					 ?>                     
                </div>
            </div>
            <!-- Merchant  terms-->	
            
            <!-- Glomp Notes-->				
			<div class="cl container p5px_0px bold" style="margin:5px 0px;background-color:#595F6D;border-radius:5px;padding:10px 12px 14px 12px;color:#F42F8F">
				<div style="text-align:justify ">
					<?php echo $this->lang->line('redeem_note');?>			
				</div>					
			</div>
			<!-- Glomp Notes-->	
           
			<!-- Glomp Form-->	
            <form method="post" name="redeem_form" id="redeem_form" action="<?php echo base_url(MOBILE_M).'/redeem/voucher2/'.$voucher['voucher_id']; ?>">
                <input type="hidden" name="merchant_id" value="<?php echo $voucher['voucher_merchant_id']?>" />
            <div class="cl container p5px_0px " style="background-color:#6D8BA5;border-radius:5px;padding:10px 12px 14px 12px;color:#fff">                            
                <div class="cl">
                     <div class="fl" style="width:35%;font-size:14px;font-weight:bold" >
                        Outlet&nbsp;Code
                    </div>
                    <div class="fr" style="width:65%;">
                        <input type="tel" class="redeem_input fr" style="" name="outlet_code"  id="outlet_code">
                    </div>
                </div>
            </div>
            <div class="cl p5px_0px" align="center">
                <button type="submit" id="validate" class="btn-custom-blue-grey_normal w200px" style="font-size:20px !important;"  >Validate</button>
            </div>	
            </form>		
			<!-- Glomp Form-->			
            
            <?php
            } // else voucher is already redeemed
            ?>
				<div class="cl container p5px_0px gray" style="text-align:justify ">
					<div class="" align="left">
						
							<div class="cl  p2px_0px" align="left">
								
								
							</div>
							<!--<div class="cl fl  p2px_0px" align="left">
								<span class="redeem_input_label fl" style="width:110px !important;" >glomp! Password</span>
								<input type="password" class="redeem_input fl" style="width:150px !important;" placeholder="glomp! Password" name="glomp_password" id="glomp_password">
							</div>
							<div class="cl fl  p2px_0px" align="left">
								<span class="redeem_input_label fl" style="width:120px !important;" >&nbsp;</span>
								<a href="<?php echo base_url(MOBILE_M).'/landing/forgotPassword/';?>" class="fl forgot_pass gray">Forgot Password?</a>
							</div>-->
                            <div class="cl p2px_0px" align="center"></div>	
							
										
					</div>
				</div>
			
			<!-- Glomp Form-->
			<div class="cl p20px">	</div>								
			
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  
        <div id="waitPopup" align="center" style="display:none;">
            <div align="center" style="margin-top:-5px;font-wieght:bold" class="title" value="Please wait...">Please wait...</div>
            <div align="center" style="margin-top:5px;" class="loader"><img src="<?php echo base_url() ?>assets/images/ajax-loader-1.gif" /></div>
            <div align="center" class="content"></div>
        </div>
        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery.pulse.min.js"></script>        
		<script src="<?php echo base_url() ?>assets/m/js/jquery.barcode.0.3.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        var FB_APP_ID                               ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
            var GLOMP_BASE_URL									="<?php echo base_url('');?>";
            $(document).ready(function(){
                
                    $('.barcode39') .barcode({code:'code39'});
                }
            );
			</script>
        <script>
            (function($) {
                $.fn.onEnter = function(func) {
                    this.bind('keypress', function(e) {
                        if (e.keyCode == 13) func.apply(this, [e]);    
                    });               
                    return this; 
                 };
                 
                 
                
                 
            })(jQuery);
            $(document).ready(function(e)
            { 



                <?php
                if($voucher['voucher_status']=='Redeemed')
                {
                    ?>

                /*if redeemed*/
                    var el = $('#pulse_');
				var originalWidth = el.width();
				var originalHeight = el.height();

				var properties = {
				   width :64,
				   height :64,
				   marginTop :3,
				   marginLeft :3
				};

				el.pulse(properties, {duration : 2000, pulses : -1});					
				
				var e2 = $('#pulseImg_');
				var originalWidth2 = e2.width();					

				var properties2 = {					 
				   width :28,
				   height :20,
				   marginTop :22,
				   marginLeft :2					   
				};
				e2.pulse(properties2, {duration : 2000, pulses : -1});
                
                
                 animate();
                
                function animate() {
                    $("#redeem_bg").animate({
                        backgroundColor: "#a1bcd2",
                    }, 1000 , function(){
                        animate2();
                    });
                }
                
                function animate2() {
                    $("#redeem_bg").animate({
                        backgroundColor: "#d5d8e4",
                    }, 1000 , function(){
                        animate();
                    });
                }
                
                
                <?php
                }
                ?>
                /*if redeemed*/
            
                $("#term_button").click(function() {
                    $('#term_content').toggle("slide", { direction: "up" }, 500);                
                });
                var isAndroid = navigator.userAgent.match(/android/i) ? true : false;
                var isIOS = navigator.userAgent.match(/(ipod|ipad|iphone)/i) ? true : false;

                if(isIOS)
                    $('#outlet_code').attr("type", "tel");
                if(isAndroid)
                {
                    $('#outlet_code').attr("type", "number");
                }
            });
            $(function() {
                var waitPopUp = $('#waitPopup');
                
                waitPopUp.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomped_alerts',
                        title:'',
                        modal: true,
                        position: 'center',
                        width:200,
                        height:120
                });
                
                 $("#outlet_code").onEnter( function(e) {
                    /*$(this).val("Enter key pressed"); */
                    e.preventDefault();                    
                    $("#validate").click();
                }).focus( function(e) {
                    
                });
                
                $( "#redeem_form" ).submit(function( event ) {									
					
					event.preventDefault();
                    
                    waitPopUp.dialog('open');
                    

                    jQuery.post(jQuery(this).attr('action'), jQuery(this).serialize(),function(response){
                        console.log( response.status );
                        if( response.status == 'Success' ) {
                            
                            ga_redeem();
                                
                            window.location = jQuery('base:first').attr('href') + 'm/redeem/validated2/' + response.voucher.voucher_id;
                            return;
                        }

                        waitPopUp.find('.loader').hide();
                        waitPopUp.find('.title:first').first().html(response.status);
                        waitPopUp.find('.content').html(response.message);
                        waitPopUp.dialog('option','buttons',[{
                            text: 'Close',
                            class: 'btn-custom-blue-grey_xs',
                            click: function(){
                                        waitPopUp.dialog('close');
                                        /* reset popup */
                                        waitPopUp.find('.title:first').first().html( waitPopUp.find('.title:first').attr('value') );
                                        waitPopUp.find('.loader').show();
                                        waitPopUp.find('.content').html('');
                                        waitPopUp.dialog('option','buttons',{});
                                    }
                            }]
                        );
                        
                    }, 'json');
				});
                
				$("#main_menu").click(function() {
					$("#hidden_menu").toggle();										
				});
				
				
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
			$(window).resize(function() {
				$(".ui-dialog-content").dialog("option", "position", "center");
			});
        </script>
        <script src="<?php echo minify('assets/m/js/custom.glomp_share.js', 'js', 'assets/m/js'); ?>"></script>
    </body>
    <?php //include_once("includes/node.php") ?>
</html>
