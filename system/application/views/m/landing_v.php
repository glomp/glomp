<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?></title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >
    <meta name="keywords" content="<?php echo meta_keywords();?>" >
    <meta name="author" content="<?php echo meta_author();?>" >
    <!-- Bootstrap -->
    <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">    
	<link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <style>
        .nonSelectable{
          user-select: none;
          -moz-user-select: none;  
          -webkit-user-select: none;  
          -ms-user-select: none;
        }                 
    </style>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>
  </head>  
  <body data-tpl="landing_v">
	<?php 
        $is_landing=true;
        if ($this->agent->is_mobile('iphone'))
        {
            $is_iphone =  true;
        }
        else
        {
            $is_iphone =  false;
        }


        $show_ad=true;
        // Safari (in-app)
        $isMobileSafari = (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari/') == false);
        if ($isMobileSafari) {
            //echo '<hr>Safari (in-app)';
            $show_ad =false;
        }
        // Android (in-app)
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "it.glomp")
        { 
            //echo 'Android (in-app)';
            $show_ad =false;
        }
    include_once("includes/analyticstracking.php") ?>
	<div id="fb-root"></div>	
	<div class="global_wrapper_landing" align="center" style="height:100%;overflow:auto;">	      
        <div  class="row "  style="margin:0px;padding:0px;cursor:pointer">
            <div class="<?php echo ($show_ad)? 'col-xs-6':'col-xs-12';?> border_grey" id="login_open" style="padding:0px;">
                <div class="col-xs-6 pull-left green_harabara" align="left" style="padding:8px 15px 6px 15px">
                    Login
                </div>
                <div class="col-xs-6 pull-right green_harabara" align="right" style=";padding:8px 15px 6px 15px">
                    <span class="" style="font-size:20px;">&gt;</span>
                </div>
            </div>
            <?php if($show_ad)
            {
            ?>
            <div class="col-xs-6 " style="padding:0px;">
                <a href="https://itunes.apple.com/us/app/glomp!/id823405266?mt=8" target="_blank">
                    <div class="col-xs-6 border_grey" align="center" style="padding:8px 0px 6px 0px">
                        <img src="<?php echo base_url('assets/m/img/apple-128.png')?>" style="height:28px; " />                    
                    </div>
                </a>
                <a href="https://play.google.com/store/apps/details?id=it.glomp" target="_blank">
                <div class="col-xs-6 border_grey" align="center" style="padding:8px 0px 6px 0px">                    
                    <img src="<?php echo base_url('assets/m/img/android-logo-grey128.PNG')?>" style="height:28px; " />
                </div>
                </a>
            </div>
            <?php
            }
            ?>
        </div>        
        <div id="body_wrapper">
        <div class="row " style="margin:0px;padding:0px;">
            <div class="col-xs-6" align="left" style="padding:0px;">                
                <div class="row" >
                    <div class="col-xs-12 border_grey" align="center" style="height:190px;background-image:url('<?php echo base_url()?>assets/m/img/logo.png');background-size: contain;background-position: center;background-repeat:no-repeat;	">                    
                        
                    </div>
                </div>                
                <a href="<?php echo base_url(MOBILE_M.'/landing/register_landing')?>">
                <div class="col-xs-12 border_grey harabara" align="center" style="border-bottom:2px solid #B5B6B9;color:#EE1C25;letter-spacing:1px;font-size:24px;padding:25px 0px 15px 0px;height:80px;text-decoration:none">
                    Sign Up!
                </div>      
                </a>        
            </div>
            <div class="col-xs-6 " align="left" style="padding:0px;height:270px; border-bottom:2px solid #B5B6B9;">
                <div class="row" id="what_is_glomp" style="cursor:pointer" >                    
                    <div class="col-xs-12 border_grey " align="left" style="padding:0px;color:#01E9FF;display:table;height:80px">
                        <div style="display:table-row">
                            <div class="harabara" align="left" style="padding:10px 0 10px 15px; padding-right:0px;font-size:20px;display:table-cell;vertical-align:middle;line-height:18px">
                                What&nbsp;is glomp!?
                            </div>                    
                            <div class="" align="right" style="width:45px;padding:10px 20px 10px 0px;display:table-cell;vertical-align:middle;">
                                <div style="background:#01E9FF;width:36px;height:36px;border-radius:100px;" align="center">
                                   <span class="glyphicon glyphicon-play" style="color:#505662;font-size:22px;margin-top:3px;margin-left:2px"></span> 
                                </div>
                            </div>            
                        </div>            
                    </div>                    
                </div>
                <div class="row" >
                    <div id="what_glomp_can_do" class="col-xs-12 border_grey" align="left" style="padding:0px;color:#FFA600;border-bottom:0px solid #B5B6B9;height:190px;cursor:pointer">
                        <div class="row" style="height:190px;width:100%;display:table;vertical-align:middle;padding:0px;" >
                            <div class="col-xs-12" align="left" style="padding:0px;display:table-cell;vertical-align:middle">                                
                                <div class="col-xs-12 harabara" style="padding:10px 15px 10px 15px;font-size:20px;line-height:22px">
                                    What can glomp! do&nbsp;for&nbsp;you?
                                </div>                        
                                <div class="col-xs-12" align="left" style="padding:0px;display:table;">                                
                                    <div style="display:table-row">
                                        <div class="col-xs-10 " style="display:table-cell;padding:10px 0px 10px 15px;font-size:14px; border:0px solid #fff;font-size:12px;line-height:15px">
                                            A&nbsp;Better Experience awaits...&nbsp;
                                        </div>
                                        <div class="col-xs-2" align="right" style="display:table-cell;font-size:18px;padding:10px 15px 0px 0px;border:0px solid #fff">                                            
                                            <span class="harabara" style="font-size:16px;font-weight:normal">&gt;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>            
                        
                    </div>                    
                </div>
            </div>
        </div>
        
        <div class="cl" style="clear:both;"></div>
        
        
         <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide nonSelectable" data-ride="carousel" data-interval="3000" >
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner" >
        <div class="item active" >
          <img src="<?php echo base_url()?>assets/m/img/glomp_Mobile_Landing_Img1.png" alt="Like Sam's post? Send her a cupcake to  show that you really Like it." style="width:100%">
          <div class="container">
            <div class="carousel-caption">      
              
            </div>
          </div>
        </div>        
        <div class="item">
          <img src="<?php echo base_url()?>assets/m/img/glomp_Mobile_Landing_Img2.png" alt="Thanks for your advice Jane!" style="width:100%">
          <div class="container">
            <div class="carousel-caption">              
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php echo base_url()?>assets/m/img/glomp_Mobile_Landing_Img3.png" alt="That smoothie made my day!" style="width:100%">
          <div class="container">
            <div class="carousel-caption">              
            </div>
          </div>
        </div>
        <div class="item">
          <img src="<?php echo base_url()?>assets/m/img/glomp_Mobile_Landing_Img4.png" alt="Can't join the boys to celebrate? Congratulate them with a beer." style="width:100%">
          <div class="container">
            <div class="carousel-caption">              
            </div>
          </div>
        </div>
      </div>      
    </div><!-- /.carousel -->
        
        
        
        
        
        
            
	</div>   <!-- /body_wrapper -->
	</div> <!-- /global_wrapper -->


    <!-- app_store_alert_body-->
    <?php
        

    if($show_ad)
    {
    ?>
    <div id="app_store_alert_body" style="position: absolute;top:44px;left:0px;bottom:0px;background-color:rgba(0,0,0,0.7);;width:100%;padding:60px 20px; color:#A5FF00" align="center">
            <div style="font-size: 26px" align="left">
                Want to maximize your <b>glomp!</b> experience?
            </div>
            <br>
            <p align="justify">
                We can't help but notice that your on an <?php echo ($is_iphone==true) ? 'iOS':'Android';?> device. 
                How about downloading our app from <?php echo ($is_iphone==true) ? 'App Store':'Google Play store';?> so that you can experience the best that <b>glomp!</b> has to offer?
            </p>
            <br>
            <a href="<?php echo ($is_iphone==true) ? 'https://itunes.apple.com/us/app/glomp!/id823405266?mt=8':'https://play.google.com/store/apps/details?id=it.glomp';?>" target="_blank">
                <img  style="width:80px;border-radius: 20%;border: 2px #fff solid" src="<?php echo base_url('assets/m/img/glomp_logo_121.png');?>" /></a>
            <br>
            <br>
            <p id="app_store_alert_link" align="justify" style="color:#fff; text-decoration: underline;cursor: pointer;">
            No thanks, continue to mobile site
            </p>

    </div> 
    <?php
    }
    ?>
    <!-- app_store_alert_body-->


    
    <!--login_wrapper -->
    <?php            
    $login_display='none';
    $verified = $this->session->flashdata('verified');
    if (empty($verified))
    {
        $verified=$this->input->get('landing_message');                    
    }
    if (! empty($verified))
    {
        $login_display='';
    }
    if(validation_errors()!="")
    {
        $login_display='';
    }      
    if (isset($_GET['say']) && $_GET['say']=='registered' )
    {
        $login_display='';   
    }
    if(isset($error_msg))
    {
        $login_display='';   
    }          
    $deactivated = $this->session->flashdata('deactivated');
    
    //echo "asdasd".$deactivated;
    if (! empty($deactivated)) {            
        $login_display='';      
    }    
      ?>

    <div id="login_wrapper" style="display:<?php echo $login_display;?>;position:fixed; top:0px;left:0px;height:100%;width:100%;z-index:10" >
         <div id="login_close" class="row border_grey"  style="margin:0px;padding:0px;background-color:#585F6B;cursor:pointer">
            <div class="col-xs-5 pull-left green_harabara" align="left" style="padding:8px 15px 6px 15px">
                Login
            </div>
            <div class="col-xs-5 pull-right green_harabara" align="right" style="padding:8px 15px 6px 15px;">
                <div class="fr" style="-ms-transform: rotate(90deg);-webkit-transform: rotate(90deg);transform: rotate(90deg);font-size:22px;">&gt;</div>
            </div>
        </div>        



        <div id="login_body" style="display:<?php echo $login_display;?>;background-color:rgba(0,0,0,0.7);height:100%;width:100%;color:#fff" align="center">
            
            
            <?php if( isset($_GET['ref']) && $_GET['ref'] == 'registered' ):?>
                <div id="user-registered" style="max-width:270px; padding-top:20px;">
                    <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>Please check your welcome email to activate your account. Click <a href="/m/landing/resendVerification/?email=<?php echo $_GET['email'];?>">here</a> to resend the verification.</div>
                </div>
            <?php endif;?>

            
            <?php echo form_open(MOBILE_M."/landing",'name="login_form" class="form-signin" id="frmLogin"')?>		                  
                <div class="row " style="margin:0px;padding:0px;">
                    <div class="col-xs-12"  align="center">
                <?php            
				$verified = $this->session->flashdata('verified');
                if (empty($verified)) {
                    $verified=$this->input->get('landing_message');                
                
                }
                                  if (! empty($verified)) {
                                        echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$verified."</div>";
                                  }
                                  
                
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if (isset($_GET['say']) && $_GET['say']=='registered' ) {
                                        echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>Please check your welcome email to activate your account.</div>";
										 
                                  }
				  if(isset($error_msg) && ! isset($_GET['ref']))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
                      
                  $deactivated = $this->session->flashdata('deactivated');
                    if (! empty($deactivated)) {
                        echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$deactivated."</div>";
                        $this->session->sess_destroy();                    
                    }    
				  ?>
                    </div>
                </div>
                <div class="row " style="margin:0px;padding:0px;">
                    <div class="col-xs-3" align="right" style="margin:0px;padding:6px 0px 0px 0px;color:#fff">
                        <?php echo $this->lang->line('E-mail');?>
                    </div>
                    <div class="col-xs-9">
                        <input name="user_name" type="email" class="form-control" placeholder="" >
                    </div>
                </div>
                <div class="row " style="padding:5px;">
                </div>
                <div class="row " style="margin:0px;padding:0px;">
                    <div class="col-xs-3" align="right" style="margin:0px;padding:6px 0px 0px 0px;color:#fff">
                        <?php echo $this->lang->line('Password');?>
                    </div>
                    <div class="col-xs-9">
                        <input name="user_password" id="user_password" type="password" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="row " style="margin:0px;padding:0px;">
                    <div class="col-xs-3" align="right" style="margin:0px;padding:0px;color:#fff">
                        
                    </div>
                    <div class="col-xs-9"  align="left">
                        <a href="<?php echo base_url(MOBILE_M).'/landing/forgotPassword/';?>" class="forgot_pass">Forgot Password?</a>
                    </div>
                </div>
                <div class="space20px" ></div>
                <div class="row " style="">
                    <div class="col-xs-12"  align="center">
                        <button class="btn-lg btn-custom-green btn-block  border_radius_4px" type="submit" name="user_login" id="user_login"><?php echo $this->lang->line('Login');?></button>
                    </div>
                    <div class="col-xs-12"  align="center">
                        <div class="p10px_0px_0px_0px" ></div>			
                        <button class=" btn-custom-blue_fb btn-block border_radius_4px" type="button" name="Login_Facebook" id="Login_Facebook">
                            <img style="width:30px;padding-right: 10px;" src="<?php echo base_url('assets/m/img/fb-icon.png') ?>" alt="Facebook logo" />Login with Facebook
                        </button>			
                    </div>
                    <div class="col-xs-12"  align="center">
                        <div class="p10px_0px_0px_0px" ></div>
                        
                        <?php 
                            $frm_and = '';//$this->session->userdata('from_android');
                            if (empty($frm_and)) { 
                        ?>
                        <button class=" btn-custom-blue_li btn-block border_radius_4px" type="button" name="LinkedIN" id="LinkedIN">
                            Login with LinkedIN
                        </button>
                        <?php } ?>
                    </div>                    
                </div>
              </form>
        </div>
    </div>
    <!--login_wrapper -->
    <!--what_is_glomp_wrapper -->
    <div id="what_is_glomp_wrapper" style="display:none;position:absolute; top:0px;left:0px;height:100%;width:100%;background-color:rgba(51,51,51,0.7);z-index:10;overflow:auto;" >
        <div align="right" style="padding:4px 15px 5px 15px;border-bottom:0px solid #fff">
            <div id="what_is_glomp_close" style="border:1px solid #A5FF00;width:32px;height:32px;border-radius:100px;cursor:pointer" align="center">
                <div style="color:#A5FF00;font-size:24px;margin-top:-3px;margin-left:0px;">x</div>
            </div>
        </div>
        <div align="" style="padding:24px 24px;height:100%;width:100%;font-size:16px;color:#fff">            
            <h3 class="what_is_glomp_red "><?php echo $this->lang->line('what_is_glomp');?></h3>
            <div class="row" align="left" style="text-align:justify" >			
            <p class="what_is_glomp_details">                
                Discover &#8216;real&#8217; expressions of Like-ing.
            </p>                
            </div>
            <div class="row" id="loginBoxVideo" align="center">
            <?php
                $res_video = $this->cmspages_m->selectStaticPage(1,DEFAULT_LANG_ID);
                if($res_video->num_rows()>0){
                    $rec_video = $res_video->row();
                    $video_code =  $this->custom_func->iframeResizer($rec_video->content_content,'100%','220');
                    echo html_entity_decode($video_code);
                }
            ?>
                
                <div style="display:block;width:100%;margin-top:10px;">
                    <a href="<?php echo base_url();?>m/landing/faq?#from_landing"
                       id="btn-goto-faq" class="btn-custom-green"
                       style="padding:10px 20px 20px;font-size:20px;display:block;border-radius:5px !important;box-shadow: 0 0 1px 2px rgba(0,0,0,0.1);text-shadow: 0 0 1px #000000;">
                        <?php echo $this->lang->line('frequently_asked_questions','Frequently Asked Questions')?></a>
                </div>
                
                
            </div>
        </div>
    </div>
    <!--what_is_glomp_wrapper -->
    
    <!--what_glomp_can_do_wrapper -->
    <div id="what_glomp_can_do_wrapper" style="display:none;position:absolute; top:0px;left:0px;height:800px;width:100%;background-color:rgba(51,51,51,0.7);z-index:10" >
        <div align="right" style="padding:4px 15px 5px 15px;border-bottom:0px solid #fff">
            <div id="what_glomp_can_do_close" style="border:1px solid #A5FF00;width:32px;height:32px;border-radius:100px;cursor:pointer" align="center">
                <div style="color:#A5FF00;font-size:24px;margin-top:-3px;margin-left:0px;">x</div>
            </div>
        </div>
        <div align="" style="font-size:16px;color:#A5FF00;padding:24px 24px;height:120%;width:100%;background-image:url('<?php echo base_url()?>assets/m/img/glomp_Mobile_Landing_WhatGlomp_BG.png');background-size: cover;background-position: top;background-repeat:no-repeat;">            
            <h1 style="padding:0px 0px 20px 0px;margin:0px;font-size:28px;">What can glomp! do for you?</h1>
            <p style="text-align:justify">Simply, <b>glomp!</b> takes you to the next level of digital connectivity.</p>
            <p style="text-align:justify">We all send nice gestures to friends by messaging, sharing content and 'Like'-ing posts. Now with <b>glomp!</b> you can do that little bit more by treating friends to a real product that they'll enjoy.</p>
            <p style="text-align:justify">And to let friends treat you something nice too...</p>
            <p style="text-align:justify">So go ahead and start <b>glomp!</b>ing now!</p>
        </div>
    </div>
    <!--what_glomp_can_do_wrapper -->
    <div class="fb_error hidden" >
        <div align="center" style="text-align:center;margin-top:15px;">
            Network Interrupted. You need to refresh the page.<br><br><button class= 'btn-custom-white_xs' > Refresh Page</button>
        </div>
    </div>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/m/js/bootstrap.min.js"></script>	
    <script src="<?php echo minify('assets/m/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/m/js'); ?>"></script>
    <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
     <script type="text/javascript">   
        function swipedetect(el, callback){
 
 var touchsurface = el,
 swipedir,
 startX,
 startY,
 distX,
 distY,
 threshold = 50, /*required min distance traveled to be considered swipe*/
 restraint = 10, /* maximum distance allowed at the same time in perpendicular direction*/
 allowedTime = 300, /* maximum time allowed to travel that distance*/
 elapsedTime,
 startTime,
 handleswipe = callback || function(swipedir){}
 
 touchsurface.addEventListener('touchstart', function(e){
  var touchobj = e.changedTouches[0]
  swipedir = 'none'
  dist = 0
  startX = touchobj.pageX
  startY = touchobj.pageY
  startTime = new Date().getTime() /* record time when finger first makes contact with surface*/
  e.preventDefault()
 
 }, false)
 
 touchsurface.addEventListener('touchmove', function(e){
  e.preventDefault() /* prevent scrolling when inside DIV*/
 }, false)
 
 touchsurface.addEventListener('touchend', function(e){
  var touchobj = e.changedTouches[0]
  distX = touchobj.pageX - startX /* get horizontal dist traveled by finger while in contact with surface*/
  distY = touchobj.pageY - startY /* get vertical dist traveled by finger while in contact with surface*/
  elapsedTime = new Date().getTime() - startTime /* get time elapsed*/
  if (elapsedTime <= allowedTime){ /* first condition for awipe met*/
   if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ /* 2nd condition for horizontal swipe met*/
    swipedir = (distX < 0)? 'left' : 'right' /* if dist traveled is negative, it indicates left swipe*/
   }
   else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ /* 2nd condition for vertical swipe met*/
    swipedir = (distY < 0)? 'up' : 'down' /* if dist traveled is negative, it indicates up swipe*/
   }
  }
  handleswipe(swipedir)
  e.preventDefault()
 }, false)
}
 
/*USAGE:*/

var el = document.getElementById('myCarousel')
swipedetect(el, function(swipedir){
/* swipedir contains either "none", "left", "right", "top", or "down"*/
 if (swipedir =='left')
 {
    $("#myCarousel").carousel('pause');
    $("#myCarousel").carousel('next'); 
    $("#myCarousel").carousel({
        wrap:false
    }); 
    
 }
 else if (swipedir =='right')
 {
    $("#myCarousel").carousel('pause');
    $("#myCarousel").carousel('prev'); 
    $("#myCarousel").carousel({
        wrap:false
    }); 
    
    
 } 
   
});

        (function($) {
            $.fn.onEnter = function(func) {
                this.bind('keypress', function(e) {
                    if (e.keyCode == 13) func.apply(this, [e]);    
                });               
                return this; 
             };
        })(jQuery);
        $(document).ready(function() {
            $("#user_login").click(function(){
                //alert($('#device_token').val());
            });
                    
            $("#user_password").onEnter( function(e) {                
                e.preventDefault();                    
                $("#user_login").click();
            });
            $("#app_store_alert_link").click(function(){                     
                //$('#app_store_alert_body').hide("slide", { direction: "up" }, 500);                
                $('#app_store_alert_body').hide();                
            });

            $("#login_open").click(function(){                     
                $('#login_wrapper').show();
                $('#login_body').show("slide", { direction: "up" }, 500);                
            });
            $("#login_close").click(function() {                                     
                $('#login_body').hide("slide", { direction: "up" }, 500, function(){
                    $('#login_wrapper').hide();
                });                
            });
            
            
            $("#what_is_glomp").click(function() {                     
                $('#what_is_glomp_wrapper').show("slide", { direction: "right" }, 500, function(){
                    $("#body_wrapper").hide();
                });
                
            });
            $("#what_is_glomp_close").click(function() {  
                $("#body_wrapper").show('fast',function(){                    
                });
                $('#what_is_glomp_wrapper').hide("slide", { direction: "right" }, 500, function(){
                    
                });                
            });
            
            $("#what_glomp_can_do").click(function() {                     
                $('#what_glomp_can_do_wrapper').show("slide", { direction: "right" }, 500, function(){
                    $("#body_wrapper").hide();
                });
                
            });
            $("#what_glomp_can_do_close").click(function() {  
                $("#body_wrapper").show('fast',function(){                    
                });
                $('#what_glomp_can_do_wrapper').hide("slide", { direction: "right" }, 500, function(){
                    
                });                
            });
        });
     
		<?php
		if($is_voucher_okay)
			{
				echo '$(document).ready(function() {
					voucherPopup("'.$voucher_from.'");
				});';
			}
		?>
                var fb_scope = 'publish_actions, email, user_birthday';
		var FB_APP_ID="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";
		var SIGNUP_POPUP_TEXT_VOUCHER	="<?php echo $this->lang->line('signup_popup_text_voucher');?>";
		var WHAT_IS_GLOMP							="<?php echo $this->lang->line('what_is_glomp');?>";
		var WHAT_IS_GLOMP_DETAILS			="<?php echo $this->lang->line('what_is_glomp_details');?>";
		var WHAT_IS_GLOMP_LEARN_MORE	="<?php echo $this->lang->line('learn_more');?>";
		var GLOMP_BASE_URL							="<?php echo base_url('');?>";
		var GLOMP_DESK_SITE_URL							="<?php echo site_url();?>";
		var firstRun=true;
        var logs = [];
		window.fbAsyncInit = function() {
		  FB.init({
			appId   : FB_APP_ID,/*/    channelUrl : 'WWW.YOUR_DOMAIN.COM/channel.html',  Channel File*/
			status     : true, /* check login status*/
			cookie     : true, /* enable cookies to allow the server to access the session*/
			xfbml      : true  /* parse XFBML	  		*/
		});		
		 
                FB.getLoginStatus(function (response) {
                       if(response.status=="unknown"){
                           firstRun=false;
                       }
                       
                       if(response.status=="connected"){
                        <?php if(isset($_GET['js_func_fb'])) { ?>
                                    facebookDialog.dialog({
                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',		
                                        title:'',
                                        autoOpen: false,
                                        resizable: false,
                                        closeOnEscape: false ,
                                        modal: true,
                                        width:280,
                                        height:120,				
                                        show: '',
                                        hide: ''                
                                    });
                                    facebookDialog.dialog("open");
                                    <?php echo urldecode($_GET['js_func_fb']).';'; ?>
                        <?php } ?>
                       }
                  });
		};
		/*/ Load the SDK Asynchronously*/
	  (function(d){
		 var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		 js = d.createElement('script'); js.id = id; js.async = true;
		 js.src = "//connect.facebook.net/en_US/all.js";
		 d.getElementsByTagName('head')[0].appendChild(js);
	   }(document));
		
		var facebookDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
                    Connecting to Facebook...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
                </div></div>');
    
    
                var Prompt = jQuery('<div id ="errors" align="left" ><div class="alert alert-error" id ="errors_inner" style="font-size:11px !important; margin: 0px !important;" ></div></div>');
                Prompt.dialog({
                    autoOpen: false,
                    closeOnEscape: false ,
                    resizable: false,					
                    dialogClass:'dialog_style_glomp_wait noTitleStuff ',
                    modal: true,
                    title:'Please wait...',
                    position: 'center',
                    width:280,                                
                    buttons:[
                        {text: "Close",
                        "class": 'btn-custom-white_xs',
                        click: function() {
                            $(this).dialog("close");
                            setTimeout(function() {

                            }, 500);
                        }}
                    ]
                });
        
		$(function() {
            <?php if (isset($_GET['js_func_li'])) { ?>
            liAuth();
            <?php } ?>



            $('#LinkedIN').click(function(e) {
               e.preventDefault();
               return liAuth();
            });
			$( "#Login_Facebook" ).click(function( event ) {
				try
				{
                    facebookDialog.dialog({
                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',		
                        title:'',
                        autoOpen: false,
                        resizable: false,
                        closeOnEscape: false ,
                        modal: true,
                        width:280,
                        height:120,				
                        show: '',
                        hide: ''                
                    });
                    facebookDialog.dialog("open"); 

    
                    setTimeout( function ()
                    {    
                        //TODO: Make this standard error handler         
                        if (typeof(FB) == 'undefined' ) {
                            $(facebookDialog).html($('.fb_error').html())
                            refresh_btn = $(facebookDialog).find('button')[0]
                            $(refresh_btn).click(function(){
                                return window.location.reload(true);
                            });
                        }

                        FB.getLoginStatus(function(response){
                            if(response && response.status == 'connected') {
                                getUserDetailsAndDoPost();
                            } else {
                                var return_url = GLOMP_BASE_URL +  'm/landing?js_func_fb=getUserDetailsAndDoPost()';
                                return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=" + fb_scope);
                                
                              /* Display the login button          */
                            } 
                        }
                        );
                    },100);
				}
				catch(e){
					var log_details = {
                        type: 'fb_api',
                        event: 'FB login button click',
                        log_message: 'Cannot connect to facebook',
                        url: window.location.href,
                        response: console.log(e)
                    };
                    var logs = [];
                    logs.push(JSON.stringify(log_details));
                    /*save_system_log(logs);*/
				}
				finally{
					/*alert("asd");*/
				}
			});
		});
                
		function getUserDetailsAndDoPost(){
                   
                    var log_details = {
                        type: 'fb_api',
                        event: 'FB login button click',
                        log_message: 'Grabbing facebook logged user information',
                        url: window.location.href,
                        response: ''
                    };
                    var logs = [];
                    logs.push(JSON.stringify(log_details));
                    /*save_system_log(logs);*/
                    
                    doFacebookDelay('facebookLoadingDialog', 'friendsList_loading',1000, 'popup' ,'facebookConnectDialog');

                    FB.api('/me?fields=hometown,email,birthday,first_name,last_name,middle_name,gender', function(response) {
                        /*re-permit;*/
                        FB.api('/me/permissions', function(r) {
                            var email_perm = false;
                            for( var key in r.data ) {
                                if (r.data[key].permission === 'email') {
                                    email_perm = true;
                                }
                            }

                            if( ! email_perm) {
                                var return_url = GLOMP_BASE_URL +  'm/landing?js_func_fb=getUserDetailsAndDoPost()';
                                return window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id="+FB_APP_ID+"&redirect_uri="+return_url+"&response_type=token&scope=" + fb_scope);
                            }

                            facebookDialog.dialog("close");
                            clearFacebookDelay();
                            var log_details = {
                                type: 'fb_api',
                                event: 'FB login button click',
                                log_message: 'Processing glomp! through facebook',
                                url: window.location.href,
                                response: JSON.stringify(response)
                            };
                            /*console.log(JSON.stringify(log_details));*/
                            var logs = [];
                            logs.push(JSON.stringify(log_details));
                            /*save_system_log(logs);*/
                            post_to_url((GLOMP_BASE_URL + 'm/landing'),response,'post')
                            /*console.log('Good to see you, ' + response.id + '::'+response.email );*/
                       });
                    });
		}
                
                function checkRecords() {
                    FB.api('/me', function(response){
                        var fb_data = response;
                        FB.api('/me/picture?width=140&height=140', function(r){
                            fb_data.profile_pic = r.data.url;
                            $.ajax({
                                type: "POST",
                                url: GLOMP_BASE_URL + 'ajax_post/checkIfUserHasAnExistingAcct/?id=' + fb_data.id,
                                data: 'fbID=' + fb_data.id,
                                dataType: 'json'
                            }).done(function(response){
                                if(response.data == 1 && response.data_status == 'Active') {
                                    getUserDetailsAndDoPost();
                                } else {
                                    facebookDialog.dialog('close');
                                    fb_data.registration_type = 'FB';
                                    var preferred_email = jQuery('<div id="preferred_dialog">\n\
                                        <div align="center" style="">\n\
                                            <label style="font-weight:normal">Preferred email address: </label><br />\
                                            <input style="width: 100%" id="preferred_email" type = "text" value = "'+ fb_data.email +'" />\n\
                                        </div>\n\
                                    </div>');

                                    preferred_email.dialog({
                                        dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                                        resizable: false,
                                        autoOpen: true,
                                        modal: true,
                                        open: function( event, ui ) {
                                            $(event.target).next().css('margin-top', '-20px');
                                        },
                                        buttons: [
                                            {
                                                text: "Submit",
                                                "class": 'btn-custom-white_xs',
                                                click: function() {
                                                    fb_data.email = $('#preferred_email').val(),
                                                    $.ajax({
                                                        type: "POST",
                                                        dataType: 'json',
                                                        url: GLOMP_BASE_URL + 'm/user/register',
                                                        data: fb_data,
                                                        success: function(r){
                                                            if (r.error != '') {
                                                                $.each(r.error_list, function(key,val) {
                                                                    $('#'+key).addClass('error_border');
                                                                });

                                                                Prompt.find('div').html(r.error);
                                                                Prompt.dialog('open');
                                                                return;
                                                            } else {
                                                                if (r.status == 'login') {
                                                                    return getUserDetailsAndDoPost(r);
                                                                }

                                                                facebookDialog.dialog('close');
                                                                return window.location = r.location;
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        ]
                                    });
                                    /*end dialog */
                                }
                            });
                        });
                    });
                }
        var timerFacebookDelay=null;
        function doFacebookDelay(targetPopup, targetElement, timeout, outputType, closeThisFirst)
        {
            
            var message='We are experiencing a loading delay from Facebook.<br>Please wait a moment.';
            message='<span style="font-size:14px">'+message+'</span>&nbsp;<img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />';
            timerFacebookDelay = setTimeout( function ()
            {
                if(closeThisFirst!='')
                {
                    $("#"+closeThisFirst).dialog('close');
                }
                if(outputType=='popup')
                {            
                    
                    if( $('#'+targetPopup).doesExist() )
                    {/*target popupExist*/
                        $('#'+targetElement).html(message);                    
                    }
                    else
                    {/*create a new popup                    */
                        
                        var NewDialog = $(' <div id="'+targetPopup+'" align="center"><div align="center" style="margin-top:15px;" id="'+targetElement+'">'+message+'</div></div>');
                        NewDialog.dialog({						
                            autoOpen: false,
                            closeOnEscape: false ,
                            resizable: false,					
                            dialogClass:'dialog_style_glomp_wait',
                            title:'',
                            modal: true,
                            position: 'center center',
                            width:280,
                            height:130
                        });
                        NewDialog.dialog('open');
                    }
                }
                else
                {   
                    $('#'+targetElement).html(message);
                }
                
                
            }, timeout);
        }
        
        function clearFacebookDelay()
        {
            clearTimeout(timerFacebookDelay);
            if( $('#facebookLoadingDialog').doesExist() )
            {
                $("#facebookLoadingDialog").dialog('close');
                $("#facebookLoadingDialog").dialog('destroy').remove();
            }        
        }
		function post_to_url(path, params, method) {
			method = method || "post"; /* Set method to post by default if not specified.
			 The rest of this code assumes you are not using a library.
			 It can be made less wordy if you use one.*/
			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);		

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);
					form.appendChild(hiddenField);
				}
			}
			
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "fb_login");
			hiddenField.setAttribute("value", "fb_login");
			form.appendChild(hiddenField);
			document.body.appendChild(form);
			form.submit();
		}
		function voucherPopup(from){
            var thru_sms = <?php echo $thru_sms; ?>;
            var puid = '<?php echo $puid; ?>';
            var recipient_email = '<?php echo $recipient_email; ?>';
            var already_member = <?php echo $already_member; ?>;

            if (already_member) {
                var NewDialog = $('<div id="voucherPopupDialog" align="center" style="text-align:left;line-height:120%;padding:15px;font-size:16px">You\'ve been glomp!ed. In order to redeem please login using this email address: <br><br><div align="right"><span style="text-decoration:underline" >' + recipient_email + '</span></div>\
                \</div>');                          
                NewDialog.dialog({
                    dialogClass:'dialog_style_glomp_wait noTitleStuff',
                    title:'',
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width:280,
                    closeOnEscape: false ,
                    height:180,
                    show: '',
                    hide: '',
                    buttons: [  
                        {
                            text: "Ok",
                            "class": 'btn-custom-ash_xs w100px',
                            click: function() {
                                $( this ).dialog( "close" );
                            }
                        }           
                    ]
                });

                NewDialog.dialog("open");

                return;
            }

            var registerSms = function() {
                return window.location = GLOMP_BASE_URL + 'm/user/register/' + puid + '/SMS';
            }

				SIGNUP_POPUP_TEXT_VOUCHER=SIGNUP_POPUP_TEXT_VOUCHER.replace("[friend_name]","<b>"+from+"</b>");
				var NewDialog = $('<div id="voucherPopupDialog" align="center" style="text-align:left;line-height:120%;padding:15px;font-size:16px">'+SIGNUP_POPUP_TEXT_VOUCHER+'<br><br><div align="right"><a href="javascript:void(0)" style="text-decoration:underline" id="what_is_glomp">What is glomp!?</a></div>\
			\</div>');							
			NewDialog.dialog({
				dialogClass:'dialog_style_glomp_wait noTitleStuff',
				title:'',
				autoOpen: false,
				resizable: false,
				modal: true,
				width:280,
				closeOnEscape: false ,
				height:180,
				show: '',
				hide: '',
				buttons: [	
					{
						text: "Register",
						"class": 'btn-custom-ash_xs w100px',
						click: function() {
                            if (thru_sms) return registerSms();
							$( this ).dialog( "close" );
							$( "#Login_Facebook" ).click();
						}
					}			
				]
			});
			NewDialog.dialog("open");
			
			$('#what_is_glomp').click(function(){				
				var NewDialog = $('<div id="what_is_glompDialog" align="center" style="text-align:left;line-height:120%;padding:15px;">\
					<div class="" align="right" style="width:100%" >\
						<span style="margin-bottom:2px" class="what_is_glompDialog_close fr" id="what_is_glompDialog_close"></span>\
					</div>\
					<div id="video_wrapper_2" style=";padding:0px;height:326px;margin:10px 0px 0px 0px;border-radius:0px 5px 0px 5px">\
						<div style="border:0px solid #fff;color:#fff;padding:0px 25px;font-wieght:normal;line-height:130%;;font-size:14px" class="glompFont cl" align="center">\
							<h3 style="color:#ff0000" class="glompFont">'+WHAT_IS_GLOMP+'</h3>\
							<p style="text-align:justify;">'+WHAT_IS_GLOMP_DETAILS+'</p>\
						</div>\
						<a href="http://www.youtube.com/embed/u97L6GuBbAE?wmode=opaque" target="_blank" style="color:#B7D436">\
							<div class="cl" style="margin-top:10px;" align="center">\
								<img src="'+GLOMP_BASE_URL+'assets/frontend/img/glomp_play.png" width="70" style="cursor:pointer" id="" />\
								<div style="color:#B7D436;font-size:18px;font-weight:bold;margin:10px;">'+WHAT_IS_GLOMP_LEARN_MORE+'</div>\
							</div>\
						</a>\
					</div>\
				</div>');							
				NewDialog.dialog({
					dialogClass: 'dialog_style_glomped_alerts noTitleStuff',		
					title:'What is glomp!?',
					autoOpen: false,
					resizable: false,
					modal: true,
					width:280,
					closeOnEscape: false ,
					height:330,				
					show: '',
					hide: ''			
				});
				NewDialog.dialog("open");
				$('#what_is_glompDialog_close').click(function(){
					$( '#what_is_glompDialog' ).dialog( "close" );						
					$('#what_is_glompDialog').dialog('destroy').remove();					
				});
				$( "#video_play_2" ).click(function( event ){
					$('#video_wrapper_2').hide();			
				});
			
			});	
		}
		$(window).resize(function() {
			$(".ui-dialog-content").dialog("option", "position", "center");
		});
         
    
        /* Linked IN */
        function liAuth() {
            /*IN.User.authorize(function(){*/

              var params = {
                  type: 'linked',
                  type_string: 'LinkedIN',
                  func: function(obj) {
                      return login_linkedIN(obj);
                  }
              };

              var log_details = {
                  type: 'linkedin_api',
                  event: 'LinkedIn login button click',
                  log_message: 'Grabbing LinkedIn logged user information',
                  url: window.location.href,
                  response: ''
              };

              var logs = [];
              logs.push(JSON.stringify(log_details));
              /*save_system_log(logs);*/

              checkIntegratedAppConnected(params);
           /*})*/;
        }
        function linkedin_api(obj) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                async: false,
                url: GLOMP_DESK_SITE_URL + 'ajax_post/linkedIn_callback',
                data: {
                    redirect_li: GLOMP_BASE_URL + 'm/landing',
                    js_func_li: obj.js_func_li,
                    api: obj.api
                },
                success: function(_data) {
                    if (_data.values[0] == 'error') {
                        var log_details = {
                            type: 'linkedin_api',
                            event: 'LinkedIn login button click',
                            log_message: 'An error occur, linkedIn server is down. Retry Later.',
                            url: window.location.href,
                            response: JSON.stringify(_data)
                        };

                        var logs = [];
                        logs.push(JSON.stringify(log_details));
                        /*save_system_log(logs);*/

                         var NewDialog = $('<div id="messageDialog" align="center"> \
                            <p style="padding:15px 0px;">An error occur, linkedIn server is down. Retry Later.</p> \
                        </div>');

                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
                            title: '',
                            modal: true,
                            show: 'clip',
                            hide: 'clip',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-blue-grey_xs w80px',
                                    click: function() {
                                        $(this).dialog("close");
                                        $(this).dialog("destroy");
                                        obj.dialog.dialog('close'); /*close loading popup*/
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                        return false;
                    }
                    if (_data.redirect_window != '')  {
                        return window.location = _data.redirect_window;
                    } else {
                      obj.func(_data);
                    }
                }
            });
        }
        function login_linkedIN(obj){
          params = {
              js_func_li: 'liAuth()',
              api: '/v1/people/~:(id,email-address,first-name,last-name,date-of-birth,picture-urls::(original),location:(country:(code)))',
              dialog: obj,
              func: function(){}
          };
          /*IN.API.Profile("me").fields("id","email-address", "first-name", "last-name", "date-of-birth").result(function(data){*/
          params.func = function(data) {
                  $.ajax({
                  type: "POST",
                  dataType: 'json',
                  url: GLOMP_DESK_SITE_URL + 'ajax_post/processIntegratedApp',
                  data: {
                      id: data.values[0].id,
                      email: data.values[0].emailAddress,
                      field: 'user_linkedin_id',
                      mode: 'linkedIN',
                      device: 'mobile',
                      data: data.values[0]
                  },
                  success: function(res) {
                      if (res != false) {
                          if (res.status == 'register') {
                                return window.location = res.redirect_url;
                                /*end dialog */
                            } else {
                                    if (res.post_to_url) {
                                        var form = document.createElement("form");
                                        form.setAttribute("method", 'post');
                                        form.setAttribute("action", res.redirect_url);		

                                          for (var key in res.data) {
                                              if (res.data.hasOwnProperty(key)) {
                                                  var hiddenField = document.createElement("input");
                                                  hiddenField.setAttribute("type", "hidden");
                                                  hiddenField.setAttribute("name", key);
                                                  hiddenField.setAttribute("value", res.data[key]);
                                                  form.appendChild(hiddenField);
                                              }
                                          }

                                            document.body.appendChild(form);
                                            obj.dialog('close');
                                            return form.submit();
                                    }
                                    obj.dialog('close');

                                    return window.location = res.redirect_url;
                            }
                        }
                  }
              });
            };
              
            linkedin_api(params);
        }

        /* Usable functions */
        function checkIntegratedAppConnected(params) {
            var connectDialog = $('<div id="facebookConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
                Connecting to '+ params.type_string +'...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
          </div></div>');


            connectDialog.dialog({
              dialogClass: 'dialog_style_glomp_wait noTitleStuff',		
              title:'',
              autoOpen: false,
              resizable: false,
              closeOnEscape: false ,
              modal: true,
              width:280,
              height:120,				
              show: '',
              hide: ''
            });

            connectDialog.dialog("open");
            params.func(connectDialog);
        }

      function processIntegratedApp(params) {
          /*var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;line-height:120%;padding-top:25px;">\
                  Connecting to ' + params.type_string + '...&nbsp;<img style="width:30px" src="' + GLOMP_BASE_URL + 'assets/m/img/ajax-loader.gif" />\
          </div></div>');*/
          var connectDialog = $('<div id="ConnectDialog"><div align="center" style="text-align:center;margin-top:15px;">\
          Connecting to connectDialog...<br><br><img style="width:30px" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" />\
      </div></div>');


          connectDialog.dialog({
              dialogClass: 'dialog_style_glomp_wait noTitleStuff',
              title: '',
              autoOpen: false,
              resizable: false,
              closeOnEscape: false,
              modal: true,
              width: 280,
              height: 120,
              show: '',
              hide: ''
          });

          connectDialog.dialog("open");
          params.func(connectDialog);
      }

      function checkIfIntegrated(param) {
          var ret = true;
          $.ajax({
              type: "POST",
              dataType: 'json',
              async: false,
              url: GLOMP_BASE_URL+'ajax_post/checkIfIntegrated',
              data: {
                  id: param.id,
                  email: param.email,
                  field: param.field,
              },
              success: function(res) {
                  if (res) {
                      ret = true;
                  } else {
                      ret = false;
                  }
              }
          });

          return ret;
        }
          
          
        $.fn.doesExist = function(){
            return jQuery(this).length > 0;
        };	   
        </script>
    <script type="text/javascript">

        jQuery(document).ready(function(){
            
            if(jQuery('#user-registered').length > 0) {
                jQuery('#login_open').trigger('click');
            }
        });
        
    </script>
    </body>
</html>