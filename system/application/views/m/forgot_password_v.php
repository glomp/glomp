<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title;?></title>
    <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="format-detection" content="telephone=no">
    <!-- Bootstrap -->
    <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">   				
    <script src="<?php echo base_url()?>assets/m/js/jquery-2.0.3.min.js"></script>	
  </head>
  <body style="background:#DEE6EB;">
	<?php include_once("includes/analyticstracking.php") ?>
	<div class="global_wrapper" align="center">		      		
        <div class="navbar navbar-default ">
            <div class="header_navigation_wrapper fl" align="center">				                
				<div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                    <nav>
                        <a href="#" id="menu-icon-nav"></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                    <div class="w100per fl white">
                                    <?php echo $this->lang->line('Home'); ?>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="glomp_header_logo_2" style="height:25px;background-image:url('<?php echo base_url()?>assets/m/img/logo.png');"></div>
            </div>
        </div>		
         <!-- hidden navigations      
        <div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
            <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                <div class="hidden_nav" align="left">
                    <?php echo $this->lang->line('Home'); ?>
                </div>
            </a>
        </div>
        <!-- hidden navigations -->				
		<!--body -->					 
		<div class="container p10px_0px global_margin" align="left" >
                    <div class="row red harabarabold" style="font-size: 20px;"><?php echo $this->lang->line('reset_password');?></div>
                
			
			<?php if(isset($error_msg))
			{  ?>
				<div class="alert alert-error">
					 <?php echo $error_msg;?>
				</div>
			<?php }
			if(validation_errors()!="")
			{?>
				<div class="alert alert-error">
					<?php echo validation_errors();?>
				</div>
			<?php			
			}
			if(isset($success_msg))
			{ ?>
			<div class="alert alert-success">
				<?php echo $success_msg;?>
			</div>
			<?php }?>
			<form method="post" action="" >
				<div class="">
					<input name="user_email" type="email" class="form-control" placeholder="<?php echo $this->lang->line('E-mail');?>" autofocus />
				</div>			
				<div class="cl p2px_0px" align="center"></div>
				<div class="cl p2px_0px" align="center"></div>
				<div class="cl p2px_0px" align="center"></div>
				<div class="cl p2px_0px" align="center">	
					<button class="fl btn-custom-green_normal" type="submit" name="reset_password"><?php echo $this->lang->line('reset_password');?></button>
					<a href="<?php echo base_url(MOBILE_M)?>" class="fr  btn-custom-blue-grey_normal white" type="button"><?php echo $this->lang->line('Cancel');?></a>					
				</div>
			</form>							
		</div>		
		
		<div class="cl p20px">	</div>
		<!--body -->	
		<div class="footer_wrapper"> <!-- /footer --></div> <!-- /footer -->
	</div> <!-- /global_wrapper -->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url()?>assets/m/js/bootstrap.min.js"></script>	
    <script>
       $(function() {	
	   $("#main_menu").click(function() {
				$("#hidden_menu").toggle();										
			});
		
		});
		$(document).mouseup(function(e)
		{
			var container = $(".hidden_menu_class");
			if (!container.is(e.target) /* if the target of the click isn't the container...*/
					&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
			{
				$('#hidden_menu').hide();
			}
		});
	</script>   
  </body>
</html>
