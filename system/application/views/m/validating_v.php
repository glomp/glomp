<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->		
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background:#DEE6EB;">
		<?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default ">
                <div class="header_navigation_wrapper fl" align="center">        				                    
					<div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <div class="glomp_header_logo_2" style="height:24px;background-image:url('<?php echo base_url() ?>assets/m/img/glomped_logo.png');"></div>                    
                </div>
            </div>
			<!-- hidden navigations 
			<div class="cl fl hidden_menu hidden_menu_class" id="hidden_menu" >		
				<a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
					<div class="hidden_nav">
						<?php echo $this->lang->line('Home'); ?>
					</div>
				</a>
			</div>
			<!-- hidden navigations -->
            
			<!--body-->  
            <div class="container p5px_20px" align="center">   
	            <div class="glomp_validating_logo" style="background-image:url('<?php echo base_url() ?>assets/m/img/validating.png');" ></div>
                <div class="validating_text">Validating...</div>
            </div>			
			<div class="hidden" align="center">   
			<form method="post" action="<?php echo base_url(MOBILE_M).'/redeem/validated/'.$voucher_id?>" name="validate_form" id="validate_form">
				<input type="text" name="outlet_code" value="<?php echo $outlet_code;?>" />
				<!--<input type="password" name="glomp_password" value="<?php echo $user_password;?>" />-->
			</form>
			</div>								
			
			<div class="cl p20px">	</div>								
			<!--body-->
             
			<div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(e) {
				setTimeout(function(){ jQuery("#validate_form").submit(); }, 1500);			
			});
			 $(function() {	
			   $("#main_menu").click(function() {
						$("#hidden_menu").toggle();										
					});
				
				});
			$(document).mouseup(function(e)
			{
				var container = $(".hidden_menu_class");
				if (!container.is(e.target) /* if the target of the click isn't the container...*/
						&& container.has(e.target).length === 0) /* ... nor a descendant of the container*/
				{
					$('#hidden_menu').hide();
				}
			});
        </script>
    </body>
</html>
