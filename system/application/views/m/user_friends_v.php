<!DOCTYPE html>
<html>
    <head>
        <title>Glomp Mobile</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="format-detection" content="telephone=no">
        <!-- Bootstrap -->
        <link href="<?php echo base_url() ?>assets/m/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url() ?>assets/m/css/style.css" rel="stylesheet" media="screen">    
        <link href="<?php echo base_url() ?>assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css" rel="stylesheet" media="screen">    
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
    </head>
    <body style="background: white;">
        <?php include_once("includes/analyticstracking.php") ?>
        <div class="global_wrapper" style="">
            <div class="navbar navbar-default" style="position:fixed;width:100%;top:-50px;left:0px;"></div>
            <div class="navbar navbar-default navbar_relative" style="position:relative;width:100%;top:0px;left:0px;">
                <div class="header_navigation_wrapper fl" align="center">        				
                    <div class="header_icons_thumb_wrapper_3 hidden_menu_class fl" id="main_menu_old">                    
                        <nav>
                            <a href="#" id="menu-icon-nav"></a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M) ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Home'); ?>
                                        </div>
                                    </a>
                                </li>                                    
                                <li>
                                    <a href="<?php echo base_url(MOBILE_M. '/user/searchFriends?edit=1') ?>" class="white ">
                                        <div class="w100per fl white">
                                        <?php echo $this->lang->line('Edit_Friends'); ?>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <a href="<?php echo site_url(MOBILE_M . '/user/searchFriends'); ?>" >
                        <div class="glomp_header_logo_2" style="height: 25px;background-image:url('<?php echo base_url() ?>assets/m/img/glomp-friends-logo.png');"></div>
                    </a>
                </div>
                
                <!-- hidden navigations        
                <div class="cl fl  hidden_menu hidden_menu_class" id="hidden_menu" >		
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo base_url(MOBILE_M) ?>">
                        <div class="hidden_nav">
                            <?php echo $this->lang->line('Home'); ?>
                        </div>
                    </a>
                    <div class="cl hidden_nav_seperator fl w200px"></div>
                    <a class=" cl hidden_nav_link fl w200px" href="<?php echo site_url(MOBILE_M . '/user/searchFriends?edit=1'); ?>">
                        <div class="hidden_nav">
                            Edit Friends
                            <?php echo $this->lang->line('Edit_Friends'); ?>
                        </div>
                    </a>
                </div>
                <!-- hidden navigations -->
            </div>
            <div class="cl p20px_0px" style="margin-top:12px;"></div>	

            

            <div class="body_bottom_1px">
                <div class="container  p10px_0px global_margin" style="background-color: white;font-size: 12px;font-weight: bold; color:#4C5E6B">
                    <div class="row">
                        <div class="fl" style="width: 130px;">
                            <?php
                            $action = site_url(MOBILE_M . '/user/searchFriends');
                            if ($edit == '1')
                                $action = site_url(MOBILE_M . '/user/searchFriends?edit=1');
                            if ($glomp == '1')
                                $action = site_url(MOBILE_M . '/user/searchFriends?glomp=1');
                            ?>

                            <form action="<?php echo base_url(MOBILE_M . '/user/searchFriends'); ?>" method ="GET">
                                <div class="global_border"  style=" padding: 4px;width: 100%;">
                                    <div class="search_icon fl" style="background-image:url('<?php echo base_url('assets/m/img/search_icon.png') ?>');" ></div>
                                    <div class="fl"><input name="keywords" style="color:#585F6B;border: 0px solid;width:90px;font-size:12px" type="text" placeholder="Search Friend" value="<?php echo $keywords; ?>"></div>
                                    <div class="cl"></div>
                                </div>
                            </form>
                        </div>
                        <div class="fr"  style="width: 41%;">
                            <div style="margin-left:4px;" class="fr info_tooltip_m" rel="popover" data-placement="left" data-content="<?php echo $this->lang->line('help_m_searchfriend_invite'); ?>" >
                                <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
                            </div>
                            <div class="fr" style="" align="center"  style="">
                                <a class="" href="<?php echo base_url(MOBILE_M . '/user/linkedinFriends') ?>">
                                    <div id="li_button" class="li_btn_small" style="margin-left:5px !important;cursor:pointer;background-image:url('<?php echo base_url('assets/images/icons/li-icon.jpg') ?>');"></div>
                                </a>
                            </div>
                            <div class="fr" style="" align="center" >
                                <a class="" href="<?php echo base_url(MOBILE_M . '/user/facebookFriends') ?>">
                                    <div  id="fb_button" class="fb_btn_small" style="cursor:pointer;background-image:url('<?php echo base_url('assets/m/img/fb-icon.png') ?>');"></div>
                                </a>
                            </div>
                            <div class="fr" style="margin-right: 15px;">
                                <a href="<?php echo site_url(MOBILE_M . '/user/inviteFriends'); ?>" class="white" >
                                    <button class="btn-custom-blue-grey_xs w60px" style="height:23px;padding:0px 6px 0px 6px !important; ">Invite</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="friendsList">
                <!-- /container -->
                <?php

                function cmp($a, $b) {
                    return strcmp($b->is_friend, $a->is_friend);
                }

                if ($resFriendList != "" && count($resFriendList) > 0) {
                    $sort = array();
                    foreach ($resFriendList as $k => $v) {
                        $sort['friend_data'][$k] = $v->friend_data;
                        $sort['friend_data'][$k] = $v->friend_data;
                        $sort['is_friend'][$k] = $v->is_friend;
                        $sort['mutual'][$k] = $v->mutual;
                    }
                    # sort by event_type desc and then title asc
                    array_multisort($sort['is_friend'], SORT_DESC, $sort['mutual'], SORT_DESC, $resFriendList);
                    $maxLimit = 200;
                    foreach ($resFriendList as $recFriendList) {
                        $maxLimit--;
                        if ($maxLimit < 0) {
                            break;
                        }
                        $mutual = $recFriendList->mutual;
                        $is_friend = $recFriendList->is_friend;
                        $recFriendList = $recFriendList->friend_data;
                        ?>					
                        <div class="body_bottom_1px">
                            <div class="container  p2px_0px global_margin">
                                <div class="row">
                                    <div class="fl">
                                        <a href="<?php echo site_url(MOBILE_M . '/profile/view/' . $recFriendList->user_id); ?>" >
                                            <img   id="data_img_<?php echo $recFriendList->user_id; ?>"  style="width: 70px; height:70px" src="<?php echo base_url() . '/' . $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_name; ?>" >
                                        </a>
                                    </div>
                                    <div class="fl" style="margin-left: 5px;">
                                        <a class="red" href="<?php echo site_url(MOBILE_M . '/profile/view/' . $recFriendList->user_id); ?>" style="color:#FF0000">
                                            <strong id="data_name_<?php echo $recFriendList->user_id; ?>" class="red" style="font-size: 14px;"><?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname ?></strong>
                                        </a>
                                        <br />
                                        <a href="<?php echo site_url(MOBILE_M . '/profile/view/' . $recFriendList->user_id); ?>" style="color:#768385" >	
                                            <span  id="data_loc_<?php echo $recFriendList->user_id; ?>"  style="color:#768385;font-size: 13px;"><?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></span>
                                        </a>
                                    </div>
                                    <?php if ($is_friend) {
                                        ?>
                                        <?php if ($edit == '1') {
                                            ?>
                                            <div class="fr" >
                                                <a href="<?php echo site_url(MOBILE_M . '/user/searchFriends?edit=1&remove_friend_id=' . $recFriendList->user_id) ?>">
                                                    <div class="menu_item_operation_wrapper" align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_minus.png'); ?>');"></div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                        <?php if ($glomp == '1' || (!$glomp && !$edit)) { ?>
                                            <div class="fr" >
                                                <a class="" href="<?php echo base_url(MOBILE_M . '/profile/menu/' . $recFriendList->user_id . '/?tab=favourites') ?>">                                            
                                                    <button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_gray.png'); ?>');"></button>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    <?php
                                    } else {
                                        ?>						
                                        <div class="fr" id="right_wrapper_<?php echo $recFriendList->user_id; ?>">	
                                            <div class="fr addThisFriendClass"  title="Add <?php echo $recFriendList->user_name; ?> to your glomp! network" id="<?php echo $recFriendList->user_id; ?>" style="cursor:pointer;border: 0px solid"  >
                                                <div class="menu_item_operation_wrapper" align="center" style="background-image:url('<?php echo base_url() ?>assets/m/img/glomp-add.png');"></div>											
                                            </div>
                                        </div>
                                        <div class="hidden" >
                                            <div id="hidden_<?php echo $recFriendList->user_id; ?>" >
                                                <a class="" href="<?php echo base_url(MOBILE_M . '/profile/menu/' . $recFriendList->user_id . '/?tab=favourites') ?>">                                            
                                                    <button class="btn-custom-blue-grey_normal  glomp_gray glomp_button_wrapper"   align="center" style="background-image:url('<?php echo base_url('/assets/m/img/glomp_gray.png'); ?>');"></button>
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo '<div class="container  p2px_0px global_margin" style="margin-top:4px;font-size:16px;" >No result found.</div>';
                    echo '<div class="container  p2px_0px global_margin" style="margin-top:4px;" >Add friends by searching, inviting via email or through Facebook above now.</div>';
                }
                ?>
            </div>
            <div class="footer_wrapper"></div>
            <div id="fb-root"></div>		
        </div> <!-- /global_wrapper -->  

        <!-- /footer -->        
        <!-- /footer -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>	
        <script src="<?php echo minify('assets/m/js/custom.js', 'js', 'assets/m/js'); ?>"></script>
        
        <script>
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_ADD_FRIEND_SUCCESS = "<?php echo $this->lang->line('GLOMP_ADD_FRIEND_SUCCESS'); ?>";
            var GLOMP_INVITE_REQUEST_SENT_BODY = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_BODY'); ?>";
            var GLOMP_INVITE_REQUEST_SENT_TITLE = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_TITLE'); ?>";
            $(function() {
                $("#main_menu").click(function() {
                    $("#hidden_menu").toggle();
                });
                $(".addThisFriendClass").click(function(ev) {
                    ev.preventDefault();
                    /*alert($(this).prop("id"));*/
                    var id = $(this).prop("id");
                    var name = $('#data_name_' + id).html();
                    var loc = $('#data_loc_' + id).html();
                    var desc = "Add " + name + " to Your glomp! network?";
                    var img = $('#data_img_' + id).prop('src');
                    var NewDialog = $('<div id="FriendPopupDialog" align="center"> \
                                                                                        <div class="friendThumb3" > \
                                                                                                <div style="width:75;height:100px; border:0px solid #333; overflow:hidden" > \
                                                                                                        <image id="fb_friend_popup_img" style="width: inherit; height: inherit; float:left;z-index:1;" src="' + img + '" alt="alt_pic"  \> \
                                                                                                </div> \
                                                                                        </div>\
                                                                                        <div class="friendThumb3_name" > \
                                                                                                <b>' + name + '</b><br /><span style="">' + loc + '</span>\
                                                                                        </div>\
                                                                                        <div class="friendThumb3_desc"  >' + desc + ' \
                                                                                        </div> \
                                                                                        <div class="cl"  ></div> \
                                                                                </div>');
                    NewDialog.dialog({
                        dialogClass: 'noTitleStuff',
                        dialogClass:'dialog_style_glomped_ash_blue',
                                title: "",
                        autoOpen: false,
                        resizable: false,
                        modal: true,
                        width: 300,
                        height: 250,
                        show: '',
                        hide: '',
                        buttons: [{text: 'Confirm',
                                "class": 'btn-custom-blue-grey_xs w80px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#FriendPopupDialog').dialog('destroy').remove();
                                        addThisFriend(id);
                                    }, 500);
                                }},
                            {text: "Cancel",
                                "class": 'btn-custom-white_xs w80px',
                                click: function() {
                                    $(this).dialog("close");
                                    setTimeout(function() {
                                        $('#FriendPopupDialog').dialog('destroy').remove();
                                    }, 500);
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                });
            });
            $(document).mouseup(function(e)
            {
                var container = $(".hidden_menu_class");
                if (!container.is(e.target) /* if the target of the click isn't the container...*/
                        && container.has(e.target).length === 0) /* ... nor a descendant of the container*/
                {
                    $('#hidden_menu').hide();
                }
            });
            function addThisFriend(id) {
                var NewDialog = $('<div id="confirmPopup" align="center">\
                                                                                <div align="center" style="margin-top:5px;"><img width="40" src="<?php echo base_url() ?>assets/m/img/ajax-loader.gif" /></div></div>');
                NewDialog.dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    resizable: false,
                    dialogClass: 'dialog_style_glomp_wait',
                    title: 'Please wait...',
                    modal: true,
                    position: 'center',
                    width: 200,
                    height: 120
                });
                NewDialog.dialog('open');
                var data = '&id=' + id;
                $.ajax({
                    type: "POST",
                    url: GLOMP_BASE_URL + 'ajax_post/addThisFriendNotFB',
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        $('#confirmPopup').dialog('destroy').remove();
                        $('#right_wrapper_' + id).html($('#hidden_' + id).html());
                        var NewDialog = $('<div id="messageDialog" align="center"> \
                                                                                                                <p style="padding:15px 0px;">' + GLOMP_ADD_FRIEND_SUCCESS + '.</p> \
                                                                                                        </div>');
                        NewDialog.dialog({
                            autoOpen: false,
                            resizable: false,
                            dialogClass: 'dialog_style_glomped_alerts',
                            title: GLOMP_INVITE_REQUEST_SENT_TITLE,
                            modal: true,
                            width: 200,
                            height: 140,
                            show: '',
                            hide: '',
                            buttons: [
                                {text: "Ok",
                                    "class": 'btn-custom-white_xs w80px',
                                    click: function() {
                                        $('#messageDialog').dialog('destroy').remove();
                                    }}
                            ]
                        });
                        NewDialog.dialog('open');
                    }
                });
            }

        </script>        

    </body>
</html>