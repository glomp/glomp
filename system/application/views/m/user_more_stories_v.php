<?php
$base_url = base_url();
$to_a = $this->lang->line('to_a');        

if($page_type=='dashboard')
{    
    if($glomp_buzz->num_rows()>0)
    {
    
        foreach ($glomp_buzz->result() as $glomp) {
            $start_count++;

            /*product and merchant info*/
            $prod_id = $glomp->voucher_product_id;
            $prod_info = json_decode($this->product_m->productInfo($prod_id));
            if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check_m->underage() ) continue;
            $prod_name = $prod_info->product->$prod_id->prod_name;
            $prod_image= $prod_info->product->$prod_id->prod_image;        
            $product_logo = $this->custom_func->product_logo($prod_image);
            $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
            $merchant_name = $prod_info->product->$prod_id->merchant_name;
            
            $from_id = $glomp->voucher_purchaser_user_id;
            $to_id = $glomp->voucher_belongs_usser_id;
            $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
            /*print_r($frn_info);*/
            $ago_date = $glomp->voucher_purchased_date;

            $form_name = $frn_info->friends->$from_id->user_name;
            $form_name_real = $frn_info->friends->$from_id->user_name;
            
                    
            if ($buzz_user_id == $from_id) {
                $form_name = $this->lang->line('You');
            }		
            $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

            $to_name = $frn_info->friends->$to_id->user_name;
            $to_name_real = $frn_info->friends->$to_id->user_name;
            
             if ($buzz_user_id == $to_id) {
                $to_name = $this->lang->line('You');
            }
            
            $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
            $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
            
            $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
            $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;
            
            $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
            $story_type="1";
            if($from_id!=$this->session->userdata('user_id'))
            {
                $story_type="2";
            }
            ?>
            <div class="body_bottom_1px" data-id="<?php echo $start_count;?>">        
                <div class="cl container p5px_0px global_margin">
                    <div class="row">
                        <div class="fl" style="width:73%;">
                            <?php if ($from_id == 1) {
                                ?>
                                <a class="red" href="javascript:void(0);"><?php echo $form_name; ?></a>
                            <?php } else { ?>
                                <a class="red" href="<?php echo $base_url . MOBILE_M.'/profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a> <?php } ?> <b>glomp</b>!ed <a class="red" href="<?php echo $base_url . MOBILE_M.'/profile/view/' . $to_id; ?>"><?php echo str_replace('ñ','&ntilde',$to_name); ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?>
                            <div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
                        </div>
                        <div class="fr" style="width:20%;border:0px solid #333;" align="right">
                            <div class="fr">
                                <!--<div><button class="btn-custom-blue-grey_xs w50px"  >Like</button></div>-->												
                                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs w50px" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>
                                <div style="height:0px;">
                                    <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_li_id="<?php echo $from_li_id;?>" data-to_li_id="<?php echo $to_li_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="dashboard_m" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--
                <div class="span6" style="text-align:right;">
                    <?php if ($from_id == 1) {
                        ?>
                        <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
                        <?php
                    } else {
                        ?>
                      <a href="<?php echo base_url('profile/view/' . $from_id); ?>"><img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>

                    <?php } ?>

                    
                    <img src="<?php echo base_url() ?>assets/m/img/glomp_arrow_red.jpg" />
                    <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                    <img src="<?php echo base_url() ?>assets/m/img/glomp_arrow_red.jpg" />									

                    <a href="<?php echo base_url('profile/view/' . $to_id); ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
                    
                </div>
                -->
            </div>
            <?php        
        }/*foeach close*/
    }
    else
    {
        echo '';
    }
}
else if($page_type=='profile')
{
    if($glomp_buzz->num_rows()>0)
    {
        foreach ($glomp_buzz->result() as $glomp) {
            $start_count++;

            /*product and merchant info*/
            $prod_id = $glomp->voucher_product_id;
            $prod_info = json_decode($this->product_m->productInfo($prod_id));
            if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check_m->underage() ) continue;
            $prod_name = $prod_info->product->$prod_id->prod_name;
            $prod_image= $prod_info->product->$prod_id->prod_image;        
            $product_logo = $this->custom_func->product_logo($prod_image);
            $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
            $merchant_name = $prod_info->product->$prod_id->merchant_name;
            
            $from_id = $glomp->voucher_purchaser_user_id;
            $to_id = $glomp->voucher_belongs_usser_id;
            $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
            $ago_date = $glomp->voucher_purchased_date;

            $form_name = $frn_info->friends->$from_id->user_name;
            $form_name_real = $frn_info->friends->$from_id->user_name;

            if ($buzz_user_id == $from_id) {
                $form_name = $this->lang->line('You');
            }					

            $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

            $to_name = $frn_info->friends->$to_id->user_name;
            $to_name_real = $frn_info->friends->$to_id->user_name;
            if ($buzz_user_id == $to_id) {
                $to_name = $this->lang->line('You');
            }
            
            $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
            $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
            
            $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
            $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;

            $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
            $story_type="1";
            if($from_id!=$this->session->userdata('user_id'))
            {
                $story_type="2";
            }
            ?>
            <div class="body_bottom_1px" data-id="<?php echo $start_count;?>">        
                <div class="container  p10px_0px global_margin">
                    <div class="row">
                        <div class="fl" style="width:73%;">                                
                            <?php if ($from_id == 1) { ?>
                                <a class="red" href="javascript:void(0);"><?php echo $form_name; ?></a>
                            <?php } else { ?>
                                <a class="red" href="<?php echo $base_url .MOBILE_M. '/profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a>
                            <?php } ?>  &nbsp;glomp!ed <a class="red" href="<?php echo $base_url .MOBILE_M. '/profile/view/' . $to_id; ?>"><?php echo $to_name; ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?>
                            <span class='red'><?php  echo str_replace(' ','&nbsp;',($this->custom_func->ago($ago_date))); ?></span>
                        </div>                            
                        <?php 
                        if($this->session->userdata('is_user_logged_in')== true)
                        {
                        ?>
                        <div class="fr" style="width:20%;border:0px solid #333;" align="right">
                            <div class="fr">
                                <!--<div><button class="btn-custom-blue-grey_xs w50px"  >Like</button></div>-->												
                                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs w50px" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line(''); ?>share</button>
                                <div style="height:0px !important;">
                                    <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name_real;?>" data-purchaser="<?php echo $form_name_real;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_li_id="<?php echo $from_li_id;?>" data-to_li_id="<?php echo $to_li_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="profile_m" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                        }
                        ?>
                        
                        
                    </div>
                </div>
            </div>
            <?php
            
        }
    }
    else
    {
        echo '';
    }
}
?>