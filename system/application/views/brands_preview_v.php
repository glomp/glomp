<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <style type="text/css">
            .brand-page {
                padding:0 0 0 5px;
                background:#5a616c;
                font-family: Arial, sans-serif;
            }
            .brand-title h1 {
                color:#fff;
                margin:0;
                padding:0;
                font-size:1em;
                font-weight:normal;
            }
            .brand-template {
                background:#fff;
                margin-bottom:5px;
                width:720px;
                -webkit-border-radius: 0 5px 0 4px;
                border-radius: 0 5px 0 4px;
                padding:20px 40px;
            }
            .brand-header {
                margin: 20px 0;
            }
            .brand-header:after {
                content:" ";
                display:block;
                clear:both;
            }
            .brand-logo{
                float:left;
                width:260px;
            }
            .brand-description {
                float:right;
                width:440px;
                text-align: justify;
            }
            .brand-template:after {
                content:" ";
                display:block;
                clear:both;
            }
            .brand-content {
                float:left;
				<?php if($brand->public_alias !='amex' && $brand->public_alias !='hsbc' && $brand->public_alias !='uob' && $brand->public_alias !='dbs'): ?>
					width:580px;
				<?php else:?>
					width:720px;
				<?php endif;?>
            }
            .products {
                float:right;
                width:120px;
            }
            .products a:hover img {
                opacity: 0.7;
            }
            .products a:hover {
                text-decoration:none;
            }
            .brand-section {
                margin-bottom:15px;
            }
            .brand-section:last-child {
                margin-bottom:0px;
            }
            .brand-section .section-type-one .section-col {
                <?php if($brand->public_alias !='amex' && $brand->public_alias !='hsbc' && $brand->public_alias !='uob' && $brand->public_alias !='dbs'): ?>
					width:580px;
				<?php else:?>
					width:720px;
				<?php endif;?>
            }
            .brand-section .section-type-two .section-col {
                width: 275px;
            }
            .brand-section .section-type-three .section-col {
                width: 190px;
            }
            .brand-section .section-type-two_one .section-col:first-child {
                width: 370px;
            }
            .brand-section .section-type-two_one .section-col:last-child {
                width: 180px;
            }
            .section-row:after {
                content:" ";
                display:block;
                clear:both;
            }
            .section-col {
                float:left;
                margin-left:5px;
            }
            .section-col:first-child {
                margin-left:0px;
            }
            .widget-title-head {
                margin:0 0 3px;
                font-size:1.4em;
                line-height:1em;
                font-weight:normal;
            }
            .widget-title-sub {
                margin:0px;
                font-size:1em;
                font-weight:bold;
            }
            .widget-photostory {
            
            }
            .widget-photostory .widget-title {
                padding:11px 8px 7px;
            }
            .widget-photostory-body {
                margin-top:5px;
            }
            .widget-photostory-body:after {
                margin-top:10px;
                content:" ";
                display:block;
                clear:both;
            }
            .widget-photostory-image {
                width:375px;
            }
            .widget-photostory-context {
                width:200px;
                text-align: justify;
            }
            .widget-photostory-context div {
                padding:16px 8px;
            }
            .widget-photostory.right .widget-photostory-image {
                float:right;
            }
            .widget-photostory.right .widget-photostory-context {
                float:left;
            }
            .widget-photostory.left .widget-photostory-image {
                float:left;
            }
            .widget-photostory.left .widget-photostory-context {
                float:right;
            }
            .widget-photostory.grey .widget-title,
            .widget-photostory.grey .widget-photostory-context {
                background:#666;
                color:#fff;
            }
            .widget-photostory.lightgrey .widget-title,
            .widget-photostory.lightgrey .widget-photostory-context {
                background:#ddd;
                color:#333;
            }
            .widget-photostory.white .widget-title,
            .widget-photostory.white .widget-photostory-context {
                background:#f0f0f0;
                color:#888;
            }
            .back-to-brands {
                float:right;
                margin-right: 131px;
                margin-top: 8px;
                border-radius: 5px;
                background: #8bc541;
                padding:4px 20px;
                color:#fff;
            }
            .back-to-brands:hover {
                background: #9bdd48;
                -webkit-box-shadow: 0px 0px 4px 1px rgba(0,0,0,0.4);
                -moz-box-shadow: 0px 0px 4px 1px rgba(0,0,0,0.4);
                box-shadow: 0px 0px 4px 1px rgba(0, 0, 0, 0.4);
            }
            .back-to-brands:hover, .back-to-brands:active {
                color:#fff;
                text-decoration:none;
            }
            .tri {
                width : 0;
                height : 0;
                border-top : 6px solid transparent;
                border-bottom : 6px solid transparent;
                border-right : 15px solid #fff;
                position : relative;
                top:2px;
                left:-5px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>
        <?php $this->load->view('includes/header.php') ?>

        <div class="container">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="outerBorder">
                        <div class="innerBorder brand-page">
                            <div class="brand-title" style="overflow:auto">
                                <h1 style="float:left"><?php echo $brand->name;?></h1>
								<?php
									if(isset($user_record->user_id))
									{
								?>
                                <a href="/profile/view/<?php echo $user_record->user_id;?>?showbrands=true" >
									<button class="back-to-brands">
                                    <?php echo $this->lang->line('brands_list','Brands List');?>
									</button>
                                </a>
								<?php
									}
								?>
                            </div>
                            <div class="brand-template">
                                <div class="brand-banner">
                                    <img src="<?php echo base_url('/custom/uploads/brands/'.$brand->banner);?>" width="720" />
                                </div>
                                <div class="brand-header">
                                    <div class="brand-logo"><img src="<?php echo base_url('/custom/uploads/brands/'.$brand->logo);?>" /></div>
                                    <div class="brand-description"><?php echo $brand->description?></div>
                                </div>
                                <div class="brand-content">
                                    <?php if(isset($sections) and is_array($sections)):?>
                                    <?php foreach($sections as $section):?>
                                    <div class="brand-section">
                                        <div class="section-type-<?php echo strtolower($section->type);?> section-row">
                                            <?php if($widgets && array_key_exists($section->id,$widgets)):?>
                                            <?php foreach($widgets[$section->id] as $widget):?>
                                            <div class="section-col">
                                                <?php include('includes/widget_type_'.$widget->widget_type.'.php'); ?>
                                            </div>
                                            <?php endforeach;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                </div>
								<?php if($brand->public_alias !='amex' && $brand->public_alias !='hsbc' && $brand->public_alias !='uob' && $brand->public_alias !='dbs'):
                                echo $brand->public_alias
								?>
                                <div class="products merchantCatProduct" style="border:1px solid #333">
                                    <?php if($brandproducts):?>
                                    <?php foreach($brandproducts as $bp):?>
                                    <div style="margin-bottom:10px" class="brand-product displayPopUp">
                                        <?php if($is_preview):?>
                                        <a href="javascript:void(0)" style="color:#495368;">
                                            <img src="/custom/uploads/brands/products/<?php echo $bp->image_logo?>" />
                                        </a>
                                        <?php else:
											if(isset($user_record->user_id))
											{
										?>	
											<a href="/profile/view/<?php echo $user_record->user_id?>?showbrandproducts=<?php echo $bp->brand_id?>/<?php echo $bp->id?>" style="color:#495368;">
											<?php
											}
											else
											{
												
											?>
											<a href="/brands/view/<?php echo $brand->public_alias?>/<?php echo $bp->id?>" style="color:#495368;">
											<?php
											}
											?>
                                            <img src="<?php echo base_url('custom/uploads/brands/products/'.$bp->image_logo);?>" />
                                            <div style="font-size:0.8em;padding:2px 6px;background:#ddd"><?php echo $bp->name?></div>
                                        </a>
                                        <?php endif;?>
                                        <div class="popUp">
                                            <div class="name1"><?php echo $bp->name?></div>
                                            <div class="name2"><?php echo $bp->description;?></div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                    <?php echo $this->lang->line('no_brandproducts', 'Product is not available at present.');?>
                                    <?php endif;?>
                                </div>
								<?php endif;?>
                                <script type="text/javascript">
                                    jQuery(document).on('focus','.tab[name=test]',function(){alert('testsetestest')});
                                    
                                    jQuery(document).on('mouseenter','.displayPopUp > *', function(e){
                                        jQuery(e.target).parents('.displayPopUp').find('.popUp').show().css({'margin':'0px'});
                                    }).on('mouseleave','.displayPopUp', function(e){
                                        jQuery('.popUp').hide();
                                    });

                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
