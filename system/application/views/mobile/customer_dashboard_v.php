<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Welcome to glomp!</title>
  
  <meta name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
 $(document).ready(function(e) {
	$(".cus-row").click(function(){
		var _this = $(this);
		var url = _this.data('url');
		document.location=url;
		});
		
		$('.more_message').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			var id = $(this).attr('id');
			var n_id = 'view_more_'+id;
			$('#'+n_id).toggle();
			});
});
  </script>
</head>
<body class="bodyMarginPadding">

<div class="container">
<div class="outerBorder">
<?php include('includes/header.php');?>
	<div class="inner-content">
      <div class="row-fluid">
      
     	
      <div class="dashboard">
      <h1>&nbsp; </h1>
      <div class="content">
      <?php 
	  $i=0;
	  if($glomped_me->num_rows()>0){
			 foreach($glomped_me->result() as $rec_glomped_me){
			 $prod_id = $rec_glomped_me->voucher_product_id;
			 $glomped_message = $rec_glomped_me->voucher_sender_glomp_message;
			 $glomped_by_name = $rec_glomped_me->user_name;
			$glomped_by_photo = $this->custom_func->profile_pic($rec_glomped_me->user_profile_pic,$rec_glomped_me->user_gender);
		$glomped_me_prod = json_decode($this->product_m->productInfo($prod_id));
		$prod_name = $glomped_me_prod->product->$prod_id->prod_name;
		$product_logo = $this->custom_func->product_logo($glomped_me_prod->product->$prod_id->prod_image);
		$merchant_name = $glomped_me_prod->product->$prod_id->merchant_name;
		
		?>
      <div class="cus-row" data-url="<?php echo base_url(MOBILE_FOLDER."/user/redeem/".$rec_glomped_me->voucher_id)?>">
      <div class="left"><div class="padding">
      <div class="time"><?php echo $this->custom_func->ago($rec_glomped_me->voucher_purchased_date);?></div>
      <div class="product">
      <img src="<?php echo $product_logo;?>" alt="<?php echo $prod_name;?>" class="product" />
      </div>
      </div></div>
      <div class="mid"><div class="padding"><?php echo stripslashes($merchant_name);?>'s <br/><?php echo stripslashes($prod_name);?></div>
    
      <div class="message"><?php echo $glomped_message;?></div>
	  <?php 
	   if($rec_glomped_me->voucher_status == 'Redeemed' || $rec_glomped_me->voucher_status == 'Expired')
	   	echo $rec_glomped_me->voucher_status;
	   ?>
      </div>
      	
      <div class="right">
      <div class="padding">
      <div class="thumbHolder">
       <img src="<?php echo $glomped_by_photo;?>" alt="<?php echo $glomped_by_name;?>" />
       <div class="name"><?php echo $glomped_by_name;?></div>
      </div>
      </div>
      </div>
      
      <div class="clearfix"></div>
      </div>
      <?php 
	  $i++;
			 }//foreeach closed
	  }//if num rows clsoed
	  else{
	  
	  ?>
	  <p class="alert alert-error-red" style="border:none"><?php echo $this->lang->line('No_result_found');?> !</p>
	  <?php
	  }
      ?>
      <div class="clearfix"></div>
      </div>
      </div>
   
		
      </div>
      </div>
      </div>
      </div>
</div>
</body>
</html>