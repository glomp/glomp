<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo stripslashes($this->lang->line('invalid_voucher'));?></title>
   <meta name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script type="text/javascript" src="assets/mobile/js/validate.js"></script>
  <script src="assets/mobile/js/custom.js" type="text/javascript"></script>
</head>
<body class="bodyMarginPadding">

<div class="container">
<div class="outerBorder">
<?php include('includes/header.php');?>
	<div class="inner-content">
      <div class="row-fluid">
      <div class="redeem">
      <div id="infoHolder">
		<div class="alert alert-error-red" style=" text-align:center;">
    			<?php echo stripslashes($invalid_voucher);?> <br/><br />
              
    </div>
   	<div align="center"> <button type="button" onClick="javascript:location.href='<?php echo base_url(MOBILE_FOLDER);?>'" name="close" class="btn-custom-gray btnValidate"><?php echo $this->lang->line('close');?></button></div>	
      </div>
      </div>
      
      </div>
      
      </div>
      </div>
      </div>
      </div>
</div>
</body>
</html>