<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Welcome to glomp</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script src="assets/mobile/js/validate.js" type="text/javascript"></script>
  <script type="text/javascript">
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#frmLogin").validate({
			rules: {
				user_name	:
				{
				 required: true,
				 email: true
				},
				user_password:"required",
			}
				,
			messages: {
				user_name	:"",
				user_password:"",
			}
	});
	});
	</script>
</head>
<body style="background:#585f6b;">
<div class="container">
	<div class="inner-content">
      <div class="row-fluid" style="margin-top:30px;">
      <div class="span12 text-center"><img src="assets/mobile/img/glomp-login-logo.png" alt="logo">
      <?php if(isset($error_msg)){?>
          <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
   	<?php echo $error_msg;?>
    </div>
    <?php 
	  }
	?>
      <?php if(validation_errors()!=""){?>
      <div class="alert alert-error">
      <?php echo validation_errors();?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
      <?php 
	  }
	  ?>
      </div>
      </div>
      <div class="row-fluid">
      <div class="span12 landingPage">
       <?php echo form_open('mobile/landing','name="frmLogin" id="frmLogin"')?>
        <div class="row-fluid">
      <div class="span12">
        <input type="text" name="user_name" placeholder="User Email" id="user_name" value="" class="span12">
      </div>
      </div>
      <div class="row-fluid">
      <div class="span12">
        <input type="password" name="user_password" id="user_password" value="" placeholder="Password" class="span12">
      </div>
      </div>
      <div class="row-fluid">
      <div class="span12">
      <input type="submit" name="user_login" id="user_login"  class="btn-custom-primary btn-block" value="<?php echo $this->lang->line('Sign_in');?>">
      
    
      </div>
      </div>
      
      <div class="row-fluid">
      <div class="span12">
	      <?php echo anchor(MOBILE_FOLDER.'/landing/register',
		  $this->lang->line('register'),
		  'class="btn-custom-gray btn-block"');?>
      </div>
      </div>
        
       <?php echo form_close();?>
      </div>
      </div>
      
      <div class="row-fluid">
      <div class="span12 termAndConditions" >
         <?php echo anchor('',
		 $this->lang->line('full_destop_website'));
		 ?> | <a href="#termModel" data-toggle="modal"><?php echo $this->lang->line('termNcondition');?></a>
     
    
      </div>
      </div>
      
      </div>
</div>


<!-- Modal -->
    <div id="termModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><?php echo $this->lang->line('termNcondition');?></h3>
    </div>
    <div class="modal-body">
   <?php
			 $res_privacy = $this->cmspages_m->selectStaticPage(2,DEFAULT_LANG_ID);
			if($res_privacy->num_rows()>0)
			{
				$rec_privacy = $res_privacy->row();
				echo  $rec_privacy->content_content;
			}
		?>
    

    </div>
    <div class="modal-footer">
    <button class="btn-custom-primary" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('close');?></button>
    
    </div>
    </div>
</body>
</html>