<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('redeem_your_voucher_now');?></title>
   <meta name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">
  <meta content="" name="description">
  <meta name="format-detection" content="telephone=no" />
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script type="text/javascript" src="assets/mobile/js/validate.js"></script>
  <script src="assets/mobile/js/custom.js" type="text/javascript"></script>
    <script src="assets/mobile/js/jquery.barcode.0.3.js" type="text/javascript"></script>
  
  <script type="text/javascript">
  function redirect()
  {
	location.href='<?php echo base_url('mobile/user');?>';  
	}
	
	/// calls in mobile screen rotation/
	$(window).resize(function(e) {
     $.fn.rensponsivePopup();
    });
	
  $().ready(function() {
	$("#frmRedeem").validate({
		rules: {
			outlet_code: "required",	
			user_password: "required"	
		},
		messages: {
			outlet_code: "",
			user_password:""	
		}
		,submitHandler: function(form) {
			
			//$.fn.loadLoader();/// calling loader 
			//form.submit();
			
			$('#verifying').show();
			$('#infoHolder').hide();
			
			$.ajax({
					type: "POST",
					url: '<?php echo MOBILE_FOLDER.'/user/redeem/'.$voucher_id?>',
					data: $('#frmRedeem').serialize(),
					cache: false,
					success: function(data){
						
						var d = eval("("+data+")");
						if(d.status=='error')
							{
								$('#verifying').hide();
								$('#infoHolder').show();
								var popup_append='<div class="loader_background" style="height:'+$('body').height()+'px"></div>';
	$("body").append(popup_append);
								$('.popUp').show();
								$.fn.rensponsivePopup();
								$('.popUpMessage').html(d.msg);
								
							}
						else if(d.status=='success')
						{
							window.location='<?php echo MOBILE_FOLDER.'/user/redeemSuccess/'.$voucher_id?>';
						}
							
						}
					 });	
			
			}
		});
		
		$('._close').click(function(){
			$(".loader_background").remove();
			$('.popUp').hide();
			
			})
});

$.fn.rensponsivePopup =function(){
	var w =$(window);
	var _y = (w.height()/2)-(parseInt($(".popUp").css("height"))/2);
	var _x = (w.width()/2)-(parseInt($(".popUp").css("width"))/2);	
	$(".popUp").css({"left":Math.abs(parseInt(_x-10)+w.scrollLeft()), "top":Math.abs(_y+w.scrollTop())});
	}


$(document).ready(function(){
				
					$('.barcode39') .barcode({code:'code39'});
				}
			);

  </script>
  
</head>




<div class="popUp">
      <div class="popUpHeading">Error!</div>
      <div class="popUpMessage"></div>
      <button type="button"  class="btn-custom-popup _close"><?php echo $this->lang->line('try_again');?></button>
      </div>
<body class="bodyMarginPadding" style="background:#DEE6EB;">

<div class="container">
<div class="outerBorder">
<?php include('includes/header.php');?>
	<div class="inner-content">
      <div class="row-fluid">
      <div class="redeem">
      <div id="infoHolder">
      <h1 class="glompFont"><?php echo $this->lang->line('Redeem');?></h1>
       <?php if(validation_errors()!=""){ ?>
      <div class="alert alert-error">
      <?php echo validation_errors();?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
      <?php 
	  }
	  if($vouhcer_valid)
	  {
	  ?>
	  <?php
	  if($voucher_status!="Consumable")
	  {
	  ?>
     <div class="alert alert-error" style=" text-align:center;">
    	<?php echo $voucher_status_msg;?>
    </div>
    <?php
	}
	if(isset($error_msg))
	{
	?>
      <div class="alert alert-error">
    			<?php echo $error_msg;?>
    </div>
    <?php
	}
	$res_product = $this->product_m->productAndMerchant($prod_id);
	$row_product = $res_product->row();
	$prod_image = $this->custom_func->product_logo($row_product->prod_image);
	$merchant_area = $this->regions_m->region_name($row_product->merchant_region_id);;
	?>
      
      <div class="productHolder">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td width="80%" valign="top"><strong><?php echo $row_product->merchant_name;?></strong><br>
              <?php echo $row_product->prod_name;?></td>
            <td valign="top"><img src="<?php echo $prod_image;?>" alt="<?php echo $row_product->prod_name;?>" class="img-rounded" width="100" ></td></td>
          </tr>
        </table>
      </div>
      
      <?php if($row_product->prod_terms_condition!=0 || $row_product->prod_terms_condition!="") {?>
      <div class="termsAndConditionsHolder">
      <strong><?php echo $this->lang->line('termNcondition');?></strong> <br />
     <?php 
	 	echo stripslashes($row_product->prod_terms_condition);
	 ?>
      </div>
      
      <?php }?>
       <?php if($row_product->prod_warning!="") {?>
      <div class="termsAndConditionsHolderpink">
     <?php 
	 	echo stripslashes($row_product->prod_warning);
	 ?>     
      </div>
      
      <?php }?>
     
      
      <div class="formHolder redeemVoucher">
          <?php
	  if($voucher_status=="Consumable")
	  {
	 	echo form_open(MOBILE_FOLDER.'/user/redeem/'.$voucher_id,'class="form-horizontal" id="frmRedeem" name="frmRedeem"')
	  ?>
      
      
          <table width="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td width="40%"><strong><?php echo $this->lang->line('outlet_code');?></strong></td>
              <td> <input type="text" id="outlet_code" name="outlet_code" value="<?php echo set_value('outlet_code');?>" class="span12"></td>
            </tr>
            <tr>
              <td><strong><?php echo $this->lang->line('glomp_password');?></strong></td>
              <td> <input type="password" id="password" name="user_password"  class="span12"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td> <div class="forgotPassword"><a href="<?php echo base_url(MOBILE_FOLDER.'/landing/forgotPassword');?>">Forgot Password?</a></div></td>
            </tr>
          </table>

    
    
   <div class="btnHolder">
     <button type="submit" name="voucher_validation" class="btn-custom-gray btnValidate"><?php echo $this->lang->line('validate');?></button>
     </div>
    <?php
	 echo form_close();
	  }
	  else if($voucher_status=='Redeemed')
	  {
	?>
     <div class="redeemSuccess">
        <div class="barCodeHolder">
       <p><strong class="successMessage"><?php echo $this->lang->line('verification_code');?> </strong> <br/>
       	<div class="verification_code" style="font-size:32px; margin:5px; color:#EC1C24; font-weight:bold"><?php echo $row_voucher->voucher_code;?></div>
        </p>
         <div class="barcode39" style="width:58%;height:50px; margin:0 auto">
			<?php echo $row_voucher->voucher_code?>
		</div>
       </div>
        <div class="locationHolder" style="color:#333"><?php echo $merchant_area;?> <br />
        	<?php echo   $this->users_m->customFormat($row_voucher->voucher_redemption_date);?>
          </div> 
     
      </div>
    <?php
	  }
	}else
	{?>
		<div class="alert alert-error" style=" text-align:center;">
    			<?php echo $invalid_voucher;?> <br/><br />
              <button type="button" name="back" onClick="redirect()" class="btn-custom-primary btn-block"><i class=" icon-circle-arrow-left"></i> <?php echo ucfirst($this->lang->line('back'));?></button>  
              
    </div>		
	<?php }
	?>
      </div>
      </div>
      
      <div id="verifying"> <img src="assets/mobile/img/verifying.png" ></div>
      
      </div>
      
      </div>
      </div>
      </div>
      </div>
</div>
</body>
</html>