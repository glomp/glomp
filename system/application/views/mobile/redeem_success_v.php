
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Voucher Validated</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <meta name="format-detection" content="telephone=no" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script src="assets/mobile/js/jquery.barcode.0.3.js" type="text/javascript"></script>
  <script language="javascript" type="text/javascript">
			$(document).ready(function(){
				
					$('.barcode39') .barcode({code:'code39'});
				}
			);
		</script>
</head>
<body class="bodyMarginPadding">

<div class="container">
<div class="outerBorder">
<?php include('includes/header.php');?>
	<div class="inner-content">
      <div class="row-fluid">
    
      <div class="redeemSuccess">
      	<div style="padding:15px">
      <div class="alert alert-success-redeem successMessage">
       <?php echo $this->lang->line('your_voucher_has_been_validated');
	   //Your voucher has been validated!
	   ?>  <br /><strong><?php echo ucfirst($this->lang->line('enjoy'));?>!</strong>
        </div>
        <div class="barCodeHolder">
       <p><strong class="successMessage"><?php echo $this->lang->line('verification_code');?></strong><br/>
       		<div class="verification_code" style="font-size:32px; color:#EC1C24; margin:5px; font-weight:bold"><?php echo $row_voucher->voucher_code;?></div>
        </p>
        <div class="barcode39" style="width:58%;height:50px; margin:0 auto">
			<?php echo $row_voucher->voucher_code?>
		</div>
       </div>
       
        <div class="locationHolder" style="color:#333"><?php echo $merchant_area;?> <br />
        	<?php echo   $this->users_m->customFormat($row_voucher->voucher_redemption_date);?>
          	<div class="validate_success_ball"><img src="assets/mobile/img/validate_success_ball_logo.png" /></div>
          </div> 
       <div class="alert alert-success-redeem successMessage" style="color:#fff; font-size:14px;">
		<?php echo $this->lang->line('reedmed_success_message_note');
		//Please do not leave this page until the merchant has view and recored the verification code. 
		?>
           </div>
      </div>
     
		
      </div>
      </div>
      </div>
      </div>
</div>
</body>
</html>