<?php
$base_url = base_url();
$to_a = $this->lang->line('to_a');        

if($page_type=='dashboard')
{
    if($glomp_buzz->num_rows()>0)
    {
        foreach($glomp_buzz->result() as $glomp)
        {
            $start_count++;            
            
            /*product and merchant info*/
            $prod_id = $glomp->voucher_product_id;
            $prod_info = json_decode($this->product_m->productInfo($prod_id));
            if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
            $prod_image= $prod_info->product->$prod_id->prod_image;        
            $product_logo = $this->custom_func->product_logo($prod_image);
            $prod_name = $prod_info->product->$prod_id->prod_name;
            $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
            $merchant_name = $prod_info->product->$prod_id->merchant_name;
            $merchant_id = $prod_info->product->$prod_id->merchant_id;

            $from_id = $glomp->voucher_purchaser_user_id;
            $to_id = $glomp->voucher_belongs_usser_id;
            $frn_info = json_decode($this->users_m->friends_info_buzz($from_id.','.$to_id));
            /*print_r($frn_info);*/
            $ago_date = $glomp->voucher_purchased_date;

            $form_name = $frn_info->friends->$from_id->user_name;
            $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic,$frn_info->friends->$from_id->user_gender);
            
            $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
            $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
            
            
            $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
            $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;
            
            
            $to_name = $frn_info->friends->$to_id->user_name;
            $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic,$frn_info->friends->$to_id->user_gender);
            $story_type="1";
            if($from_id!=$this->session->userdata('user_id'))
            {
                $story_type="2";
            }
            ?>
            <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">        
                 <div class="span7" style="font-weight:bold;border:solid 0px;">
                    <div class="row-fluid">
                        <div class="span10">
                            <?php if($from_id==1)
                            {?>
                                <a href="javascript:void(0);"><?php echo $form_name;?></a>
                            <?php 
                            } else {?>
                                <a href="<?php echo $base_url.'profile/view/'.$from_id;?>"><?php echo $form_name;?></a> <?php } ?> <b>glomp!</b>ed <a href="<?php echo $base_url.'profile/view/'.$to_id;?>"><?php echo $to_name;?></a> <?php echo $to_a;?> <?php echo $merchant_name;?> <?php echo stripslashes($prod_name);?> 
                            <div class="time"><?php echo $this->custom_func->ago($ago_date);?></div>
                        </div>
                        <div class="span2" align="right" style="border:0px solid">
                            <div style="margin-top:28px;" >
                                <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>
                                <div style="height:0px;">
                                    <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                        <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="dashboard" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>                          
                 </div>
             <div class="span5" style="text-align:right;border:solid 0px;">
             <?php if($from_id==1)
             {?>
             <img src="<?php echo $from_name_photo;?>" alt="<?php echo $form_name;?>" class="face" />
             <?php
             }
             else
             {
             ?>
             <a href="<?php echo base_url('profile/view/'.$from_id);?>"><img src="<?php echo $from_name_photo;?>" alt="<?php echo $form_name;?>" class="face" /></a>
             
             <?php }?>
             
              <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px" />
               <a href="<?php echo base_url('profile/view/'.$to_id.'?tab=tbMerchant&merID='.$merchant_id);?>"> 
                <img src="<?php echo $merchant_logo;?>" class="merchant_logo" alt="<?php echo $merchant_name;?>" />
                </a>
               <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px" />
               
              
              <a href="<?php echo base_url('profile/view/'.$to_id);?>"><img src="<?php echo $to_name_photo;?>" alt="<?php echo $to_name;?>" class="face" /></a>
             </div>
             </div>
        <?php             
        }
    }
    else
    {
        echo '';
    }
}
else if($page_type=='profile')
{
    if($glomp_buzz->num_rows()>0)
    {    
        foreach ($glomp_buzz->result() as $glomp)
        {
            $start_count++;

            /*product and merchant info*/
            $prod_id = $glomp->voucher_product_id;
            $prod_info = json_decode($this->product_m->productInfo($prod_id));
            if( in_array( $prod_info->product->$prod_id->prod_cat_id, $underage_filtered ) && $this->user_login_check->underage() ) continue;
            $prod_image = $prod_info->product->$prod_id->prod_image;
            $product_logo = $this->custom_func->product_logo($prod_image);
            $prod_name = $prod_info->product->$prod_id->prod_name;
            $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
            $merchant_name = $prod_info->product->$prod_id->merchant_name;
            $merchant_id = $prod_info->product->$prod_id->merchant_id;
            
            $from_id = $glomp->voucher_purchaser_user_id;
            $to_id = $glomp->voucher_belongs_usser_id;
            $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
            $ago_date = $glomp->voucher_purchased_date;

            $form_name = $frn_info->friends->$from_id->user_name;
            $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

            $to_name = $frn_info->friends->$to_id->user_name;
            $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);


            $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
            $to_fb_id = $frn_info->friends->$to_id->user_fb_id;


            $story_type = "1";
            if ($from_id != $this->session->userdata('user_id')) {
                $story_type = "2";
            }
            ?>

                                                                                <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">        
                                                                                    <div class="span7">
                                                                                        <div class="row-fluid">
                                                                                            <div class="span10">
            <?php if ($from_id == 1) {
                ?>
                                                                                                    <a href="javascript:void(0);"><?php echo $form_name; ?></a>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"><?php echo $form_name; ?></a>
                                                                                                <?php } ?>
                                                                                                glomp!ed <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><?php echo $to_name; ?></a> <?php echo $to_a; ?> <?php echo $merchant_name; ?> <?php echo stripslashes($prod_name); ?><div class="time"><?php echo $this->custom_func->ago($ago_date); ?></div>
                                                                                            </div>
                                                                                            <?php 
                                                                                            if($this->session->userdata('is_user_logged_in')== true)
                                                                                            {
                                                                                            ?>
                                                                                            <div class="span2" align="right">                    
                                                                                                <div style="margin-top:28px;" >
                                                                                                    <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs" data-voucher_id="<?php echo $glomp->voucher_id; ?>" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>
                                                                                                    <div style="height:0px;">
                                                                                                        <div id="share_wrapper_box_<?php echo $glomp->voucher_id; ?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id; ?>" style="" align="center">
                                                                                                            <button data-merchant="<?php echo $merchant_name; ?>"  data-belongs="<?php echo $to_name; ?>" data-purchaser="<?php echo $form_name; ?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg'); ?>'); background-size: 21px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id; ?>" data-to_fb_id="<?php echo $to_fb_id; ?>" data-story_type="<?php echo $story_type; ?>" data-prod_name="<?php echo $prod_name; ?>" data-prod_img="<?php echo $product_logo; ?>"  data-voucher_id="<?php echo $glomp->voucher_id; ?>"  ></button>
                                                                                                            <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $form_name;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                                                                                            <button title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg'); ?>'); background-size: 21px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="profile" data-voucher_id="<?php echo $glomp->voucher_id; ?>"  ></button>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php 
                                                                                            }
                                                                                            ?>
                                                                                        </div> 


                                                                                    </div>
                                                                                    <div class="span5">
            <?php if ($from_id == 1) { ?>
                                                                                            <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" />
            <?php
            } else {
                ?>
                                                                                            <a href="<?php echo $base_url . 'profile/view/' . $from_id; ?>"> <img src="<?php echo $from_name_photo; ?>" alt="<?php echo $form_name; ?>" class="face" /></a>
                                                                                        <?php } ?>
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg" />
                                                                                        <a href="<?php echo base_url('profile/view/'.$to_id.'?tab=tbMerchant&merID='.$merchant_id);?>"> 
                                                                                            <img src="<?php echo $merchant_logo; ?>" class="merchant_logo" alt="<?php echo $merchant_name; ?>" />
                                                                                        </a>
                                                                                        <img src="assets/frontend/img/glomp_arrow_red.jpg" />
                                                                                        <a href="<?php echo $base_url . 'profile/view/' . $to_id; ?>"><img src="<?php echo $to_name_photo; ?>" alt="<?php echo $to_name; ?>" class="face" /></a>
                                                                                    </div>
                                                                                </div> 

            <?php
            
        }
        /*foeach close*/
    }
    else
    {
        echo '';
    }
}
?>