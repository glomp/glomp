<?php 
if($activity->num_rows()>0)
{
    foreach($activity->result() as $rec)
    {
        $start_count++;
        $details = json_decode($rec->details);
        $user_id=$this->session->userdata('user_id');
        switch($rec->log_type)
        {
            case 'glomp_someone':                                                                            
                $product_id=$details->product_id;
                $recipient_id=$details->recipient_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                $recipient_name =$info->friends->$recipient_id->user_name;
                $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            <span class="red"><b>glomp!</b>ed</span>&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                <b><?php echo $recipient_name;?></b>
                            </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $recipient_name;?>" />
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $recipient_photo;?>" alt="<?php echo $recipient_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'received_a_glomp':
                $product_id=$details->product_id;
                $glomper_id=$details->glomper_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$glomper_id));
                $glomper_name =$info->friends->$glomper_id->user_name;
                $recipient_photo = $this->custom_func->profile_pic($info->friends->$glomper_id->user_prifile_pic,$info->friends->$glomper_id->user_gender);
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>                                                                                        
                            <a href="<?php echo base_url('profile/view/'.$glomper_id);?>" class="red">
                                <b><?php echo $glomper_name;?></b>
                            </a> <span class="red"><b>glomp!</b>ed</span>&nbsp; a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $glomper_name;?>" />
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$glomper_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $recipient_photo;?>" alt="<?php echo $glomper_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'received_a_reward':                                                                            
                $product_id=$details->product_id;
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>    
                            <span class="red"><b>Rewarded</b></span>&nbsp;
                            a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?> from&nbsp;
                            <span class="red"><b>glomp!</b></span>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                        </div>
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo base_url('assets/images/glomp_icon.png');?>" />
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'redeemed_a_voucher':                                                                            
                $product_id=$details->product_id;
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>    
                            <span class="red"><b>Redeemed</b></span>&nbsp;
                            a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                        </div>
                        <div class="span6" style="padding:4px;">
                            <img style="width:100%;border-radius:8px;" src="<?php echo base_url('assets/images/glomp_icon.png');?>" />
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;                                                                        
            case 'friend_redeemed_a_voucher':
                
                $friend_id=$details->friend_id;
                
                
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                $friend_name =$info->friends->$friend_id->user_name;
                $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                
                $product_id=$details->product_id;
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;
                
                
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            Your friend&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a>&nbsp;
                            <span class="red"><b>redeemed</b></span>&nbsp;
                            a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">                                                                                        
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                            </a>
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            break;
            case 'added_a_friend': 
                                                                       
                $friend_id=$details->friend_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                $friend_name =$info->friends->$friend_id->user_name;
                $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>    
                            <span class="red"><b>Added</b></span>&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a> to your Friends List
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">                                                                                        
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'friend_added_you':                                                                            
                $friend_id=$details->friend_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                $friend_name =$info->friends->$friend_id->user_name;
                $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>                                                                                        
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a> <span class="red"><b>added</b></span>&nbsp;you to their Friends List
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">                                                                                        
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'unfriend_a_friend':                                                                            
                $friend_id=$details->friend_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                $friend_name =$info->friends->$friend_id->user_name;
                $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            <span class="red"><b>Removed</b></span>&nbsp;                                                                                    
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a> from your Friends List
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">                                                                                        
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'invited_a_friend':
                                                                                                                                                                        
                if($details->invited_through=='facebook')
                { ?>
                    <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                        <div class="span8" style="padding:10px 10px">
                            <div style="margin-bottom:10px;font-size:11px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div>
                                <span class="red"><b>Invited</b></span>&nbsp;                                                                                    
                                <a href="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>" target="_blank" class="red">
                                    <b><?php echo $details->invited_name;?></b>
                                </a> from Facebook to join <span class="red"><b>glomp</b>!ed</span>
                            </div>
                        </div>
                        <div class="span4" style="padding:4px 8px">
                            <div class="span6" style="padding:4px;">                                                                                        
                            </div>
                            <div class="span6" style="padding:4px;">
                                <a href="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>" target="_blank" class="red">
                                    <img style="width:100%;border-radius:8px;" src="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>/picture?width=140&height=140" alt="<?php echo $details->invited_name;?>" />
                                </a>
                            </div>                                                               
                        </div>
                    </div>
                
                <?php
                }
                else if($details->invited_through=='linkedIn')
                { ?>
                    <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                        <div class="span8" style="padding:10px 10px">
                            <div style="margin-bottom:10px;font-size:11px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div>
                                <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                    <b><?php echo $details->invited_name;?></b>
                                from LinkedIn to join <span class="red"><b>glomp</b>!ed</span>
                            </div>
                        </div>
                        <div class="span4" style="padding:4px 8px">
                            <div class="span6" style="padding:4px;">                                                                                        
                            </div>
                            <div class="span6" style="padding:4px;">
                                
                            </div>                                                               
                        </div>
                    </div>
                
                <?php
                
                }
                else if($details->invited_through=='email')
                { ?>
                    <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                        <div class="span8" style="padding:10px 10px">
                            <div style="margin-bottom:10px;font-size:11px;">
                                <b>
                                <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                </b>
                            </div>
                            <div>
                                <span class="red"><b>Invited</b></span>&nbsp;                                                                                                                                                                                
                                    <b><?php echo $details->invited_name;?></b>
                                    &nbsp;through email to join <span class="red"><b>glomp</b>!ed</span>
                            </div>
                        </div>
                        <div class="span4" style="padding:4px 8px">
                            <div class="span6" style="padding:4px;">                                                                                        
                            </div>
                            <div class="span6" style="padding:4px;">
                                
                            </div>                                                               
                        </div>
                    </div>
                
                <?php
                
                }
            break;
            case 'friend_added_by_friend':
                
                $friend_id=$details->friend_id;
                $added_friend_id=$details->added_friend_id;
                
                $info = json_decode($this->users_m->friends_info_buzz($added_friend_id.','.$friend_id));
                $friend_name =$info->friends->$friend_id->user_name;
                $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                
                $added_friend_name =$info->friends->$added_friend_id->user_name;
                $added_friend_photo = $this->custom_func->profile_pic($info->friends->$added_friend_id->user_prifile_pic,$info->friends->$added_friend_id->user_gender);
                
                
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            Your friend&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <b><?php echo $friend_name;?></b>
                            </a>&nbsp;
                            <span class="red"><b>added</b></span>&nbsp;
                            your friend&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$added_friend_id);?>" class="red">
                                <b><?php echo $added_friend_name;?></b>
                            </a>
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span6" style="padding:4px;">                                                                                        
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                            </a>
                        </div>
                        <div class="span6" style="padding:4px;">
                            <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                <img style="width:100%;border-radius:8px;" src="<?php echo $added_friend_photo;?>" alt="<?php echo $added_friend_name;?>" />
                            </a>
                        </div>                                                               
                    </div>
                </div>
                <?php
                break;
            case 'points_purchased':                                                                            
                ?>
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            <span class="red"><b>Purchased</b></span>&nbsp;                                                                                        
                            <?php echo $details->points;?> points
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span12" style="padding:4px;" align="right">
                            <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                <?php echo $details->balance;?>
                            </div>                                                                                            
                        </div>                               
                    </div>
                </div>
                <?php
                break;
            case 'points_spent':                                                                            
                $product_id=$details->product_id;
                $recipient_id=$details->recipient_id;
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                $recipient_name =$info->friends->$recipient_id->user_name;
                $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                $points =((int) $details->points) *-1;
                if($points>0)
                {
                    $points =" + ".abs($points);
                }
                else
                {
                    $points =" - ".abs($points);
                }
                
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            <span class="red"><b>glomp!</b>ed</span>&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                <b><?php echo $recipient_name;?></b>
                            </a> a <?php echo $details->merchant_name;?> <?php echo $details->product_name;?>&nbsp;
                            (<?php echo $points;?>)
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span12" style="padding:4px;" align="right">
                            <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                <?php echo $details->balance;?>
                            </div>                                                                                            
                        </div>                               
                    </div>
                </div>
                <?php
                break;
            break;
            case 'points_reversed':                                                                            
                $voucher_id=$details->voucher_id;
                $voucher_data=$this->users_m->get_specific_voucher_details($voucher_id);
                $voucher_data=$voucher_data->row();
                
                
                $product_id     =$voucher_data->voucher_product_id;
                $recipient_id   =$voucher_data->voucher_belongs_usser_id;
                
                
                
                $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                $recipient_name =$info->friends->$recipient_id->user_name;
                $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                
                $prod_info = json_decode($this->product_m->productInfo($product_id));
                $prod_image= $prod_info->product->$product_id->prod_image;
                
                $merchant_name  =$prod_info->product->$product_id->merchant_name;
                $product_name   =$prod_info->product->$product_id->prod_name;
                $points =((int) $details->points) *-1;
                if($points>0)
                {
                    $points =" + ".abs($points);
                }
                else  if($points<0)
                {
                    $points =" - ".abs($points);
                }
                else
                {
                    $points =" ".abs($points);
                }
                
                ?>
                
                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                    <div class="span8" style="padding:10px 10px">
                        <div style="margin-bottom:10px;font-size:11px;">
                            <b>
                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                            </b>
                        </div>
                        <div>
                            <span class="red"><b>glomp!</b></span> of a&nbsp;
                            <?php echo $merchant_name;?> <?php echo $product_name;?>&nbsp;
                            was <span class="red"><b>unredeemed</b></span> by&nbsp;
                            <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                <b><?php echo $recipient_name;?></b>
                            </a>&nbsp;
                            (<?php echo $points;?>)
                        </div>
                    </div>
                    <div class="span4" style="padding:4px 8px">
                        <div class="span12" style="padding:4px;" align="right">
                            <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                <?php echo $details->balance;?>
                            </div>                                                                                            
                        </div>                               
                    </div>
                </div>
                <?php
                break;
            case 'points_rewarded':
                //print_r($details);
            break;
        }
    }
}
else
{
    echo '';
}
?>