<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('Buy_points');?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
     <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
<script src="assets/frontend/js/custom.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    var session_id="<?php echo $session_id;?>";
    var token="<?php echo $token;?>";
    var amount="<?php echo $amount;?>";
    var points="<?php echo $points;?>";        
    var retry="<?php echo $retry;?>";
    $.ajax({
        type: "POST",
        url:"<?php echo base_url();?>buyPoints/checkPoints/",
        data: "session_id="+session_id+"&token="+token+"&amount="+token,
        success: function(data){            
            var data = eval("("+data+")");
            if(data.status=='success')
            {
                $('#point_balance').html(data.points);
                $('#point_balance').fadeIn();
                $('#message_content').html('<p><h3>'+ $('#Payment_lang_10').text() +'</p>');
                $('#message_content').fadeIn();
                /*Your payment of $##.## USD for ### glomp! points is successful. Start glomp!ing now*/
            }
            else
            {
                if(retry=='') {
                    var here_text = $('#Payment_lang_5').text().match(/[^{\}]+(?=})/g);
                    var s_here_text = '<a href="<?php echo base_url('buyPoints');?>/get_checkout?token='+token+'&retry=1">'+ here_text[0] + '</a>';
                    $('#message_wrapper').html('<div class="content alert alert-error" align="center" style="margin:130px 0px;padding:30px 20px;font-size:14px" >'+ $('#Payment_lang_5').text().replace('{'+here_text[0]+'}', s_here_text) +'</div>');
                }
                else
                {
                    var here_text = $('#Payment_lang_8').text().match(/[^{\}]+(?=})/g);
                    var s_here_text = '<a href="<?php echo base_url();?>user/dashboard/" >'+ here_text[0] + '</a>';
                    
                    $('#message_wrapper').html('<div class="content alert alert-error" align="center" style="margin:130px 0px;padding:30px 20px;font-size:14px" >\
                    <p>'+ $('#Payment_lang_7').text() +'</p>\
                    <br><p>'+ $('#Payment_lang_8').text().replace('{'+here_text[0]+'}', s_here_text) +'</div>');                    
                    setTimeout(function() {
                        window.location.href = '<?php echo base_url();?>user/dashboard/';												 
                    }, 10000);
                }
            }
        }
    });
});
</script>
</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
<div class="outerBorder">
	<div class="inner-content">
      <div class="clearfix"></div>
      <div class="row-fluid">
      <div class="span12">
       <div class="middleBorder topPaddingZero" >
            <div class="buyPoints" >
                <div class="row-fluid">
                    <div class="span12" id="message_wrapper">
                        <div class="content alert alert-success" align="center" style="margin:130px 0px;padding:10px 10px;font-size:14px" id="message_content">
                            <span style="font-size:14px" ><?php echo $this->lang->line('Payment_lang_4', "Please wait, your payment is being processed." ); ?>&nbsp;&nbsp;</span>
                            <img src="<?php echo base_url('assets/images/ajax-loader-1.gif');?>" style="width:20px" />
                        </div>                        
                    </div>
                </div>        
            </div>
      </div>
      </div>
      </div>
      </div>
      </div>      
</div>
<div id ="gl_languages" style="display:none;">
    <span id="Payment_lang_10"><?php echo $this->lang->compound('Payment_lang_10', 
            array(
                'amount'   => '$'.$amount,//#TODO: DYNAMIC CURRENCY
                'number'   => $points,
            )
            , "Your payment of {amount} for {number} points is successful. Start glomp!ing now." ); ?>
    </span>
    <span id="Payment_lang_5"><?php echo $this->lang->line('Payment_lang_5', "Sorry for the delay. To continue, please click {here}." ); ?></span>
    <span id="Payment_lang_7"><?php echo $this->lang->line('Payment_lang_7', "There seems to be a connection problem with the payment processor. Your payment has been recorded and there's no need to make the purchase again. Please check back later." ); ?></span>
    <span id="Payment_lang_8"><?php echo $this->lang->line('Payment_lang_8', "If your page does not automatically redirect in 5 seconds, please click {here}" ); ?></span>
</div>
</body>
</html>