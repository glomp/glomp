<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $page_title;?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <link href="assets/frontend/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>

</head>
<body>
<?php include_once("includes/analyticstracking.php") ?>
<?php include('includes/header.php')?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="outerBorder">
      	<div class="register">
                    <div class="innerPageTitleRed"><?php echo $this->lang->line('update_password');?></div>
                         <div class="content">
               <?php echo form_open("landing/update_password/".$hash_key.'/'.$user_id,'name="login_form" class="form-horizontal" id="frmLogin"')?>
     <?php
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if(isset($error_msg))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
					   if(isset($success_msg))
				  {
					   echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$success_msg."</div>";
					  
					  }
					   ?>
     <div class="control-group">
                    <label class="control-label" for="new_pword"><?php echo $this->lang->line('New_password');?></label>
                    <div class="controls">
                    <input type="password"  id="new_pword" name="new_pword" class="span5" >
                    </div>
                    </div>
                    <div class="control-group">
                    <label class="control-label" for="confirm_pword"><?php echo $this->lang->line('Confirm_Password');?></label>
                    <div class="controls">
                    <input type="password" id="confirm_pword" name="confirm_pword" class="span5" >
                    </div>
                        </div>
    <div class="control-group">
    <button type="submit" name="update_password" class="btn-custom-primary"><?php echo $this->lang->line('update_password');?></button>
     <div class="clearfix"></div>
    </div>
    </form>
            </div>
                    </div>
       </div>
      
      </div>
      </div>
</div>
</body>
</html>