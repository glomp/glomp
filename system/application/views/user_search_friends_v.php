<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Search Friends | Glomp</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->  
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/nanoscroller.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>

        <script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script>
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var GLOMP_INVITE_REQUEST_SENT_BODY = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_BODY', 'Your glomp! invite request has been sent.'); ?>";
            var GLOMP_INVITE_REQUEST_SENT_TITLE = "<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_TITLE', 'Success!'); ?>";
            var GLOMP_MESSAGE_JOIN = "<?php echo $this->lang->line('GLOMP_MESSAGE_JOIN', 'Join glomp! Now!'); ?>";
            var GLOMP_FB_FRIENDS_LIST_TITLE = "<?php echo $this->lang->line('GLOMP_FB_FRIENDS_LIST_TITLE', 'Facebook Friends List'); ?>";
            var GLOMP_GETTING_FB_FRIENDS = "<?php echo $this->lang->line('GLOMP_GETTING_FB_FRIENDS', 'Getting you Facebook friends...'); ?>";
            var GLOMP_GETTING_LI_FRIENDS = "<?php echo $this->lang->line('GLOMP_GETTING_LI_FRIENDS', 'Sorry. Linkedin recently made changes to 3rd party access and therefore glomp! is temporarily unable to access your Linkedin connections. Please connect to your Linkedin connections via Facebook or email in the meantime. We will have this issue resolved soon.'); ?>";
            var GLOMP_PUBLIC_IMAGE_LOC = "<?php echo base_url("assets/images/"); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
            var GLOMP_FB_POPUP_TITLE_ADD = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_ADD', 'Add Friend'); ?>";
            var GLOMP_FB_POPUP_TITLE_INVITE = "<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_INVITE', 'Invite Friend'); ?>";
            var GLOMP_ADD_FRIEND_SUCCESS = "<?php echo $this->lang->line('GLOMP_ADD_FRIEND_SUCCESS', 'Friend added'); ?>";
            var GLOMP_FB_INVITE_PRE_MESSAGE = "<?php echo $this->lang->line('GLOMP_FB_INVITE_PRE_MESSAGE', 'Its time for a treat!<newline>glomp! is a a revolutionary new digital platform where you can give and receive enjoyable treats such as a coffee, ice-cream, a beer and so on from a host of quality retail and service outlets.<newline>Join <name> so you too can give and receive great treats. Click the icon below to get started now!'); ?>";
            var GLOMP_ASK_OPTION = "<?php echo $this->lang->line('GLOMP_ASK_OPTION', 'Would you like to'); ?>";
            var GLOMP_FB_INVITE_REMINDER = "<?php echo $this->lang->line('GLOMP_FB_INVITE_REMINDER', 'Please note: As Facebook only allows 10 friends to be invited in one notification, if you have invited more than 10 Facebook friends, there will be a multiple of notifications appearing on your Facebook Wall.'); ?>";
            var glomp_user_fname = "<?php echo $user_short_info->user_fname; ?>";
            var glomp_user_id = "<?php echo $user_short_info->user_id; ?>";
            var glomp_user_fb_id = "<?php echo $user_short_info->user_fb_id; ?>";
            var inTour = "<?php echo $inTour; ?>";
            var countries = <?php echo $countries_json; ?>;
            var linkedIn = false;
            <?php if (isset($_GET['getLinkedIn']) && $_GET['getLinkedIn'] == 'true') { ?>
            linkedIn = true;
            <?php } ?>
            function liInitOnload() {
                if (IN.User.isAuthorized()) {
                    //NO NEED TO DO ANYTHING
                } 
            }
        </script>
        <!-- the javascript file has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.glomp_share.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/custom.user_search_friends.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".searchUser").nanoScroller();
<?php
if (isset($getFb) && $getFb == 'true') {
    echo 'askFbOption();';
}
?>
<?php
if (isset($_GET['getLinkedIn']) && $_GET['getLinkedIn'] == 'true') {
    echo 'setTimeout(function() {
        askLinkedInOption();
    }, 1000)';
}
?>
<?php
$tweet_status = $this->session->flashdata('tweet_status');
$tweet_message = $this->session->flashdata('tweet_message');
if (!empty($tweet_status)) {
    echo "doTweetSucces('" . $tweet_status . "','" . $tweet_message . "');";
}
?>
            });
        </script>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
        <div id="fb-root"></div>		
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="middleBorder topPaddingZero">
                        <div class="row-fluid searchPageHeader" >
                            <h1 class="glompFont"><?php echo $this->lang->line('Friends', 'Friends'); ?></h1>
                        </div>

                        <div class="row-fluid">      
                            <div class="span7">

                                <div class="searchFriends">

                                    <form name="form1" method="post" action="" style="float:left;">
                                        <input type="text" class="fl" placeholder="<?php echo $this->lang->line('search_or_find_friend_placeholder', 'Search or Find Friend'); ?>" name="keywords" value="<?php echo $keywords ?>" id="searchTextField">
                                        <div class="fl" style="border:0px solid;margin-left:20px;display:none" id="fb_paginator_wrapper">
                                            <div class="fl" style="border:0px solid; ">
                                                <button type="button" id="page_next" disabled  class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px 2px !important;" name="" >&nbsp;&gt;&nbsp;</button>
                                                <button type="button" id="page_prev" disabled class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px  2px !important;" name="" >&nbsp;&lt;&nbsp;</button>
                                            </div>
                                        </div>
                                        <div class="fl" style="border:0px solid;margin-left:20px;display:none" id="linkedIn_paginator_wrapper">
                                            <div class="fl" style="border:0px solid; ">
                                                <button type="button" id="li_page_next" disabled  class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px 2px !important;" name="" >&nbsp;&gt;&nbsp;</button>
                                                <button type="button" id="li_page_prev" disabled class="fr btn-custom-gray-suppoetLink w50px" style="margin:0px  2px !important;" name="" >&nbsp;&lt;&nbsp;</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div style="border:0px solid #000000; float:right; color:#FFFFFF; margin-top:2px; display:none;" id="fb_invite_button_wrapper" >
                                        <button type="button" id="invite_fb_friends" class="btn-custom-primary" name="" ><?php echo $this->lang->line('invite', 'Invite'); ?></button>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="chk_select_all" ><label style="margin-top:6px; float:right">&nbsp;<?php echo $this->lang->line('Select_All', 'Select All'); ?></label>
                                    </div>
                                    <div style="border:0px solid #000000; float:right; color:#FFFFFF; margin-top:2px; display:none;" id="li_invite_button_wrapper" >
                                        <button type="button" id="invite_li_friends" class="btn-custom-primary" name="" ><?php echo $this->lang->line('invite', 'Invite'); ?></button>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="li_chk_select_all" ><label style="margin-top:6px; float:right">&nbsp;<?php echo $this->lang->line('Select_All', 'Select All'); ?></label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="searchUser nano" id="searchUserScroller">
                                        <div class="content" id="friendsList" style="">

<?php

function cmp($a, $b) {
    return strcmp($b->is_friend, $a->is_friend);
}

/* echo print_r($resFriendList); */
/* if(( !isset($getFb) || (isset($getFb) && $getFb!=true ) ) && $resFriendList->num_rows()>0) */
if ((!isset($getFb) || (isset($getFb) && $getFb != true ) ) && $resFriendList != "" && count($resFriendList) > 0) {
    ?>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <?php
                                                        /* usort($resFriendList, "cmp"); */
                                                        $sort = array();
                                                        foreach ($resFriendList as $k => $v) {
                                                            $sort['friend_data'][$k] = $v->friend_data;
                                                            $sort['friend_data'][$k] = $v->friend_data;
                                                            $sort['is_friend'][$k] = $v->is_friend;
                                                            $sort['mutual'][$k] = $v->mutual;
                                                        }
                                                        # sort by event_type desc and then title asc
                                                        array_multisort($sort['is_friend'], SORT_DESC, $sort['mutual'], SORT_DESC, $resFriendList);
                                                        /* print_r($resFriendList); */
                                                        /* 		  $resFriendList=sort */
                                                        $i = 1;
                                                        $j = 1;
                                                        foreach ($resFriendList as $recFriendList) {
                                                            $mutual = $recFriendList->mutual;
                                                            $is_friend = $recFriendList->is_friend;
                                                            $recFriendList = $recFriendList->friend_data;


                                                            if ($i == 1) {
                                                                ?><ul class="thumbnails"> <?php } ?>
                                                                <li class="span4">
                                                                    <div class="friendThumb">    
                                                                        <div class="thumbnail" style="position:relative">   
        <?php
        if ($is_friend == 0) {
            echo '
				<div title="Add ' . $recFriendList->user_name . ' to your glomp! network" id="' . $recFriendList->user_id . '"  class="addThisFriendClass" style="cursor:pointer;position:absolute;left:105px; margin-top:108px; z-index:5; background-color:#AACF37;  border-radius:4px; margin-right:2px" align="center">
					<img  id="add_' . $recFriendList->user_id . '"  src="' . base_url("assets/images/icons/glomp-add.png") . '" alt="Add Friend" style="height:25px;margin:2px;width:25px"/>
				</div>	
				';
        }
        ?>        
                                                                            <a href="<?php echo base_url("profile/view/" . $recFriendList->user_id); ?>" style="">
                                                                                <img id="data_img_<?php echo $recFriendList->user_id; ?>" src="<?php echo $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_name; ?>" >
                                                                                <div class="thumbUserName">
                                                                                    <span id="data_name_<?php echo $recFriendList->user_id; ?>" ><?php echo $recFriendList->user_name; ?></span><br />
                                                                                    <span id="data_loc_<?php echo $recFriendList->user_id; ?>" ><?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></span>
                                                                                </div>
                                                                            </a>          
                                                                        </div>
                                                                    </div>
                                                                </li>

        <?php if ($i == 3 || $j == count($resFriendList)) { ?></ul>
                                                                    <?php
                                                                }
                                                                $j++;
                                                                if ($i == 3) {
                                                                    $i = 1;
                                                                }
                                                                else
                                                                    $i++;
                                                            } /* / end of foreach($r->result() as $p) */
                                                            ?>
                                                    </div>
                                                </div>
                                                    <?php
                                                    }
                                                    else {
                                                        ?>

                                                <div class="alert alert-error">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <?php echo $this->lang->line('No_result_found', 'No result found'); ?>
                                                </div>

                                                <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span5">
<?php include('includes/invite_friends.php'); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id ="gl_languages" style="display:none;">
            <span id="invite_btn_search_friends"><?php echo $this->lang->line('invite', "Invite"); ?></span>
            <span id="cancel_btn_search_friends"><?php echo $this->lang->line('Cancel', "Cancel"); ?></span>
            <span id="notice_facebook_1"><?php echo $this->lang->line('notice_facebook_1', "We've noticed that you are logged into a different Facebook account than the one you have linked on glomp!. Would you like to login with the other account or to continue?"); ?></span>
            <span id="notice_facebook_btn_1"><?php echo $this->lang->line('notice_facebook_btn_1', "Login using your linked Facebook account"); ?></span>
            <span id="notice_facebook_btn_2"><?php echo $this->lang->line('Continue', "Continue"); ?></span>
            <span id="your_personal_message_placeholder"><?php echo $this->lang->line('your_personal_message_placeholder', "Your personal message"); ?></span>
            <span id="we_are_experiencing_a_loading"><?php echo $this->lang->line('we_are_experiencing_a_loading', "We are experiencing a loading delay from Facebook."); ?></span>
            <span id="please_wait"><?php echo $this->lang->line('Please_wait', "Please wait..."); ?></span>
            <span id="invite_sent"><?php echo $this->lang->line('Invite_sent', "Invite sent."); ?></span>
            <span id="Ok"><?php echo $this->lang->line('Ok', "Ok"); ?></span>
            <span id="Forgot_Password"><?php echo $this->lang->line('Forgot_Password', "Forgot Password"); ?></span>
            <span id="Password"><?php echo $this->lang->line('Password', "Password"); ?></span>
            <span id="getting_friends_location"><?php echo $this->lang->line('getting_friends_location', "Getting your friend's location..."); ?></span>
            <span id="LinkedIn_error_1"><?php echo $this->lang->line('LinkedIn_error_1', "An error occur, LinkedIn server is down. Please try again later."); ?></span>
            <span id="Message"><?php echo $this->lang->line('Message', "Message"); ?></span>
            <span id="glomp_confirmation_message"><?php echo $this->lang->line('glomp_confirmation_message', "Are you sure you dont want to include a message?"); ?></span>
            <span id="Add_message"><?php echo $this->lang->line('Add_message', "Add Message"); ?></span>
            <span id="No_thanks"><?php echo $this->lang->line('No_thanks', "No Thanks"); ?></span>
            <span id="Successfully_Glomped"><?php echo $this->lang->line('Successfully_Glomped', "Successfully glomp!ed"); ?></span>
            <span id="Confirm"><?php echo $this->lang->line('Confirm', "Confirm"); ?></span>
            <span id="add_to_glomp_network"><?php echo $this->lang->compound('add_to_glomp_network', array('username'   => '',), "Add [username] to your glomp! Network?" ); ?></span>
            <span id="Others"><?php echo $this->lang->line('others', "Others" ); ?></span>
            <span id="Others"><?php echo $this->lang->line('others', "Others" ); ?></span>
            <span id="No_result_found"><?php echo $this->lang->line('No_result_found', "No result found" ); ?></span>
            <span id="No_friends_selected"><?php echo $this->lang->line('No_friends_selected', "No friends selected." ); ?></span>
            <span id="an_error_occured"><?php echo $this->lang->line('an_error_occured', "An unknown error occurred." ); ?></span>
            <span id="hidden_location_msg"><?php echo $this->lang->line('hidden_location_msg', "has hidden their location in Facebook.%newline% %newline% Please enter their location here:" ); ?></span>
            <span id="Re_try"><?php echo $this->lang->line('Re_try', "Re-try" ); ?></span>
            <span id="notice_facebook_2"><?php echo $this->lang->line('notice_facebook_2', "We've noticed that you are logged into a facebook account that is already linked on glomp!. Would you like to login with another account?" ); ?></span>
            <span id="login_using_diff_fb_account_btn"><?php echo $this->lang->line('login_using_diff_fb_account_btn', "Login using different Facebook account" ); ?></span>
            <span id="havent_link_fb_msg"><?php echo $this->lang->line('havent_link_fb_msg', "Seems like you haven't link your Facebook account. %newline% Do you like to link it?" ); ?></span>
            <span id="Yes"><?php echo $this->lang->line('Yes', "Yes" ); ?></span>
            <span id="No"><?php echo $this->lang->line('No', "Yes" ); ?></span>
            <span id="add_more_friends"><?php echo $this->lang->line('add_more_friends', "Add more Friends?" ); ?></span>
            <span id="add_more"><?php echo $this->lang->line('add_more', "Add more" ); ?></span>
            <span id="Return_to_Tour"><?php echo $this->lang->line('Return_to_Tour', "Return to Tour" ); ?></span>
        </div>
    </body>
</html>