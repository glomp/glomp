<link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen"> 

<script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>	
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url() ?>assets/m/js/bootstrap.min.js"></script>	
<script src="<?php echo base_url() ?>assets/m/js/jquery-ui-1.10.3.custom.js"></script>    

<!-- ci-tpl=profile_menu_merchant -->
<div class="page">
    <div class="page-line">
        <div class="tabs-content">
            <div id="tab-1" class="news brand-content">
                <?php if(isset($sections) && is_array($sections)):?>
                <?php foreach($sections as $section):?>
                <div class="brand-section">
                    <div class="section-type-<?php echo strtolower($section->type);?> section-row">
                        <?php if($widgets && array_key_exists($section->id,$widgets)):?>
                        <?php foreach($widgets[$section->id] as $widget):?>
                        <div class="section-col">
                            <?php include(APPPATH . 'views/includes/widget_type_'.$widget->widget_type.'.php'); ?>
                        </div>
                        <?php endforeach;?>
                        <?php endif;?>
                    </div>
                </div>
                <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>
    &nbsp;
</div>


<style type="text/css">
    .tabs-menu ul {
        padding:0;
        margin:0;
    }
    .tabs-menu ul li {
        list-style-type:none;
        float:left;
        margin-left:3px;
    }
    /*.tabs-menu ul li a {
        display:block;
        padding:10px;
        text-align:center;
        font-size:1.2em;
        text-decoration:none;
        background:#5A606C;
        color:#ABB2B8;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
    }*/
    .tabs-menu ul li.selected a {
        display:none;
    }
    .tabs-menu ul li:first-child{
        float:left;
    }
    .tabs-menu ul li:nth-child(2){
        
    }
    .tabs-menu ul li:last-child{
        float:right;
    }

    .brand-page-header,
    .brand-page-logo,
    .brand-page-description {
        position: relative;
    }
    .brand-page-title {
        position:absolute;
        top:10%;
        right:0;
        background: #5A606C;
        -webkit-border-top-left-radius: 10px;
        -webkit-border-bottom-left-radius: 10px;
        -moz-border-radius-topleft: 10px;
        -moz-border-radius-bottomleft: 10px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        padding:10px 40px 10px 15px;
    }
    .brand-page-title h1 {
        margin:0;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
    }
    .brand-page-logo {
        padding: 0 20px;
        max-width: 100%;
    }
    .brand-page-logo img {
        /*width:100%;*/
    }
    .brand-page-description {
        padding: 0 20px;
        text-align: justify;
    }
    .brand-page-products-link {
        padding: 0 20px;
    }
    .brand-page-products-link a {
        background: #5A606C;
        padding:8px 20px;
        font-size: 1.5em;
        font-weight: bold;
        color:#eee;
        display: inline-block;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    .brand-page-products-link a:active {
        background: #777;
        text-decoration: none;
    }
    .page-line {
        margin-top:20px;
        position: relative;
    }
    .brand-section {
        margin-bottom:2px;
    }

    .widget-photostory h3,
    .widget-photostory p {
        margin:0;
    }
    .widget-photostory h3 {
        font-size:1.4em
    }
    .widget-photostory {
        font-size:1em;
        line-height: 1em;
    }
    .widget-photostory .widget-title,
    .widget-photostory .widget-photostory-context {
        padding:5px 10px 4px;
        color: #333;
    }
    .widget-photostory .widget-title {
        color: #eee;
        background: #5A606C;
    }
    .widget-photostory .widget-photostory-context {
        color: #333;
        background: #fff;
        padding: 5px 20px 2px;
    }
    .widget-photostory.grey .widget-title {
        color: #eee;
        background: #5A606C;
    }
    .widget-photostory.grey .widget-photostory-context {
        color: #333;
        background: #fff;
        padding: 10px 10px 5px;
    }
    .tri {
        width : 0;
        height : 0;
        border-left : 15px solid transparent;
        border-right : 15px solid transparent;
        border-top : 15px solid #AACA38;
        position : relative;
    }
    .tri .tri2 {
        width : 0;
        height : 0;
        border-left : 13px solid transparent;
        border-right : 13px solid transparent;
        border-top : 13px solid #5A606C;
        position : absolute;
        top : -15px;
        left : -13px;
    }
    .tri.up {
        border-bottom : 15px solid #AACA38;
        border-top : 0;
    }
    .tri.up .tri2 {
        border-bottom : 13px solid #5A606C;
        border-top : 0;
        top : 2px;
    }
    .tabs-menu .btn-custom-blue-grey_xs {
        margin:0 1px !important;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.tabs-menu a').on('click',function(e){
            e.preventDefault();
            jQuery('.tabs-content > div').hide();
            jQuery('.tabs-menu .selected').removeClass('selected');
            jQuery('.tabs-content '+jQuery(this).attr('href')).show();
            jQuery(this).parent().addClass('selected');
            
            if(jQuery(this).parent().attr('id') == 'tab-3') {
                jQuery('#tab-2').css({'float':'right'});
            } else {
                jQuery('#tab-2').css({'float':'left'});
            }
        });
        /* photostory widget */
        if( jQuery('.widget-photostory').length > 0 ) {
            var toggle = jQuery('<a href="#" class="widget-photostory-text-toggle">[+]</a>');
            toggle.css({'position':'absolute','top':'2px','right':'0','padding':'10px','color':'#eee'});
            jQuery('.widget-photostory .widget-title')
                .css({'position':'relative'})
                .append(toggle);
            jQuery('.widget-photostory .widget-photostory-context').hide();
            jQuery('.widget-photostory-text-toggle')
                .html('<div class="tri"><div class="tri2"></div></div>')
                .click(function(e){
                e.preventDefault();
                jQuery(this)
                    .find('.tri')
                        .toggleClass('up')
                    .parents('.widget-photostory')
                    .find('.widget-photostory-context')
                        .slideToggle(100);
            });
        }
    });
</script>