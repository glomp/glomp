<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <!-- Bootstrap -->
        <link href="<?php echo minify('assets/m/css/bootstrap.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/style.css', 'css', 'assets/m/css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo minify('assets/m/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/m/css/south-street'); ?>" rel="stylesheet" media="screen">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/m/js/jquery-2.0.3.min.js"></script>
    </head>
    <body style="background: white;" class="mobile" ci-tpl="brands_preview">
        <div class="global_wrapper mobile" >
            <style type="text/css">
                .brand-page-header,
                .brand-page-logo,
                .brand-page-description {
                    position: relative;
                }
                .brand-page-title {
                    position:absolute;
                    top:10%;
                    right:0;
                    background: #5A606C;
                    -webkit-border-top-left-radius: 10px;
                    -webkit-border-bottom-left-radius: 10px;
                    -moz-border-radius-topleft: 10px;
                    -moz-border-radius-bottomleft: 10px;
                    border-top-left-radius: 10px;
                    border-bottom-left-radius: 10px;
                    padding:10px 40px 10px 15px;
                }
                .brand-page-title h1 {
                    margin:0;
                    font-size: 1.5em;
                    font-weight: bold;
                    color:#eee;
                }
                .brand-page-logo {
                    padding: 0 20px;
                    max-width: 100%;
                }
                .brand-page-logo img {
                    width:100%;
                }
                .brand-page-description {
                    padding: 0 20px;
                    text-align: justify;
                }
                .brand-page-products-link {
                    padding: 0 20px;
                }
                .brand-page-products-link a {
                    background: #5A606C;
                    padding:8px 20px;
                    font-size: 1.5em;
                    font-weight: bold;
                    color:#eee;
                    display: inline-block;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .brand-page-products-link a:active {
                    background: #777;
                    text-decoration: none;
                }
                .brand-page .page-line {
                    margin-top:20px;
                    position: relative;
                }
                .brand-section {
                    margin-bottom:2px;
                }

                .widget-photostory h3,
                .widget-photostory p {
                    margin:0;
                }
                .widget-photostory h3 {
                    font-size:1.4em
                }
                .widget-photostory p {
                    font-size:0.8em;
                    font-weight: bold;
                    line-height: 1em;
                }
                .widget-photostory .widget-title,
                .widget-photostory .widget-photostory-context {
                    padding:5px 10px 4px;
                    color: #333;
                }
                .widget-photostory .widget-title {
                    color: #eee;
                    background: #5A606C;
                }
                .widget-photostory .widget-photostory-context {
                    color: #333;
                    background: #fff;
                    padding: 5px 20px 2px;
                }
                .widget-photostory.grey .widget-title {
                    color: #eee;
                    background: #5A606C;
                }
                .widget-photostory.grey .widget-photostory-context {
                    color: #333;
                    background: #fff;
                    padding: 10px 20px 5px;
                }
                .tri {
                    width : 0;
                    height : 0;
                    border-left : 15px solid transparent;
                    border-right : 15px solid transparent;
                    border-top : 15px solid #AACA38;
                    position : relative;
                }
                .tri .tri2 {
                    width : 0;
                    height : 0;
                    border-left : 13px solid transparent;
                    border-right : 13px solid transparent;
                    border-top : 13px solid #5A606C;
                    position : absolute;
                    top : -15px;
                    left : -13px;
                }
                .tri.up {
                    border-bottom : 15px solid #AACA38;
                    border-top : 0;
                }
                .tri.up .tri2 {
                    border-bottom : 13px solid #5A606C;
                    border-top : 0;
                    top : 2px;
                }

                .product-list:after {
                    content: " ";
                    display:block;
                    clear: left;
                }
                .product-list a.product {
                    width:47%;
                    margin-left: 2%;
                    float: left;
                    background: #ddd;
                    margin-bottom: 2%;
                }
                .product-list a.product img {
                    width:100%;
                    background: #222;
                    border:0;
                }
                .product-list a.product p {
                    margin:0;
                    padding:5px 10px;
                    color: #555;
                }
                div.product {
                }
                div.product .product-image {
                    text-align: center;
                }
                div.product .product-image img {
                    width: 96%;
                }
                div.product .product-info {
                    padding: 0 20px;
                }
                div.product .product-info h3 {
                    margin:0;
                    padding: 0.6em 0;
                    font-size:1.4em;
                }
                div.product-glomp {
                    padding: 20px;
                }
                div.product-glomp a {
                    display:block;
                    background: #A8CF3E;
                    padding: 5px;
                    text-align:center;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                }
                div.product-glomp a img {
                    height:2em;
                }
                .btn-back {
                    position:absolute;
                    top:35%;
                    right:1em;
                    padding:5px 20px;
                    background:#5A606C;
                    color:#eee;
                    font-weight:bold;
                    font-size: 1.2em;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                }
                div.product-merchants {

                }
                div.product-merchants a {
                    float: left;
                    width: 24%;
                    margin-left: 4%;
                    -webkit-border-radius: 0.5em;
                    -moz-border-radius: 0.5em;
                    border-radius: 0.5em;
                    -webkit-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    -moz-box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    box-shadow: 2px 2px 6px 0px rgba(26,26,26,1);
                    overflow: hidden;
                }
                div.merchants-list {
                    overflow: auto;
                }
                div.product-merchants a img {
                    width: 100%;
                }
                div.search-info {
                    margin-bottom: 20px;
                }
                div.search-info h4 {
                    font-size: 0.9em;
                    text-transform: uppercase;
                    margin:0;
                    padding: 5px;
                    background: #5A606C;
                    color: #eee;
                }
                div.search-info .search-data {
                    padding:5px;
                    background: #bfc4ce;
                }
                div.search-info .search-data:after {
                    content: "";
                    display:block;
                    clear:left;
                }
                div.search-info .search-data img {
                    float:left;
                    width: 25%;
                }
                div.search-info .search-data p {
                    float:left;
                    margin-left:5px;
                }
            </style>
            <div class="page brand-page">
                <?php if(isset($sections) && !isset($products)):?>
                <div class="page-line">
                    <div class="page-sections">
                        <?php if($sections):?>
                        <?php foreach($sections as $section):?>
                        <div class="brand-section">
                            <div class="section-type-<?php echo strtolower($section->type);?> section-row">
                                <?php if($widgets && array_key_exists($section->id,$widgets)):?>
                                <?php foreach($widgets[$section->id] as $widget):?>
                                <div class="section-col">
                                    <?php include(APPPATH.'views/includes/widget_type_'.$widget->widget_type.'.php'); ?>
                                </div>
                                <?php endforeach;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <script type="text/javascript">
                        jQuery('.widget-photostory-context').removeAttr('style');
                    </script>
                </div>
                <?php endif;?>
            </div>
            <!--body-->
            <div class="footer_wrapper"></div>
        </div> <!-- /global_wrapper -->
        <!-- /footer -->
    </body>
</html>
