<?php
/*this is only used for language translation*/
$this->lang->line('ago');
$this->lang->line('second');
$this->lang->line('minute');
$this->lang->line('hour');
$this->lang->line('day');
$this->lang->line('week');
$this->lang->line('month');
$this->lang->line('year');
$this->lang->line('decade');

if(!isset($user_start_tour))/*check for tour*/
	$user_start_tour='N';
	
$hasPromoResponse='Y';
$promo_prod_details='';
if($res_info_prod=="")
	$res_info_prod="''";
if($user_promo_code==""){
	$hasPromoResponse='N';
	$user_promo_code="''";
}
else
{
	$promo_res=($user_promo_code);
	
}	
	
$resReg = $this->regions_m->region_name_only();

$is_reactivated =  $this->session->userdata('reactivated');
if($is_reactivated=='true')
    $this->session->unset_userdata('reactivated');

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $page_title;?> | glomp!</title>
  <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  
  <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
  <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="favicon.ico" rel="shortcut icon">
  <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
  <link href="<?php echo minify('assets/frontend/css/nanoscroller.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
   
   <link href="assets/frontend/css/south-street/jquery-ui-1.10.3.custom.css" rel="stylesheet">
   
   
   
   
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
<script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
<script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>


<script type="text/javascript">
<?php 
if($resReg->num_rows()>0)
{
$reg_list = "";
	foreach($resReg->result() as $recReg){
		$reg_list .= '"'.$recReg->region_name.'"'.",";
	}
	$reg_list =  rtrim($reg_list,",")
?>
var all_location_array = [<?php echo strtolower($reg_list);?>];

<?php 
}
?>
</script>
<script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>

<script src="assets/frontend/js/jquery-ui-1.10.3.custom.js"></script>
<script>
	var FB_APP_ID													="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
	var GLOMP_INVITE_REQUEST_SENT_BODY 	="<?php echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_BODY');?>";
	var GLOMP_INVITE_REQUEST_SENT_TITLE 	="<?php	 echo $this->lang->line('GLOMP_INVITE_REQUEST_SENT_TITLE');?>";
	var GLOMP_MESSAGE_JOIN 							="<?php echo $this->lang->line('GLOMP_MESSAGE_JOIN');?>";
	var GLOMP_INVITE_FB_FRIENDS					="<?php echo $this->lang->line('GLOMP_INVITE_FB_FRIENDS');?>";
	var GLOMP_TOUR_GET_STARTED					="<?php echo $this->lang->line('GLOMP_TOUR_GET_STARTED');?>";
	var GLOMP_TOUR_START								="<?php echo $this->lang->line('GLOMP_TOUR_START');?>";
	var GLOMP_TOUR_MAIN_DESC								="<?php echo $this->lang->line('GLOMP_TOUR_MAIN_DESC');?>";	
	var GLOMP_TOUR_DESC_1								="<?php echo $this->lang->line('GLOMP_TOUR_DESC_1');?>";	
	var GLOMP_TOUR_DESC_2								="<?php echo $this->lang->line('GLOMP_TOUR_DESC_2');?>";	
	var GLOMP_TOUR_DESC_3								="<?php echo $this->lang->line('GLOMP_TOUR_DESC_3');?>";	
	
	var GLOMP_BASE_URL									="<?php echo base_url('');?>";
	var GLOMP_SITE_URL									="<?php echo site_url();?>";
	var GLOMP_START_TOUR								="<?php echo $user_start_tour;?>";
	var GLOMP_GETTING_FB_FRIENDS				="<?php echo $this->lang->line('GLOMP_GETTING_FB_FRIENDS');?>";
	var GLOMP_PUBLIC_IMAGE_LOC					="<?php echo base_url("assets/images/");?>";
	var GLOMP_BASE_URL									="<?php echo base_url('');?>";
	var GLOMP_FB_POPUP_TITLE_ADD				="<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_ADD');?>";
	var GLOMP_FB_POPUP_TITLE_INVITE			="<?php echo $this->lang->line('GLOMP_FB_POPUP_TITLE_INVITE');?>";
	var GLOMP_ADD_FRIEND_SUCCESS			="<?php echo $this->lang->line('GLOMP_ADD_FRIEND_SUCCESS');?>";
	var inTour="<?php echo $inTour;?>";
	var is_connected_to_fb ="<?php echo $is_connected_to_fb;?>";
    var is_reactivated ="<?php echo $is_reactivated;?>";

	
		
	var GLOMP_PC_RESPONSE						="<?php echo $hasPromoResponse;?>";
	var GLOMP_PC_DATA							=<?php echo json_encode($user_promo_code);?>;	
	var GLOMP_PC_DATA_PROD						=<?php echo json_encode($res_info_prod);?>;		
        var GLOMP_PROMO_RULE_DESC					="<?php echo $promo_rule_desc;?>";
	var PROMO_CODE_DESC_INVALID					="<?php echo $this->lang->line('PROMO_CODE_DESC_INVALID');?>";
	var PROMO_CODE_DESC_INVALID_2				="<?php echo $this->lang->line('PROMO_CODE_DESC_INVALID_2');?>";
	var PROMO_CODE_DESC_ENDED_ALREADY			="<?php echo $this->lang->line('PROMO_CODE_DESC_ENDED_ALREADY');?>";
	var PROMO_CODE_DESC_FOR_MEMBER_ONLY			="<?php echo $this->lang->line('PROMO_CODE_DESC_FOR_MEMBER_ONLY');?>";
	var PROMO_CODE_DESC_NO_VOUCHER_LEFT			="<?php echo $this->lang->line('PROMO_CODE_DESC_NO_VOUCHER_LEFT');?>";
	var PROMO_CODE_DESC_SUCCESS_PRIMARY			="<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_PRIMARY');?>";
	var PROMO_CODE_DESC_SUCCESS_ASSIGNABLE			="<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_ASSIGNABLE');?>";
	var PROMO_CODE_DESC_SUCCESS_FOLLOW_UP       ="<?php echo $this->lang->line('PROMO_CODE_DESC_SUCCESS_FOLLOW_UP');?>";
        var NOT_CLAIM_MESSAGE                       ="<?php echo $not_claim_message; ?>";
	
</script>
<!-- the javascript file has been moved the below file-->
<script src="<?php echo minify('assets/frontend/js/custom.user.dashboard.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
<script src="<?php echo minify('assets/frontend/js/custom.glomp_share.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function(e) {
    <?php
        $tweet_status = $this->session->flashdata('tweet_status');
        $tweet_message = $this->session->flashdata('tweet_message');        
          if (! empty($tweet_status)) {
              echo "doTweetSucces('".$tweet_status."','".$tweet_message."');";
          }
    
    ?>	  
  
	$('.reAssign').click(function(){
                form = $(this).parent().parent();
                $(form).submit();
                return;
		$("#showError").hide();
		$("#showSuccess").hide();
		var _this = $(this);
		$('.glompItemPopUp').fadeIn();
		var _voucher_id = _this.data('voucherid');	
		$("#_voucher_id").val(_voucher_id);
		});
		/* send glomp to friend*/
		

	
	$('._send_glomp').click(function(){
		var msg = $('#glomp_user_message').val();
		if(msg == "")
		{
			if(!confirm("<?php echo $this->lang->line('glomp_confirmation_message');?>?"))
			{
				$('#glomp_user_message').focus();
				return false;
			}
		}
		$('#cancel_glomp_option').addClass('disabled');
		$('#cancel_glomp_option').removeClass('_close_popup');
		$(".Loader").show();
		$("#showError").hide();
		$("#showSuccess").hide();
		$.ajax({
					type: "POST",
					url:"profile/reassignVoucher/",
					data: $('#frmSendVoucher').serialize(),
					success: function(data){
						var d = eval("("+data+")");
						if(d.status=='success')
						{
							$("#showSuccess").show();
							$("#showSuccessMessage").text(d.msg);
							$('.glompItemPopUp').fadeOut(5500);
							location.href="<?php echo base_url();?>";
						}
						else if(d.status=='error')
							{
							$(".Loader").hide();
							$("#showError").show();
							$("#showErrorMessage").text(d.msg);
							}
					}
				});
		});  
});

function checkIfAllIsBlank(){
	var errCtr=0;
	if($('#location_id').val()==0)
	{
		$('#location').addClass("on_error");
		errCtr++;
	}
	if($('#i_name').val()==0)
	{
		$('#i_name').addClass("on_error");
		errCtr++;
	}
	if($('#i_email').val()==0)
	{
		$('#i_email').addClass("on_error");
		errCtr++;
	}
	
	if(errCtr>0)
	{		
		var _s="";
		if(errCtr>1)
			_s="s";
		var NewDialog = $('<div id="succesPopup" align="center">\
				<div align="center" style="margin-top:5px;font-size:12px;">Please complete the missing field'+_s+'.</div>\
				<div style="padding:20px 0px;" id="button_wrapper_alert" > \
				</div> \
		</div>');																
		NewDialog.dialog({						
			autoOpen: false,
			closeOnEscape: false ,
			resizable: false,					
			dialogClass:'dialog_style_blue_grey noTitleStuff',
			title:'Please wait...',
			modal: true,
			position: 'center',
			width:250,
			height:110
		});
		$elem = $('<button type="button" class="btn-custom-white2"  style="margin:0px 2px !important;width:100px" >Ok</button>');
		$elem.on('click', function(e) {
			$('#succesPopup').dialog("close");
			setTimeout(function() {
				$('#succesPopup').dialog('destroy').remove();
			}, 500);
			
			$('#i_name').removeClass("on_error");
			$('#i_email').removeClass("on_error");
			$('#location').removeClass("on_error");

		});
		$("#button_wrapper_alert").append($elem);
		NewDialog.dialog('open');
		return false;
	}
	else{		
		return true;
	}

}
</script>
<style>
.on_error{
	border:1px #b94a48 solid !important;
	border-color: #b94a48 !important;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075) !important;
	-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075) !important;
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075) !important;
}	
</style>
<div class="glompItemPopUp">

<div class="row-fluid">
 <div class="span12">
 
 <div class="row-fluid">
 
 <div class="span12" >
 <div class="alert alert-success"  id="showSuccess" style="display:none; margin-top:10px; margin-bottom:-5px;">
    
   <span id="showSuccessMessage"></span>
    </div>
    <div class="alert alert-error"  id="showError" style="display:none;margin-top:10px; margin-bottom:-5px;">
  
   <span id="showErrorMessage"></span>
    </div>
    
<h2 class="text-center"><?php echo $this->lang->line('Reassign_Voucher');?></h2>
 </div>
 
 </div>

   <form name="frmSendVoucher" id="frmSendVoucher" method="post" action="">
     
     <textarea name="message" id="glomp_user_message" placeholder="<?php echo $this->lang->line('Message');?>" class="span12"></textarea>
     <br /> <br />
     <input type="password" name="password"  id="password" placeholder=" <?php echo $this->lang->line('Your_Password');?>" class="span12">
      <div class="forgotPassword"><a href="<?php echo base_url('landing/reset_password');?>"><?php echo $this->lang->line('Forgot_Password');?> ?</a></div>
     
     <input type="text" name="fname"  id="fname" class="span12" placeholder=" <?php echo $this->lang->line('Friend_First_Name');?> ">
     
     <input type="text" name="lname"  id="lname" class="span12" placeholder="<?php echo $this->lang->line('Friend_Last_Name');?> ">
      
     <input type="text" name="email"  id="email" class="span12" placeholder="<?php echo $this->lang->line('Friend_Email');?> ">
     <input type="hidden" name="voucger_id"  id="_voucher_id" class="span12">
     <div class="text-center" style="padding-top:5px;">
    
    <span class="Loader" style="display:none;"><i class="icon-spinner icon-spin icon-large"></i></span>  <button type="button" class="btn-custom-gray _send_glomp" data-loading-text="loading stuff..." name="Confirm" ><?php echo $this->lang->line('Confirm');?></button>
    <button type="button" id="cancel_glomp_option" class="btn-custom-white  _close_popup" name="Cancel" style="margin-left:100px;" ><?php echo $this->lang->line('Cancel');?></button>  
    
   </div>
   </form>
 </div>
</div>
</div>

</head>
<body>
    
<!-- Walk through dialogs -->

<div id ="wt_overlay" class="hide" style="position: fixed;width: 100%;height: 100%;background-color:rgba(0,0,0,0.5);z-index:100"></div>

<div id="how_to_glomp_diag" style="display:none; font-size: 11px; padding: 10px;">
    <div style="position: absolute;">
        <div class="circle_with_text" style="background-color: #CB128B;position: relative;top: -29px;left: -29px;font-size: 18px;">1</div>
    </div>
    <?php echo $this->lang->line('wt_desk_how_to_1', '<div>Choose a friend you want to <span style="font-weight: bold">glomp!</span></div>
    <br />
    <div>Either select a fellow <span style="font-weight: bold">glomp!</span> friend or invite one via email or Facebook.</div>
    <br />'); ?>
    
    <div id ="wt_search_friends" class="btn-custom-magenta" style="padding: 10px; margin-bottom: 5px; cursor: pointer;"><?php echo $this->lang->line('wt_desk_how_to_2', 'Search for Friends'); ?></div>
    <div id ="wt_invite_friends" class="btn-custom-orange_F37A20" style="padding: 10px; margin-bottom: 5px; cursor: pointer;"><?php echo $this->lang->line('wt_desk_how_to_3', 'Invite via Email'); ?></div>
    <div id ="wt_invite_fb" class="btn-custom-blue_8ED3" style="padding: 10px; cursor: pointer;"><?php echo $this->lang->line('wt_desk_how_to_4', 'Invite from Facebook'); ?></div>
</div>

<div class="hide" id ="wt_frame_outer" style="position:relative; width: 960px; margin: 0 auto; z-index: 101">
    <div id ="wt_frame_inner" style="position:absolute;width: 960px;margin: 0 auto; top: 124px;">
        <div align="center">
            <img id="wt_frame_loader" src="<?php echo base_url('assets/images/ajax-loader-1.gif'); ?>" />
            <div align="center" id="wt_frame" style="height:500px">
                <!--<img id="wt_frame_img" src="" >-->
            </div>
            <img src="<?php /*echo base_url('assets/frontend/img/Glomp_WebWT_glomp_friend_F1.png')*/ ?>" />
        </div>
        <div id="wt_arrow_wrapper" style="position:relative; width: 100%;">
            <div id="arrow_prev_wrapper" class="fl pointer hide" style="position:relative;top: -285px; left: 55px;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_prev fl"></div>
                <div class="fl" style="font-size:9px;margin-top:17px;">&nbsp;PREV</div>
            </div>
            <div id="arrow_next_wrapper" class="fr pointer hide" style="position:relative;top: -300px;left: -42px;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_next fr"></div>
                <div class="fl" style="font-size:9px;margin-top:17px;">&nbsp;NEXT</div>
            </div>
            <div id="arrow_close_wrapper" class="fr pointer hide" style="position:relative;top: -300px;left: -42px;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_close fr"></div>
            </div>
        </div>
    </div>
</div>

<div class="hide" id ="wt_frame_outer_2" style="position:relative; width: 300px; margin: 0 auto; z-index: 101;border:solid 1px;">
    <div id ="wt_frame_inner_2" style="position:absolute;width: 300px;border-radius:8px;height:480px;background-color:#59606B;margin: 0 auto; top: 124px;border:solid 0px #f00;">
        <div style="margin:40px 30px 20px 30px;background-color:#fff;width:240px;height:360px;overflow:hidden;box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.5) inset;" align="center">
            <img id="wt_frame_loader_2" src="<?php echo base_url('assets/images/ajax-loader-1.gif'); ?>" />
            <div id="wt_frame_2">
            	<!--<img id="wt_frame_2" src="" style="width:240px">-->
			</div>            
        </div>
        <div style="border:0px solid #f00;height:0px;overflow:visible" align="center">
            <div style="border-radius:100%;width:30px;height:30px;background-color:#D4D8E2;margin-top:8px;">
            </div>
        </div>
        <div class="cl "style="border:0px solid #f00;" align="center">
            <div id="arrow_prev_wrapper_2" class="fl pointer" style="margin-left:30px;background-color:#BB1E23;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_prev fl" ></div>
                <div class="fl" style="font-size:9px;margin-top:14px;">&nbsp;PREV</div>
            </div>
            
            <div id="arrow_next_wrapper_2" class="fr pointer" style="margin-right:30px;background-color:#BB1E23;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_next fr" ></div>
                <div class="fr" style="font-size:9px;margin-top:14px;">NEXT&nbsp;</div>
            </div>
            
            <div id="arrow_close_wrapper_2" class="fr pointer" style="margin-right:30px;background-color:#BB1E23;color:#fff;width:45px;height:45px;border-radius:100%;">
                <div class="arrow_close fr" ></div>                
            </div>
        </div>
    </div>
</div>
<!-- Walk through dialogs end -->    
    
<?php include_once("includes/analyticstracking.php") ?>
<div id="fb-root"></div>
<?php include('includes/header.php')?>
<div class="container">
<div class="outerBorder">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="span12">
  	<div class="searchBox">
    <div class="search" style="width: 243px;">
        <form name="frmSearchFriend" method="post" action="<?php echo base_url('user/searchFriends');?>">
        <input  style="width: 224px;" type="text" name="keywords" value="" id="keywords" class="span12" placeholder="<?php echo $this->lang->line('search_my_friends_placeholder', 'Search Friends'); ?>" >
        <button style="width: 224px;" type="submit" class="btn-custom-green btn-block " name="Invite" ><?php echo $this->lang->line('search_my_friends', 'Friends'); ?></button>         
        <button style="width: 112px; margin-right: 2px;" type="button" class="fl btn-custom-blue btn-block " name="Invite_Facebook" id="Invite_Facebook" ><?php echo $this->lang->line('Facebook', 'Facebook'); ?></button>
        <button style="width: 112px;" type="button" class="fl btn-custom-007BB6 btn-block " name="Invite_LinkedIn" id="Invite_LinkedIn" ><?php echo $this->lang->line('LinkedIn', 'LinkedIn'); ?></button>
        <div style="margin-top: 4px;margin-left: 4px;" class="fl info_tooltip" rel="popover" data-content="<?php echo $this->lang->line('help_dashboard_fb');?> ">
            <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
        </div>
        <div class="clearfix"></div>
        </form>
        </div>
      <div class="face" style="width: 276px;">
     <?php
	 foreach($random_friends->result() as $random_row){
		 $pic = $this->custom_func->profile_pic($random_row->user_profile_pic,$random_row->user_gender);
		 $friend_pic = '<img src="'.$pic.'"  alt="'.$random_row->user_name.'"/>';
		 echo anchor('profile/view/'.$random_row->user_id,$friend_pic); 
	 }
	 ?>
      </div>
       <div class="inviteFriends" id="tour_3_location" style="float:right;margin-right:13px;">
      <span class="glompFont fl" style="font-size:22px;"><?php echo ucfirst($this->lang->line('invite_friend'));?></span>
      <div style="margin-left: 2px;margin-top: -3px;" class="fl info_tooltip" rel="popover" data-content="<?php echo $this->lang->line('help_dashboard_invite'); ?>" >
          <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
      </div>
      <div class="clearfix"></div>
      <form method="post" id="" name="" action="<?php echo base_url('user/searchFriends');?>"  onSubmit="return checkIfAllIsBlank()">
      <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr>
          <td><?php echo $this->lang->line('Name');?></td>
          <td><input type="text" name="name" id="i_name"></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><?php echo $this->lang->line('Email');?></td>
          <td><input type="text" name="email" id="i_email"></td>
          <td >
              <button type="button" id="invite_btn" class="btn-custom-darkblue" name="invite_by_email" style="width: 87px!important;margin-left: 2px;padding: 2px !important; margin-left:5px !important;" ><?php echo $this->lang->line('invite'); ?></button>
          </td>
        </tr>
        <tr>
          <td><?php echo $this->lang->line('Location');?></td>
          <td><input type="text" name="location" id="location" autocomplete="off"> 
           <div class="sutoSugSearch">
  </div>
          <input type="hidden" name="location_id" id="location_id" value="0" autocomplete="off" class="span12"></td>
          <td >
              <button value="invite_glomp" type="submit" style="background-color:#d2d7e1; border:0px;width: 87px; padding:0px; border:solid 0px; margin-left:5px" name="invite_glomp" ><img src="assets/frontend/img/glompSubmitButton.png" style="border:0px; width:87px" width="87" /></button>
          </td>
        </tr>
      </table>
      </form>
       </div>
       <div class="clearfix"></div>
    </div>
      </div>
      </div>
      
     <?php  
        $u = $this->users_m->user_info_by_id($this->session->userdata('user_id'));
        if ($u->num_rows() > 0) {
          if ($u->row()->user_email_verified  == 'N') { ?>
        <div class="alert alert-error" style="margin-bottom: 0px;border-bottom-width: 0px;">
             Your email needs to be verified. Please check your welcome email.
        </div>
     <?php
          }
        }
        $verified = $this->session->flashdata('verified');
        if (! empty($verified) ) {
     ?>
        <div style="margin-bottom: 0px;border-bottom-width: 0px;" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $verified; ?></div>
      <?php } ?>
        <div class="glomp_dashboard helvetica_font">
            <div class="fl" style="font-size: 31px;color: white;padding: 25px 30px 20px 30px;"><?php echo $this->lang->line('wt_desk_dashboard_1', 'Do you have to:'); ?></div>
            <div class="fl" style="width: 633px;">
                <div class="fl">
                    <div class="glomp_dashboard_menu" style="background-color: #00827A;"><?php echo $this->lang->line('wt_desk_dashboard_2', 'Thank a friend?'); ?></div>
                    <div class="glomp_dashboard_menu" style="width: 4px;"></div>
                    <div class="glomp_dashboard_menu" style="background-color: #0054A6;"><?php echo $this->lang->line('wt_desk_dashboard_3', 'Cheer a buddy up?'); ?></div>
                    <div class="glomp_dashboard_menu" style="width: 4px;"></div>
                    <div class="glomp_dashboard_menu" style="background-color: #6F8BA8;"><?php echo $this->lang->line('wt_desk_dashboard_4', 'Congratulate Someone?'); ?></div>
                    <div class="glomp_dashboard_menu" style="width: 4px;"></div>
                    <div class="glomp_dashboard_menu" style="background-color: #ED1D24;"><?php echo $this->lang->line('wt_desk_dashboard_5', 'Send a birthday wish?'); ?></div>
                    <div class="glomp_dashboard_menu" style="width: 4px;"></div>
                    <div class="glomp_dashboard_menu" style="background-color: #B2D235;"><?php echo $this->lang->line('wt_desk_dashboard_6', 'Impress anyone?'); ?></div>
                    <div class="glomp_dashboard_menu" style="width: 4px;"></div>
                    <div class="glomp_dashboard_menu" style="background-color: #D4D8E2;"><?php echo $this->lang->line('wt_desk_dashboard_7', 'Touch base with a client?'); ?></div>
                    <div class="cl"></div>
                </div>
            </div>
            <div class="cl"></div>
        </div>
      
      <div class="dashBoard">
        <h1>&nbsp;</h1>
        
        <div id="glomp_walkthrough_menu" class="helvetica_font">
            <div id="how_to_close" class="fr" style="cursor: pointer;padding: 3px; background-color: #D4D8E2">x</div>
            <div class="cl"></div>
            <div class="fl" style="min-height: 61px;width: 509px;padding: 10px;">
                <div style="margin: 0 auto;width: 300px;" align="center">
                    <div class="btn-custom-magenta" id="how_to_glomp" style="cursor: pointer;" ><?php echo $this->lang->line('wt_desk_how_to', 'How to glomp! (treat) a friend'); ?></div>
                    <br />
                    <div class="btn-custom-blue_8ED3" id="how_to_redeem" style="cursor: pointer"><?php echo $this->lang->line('wt_desk_redeem', 'How to redeem a voucher'); ?></div>
                </div>
            </div>
            <div class="cl"></div>
        </div>
        
        <div class="buzzScroll nano" id="glomp_buzz_nano">
        
         <?php $bg = ($glom_buzz_count==0)?"#d3d7e1":"#fff"; ?>
         
        <div class="content" id="glomp_buzz_content" style=" background:<?php echo $bg;?>">        
        <?php if($glom_buzz_count>0){
		$base_url = base_url();
		$to_a = $this->lang->line('to_a');        
        $start_count=0;
		foreach($glomp_buzz->result() as $glomp)
		{
            
            
            
			$from_id = $glomp->voucher_purchaser_user_id;
			$to_id = $glomp->voucher_belongs_usser_id;
			$frn_info = json_decode($this->users_m->friends_info_buzz($from_id.','.$to_id));
			/*print_r($frn_info);*/
			$ago_date = $glomp->voucher_purchased_date;
		
		$from_name = $frn_info->friends->$from_id->user_name;
		$from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic,$frn_info->friends->$from_id->user_gender);
        
        $from_fb_id = $frn_info->friends->$from_id->user_fb_id;
        $to_fb_id = $frn_info->friends->$to_id->user_fb_id;
        
        $from_li_id = $frn_info->friends->$to_id->user_linkedin_id;
        $to_li_id = $frn_info->friends->$to_id->user_linkedin_id;
        
		
		$to_name = $frn_info->friends->$to_id->user_name;
		$to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic,$frn_info->friends->$to_id->user_gender);
		/*product and merchant info*/
		$prod_id = $glomp->voucher_product_id;
		$prod_info = json_decode($this->product_m->productInfo($prod_id));
        $prod_image= $prod_info->product->$prod_id->prod_image;        
        $product_logo = $this->custom_func->product_logo($prod_image);
		$prod_name = $prod_info->product->$prod_id->prod_name;
		$merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
		$merchant_name = $prod_info->product->$prod_id->merchant_name;
        $merchant_id = $prod_info->product->$prod_id->merchant_id;
        
        $story_type="1";
        if($from_id!=$this->session->userdata('user_id'))
        {
            $story_type="2";
        }
		?>
        <div class="row-fluid" style="margin-bottom:20px;" data-id="<?php echo $start_count;?>">        
             <div class="span7" style="font-weight:bold;border:solid 0px;">
                <div class="row-fluid">
                    <div class="span10">

                        <?php

                            $sender = '<a href="'.base_url().'profile/view/'.$from_id.'">'.$from_name.'</a>';
                            if( $from_id == 1 ) {
                                $sender = '<a href="javascript:void(0);">'.$from_name.'</a>';
                            }
                            echo $this->lang->compound(
                                'glomp_buzz',
                                array(
                                    'sender'   => $sender,
                                    'reciever' => '<a href="'.base_url().'profile/view/'.$to_id.'">'.$to_name.'</a>',
                                    'merchant' => $merchant_name,
                                    'product'  => $prod_name
                                ),
                                "{sender} <strong>glomp!ed</strong> {reciever} to a {merchant} {product}" );
                        ?>

                        <div class="time"><?php echo $this->custom_func->ago($ago_date);?></div>
                    </div>
                    <div class="span2" align="right" style="border:0px solid">
                        <div style="margin-top:28px;" >
                            <button type="button" id="" class="glomp_fb_share btn-custom-blue-grey_xs" data-voucher_id="<?php echo $glomp->voucher_id;?>" ><?php echo $this->lang->line('glomp_share', 'share'); ?></button>
                            <div style="height:0px;">
                                <div id="share_wrapper_box_<?php echo $glomp->voucher_id;?>" class="share_wrapper_box" data-voucher_id="<?php echo $glomp->voucher_id;?>" style="" align="center">
                                    <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $from_name;?>" title="Share on Facebook" style="background:url('<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                    <button data-merchant="<?php echo $merchant_name;?>"  data-belongs="<?php echo $to_name;?>" data-purchaser="<?php echo $from_name;?>" title="Share on LinkedIn" style="background:url('<?php echo base_url('assets/frontend/img/li_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet" data-from_fb_id="<?php echo $from_fb_id;?>" data-to_fb_id="<?php echo $to_fb_id;?>" data-story_type="<?php echo $story_type;?>" data-prod_name="<?php echo $prod_name;?>" data-prod_img="<?php echo $product_logo;?>"  data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                    <button  title="Share on Twitter"  style="background:url('<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>'); background-size: 21px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet" data-from="dashboard" data-voucher_id="<?php echo $glomp->voucher_id;?>"  ></button>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>                          
             </div>
         <div class="span5" style="text-align:right;border:solid 0px;">
         <?php if($from_id==1)
         {?>
         <img src="<?php echo $from_name_photo;?>" alt="<?php echo $from_name;?>" class="face" />
         <?php
         }
		 else
		 {
		 ?>
         <a href="<?php echo base_url('profile/view/'.$from_id);?>"><img src="<?php echo $from_name_photo;?>" alt="<?php echo $from_name;?>" class="face" /></a>
         
         <?php }?>
         
          <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px" />
          
            <a href="<?php echo base_url('profile/view/'.$to_id.'?tab=tbMerchant&merID='.$merchant_id);?>">
                <img src="<?php echo $merchant_logo;?>" class="merchant_logo" alt="<?php echo $merchant_name;?>" />
            </a>
    
           <img src="assets/frontend/img/glomp_arrow_red.jpg" style="margin:0px 5px" />
           
          
          <a href="<?php echo base_url('profile/view/'.$to_id);?>"><img src="<?php echo $to_name_photo;?>" alt="<?php echo $to_name;?>" class="face" /></a>
         </div>
         </div>
         <?php 
            $start_count++;
		}/*foeach close */
		 }/*num count close*/
		 else
		 {
			 if($this->session->userdata('user_id')== $buzz_user_id){
			 ?>
           
         <div class="no_more_buzz">
         <p><?php echo $this->lang->line('create_more_buzz_msg');?> </p> <br />
       
         <a href="<?php echo base_url('user/searchFriends');?>" class="btn-custom-primary-big"><?php echo $this->lang->line('Add_Friends');?></a>
         </div>
        
          <?php 
			 }
		  }?>
          
         </div>
         </div>
         </div>
      <div class="dashBoardRight">
       <h1>&nbsp;</h1>
       <div class="glompedPanel nano">
        <div class="content" id="tour_2_location">
         <?php if($glomped_me->num_rows()>0){
			 foreach($glomped_me->result() as $rec_glomped_me)
			 {
			 $prod_id = $rec_glomped_me->voucher_product_id;
			 $glomped_message = $rec_glomped_me->voucher_sender_glomp_message;
			 $glomped_by_name = $rec_glomped_me->user_name;
			$glomped_by_photo = $this->custom_func->profile_pic($rec_glomped_me->user_profile_pic,$rec_glomped_me->user_gender);
			 
		$glomped_me_prod = json_decode($this->product_m->productInfo($prod_id));
		$prod_name = $glomped_me_prod->product->$prod_id->prod_name;
		$product_logo = $this->custom_func->product_logo($glomped_me_prod->product->$prod_id->prod_image);
		$merchant_name = $glomped_me_prod->product->$prod_id->merchant_name;
		
		?>
       <div class="row-fluid btnBorder">
       	<div class="span33" style="" >
        <?php $messageCount = strlen($glomped_message);
		if($messageCount>70)
		{
		?>
        <div class="popUp nano" id="glomp_full_message_<?php echo $rec_glomped_me->voucher_id;?>_open">
        <div class="content" style="">
       <div class="pull-right close"> <a href="javascript:void(0);"><i class="icon-remove"></i> </a></div>       
                  <p><?php echo stripslashes($glomped_message);?></p>
              </div>
              </div>
              
              <?php }?>
            
        <div class="time" align="left" ><?php echo $this->custom_func->ago($rec_glomped_me->voucher_purchased_date);?> </div>
        <img src="<?php echo $product_logo;?>" alt="<?php echo $prod_name;?>" class="product" />
       <div class="merchantNProducts">
       <?php echo stripslashes($merchant_name);?><br/>
	   <?php echo stripslashes($prod_name); ?>
       </div>
       <div>
        </div>
        </div>
        <div class="span88" style="margin-top:2px !important">
        <div class="messageBox">
        <div class="row-fluid">
        <div class="span88">
        "<?php echo character_limiter(stripslashes($glomped_message),70);?>"</div>
      <div class="span33" align="center"> 
      <?php if($rec_glomped_me->voucher_purchaser_user_id==1)
	  {?>
      <img src="<?php echo $glomped_by_photo;?>" alt="<?php echo $glomped_by_name;?>" style="height:39px"  />
      <?php } else{?>
     <a href="<?php echo base_url('profile/view/'.$rec_glomped_me->voucher_purchaser_user_id);?>"> <img src="<?php echo $glomped_by_photo;?>" alt="<?php echo $glomped_by_name;?>" style="height:39px" /></a>
     <?php }?>
      </div>
       </div>
       <div class="row-fluid paddingTop5px">
        <div class="span88">
        <?php 
		if($messageCount>70)
		{
		?>
        <a href="javascript:void(0)" class="full_message" id="glomp_full_message_<?php echo $rec_glomped_me->voucher_id;?>" ><?php echo $this->lang->line('Full_Message');?></a>
        
        <?php }?>
        </div>
      <div class="span33" align="center"> 
      <div class="userName"><?php $temp=$glomped_by_name; echo str_replace(' ','<br>',$temp);?></div>
      </div>
       </div>
        </div>
       	<div style="color:#fff">
        <?php 
		$expired_msg = '';
		if($rec_glomped_me->voucher_status=="Assigned") {?>
        <form action="<?php echo site_url('user/giveVoucher'); ?>" method="POST">
            <input type="hidden" name="voucher_id" id="voucher_id" value="<?php echo $rec_glomped_me->voucher_id; ?>"/>
            <a href="javascript:void(0);" ><img src="assets/frontend/img/glomp_small_group_logo_re_assign.jpg" class="product reAssign" data-voucherid="<?php echo $rec_glomped_me->voucher_id;?>" /></a>
        </form>
         <?php 
		}
		else if($rec_glomped_me->voucher_type == 'Assignable' && $rec_glomped_me->voucher_status == 'Redeemed')
		{
			echo $this->lang->line('reward_voucher_gifted');/*Reward voucher has been gifted.*/
				
		}else if ($rec_glomped_me->voucher_type != 'Assignable'  && $rec_glomped_me->voucher_status == 'Redeemed')
		{
			echo $this->lang->line('voucher_alrady_reddemed_msg');/*This voucher is already redeemed*/
				
		}
		else if($rec_glomped_me->voucher_status == 'Expired')
		{
			$expired_msg =  $this->lang->line('sorry_voucher_expired_msg'); /*Sorry voucher has been expired.*/
				
		}
		 if($rec_glomped_me->voucher_purchaser_user_id!=1){?>
          &nbsp;&nbsp;<a href="<?php echo base_url('profile/view/'.$rec_glomped_me->voucher_purchaser_user_id);?>"><img src="assets/frontend/img/glomp_small_group_logo.jpg" class="product" /></a>
          <?php 
		 }
		  	echo $expired_msg;
		  ?>
          </div>
        </div>
       </div>
       <?php 
			 }/*foreach close of glomped me*/
	   } else  { 
	   ?>
       
       <div class="no_glomped" id="no_glomped">
     
      <p> <?php echo $this->lang->line('dashboard_no_glomped_msg') ?> </p>
       <br />
        <a href="<?php echo base_url('profile');?>" class="btn-custom-primary-big btn-block"><?php echo $this->lang->line('Add_Favorites_Now');?></a>
         <br /> <br />
         <p> <?php echo $this->lang->line('dashboard_no_glomped_msg_2') ?></p> <br /> <br />
          <a href="<?php echo site_url('user/searchFriends?showall=1'); ?>" class="btn-custom-primary-big btn-block"><?php echo $this->lang->line('Add_Friends');?></a> <br />
       </div>
       
       <?php }?>       
        </div>
        
        </div>
        </div>
       
      <div class="clearfix"></div>
     
      </div>
      </div>
    <div id="gl_languages" style="display:none;">
        <span id="wt_desk_redeem_1"><?php echo $this->lang->line('wt_desk_redeem_1', "Redemptions can only be made via your smartphone in the presence of the merchant outlet's staff. Would you like to learn how to redeem on your smartphone?"); ?></span>
        <span id="wt_desk_redeem_2"><?php echo $this->lang->line('wt_desk_redeem_2', "Yes, show me how"); ?></span>
        <span id="Loading_more_stories"><?php echo $this->lang->line('Loading_more_stories', "Loading more stories..."); ?></span>
    </div>
</div>
</body>
</html>
