<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $page_title; ?> | glomp!</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="description" content="" >
        <meta name="keywords" content="<?php echo meta_keywords();?>" >
        <meta name="author" content="<?php echo meta_author();?>" >
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo 'assets/frontend/css/bootstrap.css'; ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo 'assets/frontend/css/style.css'; ?>" rel="stylesheet">        
        <link href="<?php echo 'assets/frontend/css/nanoscroller.css'; ?>" rel="stylesheet">        
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>         
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script src="<?php echo 'assets/frontend/js/bootstrap.js'; ?>" type="text/javascript"></script>
        <script type="text/javascript">
        var loadingMoreStories=false;
        $(document).ready(function() {
            $(".buzzScroll").nanoScroller();
            $(".buzzScroll").bind("scrollend", function(e){
                
                if(!loadingMoreStories)
                {
                    $content_id = '#'+$(this).data().id;                    
                    var type    = $(this).data().type;                    
                    loadingMoreStories=true;                        
                    start_count=$($content_id).children().last().data().id;                    
                    if(start_count>0)
                    {
                        
                        $loader=$('<div id="id_loader'+type+'" align="center" class="row-fluid" style="padding-bottom:30px;">'+ $('#Loading_more_stories').text() + '<img width="25" src="<?php echo base_url(); ?>assets/images/ajax-loader-1.gif" /></div>');
                        $($content_id).append($loader);
                        data ='start_count='+start_count;
                        setTimeout(function() {
                            $.ajax({
                                type: "POST",
                                dataType: 'html',
                                url: 'activity/getMoreActivity/'+type,
                                data: data,
                                success: function(response){ 
                                    if(response!='' &&  response!='<!---->')
                                    {
                                         $('#id_loader'+type).remove();
                                        $($content_id).append(response);
                                        $($content_id).parent().nanoScroller();
                                        loadingMoreStories=false;
                                        
                                    }
                                    else
                                    {                                
                                        $('#id_loader'+type).remove();
                                        $no_more=$('<div id="id_loader'+type+'" align="center" class="row-fluid" style="padding-bottom:30px;">'+ $('#No_more_log_to_load').text() +'</div>');
                                        $($content_id).append($no_more);
                                        $($content_id).parent().nanoScroller();
                                        loadingMoreStories=false;
                                    }
                                }
                            });
                        }, 1);
                    
                    }
                    
                }
                   
            });
            
            
            $(".tabs").hide();
            $(".tabs:first").show();
            $("#glomped_inactive").hide();
            $(".activityTab a").click(function(e) {
                var activeTab = $(this).data().href;                
                if(activeTab == "#tabPoints" || activeTab == "#tabNormal")
                {
                    if (activeTab == "#tabPoints")
                    {
                        $("#glomped_active").show();
                        $("#glomped_inactive").hide();
                    }
                    else
                    {
                        $("#glomped_inactive").show();
                        $("#glomped_active").hide();
                    }
                    /*e.preventDefault();*/
                    $(".activityTab a").removeClass("activeTab");
                    $(this).addClass("activeTab");
                    $(".tabs").hide();
                    $(activeTab).fadeIn();                
                }
                
            });
            
        });
        $("document").ready(function()
        {
            $("#glomped_inactive").show();
                $("#glomped_active").hide();
        });
        
        </script>
    </head>
    <body>
    <?php include_once("includes/analyticstracking.php") ?>    
    <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">                        
                            <div class="middleBorder">
                                <div class="row-fluid">
                                    <div class="span8" style="margin-top:-3px;">
                                        <div class="activityTab">
                                            <ul>
                                                <li style="float:left"><a href="javascript:void(0);" data-href="#tabNormal" class="activeTab" style="width:292px !important;text-align:center;margin-right:0px;" ><?php echo ucfirst($this->lang->line('Activity', 'Activity')); ?></a></li>
                                                <li style="float:right">
                                                    <a href="javascript:void(0);" data-href="#tabPoints" class="" style="width:292px !important;text-align:center;margin-right:0px;" >                                                        
                                                        &nbsp;<span id="glomped_inactive"><img style="width:140px;" src="assets/frontend/img/glomp_points_log_1.png" alt="glomp!points Log"></span>
                                                        <span id="glomped_active" style="display:none"> <img style="width:140px;" src="assets/frontend/img/glomp_points_log_2.png" alt="glomp!points Log"></span>                                                        
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <div class="tabContent tabs" id="tabNormal" style="border-radius:0px !important;">
                                                <div class="tabInnerContent" style="color:#000;padding:27px 5px 27px 17px; ">                                                    
                                                    <div class="buzzScroll nano" style="height:480px;" data-id="log_content" data-type="log">
                                                        <div class="content"  id="log_content" style="padding-right:15px;" >                                                        
                                                            <?php 
                                                                $start_count=0;
                                                                foreach($activity->result() as $rec)
                                                                {
                                                                    $start_count++;
                                                                    $details = json_decode($rec->details);
                                                                    $user_id=$this->session->userdata('user_id');
                                                                    switch($rec->log_type)
                                                                    {
                                                                        case 'glomp_someone':                                                                            
                                                                            $product_id=$details->product_id;
                                                                            $recipient_id=$details->recipient_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                                                                            $recipient_name =$info->friends->$recipient_id->user_name;
                                                                            $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>

                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_glomped_friend',
                                                                                                array(
                                                                                                    'reciever' => '<a href="' . base_url('profile/view/'.$recipient_id) . '" class="red"><b>' . $recipient_name . '</b></a>',
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                '<span class="red"><b>glomp!</b>ed</span> {reciever} a {merchant} {product}' );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $recipient_name;?>" />
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$recipient_id);?>" class="red">
                                                                                            <div style="border-radius:8px; overflow:hidden; height:68px;width:68px;">
                                                                                                <img src="<?php echo $recipient_photo;?>" alt="<?php echo $recipient_name;?>" class="user-photo" />
                                                                                            </div>
                                                                                        </a>
                                                                                    </div>        
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'received_a_glomp':
                                                                            $product_id=$details->product_id;
                                                                            $glomper_id=$details->glomper_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$glomper_id));
                                                                            $glomper_name =$info->friends->$glomper_id->user_name;
                                                                            $recipient_photo = $this->custom_func->profile_pic($info->friends->$glomper_id->user_prifile_pic,$info->friends->$glomper_id->user_gender);
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid"  style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>                                                                                        
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_glomp_recieved',
                                                                                                array(
                                                                                                    'sender' => '<a href="' . base_url('profile/view/'.$glomper_id) . '"><b>' . $glomper_name . '</b></a>',
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                '{sender} <span class="red"><b>glomp!</b>ed</span> a {merchant} {product}' );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $glomper_name;?>" />
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$glomper_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $recipient_photo;?>" alt="<?php echo $glomper_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'received_a_reward':                                                                            
                                                                            $product_id=$details->product_id;
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>    
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_reward_recieved',
                                                                                                array(
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                '<span class="red"><b>Rewarded</b></span> a {merchant} {product} from <span class="red"><b>glomp!</b></span>' );
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo base_url('assets/images/glomp_icon.png');?>" />
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'redeemed_a_voucher':                                                                            
                                                                            $product_id=$details->product_id;
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>    
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_redeemed_voucher',
                                                                                                array(
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                '<span class="red"><b>Redeemed</b></span> a {merchant} {product}' );
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <img style="width:100%;border-radius:8px;" src="<?php echo base_url('assets/images/glomp_icon.png');?>" />
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;                                                                        
                                                                        case 'friend_redeemed_a_voucher':
                                                                            
                                                                            $friend_id=$details->friend_id;
                                                                            
                                                                            
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                                                                            $friend_name =$info->friends->$friend_id->user_name;
                                                                            $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                                                                            
                                                                            $product_id=$details->product_id;
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;
                                                                            
                                                                            
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_friend_redeemed',
                                                                                                array(
                                                                                                    'reciever' => '<a href="' . base_url('profile/view/'.$friend_id) . '" class="red"><b>' . $friend_name . '</b></a>',
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                'Your friend {reciever} <span class="red"><b>redeemed</b></span> a {merchant} {product}' );
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">                                                                                        
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $this->custom_func->product_logo($prod_image);?>" alt="<?php echo $details->product_name;?>" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        break;
                                                                        case 'added_a_friend': 
                                                                                                                                   
                                                                            $friend_id=$details->friend_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                                                                            $friend_name =$info->friends->$friend_id->user_name;
                                                                            $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>    
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_added_friend',
                                                                                                array(
                                                                                                    'friend' => '<a href="' . base_url('profile/view/'.$friend_id) . '"><b>' . $friend_name . '</b></a>'
                                                                                                ),
                                                                                                '<span class="red"><b>Added</b></span> a {friend} to your Friends list' );
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">                                                                                        
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'friend_added_you':                                                                            
                                                                            $friend_id=$details->friend_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                                                                            $friend_name =$info->friends->$friend_id->user_name;
                                                                            $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_friend_added_you',
                                                                                                array(
                                                                                                    'friend' => '<a href="' . base_url('profile/view/'.$friend_id) . '"><b>' . $friend_name . '</b></a>'
                                                                                                ),
                                                                                                '{friend} <span class="red"><b>Added</b></span> you to their Friends list' );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">                                                                                        
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'unfriend_a_friend':                                                                            
                                                                            $friend_id=$details->friend_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$friend_id));
                                                                            $friend_name =$info->friends->$friend_id->user_name;
                                                                            $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;"  data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>

                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_unfriend',
                                                                                                array(
                                                                                                    'friend' => '<a href="' . base_url('profile/view/'.$friend_id) . '"><b>' . $friend_name . '</b></a>'
                                                                                                ),
                                                                                                '<span class="red"><b>Removed</b></span> {friend} from your Friends list' );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">                                                                                        
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'invited_a_friend':
                                                                                                                                                                                                                                    
                                                                            if($details->invited_through=='facebook')
                                                                            {
                                                                                ?>
                                                                                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                    <div class="span8" style="padding:10px 10px">
                                                                                        <div style="margin-bottom:10px;font-size:11px;">
                                                                                            <b>
                                                                                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                            </b>
                                                                                        </div>
                                                                                        <div>
                                                                                            <?php
                                                                                                echo $this->lang->compound(
                                                                                                    'activity_invited_facebook_friend',
                                                                                                    array(
                                                                                                        'friend' => '<a href="http://graph.facebook.com/' . $details->invite_facebook_id . '" target="_blank" class="red"><b>' . $details->invited_name . '</b></a>',
                                                                                                    ),
                                                                                                    '<span class="red"><b>Invited</b></span> {friend} from Facebook to join <span class="red"><b>glomp</b>!ed</span>'
                                                                                                );
                                                                                            ?>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="span4" style="padding:4px 8px">
                                                                                        <div class="span6" style="padding:4px;">                                                                                        
                                                                                        </div>
                                                                                        <div class="span6" style="padding:4px;">
                                                                                            <a href="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>" target="_blank" class="red">
                                                                                                <img style="width:100%;border-radius:8px;" src="http://graph.facebook.com/<?php echo $details->invite_facebook_id;?>/picture?width=140&height=140" alt="<?php echo $details->invited_name;?>" />
                                                                                            </a>
                                                                                        </div>                                                               
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                            <?php
                                                                            }
                                                                            else if($details->invited_through=='linkedIn')
                                                                            { ?>
                                                                                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                    <div class="span8" style="padding:10px 10px">
                                                                                        <div style="margin-bottom:10px;font-size:11px;">
                                                                                            <b>
                                                                                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                            </b>
                                                                                        </div>
                                                                                        <div>
                                                                                            <?php
                                                                                                echo $this->lang->compound(
                                                                                                    'activity_invited_linkedin_friend',
                                                                                                    array(
                                                                                                        'friend' => '<b>' . $details->invited_name . '</b>',
                                                                                                    ),
                                                                                                    '<span class="red"><b>Invited</b></span> {friend} from LinkedIn to join <span class="red"><b>glomp</b>!ed</span>'
                                                                                                );
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="span4" style="padding:4px 8px">
                                                                                        <div class="span6" style="padding:4px;">                                                                                        
                                                                                        </div>
                                                                                        <div class="span6" style="padding:4px;">
                                                                                            
                                                                                        </div>                                                               
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                            <?php
                                                                            
                                                                            }
                                                                            else if($details->invited_through=='email')
                                                                            { ?>
                                                                                <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                    <div class="span8" style="padding:10px 10px">
                                                                                        <div style="margin-bottom:10px;font-size:11px;">
                                                                                            <b>
                                                                                            <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                            </b>
                                                                                        </div>
                                                                                        <div>
                                                                                            <?php
                                                                                                echo $this->lang->compound(
                                                                                                    'activity_invited_email_friend',
                                                                                                    array(
                                                                                                        'friend' => '<b>' . $details->invited_name . '</b>',
                                                                                                    ),
                                                                                                    '<span class="red"><b>Invited</b></span> {friend} through email to join <span class="red"><b>glomp</b>!ed</span>'
                                                                                                );
                                                                                            ?>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="span4" style="padding:4px 8px">
                                                                                        <div class="span6" style="padding:4px;">                                                                                        
                                                                                        </div>
                                                                                        <div class="span6" style="padding:4px;">
                                                                                            
                                                                                        </div>                                                               
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                            <?php
                                                                            
                                                                            }
                                                                        break;
                                                                        case 'friend_added_by_friend':
                                                                            
                                                                            $friend_id=$details->friend_id;
                                                                            $added_friend_id=$details->added_friend_id;
                                                                            
                                                                            $info = json_decode($this->users_m->friends_info_buzz($added_friend_id.','.$friend_id));
                                                                            $friend_name =$info->friends->$friend_id->user_name;
                                                                            $friend_photo = $this->custom_func->profile_pic($info->friends->$friend_id->user_prifile_pic,$info->friends->$friend_id->user_gender);
                                                                            
                                                                            $added_friend_name =$info->friends->$added_friend_id->user_name;
                                                                            $added_friend_photo = $this->custom_func->profile_pic($info->friends->$added_friend_id->user_prifile_pic,$info->friends->$added_friend_id->user_gender);
                                                                            
                                                                            
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_friend_added_friend',
                                                                                                array(
                                                                                                    'friend' => '<a href="' . base_url('profile/view/'.$friend_id) . '"><b>' . $friend_name . '</a></b>',
                                                                                                    'other_friend' => '<a href="' . base_url('profile/view/'.$added_friend_id) . '"><b>' . $added_friend_name . '</a></b>'
                                                                                                ),
                                                                                                'Your friend {friend} <span class="red"><b>added</b></span> your other friend {other_friend}'
                                                                                            );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span6" style="padding:4px;">                                                                                        
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $friend_photo;?>" alt="<?php echo $friend_name;?>" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="span6" style="padding:4px;">
                                                                                        <a href="<?php echo base_url('profile/view/'.$friend_id);?>" class="red">
                                                                                            <img style="width:100%;border-radius:8px;" src="<?php echo $added_friend_photo;?>" alt="<?php echo $added_friend_name;?>" />
                                                                                        </a>
                                                                                    </div>                                                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                    }
                                                                ?>                                                                    
                                                                <script type="text/javascript">
                                                                    /* for resetting photos orientation to square */
                                                                    jQuery(window).load(function(){
                                                                        jQuery('.user-photo').each( function(i,o) {

                                                                            if( o.naturalWidth >= o.naturalHeight ) {
                                                                                /* landscape oriented */
                                                                                var diff;
                                                                                jQuery(o).height(68);
                                                                                jQuery(o).width('auto');
                                                                                diff = jQuery(o).width() - 68;
                                                                                diff = diff/2;
                                                                                jQuery(o).css('margin-left','-'+diff+'px');
                                                                            } else {
                                                                                /* portrait oriented */
                                                                                jQuery(o).height('auto');
                                                                                jQuery(o).width(68);
                                                                                jQuery(o).css('margin-top','-20%');
                                                                            }
                                                                        });
                                                                    });
                                                                </script>
                                                                <?php
                                                                }
                                                                if($start_count==0)
                                                                { 
                                                                ?>
                                                                    <div class="row-fluid" >
                                                                        <div class="span12" style="padding:10px 10px" align="center">
                                                                            <?php echo $this->lang->line('No_activity_logs_yet', "No activity logs yet." ); ?>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                }
                                                            ?>
                                                            <!--activity log -->
                                                            
                                                           
                                                            
                                                            <!--activity log -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tabContent tabs" id="tabPoints" style="border-radius:0px !important; ">                                                 
                                                <div class="tabInnerContent" style="color:#000;padding:27px 5px 27px 17px; ">
                                                    <div class="buzzScroll nano" style="height:480px;" data-id="log_content_points" data-type="points">
                                                        <div class="content"  id="log_content_points" style="padding-right:15px;" >
                                                            <?php 
                                                                $start_count=0;
                                                                foreach($activity_points->result() as $rec)
                                                                {
                                                                    $start_count++;
                                                                    $details = json_decode($rec->details);
                                                                    $user_id=$this->session->userdata('user_id');
                                                                    switch($rec->log_type)
                                                                    {
                                                                        case 'points_purchased':                                                                            
                                                                            ?>
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_purchased_points',
                                                                                                array(
                                                                                                    'points' => $details->points
                                                                                                ),
                                                                                                '<span class="red"><b>Purchased</b></span> {points} points'
                                                                                            );
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span12" style="padding:4px;" align="right">
                                                                                        <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                                                                            <?php echo $details->balance;?>
                                                                                        </div>                                                                                            
                                                                                    </div>                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'points_spent':                                                                            
                                                                            $product_id=$details->product_id;
                                                                            $recipient_id=$details->recipient_id;
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                                                                            $recipient_name =$info->friends->$recipient_id->user_name;
                                                                            $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;                                                                                    
                                                                            $points =((int) $details->points) *-1;
                                                                            if($points>0)
                                                                            {
                                                                                $points =" + ".abs($points);
                                                                            }
                                                                            else
                                                                            {
                                                                                $points =" - ".abs($points);
                                                                            }
                                                                            
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_spent_points',
                                                                                                array(
                                                                                                    'reciever' => '<a href="'. base_url('profile/view/'.$recipient_id) .'">'. $recipient_name .'</a>',
                                                                                                    'points' => $points,
                                                                                                    'merchant' => $details->merchant_name,
                                                                                                    'product' => $details->product_name
                                                                                                ),
                                                                                                '<span class="red"><b>glomp!</b></span> {reciever} a {merchant} {product} ({points})'
                                                                                            );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span12" style="padding:4px;" align="right">
                                                                                        <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                                                                            <?php echo $details->balance;?>
                                                                                        </div>                                                                                            
                                                                                    </div>                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        break;
                                                                        case 'points_reversed':                                                                            
                                                                            $voucher_id=$details->voucher_id;
                                                                            $voucher_data=$this->users_m->get_specific_voucher_details($voucher_id);
                                                                            $voucher_data=$voucher_data->row();
                                                                            
                                                                            
                                                                            $product_id     =$voucher_data->voucher_product_id;
                                                                            $recipient_id   =$voucher_data->voucher_belongs_usser_id;
                                                                            
                                                                            
                                                                            
                                                                            $info = json_decode($this->users_m->friends_info_buzz($user_id.','.$recipient_id));
                                                                            $recipient_name =$info->friends->$recipient_id->user_name;
                                                                            $recipient_photo = $this->custom_func->profile_pic($info->friends->$recipient_id->user_prifile_pic,$info->friends->$recipient_id->user_gender);
                                                                            
                                                                            $prod_info = json_decode($this->product_m->productInfo($product_id));
                                                                            $prod_image= $prod_info->product->$product_id->prod_image;
                                                                            
                                                                            $merchant_name  =$prod_info->product->$product_id->merchant_name;
                                                                            $product_name   =$prod_info->product->$product_id->prod_name;
                                                                            $points =((int) $details->points) *-1;
                                                                            if($points>0)
                                                                            {
                                                                                $points =" + ".abs($points);
                                                                            }
                                                                            else  if($points<0)
                                                                            {
                                                                                $points =" - ".abs($points);
                                                                            }
                                                                            else
                                                                            {
                                                                                $points =" ".abs($points);
                                                                            }
                                                                            
                                                                            ?>
                                                                            
                                                                            <div class="row-fluid" style="margin-bottom:3px;border: 2px solid #CFCFCF; border-radius:8px;" data-id="<?php echo $start_count;?>">
                                                                                <div class="span8" style="padding:10px 10px">
                                                                                    <div style="margin-bottom:10px;font-size:11px;">
                                                                                        <b>
                                                                                        <?php echo date('h:i a F d Y',(strtotime($rec->log_timestamp)));?>
                                                                                        </b>
                                                                                    </div>
                                                                                    <div>
                                                                                        <?php
                                                                                            echo $this->lang->compound(
                                                                                                'activity_reversed_points',
                                                                                                array(
                                                                                                    'reciever' => '<a href="'. base_url('profile/view/'.$recipient_id) .'">'. $recipient_name .'</a>',
                                                                                                    'points' => $points,
                                                                                                    'merchant' => $merchant_name,
                                                                                                    'product' => $product_name
                                                                                                ),
                                                                                                '<span class="red"><b>glomp!</b></span> of a {merchant} {product} was <span class="red"><b>unredeemed</b></span> by {reciever} ({points})'
                                                                                            );
                                                                                        ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="span4" style="padding:4px 8px">
                                                                                    <div class="span12" style="padding:4px;" align="right">
                                                                                        <div class="fr" style="padding:23px 10px 17px 10px;height:20px;min-width:40px;background-color:#ED2228;border-radius:8px;color:#fff;font-size:24px" align="center">
                                                                                            <?php echo $details->balance;?>
                                                                                        </div>                                                                                            
                                                                                    </div>                               
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            break;
                                                                        case 'points_rewarded':
                                                                            //print_r($details);
                                                                        break;
                                                                    }
                                                                }
                                                                if($start_count==0)
                                                                { 
                                                                ?>
                                                                    <div class="row-fluid" >
                                                                        <div class="span12" style="padding:10px 10px" align="center">
                                                                            <?php echo $this->lang->line('No_activity_logs_yet', "No activity logs yet." ); ?>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id ="gl_languages" style="display:none;">
            <span id="Payment_lang_5"><?php echo $this->lang->line('Loading_more_stories', "Loading more stories..."); ?></span>
            <span id="Payment_lang_7"><?php echo $this->lang->line('No_more_log_to_load', "No more activity log to load."); ?></span>
            <span id="Payment_lang_8"><?php echo $this->lang->line('Payment_lang_8', "" ); ?></span>
        </div>
    </body>
</html>
