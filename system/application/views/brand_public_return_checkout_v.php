<?php
?>
<script>
    var selectedTab;
    var merID;
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title>Brands - Glomp.it</title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">

        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <?php /*<script src="//connect.facebook.net/en_US/all.js"></script> */ ?>
        <script type="text/javascript" src="<?php echo get_protocol(); ?>://platform.linkedin.com/in.js">
            api_key: 75ocjfra1o2yjt
            authorize: true
            onLoad: liInitOnload
        </script>
        <script type="text/javascript">
            var FB_APP_ID = "<?php echo ($this->custom_func->config_value('FB_API_APP_ID_' . FACEBOOK_API_STATUS)); ?>";
            var LOCATION_REGISTER = "<?php echo base_url('index.php/landing/register'); ?>";
            var SIGNUP_POPUP_TEXT = "<?php echo $this->lang->line('signup_popup_text'); ?>";
            var SIGNUP_WITH_FB_BUT_REG = "<?php echo $this->lang->line('signup_with_fb_but_already_registered'); ?>";
            var GLOMP_BASE_URL = "<?php echo base_url(''); ?>";
			
			var fb_id ="";
			
        </script>
        <!-- the javascript inline code  has been moved the below file-->
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        <script type="text/javascript">
			$(document).ready(function(e) {
				<?php
					if($gl_voucher_brand_data->transaction_type =='glomp_to_fb')
					{
						$temp = explode("@",$gl_voucher_brand_data->email);
				?>		fb_id ="<?php echo $temp[0];?>";
						do_send_fb_confirm();
						
				<?php
					}
				?>
			});
			function do_send_fb_confirm()
			{
				var NewDialog = $('<div id="" align="center" style="padding-top:25px !important; font-size:14px">\
				<p align="justify">If your friend is new to glomp!, we recommend that you send a personal message to inform them that you have glomp!ed (treated) them in order to ensure that they accept the treat.</p>\
								</div>');
				NewDialog.dialog({                    
					dialogClass:'noTitleStuff',
					title: "",
					autoOpen: false,
					resizable: false,
					modal: true,
					width: 500,
					show: '',
					hide: '',
					buttons: [
							{text: "OK",
							"class": 'btn-custom-darkblue width_80_per',
							click: function() {
								$(this).dialog("close");
								
								FB.ui({
								  to: fb_id,
								  method: 'send',
								  link: '<?php echo site_url('m/brands/details/'.$gl_voucher_brand_data->group_voucher_id);?>'
								  
								},
								  function(response) {
									var msg ='Message successfully sent.';
									if (response && !response.error_code)
									{
									  
										
									} else {
									  msg = 'Message not sent. Refresh the page to retry.'
									}
									
									var NewDialog = $('<div id="" align="center" style="padding-top:20px !important; font-size:14px">\
										<p align="center">'+msg+'</p>\
														</div>');
										NewDialog.dialog({                    
											dialogClass:'noTitleStuff',
											title: "",
											autoOpen: false,
											resizable: false,
											modal: true,
											width: 320,
											height:100,
											show: '',
											hide: '',
											buttons: [
													{text: "OK",
													"class": 'btn-custom-darkblue width_80_per',
													click: function() {
														$(this).dialog("close");
													}}
											]
										});
										NewDialog.dialog('open');
								  }
								);
							}}
					]
				});
				NewDialog.dialog('open');
				
			}
        </script>
		<?php include('includes/glomp_pop_up_my_brand_js.php'); ?>
    </head>
    <body>
        <?php include_once("includes/analyticstracking.php") ?>
       
        <?php include('includes/header.php') ?>
        <div class="container">
            <div class="outerBorder">
                <div class="inner-content">
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="middleBorder topPaddingZero">
                                <div class="profile">
									
                                    <div class="row-fluid" >
                                        <div class="span12" style="margin-top:10px;">
                                                
												<div id="tabBrands" class="tabs" style="background:#343239">
														<h3 style="color:#fff;padding:10px 0px 0px 15px;margin:0px;">Congratulations!</h3>
													<div id="brand-cart-layer" class="row-fluid" style="display: block;padding-bottom:10px;margin-top:-15px;">
														<div class="tabContent" style="background:#343239">
															<div id="" class="span12 tabInnerContent" comment="" style="display: block;">
																<h4 style="padding:0px ;margin:0px;">Order Summary for <?php echo $gl_voucher_brand_data->group_voucher_id;?></h4>
																<?php
																if($gl_voucher_brand_data->transaction_type=='buy')
																{
																?>
																<hr>
																<div>
																	<div style="font-weight:bold;font-size:16px;">
																		<?php echo $gl_voucher_brand_data->first_name;?> <?php echo $gl_voucher_brand_data->last_name;?>
																	</div>
																	<div >
																		<b>Contact Number:</b> <?php echo $gl_voucher_brand_data->contact_num;?>
																	</div>
																	<div >
																		<b>Delivery Type:</b> For <?php echo $gl_voucher_brand_data->delivery_type;?>
																	</div>
																	<div >
																		<b>Address:</b> <?php echo $gl_voucher_brand_data->delivery_address;?>
																	</div>
																</div>
																<?php
																}
																?>
																<hr>
																
																<?php 
																if($cart_data!="")
																{
																
																	$grand_total = 0;
				
																	foreach($cart_data as $row):
																	$product = json_decode($row->prod_details);
																	//print_r($product);
																		$grand_total += ( ($row->prod_cost ) * $row->prod_qty);
																?>
																<div class="body_bottom_1px" id="item_wrapper_<?php echo $product->product_id;?>">
																
																	<div class="row-fluid" >
																		<div class="span1">
																			<div class="thumbnail">
																				<img src="<?php echo site_url();?>/custom/uploads/products/thumb/<?php echo $product->prod_image;?>" alt="<?php echo $product->prod_name;?>">
																			</div>
																		</div>
																		<div class="span6">
																			<b class="name1" style="font-size:15px;padding:0px 4px; border:solid 0px"><?php echo $product->merchant_name;?></b>
																			<div class="name2" style="font-size:14px;padding:0px 4px; border:solid 0px"><?php echo $product->prod_name;?></div>
																			
																		</div>
																		<div class="span2" align="right">
																			<div class="notification">
																				$ <?php echo number_format(($row->prod_cost ),2);?>
																			</div>
																		</div>
																		<div class="span1" align="right">
																			Qty: <?php echo $row->prod_qty;?>
																		</div>
																		<div class="span2" align="right" style="font-weight:bold;font-size:15px;" id="total_<?php echo $product->product_id;?>">
																			$ <?php echo number_format(($row->prod_cost) * ($row->prod_qty),2);?>
																		</div>
																	</div>
																</div>
																<?php endforeach;
																}
																else
																{
																	echo "No items to display.";
																}
																?>
																
																<hr style="padding:0px; margin:0px;">
																<div class="row-fluid" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:16px;font-weight:bold">
																		Total: S$ <span id="grand_total">
																			<?php echo number_format($grand_total,2);?>
																		</span>
																	</div>
																</div>
																<div class="row-fluid hide" >
																	<div class="span8">
																	</div>
																	<div class="span4" align="right" style="font-size:12px;font-weight:bold">
																		Total: USD$ <span id="grand_total">
																			<?php echo number_format(($grand_total*$gl_voucher_brand_data->forex),2);?>
																		</span>
																	</div>
																</div>

																<div class="row-fluid" >
																	<div class="span9">
																	</div>
																	<div class="span3" align="right" style="font-size:16px;font-weight:bold">
																		<button id="order_confirm_ok" class="btn-custom-green" style="font-size:20px !important">OK</button>
																	</div>
																</div>
                                                                                                                                <script>
                                                                                                                                    $('#order_confirm_ok').click(function(e){
                                                                                                                                       e.preventDefault();
                                                                                                                                       return window.location = '<?php echo site_url('brands/view/amex');?>';
                                                                                                                                    });
                                                                                                                                </script>
															</div>
														</div>
														
														
													</div>
												</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>