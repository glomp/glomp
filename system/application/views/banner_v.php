<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>  
    <?php if($banner_id!='all')        
    {
    ?>
        <meta property="og:title" content="glomp!" /> 
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo base_url('banner/image/'.$banner_id);?>" />
        <meta property="og:image" content="<?php echo $image_url[$banner_id];?>"/>    
        <meta name="description" content="Discover ‘real’ expressions of Like-ing. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!" >	
        
        
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@glompit">
        <meta name="twitter:title" content="glomp!">
        <meta name="twitter:description" content="Up your 'Like'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!">
        <meta name="twitter:creator" content="@glompit">
        <meta name="twitter:image:src" content="<?php echo $image_url[$banner_id];?>">
        <meta name="twitter:domain" content="<?php echo base_url();?>">        
    <?php    
    }
    ?>
    
    <meta http-equiv="refresh" content="3; url=http://glomp.it/landing/">
    
	<body style=" background:#d3d7e1;">		
        <?php if($banner_id=='all')        
        {
            foreach($image_url as $img)
            {
            ?>
                <img src="<?php echo $img;?>" />
                <br>
            <?php                
            }
        
        }
        else
        {
        ?>
            <img src="<?php echo $image_url[$banner_id];?>" />
        <?php
        }
        ?>        
		
	</body>
    
</html> 