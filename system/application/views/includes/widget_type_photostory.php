<div class="widget-photostory <?php echo $widget->data->bg_color?> <?php echo $widget->data->type?>">
    <div class="widget-title">
		<?php 
			if($widget->data->brand_product_button_id !=0)
			{
			
				$public_alias = isset($brand->public_alias)? $brand->public_alias:'amex';
		?>	
			<a href="<?php echo site_url();?>brands/view/<?php echo $public_alias;?>/<?php echo $widget->data->brand_product_button_id;?>" style="float:right" class="btn-custom-transparent_white">Products</a>
		
			<div style="float:left;">
		<?php
			}
		?>
			
				<h3 class="widget-title-head"><?php echo $widget->data->title;?></h3>
				<p class="widget-title-sub"><?php echo $widget->data->subtitle;?></p>
		<?php 
			if($widget->data->brand_product_button_id !=0)
			{
		?>
			</div>
			<div style="clear:both"></div>
		<?php
			}
		?>
		
    </div>
    <div class="widget-photostory-body">
        <?php //if($widget->data->brand_product_button_id ==0) { ?>
        <div class="widget-photostory-image">
            <img src="<?php echo $widget->data->image;?>" width="100%" />
        </div>
        <div class="widget-photostory-context" <?php echo ($widget->data->brand_product_button_id ==0 ) ? : 'style="width: 325px;padding:10px;"' ?> >
            <div><?php echo $widget->data->context;?></div>
        </div>
        <?php //} ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('div.widget-photostory-image img').one('load',function(){
            var img = jQuery(this);
            var txt = jQuery(this).parent().next();
            if( img.height() > txt.height() ) {
                txt.css('height',img.height() - 20);
            }
        }).each(function(){
            if(this.complete) jQuery(this).load();
        });
        
        var hold_msg = $('<div style="padding-top: 15px;font-size: 14px;">Please check back shortly for great products on offer.</div>');
        hold_msg.dialog({
            dialogClass: 'noTitleStuff',
            title: '',
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            height: 120,
            show: '',
            hide: '',
            buttons: [
                {
                    text: "Close",
                    "class": 'btn-custom-darkblue',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        });
        
        
        $('.hold_on').unbind('click').click(function(e){
           e.preventDefault();
           hold_msg.dialog('open');
        });
    });
</script>
