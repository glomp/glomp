<!-- Google Analytics -->
<script async src='//www.google-analytics.com/analytics.js'></script>
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;

/*ga('create', 'UA-44929675-1', 'auto'); live ID/**/ 
/*ga('create', 'UA-49192766-1', { stagin ID**/
ga('create', 'UA-44929675-1', {
  'cookieDomain': 'none'
});
ga('send', 'pageview');/**/
/*ga('send', 'pageview', {
   'title': 'Registration Page',
    'hitCallback': function() {
        console.log('analytics.js done sending data');
    }
});/**/
$(document).ready(function(){
    $('#user_login').on('click', function(e){
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'login',    
            'eventLabel': 'normal login',
            'eventValue': 1,
            'hitCallback': function() {
                console.log('analytics.js done sending data');
            }
        },
        {useBeacon: true}
        );
    });
    
    $('#Login_Facebook').on('click', function(e){
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'login',    
            'eventLabel': 'facebook login',
            'eventValue': 1,
            'hitCallback': function() {
                console.log('analytics.js done sending data');
            }
        },
        {useBeacon: true}
        );
    });
    
    $('#LinkedIN').on('click', function(e){
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'login',    
            'eventLabel': 'linkedin login',
            'eventValue': 1,
            'hitCallback': function() {
                console.log('analytics.js done sending data');
            }
        },
        {useBeacon: true}
        );
    });
    
    $('._send_glomp1, ._send_glomp').on('click', function(e){
        
        ga_glomp_on_email();
    });
    
    $('.share_on_facebook_redeem').on('click', function(e){
    
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'share',    
            'eventLabel': 'shared redeem on facebook',
            'eventValue': 3,
            'hitCallback': function() {
                console.log('analytics.js done sending data. shared redeem on facebook');
            }
        },
        {useBeacon: true}
        );
    });
    $('.share_on_linkedin_redeem').on('click', function(e){
    
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'share',    
            'eventLabel': 'shared redeem on linkedin',
            'eventValue': 3,
            'hitCallback': function() {
                console.log('analytics.js done sending data. shared redeem on linkedin');
            }
        },
        {useBeacon: true}
        );
    });
    $('.share_on_twitter_redeem').on('click', function(e){
    
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'share',    
            'eventLabel': 'shared redeem on twitter',
            'eventValue': 3,
            'hitCallback': function() {
                console.log('analytics.js done sending data. shared redeem on twitter');
            }
        },
        {useBeacon: true}
        );
    });
    $('.share_on_facebook').on('click', function(e){
        ga_share_on_facebook();
    });
    $('.share_on_linkedin').on('click', function(e){
    
        ga('send', {
            'hitType': 'event',       
            'eventCategory': 'button', 
            'eventAction': 'share',    
            'eventLabel': 'shared on linkedin',
            'eventValue': 3,
            'hitCallback': function() {
                console.log('analytics.js done sending data. shared on linkedin');
            }
        },
        {useBeacon: true}
        );
    });
    $('.share_on_twitter').on('click', function(e){
        ga_share_on_twitter();
    });
    
    $('#invite_btn').on('click', function(e){
        ga_invite_by_email();
    });
    $('.btn-custom-blue-grey_xs').on('click', function(e){
        if($(this).val() =='invite')
            ga_invite_by_email();
    });
});
function doGlomp_ga_gs()
{
    ga_glomp_on_email();
}
function ga_share_on_twitter()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'share',    
        'eventLabel': 'shared on twitter',
        'eventValue': 3,
        'hitCallback': function() {
            console.log('analytics.js done sending data. shared on twitter');
        }
    },
    {useBeacon: true}
    );
}
function ga_share_on_facebook()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'share',    
        'eventLabel': 'shared on facebook',
        'eventValue': 3,
        'hitCallback': function() {
            console.log('analytics.js done sending data. shared on facebook');
        }
    },
    {useBeacon: true}
    );
}
function ga_glomp_on_email()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'glomp!',    
        'eventLabel': 'glomp!ing',
        'eventValue': 5,
        'hitCallback': function() {
            console.log('analytics.js done sending data. glomp!ing');
        }
    },
    {useBeacon: true}
    );
}
function ga_glomp_on_fb()
{   
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'glomp!',    
        'eventLabel': 'glomp!ing on facebook',
        'eventValue': 5,
        'hitCallback': function() {
            console.log('analytics.js done sending data. glomp!ing on facebook');
        }
    },
    {useBeacon: true}
    );
    
}
function ga_invite_by_email()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'invite',    
        'eventLabel': 'inviting thru email',
        'eventValue': 2,
        'hitCallback': function() {
            console.log('analytics.js done sending data. inviting thru email');
        }
    },
    {useBeacon: true}
    );
}
function ga_invite_by_facebook()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'invite',    
        'eventLabel': 'inviting thru facebook',
        'eventValue': 2,
        'hitCallback': function() {
            console.log('analytics.js done sending data. inviting thru facebook');
        }
    },
    {useBeacon: true}
    );
}
function ga_redeem()
{
    ga('send', {
        'hitType': 'event',       
        'eventCategory': 'button', 
        'eventAction': 'redeem',    
        'eventLabel': 'redeeming',
        'eventValue': 5,
        'hitCallback': function() {
            console.log('analytics.js done sending data. redeeming');
        }
    },
    {useBeacon: true}
    );
}



</script>
<!-- End Google Analytics -->
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-44929675-1', 'glomp.it');
  ga('send', 'pageview');
</script>
-->
<?php
function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
    $ub = "";

    /*First get the platform?*/
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
   
    /* Next get the name of the useragent yes seperately and for good reason*/
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
   
    /* finally get the correct version number*/
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        /* we have no matching number just continue*/
    }
   
    /* see how many we have*/
    $i = count($matches['browser']);
    if ($i != 1) {
        /*we will have two since we are not using 'other' argument yet*/
        /*see if version is before or after the name*/
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
   
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
   
    return array(
        'userAgent' => $u_agent,
        'name'      => $ub,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}
// now try it
$ua=getBrowser();
$support_link='http://activatejavascript.org/en/instructions/';
if($ua['name']=='MSIE'){
    $support_link='http://support.microsoft.com/gp/howtoscript';
}
else if($ua['name']=='Firefox'){
    $support_link='https://support.mozilla.org/en-US/kb/javascript-settings-for-interactive-web-pages';
}
else if($ua['name']=='Chrome'){
    $support_link='https://support.google.com/adsense/answer/12654?hl=en';
}
else if($ua['name']=='Safari'){
    $support_link='http://support.apple.com/kb/HT1677';
}
else if($ua['name']=='Opera'){
    $support_link='http://activatejavascript.org/en/instructions/opera#instructions';
}

/*$yourbrowser= "Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];*/
/*print_r($yourbrowser);*/
?>
<noscript>
    <div align="center" style="padding:15px;font-size:14px;font-weight:bold;background:#A00002;border-bottom:solid 1px #585F6B; color:#FFF">
        <div class="container">                
            We're sorry. In order to fully utilise our platform, please enable JavaScript in your browser. Click <a target="_blank" style="color:#22C8FF" href="<?php echo $support_link;?>">here</a> for instructions.
        </div>        
    </div>    
</noscript> 
<div id="unsupported_wrapper"></div>
<script type="text/javascript"> 
    var nav=check_browser();
    function check_browser()
    {
        var unsupported=false;
        var browser=get_browser();        
        var res=browser.split(' ',2);                
        if((res[0].toLowerCase()) == 'msie')
        {
            if(parseInt(res[1])< 9)
            {
                unsupported=true;
            }
        }        
        if((res[0].toLowerCase()) == 'firefox')
        {            
            if(parseInt(res[1])< 6)
            {
                unsupported=true;
            }
        }   
        if((res[0].toLowerCase()) == 'chrome')
        {           
            
            if(parseInt(res[1])< 12)
            {
                unsupported=true;
            }
        }
        
        if(unsupported){
            document.getElementById('unsupported_wrapper').innerHTML='<div align="center" id="unsupported" style="padding:15px;font-size:14px;font-weight:bold;background:#A00002;border-bottom:solid 1px #585F6B; color:#FFF">\
                                        We\'re sorry. In order to fully utilise our platform, please use the following browsers [ \
                                        <a href="http://windows.microsoft.com/en-PH/internet-explorer/download-ie" style="color:#22C8FF">IE9+</a>, \
                                        <a href="http://www.mozilla.org/en-US/firefox/all/" style="color:#22C8FF">Firefox 6+</a>, \
                                        <a href="https://www.google.com/intl/en/chrome/browser/" style="color:#22C8FF">Chrome 12+</a>, \
                                        <a href="http://www.apple.com/safari/" style="color:#22C8FF">Safari 5.1+</a>, \
                                        <a href="http://www.opera.com/" style="color:#22C8FF">Opera 12.1x+</a> \
                                        ]\
                                    </div>';            
        }
    }
    function get_browser(){
        var s;
        var ua= s || navigator.userAgent, tem, 
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
        return M.join(' ');
    }
</script>