<script>
    var GLOMP_BASE_URL = "<?php echo base_url(); ?>";
</script>
<div class="header"><div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <?php

      if (isset($public_alias) && ($public_alias=='hsbc' || $public_alias=='hsbcsg'))
					$l = 'hsbcsg';
				else if (isset($public_alias) && ($public_alias=='uob' || $public_alias=='uobsg'))
					$l = 'uobsg';
				else if (isset($public_alias) && ($public_alias=='dbs' || $public_alias=='dbssg'))
					$l = 'dbssg';
				else
					$l = 'amex-sg';


      	$p = isset($public_alias)? '/'.$l :'';

      	

      ?>
      <div class="span3"><a href="<?php echo base_url($p); ?>"><img src="assets/frontend/img/logo-small.png" alt="Glomp Logo"></a></div>
      <div class="span9 innerTopNav">
      <?php
	  if($this->session->userdata('user_id') && $this->session->userdata('user_id')>0){
		
		  $user_id = $this->session->userdata('user_id');
		  $res_header = json_decode($this->user_account_m->user_summary($user_id));
		  
		  $user_info = json_decode($this->user_account_m->user_short_info($user_id));
	  ?>
       <div class="settings"><?php echo anchor('user/update',$this->lang->line('setting', 'setting'));?></div>
      <div class="faq"><?php echo anchor('page/index/faq',$this->lang->line('faq', 'faq'));?></div>
      <div class="faq"><?php echo anchor('user/searchFriends', $this->lang->line('search_my_friends', 'Friends')); ?></div>
       <span class="faq"><?php echo anchor('user/',strtoupper($this->lang->line('Home', 'Home')));?></span> 
      
      
      <div class="pull-right" style="width:390px; text-align:right;">
      <div class="glomp"><a href="<?php echo base_url();?>glomp">
      <?php 
	  $active_voucher = $this->users_m->count_active_voucher($user_id);
	 if($active_voucher>0)
	 {
	  ?>
      <div class="notification"><?php echo $active_voucher;?></div><?php }?></a></div>
      <span class="cusMenuHolder" style="padding-top:5px;">
      <span class="user_name "><a href="javascript:void(0);" > <?php echo stripslashes($user_info->user_fname.' '.$user_info->user_lname); ?></a></span> 
      <span class="redPontHolder" id="point_balance" ><?php if( isset($res_header->point_balance) )echo $res_header->point_balance; else echo "0";?></span> 
      <div class="cusMenu" style="margin-left: 233px;">
<script language="javascript" type="application/javascript">
        $(document).ready(function() {
		$(".cusMenuHolder").on({
			mouseenter: function (e) {
				e.stopPropagation();
				$(".cusMenu").show();				
			
		},
		mouseleave: function (e) {
			e.stopPropagation();
			
		}		
		});
		
		$('#user_menu li').on({
			mouseenter: function (e) {
				e.stopPropagation();
				$(".cusMenu").show();				
			
		},
		mouseleave: function (e) {
			e.stopPropagation();
				$(".cusMenu").hide();				
			
		}		
		});
		
		
	
	
	
		$("body").click(function(){
			$(".cusMenu").hide();
		});
		$('body').on("mouseover", function (e) {
				
			
		});
		$('#user_menu li').click(function(e) {
            var href = $(this).find('a').attr('href');
			window.location=href;
        });
});
</script>
                       <ul id="user_menu" >
                       <li><?php echo anchor('profile',$this->lang->line('profile'));?> </li>
                       <li><?php echo anchor('buyPoints',$this->lang->line('buy_points')); ?> </li>
                       <li><?php echo anchor('activity', $this->lang->line('activity_log')); ?> </li>
                      <?php /*?> <li><?php echo anchor('user/menu',$this->lang->line('menu'));?> </li>
                       <li><?php echo anchor('user/favourites',$this->lang->line('favourites'));?> </li><?php */?>
                       <li><?php echo anchor('user/logOut',$this->lang->line('log_out'));?> </li>
                       </ul>
                       </div>
      </span>
     
      </div> 
      <?php 
	  }else
	  {
	  ?>
		<?php
		
			if(
				$this->session->userdata('user_id') =='' &&
				$this->session->userdata('public_user_since_user_logged_in') !=''
				)
			{
				if (isset($public_alias) && ($public_alias=='hsbc' || $public_alias=='hsbcsg'))
					$l = 'hsbcsg';
				else if (isset($public_alias) && ($public_alias=='uob' || $public_alias=='uobsg'))
					$l = 'uobsg';
				else if (isset($public_alias) && ($public_alias=='dbs' || $public_alias=='dbssg'))
					$l = 'dbssg';
				else
					$l = 'amex-sg';
		?>
				<div class="faq"><?php echo anchor('brands/logout/'.$l,$this->lang->line('exit','Exit'));?></div>
		<?php
			}
		?>
        <div class="faq"><?php echo anchor('page/index/faq',$this->lang->line('faq'));?></div>
		
		<?php
		
			if(
				$this->session->userdata('user_id') =='' &&
				$this->session->userdata('public_user_since_user_logged_in') !=''
				)
			{
				

		?>

				<span class="faq"><?php echo anchor($l,$this->lang->line('Home'));?></span>
		<?php
			}
			else
			{
		?>
			<span class="faq"><?php echo anchor('',$this->lang->line('Home'));?></span>
		<?php
			}
		?>
		
		
      <?php 
	  }
	  ?>
      </div>
      </div>
      </div>
</div></div>


        
         