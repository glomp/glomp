<?php

//echo print_r($resFriendList);
//if(( !isset($getFb) || (isset($getFb) && $getFb!=true ) ) && $resFriendList->num_rows()>0)
if ((!isset($getFb) || (isset($getFb) && $getFb != true ) ) && $resFriendList != "" && count($resFriendList) > 0) {
    ?>
    <div class="row-fluid">
        <div class="span12">
            <?php
           
            $i = 1;
            $j = 1;
            foreach ($resFriendList->result() as $recFriendList) {
                                                            if ($i == 1) {
                                                                ?> <ul class="thumbnails"> <?php } ?>

                                                                <li class="span4 addAsFriend" data-position="<?php echo $i; ?>">
                                                                    <div class="addAsFriendPopUp"><h1><?php echo $this->lang->line('add_friend'); ?></h1>

                                                                        <div class="thumbHolder"><img src="<?php echo $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_fname; ?>" ></div>
                                                                        <div class="nameHolder">
                                                                            <div class="nameHolderInner">
                                                                                <?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?> <br /> <?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></div>
                                                                            <div class="userInfo">
                                                                                Add <?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?>  to your glomp! network.?
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="btns">

                                                                            <form name="form<?php echo $recFriendList->user_id ?>" method="post" action="">
                                                                                <button type="button" class="btn-custom-gray" name="Invite" onClick="javascript:location.href = '<?php echo base_url("profile/view/" . $recFriendList->user_id); ?>'" ><?php
                                                                                echo
                                                                                ucfirst($this->lang->line('View_Profile'))
                                                                                ?></button>



                                                                                <button type="button" class="btn-custom-gray addFren" id="<?php echo $recFriendList->user_id ?>"><?php
                                                                                    echo
                                                                                    ucfirst($this->lang->line('Yes'))
                                                                                    ?></button>


                                                                                <button type="button" class="btn-custom-white _hide" name="Invite" ><?php
                                                                                    echo
                                                                                    ucfirst($this->lang->line('Close'))
                                                                                    ?></button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div class="friendThumb">
                                                                        <a href="<?php echo base_url("profile/view/" . $recFriendList->user_id); ?>">
                                                                            <div class="thumbnail">
                                                                                <img src="<?php echo $this->custom_func->profile_pic($recFriendList->user_profile_pic, $recFriendList->user_gender); ?>" alt="<?php echo $recFriendList->user_fname; ?>" >
                                                                                <div class="thumbUserName"><?php echo $recFriendList->user_fname . ' ' . $recFriendList->user_lname; ?> <br /> <?php echo $this->regions_m->region_name($recFriendList->user_city_id); ?></div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </li>

                                                            <?php if ($i == 3 || $j == $resFriendList->num_rows()) { ?></ul>
                                                                <?php
                                                            }
                                                            $j++;
                                                            if ($i == 3) {
                                                                $i = 1;
                                                            }
                                                            else
                                                                $i++;
                                                        } /// end of foreach($r->result() as $p)
                                                        ?>
        </div>
    </div>
        <?php
        }
        else {
            ?>

    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $this->lang->line('No_result_found'); ?>
    </div>

    <?php } ?>