<div class="searchFriendsRight">
    <h1 class="pull-left glompFont"><?php echo $this->lang->line('invite_friend'); ?></h1>
    <div class="pull-left info_tooltip" style="margin-top: 8px;margin-left: 3px;" rel="popover" data-content="<?php echo $this->lang->line('help_searchfriend_invite'); ?>" >
        <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
    </div>
    <div class="pull-right info_tooltip" style="margin-top: 4px;margin-left: 3px;" rel="popover" data-content="<?php echo $this->lang->line('help_searchfriend_fb'); ?>" >
        <img src="<?php echo base_url() ?>assets/images/q-mark.png" />
    </div>
    <div class="pull-right" style="margin-top:8px;margin-bottom: 5px;">						
        <button type="button" class="btn-custom-blue btn-block" name="Invite_Through_Fb" id="Invite_Through_Fb" >&nbsp;<?php echo $this->lang->line('Facebook'); ?>&nbsp;</button>
        <button type="button" class="btn-custom-007BB6 btn-block" name="Invite_Through_LinkedIn" id="Invite_Through_LinkedIn" >&nbsp;<?php echo 'LinkedIn'//echo $this->lang->line('Facebook');?>&nbsp;</button>
    </div>
    <div class="clearfix"></div>
    <?php
    if (isset($invite_message_sent)){
        /*echo '<div class="alert alert-success">' . $invite_message_sent . '</div>';*/
		echo '
		<script>
			invitationSent("' . $invite_message_sent . '");
		</script>	
		';
		
	}
    if (isset($invite_error_msg))
        echo '<div class="alert alert-error">' . $invite_error_msg . '</div>';
    ?>
    <div class="content">
        <form method="post" action="<?php echo base_url('user/searchFriends/'); ?>">
            <table width="100%" border="0" cellspacing="5" cellpadding="5">
                <tr>
                    <td><?php echo $this->lang->line('name'); ?></td>
                    <td><input type="text" name="name" id="name" value="<?php echo isset($name) ? $name : ''; ?>" class="span12 textBoxGray" ></td>
                </tr>
                <tr>
                    <td><?php echo $this->lang->line('email'); ?> </td>
                    <td><input type="text" name="email" id="email" value="<?php echo isset($email) ? $email : ''; ?>" class="span12 textBoxGray"></td>
                </tr>
                <tr>
                    <td><?php echo $this->lang->line('location'); ?></td>
                    <td><input type="text" name="location" id="location" value="<?php echo isset($location) ? $location : ''; ?>" autocomplete="off" class="span12 textBoxGray">
                        <input type="hidden" name="location_id" id="location_id" value="<?php echo isset($location_id) ? $location_id : ''; ?>" autocomplete="off" class="span12 textLightBoxGray">
                        <div class="sutoSugSearch">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><textarea name="message" id="message" class="span12 textBoxGray"  placeholder="<?php echo $this->lang->line('add_your_message'); ?>"></textarea></td>
                </tr>
                <tr>
                    <td colspan ="2">
                        <button type="submit" id="invite_btn" class="btn-custom-darkblue" name="invite_by_email" style="width: 87px !important;margin-left: 2px;padding: 2px !important; margin-left:5px !important;height: 25px;" ><?php echo $this->lang->line('invite'); ?></button>
                        <button value="invite_glomp" type="submit" style="background-color:#d2d7e1; border:0px;width: 87px; padding:0px; border:solid 0px; margin-left:5px" name="invite_glomp" ><img src="assets/frontend/img/glompSubmitButton.png" style="border:0px; width:87px" width="87" /></button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>