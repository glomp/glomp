<div class="inviteFriendsRight">
    <h1 class="pull-left glompFont"><?php echo $this->lang->line('invite_friend'); ?></h1>       
    <div class="pull-right" style="margin-top:8px;margin-bottom:5px;">						
        <button type="button" class="btn-custom-blue btn-block" name="Invite_Through_Fb" id="Invite_Through_Fb" >&nbsp;<?php echo $this->lang->line('Facebook'); ?>&nbsp;</button>
    </div>
    <div class="clearfix"></div>
    <div style="position: relative;left: -53px;width: 436px;">
        <div class="triangle pull-left" style="border-bottom-width: 20px;border-right-width: 54px;position: relative; left: 1px;border-top-width: 26px;"></div>
        <div class="content pull-left">
            <div class="pull-left" style="width: 289px;">
                Is your friend shown here?
                <br />
                <br />
            </div>
            <div class="pull-left">
                <button type="button" class="btn-custom-gray" name="no_show_glomp" id="no_show_glomp" >&nbsp;No</button>
            </div>

            <div class="clearfix"></div>

            <div class="pull-left" style="min-width: 320px;min-height: 324px; padding: 10px;">
                <div id="error_container" class="alert alert-error hide" style="width: 269px;">
                    <button type="button" class="close" id="error_close_btn">&times;</button>
                    <div id ="errors" align="left"></div>
                </div>
                <form action ="<?php echo site_url('ajax_post/assignVoucherCheck'); ?>" id ="glomp_form" method="POST">
                    <input type="hidden" id="voucher_id" name="voucher_id" value="<?php echo $voucher_id; ?>" />
                    <input type="hidden" id="fb_id" name="fb_id"  />
                    <input type="hidden" id="fb_fname" name="fb_fname"  />
                    <input type="hidden" id="fb_lname" name="fb_lname"  />
                    <input type="hidden" id="fb_email" name="fb_email"  />
                    <input type="hidden" id="fb_dob" name="fb_dob"  />
                    <div style="padding: 5px;background-color: white;">
                        <div class="fl">
                            <label class="fl" style="width: 66px;color:#585F6B;font-weight:bold;">Name</label>
                            <input class="fl" type="text" name="name" id="name" style="width: 210px;">
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="fl">
                            <label class="fl" style="width: 66px;color:#585F6B;font-weight:bold;">Email</label>
                            <input class="fl" type="text" name="email" id="email" style="width: 210px;">
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="fl">
                            <label class="fl" style="width: 66px;color:#585F6B;font-weight:bold;">Location</label>
                            <input class="fl" type="text" autocomplete="off" name="location" id="location" style="width: 210px;">
                            <div class="sutoSugSearch" style="margin-top: 30px;left: auto;margin-left: 65px;"></div>
                            <input type="hidden" name="location_id" id="location_id" autocomplete="off" class="span12">
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div style="position: absolute; height: 1px;">
    <div class="glompItemPopUp" style="position: relative;top: -422px;left: -268px;">
        <div id="glompShow" class="glompItemPopUp">
            <div class="row-fluid">
                <div class="span12">
                    <div class="text-center"><img src="assets/frontend/img/logo-small.png" alt="Glomp Logo"></div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="alert alert-success"  id="showSuccess" style="display:none;">
                    <span id="showSuccessMessage"></span>
                    </div>
                    <div class="alert alert-error"  id="showError" style="display:none;">
                        <span id="showErrorMessage"></span>
                    </div>

                    <form name="frmGlomp" id="frmGlomp" method="post" action="">
                        <br/>
                        <textarea name="message" id="glomp_user_message" class="span12" placeholder="<?php echo $this->lang->line('Message'); ?>"></textarea>
                        <br/><br/>
                        <input type="password" name="password"  id="password" class="span12" placeholder="<?php echo $this->lang->line('Your_Password'); ?>">

                        <div class="text-center">
                            <div class="forgotPassword"><a href="<?php echo base_url('landing/reset_password'); ?>"><?php echo $this->lang->line('Forgot_Password'); ?></a></div>
                            <span class="Loader" style="display:none;"><i class="icon-spinner icon-spin icon-large"></i></span>  <button type="button" class="btn-custom-gray _send_glomp" data-loading-text="loading stuff..." name="Confirm" ><?php echo $this->lang->line('Confirm'); ?></button>
                            <button type="button" id="cancel_glomp_option" class="btn-custom-white  _close_popup" style="margin-left:100px;" name="Cancel" ><?php echo $this->lang->line('Cancel'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var site_url = '<?php echo site_url(); ?>';
    /*initialize*/
    searchForFriendAjax();

    $('#searchForFriend').keyup($.debounce(250, searchForFriendAjax));
    $('#name').keyup($.debounce(250, searchForFriendAjax));
	$('#email').keyup($.debounce(250, searchForFriendAjax));
	$('#location').keyup($.debounce(250, searchForFriendAjax));
	$('#location').change($.debounce(250, searchForFriendAjax));

    function changeSearchField() {
        $('#searchForFriend').val($(this).val());
		$('#name').val($('#searchForFriend').val());
        /*trigger search*/
        searchForFriendAjax();
    }

    function searchForFriendAjax() {
		$('#searchForFriend').val($('#name').val());
        /*If FB*/
        if (FB_WHAT_TO_DO != '') {
            return getKey();
        }

        $('#friendList').html('Searching...');
        $.ajax({	
            type: "post",
            url: site_url + 'ajax_post/searchForFriendsAjax',
            dataType: 'html',
            data: {
                name: $('#searchForFriend').val(),
				email: $('#email').val(),
				location_id: $('#location_id').val()
            },
            success: function(response) {
                $('#friendsList').html(response);
            }
        });
    }

    $('#no_show_glomp').click(function(e) {
        e.preventDefault();

        /*If the location is set to nothing*/
        if ($('#location').val() == '') {
            $('#location_id').val('');
        }

        var form = $('#glomp_form');
        var NewDialog = $('<div id="waitPopup" align="center">\
        <div align="center" style="margin-top:5px;">Please wait...<br><img width="40" src="<?php echo base_url(); ?>assets/m/img/ajax-loader.gif" /></div></div>');
        NewDialog.dialog({
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
            title: 'Please wait...',
            modal: true,
            position: 'center',
            width: 200,
            height: 120
        });
        NewDialog.dialog('open');


        $.ajax({
            type: "post",
            url: $(form).attr('action'),
            dataType: 'json',
            data: $(form).serialize(),
            success: function(response) {
                $("#waitPopup").dialog('destroy').remove();

                if (!response.success) {
                    process_error(response);
                    return;
                }

                $('#error_container').addClass('hide');

                if (response.email_exists == 1) {
                    showAddFriend(response.user);
                    return;
                }

                show_glomp();
            }
        });

    });

    function showAddFriend(user) {
        $('#member_name').html(user.fname + ' ' + user.lname);
        $('#member_id').attr('user_id', user.id);
        $('#member_id').show();

        /*Hide add friend if already friend*/
        if (user.friend_id != null) {
            $('#member_id').hide();
        }

        $('#addAsFriendPopUpID').show();
    }

    function process_error(response) {
        $('#errors').html('');
        for (var e in response.errors) {
            if (response.errors[e] != '') {
                $('#error_container').removeClass('hide');
                $('#errors').append(response.errors[e]);
                $('#' + e).addClass('error_border');
            }

            /*Remove any error message if theres any*/
            if (response.errors[e] == '') {
                $('#' + e).removeClass('error_border');
            }
        }
    }

    /*Bind to X button on error container*/
    $('#error_close_btn').click(function(e) {
        e.preventDefault();
        $('#error_container').addClass('hide');
    });

    /* start of glomp item popup */

    /*/open glomp box*/
    function show_glomp() {
        $('#glomp_user_message').show();
        
        if (FB_WHAT_TO_DO != '') {
           $('#glomp_user_message').hide();
        }

        var NewDialog = $('#glompShow');
        NewDialog.dialog({
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            dialogClass: 'dialog_style_glomp_wait noTitleStuff',
            title: 'Please wait...',
            modal: true,
            position: 'center',
            width: 400,
            height: 350
        });
        NewDialog.dialog('open')


        $("#showError").hide();
/*        $("#showSuccess").hide();
        var _this = $(this);
        $('.glompItemPopUp').fadeIn();*/
    }

    /* send glomp to friend/*/
    $('._send_glomp').click(function() {
        var msg = $('#glomp_user_message').val();
        if (msg == "")
        {
            if (!confirm("<?php echo $this->lang->line('glomp_confirmation_message'); ?>?"))
            {
                $('#glomp_user_message').focus();
                return false;
            }
        }
        $('#cancel_glomp_option').addClass('disabled');
        $('#cancel_glomp_option').removeClass('_close_popup');
        $(".Loader").show();
        $("#showError").hide();
        $("#showSuccess").hide();
        
        $.ajax({
            type: "POST",
            url: "profile/reassignVoucherNew/",
            data: {
                voucher_id: $('#voucher_id').val(),
                email: $('#email').val(),
                fname: $('#name').val(),
                message: $('#glomp_user_message').val(),
                password: $('#password').val(),
                fb_id: $('#fb_id').val(),
                fb_fname: $('#fb_fname').val(),
                fb_lname: $('#fb_lname').val(),
                fb_email: $('#fb_email').val(),
                fb_dob: $('#fb_dob').val(),
                location_id: $('#location_id').val(),
                is_fb: FB_WHAT_TO_DO
            },
            success: function(data) {
                var d = eval("(" + data + ")");
                if (d.status == 'success')
                {
                    $("#showSuccess").show();
                    $("#showSuccessMessage").text(d.msg);
                    $('#glompShow').dialog('destroy');
                    
                    if (FB_WHAT_TO_DO != '') {
                        return fbSendGlomp(d.voucher_id, $('#fb_id').val());
                    }
                    
                    location.href = "<?php echo base_url(); ?>";
                }
                else if (d.status == 'error')
                {
                    $(".Loader").hide();
                    $("#showError").show();
                    $("#showErrorMessage").text(d.msg);
                }
            }
        });
    });
    /*close glomp popup box*/

    $('#cancel_glomp_popup').click(function() {
        $('#glomp_non_popup').fadeOut();
    });

    $('._close_popup').click(function() {
        /*$('.glompItemPopUp').fadeOut();*/
        $("#glompShow").dialog('destroy');
    });
    /* end of glomp item popup */
    $(window).resize(function() {
        $(".ui-dialog-content").dialog("option", "position", "center");
    });
</script>