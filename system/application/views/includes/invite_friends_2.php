<div class="inviteFriendsRight">
    <h1 class="pull-left glompFont"><?php echo $this->lang->line('invite_friend'); ?></h1>       
    <div class="pull-right" style="margin-top:8px;margin-bottom:5px;">						
        <button type="button" class="btn-custom-blue btn-block" name="Invite_Through_Fb" id="Invite_Through_Fb" >&nbsp;<?php echo $this->lang->line('Facebook'); ?>&nbsp;</button>
    </div>
    <div class="clearfix"></div>
    <?php if($num_found_result != 0) { ?>
    <div style="position: relative;left: -53px;width: 436px;">
        <div class="triangle pull-left" style="border-bottom-width: 66px;border-right-width: 65px;position: relative; left: 1px;"></div>
        <div class="content pull-left">
            <div class="pull-left" style="width: 289px;">
                Is your friend shown here? <br />
                If so, add them.
                <br />
                <br />
            </div>
            <div class="pull-left">
                <button id="no_show_glomp" type="button" class="btn-custom-gray" name="no_show_glomp" id="no_show_glomp" >&nbsp;No</button>
            </div>

            <div class="clearfix"></div>

            <div class="pull-left" style="width: 33px;">
                <div><img src ="<?php echo base_url('assets/frontend/img/search-green.png'); ?>"></div>
            </div>
            <div class="pull-left" style="width: 100px;">
                <span style="font-size: 14px;"><?php echo $name; ?></span><br />
                <?php echo $email; ?><br />
                <?php echo $location; ?><br />                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php } ?>
</div>


<div id="glomp_non_popup" style="position: absolute; display:none; width: 0px;">
    <div id="glomp_non_popup_relative" style="position: relative; left: -443px;background-color: #59606B;padding: 10px; border-radius: 5px;top: -127px;width: 667px;" >;
        <div class="row-fluid" style="color:white;font-size: 10px; font-weight: bold;margin-bottom: 10px;margin-top: 10px;">
            <span style="font-size: 16px;"><?php echo $name; ?></span><br />
            <?php echo $email; ?><br />
            <?php echo $location; ?><br />
        </div>
        <div class="row-fluid">
            <div class="span12" style="margin-top:-3px;">
                <div class="profileTab">
                    <ul>
                        <li><a class="activeTab" id="tbMerchant" href="#tabMerchant"><?php echo ucfirst($this->lang->line('merchants')); ?></a></li>
                        <li><a href="#tabDrink"><?php echo ucfirst($this->lang->line('drinks')); ?></a></li>
                        <li><a href="#tabSnaks"><?php echo ucfirst($this->lang->line('snacks')); ?></a></li>
                        <li><a href="#tabCocktails"><?php echo ucfirst($this->lang->line('cocktails')); ?></a></li>
                        <li><a href="#tabSweets"><?php echo ucfirst($this->lang->line('sweets')); ?></a></li>
                        <li><a href="#tabOthers"><?php echo ucfirst($this->lang->line('others')); ?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tabContent tabs" id="tabMerchant" style="display: block;"> 
                    <div class="tabInnerContent">
                        <?php
                        $num_merchant = $region_wise_merchant->num_rows();
                        if ($num_merchant > 0) {
                            $i = 1;
                            $j = 1;
                            foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                if ($i == 1) {
                                    ?> <ul class="thumbnails"> <?php } ?>
                                    <li class="span2 merchantShadow">
                                        <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>"   href="<?php echo base_url("profile/merchantProductNonMember/" . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                            <div class="thumbnail">
                                                <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>">
                                            </div>
                                        </a>
                                    </li>
                                    <?php if ($i == 6 || $j == $num_merchant) { ?>
                                    </ul>
                                    <?php
                                }
                                $j++;
                                if ($i == 6) {
                                    $i = 1;
                                }
                                else
                                    $i++;
                            } /*/ end of foreach*/
                        }/*enf of num result check*/
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabDrink" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 1;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabSnaks" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 2;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>

                <div class="tabContent tabs" id="tabCocktails" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 3;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>


                <div class="tabContent tabs" id="tabSweets" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 4;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabOthers" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                        $data_v['cat_id'] = 5;
                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="merchantProductDetails" style="display: none;">
                    <?php /*/ to load merchant*/?>
                </div> 
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <div class="pull-right">
                <button type="button" id="cancel_glomp_popup" class="btn-custom-white  _close_popup" style="margin-left:100px;" name="Cancel" ><?php echo $this->lang->line('Cancel'); ?></button>
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
</div>

<script>
    $(".tabs").hide();
    $("#glomped_inactive").hide();

    $(".tabs:first").show();
    $(".profileTab a").click(function(e) {
        var activeTab = $(this).attr('href');
        if (activeTab == "#tabBuz")
        {
            $("#glomped_active").show();
            $("#glomped_inactive").hide();
        }
        else
        {
            $("#glomped_inactive").show();
            $("#glomped_active").hide();
        }

        e.preventDefault();
        $(".profileTab a").removeClass("activeTab");
        $(this).addClass("activeTab");
        $(".tabs").hide();
        $(activeTab).fadeIn();
    });

    $('.nameOfMerchant').click(function(e) {
        e.preventDefault();
        var _anchor = $(this).attr('href');
        
        $('#merchantProductDetails').load(_anchor, function() {
            $('#tabMerchant').hide();
            $('#merchantProductDetails').fadeIn();
        });

    });
    
    <?php if($num_found_result == 0) { ?>
        $('#glomp_non_popup').fadeIn();
    <?php } ?>
        
    $('#no_show_glomp').click(function() {
        $('#glomp_non_popup').fadeIn();
    });

    $('#cancel_glomp_popup').click(function() {
        $('#glomp_non_popup').fadeOut();
    });
</script>

<?php
    $data['message']=$message;
    $this->load->view('includes/glomp_pop_up_nonmember');
    $this->load->view('includes/glomp_pop_up_js_nonmember',$data);
?>