<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $page_title; ?></title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="<?php echo minify('assets/frontend/css/bootstrap.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="favicon.ico" rel="shortcut icon">
        <link href="<?php echo minify('assets/frontend/css/style.css', 'css', 'assets/frontend/css'); ?>" rel="stylesheet">
        <link href="<?php echo minify('assets/frontend/css/south-street/jquery-ui-1.10.3.custom.grey.css', 'css', 'assets/frontend/css/south-street'); ?>" rel="stylesheet">        
        
        <?php foreach ($css as $css): ?>
        <?php echo $css; ?>
        <?php endforeach; ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="<?php echo minify('assets/frontend/js/jquery-ui-1.10.3.custom.js', 'js', 'assets/frontend/js'); ?>"></script>
        <script src="<?php echo minify('assets/frontend/js/bootstrap.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
        
        <script>
            var GLOMP_BASE_URL = "<?php echo base_url(); ?>";
        </script>
        
        <?php foreach ($js as $js): ?>
        <?php echo $js; ?>
        <?php endforeach; ?>
        <!-- the javascript inline code  has been moved the below file-->
        <?php /* <script src="<?php echo minify('assets/frontend/js/custom.landing.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script> */ ?>
        <script src="<?php echo minify('assets/frontend/js/custom.js', 'js', 'assets/frontend/js'); ?>" type="text/javascript"></script>
</head>
<body>
    <?php include_once("analyticstracking.php") ?>
    <div id="fb-root"></div>
<div class="header"><div class="container">
        <div class="inner-content">
            <div class="row-fluid">
                <div class="span3"><a href="<?php echo base_url(); ?>"><img src="assets/frontend/img/logo-small.png" alt="Glomp Logo"></a></div>
                <div class="span9 innerTopNav">
                    <?php
                    if ($this->session->userdata('user_id') && $this->session->userdata('user_id') > 0) {

                        $user_id = $this->session->userdata('user_id');
                        $res_header = json_decode($this->user_account_m->user_summary($user_id));

                        $user_info = json_decode($this->user_account_m->user_short_info($user_id));
                        ?>
                        <div class="settings"><?php echo anchor('user/update', $this->lang->line('setting', 'setting')); ?></div>
                        <div class="faq"><?php echo anchor('page/index/faq', $this->lang->line('faq', 'faq')); ?></div>
                        <div class="faq"><?php echo anchor('user/searchFriends', $this->lang->line('search_my_friends', 'Friends')); ?></div>
                        <span class="faq"><?php echo anchor('user/', strtoupper($this->lang->line('Home', 'Home'))); ?></span> 


                        <div class="pull-right" style="width:390px; text-align:right;">
                            <div class="glomp"><a href="<?php echo base_url(); ?>glomp">
                                    <?php
                                    $active_voucher = $this->users_m->count_active_voucher($user_id);
                                    if ($active_voucher > 0) {
                                        ?>
                                        <div class="notification"><?php echo $active_voucher; ?></div><?php } ?></a></div>
                            <span class="cusMenuHolder" style="padding-top:5px;">
                                <span class="user_name "><a href="javascript:void(0);" > <?php echo stripslashes($user_info->user_fname . ' ' . $user_info->user_lname); ?></a></span> 
                                <span class="redPontHolder" id="point_balance" ><?php if (isset($res_header->point_balance)) echo $res_header->point_balance;
                                else echo "0"; ?></span> 
                                <div class="cusMenu" style="margin-left: 233px;">
                                    <script language="javascript" type="application/javascript">
                                        $(document).ready(function() {
                                        $(".cusMenuHolder").on({
                                        mouseenter: function (e) {
                                        e.stopPropagation();
                                        $(".cusMenu").show();				

                                        },
                                        mouseleave: function (e) {
                                        e.stopPropagation();

                                        }		
                                        });

                                        $('#user_menu li').on({
                                        mouseenter: function (e) {
                                        e.stopPropagation();
                                        $(".cusMenu").show();				

                                        },
                                        mouseleave: function (e) {
                                        e.stopPropagation();
                                        $(".cusMenu").hide();				

                                        }		
                                        });





                                        $("body").click(function(){
                                        $(".cusMenu").hide();
                                        });
                                        $('body').on("mouseover", function (e) {


                                        });
                                        $('#user_menu li').click(function(e) {
                                        var href = $(this).find('a').attr('href');
                                        window.location=href;
                                        });
                                        });
                                    </script>
                                    <ul id="user_menu" >
                                        <li><?php echo anchor('profile', $this->lang->line('profile')); ?> </li>
                                        <li><?php echo anchor('buyPoints', $this->lang->line('buy_points')); ?> </li>
                                        <li><?php echo anchor('activity', $this->lang->line('activity_log')); ?> </li>
                                        <?php /* ?> <li><?php echo anchor('user/menu',$this->lang->line('menu'));?> </li>
                                          <li><?php echo anchor('user/favourites',$this->lang->line('favourites'));?> </li><?php */ ?>
                                        <li><?php echo anchor('user/logOut', $this->lang->line('log_out')); ?> </li>
                                    </ul>
                                </div>
                            </span>

                        </div> 
                        <?php
                    }else {
                        ?>
                        <div class="faq"><?php echo anchor('page/index/faq', $this->lang->line('faq')); ?></div>
                        <span class="faq"><?php echo anchor('', $this->lang->line('Home')); ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div></div>



