<div id="glomp_non_popup" style="position: absolute; display:none; width: 0px;">
    <div id="glomp_non_popup_relative" style="position: relative; left: 100px;background-color: #59606B;padding: 10px; border-radius: 5px;top: -100px;width: 667px;" >;
        <div class="row-fluid" style="color:white;font-size: 10px; font-weight: bold;margin-bottom: 10px;margin-top: 10px;">
            <span style="font-size: 16px;"><?php echo $user_record->user_fname . ' ' . $user_record->user_lname; ?></span><br />
            <?php echo $user_record->user_email; ?><br />
            <?php echo $this->regions_m->region_name($user_record->user_city_id);; $location_id=$user_record->user_city_id; ?><br />
        </div>
        <div class="row-fluid">
            <div class="span12" style="margin-top:-3px;">
                <div class="profileTab">
                    <ul>
                        <li><a class="activeTab" id="tbMerchant" data-href="#tabMerchant"><?php echo ucfirst($this->lang->line('merchants')); ?></a></li>
                        <li><a data-href="#tabDrink"><?php echo ucfirst($this->lang->line('drinks')); ?></a></li>
                        <li><a data-href="#tabSnaks"><?php echo ucfirst($this->lang->line('snacks')); ?></a></li>
                        <li><a data-href="#tabCocktails"><?php echo ucfirst($this->lang->line('cocktails')); ?></a></li>
                        <li><a data-href="#tabSweets"><?php echo ucfirst($this->lang->line('sweets')); ?></a></li>
                        <li><a data-href="#tabOthers"><?php echo ucfirst($this->lang->line('others')); ?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tabContent tabs" id="tabMerchant" style=""> 				
                    <div class="tabInnerContent">					
                        <?php						
                        $num_merchant = $region_wise_merchant->num_rows();
                        if ($num_merchant > 0) {
                            $i = 1;
                            $j = 1;
                            foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                if ($i == 1) {
                                    ?> <ul class="thumbnails"> <?php } ?>
                                    <li class="span2 merchantShadow">
                                        <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>"   href="<?php echo base_url("profile/merchantProductNonMember/" . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                            <div class="thumbnail">
                                                <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>">
                                            </div>
                                        </a>
                                    </li>
                                    <?php if ($i == 6 || $j == $num_merchant) { ?>
                                    </ul>
                                    <?php
                                }
                                $j++;
                                if ($i == 6) {
                                    $i = 1;
                                }
                                else
                                    $i++;
                            } /// end of foreach
                        }//enf of num result check
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabDrink" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 1;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabSnaks" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 2;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>

                <div class="tabContent tabs" id="tabCocktails" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 3;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>


                <div class="tabContent tabs" id="tabSweets" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 4;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabOthers" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                        $data_v['cat_id'] = 5;
                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="merchantProductDetails" style="display: none;">
                    <?php // to load merchant?>
                </div> 
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <div class="pull-right">
                <button type="button" id="cancel_glomp_popup" class="btn-custom-white  _close_popup" style="margin-left:100px;" name="Cancel" ><?php echo $this->lang->line('Cancel'); ?></button>
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
</div>

<script>
    $('#glomp_non_member_from_profile').click(function() {		
        $('#glomp_non_popup').fadeIn();
    });

    $('#cancel_glomp_popup').click(function() {
        $('#glomp_non_popup').fadeOut();
    });
    /*$('#glomp_non_member_from_profile').click(function() {		
        $('#glomp_non_popup').fadeIn();
    });

    $('#cancel_glomp_popup').click(function() {
        $('#glomp_non_popup').fadeOut();
    });*/
</script>

<?php
///    $this->load->view('includes/glomp_pop_up_js_nonmember');
?>