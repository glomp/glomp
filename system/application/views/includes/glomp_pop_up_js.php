<script src="assets/frontend/js/validate.js" type="text/javascript"></script>
<script src="assets/frontend/js/custom.glomp_share.js" type="text/javascript"></script>
<script type="text/javascript">
var FB_APP_ID   ="<?php echo ($this->custom_func->config_value('FB_API_APP_ID_'.FACEBOOK_API_STATUS));?>";	
	$().ready(function() {
		/*/ validate signup form on keyup and submit*/
		$("#frmGlomp").validate({
			rules: {
				password:"required"
				},
			messages: {
				password:""
			}
	});
	});
	</script>
    
<script type="text/javascript">
var glomped=false;
$(document).ready(function(e) {
	/* start of glomp item popup */
	 
	/*/ align pop ip box to the center of the window*/
	var _y = 100;/*/($(window).height()/2)-(parseInt($(".glompItemPopUp").css("height"))/2);*/
	var _x = ($(window).width()/2)-(parseInt($(".glompItemPopUp").css("width"))/2);	
	$(".glompItemPopUp").css({"margin-top":_y,"margin-left":_x}); 
	
	$( "#frmGlomp" ).submit(function( event ) {									
		$('._send_glomp').click();
		event.preventDefault();
	});
	
	/*/open glomp box*/
	$('.glompItem, .glompItemFromFev').on('click',function(){
		$("#showError").hide();
		$("#showSuccess").hide();
		var _this = $(this);
		$('.glompItemPopUp').fadeIn();
		var _image = _this.data('image');
		var _point = _this.data('point');
		var _productName = _this.data('productname');
		var _merchantName = _this.data('merchantname');
		var _id = _this.data('id');
		$("#pipup_image").attr('src',_image);
		$("#popUpItem").text(_productName);
		$("#popUpPoints").text(_point);
		$("#popUpMerchant").text(_merchantName);
		
		$("#product_id").val(_id);
		});
		
		/*/ send glomp to friend/*/
	$('._send_glomp').on('click',function(){	
		if(!glomped)
		{
			glomped=true;
		
			var msg = $('#glomp_user_message').val();
			if(msg == "")
			{
                var NewDialog = $('<div id="" align="center" style="padding-top:25px !important;">\
                <?php echo addslashes($this->lang->line('glomp_confirmation_message'));?>\
								</div>');
                NewDialog.dialog({                    
                    dialogClass:'noTitleStuff dialog_style_blue_grey',
                    title: "",
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 320,
                    height: 140,
                    show: '',
                    hide: '',
                    buttons: [{text: 'Add Message',
                            "class": 'btn-custom-darkblue',
                            click: function() {
                                $(this).dialog("close");
                                glomped=false;
                                $('#glomp_user_message').focus();
                                
                            }},
                        {text: "No Thanks",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                doSendGlomp();                                
                            }}
                    ]
                });
                NewDialog.dialog('open');
            
				/*if(!confirm("<?php echo $this->lang->line('glomp_confirmation_message');?>?"))
					{
						$('#glomp_user_message').focus();
						return false;
                }*/
			}
            else
            {
                doSendGlomp();
            }   
			
        }
    });
			/*close glomp popup box*/
		$('._close_popup').click(function(){ $('.glompItemPopUp').fadeOut();});
		/* end of glomp item popup */
		
		
	});
    
    function doSendGlomp(){
        $('#cancel_glomp_option').addClass('disabled');
        $('#cancel_glomp_option').removeClass('_close_popup');
        $(".Loader").show();
        $("#showError").hide();
        $("#showSuccess").hide();
        $.ajax({
            type: "POST",
            url:"profile/glompToUser/<?php echo $profile_id;?>",
            data: $('#frmGlomp').serialize(),
            success: function(data){
                var d = eval("("+data+")");
                if(d.status=='success')
                {
                    
                    $('#glomp_user_message').val('');
                    $('.password_this').val('');
                    $(".Loader").hide();
                    glomped=false;
                    /*$("#showSuccess").show();*/
                    /*$("#showSuccessMessage").text(d.msg);*/
                    var response=d;
                    var details=d;
                    
                    /* success*/
                    tryAutoShare(details);
                    
                    
                    $('.glompItemPopUp').hide();
                    var NewDialog = $('<div id="confirmPopup" align="center">\
                                        <div align="center" style="margin:5px 0px;">'+response.msg+'.<br><span style="font-size:14px;">Would you like to share the news?</span></div>\
                                        <div id="success_buttons_wrapper" class="cl" align="center" style="padding:15px 0px;"></div>\
                                        <div style="height:0px;" class="cl">\
                                            <div id="share_wrapper_box_up" class="cl fl share_wrapper_box_up" style="" align="center">\
                                                <button data-merchant="'+details.merchant_name+'"  data-belongs="'+details.to_name_real+'" data-purchaser="'+details.form_name_real+'" data-from_fb_id="'+details.from_fb_id+'" data-to_fb_id="'+details.to_fb_id+'" data-story_type="'+details.story_type+'" data-prod_name="'+details.prod_name+'" data-prod_img="'+details.product_logo+'"  data-voucher_id="'+details.voucher_id+'" title="Share on Facebook" style="background:url(\'<?php echo base_url('assets/frontend/img/fb_icon_mini.jpg');?>\'); background-size: 27px;background-position: center;" class="share_on_facebook btn-custom-blue_xs_fb"   ></button>\
                                                <button data-merchant="' + details.merchant_name + '"  data-belongs="' + details.to_name_real + '" data-purchaser="' + details.form_name_real + '" data-from_fb_id="' + details.from_fb_id + '" data-to_fb_id="' + details.to_fb_id + '" data-story_type="' + details.story_type + '" data-prod_name="' + details.prod_name + '" data-prod_img="' + details.product_logo + '"  data-voucher_id="' + details.voucher_id + '" title="Share on LinkedIn" style="background:url(\'<?php echo base_url('assets/frontend/img/li_icon_mini.jpg'); ?>\'); background-size: 20px;background-position: center;" class="share_on_linkedin btn-custom-light_blue_xs_tweet"   ></button>\
                                                <button  data-from="<?php echo $profile_id;?>" data-voucher_id="'+details.voucher_id+'" title="Share on Twitter"  style="background:url(\'<?php echo base_url('assets/frontend/img/tweeter_icon_mini.jpg');?>\'); background-size: 27px;background-position: center;" class="share_on_twitter btn-custom-light_blue_xs_tweet"  ></button>\
                                            </div>\
                                        </div>\
                                        </div>');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                        title:'',
                        modal: true,
                        width:300,
                        position: 'center',
                        height:140									
                    });
                    NewDialog.dialog('open'); 
                    $elem = $('<button type="button" class=" w100px glomp_fb_share btn-custom-blue-grey_normal fl" style="height:30px;"  >Share</button>');
                    $elem.on('click', function(e) {
                            $('#share_wrapper_box_up').toggle();
                    });
                    $("#success_buttons_wrapper").append($elem);
                    
                    
                    $elem = $('<button type="button" class=" w100px btn-custom-white  fr"   style=""  >Close</button>');
                    $elem.on('click', function(e) {
                        $('#confirmPopup').dialog("close");
                        setTimeout(function() {
                            $('#confirmPopup').dialog('destroy').remove();
                        }, 500 );
                    });
                    $("#success_buttons_wrapper").append($elem);
                    /*share */ 
                    /*success*/
                    
                    /*location.href="<?php echo base_url();?>";*/
                }
                else if(d.status=='error')
                {
                    $('.password_this').val('');
                    glomped=false;
                    $(".Loader").hide();
                    $("#showError").show();
                    $("#showErrorMessage").text(d.msg);
                }
            }
        });
    }
</script>
