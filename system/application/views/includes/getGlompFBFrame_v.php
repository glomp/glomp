
        <div class="row-fluid" style="color:white;font-size: 10px; font-weight: bold;margin-bottom: 10px;margin-top: 10px;">
            <span style="font-size: 16px;"><?php echo $name; ?></span><br />            
            <?php echo $location; ?><br />
        </div>
        <div class="row-fluid">
            <div class="span12" style="margin-top:-3px;">
                <div class="profileTab">
                    <ul>
                        <li><a class="activeTab" id="tbMerchant" href="#tabMerchant"><?php echo ucfirst($this->lang->line('merchants')); ?></a></li>
                        <li><a href="#tabDrink"><?php echo ucfirst($this->lang->line('drinks')); ?></a></li>
                        <li><a href="#tabSnaks"><?php echo ucfirst($this->lang->line('snacks')); ?></a></li>
                        <li><a href="#tabCocktails"><?php echo ucfirst($this->lang->line('cocktails')); ?></a></li>
                        <li><a href="#tabSweets"><?php echo ucfirst($this->lang->line('sweets')); ?></a></li>
                        <li><a href="#tabOthers"><?php echo ucfirst($this->lang->line('others')); ?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tabContent tabs" id="tabMerchant" style="display: block;"> 
                    <div class="tabInnerContent">
                        <?php
                        $num_merchant = $region_wise_merchant->num_rows();
                        if ($num_merchant > 0) {
                            $i = 1;
                            $j = 1;
                            foreach ($region_wise_merchant->result() as $row_mercahnt) {
                                if ($i == 1) {
                                    ?> <ul class="thumbnails"> <?php } ?>
                                    <li class="span2 merchantShadow">
                                        <a id="merID_<?php echo $row_mercahnt->merchant_id; ?>"   href="<?php echo base_url("profile/merchantProductNonMember/" . $row_mercahnt->merchant_id) ?>" class="nameOfMerchant">
                                            <div class="thumbnail">
                                                <img src="<?php echo $this->custom_func->merchant_logo($row_mercahnt->merchant_logo) ?>" alt="<?php echo $row_mercahnt->merchant_name; ?>">
                                            </div>
                                        </a>
                                    </li>
                                    <?php if ($i == 6 || $j == $num_merchant) { ?>
                                    </ul>
                                    <?php
                                }
                                $j++;
                                if ($i == 6) {
                                    $i = 1;
                                }
                                else
                                    $i++;
                            } /// end of foreach
                        }//enf of num result check
                        ?>
                    </div>
                </div>				
                <div class="tabContent tabs" id="tabDrink" style="display: block;">
                    <div class="tabInnerContent">					
                        <?php
                            $data_v['profile_region_id'] = $location_id;							
                            $data_v['cat_id'] = 1;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabSnaks" style="display: none;">
                    <div class="tabInnerContent">
					asdasdasdasdasdasdasdasd
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 2;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>

                <div class="tabContent tabs" id="tabCocktails" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 3;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>


                <div class="tabContent tabs" id="tabSweets" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                            $data_v['profile_region_id'] = $location_id;
                            $data_v['cat_id'] = 4;
                            $this->load->view('profile_tab/user_profile_cat_tab_nonmember_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="tabOthers" style="display: none;">
                    <div class="tabInnerContent">
                        <?php
                        $data_v['cat_id'] = 5;
                        $this->load->view('profile_tab/user_profile_cat_tab_v', $data_v)
                        ?>
                    </div>
                </div>
                <div class="tabContent tabs" id="merchantProductDetails" style="display: none;">
                    <?php // to load merchant?>
                </div> 
            </div>
        </div>                