<div class="uploadImage" style="height:140px;width:140px;overflow:visible; border:0px solid">
    <img src="assets/frontend/img/default_profile_pic_register.jpg" alt="" id="profile_pic" style="height:100%;"  />
</div>
<div class="upload">
    <span id="uploadFile" style="" ><?php echo $this->lang->line('upload_profile_photo'); ?></span><span class="required">*</span>
    <span id="upload_loader"></span>
    <p><?php echo $this->lang->line('Note'); ?>: <?php echo $this->lang->line('profile_image_upload_note'); ?></p>
</div>
<div id="fileUploadField_wrapper">
    <input type="file" name="user_photo" value="" accept="image/*" style="visibility:" id="fileUploadField" />
</div>
<input type="hidden" id="ie9" name="ie9" value="ie9"  />
<input type="hidden" id="x" name="x" value="0"  />
<input type="hidden" id="y" name="y" value="0" />
<input type="hidden" id="w" name="w" value="100" />
<input type="hidden" id="h" name="h" value="100" />
<input type="hidden" id="profile_pic_latest" name="profile_pic_latest" value="" />
<input type="hidden" id="profile_pic_orig" name="profile_pic_orig" value="" />
<img src="" id="tempImg" style="display:none" />
<script>
    $(document).ready(function(e) {            
        $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
    });
            var jcrop_api;
            var JcropW=0;
            var JcropH=0;
			function initJcrop()/*{{{*/
			{                
                
				jcrop_api = $.Jcrop($('#jCrop_target'), {                    
                    onSelect: updateCoords,                   
                    onChange: updateCoords,
                    onRelease: updateCoords
                });			  
				jcrop_api.setOptions({
					minSize: [ 140, 140 ],					
					aspectRatio: 1/1 
				});
				jcrop_api.animateTo([140,140,0,0]);
			}
			  function updateCoords(c)
			  {
				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
			  };

			 function resetCoords()
			  {
				$('#x').val(0);
				$('#y').val(0);
				$('#w').val(140);
				$('#h').val(140);
			  };	
      /*for profile picture	*/
     function UpdatePreviewSample(_this)
    {
        asdasd();
    }    
    function asdasd()
    {
		var form = $('#frmRegister');
		$('#waitPopup').dialog("close");
		$('#waitPopup').dialog('destroy').remove();																	
		var NewDialog = $('<div id="waitPopup" align="center">\
									<div align="center" style="margin-top:5px;">Please wait. The large image file is uploading.<br><img width="40"  style="width:40px"  src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');																
		NewDialog.dialog({						
			autoOpen: false,
			closeOnEscape: false ,
			resizable: false,					
			dialogClass:'dialog_style_glomp_wait noTitleStuff',
			title:'Please wait...',
			modal: true,
			position: 'center',
			width:240,
			height:120
		});
		NewDialog.dialog('open');
		
		$(form).ajaxSubmit({
			type: "post",
			url: GLOMP_BASE_URL+'ajax_post/upload_temp',
			dataType: 'json',
			data: $(form).serialize(),
			success: function(response) {
				if(response.status=='Error'){
					$('#waitPopup').dialog("close");																	
					$('#waitPopup').dialog('destroy').remove();																	
					var NewDialog = $('<div id="waitPopup" align="center">\
									<h4 style="padding:0px;margin:0px;">Error</h4>\
									<div align="center" style="margin-top:5px;">'+response.message+'</div></div>');																
					NewDialog.dialog({						
						autoOpen: false,
						closeOnEscape: false ,
						resizable: false,					
						dialogClass:'dialog_style_glomp_wait noTitleStuff',
						title:'Please wait...',
						modal: true,
						position: 'center',
						width:240,
						buttons:[
							{text: "Cancel",
							"class": 'btn-custom-white2',
							click: function() {
								$(this).dialog("close");
								setTimeout(function() {
									$('#waitPopup').dialog('destroy').remove();
								}, 500);
							}}
						]
						
						
					});
					NewDialog.dialog('open');
				
				}
				else{					
					$("#tempImg").prop("src", GLOMP_BASE_URL+"custom/uploads/users/temp/"+response.img);					
					$('#tempImg').waitForImages(function() {
						$('#waitPopup').dialog("close");
						$('#waitPopup').dialog('destroy').remove();					
						UpdatePreview(response);
					});					
				/*
					$("#profile_pic").prop("src", "custom/uploads/users/temp/"+response.thumb);
					$("#profile_pic_latest").val(response.thumb);
					$("#profile_pic_orig").val(response.orig);
					$('#waitPopup').dialog("close");
					setTimeout(function() {
						$('#waitPopup').dialog('destroy').remove();
					},100);
				*/
				}													
			}
		});
	}  
    function  UpdatePreview(target)
        {
            /*alert(target.result);*/
            /*$("#profile_pic").prop("src", target.result);*/
            /*$("#jCrop_target").prop("src", ($("#profile_pic").prop("src")));*/
            var tempW=target.w;
            var tempH=target.h;
            JcropW=tempW;
            JcropH=tempH;
            setTimeout(function()
            {
                if(( (parseInt(tempW) <140 ) ) 
                || ( (parseInt(tempH) <140 ) ))
                {
                    $('#waitPopup').dialog("close");																	
                    $('#waitPopup').dialog('destroy').remove();																	
                    var NewDialog = $('<div id="waitPopup" align="center">\
                                    <h4 style="padding:0px;margin:0px;">Error</h4>\
                                    <div align="center" style="margin-top:5px;font-size:14px">Sorry this image is too small. Please select a larger one.</div></div>');																
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false ,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                        title:'Please wait...',
                        modal: true,
                        position: 'center',
                        width:300,
                        height:140,
                        buttons:[
                            {text: "Ok",
                            "class": 'btn-custom-white2',
                            click: function() {
                                $(this).dialog("close");
                                setTimeout(function() {
                                    $('#waitPopup').dialog('destroy').remove();
                                }, 500);
                            }}
                        ]
                        
                        
                    });
                    NewDialog.dialog('open');
                }
                else
                {
                    var popW=300;
                    var popH=300;
                    if( (parseInt(tempW+120)*1) > parseInt(popW) )
                    {												
                        popW=tempW+120;
                        
                    }
                    if( (parseInt(tempH+80)*1) > parseInt(popH) )
                    {												
                        popH=tempH+80;
                        
                    }
                    
                    if (popW>1200)
                        popW=1200;											
                    if(popH>800)
                        popH=800;	
                    
                    /*create a popup for jcrop*/
                    /*create a popup for jcrop*/
                    $('#jCropDialog').dialog('destroy').remove();
                    var NewDialog = $('<div id="jCropDialog" align="center">\
                                            <img style="width:'+tempW+'px;height'+tempH+':px" id="jCrop_target" alt="" class="fixed-size" />\
                                        </div>');
                    NewDialog.dialog({						
                        autoOpen: false,
                        closeOnEscape: false,
                        resizable: false,					
                        dialogClass:'dialog_style_glomp_wait noTitleStuff contentOverflowAuto',
                        title:'Please wait...',
                        modal: true,
                        position: 'center',
                        width:popW,
                        height:popH,
                        buttons: [
                            {text: "Crop",
                                "class": 'btn-custom-blue',
                                click: function() {											
                                    var form = $('#frmRegister');							
                                    $('#jCropDialog').dialog("close");
                                    var NewDialog = $('<div id="waitPopup" align="center">\
                                                                <div align="center" style="margin-top:5px;">Please wait...<br><img  style="width:40px"  width="40" src="'+GLOMP_BASE_URL+'assets/m/img/ajax-loader.gif" /></div></div>');																
                                    NewDialog.dialog({						
                                        autoOpen: false,
                                        closeOnEscape: false ,
                                        resizable: false,					
                                        dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                        title:'Please wait...',
                                        modal: true,
                                        position: 'center',
                                        width:200,
                                        height:120
                                    });
                                    NewDialog.dialog('open');
                                    
                                    $(form).ajaxSubmit({
                                        type: "post",
                                        url: GLOMP_BASE_URL+'ajax_post/crop_register',
                                        dataType: 'json',
                                        data: $(form).serialize(),
                                        success: function(response) {
                                            if(response.status=='Error'){
                                                $('#waitPopup').dialog("close");																	
                                                $('#waitPopup').dialog('destroy').remove();																	
                                                var NewDialog = $('<div id="waitPopup" align="center">\
                                                                <h4 style="padding:0px;margin:0px;">Error</h4>\
                                                                <div align="center" style="margin-top:5px;">'+response.message+'</div></div>');																
                                                NewDialog.dialog({						
                                                    autoOpen: false,
                                                    closeOnEscape: false ,
                                                    resizable: false,					
                                                    dialogClass:'dialog_style_glomp_wait noTitleStuff',
                                                    title:'Please wait...',
                                                    modal: true,
                                                    position: 'center',
                                                    width:200,
                                                    height:120,
                                                    buttons:[
                                                        {text: "Cancel",
                                                        "class": 'btn-custom-white2',
                                                        click: function() {
                                                            $(this).dialog("close");
                                                            setTimeout(function() {
                                                                $('#waitPopup').dialog('destroy').remove();
                                                            }, 500);
                                                        }}
                                                    ]
                                                    
                                                    
                                                });
                                                NewDialog.dialog('open');
                                            
                                            }
                                            else{
                                                $("#profile_pic").prop("src", "custom/uploads/users/temp/"+response.thumb);
                                                $("#profile_pic_latest").val(response.thumb);
                                                $("#profile_pic_orig").val(response.orig);
                                                $('#waitPopup').dialog("close");
                                                setTimeout(function() {
                                                    $('#waitPopup').dialog('destroy').remove();
                                                },100);
                                            }
                                            $('#fileUploadField').remove();                                                
                                            $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:" id="fileUploadField" />');
                                            $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
                                        }
                                    });
                                }},
                            {text: "Cancel",
                                "class": 'btn-custom-white2',
                                click: function() {
                                    $('#fileUploadField').remove();                                                
                                    $('#fileUploadField_wrapper').append('<input type="file" name="user_photo" value="" accept="image/*" style="visibility:" id="fileUploadField" />');
                                    $('#fileUploadField').on('change', function(){ UpdatePreviewSample(); });
                                    $(this).dialog("close");
                                    setTimeout(function() {                                        
                                        $('#waitPopup').dialog('destroy').remove();
                                    }, 500);
                                }}
                        ]
                    });
                    NewDialog.dialog('open');
                    setTimeout(function() {
                        $("#jCrop_target").prop("src", GLOMP_BASE_URL+"custom/uploads/users/temp/"+target.img);
                        /*create a popup for jcrop	*/
                        $('#jCrop_target').waitForImages(function() {
                            setTimeout(function() {
                                initJcrop();
                                resetCoords();
                            }, 500);
                        });
                    }, 50);                    
                    /*Popover*/
                }
            }, 100);
            
            
            
        }
</script>