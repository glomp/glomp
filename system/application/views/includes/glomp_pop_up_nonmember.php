<div style="position: absolute; height: 1px;">
    <div class="glompItemPopUp" style="position: relative;top: -119px;left: -268px;">
        <div class="row-fluid">
            <div class="span12">
                <div class="text-center"><img src="assets/frontend/img/logo-small.png" alt="Glomp Logo"></div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="alert alert-success"  id="showSuccess" style="display:none;">

                    <span id="showSuccessMessage"></span>
                </div>
                <div class="alert alert-error"  id="showError" style="display:none;">

                    <span id="showErrorMessage"></span>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <span class="label label-important" id="popUpPoints">80</span>
                        <img id="pipup_image" src="" >
                    </div>
                    <div class="span1">
                        &nbsp;
                    </div>
                    <div class="span8">
                        <div class="itemname">
                            <div id="popUpMerchant">Merchant</div> <br />
                            <div id="popUpItem">&nbsp;</div> <br />
                        </div>
                    </div>
                </div>

                <form name="frmGlomp" id="frmGlomp" method="post" action="">
                    <br/>
                    <textarea name="message" id="glomp_user_message" class="span12" placeholder="<?php echo $this->lang->line('Message'); ?>"><?php echo $message; ?></textarea>
                    <br/><br/>
                    <input type="password" name="password"  id="password" class="span12 password_this" placeholder="<?php echo $this->lang->line('Your_Password'); ?>">
                    <?php /* ?><div class="row-fluid">
                      <div class="span6">
                      <?php echo $this->lang->line('Friend_First_Name');?> *
                      <input type="text" name="user_fname"  id="user_fname" value="<?php echo $recUser->user_fname;?>" class="span12">
                      </div>
                      <div class="span6">
                      <?php echo $this->lang->line('Friend_Last_Name');?>*
                      <input type="text" name="user_lname"  id="user_lname" value="<?php echo $recUser->user_lname;?>" class="span12">
                      </div>
                      </div>
                      <?php echo $this->lang->line('Friend_Email');?> *<?php */ ?>

                    <input type="hidden" name="user_lname"  id="user_lname" value=" " class="span12" />
                    <input type="hidden" name="user_fname"  id="user_fname" value="<?php echo $name; ?>" class="span12" />
                    <input type="hidden" name="user_email"  id="user_email" value="<?php echo $email; ?>" class="span12" />
					<input type="hidden" name="location_id"  id="location_id" value="<?php echo $location_id; ?>" class="span12" />

                    <input type="hidden" name="product_id"  id="product_id" class="span12">


                    <div class="text-center">
                        <div class="forgotPassword"><a href="<?php echo base_url('landing/reset_password'); ?>"><?php echo $this->lang->line('Forgot_Password'); ?></a></div>
                        <span class="Loader" style="display:none;"><i class="icon-spinner icon-spin icon-large"></i></span>  <button type="button" class="btn-custom-gray _send_glomp" data-loading-text="loading stuff..." name="Confirm" ><?php echo $this->lang->line('Confirm'); ?></button>
                        <button type="button" id="cancel_glomp_option" class="btn-custom-white  _close_popup" style="margin-left:100px;" name="Cancel" ><?php echo $this->lang->line('Cancel'); ?></button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>