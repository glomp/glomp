<?php
$resReg = $this->regions_m->region_name_only();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $page_title; ?></title>
        <link href="<?php echo base_url('favicon_24x24_v2.png');?>"  type="image/png"  rel="icon">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/frontend/css/bootstrap.css" rel="stylesheet">
        <link href="assets/frontend/font/font-awesome/css/font-awesome.min.css" rel=
              "stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/nanoscroller.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/frontend/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/frontend/js/bootstrap.js" type="text/javascript"></script>
        <script src="assets/frontend/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script type="text/javascript">
<?php
if ($resReg->num_rows() > 0) {
    $reg_list = "";
    foreach ($resReg->result() as $recReg) {
        $reg_list .= '"' . $recReg->region_name . '"' . ",";
    }
    $reg_list = rtrim($reg_list, ",")
    ?>
                var all_location_array = [<?php echo strtolower($reg_list); ?>];

    <?php
}
?>
        </script>
        <script src="assets/frontend/js/custom.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".searchUser").nanoScroller();

                $(".addAsFriend").on('click', function(e) {
                    e.preventDefault();

                    var width = $(this).find(".addAsFriendPopUp").width();

                    var _position = $(this).data("position");


                    if (_position == 1)
                        _x = 100;
                    else if (_position == 2)
                        _x = -20;
                    else if (_position == 3)
                        _x = -140;
                    else if (_position == 4)
                        _x = -260;

                    $(this).find(".addAsFriendPopUp").css({"margin-left": _x});
                    $('.addAsFriendPopUp').hide(100);
                    $(this).find(".addAsFriendPopUp").show();

                    e.stopPropagation();
                    return false;
                })
                /*$('body').click(function(){ $('.popUp').hide(100);});*/
                $('._hide').click(function() {
                    $('.popUp').hide(100);
                });

                $('.addFren').on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).html("<i class=\"icon-ok\"></i> Friend");
                    $.ajax({
                        type: "GET",
                        url: 'profile/add_friend2/' + $(this).attr('id'),
                        cache: false,
                        success: function(html) {
                            if (html == 'success') {
                            }
                        }

                    });


                })

                /*add_friend2*/
            });
        </script>
    </head>
    <body>