<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('reset_password');?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  
  <script type="text/javascript" src="assets/mobile/js/validate.js"></script>
  <script src="assets/mobile/js/custom.js" type="text/javascript"></script>
  
  <script type="text/javascript">
  
  $().ready(function() {
	$("#frmReset").validate({
		rules: {
			email: {
				required: true,
				 email: true
				}	
		},
		messages: {
			email: "",
			
		}
		,submitHandler: function(form) {
			$.fn.loadLoader();/// calling loader 
			form.submit();
			}
		});
});
  </script>
  
</head>
<body class="bodyMarginPadding">
<?php include('includes/header.php');?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="span12">
      <div class="innerPageWrapper">	
      <div class="redeem">
      <h1><?php echo $this->lang->line('reset_password');?></h1>
         
      <div class="formHolder">
       <?php 
		  	if(isset($error_msg)){
		  ?>
        <div class="alert alert-error">
   			 <?php echo $error_msg;?>
    </div>
    <?php
			}
	if(validation_errors()!=""){
	?>
     <div class="alert alert-error">
   			 <?php echo validation_errors();?>
    </div>
    	<?php
		
			}
		if(isset($success_msg)){
		?>
    <div class="alert alert-success">
   	<?php echo $success_msg;?>
    </div>
    <?php
		}
	?>
          <?php
		  echo form_open('mobile/landing/forgotPassword/','class="form-inline" id="frmReset"');?>
     <span id="display_msg"><span id="loader"></span></span>
    <div class="control-group">
    <label class="control-label" for="email"><?php echo ucfirst($this->lang->line('Email'));?></label>
    <div class="controls">
    <input type="text" id="email" name="user_email" class="span12">
    </div>
    </div>
    
    <div class="control-group">
    <div class="controls">
    <button type="submit" name="reset_password" class="btn-custom-primary"><?php echo ucfirst($this->lang->line('reset_password'));?></button> 
    <button type="button" onClick="javascript:location.href='<?php echo base_url().'mobile/'?>'" class="btn-custom-gray"><?php echo $this->lang->line('Cancel');?></button>
    </div>
    </div>
    </form>
      </div>
      
      </div>
      </div>
		</div>
      </div>
      </div>
      </div>
</div>
</body>
<?php
if(isset($reset_confirm_popup))
{
?>
<script language="javascript" type="text/javascript">
    if(confirm('<?php echo $this->lang->line('reset_password_confirm_msg')?>'))
	{    
		 $('#loader').html('<i class="icon-spinner icon-spin icon-large"></i>  Please wait..');
		var base_url = '<?php echo base_url(MOBILE_FOLDER.'/landing/update_password/');?>';
			$.ajax({
			type:'POST',
			data:"email=<?php echo $email_address;?>",
			url:base_url,
			success:function(html){
				 $('#display_msg').html(html);		
					$('#loader').html('');
				}
			});
		
	}else
	{
		location.href='<?php echo base_url(MOBILE_FOLDER);?>'+'/landing/forgotPassword';		
	}
    
	
		
	
</script>
<?php
}
?>
</html>