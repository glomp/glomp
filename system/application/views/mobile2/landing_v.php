<?php echo $main_header; ?>
<div class="row-fluid" style="margin-top:30px;">
    <div class="span12 text-center"><img src="assets/mobile/img/glomp-login-logo.png" alt="logo">
        <div id="error_container" class="alert alert-error hide">
            <button type="button" class="close" id="error_close_btn">&times;</button>
            <div id ="errors"></div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12 landingPage">
        <div class="row-fluid">
            <div class="span12">
                <a href ="<?php echo site_url(MOBILE_FOLDER2.'/landing/what_is_glomp') ?>" class="btn-custom-gray btn-block" >What is glomp!?</a>
            </div>
        </div>
        <br />
        <?php echo form_open('mobile2/landing/check_login', 'name="frmLogin" id="frmLogin"') ?>
        <div class="row-fluid">
            <div class="span12">
                <input type="text" name="user_name" placeholder="User Email" id="user_name" value="" class="span12">
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <input type="password" name="user_password" id="user_password" value="" placeholder="Password" class="span12">
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <input type="submit" name="user_login" id="user_login"  class="btn-custom-primary btn-block" value="<?php echo $this->lang->line('Sign_in'); ?>">
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <?php
                echo anchor(MOBILE_FOLDER2 . '/landing/register', $this->lang->line('register'), 'class="btn-custom-gray btn-block"');
                ?>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>
</div>

<div class="row-fluid">
    <div class="span12 termAndConditions" >
        <?php
        echo anchor('', $this->lang->line('full_destop_website'));
        ?> | <a href="#termModel" data-toggle="modal"><?php echo $this->lang->line('termNcondition'); ?></a>
    </div>
</div>

<div id ="loader" class="modal hide fade">
    <div class="modal-body">
        <div align="center">
            <img style="width: 60px;height: 60px;" src ="assets/mobile/img/ajax-loader.gif" />
        </div>
    </div>
    <div class="modal-footer"></div>
</div>

<script>
    $('#loader').modal({
        keyboard: false,
        show: false,
        backdrop: 'static',
    });
    
    
    $('#error_close_btn').click(function(e){
        e.preventDefault();
        $('#error_container').addClass('hide');
    });

    $('#user_login').click(function(e) {
        e.preventDefault();
        var form = $('#frmLogin');
        $('#loader').modal('show');

        $.ajax({
            type: "post",
            url: $(form).attr('action'),
            dataType: 'json',
            data: $(form).serialize(),
            success: function(response) {
                $('#loader').modal('hide');

                if (!response.success) {
                    process_error(response);
                    return;
                }

                window.location = response.location;
            }
        });
    });


//    });

    function process_error(response) {
        $('#errors').html('');
        for (var e in response.errors) {
            if (response.errors[e] != '') {
                $('#error_container').removeClass('hide');
                $('#errors').append(response.errors[e]);
                $('#' + e).addClass('error_border')
            }

            //Remove any error message if theres any
            if (response.errors[e] == '') {
                $('#' + e).removeClass('error_border');
            }
        }
    }
</script>

<?php echo $footer; ?>