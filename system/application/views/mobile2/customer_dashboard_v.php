<?php 
echo $main_header;
if($this->session->userdata('user_id') && $this->session->userdata('user_id')>0)
{
	$user_id = $this->session->userdata('user_id');
	#user info
	$user_info = json_decode($this->user_account_m->user_short_info($user_id));
}
?>
<div class="row-fluid" style='background-color: #D3D9E5'>
	<div class="span6 border_only" style="padding: 20px;">
		<img style="margin-right: 5px;" src="<?php echo base_url('assets/mobile2/img/home/home_main_profile_icon.png'); ?>" />
		<a href="#" style="color:#585F6B;font-size:1.2em;font-weight: bolder">
			<?php echo stripslashes($user_info->user_name);?>
		</a>
	</div>
	<div class="span6" style="margin-left: 0px;">
		<div class="span4" align="center">
			<img src="<?php echo base_url('assets/mobile2/img/home/home_main_notify_msg_icon.png'); ?>" />
			Messages
		</div >		
		<div class="span4" style="margin-left: 0px;" align="center">
			<img src="<?php echo base_url('assets/mobile2/img/home/home_main_notify_event_icon.png'); ?>" />
			Events
		</div>
		<div class="span4" style="margin-left: 0px;" align="center" >
			<img src="<?php echo base_url('assets/mobile2/img/home/home_main_notify_req_icon.png'); ?>" />
			Request
		</div>
	</div>
</div>
<div class="row-fluid">
    <?php
    $to_a = $this->lang->line('to_a');
    foreach ($glomp_buzz->result() as $glomp) {
        $from_id = $glomp->voucher_purchaser_user_id;
        $to_id = $glomp->voucher_belongs_usser_id;
        $frn_info = json_decode($this->users_m->friends_info_buzz($from_id . ',' . $to_id));
        //print_r($frn_info);
        $ago_date = $glomp->voucher_purchased_date;

        $form_name = $frn_info->friends->$from_id->user_name;
        $from_name_photo = $this->custom_func->profile_pic($frn_info->friends->$from_id->user_prifile_pic, $frn_info->friends->$from_id->user_gender);

        $to_name = $frn_info->friends->$to_id->user_name;
        $to_name_photo = $this->custom_func->profile_pic($frn_info->friends->$to_id->user_prifile_pic, $frn_info->friends->$to_id->user_gender);
        //product and merchant info
        $prod_id = $glomp->voucher_product_id;
        $prod_info = json_decode($this->product_m->productInfo($prod_id));
        $prod_name = $prod_info->product->$prod_id->prod_name;
        $merchant_logo = $this->custom_func->merchant_logo($prod_info->product->$prod_id->merchant_logo);
        $merchant_name = $prod_info->product->$prod_id->merchant_name;
        ?>
        <div class="row-fluid" style="padding: 10px;">
            <div class="left" style="width: 280px;">
                <span style="color:red"><?php echo $form_name;?></span> glomp!ed <span style="color:red"><?php echo $to_name;?></span> <?php echo $to_a;?> <?php echo stripslashes($prod_name);?>  <?php echo $merchant_name;?>
                <br />
                <span style="color:red" class="NormalCharacterStyle24"><?php echo $this->custom_func->ago($ago_date);?></span>
            </div>
            <div class="left" style="width: 90px;" align="center">
				<div class= "home_buzz_glomp_like_button" style="margin-bottom: 10px;">
					<a class= "NormalCharacterStyle32">Like</a>
				</div>
				<div class= "home_buzz_glomp_like_button">
					<a class= "NormalCharacterStyle32">Share</a>
				</div>
			</div>
        </div>
    <?php } ?>
</div>
<?php echo $footer; ?>