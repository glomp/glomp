<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title><?php echo $this->lang->line('sorry_invalid_voucher');?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script type="text/javascript" src="assets/mobile/js/validate.js"></script>
  <script src="assets/mobile/js/custom.js" type="text/javascript"></script>
  
</head>
<body class="bodyMarginPadding">
<?php include('includes/header.php');?>
<div class="container">
	<div class="inner-content">
      <div class="row-fluid">
      <div class="span12">
      <div class="innerPageWrapper">	
      <div class="redeem">
      <h1><?php echo $this->lang->line('sorry_invalid_voucher');?></h1>
      
        <div class="alert alert-error">
   				<strong>Error!</strong> <?php echo $this->lang->line('sorry_invalid_voucher');?>
    </div>
      
      <div class="termsAndConditionsHolderpink">
      
      <div class="formHolder">
          <form class="form-inline" id="frmRedeem" name="frmRedeem" method="post">
    <div class="control-group">
    <div class="controls">
    <button type="button" onClick="javascript:location.href='<?php echo base_url(MOBILE_FOLDER.'/landing/');?>'" class="btn-custom-gray btn-block"><?php echo $this->lang->line('back');?></button>
    </div>
    </div>
    </form>
      </div>
      
      </div>
      </div>
		</div>
      </div>
      </div>
      </div>
</div>
</body>
</html>