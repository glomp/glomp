<div class="header">
    <div class="container">
        <div class="inner-content">
            <div class="row-fluid">
                <div class="left home_main_glomp_logo_box pointer">
                    <a href="<?php echo site_url('mobile2') ?>" >
                        <img src="<?php echo base_url('assets/mobile2/img/home/home_main_glomp_logo.png'); ?>" />
                    </a>
                </div>
                <div class="left">
                    <div class="row-fluid">
                        <div class="left box1 border pointer" align="center">
                            <div >
                                <img src="<?php echo base_url('assets/mobile2/img/home/home_main_glomped_icon.png'); ?>" />
                            </div>
                            <div>
                                <img src="<?php echo base_url('assets/mobile2/img/home/home_main_glomped_text.png'); ?>" />
                            </div>
                        </div>
                        
                        <div class="left box2 border" align="center">
                            <div class="pointer">
                                <img src="<?php echo base_url('assets/mobile2/img/home/home_main_friends_icon.png'); ?>" />
                                <span class="NormalCharacterStyle24">Friends</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="left box1 border" align="center">
                            <div class="pointer">
                                <img src="<?php echo base_url('assets/mobile2/img/home/tnc_header_menu_icon.png'); ?>" />
                                <br/>
                                <span class="NormalCharacterStyle24">Menu</span>
                            </div>
                        </div>
                        <div class="left box2 border" align="center">
                            <div class="pointer">
                                <img src="<?php echo base_url('assets/mobile2/img/home/home_main_points_icon.png'); ?>" />
                                <br/>
                                <span class="NormalCharacterStyle24">Points</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>