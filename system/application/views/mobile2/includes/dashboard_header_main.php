<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
        <title><?php echo $Page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <base href="<?php echo base_url(); ?>" />
        <!-- Le styles -->
        <link href="assets/mobile2/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="assets/mobile2/font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="favicon.ico" rel="shortcut icon">
        <link href="assets/mobile2/css/style.css" rel="stylesheet">
        <link href="assets/mobile2/css/glomp_mobile_Home_CC.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
              <![endif]-->
        <script src="assets/mobile2/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
        <script src="assets/mobile2/js/bootstrap.js" type="text/javascript"></script>
    </head>
    <body class="bodyMarginPadding">
        <div class="container">
            <div class="outerBorder">
                <?php echo $header; ?>
                <div class="inner-content">