<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=9; IE=8; IE=7; IE=EDGE" http-equiv="X-UA-Compatible">
  <title>Register </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="author">
  <base href="<?php echo base_url();?>" />
  <!-- Le styles -->
  <link href="assets/mobile/css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="assets/mobile/font/font-awesome/css/font-awesome.min.css" rel=
  "stylesheet">
 
  <link href="favicon.ico" rel="shortcut icon">
  <link href="assets/mobile/css/style.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   <script src="assets/mobile/js/jquery-2.0.3.min.js" type="text/javascript"></script> 
  <script src="assets/mobile/js/bootstrap.js" type="text/javascript"></script>
  <script src="assets/mobile/js/validate.js" type="text/javascript"></script>
  <script type="text/javascript">
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#frmRegister").validate({
			rules: {
				
				fname	:"required",
				lname	:"required",
				day	:{
					 required: true,
						minlength: 2,
						maxlength: 2,
						number: true,
						range: [01, 31]
					},
				months	:{
					 required: true,
						minlength: 2,
						maxlength: 2,
						number: true,
						range: [01, 12]
					},
				year	:{
					 required: true,
						minlength: 4,
						maxlength: 4,
						number: true,
						range: [1900, 3000]
					},
					gender:{ required:true },
				location	:"required",
				email	:{
				 required: true,
				 email: true
				},
				pword:"required",
				cpword:"required",
				agree:"required"
				}
				,
			messages: {
				fname:"",
				lname:"",
				day	:"",
				months	:"",
				year	:"",
				gender:"",
				location:"",
				email	:"",
				pword:"",
				cpword:"",
				agree:""
			}
	});
	});
	</script>
</head>
<body style="background:#585f6b;">
<div class="container">
	<div class="inner-content">
      <div class="row-fluid" style="margin-top:30px;">
      <div class="span12 text-center"><img src="assets/mobile/img/glomp-login-logo.png" alt="logo">
      
         <?php
				  if(validation_errors()!="")
				  {
					  echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".validation_errors()."</div>";
				  }
				  if(isset($error_msg))
				  {
					   echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$error_msg."</div>";
					  
					  }
					   ?>
      </div>
      </div>
      <div class="row-fluid">
      <div class="span12 registerPage">
         	<?php echo form_open_multipart('mobile/landing/register','name="frmCms" class="form-inline" id="frmRegister"')?>
                      
    	  <div class="control-group">
    <label class="control-label" for="fname"><?php echo $this->lang->line('fame');?></label>
    <div class="controls">
    <input type="text"  id="fname" name="fname" value="<?php echo set_value('fname');?>" class="span12 textBoxGray" >
    </div>
    </div>
     <div class="control-group">
    <label class="control-label" for="lname"><?php echo $this->lang->line('lname');?></label>
    <div class="controls">
    <input type="text"  id="lname" name="lname" value="<?php echo set_value('lname');?>" class="span12 textBoxGray" >
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="dob"><?php echo $this->lang->line('Date_of_Birth');?></label>
    <div class="controls">
    <input type="number" id="day" name="day" maxlength="2" value="<?php echo set_value('day');?>" style="max-width:50px;" placeholder="DD" class="textBoxGray"> /
     <input type="number" id="months" name="months" value="<?php echo set_value('months');?>" maxlength="2"  style="max-width:50px;" placeholder="MM" class="textBoxGray"> /
      <input type="number" id="year" name="year" maxlength="4" value="<?php echo set_value('year');?>"  style="max-width:100px;" placeholder="YYYY" class="textBoxGray">
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="gender-male"><?php echo $this->lang->line('Gender');?></label>
    <div class="controls">
    <label class="radio inline">
<input type="radio" name="gender" id="gender-male" value="Male" <?php echo $checked=($this->input->post('gender')!='Female')?'Checked="checked"':"";?>  /> <?php echo $this->lang->line('Male');?>
</label>
<label class="radio inline">
<input type="radio" name="gender" id="gender-female" <?php echo $checked=($this->input->post('gender')=='Female')?'Checked="checked"':"";?> value="Female"> <?php echo $this->lang->line('Female');?>
</label>
    </div>
    </div>
    
    <div class="control-group">
    <label class="control-label" for="location"><?php echo $this->lang->line('Location');?></label>
    <div class="controls">
    
      <select name="location" id="location" class="textBoxGray">
      <option value=""><?php echo $this->lang->line('Select_Location');?></option>
      <?php 
	  echo $this->regions_m->location_dropdown(0,set_value('location'));
	  ?>
      </select>
    </div>
    </div>
    <div class="control-group">
                    <label class="control-label" for="email"><?php echo $this->lang->line('E-mail');?></label>
                    <div class="controls">
                    <input type="text"  id="email" value="<?php echo set_value('email');?>" name="email" class="span12 textBoxGray" rel="popover" data-content="<?php echo $this->lang->line('emailTips');?>" data-original-title="<?php echo $this->lang->line('E-mail');?>" >
                    </div>
                    </div>
                    
                    <div class="control-group">
                    <label class="control-label" for="pword"><?php echo $this->lang->line('Create_Password');?></label>
                    <div class="controls">
                    <input type="password"  id="pword" name="pword" class="span12 textBoxGray" >
                    </div>
                    </div>
                    <div class="control-group">
                    <label class="control-label" for="cpword"><?php echo $this->lang->line('Confirm_Password');?></label>
                    <div class="controls">
                    <input type="password" id="cpword" name="cpword" class="span12 textBoxGray" >
                    </div>
                    </div>
                    
                    <div class="control-group">
                    <label class="control-label" for="user_photo"><?php echo $this->lang->line('Profile_Picture');?></label>
                    <div class="controls">
                    <input type="file" name="user_photo" id="fileUploadField" />
                    </div>
                    </div>
                       
                       <label class="checkbox">
                <input type="checkbox" name="agree">
				<?php echo $this->lang->line('I_Agree_to_the');?><strong> 
				<?php echo $this->lang->line('termNconditi0n');?></strong> <a href="<?php echo base_url('index.php/page/index/2');?>" target="_blank">
                <?php echo $this->lang->line('Read');?></a>
                </label>
                
                <div class="control-group" style="padding-top:10px;">
                <input type="hidden" name="token" value="<?php echo $token;?>" />
    <button type="submit" class="btn-custom-gray " name="Register" ><?php echo $this->lang->line('Submit');?></button>
    <div class="pull-right"><button type="reset" class="btn-custom-gray" name="Reset" ><?php echo $this->lang->line('Reset_All_Fields');?></button></div>
    
    <div class="clearfix"></div>
    </div>
    	</form>
        
      </div>
      </div>
      </div>
</div>
</body>
</html>