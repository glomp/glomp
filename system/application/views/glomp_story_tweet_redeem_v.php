<?php    
    $prod_info = json_decode($this->product_m->productInfo($product_id));    
    $merchant_name = $prod_info->product->$product_id->merchant_name;
    $prod_name = $prod_info->product->$product_id->prod_name;
    $prod_image= $prod_info->product->$product_id->prod_image;        
    $product_logo = base_url().$this->custom_func->product_logo($prod_image);
    //glomp_story/tweet/'.$voucher
?>
<!DOCTYPE html>
<html>
     <head>
        <title>glomp!</title>
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@glompit">
        <meta name="twitter:title" content="glomp!">
        <meta name="twitter:description" content="Up your 'Like'-ability. glomp! is a digital platform where you can give and receive real enjoyable treats such as a coffee, ice-cream, beer and so on between friends. Its time for a treat!">
        <meta name="twitter:creator" content="@glompit">
        <meta name="twitter:image:src" content="<?php echo $product_logo;?>">
        <meta name="twitter:domain" content="<?php echo base_url();?>">        
        <meta http-equiv="refresh" content="0; url=<?php echo base_url('landing/?voucher='.$voucher);?>">
    </head>
    <body style=" background:#d3d7e1;">
        <div>            
            <?php echo $belongs_data->user_name;?> has redeemed a <?php echo $merchant_name;?> <?php echo stripslashes($prod_name);?>.
        </div>
    </body>
</html>

 